.class public Lcom/konka/demomode/MainActivity;
.super Landroid/app/Activity;
.source "MainActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# instance fields
.field private mMediaController:Landroid/widget/MediaController;

.field private mVideoPath:Ljava/lang/String;

.field private mVideoView:Landroid/widget/VideoView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/demomode/MainActivity;->mVideoPath:Ljava/lang/String;

    return-void
.end method

.method private switchToStorageInputSource()V
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/view/KeyEvent;

    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;

    const-string v0, "onCompletion-----"

    invoke-static {v0}, Lcom/konka/demomode/tools/DMLog;->Log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/demomode/MainActivity;->mVideoView:Landroid/widget/VideoView;

    iget-object v1, p0, Lcom/konka/demomode/MainActivity;->mVideoPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setVideoPath(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/demomode/MainActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/high16 v1, 0x7f030000    # com.konka.demomode.R.layout.activity_main

    invoke-virtual {p0, v1}, Lcom/konka/demomode/MainActivity;->setContentView(I)V

    const-string v1, "MainActivity onCreate()"

    invoke-static {v1}, Lcom/konka/demomode/tools/DMLog;->Log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/demomode/MainActivity;->switchToStorageInputSource()V

    invoke-virtual {p0}, Lcom/konka/demomode/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "vfn"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/demomode/MainActivity;->mVideoPath:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/demomode/MainActivity;->mVideoPath:Ljava/lang/String;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/demomode/MainActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/widget/MediaController;

    invoke-direct {v1, p0}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/demomode/MainActivity;->mMediaController:Landroid/widget/MediaController;

    const/high16 v1, 0x7f080000    # com.konka.demomode.R.id.vidoe_view

    invoke-virtual {p0, v1}, Lcom/konka/demomode/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/VideoView;

    iput-object v1, p0, Lcom/konka/demomode/MainActivity;->mVideoView:Landroid/widget/VideoView;

    iget-object v1, p0, Lcom/konka/demomode/MainActivity;->mVideoView:Landroid/widget/VideoView;

    iget-object v2, p0, Lcom/konka/demomode/MainActivity;->mMediaController:Landroid/widget/MediaController;

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setMediaController(Landroid/widget/MediaController;)V

    iget-object v1, p0, Lcom/konka/demomode/MainActivity;->mMediaController:Landroid/widget/MediaController;

    iget-object v2, p0, Lcom/konka/demomode/MainActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v1, v2}, Landroid/widget/MediaController;->setMediaPlayer(Landroid/widget/MediaController$MediaPlayerControl;)V

    iget-object v1, p0, Lcom/konka/demomode/MainActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v1, p0}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v1, p0, Lcom/konka/demomode/MainActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v1, p0}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v1, p0, Lcom/konka/demomode/MainActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v1, p0}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v1, p0, Lcom/konka/demomode/MainActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->requestFocus()Z

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/konka/demomode/MainActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f070000    # com.konka.demomode.R.menu.main

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const-string v0, "onError-----"

    invoke-static {v0}, Lcom/konka/demomode/tools/DMLog;->Log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/demomode/MainActivity;->mVideoView:Landroid/widget/VideoView;

    iget-object v1, p0, Lcom/konka/demomode/MainActivity;->mVideoPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setVideoPath(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/demomode/MainActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    const/4 v0, 0x0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 1

    iget-object v0, p0, Lcom/konka/demomode/MainActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1    # Landroid/media/MediaPlayer;

    const-string v0, "onPrepared-----"

    invoke-static {v0}, Lcom/konka/demomode/tools/DMLog;->Log(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    iget-object v0, p0, Lcom/konka/demomode/MainActivity;->mVideoView:Landroid/widget/VideoView;

    iget-object v1, p0, Lcom/konka/demomode/MainActivity;->mVideoPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setVideoPath(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/demomode/MainActivity;->mVideoView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    return-void
.end method
