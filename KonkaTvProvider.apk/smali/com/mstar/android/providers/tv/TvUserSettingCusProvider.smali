.class public Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;
.super Landroid/content/ContentProvider;
.source "TvUserSettingCusProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;,
        Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;
    }
.end annotation


# static fields
.field private static final s_urlMatcher:Landroid/content/UriMatcher;


# instance fields
.field private PicMode_Setting:Ljava/lang/String;

.field private SoundModeSetting:Ljava/lang/String;

.field private syncRun:Ljava/lang/Runnable;

.field private tvUserSettingCusDB:Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;

.field private userHandler:Landroid/os/Handler;

.field private userSettingCusDB:Landroid/database/sqlite/SQLiteDatabase;

.field private userThread:Landroid/os/HandlerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "konka.tv.usersetting"

    const-string v2, "picmode_setting"

    const/16 v3, 0x69

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "konka.tv.usersetting"

    const-string v2, "picmode_setting/inputsrc/#/picmode/#"

    const/16 v3, 0x6a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "konka.tv.usersetting"

    const-string v2, "soundmodesetting"

    const/16 v3, 0x73

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "konka.tv.usersetting"

    const-string v2, "soundmodesetting/#"

    const/16 v3, 0x74

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "com.konka.android.tv.usersetting.handler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userThread:Landroid/os/HandlerThread;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$1;

    invoke-direct {v0, p0}, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$1;-><init>(Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;)V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->syncRun:Ljava/lang/Runnable;

    const-string v0, "tbl_PicMode_Setting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->PicMode_Setting:Ljava/lang/String;

    const-string v0, "tbl_SoundModeSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->SoundModeSetting:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$400(Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->syncRun:Ljava/lang/Runnable;

    return-object v0
.end method

.method private closeDB()V
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userSettingCusDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userSettingCusDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    return-void
.end method

.method private openDB()V
    .locals 6

    const/16 v5, 0x14

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userSettingCusDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userSettingCusDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v2

    if-nez v2, :cond_4

    :cond_0
    const-string v2, "TvUserSettingCusProvider"

    const-string v3, "================>>>>>> open db start"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    if-ge v1, v5, :cond_4

    :try_start_0
    const-string v2, "/customercfg/user_setting.db"

    const/4 v3, 0x0

    const/16 v4, 0x10

    invoke-static {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userSettingCusDB:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userSettingCusDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v2, :cond_1

    const-string v2, "TvUserSettingCusProvider"

    const-string v3, "================>>>>>> open db success"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x14

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "TvUserSettingCusProvider"

    const-string v3, "================>>>>>> open db fail,retry ..."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v1, v1, 0x1

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userSettingCusDB:Landroid/database/sqlite/SQLiteDatabase;

    if-lt v1, v5, :cond_2

    const-string v2, "TvUserSettingCusProvider"

    const-string v3, "!!!!!!!!!!open db fail,Please check sw bug!!!!!!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userSettingCusDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v2, :cond_1

    const-string v2, "TvUserSettingCusProvider"

    const-string v3, "================>>>>>> open db success"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x14

    goto :goto_0

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userSettingCusDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v3, :cond_3

    const-string v3, "TvUserSettingCusProvider"

    const-string v4, "================>>>>>> open db success"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x14

    :cond_3
    throw v2

    :cond_4
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    const/4 v0, -0x1

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 2

    invoke-direct {p0}, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->openDB()V

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userSettingCusDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {v0, p0, v1}, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;-><init>(Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;Landroid/database/sqlite/SQLiteDatabase;)V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->tvUserSettingCusDB:Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userSettingCusDB:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    sget-object v1, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userSettingCusDB:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    invoke-virtual {p0}, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v8, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    return-object v8

    :sswitch_0
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->PicMode_Setting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->PicMode_Setting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "InputSrcType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x2

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and PictureModeType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->SoundModeSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->SoundModeSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x69 -> :sswitch_0
        0x6a -> :sswitch_1
        0x73 -> :sswitch_2
        0x74 -> :sswitch_3
    .end sparse-switch
.end method

.method public shutdown()V
    .locals 2

    const-string v0, "TvUserSettingCusProvider"

    const-string v1, "================>>>>>> now shutdown"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/content/ContentProvider;->shutdown()V

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    invoke-direct {p0}, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->closeDB()V

    return-void
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 10
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :goto_0
    return v6

    :sswitch_0
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->tvUserSettingCusDB:Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->PicMode_Setting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    :sswitch_1
    if-nez p3, :cond_0

    if-eqz p4, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->tvUserSettingCusDB:Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->PicMode_Setting:Ljava/lang/String;

    const-string v4, "InputSrcType=? and PictureModeType=?"

    new-array v5, v9, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v3

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v8

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->tvUserSettingCusDB:Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->SoundModeSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    :sswitch_3
    if-nez p3, :cond_2

    if-eqz p4, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->tvUserSettingCusDB:Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->SoundModeSetting:Ljava/lang/String;

    const-string v4, "_id=?"

    new-array v5, v8, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v3

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x69 -> :sswitch_0
        0x6a -> :sswitch_1
        0x73 -> :sswitch_2
        0x74 -> :sswitch_3
    .end sparse-switch
.end method
