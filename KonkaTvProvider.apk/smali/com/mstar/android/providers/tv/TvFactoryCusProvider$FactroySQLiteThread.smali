.class Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;
.super Ljava/lang/Thread;
.source "TvFactoryCusProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/providers/tv/TvFactoryCusProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FactroySQLiteThread"
.end annotation


# instance fields
.field db:Landroid/database/sqlite/SQLiteDatabase;

.field factoryDBTable:Ljava/lang/String;

.field factoryDBURI:Landroid/net/Uri;

.field factoryDBValues:Landroid/content/ContentValues;

.field factoryDBWhereArgs:[Ljava/lang/String;

.field factoryDBWhereClause:Ljava/lang/String;

.field redetect:I

.field ret:J

.field final synthetic this$0:Lcom/mstar/android/providers/tv/TvFactoryCusProvider;


# direct methods
.method public constructor <init>(Lcom/mstar/android/providers/tv/TvFactoryCusProvider;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .param p2    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3    # Landroid/net/Uri;
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/content/ContentValues;
    .param p6    # Ljava/lang/String;
    .param p7    # [Ljava/lang/String;

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->this$0:Lcom/mstar/android/providers/tv/TvFactoryCusProvider;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->ret:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->redetect:I

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->db:Landroid/database/sqlite/SQLiteDatabase;

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->factoryDBURI:Landroid/net/Uri;

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->factoryDBTable:Ljava/lang/String;

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->factoryDBValues:Landroid/content/ContentValues;

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->factoryDBWhereClause:Ljava/lang/String;

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->factoryDBWhereArgs:[Ljava/lang/String;

    iput-object p2, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->db:Landroid/database/sqlite/SQLiteDatabase;

    iput-object p3, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->factoryDBURI:Landroid/net/Uri;

    iput-object p4, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->factoryDBTable:Ljava/lang/String;

    iput-object p5, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->factoryDBValues:Landroid/content/ContentValues;

    iput-object p6, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->factoryDBWhereClause:Ljava/lang/String;

    iput-object p7, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->factoryDBWhereArgs:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const-wide/16 v5, 0x0

    :goto_0
    iget-wide v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->ret:J

    cmp-long v0, v0, v5

    if-gez v0, :cond_0

    iget v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->redetect:I

    const/16 v1, 0x1e

    if-ge v0, v1, :cond_0

    const-wide/16 v0, 0x32

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->db:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->factoryDBTable:Ljava/lang/String;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->factoryDBValues:Landroid/content/ContentValues;

    iget-object v3, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->factoryDBWhereClause:Ljava/lang/String;

    iget-object v4, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->factoryDBWhereArgs:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->ret:J

    iget v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->redetect:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->redetect:I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    iget-wide v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->ret:J

    cmp-long v0, v0, v5

    if-gez v0, :cond_1

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, " update factoryDB db fail !!!"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->this$0:Lcom/mstar/android/providers/tv/TvFactoryCusProvider;

    invoke-virtual {v0}, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->factoryDBURI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_1
.end method
