.class Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;
.super Ljava/lang/Thread;
.source "TvUserSettingCusProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UserSQLiteThread"
.end annotation


# instance fields
.field redetect:I

.field ret:J

.field final synthetic this$0:Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;

.field userDB:Landroid/database/sqlite/SQLiteDatabase;

.field userTable:Ljava/lang/String;

.field userURI:Landroid/net/Uri;

.field userValues:Landroid/content/ContentValues;

.field userWhereArgs:[Ljava/lang/String;

.field userWhereClause:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .param p2    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3    # Landroid/net/Uri;
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/content/ContentValues;
    .param p6    # Ljava/lang/String;
    .param p7    # [Ljava/lang/String;

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->this$0:Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->ret:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->redetect:I

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userDB:Landroid/database/sqlite/SQLiteDatabase;

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userURI:Landroid/net/Uri;

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userTable:Ljava/lang/String;

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userValues:Landroid/content/ContentValues;

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userWhereClause:Ljava/lang/String;

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userWhereArgs:[Ljava/lang/String;

    iput-object p2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userDB:Landroid/database/sqlite/SQLiteDatabase;

    iput-object p3, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userURI:Landroid/net/Uri;

    iput-object p4, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userTable:Ljava/lang/String;

    iput-object p5, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userValues:Landroid/content/ContentValues;

    iput-object p6, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userWhereClause:Ljava/lang/String;

    iput-object p7, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userWhereArgs:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    const-wide/16 v6, 0x0

    :goto_0
    iget-wide v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->ret:J

    cmp-long v1, v1, v6

    if-gez v1, :cond_0

    iget v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->redetect:I

    const/16 v2, 0x1e

    if-ge v1, v2, :cond_0

    const-wide/16 v1, 0x32

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userDB:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userTable:Ljava/lang/String;

    iget-object v3, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userValues:Landroid/content/ContentValues;

    iget-object v4, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userWhereClause:Ljava/lang/String;

    iget-object v5, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userWhereArgs:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->ret:J

    iget v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->redetect:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->redetect:I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v0

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->ret:J

    goto :goto_0

    :cond_0
    iget-wide v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->ret:J

    cmp-long v1, v1, v6

    if-gez v1, :cond_1

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, " update usersetting db fail !!!"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->this$0:Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;

    invoke-virtual {v1}, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->userURI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->this$0:Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;

    # getter for: Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->access$400(Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->this$0:Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;

    # getter for: Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->syncRun:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->access$500(Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->this$0:Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;

    # getter for: Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->userHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->access$400(Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteThread;->this$0:Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;

    # getter for: Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->syncRun:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;->access$500(Lcom/mstar/android/providers/tv/TvUserSettingCusProvider;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method
