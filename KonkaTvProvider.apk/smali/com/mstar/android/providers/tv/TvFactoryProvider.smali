.class public Lcom/mstar/android/providers/tv/TvFactoryProvider;
.super Landroid/content/ContentProvider;
.source "TvFactoryProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/providers/tv/TvFactoryProvider$FactroySQLiteThread;,
        Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;
    }
.end annotation


# static fields
.field private static final s_urlMatcher:Landroid/content/UriMatcher;


# instance fields
.field private AdcAdjust:Ljava/lang/String;

.field private AtvOverscanSetting:Ljava/lang/String;

.field private CusDefSetting:Ljava/lang/String;

.field private DtvOverscanSetting:Ljava/lang/String;

.field private FactoryAudioSetting:Ljava/lang/String;

.field private FactoryColorTemp:Ljava/lang/String;

.field private FactoryColorTempEx:Ljava/lang/String;

.field private FactoryDBVersion:Ljava/lang/String;

.field private FactoryExtern:Ljava/lang/String;

.field private HDMIOverScanSetting:Ljava/lang/String;

.field private MiscSetting:Ljava/lang/String;

.field private NonLinearAdjust:Ljava/lang/String;

.field private NonStandardAdjust:Ljava/lang/String;

.field private OverScanAdjust:Ljava/lang/String;

.field private PEQAdjust:Ljava/lang/String;

.field private SRSAdjust:Ljava/lang/String;

.field private SSCAdjust:Ljava/lang/String;

.field private YPBPROverScanSetting:Ljava/lang/String;

.field private factoryDB:Landroid/database/sqlite/SQLiteDatabase;

.field private factoryHandler:Landroid/os/Handler;

.field private factoryThread:Landroid/os/HandlerThread;

.field private tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "adcadjust"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "adcadjust/sourceid/#"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "atvoverscansetting"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "atvoverscansetting/resolutiontypenum/#/_id/#"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "dtvoverscansetting"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "dtvoverscansetting/resolutiontypenum/#/_id/#"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "factoryaudiosetting"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "factorycolortemp"

    const/16 v3, 0xf

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "factorycolortemp/colortemperatureid/#"

    const/16 v3, 0x10

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "factorycolortempex"

    const/16 v3, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "factorycolortempex/inputsourceid/#/colortemperatureid/#"

    const/16 v3, 0x15

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "factoryextern"

    const/16 v3, 0x19

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "factory_db_version"

    const/16 v3, 0x1e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "hdmioverscansetting"

    const/16 v3, 0x23

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "hdmioverscansetting/resolutiontypenum/#/_id/#"

    const/16 v3, 0x24

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "nonlinearadjust"

    const/16 v3, 0x28

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "nonlinearadjust/inputsrctype/#/curvetypeindex/#"

    const/16 v3, 0x29

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "nonstandardadjust"

    const/16 v3, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "overscanadjust"

    const/16 v3, 0x37

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "overscanadjust/factoryoverscantype/#/_id/#"

    const/16 v3, 0x38

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "peqadjust"

    const/16 v3, 0x3c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "peqadjust/#"

    const/16 v3, 0x3d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "sscadjust"

    const/16 v3, 0x41

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "ypbproverscansetting"

    const/16 v3, 0x46

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "ypbproverscansetting/resolutiontypenum/#/_id/#"

    const/16 v3, 0x47

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "miscsetting"

    const/16 v3, 0x48

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "srsadjust"

    const/16 v3, 0x49

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.factory"

    const-string v2, "cusdefsetting"

    const/16 v3, 0x4a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "com.mstar.android.tv.factory.handler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->factoryThread:Landroid/os/HandlerThread;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->factoryHandler:Landroid/os/Handler;

    const-string v0, "tbl_ADCAdjust"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->AdcAdjust:Ljava/lang/String;

    const-string v0, "tbl_ATVOverscanSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->AtvOverscanSetting:Ljava/lang/String;

    const-string v0, "tbl_DTVOverscanSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->DtvOverscanSetting:Ljava/lang/String;

    const-string v0, "tbl_FactoryAudioSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryAudioSetting:Ljava/lang/String;

    const-string v0, "tbl_FactoryColorTemp"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryColorTemp:Ljava/lang/String;

    const-string v0, "tbl_FactoryColorTempEx"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryColorTempEx:Ljava/lang/String;

    const-string v0, "tbl_FactoryExtern"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryExtern:Ljava/lang/String;

    const-string v0, "tbl_Factory_DB_VERSION"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryDBVersion:Ljava/lang/String;

    const-string v0, "tbl_HDMIOverscanSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->HDMIOverScanSetting:Ljava/lang/String;

    const-string v0, "tbl_NonLinearAdjust"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->NonLinearAdjust:Ljava/lang/String;

    const-string v0, "tbl_NonStandardAdjust"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->NonStandardAdjust:Ljava/lang/String;

    const-string v0, "tbl_OverscanAdjust"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->OverScanAdjust:Ljava/lang/String;

    const-string v0, "tbl_PEQAdjust"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->PEQAdjust:Ljava/lang/String;

    const-string v0, "tbl_SSCAdjust"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->SSCAdjust:Ljava/lang/String;

    const-string v0, "tbl_YPbPrOverscanSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->YPBPROverScanSetting:Ljava/lang/String;

    const-string v0, "tbl_MiscSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->MiscSetting:Ljava/lang/String;

    const-string v0, "tbl_SRSAdjust"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->SRSAdjust:Ljava/lang/String;

    const-string v0, "tbl_CusDefSettings"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->CusDefSetting:Ljava/lang/String;

    return-void
.end method

.method private closeDB()V
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->factoryDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    return-void
.end method

.method private openDB()V
    .locals 6

    const/16 v5, 0x14

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->factoryDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->factoryDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v2

    if-nez v2, :cond_4

    :cond_0
    const-string v2, "FactoryProvider"

    const-string v3, "================>>>>>> open db start"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    if-ge v1, v5, :cond_4

    :try_start_0
    const-string v2, "/tvdatabase/Database/factory.db"

    const/4 v3, 0x0

    const/16 v4, 0x10

    invoke-static {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->factoryDB:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->factoryDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v2, :cond_1

    const-string v2, "FactoryProvider"

    const-string v3, "================>>>>>> open db success"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x14

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "FactoryProvider"

    const-string v3, "================>>>>>> open db fail,retry ..."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v1, v1, 0x1

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->factoryDB:Landroid/database/sqlite/SQLiteDatabase;

    if-lt v1, v5, :cond_2

    const-string v2, "FactoryProvider"

    const-string v3, "!!!!!!!!!!open db fail,Please check sw bug!!!!!!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->factoryDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v2, :cond_1

    const-string v2, "FactoryProvider"

    const-string v3, "================>>>>>> open db success"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x14

    goto :goto_0

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->factoryDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v3, :cond_3

    const-string v3, "FactoryProvider"

    const-string v4, "================>>>>>> open db success"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x14

    :cond_3
    throw v2

    :cond_4
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    const/4 v0, -0x1

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 2

    invoke-direct {p0}, Lcom/mstar/android/providers/tv/TvFactoryProvider;->openDB()V

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->factoryThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->factoryThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->factoryHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->factoryDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {v0, p0, v1}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;-><init>(Lcom/mstar/android/providers/tv/TvFactoryProvider;Landroid/database/sqlite/SQLiteDatabase;)V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->factoryDB:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v3, 0x2

    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    sget-object v1, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->factoryDB:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    invoke-virtual {p0}, Lcom/mstar/android/providers/tv/TvFactoryProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v8, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    return-object v8

    :sswitch_0
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->AdcAdjust:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->AdcAdjust:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SourceID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->AtvOverscanSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->AtvOverscanSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and ResolutionTypeNum = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto :goto_0

    :sswitch_4
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->DtvOverscanSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_5
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->DtvOverscanSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and ResolutionTypeNum = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryAudioSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryColorTemp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryColorTemp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ColorTemperatureID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryColorTempEx:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryColorTempEx:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ColorTemperatureID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and InputSourceID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryExtern:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryDBVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->HDMIOverScanSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_e
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->HDMIOverScanSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and ResolutionTypeNum = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->NonLinearAdjust:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_10
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->NonLinearAdjust:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CurveTypeIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and InputSrcType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_11
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->NonStandardAdjust:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_12
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->OverScanAdjust:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_13
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->OverScanAdjust:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and FactoryOverScanType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_14
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->PEQAdjust:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_15
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->PEQAdjust:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_16
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->SSCAdjust:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_17
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->YPBPROverScanSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_18
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->YPBPROverScanSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and ResolutionTypeNum = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_19
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->MiscSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1a
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->SRSAdjust:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1b
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->CusDefSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0xa -> :sswitch_6
        0xf -> :sswitch_7
        0x10 -> :sswitch_8
        0x14 -> :sswitch_9
        0x15 -> :sswitch_a
        0x19 -> :sswitch_b
        0x1e -> :sswitch_c
        0x23 -> :sswitch_d
        0x24 -> :sswitch_e
        0x28 -> :sswitch_f
        0x29 -> :sswitch_10
        0x32 -> :sswitch_11
        0x37 -> :sswitch_12
        0x38 -> :sswitch_13
        0x3c -> :sswitch_14
        0x3d -> :sswitch_15
        0x41 -> :sswitch_16
        0x46 -> :sswitch_17
        0x47 -> :sswitch_18
        0x48 -> :sswitch_19
        0x49 -> :sswitch_1a
        0x4a -> :sswitch_1b
    .end sparse-switch
.end method

.method public shutdown()V
    .locals 2

    const-string v0, "FactoryProvider"

    const-string v1, "================>>>>>> now shutdown"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/content/ContentProvider;->shutdown()V

    invoke-direct {p0}, Lcom/mstar/android/providers/tv/TvFactoryProvider;->closeDB()V

    return-void
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 10
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v3, 0x2

    const/4 v6, 0x0

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :goto_0
    return v6

    :sswitch_0
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->AdcAdjust:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    :sswitch_1
    if-nez p3, :cond_0

    if-eqz p4, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->AdcAdjust:Ljava/lang/String;

    const-string v4, "SourceID=?"

    new-array v5, v9, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v8

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->AtvOverscanSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    :sswitch_3
    if-nez p3, :cond_2

    if-eqz p4, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->AtvOverscanSetting:Ljava/lang/String;

    const-string v4, "ResolutionTypeNum=? and _id=?"

    new-array v5, v3, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v8

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v9

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->DtvOverscanSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_5
    if-nez p3, :cond_4

    if-eqz p4, :cond_5

    :cond_4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->DtvOverscanSetting:Ljava/lang/String;

    const-string v4, "ResolutionTypeNum=? and _id=?"

    new-array v5, v3, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v8

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v9

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryAudioSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryColorTemp:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_8
    if-nez p3, :cond_6

    if-eqz p4, :cond_7

    :cond_6
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryColorTemp:Ljava/lang/String;

    const-string v4, "ColorTemperatureID=?"

    new-array v5, v9, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v8

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryColorTempEx:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_a
    if-nez p3, :cond_8

    if-eqz p4, :cond_9

    :cond_8
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryColorTempEx:Ljava/lang/String;

    const-string v4, "InputSourceID=? and ColorTemperatureID=?"

    new-array v5, v3, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v8

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v9

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryExtern:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->FactoryDBVersion:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->HDMIOverScanSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_e
    if-nez p3, :cond_a

    if-eqz p4, :cond_b

    :cond_a
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->HDMIOverScanSetting:Ljava/lang/String;

    const-string v4, "ResolutionTypeNum=? and _id=?"

    new-array v5, v3, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v8

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v9

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->NonLinearAdjust:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_10
    if-nez p3, :cond_c

    if-eqz p4, :cond_d

    :cond_c
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->NonLinearAdjust:Ljava/lang/String;

    const-string v4, "InputSrcType=? and CurveTypeIndex=?"

    new-array v5, v3, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v8

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v9

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->NonStandardAdjust:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_12
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->OverScanAdjust:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_13
    if-nez p3, :cond_e

    if-eqz p4, :cond_f

    :cond_e
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->OverScanAdjust:Ljava/lang/String;

    const-string v4, "FactoryOverScanType=? and _id=?"

    new-array v5, v3, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v8

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v9

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_14
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->PEQAdjust:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_15
    if-nez p3, :cond_10

    if-eqz p4, :cond_11

    :cond_10
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->PEQAdjust:Ljava/lang/String;

    const-string v4, "_id=?"

    new-array v5, v9, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v8

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_16
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->SSCAdjust:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_17
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->YPBPROverScanSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_18
    if-nez p3, :cond_12

    if-eqz p4, :cond_13

    :cond_12
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_13
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->YPBPROverScanSetting:Ljava/lang/String;

    const-string v4, "FactoryOverScanType=? and _id=?"

    new-array v5, v3, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v8

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v9

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_19
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->MiscSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_1a
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->SRSAdjust:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_1b
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->tvFactoryDB:Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryProvider;->CusDefSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryProvider$FactorySQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0xa -> :sswitch_6
        0xf -> :sswitch_7
        0x10 -> :sswitch_8
        0x14 -> :sswitch_9
        0x15 -> :sswitch_a
        0x19 -> :sswitch_b
        0x1e -> :sswitch_c
        0x23 -> :sswitch_d
        0x24 -> :sswitch_e
        0x28 -> :sswitch_f
        0x29 -> :sswitch_10
        0x32 -> :sswitch_11
        0x37 -> :sswitch_12
        0x38 -> :sswitch_13
        0x3c -> :sswitch_14
        0x3d -> :sswitch_15
        0x41 -> :sswitch_16
        0x46 -> :sswitch_17
        0x47 -> :sswitch_18
        0x48 -> :sswitch_19
        0x49 -> :sswitch_1a
        0x4a -> :sswitch_1b
    .end sparse-switch
.end method
