.class Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$1$StreamGobbler;
.super Ljava/lang/Thread;
.source "TvUserSettingCusProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "StreamGobbler"
.end annotation


# instance fields
.field is:Ljava/io/InputStream;

.field final synthetic this$1:Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$1;

.field type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$1;Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/io/InputStream;
    .param p3    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$1$StreamGobbler;->this$1:Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$1;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$1$StreamGobbler;->is:Ljava/io/InputStream;

    iput-object p3, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$1$StreamGobbler;->type:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    :try_start_0
    new-instance v2, Ljava/io/InputStreamReader;

    iget-object v4, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$1$StreamGobbler;->is:Ljava/io/InputStream;

    invoke-direct {v2, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$1$StreamGobbler;->type:Ljava/lang/String;

    const-string v5, "Error"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error   :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    :cond_0
    return-void

    :cond_1
    :try_start_1
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Debug:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
