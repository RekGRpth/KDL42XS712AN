.class public Lcom/mstar/android/providers/tv/TvUserSettingProvider;
.super Landroid/content/ContentProvider;
.source "TvUserSettingProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteThread;,
        Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;
    }
.end annotation


# static fields
.field private static final s_urlMatcher:Landroid/content/UriMatcher;


# instance fields
.field private BlockSysSetting:Ljava/lang/String;

.field private CECSetting:Ljava/lang/String;

.field private CISetting:Ljava/lang/String;

.field private ChinaDVBCSetting:Ljava/lang/String;

.field private DB_VERSION:Ljava/lang/String;

.field private DvbtPresetting:Ljava/lang/String;

.field private EpgTimer:Ljava/lang/String;

.field private FavTypeName:Ljava/lang/String;

.field private InputSource_Type:Ljava/lang/String;

.field private IsdbSysSetting:Ljava/lang/String;

.field private IsdbUserSetting:Ljava/lang/String;

.field private MediumSetting:Ljava/lang/String;

.field private MfcMode:Ljava/lang/String;

.field private NRMode:Ljava/lang/String;

.field private NitInfo:Ljava/lang/String;

.field private Nit_TSInfo:Ljava/lang/String;

.field private OADInfo:Ljava/lang/String;

.field private OADInfo_UntDescriptor:Ljava/lang/String;

.field private OADWakeUpInfo:Ljava/lang/String;

.field private PicMode_Setting:Ljava/lang/String;

.field private PipSetting:Ljava/lang/String;

.field private SNConfig:Ljava/lang/String;

.field private SRSSetting:Ljava/lang/String;

.field private SoundModeSetting:Ljava/lang/String;

.field private SoundSetting:Ljava/lang/String;

.field private StandbyMode:Ljava/lang/String;

.field private SubtitleSetting:Ljava/lang/String;

.field private SystemSetting:Ljava/lang/String;

.field private ThreeDVideoMode:Ljava/lang/String;

.field private ThreeDVideoRouterSetting:Ljava/lang/String;

.field private TimeSetting:Ljava/lang/String;

.field private UserColorTemp:Ljava/lang/String;

.field private UserColorTempEx:Ljava/lang/String;

.field private UserLocationSetting:Ljava/lang/String;

.field private UserMMSetting:Ljava/lang/String;

.field private UserOverScanMode:Ljava/lang/String;

.field private UserPCModeSetting:Ljava/lang/String;

.field private VideoSetting:Ljava/lang/String;

.field private _3DInfo:Ljava/lang/String;

.field private _3DSetting:Ljava/lang/String;

.field private syncRun:Ljava/lang/Runnable;

.field private tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

.field private userHandler:Landroid/os/Handler;

.field private userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

.field private userThread:Landroid/os/HandlerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "3dinfo"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "3dsetting"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "blocksyssetting"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "cecsetting"

    const/16 v3, 0xf

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "chinadvbcsetting"

    const/16 v3, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "cisetting"

    const/16 v3, 0x19

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "cisetting/#"

    const/16 v3, 0x1a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "db_version"

    const/16 v3, 0x1e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "dvbtpresetting"

    const/16 v3, 0x23

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "epgtimer"

    const/16 v3, 0x28

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "epgtimer/#"

    const/16 v3, 0x29

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "favtypename"

    const/16 v3, 0x2d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "favtypename/typeid/#"

    const/16 v3, 0x2e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "inputsource_type"

    const/16 v3, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "inputsource_type/#"

    const/16 v3, 0x33

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "isdbsyssetting"

    const/16 v3, 0x37

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "isdbusersetting"

    const/16 v3, 0x3c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "mediumsetting"

    const/16 v3, 0x41

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "mfcmode"

    const/16 v3, 0x46

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "nit_tsinfo"

    const/16 v3, 0x4b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "nit_tsinfo/nit_id/#/id/#"

    const/16 v3, 0x4c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "nitinfo"

    const/16 v3, 0x50

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "nitinfo/#"

    const/16 v3, 0x51

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "nrmode"

    const/16 v3, 0x55

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "nrmode/nrmode/#/inputsrc/#"

    const/16 v3, 0x56

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "oadinfo"

    const/16 v3, 0x5a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "oadinfo_untdescriptor"

    const/16 v3, 0x5f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "oadinfo_untdescriptor/#"

    const/16 v3, 0x60

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "oadwakeupinfo"

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "picmode_setting"

    const/16 v3, 0x69

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "picmode_setting/inputsrc/#/picmode/#"

    const/16 v3, 0x6a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "pipsetting"

    const/16 v3, 0x6e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "snconfig"

    const/16 v3, 0x6f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "soundmodesetting"

    const/16 v3, 0x73

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "soundmodesetting/#"

    const/16 v3, 0x74

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "soundsetting"

    const/16 v3, 0x78

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "subtitlesetting"

    const/16 v3, 0x7d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "systemsetting"

    const/16 v3, 0x82

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "threedvideomode"

    const/16 v3, 0x87

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "threedvideomode/inputsrc/#"

    const/16 v3, 0x88

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "threedvideoroutersetting"

    const/16 v3, 0x8c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "threedvideoroutersetting/e3dtype/#"

    const/16 v3, 0x8d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "timesetting"

    const/16 v3, 0x91

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "usercolortemp"

    const/16 v3, 0x96

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "usercolortempex"

    const/16 v3, 0x9b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "usercolortempex/#"

    const/16 v3, 0x9c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "userlocationsetting"

    const/16 v3, 0xa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "usermmsetting"

    const/16 v3, 0xa5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "useroverscanmode"

    const/16 v3, 0xaa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "useroverscanmode/inputsrc/#"

    const/16 v3, 0xab

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "userpcmodesetting"

    const/16 v3, 0xaf

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "userpcmodesetting/#"

    const/16 v3, 0xb0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "videosetting"

    const/16 v3, 0xb4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "videosetting/inputsrc/#"

    const/16 v3, 0xb5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "standbymode"

    const/16 v3, 0xb6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "mstar.tv.usersetting"

    const-string v2, "srssetting"

    const/16 v3, 0xb7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "com.mstar.android.tv.usersetting.handler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userThread:Landroid/os/HandlerThread;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$1;

    invoke-direct {v0, p0}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$1;-><init>(Lcom/mstar/android/providers/tv/TvUserSettingProvider;)V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->syncRun:Ljava/lang/Runnable;

    const-string v0, "tbl_3DInfo"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->_3DInfo:Ljava/lang/String;

    const-string v0, "tbl_3DSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->_3DSetting:Ljava/lang/String;

    const-string v0, "tbl_BlockSysSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->BlockSysSetting:Ljava/lang/String;

    const-string v0, "tbl_CECSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->CECSetting:Ljava/lang/String;

    const-string v0, "tbl_ChinaDVBCSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->ChinaDVBCSetting:Ljava/lang/String;

    const-string v0, "tbl_CISetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->CISetting:Ljava/lang/String;

    const-string v0, "tbl_DB_VERSION"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->DB_VERSION:Ljava/lang/String;

    const-string v0, "tbl_DvbtPresetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->DvbtPresetting:Ljava/lang/String;

    const-string v0, "tbl_EpgTimer"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->EpgTimer:Ljava/lang/String;

    const-string v0, "tbl_FavTypeName"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->FavTypeName:Ljava/lang/String;

    const-string v0, "tbl_InputSource_Type"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->InputSource_Type:Ljava/lang/String;

    const-string v0, "tbl_IsdbSysSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->IsdbSysSetting:Ljava/lang/String;

    const-string v0, "tbl_IsdbUserSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->IsdbUserSetting:Ljava/lang/String;

    const-string v0, "tbl_MediumSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->MediumSetting:Ljava/lang/String;

    const-string v0, "tbl_MfcMode"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->MfcMode:Ljava/lang/String;

    const-string v0, "tbl_Nit_TSInfo"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->Nit_TSInfo:Ljava/lang/String;

    const-string v0, "tbl_NitInfo"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->NitInfo:Ljava/lang/String;

    const-string v0, "tbl_NRMode"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->NRMode:Ljava/lang/String;

    const-string v0, "tbl_OADInfo"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->OADInfo:Ljava/lang/String;

    const-string v0, "tbl_OADInfo_UntDescriptor"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->OADInfo_UntDescriptor:Ljava/lang/String;

    const-string v0, "tbl_OADWakeUpInfo"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->OADWakeUpInfo:Ljava/lang/String;

    const-string v0, "tbl_PicMode_Setting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->PicMode_Setting:Ljava/lang/String;

    const-string v0, "tbl_PipSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->PipSetting:Ljava/lang/String;

    const-string v0, "tbl_SNConfig"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SNConfig:Ljava/lang/String;

    const-string v0, "tbl_SoundModeSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SoundModeSetting:Ljava/lang/String;

    const-string v0, "tbl_SoundSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SoundSetting:Ljava/lang/String;

    const-string v0, "tbl_SubtitleSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SubtitleSetting:Ljava/lang/String;

    const-string v0, "tbl_SystemSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SystemSetting:Ljava/lang/String;

    const-string v0, "tbl_ThreeDVideoMode"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->ThreeDVideoMode:Ljava/lang/String;

    const-string v0, "tbl_ThreeDVideoRouterSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->ThreeDVideoRouterSetting:Ljava/lang/String;

    const-string v0, "tbl_TimeSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->TimeSetting:Ljava/lang/String;

    const-string v0, "tbl_UserColorTemp"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserColorTemp:Ljava/lang/String;

    const-string v0, "tbl_UserColorTempEx"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserColorTempEx:Ljava/lang/String;

    const-string v0, "tbl_UserLocationSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserLocationSetting:Ljava/lang/String;

    const-string v0, "tbl_UserMMSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserMMSetting:Ljava/lang/String;

    const-string v0, "tbl_UserOverScanMode"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserOverScanMode:Ljava/lang/String;

    const-string v0, "tbl_UserPCModeSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserPCModeSetting:Ljava/lang/String;

    const-string v0, "tbl_VideoSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->VideoSetting:Ljava/lang/String;

    const-string v0, "tbl_StandbyMode"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->StandbyMode:Ljava/lang/String;

    const-string v0, "tbl_SRSSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SRSSetting:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$400(Lcom/mstar/android/providers/tv/TvUserSettingProvider;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mstar/android/providers/tv/TvUserSettingProvider;

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mstar/android/providers/tv/TvUserSettingProvider;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/mstar/android/providers/tv/TvUserSettingProvider;

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->syncRun:Ljava/lang/Runnable;

    return-object v0
.end method

.method private closeDB()V
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    return-void
.end method

.method private openDB()V
    .locals 6

    const/16 v5, 0x14

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v2

    if-nez v2, :cond_4

    :cond_0
    const-string v2, "UserSettingProvider"

    const-string v3, "================>>>>>> open db start"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    if-ge v1, v5, :cond_4

    :try_start_0
    const-string v2, "/tvdatabase/Database/user_setting.db"

    const/4 v3, 0x0

    const/16 v4, 0x10

    invoke-static {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v2, :cond_1

    const-string v2, "UserSettingProvider"

    const-string v3, "================>>>>>> open db success"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x14

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "UserSettingProvider"

    const-string v3, "================>>>>>> open db fail,retry ..."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v1, v1, 0x1

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    if-lt v1, v5, :cond_2

    const-string v2, "UserSettingProvider"

    const-string v3, "!!!!!!!!!!open db fail,Please check sw bug!!!!!!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v2, :cond_1

    const-string v2, "UserSettingProvider"

    const-string v3, "================>>>>>> open db success"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x14

    goto :goto_0

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v3, :cond_3

    const-string v3, "UserSettingProvider"

    const-string v4, "================>>>>>> open db success"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x14

    :cond_3
    throw v2

    :cond_4
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    const/4 v0, -0x1

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 2

    invoke-direct {p0}, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->openDB()V

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {v0, p0, v1}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;-><init>(Lcom/mstar/android/providers/tv/TvUserSettingProvider;Landroid/database/sqlite/SQLiteDatabase;)V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v3, 0x2

    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    sget-object v1, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    invoke-virtual {p0}, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v8, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    return-object v8

    :sswitch_0
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->_3DInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->_3DSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->BlockSysSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->CECSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_4
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->ChinaDVBCSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_5
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->CISetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_6
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->CISetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto :goto_0

    :sswitch_7
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->DB_VERSION:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_8
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->DvbtPresetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_9
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->EpgTimer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_a
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->EpgTimer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->FavTypeName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->FavTypeName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TypeId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->InputSource_Type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_e
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->InputSource_Type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->IsdbSysSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_10
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->IsdbUserSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_11
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->MediumSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_12
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->MfcMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_13
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->Nit_TSInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_14
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->Nit_TSInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and _NIT_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_15
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->NitInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_16
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->NitInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_17
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->NRMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_18
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->NRMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NRMode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and InputSrcType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_19
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->OADInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1a
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->OADInfo_UntDescriptor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1b
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->OADInfo_UntDescriptor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_1c
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->OADWakeUpInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1d
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->PicMode_Setting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_1e
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->PicMode_Setting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "InputSrcType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and PictureModeType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_1f
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->PipSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_20
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SNConfig:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_21
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SoundModeSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_22
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SoundModeSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_23
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SoundSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_24
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SubtitleSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_25
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SystemSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_26
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->ThreeDVideoMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_27
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->ThreeDVideoMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "InputSrcType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_28
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->ThreeDVideoRouterSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_29
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->ThreeDVideoRouterSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "e3DType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_2a
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->TimeSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_2b
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserColorTemp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_2c
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserColorTempEx:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_2d
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserColorTempEx:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_2e
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserLocationSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_2f
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserMMSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_30
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserOverScanMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_31
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserOverScanMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "InputSrcType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_32
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserPCModeSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_33
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserPCModeSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "_id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_34
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->VideoSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_35
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->VideoSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "InputSrcType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_36
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->StandbyMode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_37
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SRSSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_1
        0xa -> :sswitch_2
        0xf -> :sswitch_3
        0x14 -> :sswitch_4
        0x19 -> :sswitch_5
        0x1a -> :sswitch_6
        0x1e -> :sswitch_7
        0x23 -> :sswitch_8
        0x28 -> :sswitch_9
        0x29 -> :sswitch_a
        0x2d -> :sswitch_b
        0x2e -> :sswitch_c
        0x32 -> :sswitch_d
        0x33 -> :sswitch_e
        0x37 -> :sswitch_f
        0x3c -> :sswitch_10
        0x41 -> :sswitch_11
        0x46 -> :sswitch_12
        0x4b -> :sswitch_13
        0x4c -> :sswitch_14
        0x50 -> :sswitch_15
        0x51 -> :sswitch_16
        0x55 -> :sswitch_17
        0x56 -> :sswitch_18
        0x5a -> :sswitch_19
        0x5f -> :sswitch_1a
        0x60 -> :sswitch_1b
        0x64 -> :sswitch_1c
        0x69 -> :sswitch_1d
        0x6a -> :sswitch_1e
        0x6e -> :sswitch_1f
        0x6f -> :sswitch_20
        0x73 -> :sswitch_21
        0x74 -> :sswitch_22
        0x78 -> :sswitch_23
        0x7d -> :sswitch_24
        0x82 -> :sswitch_25
        0x87 -> :sswitch_26
        0x88 -> :sswitch_27
        0x8c -> :sswitch_28
        0x8d -> :sswitch_29
        0x91 -> :sswitch_2a
        0x96 -> :sswitch_2b
        0x9b -> :sswitch_2c
        0x9c -> :sswitch_2d
        0xa0 -> :sswitch_2e
        0xa5 -> :sswitch_2f
        0xaa -> :sswitch_30
        0xab -> :sswitch_31
        0xaf -> :sswitch_32
        0xb0 -> :sswitch_33
        0xb4 -> :sswitch_34
        0xb5 -> :sswitch_35
        0xb6 -> :sswitch_36
        0xb7 -> :sswitch_37
    .end sparse-switch
.end method

.method public shutdown()V
    .locals 2

    const-string v0, "UserSettingProvider"

    const-string v1, "================>>>>>> now shutdown"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/content/ContentProvider;->shutdown()V

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    invoke-direct {p0}, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->closeDB()V

    return-void
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 10
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    sget-object v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :goto_0
    return v6

    :sswitch_0
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->_3DInfo:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->_3DSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->BlockSysSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->CECSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->ChinaDVBCSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->CISetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    :sswitch_6
    if-nez p3, :cond_0

    if-eqz p4, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->CISetting:Ljava/lang/String;

    const-string v4, "_id=?"

    new-array v5, v8, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v3

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->DB_VERSION:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->DvbtPresetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->EpgTimer:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_a
    if-nez p3, :cond_2

    if-eqz p4, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->EpgTimer:Ljava/lang/String;

    const-string v4, "_id=?"

    new-array v5, v8, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v3

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->FavTypeName:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_c
    if-nez p3, :cond_4

    if-eqz p4, :cond_5

    :cond_4
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->FavTypeName:Ljava/lang/String;

    const-string v4, "TypeId=?"

    new-array v5, v8, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v3

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->InputSource_Type:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_e
    if-nez p3, :cond_6

    if-eqz p4, :cond_7

    :cond_6
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->InputSource_Type:Ljava/lang/String;

    const-string v4, "_id=?"

    new-array v5, v8, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v3

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->IsdbSysSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->IsdbSysSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->MediumSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_12
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->MfcMode:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_13
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->Nit_TSInfo:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_14
    if-nez p3, :cond_8

    if-eqz p4, :cond_9

    :cond_8
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->Nit_TSInfo:Ljava/lang/String;

    const-string v4, "_id=? and _NIT_id=?"

    new-array v5, v9, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v3

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v8

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_15
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->NitInfo:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_16
    if-nez p3, :cond_a

    if-eqz p4, :cond_b

    :cond_a
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->NitInfo:Ljava/lang/String;

    const-string v4, "_id=?"

    new-array v5, v8, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v3

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_17
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->NRMode:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_18
    if-nez p3, :cond_c

    if-eqz p4, :cond_d

    :cond_c
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->NRMode:Ljava/lang/String;

    const-string v4, "NRMode=? and InputSrcType=?"

    new-array v5, v9, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v3

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v8

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_19
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->OADInfo:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_1a
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->OADInfo_UntDescriptor:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_1b
    if-nez p3, :cond_e

    if-eqz p4, :cond_f

    :cond_e
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->OADInfo_UntDescriptor:Ljava/lang/String;

    const-string v4, "_id=?"

    new-array v5, v8, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v3

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_1c
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->OADWakeUpInfo:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_1d
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->PicMode_Setting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_1e
    if-nez p3, :cond_10

    if-eqz p4, :cond_11

    :cond_10
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->PicMode_Setting:Ljava/lang/String;

    const-string v4, "InputSrcType=? and PictureModeType=?"

    new-array v5, v9, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v3

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v8

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_1f
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->PipSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_20
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SNConfig:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_21
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SoundModeSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_22
    if-nez p3, :cond_12

    if-eqz p4, :cond_13

    :cond_12
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_13
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SoundModeSetting:Ljava/lang/String;

    const-string v4, "_id=?"

    new-array v5, v8, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v3

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_23
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SoundSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_24
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SubtitleSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_25
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SystemSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_26
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->ThreeDVideoMode:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_27
    if-nez p3, :cond_14

    if-eqz p4, :cond_15

    :cond_14
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_15
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->ThreeDVideoMode:Ljava/lang/String;

    const-string v4, "InputSrcType=?"

    new-array v5, v8, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v3

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_28
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->ThreeDVideoRouterSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_29
    if-nez p3, :cond_16

    if-eqz p4, :cond_17

    :cond_16
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_17
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->ThreeDVideoRouterSetting:Ljava/lang/String;

    const-string v4, "e3DType=?"

    new-array v5, v8, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v3

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_2a
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->TimeSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_2b
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserColorTemp:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_2c
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserColorTempEx:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_2d
    if-nez p3, :cond_18

    if-eqz p4, :cond_19

    :cond_18
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_19
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserColorTempEx:Ljava/lang/String;

    const-string v4, "_id=?"

    new-array v5, v8, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v3

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_2e
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserLocationSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_2f
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserMMSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_30
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserOverScanMode:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_31
    if-nez p3, :cond_1a

    if-eqz p4, :cond_1b

    :cond_1a
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1b
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserOverScanMode:Ljava/lang/String;

    const-string v4, "InputSrcType=?"

    new-array v5, v8, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v3

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_32
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserPCModeSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_33
    if-nez p3, :cond_1c

    if-eqz p4, :cond_1d

    :cond_1c
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1d
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->UserPCModeSetting:Ljava/lang/String;

    const-string v4, "_id=?"

    new-array v5, v8, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v3

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_34
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->VideoSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_35
    if-nez p3, :cond_1e

    if-eqz p4, :cond_1f

    :cond_1e
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1f
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->VideoSetting:Ljava/lang/String;

    const-string v4, "InputSrcType=?"

    new-array v5, v8, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v3

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_36
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->StandbyMode:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_37
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->SRSSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_1
        0xa -> :sswitch_2
        0xf -> :sswitch_3
        0x14 -> :sswitch_4
        0x19 -> :sswitch_5
        0x1a -> :sswitch_6
        0x1e -> :sswitch_7
        0x23 -> :sswitch_8
        0x28 -> :sswitch_9
        0x29 -> :sswitch_a
        0x2d -> :sswitch_b
        0x2e -> :sswitch_c
        0x32 -> :sswitch_d
        0x33 -> :sswitch_e
        0x37 -> :sswitch_f
        0x3c -> :sswitch_10
        0x41 -> :sswitch_11
        0x46 -> :sswitch_12
        0x4b -> :sswitch_13
        0x4c -> :sswitch_14
        0x50 -> :sswitch_15
        0x51 -> :sswitch_16
        0x55 -> :sswitch_17
        0x56 -> :sswitch_18
        0x5a -> :sswitch_19
        0x5f -> :sswitch_1a
        0x60 -> :sswitch_1b
        0x64 -> :sswitch_1c
        0x69 -> :sswitch_1d
        0x6a -> :sswitch_1e
        0x6e -> :sswitch_1f
        0x6f -> :sswitch_20
        0x73 -> :sswitch_21
        0x74 -> :sswitch_22
        0x78 -> :sswitch_23
        0x7d -> :sswitch_24
        0x82 -> :sswitch_25
        0x87 -> :sswitch_26
        0x88 -> :sswitch_27
        0x8c -> :sswitch_28
        0x8d -> :sswitch_29
        0x91 -> :sswitch_2a
        0x96 -> :sswitch_2b
        0x9b -> :sswitch_2c
        0x9c -> :sswitch_2d
        0xa0 -> :sswitch_2e
        0xa5 -> :sswitch_2f
        0xaa -> :sswitch_30
        0xab -> :sswitch_31
        0xaf -> :sswitch_32
        0xb0 -> :sswitch_33
        0xb4 -> :sswitch_34
        0xb5 -> :sswitch_35
        0xb6 -> :sswitch_36
        0xb7 -> :sswitch_37
    .end sparse-switch
.end method
