.class Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;
.super Ljava/lang/Object;
.source "TvFactoryCusProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/providers/tv/TvFactoryCusProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UserSQLiteDatabase"
.end annotation


# instance fields
.field private db:Landroid/database/sqlite/SQLiteDatabase;

.field ret:J

.field final synthetic this$0:Lcom/mstar/android/providers/tv/TvFactoryCusProvider;


# direct methods
.method constructor <init>(Lcom/mstar/android/providers/tv/TvFactoryCusProvider;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p2    # Landroid/database/sqlite/SQLiteDatabase;

    iput-object p1, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;->this$0:Lcom/mstar/android/providers/tv/TvFactoryCusProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;->ret:J

    iput-object p2, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;->db:Landroid/database/sqlite/SQLiteDatabase;

    return-void
.end method


# virtual methods
.method public update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/ContentValues;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1, p2, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;->ret:J

    iget-wide v1, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;->ret:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    new-instance v0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;->this$0:Lcom/mstar/android/providers/tv/TvFactoryCusProvider;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;->db:Landroid/database/sqlite/SQLiteDatabase;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;-><init>(Lcom/mstar/android/providers/tv/TvFactoryCusProvider;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;->start()V

    :goto_0
    const/4 v1, 0x1

    return v1

    :cond_0
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;->this$0:Lcom/mstar/android/providers/tv/TvFactoryCusProvider;

    invoke-virtual {v1}, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0
.end method
