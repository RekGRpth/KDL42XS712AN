.class Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;
.super Ljava/lang/Object;
.source "TvUserSettingProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/providers/tv/TvUserSettingProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UserSQLiteDatabase"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;
    }
.end annotation


# instance fields
.field private curURI:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

.field private final isQueueReady:Z

.field private preURI:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

.field private queueRun:Ljava/lang/Runnable;

.field private ret:J

.field private syncCount:I

.field final synthetic this$0:Lcom/mstar/android/providers/tv/TvUserSettingProvider;

.field private userDB:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method constructor <init>(Lcom/mstar/android/providers/tv/TvUserSettingProvider;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p2    # Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->this$0:Lcom/mstar/android/providers/tv/TvUserSettingProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->isQueueReady:Z

    iput-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->userDB:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

    invoke-direct {v0, p0, v1}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;-><init>(Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;Lcom/mstar/android/providers/tv/TvUserSettingProvider$1;)V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->preURI:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

    new-instance v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

    invoke-direct {v0, p0, v1}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;-><init>(Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;Lcom/mstar/android/providers/tv/TvUserSettingProvider$1;)V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->curURI:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->ret:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->syncCount:I

    new-instance v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$1;

    invoke-direct {v0, p0}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$1;-><init>(Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;)V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->queueRun:Ljava/lang/Runnable;

    iput-object p2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->userDB:Landroid/database/sqlite/SQLiteDatabase;

    return-void
.end method

.method static synthetic access$102(Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;I)I
    .locals 0
    .param p0    # Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->syncCount:I

    return p1
.end method

.method static synthetic access$200(Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;)Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;
    .locals 1
    .param p0    # Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->curURI:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/ContentValues;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;

    invoke-direct/range {p0 .. p5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->doDataUpdate(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method private doDataUpdate(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 9
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/ContentValues;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->userDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1, p2, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->ret:J
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-wide v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->ret:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    new-instance v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteThread;

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->this$0:Lcom/mstar/android/providers/tv/TvUserSettingProvider;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->userDB:Landroid/database/sqlite/SQLiteDatabase;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteThread;-><init>(Lcom/mstar/android/providers/tv/TvUserSettingProvider;Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteThread;->start()V

    :goto_1
    return-void

    :catch_0
    move-exception v8

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->ret:J

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->this$0:Lcom/mstar/android/providers/tv/TvUserSettingProvider;

    invoke-virtual {v1}, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->this$0:Lcom/mstar/android/providers/tv/TvUserSettingProvider;

    # getter for: Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->access$400(Lcom/mstar/android/providers/tv/TvUserSettingProvider;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->this$0:Lcom/mstar/android/providers/tv/TvUserSettingProvider;

    # getter for: Lcom/mstar/android/providers/tv/TvUserSettingProvider;->syncRun:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->access$500(Lcom/mstar/android/providers/tv/TvUserSettingProvider;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->this$0:Lcom/mstar/android/providers/tv/TvUserSettingProvider;

    # getter for: Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->access$400(Lcom/mstar/android/providers/tv/TvUserSettingProvider;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->this$0:Lcom/mstar/android/providers/tv/TvUserSettingProvider;

    # getter for: Lcom/mstar/android/providers/tv/TvUserSettingProvider;->syncRun:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->access$500(Lcom/mstar/android/providers/tv/TvUserSettingProvider;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method private isNeedQueue(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "tbl_PicMode_Setting"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "tbl_SoundSetting"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "tbl_SoundModeSetting"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/ContentValues;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;

    const-wide/16 v6, 0x12c

    invoke-direct {p0, p2}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->isNeedQueue(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->preURI:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->curURI:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

    invoke-virtual {v0, v1}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;->copyFrom(Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;)V

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->curURI:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;->setData(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->curURI:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

    iget-object v0, v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;->uri:Landroid/net/Uri;

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->preURI:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

    iget-object v1, v1, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->syncCount:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->preURI:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

    iget-object v0, v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;->uri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->this$0:Lcom/mstar/android/providers/tv/TvUserSettingProvider;

    # getter for: Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->access$400(Lcom/mstar/android/providers/tv/TvUserSettingProvider;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->queueRun:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->preURI:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

    iget-object v1, v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;->uri:Landroid/net/Uri;

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->preURI:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

    iget-object v2, v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;->table:Ljava/lang/String;

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->preURI:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

    iget-object v3, v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;->values:Landroid/content/ContentValues;

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->preURI:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

    iget-object v4, v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;->whereClause:Ljava/lang/String;

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->preURI:Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;

    iget-object v5, v0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase$keptData;->whereArgs:[Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->doDataUpdate(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->syncCount:I

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->this$0:Lcom/mstar/android/providers/tv/TvUserSettingProvider;

    # getter for: Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->access$400(Lcom/mstar/android/providers/tv/TvUserSettingProvider;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->queueRun:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_2
    iget v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->syncCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->syncCount:I

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->this$0:Lcom/mstar/android/providers/tv/TvUserSettingProvider;

    # getter for: Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->access$400(Lcom/mstar/android/providers/tv/TvUserSettingProvider;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->queueRun:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->this$0:Lcom/mstar/android/providers/tv/TvUserSettingProvider;

    # getter for: Lcom/mstar/android/providers/tv/TvUserSettingProvider;->userHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/mstar/android/providers/tv/TvUserSettingProvider;->access$400(Lcom/mstar/android/providers/tv/TvUserSettingProvider;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->queueRun:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_3
    invoke-direct/range {p0 .. p5}, Lcom/mstar/android/providers/tv/TvUserSettingProvider$UserSQLiteDatabase;->doDataUpdate(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0
.end method
