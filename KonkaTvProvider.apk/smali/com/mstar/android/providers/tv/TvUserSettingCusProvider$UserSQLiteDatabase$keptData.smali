.class Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;
.super Ljava/lang/Object;
.source "TvUserSettingCusProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "keptData"
.end annotation


# instance fields
.field public table:Ljava/lang/String;

.field final synthetic this$1:Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;

.field public uri:Landroid/net/Uri;

.field public values:Landroid/content/ContentValues;

.field public whereArgs:[Ljava/lang/String;

.field public whereClause:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->this$1:Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->uri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->table:Ljava/lang/String;

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->values:Landroid/content/ContentValues;

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->whereClause:Ljava/lang/String;

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->whereArgs:[Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$1;)V
    .locals 0
    .param p1    # Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;
    .param p2    # Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;-><init>(Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase;)V

    return-void
.end method


# virtual methods
.method public copyFrom(Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;)V
    .locals 1
    .param p1    # Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;

    iget-object v0, p1, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->uri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->uri:Landroid/net/Uri;

    iget-object v0, p1, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->table:Ljava/lang/String;

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->table:Ljava/lang/String;

    iget-object v0, p1, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->values:Landroid/content/ContentValues;

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->values:Landroid/content/ContentValues;

    iget-object v0, p1, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->whereClause:Ljava/lang/String;

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->whereClause:Ljava/lang/String;

    iget-object v0, p1, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->whereArgs:[Ljava/lang/String;

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->whereArgs:[Ljava/lang/String;

    return-void
.end method

.method public setData(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/ContentValues;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->uri:Landroid/net/Uri;

    iput-object p2, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->table:Ljava/lang/String;

    iput-object p3, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->values:Landroid/content/ContentValues;

    iput-object p4, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->whereClause:Ljava/lang/String;

    iput-object p5, p0, Lcom/mstar/android/providers/tv/TvUserSettingCusProvider$UserSQLiteDatabase$keptData;->whereArgs:[Ljava/lang/String;

    return-void
.end method
