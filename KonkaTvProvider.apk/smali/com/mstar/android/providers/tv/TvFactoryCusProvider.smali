.class public Lcom/mstar/android/providers/tv/TvFactoryCusProvider;
.super Landroid/content/ContentProvider;
.source "TvFactoryCusProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/providers/tv/TvFactoryCusProvider$FactroySQLiteThread;,
        Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;
    }
.end annotation


# static fields
.field private static final s_urlMatcher:Landroid/content/UriMatcher;


# instance fields
.field private FactoryColorTempEx:Ljava/lang/String;

.field private MiscSetting:Ljava/lang/String;

.field private NonLinearAdjust:Ljava/lang/String;

.field private SRSAdjust:Ljava/lang/String;

.field private SSCAdjust:Ljava/lang/String;

.field private tvUserSettingDB:Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;

.field private userHandler:Landroid/os/Handler;

.field private userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

.field private userThread:Landroid/os/HandlerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "konka.tv.factory"

    const-string v2, "factorycolortempex"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "konka.tv.factory"

    const-string v2, "miscsetting"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "konka.tv.factory"

    const-string v2, "nonlinearadjust"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "konka.tv.factory"

    const-string v2, "nonlinearadjust/inputsrctype/#/curvetypeindex/#"

    const/16 v3, 0xb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "konka.tv.factory"

    const-string v2, "srsadjust"

    const/16 v3, 0xf

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    const-string v1, "konka.tv.factory"

    const-string v2, "sscadjust"

    const/16 v3, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "konka.tv.factory.handler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->userThread:Landroid/os/HandlerThread;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->userHandler:Landroid/os/Handler;

    const-string v0, "tbl_FactoryColorTempEx"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->FactoryColorTempEx:Ljava/lang/String;

    const-string v0, "tbl_MiscSetting"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->MiscSetting:Ljava/lang/String;

    const-string v0, "tbl_NonLinearAdjust"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->NonLinearAdjust:Ljava/lang/String;

    const-string v0, "tbl_SRSAdjust"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->SRSAdjust:Ljava/lang/String;

    const-string v0, "tbl_SSCAdjust"

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->SSCAdjust:Ljava/lang/String;

    return-void
.end method

.method private closeDB()V
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    return-void
.end method

.method private openDB()V
    .locals 3

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "/customercfg/panel/factory.db"

    const/4 v1, 0x0

    const/16 v2, 0x10

    invoke-static {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    :cond_1
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    const/4 v0, -0x1

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 2

    invoke-direct {p0}, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->openDB()V

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->userThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->userThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->userHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;

    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {v0, p0, v1}, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;-><init>(Lcom/mstar/android/providers/tv/TvFactoryCusProvider;Landroid/database/sqlite/SQLiteDatabase;)V

    iput-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    sget-object v1, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->userSettingDB:Landroid/database/sqlite/SQLiteDatabase;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    invoke-virtual {p0}, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-interface {v8, v1, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    return-object v8

    :sswitch_0
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->FactoryColorTempEx:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->MiscSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->NonLinearAdjust:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->SRSAdjust:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_4
    iget-object v1, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->SSCAdjust:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_1
        0xa -> :sswitch_2
        0xf -> :sswitch_3
        0x14 -> :sswitch_4
    .end sparse-switch
.end method

.method public shutdown()V
    .locals 2

    const-string v0, "UserSettingProvider"

    const-string v1, "================>>>>>> now shutdown"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/content/ContentProvider;->shutdown()V

    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->userThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    invoke-direct {p0}, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->closeDB()V

    return-void
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 9
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    const/4 v8, 0x2

    const/4 v6, 0x0

    sget-object v0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->s_urlMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :goto_0
    return v6

    :sswitch_0
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->FactoryColorTempEx:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->MiscSetting:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->NonLinearAdjust:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    :sswitch_3
    if-nez p3, :cond_0

    if-eqz p4, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a where clause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->NonLinearAdjust:Ljava/lang/String;

    const-string v4, "InputSrcType=? and CurveTypeIndex=?"

    new-array v5, v8, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v5, v3

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v1

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->SRSAdjust:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->tvUserSettingDB:Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;

    iget-object v2, p0, Lcom/mstar/android/providers/tv/TvFactoryCusProvider;->SSCAdjust:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/providers/tv/TvFactoryCusProvider$UserSQLiteDatabase;->update(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_1
        0xa -> :sswitch_2
        0xb -> :sswitch_3
        0xf -> :sswitch_4
        0x14 -> :sswitch_5
    .end sparse-switch
.end method
