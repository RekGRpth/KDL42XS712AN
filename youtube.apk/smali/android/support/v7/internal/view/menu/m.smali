.class public final Landroid/support/v7/internal/view/menu/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/view/menu/ad;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field a:Landroid/content/Context;

.field b:Landroid/view/LayoutInflater;

.field c:Landroid/support/v7/internal/view/menu/o;

.field d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

.field e:I

.field f:I

.field g:Landroid/support/v7/internal/view/menu/n;

.field private h:I

.field private i:Landroid/support/v7/internal/view/menu/ae;


# direct methods
.method public constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Landroid/support/v7/internal/view/menu/m;->f:I

    iput p2, p0, Landroid/support/v7/internal/view/menu/m;->e:I

    return-void
.end method

.method static synthetic a(Landroid/support/v7/internal/view/menu/m;)I
    .locals 1

    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->h:I

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/af;
    .locals 3

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/n;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/internal/view/menu/n;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/view/menu/n;-><init>(Landroid/support/v7/internal/view/menu/m;)V

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/n;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/n;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/n;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->b:Landroid/view/LayoutInflater;

    sget v1, Landroid/support/v7/a/h;->l:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/m;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/n;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/ExpandedMenuView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/ExpandedMenuView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->d:Landroid/support/v7/internal/view/menu/ExpandedMenuView;

    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Landroid/widget/ListAdapter;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/n;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/internal/view/menu/n;

    invoke-direct {v0, p0}, Landroid/support/v7/internal/view/menu/n;-><init>(Landroid/support/v7/internal/view/menu/m;)V

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/n;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/n;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Landroid/support/v7/internal/view/menu/o;)V
    .locals 2

    iget v0, p0, Landroid/support/v7/internal/view/menu/m;->e:I

    if-eqz v0, :cond_2

    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget v1, p0, Landroid/support/v7/internal/view/menu/m;->e:I

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/m;->a:Landroid/content/Context;

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/m;->b:Landroid/view/LayoutInflater;

    :cond_0
    :goto_0
    iput-object p2, p0, Landroid/support/v7/internal/view/menu/m;->c:Landroid/support/v7/internal/view/menu/o;

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/n;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/n;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/n;->notifyDataSetChanged()V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->a:Landroid/content/Context;

    if-eqz v0, :cond_0

    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->a:Landroid/content/Context;

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->b:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/internal/view/menu/m;->b:Landroid/view/LayoutInflater;

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/internal/view/menu/ae;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v7/internal/view/menu/ae;

    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/o;Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v7/internal/view/menu/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v7/internal/view/menu/ae;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/internal/view/menu/ae;->a(Landroid/support/v7/internal/view/menu/o;Z)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/aj;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/aj;->hasVisibleItems()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v0, Landroid/support/v7/internal/view/menu/r;

    invoke-direct {v0, p1}, Landroid/support/v7/internal/view/menu/r;-><init>(Landroid/support/v7/internal/view/menu/o;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/r;->a(Landroid/os/IBinder;)V

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v7/internal/view/menu/ae;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->i:Landroid/support/v7/internal/view/menu/ae;

    invoke-interface {v0, p1}, Landroid/support/v7/internal/view/menu/ae;->b(Landroid/support/v7/internal/view/menu/o;)Z

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Landroid/support/v7/internal/view/menu/s;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final c(Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/n;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/n;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final c(Landroid/support/v7/internal/view/menu/s;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/internal/view/menu/m;->c:Landroid/support/v7/internal/view/menu/o;

    iget-object v1, p0, Landroid/support/v7/internal/view/menu/m;->g:Landroid/support/v7/internal/view/menu/n;

    invoke-virtual {v1, p3}, Landroid/support/v7/internal/view/menu/n;->a(I)Landroid/support/v7/internal/view/menu/s;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/internal/view/menu/o;->a(Landroid/view/MenuItem;I)Z

    return-void
.end method
