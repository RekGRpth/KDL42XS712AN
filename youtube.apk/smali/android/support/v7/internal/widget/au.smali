.class final Landroid/support/v7/internal/widget/au;
.super Landroid/support/v7/internal/widget/ListPopupWindow;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/widget/aw;


# instance fields
.field final synthetic b:Landroid/support/v7/internal/widget/ar;

.field private c:Ljava/lang/CharSequence;

.field private d:Landroid/widget/ListAdapter;


# direct methods
.method public constructor <init>(Landroid/support/v7/internal/widget/ar;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    iput-object p1, p0, Landroid/support/v7/internal/widget/au;->b:Landroid/support/v7/internal/widget/ar;

    invoke-direct {p0, p2, p3, p4}, Landroid/support/v7/internal/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0, p1}, Landroid/support/v7/internal/widget/au;->a(Landroid/view/View;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/au;->a(Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/au;->a(I)V

    new-instance v0, Landroid/support/v7/internal/widget/y;

    new-instance v1, Landroid/support/v7/internal/widget/av;

    invoke-direct {v1, p0, p1}, Landroid/support/v7/internal/widget/av;-><init>(Landroid/support/v7/internal/widget/au;Landroid/support/v7/internal/widget/ar;)V

    invoke-direct {v0, p1, v1}, Landroid/support/v7/internal/widget/y;-><init>(Landroid/support/v7/internal/widget/v;Landroid/support/v7/internal/widget/x;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/au;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/internal/widget/au;)Landroid/widget/ListAdapter;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/internal/widget/au;->d:Landroid/widget/ListAdapter;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/widget/ListAdapter;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/v7/internal/widget/ListPopupWindow;->a(Landroid/widget/ListAdapter;)V

    iput-object p1, p0, Landroid/support/v7/internal/widget/au;->d:Landroid/widget/ListAdapter;

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 0

    iput-object p1, p0, Landroid/support/v7/internal/widget/au;->c:Ljava/lang/CharSequence;

    return-void
.end method

.method public final c()V
    .locals 6

    iget-object v0, p0, Landroid/support/v7/internal/widget/au;->b:Landroid/support/v7/internal/widget/ar;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ar;->getPaddingLeft()I

    move-result v1

    iget-object v0, p0, Landroid/support/v7/internal/widget/au;->b:Landroid/support/v7/internal/widget/ar;

    iget v0, v0, Landroid/support/v7/internal/widget/ar;->E:I

    const/4 v2, -0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/internal/widget/au;->b:Landroid/support/v7/internal/widget/ar;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ar;->getWidth()I

    move-result v2

    iget-object v0, p0, Landroid/support/v7/internal/widget/au;->b:Landroid/support/v7/internal/widget/ar;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ar;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Landroid/support/v7/internal/widget/au;->b:Landroid/support/v7/internal/widget/ar;

    iget-object v0, p0, Landroid/support/v7/internal/widget/au;->d:Landroid/widget/ListAdapter;

    check-cast v0, Landroid/widget/SpinnerAdapter;

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/au;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/support/v7/internal/widget/ar;->a(Landroid/widget/SpinnerAdapter;Landroid/graphics/drawable/Drawable;)I

    move-result v0

    sub-int/2addr v2, v1

    sub-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/au;->d(I)V

    :goto_0
    invoke-virtual {p0}, Landroid/support/v7/internal/widget/au;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v0, 0x0

    if-eqz v2, :cond_0

    iget-object v0, p0, Landroid/support/v7/internal/widget/au;->b:Landroid/support/v7/internal/widget/ar;

    invoke-static {v0}, Landroid/support/v7/internal/widget/ar;->a(Landroid/support/v7/internal/widget/ar;)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Landroid/support/v7/internal/widget/au;->b:Landroid/support/v7/internal/widget/ar;

    invoke-static {v0}, Landroid/support/v7/internal/widget/ar;->a(Landroid/support/v7/internal/widget/ar;)Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    neg-int v0, v0

    :cond_0
    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/au;->b(I)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/au;->e(I)V

    invoke-super {p0}, Landroid/support/v7/internal/widget/ListPopupWindow;->c()V

    invoke-virtual {p0}, Landroid/support/v7/internal/widget/au;->h()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    iget-object v0, p0, Landroid/support/v7/internal/widget/au;->b:Landroid/support/v7/internal/widget/ar;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ar;->d()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/au;->f(I)V

    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/internal/widget/au;->b:Landroid/support/v7/internal/widget/ar;

    iget v0, v0, Landroid/support/v7/internal/widget/ar;->E:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Landroid/support/v7/internal/widget/au;->b:Landroid/support/v7/internal/widget/ar;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ar;->getWidth()I

    move-result v0

    iget-object v2, p0, Landroid/support/v7/internal/widget/au;->b:Landroid/support/v7/internal/widget/ar;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ar;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v1

    sub-int/2addr v0, v2

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/au;->d(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Landroid/support/v7/internal/widget/au;->b:Landroid/support/v7/internal/widget/ar;

    iget v0, v0, Landroid/support/v7/internal/widget/ar;->E:I

    invoke-virtual {p0, v0}, Landroid/support/v7/internal/widget/au;->d(I)V

    goto :goto_0
.end method
