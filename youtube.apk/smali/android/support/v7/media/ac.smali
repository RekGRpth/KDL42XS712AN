.class public final Landroid/support/v7/media/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/support/v7/media/f;

.field private final b:Ljava/util/ArrayList;

.field private final c:Landroid/support/v7/media/i;

.field private d:Landroid/support/v7/media/k;


# direct methods
.method constructor <init>(Landroid/support/v7/media/f;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/ac;->b:Ljava/util/ArrayList;

    iput-object p1, p0, Landroid/support/v7/media/ac;->a:Landroid/support/v7/media/f;

    invoke-virtual {p1}, Landroid/support/v7/media/f;->c()Landroid/support/v7/media/i;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/ac;->c:Landroid/support/v7/media/i;

    return-void
.end method

.method static synthetic a(Landroid/support/v7/media/ac;)Landroid/support/v7/media/f;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ac;->a:Landroid/support/v7/media/f;

    return-object v0
.end method

.method static synthetic b(Landroid/support/v7/media/ac;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ac;->b:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method final a(Ljava/lang/String;)I
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/media/ac;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/ad;

    invoke-static {v0}, Landroid/support/v7/media/ad;->a(Landroid/support/v7/media/ad;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final a()Landroid/support/v7/media/f;
    .locals 1

    invoke-static {}, Landroid/support/v7/media/u;->d()V

    iget-object v0, p0, Landroid/support/v7/media/ac;->a:Landroid/support/v7/media/f;

    return-object v0
.end method

.method final a(Landroid/support/v7/media/k;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ac;->d:Landroid/support/v7/media/k;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Landroid/support/v7/media/ac;->d:Landroid/support/v7/media/k;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ac;->c:Landroid/support/v7/media/i;

    invoke-virtual {v0}, Landroid/support/v7/media/i;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Landroid/content/ComponentName;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ac;->c:Landroid/support/v7/media/i;

    invoke-virtual {v0}, Landroid/support/v7/media/i;->b()Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediaRouter.RouteProviderInfo{ packageName="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v7/media/ac;->c:Landroid/support/v7/media/i;

    invoke-virtual {v1}, Landroid/support/v7/media/i;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
