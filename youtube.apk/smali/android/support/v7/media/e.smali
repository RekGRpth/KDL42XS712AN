.class public final Landroid/support/v7/media/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Bundle;

.field private b:Landroid/support/v7/media/s;


# direct methods
.method private constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/v7/media/e;->a:Landroid/os/Bundle;

    return-void
.end method

.method public constructor <init>(Landroid/support/v7/media/s;Z)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/e;->a:Landroid/os/Bundle;

    iput-object p1, p0, Landroid/support/v7/media/e;->b:Landroid/support/v7/media/s;

    iget-object v0, p0, Landroid/support/v7/media/e;->a:Landroid/os/Bundle;

    const-string v1, "selector"

    invoke-virtual {p1}, Landroid/support/v7/media/s;->d()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object v0, p0, Landroid/support/v7/media/e;->a:Landroid/os/Bundle;

    const-string v1, "activeScan"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public static a(Landroid/os/Bundle;)Landroid/support/v7/media/e;
    .locals 1

    if-eqz p0, :cond_0

    new-instance v0, Landroid/support/v7/media/e;

    invoke-direct {v0, p0}, Landroid/support/v7/media/e;-><init>(Landroid/os/Bundle;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/e;->b:Landroid/support/v7/media/s;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/e;->a:Landroid/os/Bundle;

    const-string v1, "selector"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/media/s;->a(Landroid/os/Bundle;)Landroid/support/v7/media/s;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/e;->b:Landroid/support/v7/media/s;

    iget-object v0, p0, Landroid/support/v7/media/e;->b:Landroid/support/v7/media/s;

    if-nez v0, :cond_0

    sget-object v0, Landroid/support/v7/media/s;->a:Landroid/support/v7/media/s;

    iput-object v0, p0, Landroid/support/v7/media/e;->b:Landroid/support/v7/media/s;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Landroid/support/v7/media/s;
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/media/e;->e()V

    iget-object v0, p0, Landroid/support/v7/media/e;->b:Landroid/support/v7/media/s;

    return-object v0
.end method

.method public final b()Z
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/e;->a:Landroid/os/Bundle;

    const-string v1, "activeScan"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/media/e;->e()V

    iget-object v0, p0, Landroid/support/v7/media/e;->b:Landroid/support/v7/media/s;

    invoke-virtual {v0}, Landroid/support/v7/media/s;->c()Z

    move-result v0

    return v0
.end method

.method public final d()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/e;->a:Landroid/os/Bundle;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Landroid/support/v7/media/e;

    if-eqz v1, :cond_0

    check-cast p1, Landroid/support/v7/media/e;

    invoke-virtual {p0}, Landroid/support/v7/media/e;->a()Landroid/support/v7/media/s;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/v7/media/e;->a()Landroid/support/v7/media/s;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/media/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/media/e;->b()Z

    move-result v1

    invoke-virtual {p1}, Landroid/support/v7/media/e;->b()Z

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/media/e;->a()Landroid/support/v7/media/s;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/s;->hashCode()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/media/e;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    xor-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DiscoveryRequest{ selector="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/media/e;->a()Landroid/support/v7/media/s;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", activeScan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/media/e;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isValid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/media/e;->c()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
