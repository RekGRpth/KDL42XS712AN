.class final Landroid/support/v7/media/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/media/bc;
.implements Landroid/support/v7/media/bt;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/ArrayList;

.field private final c:Ljava/util/ArrayList;

.field private final d:Ljava/util/ArrayList;

.field private final e:Ljava/util/ArrayList;

.field private final f:Landroid/support/v7/media/bh;

.field private final g:Landroid/support/v7/media/aa;

.field private final h:Landroid/support/v7/media/z;

.field private final i:Landroid/support/v4/b/a/a;

.field private final j:Landroid/support/v7/media/bj;

.field private k:Landroid/support/v7/media/az;

.field private l:Landroid/support/v7/media/ad;

.field private m:Landroid/support/v7/media/ad;

.field private n:Landroid/support/v7/media/j;

.field private o:Landroid/support/v7/media/e;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/y;->b:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/y;->c:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/y;->d:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/y;->e:Ljava/util/ArrayList;

    new-instance v0, Landroid/support/v7/media/bh;

    invoke-direct {v0}, Landroid/support/v7/media/bh;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/y;->f:Landroid/support/v7/media/bh;

    new-instance v0, Landroid/support/v7/media/aa;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/media/aa;-><init>(Landroid/support/v7/media/y;B)V

    iput-object v0, p0, Landroid/support/v7/media/y;->g:Landroid/support/v7/media/aa;

    new-instance v0, Landroid/support/v7/media/z;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/media/z;-><init>(Landroid/support/v7/media/y;B)V

    iput-object v0, p0, Landroid/support/v7/media/y;->h:Landroid/support/v7/media/z;

    iput-object p1, p0, Landroid/support/v7/media/y;->a:Landroid/content/Context;

    invoke-static {p1}, Landroid/support/v4/b/a/a;->a(Landroid/content/Context;)Landroid/support/v4/b/a/a;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/y;->i:Landroid/support/v4/b/a/a;

    invoke-static {p1, p0}, Landroid/support/v7/media/bj;->a(Landroid/content/Context;Landroid/support/v7/media/bt;)Landroid/support/v7/media/bj;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/y;->j:Landroid/support/v7/media/bj;

    iget-object v0, p0, Landroid/support/v7/media/y;->j:Landroid/support/v7/media/bj;

    invoke-virtual {p0, v0}, Landroid/support/v7/media/y;->a(Landroid/support/v7/media/f;)V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/media/y;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/y;->a:Landroid/content/Context;

    return-object v0
.end method

.method private a(Landroid/support/v7/media/ac;Landroid/support/v7/media/k;)V
    .locals 15

    invoke-virtual/range {p1 .. p2}, Landroid/support/v7/media/ac;->a(Landroid/support/v7/media/k;)Z

    move-result v1

    if-eqz v1, :cond_11

    const/4 v6, 0x0

    const/4 v5, 0x0

    if-eqz p2, :cond_c

    invoke-virtual/range {p2 .. p2}, Landroid/support/v7/media/k;->b()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual/range {p2 .. p2}, Landroid/support/v7/media/k;->a()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    const/4 v1, 0x0

    move v7, v1

    :goto_0
    if-ge v7, v9, :cond_c

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/media/c;

    invoke-virtual {v1}, Landroid/support/v7/media/c;->a()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/support/v7/media/ac;->a(Ljava/lang/String;)I

    move-result v4

    if-gez v4, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/support/v7/media/ac;->c()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Landroid/support/v7/media/y;->b(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_1

    move-object v2, v3

    :goto_1
    new-instance v3, Landroid/support/v7/media/ad;

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v10, v2}, Landroid/support/v7/media/ad;-><init>(Landroid/support/v7/media/ac;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {p1 .. p1}, Landroid/support/v7/media/ac;->b(Landroid/support/v7/media/ac;)Ljava/util/ArrayList;

    move-result-object v4

    add-int/lit8 v2, v6, 0x1

    invoke-virtual {v4, v6, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v4, p0, Landroid/support/v7/media/y;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3, v1}, Landroid/support/v7/media/ad;->a(Landroid/support/v7/media/c;)I

    invoke-static {}, Landroid/support/v7/media/u;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "MediaRouter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Route added: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Landroid/support/v7/media/y;->h:Landroid/support/v7/media/z;

    const/16 v4, 0x101

    invoke-virtual {v1, v4, v3}, Landroid/support/v7/media/z;->a(ILjava/lang/Object;)V

    move v1, v5

    :goto_2
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move v5, v1

    move v6, v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x2

    :goto_3
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v11, "%s_%d"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v3, v12, v13

    const/4 v13, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v4, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Landroid/support/v7/media/y;->b(Ljava/lang/String;)I

    move-result v11

    if-gez v11, :cond_2

    move-object v2, v4

    goto :goto_1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_3
    if-ge v4, v6, :cond_4

    const-string v2, "MediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ignoring route descriptor with duplicate id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v5

    move v2, v6

    goto :goto_2

    :cond_4
    invoke-static/range {p1 .. p1}, Landroid/support/v7/media/ac;->b(Landroid/support/v7/media/ac;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/media/ad;

    invoke-static/range {p1 .. p1}, Landroid/support/v7/media/ac;->b(Landroid/support/v7/media/ac;)Ljava/util/ArrayList;

    move-result-object v10

    add-int/lit8 v3, v6, 0x1

    invoke-static {v10, v4, v6}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    invoke-virtual {v2, v1}, Landroid/support/v7/media/ad;->a(Landroid/support/v7/media/c;)I

    move-result v1

    if-eqz v1, :cond_12

    and-int/lit8 v4, v1, 0x1

    if-eqz v4, :cond_6

    invoke-static {}, Landroid/support/v7/media/u;->e()Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "MediaRouter"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v10, "Route changed: "

    invoke-direct {v6, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v4, p0, Landroid/support/v7/media/y;->h:Landroid/support/v7/media/z;

    const/16 v6, 0x103

    invoke-virtual {v4, v6, v2}, Landroid/support/v7/media/z;->a(ILjava/lang/Object;)V

    :cond_6
    and-int/lit8 v4, v1, 0x2

    if-eqz v4, :cond_8

    invoke-static {}, Landroid/support/v7/media/u;->e()Z

    move-result v4

    if-eqz v4, :cond_7

    const-string v4, "MediaRouter"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v10, "Route volume changed: "

    invoke-direct {v6, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    iget-object v4, p0, Landroid/support/v7/media/y;->h:Landroid/support/v7/media/z;

    const/16 v6, 0x104

    invoke-virtual {v4, v6, v2}, Landroid/support/v7/media/z;->a(ILjava/lang/Object;)V

    :cond_8
    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_a

    invoke-static {}, Landroid/support/v7/media/u;->e()Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "MediaRouter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Route presentation display changed: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    iget-object v1, p0, Landroid/support/v7/media/y;->h:Landroid/support/v7/media/z;

    const/16 v4, 0x105

    invoke-virtual {v1, v4, v2}, Landroid/support/v7/media/z;->a(ILjava/lang/Object;)V

    :cond_a
    iget-object v1, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    if-ne v2, v1, :cond_12

    const/4 v1, 0x1

    move v2, v3

    goto/16 :goto_2

    :cond_b
    const-string v1, "MediaRouter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Ignoring invalid provider descriptor: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    invoke-static/range {p1 .. p1}, Landroid/support/v7/media/ac;->b(Landroid/support/v7/media/ac;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_4
    if-lt v2, v6, :cond_d

    invoke-static/range {p1 .. p1}, Landroid/support/v7/media/ac;->b(Landroid/support/v7/media/ac;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/media/ad;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/support/v7/media/ad;->a(Landroid/support/v7/media/c;)I

    iget-object v3, p0, Landroid/support/v7/media/y;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_4

    :cond_d
    invoke-direct {p0, v5}, Landroid/support/v7/media/y;->a(Z)V

    invoke-static/range {p1 .. p1}, Landroid/support/v7/media/ac;->b(Landroid/support/v7/media/ac;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_5
    if-lt v2, v6, :cond_f

    invoke-static/range {p1 .. p1}, Landroid/support/v7/media/ac;->b(Landroid/support/v7/media/ac;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/media/ad;

    invoke-static {}, Landroid/support/v7/media/u;->e()Z

    move-result v3

    if-eqz v3, :cond_e

    const-string v3, "MediaRouter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Route removed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    iget-object v3, p0, Landroid/support/v7/media/y;->h:Landroid/support/v7/media/z;

    const/16 v4, 0x102

    invoke-virtual {v3, v4, v1}, Landroid/support/v7/media/z;->a(ILjava/lang/Object;)V

    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_5

    :cond_f
    invoke-static {}, Landroid/support/v7/media/u;->e()Z

    move-result v1

    if-eqz v1, :cond_10

    const-string v1, "MediaRouter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Provider changed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_10
    iget-object v1, p0, Landroid/support/v7/media/y;->h:Landroid/support/v7/media/z;

    const/16 v2, 0x203

    move-object/from16 v0, p1

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/media/z;->a(ILjava/lang/Object;)V

    :cond_11
    return-void

    :cond_12
    move v1, v5

    move v2, v3

    goto/16 :goto_2
.end method

.method static synthetic a(Landroid/support/v7/media/y;Landroid/support/v7/media/f;Landroid/support/v7/media/k;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/support/v7/media/y;->c(Landroid/support/v7/media/f;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/ac;

    invoke-direct {p0, v0, p2}, Landroid/support/v7/media/y;->a(Landroid/support/v7/media/ac;Landroid/support/v7/media/k;)V

    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Landroid/support/v7/media/y;->l:Landroid/support/v7/media/ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/y;->l:Landroid/support/v7/media/ad;

    invoke-static {v0}, Landroid/support/v7/media/y;->b(Landroid/support/v7/media/ad;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Clearing the default route because it is no longer selectable: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v7/media/y;->l:Landroid/support/v7/media/ad;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v4, p0, Landroid/support/v7/media/y;->l:Landroid/support/v7/media/ad;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/y;->l:Landroid/support/v7/media/ad;

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/media/y;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/media/y;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/ad;

    invoke-virtual {v0}, Landroid/support/v7/media/ad;->p()Landroid/support/v7/media/f;

    move-result-object v1

    iget-object v3, p0, Landroid/support/v7/media/y;->j:Landroid/support/v7/media/bj;

    if-ne v1, v3, :cond_5

    invoke-static {v0}, Landroid/support/v7/media/ad;->a(Landroid/support/v7/media/ad;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "DEFAULT_ROUTE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    invoke-static {v0}, Landroid/support/v7/media/y;->b(Landroid/support/v7/media/ad;)Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object v0, p0, Landroid/support/v7/media/y;->l:Landroid/support/v7/media/ad;

    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Found default route: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v7/media/y;->l:Landroid/support/v7/media/ad;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    invoke-static {v0}, Landroid/support/v7/media/y;->b(Landroid/support/v7/media/ad;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unselecting the current route because it is no longer selectable: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v4}, Landroid/support/v7/media/y;->c(Landroid/support/v7/media/ad;)V

    :cond_3
    iget-object v0, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    if-nez v0, :cond_6

    invoke-direct {p0}, Landroid/support/v7/media/y;->f()Landroid/support/v7/media/ad;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v7/media/y;->c(Landroid/support/v7/media/ad;)V

    :cond_4
    :goto_1
    return-void

    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    :cond_6
    if-eqz p1, :cond_4

    invoke-direct {p0}, Landroid/support/v7/media/y;->g()V

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)I
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/y;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/media/y;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/ad;

    invoke-static {v0}, Landroid/support/v7/media/ad;->c(Landroid/support/v7/media/ad;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic b(Landroid/support/v7/media/y;)Landroid/support/v7/media/bh;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/y;->f:Landroid/support/v7/media/bh;

    return-object v0
.end method

.method private static b(Landroid/support/v7/media/ad;)Z
    .locals 1

    invoke-static {p0}, Landroid/support/v7/media/ad;->d(Landroid/support/v7/media/ad;)Landroid/support/v7/media/c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Landroid/support/v7/media/ad;->b(Landroid/support/v7/media/ad;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/support/v7/media/f;)I
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/media/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/ac;

    invoke-static {v0}, Landroid/support/v7/media/ac;->a(Landroid/support/v7/media/ac;)Landroid/support/v7/media/f;

    move-result-object v0

    if-ne v0, p1, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private c(Ljava/lang/Object;)I
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/media/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/ab;

    invoke-virtual {v0}, Landroid/support/v7/media/ab;->a()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic c(Landroid/support/v7/media/y;)Landroid/support/v7/media/ad;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    return-object v0
.end method

.method private c(Landroid/support/v7/media/ad;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    if-eq v0, p1, :cond_5

    iget-object v0, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    if-eqz v0, :cond_1

    invoke-static {}, Landroid/support/v7/media/u;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Route unselected: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/y;->h:Landroid/support/v7/media/z;

    const/16 v1, 0x107

    iget-object v2, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/media/z;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v7/media/y;->n:Landroid/support/v7/media/j;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/media/y;->n:Landroid/support/v7/media/j;

    invoke-virtual {v0}, Landroid/support/v7/media/j;->c()V

    iget-object v0, p0, Landroid/support/v7/media/y;->n:Landroid/support/v7/media/j;

    invoke-virtual {v0}, Landroid/support/v7/media/j;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/media/y;->n:Landroid/support/v7/media/j;

    :cond_1
    iput-object p1, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    iget-object v0, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Landroid/support/v7/media/ad;->p()Landroid/support/v7/media/f;

    move-result-object v0

    invoke-static {p1}, Landroid/support/v7/media/ad;->a(Landroid/support/v7/media/ad;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/media/f;->a(Ljava/lang/String;)Landroid/support/v7/media/j;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/y;->n:Landroid/support/v7/media/j;

    iget-object v0, p0, Landroid/support/v7/media/y;->n:Landroid/support/v7/media/j;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/media/y;->n:Landroid/support/v7/media/j;

    invoke-virtual {v0}, Landroid/support/v7/media/j;->b()V

    :cond_2
    invoke-static {}, Landroid/support/v7/media/u;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Route selected: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v0, p0, Landroid/support/v7/media/y;->h:Landroid/support/v7/media/z;

    const/16 v1, 0x106

    iget-object v2, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/media/z;->a(ILjava/lang/Object;)V

    :cond_4
    invoke-direct {p0}, Landroid/support/v7/media/y;->g()V

    :cond_5
    return-void
.end method

.method static synthetic d(Landroid/support/v7/media/y;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/y;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic e(Landroid/support/v7/media/y;)Landroid/support/v7/media/bj;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/y;->j:Landroid/support/v7/media/bj;

    return-object v0
.end method

.method private f()Landroid/support/v7/media/ad;
    .locals 4

    iget-object v0, p0, Landroid/support/v7/media/y;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/ad;

    iget-object v1, p0, Landroid/support/v7/media/y;->l:Landroid/support/v7/media/ad;

    if-eq v0, v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/media/ad;->p()Landroid/support/v7/media/f;

    move-result-object v1

    iget-object v3, p0, Landroid/support/v7/media/y;->j:Landroid/support/v7/media/bj;

    if-ne v1, v3, :cond_1

    const-string v1, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v1}, Landroid/support/v7/media/ad;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {v0, v1}, Landroid/support/v7/media/ad;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    invoke-static {v0}, Landroid/support/v7/media/y;->b(Landroid/support/v7/media/ad;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_1
    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Landroid/support/v7/media/y;->l:Landroid/support/v7/media/ad;

    goto :goto_1
.end method

.method private g()V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/y;->f:Landroid/support/v7/media/bh;

    iget-object v1, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    invoke-virtual {v1}, Landroid/support/v7/media/ad;->k()I

    move-result v1

    iput v1, v0, Landroid/support/v7/media/bh;->a:I

    iget-object v0, p0, Landroid/support/v7/media/y;->f:Landroid/support/v7/media/bh;

    iget-object v1, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    invoke-virtual {v1}, Landroid/support/v7/media/ad;->l()I

    move-result v1

    iput v1, v0, Landroid/support/v7/media/bh;->b:I

    iget-object v0, p0, Landroid/support/v7/media/y;->f:Landroid/support/v7/media/bh;

    iget-object v1, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    invoke-virtual {v1}, Landroid/support/v7/media/ad;->j()I

    move-result v1

    iput v1, v0, Landroid/support/v7/media/bh;->c:I

    iget-object v0, p0, Landroid/support/v7/media/y;->f:Landroid/support/v7/media/bh;

    iget-object v1, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    invoke-virtual {v1}, Landroid/support/v7/media/ad;->i()I

    move-result v1

    iput v1, v0, Landroid/support/v7/media/bh;->d:I

    iget-object v0, p0, Landroid/support/v7/media/y;->f:Landroid/support/v7/media/bh;

    iget-object v1, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    invoke-virtual {v1}, Landroid/support/v7/media/ad;->h()I

    move-result v1

    iput v1, v0, Landroid/support/v7/media/bh;->e:I

    iget-object v0, p0, Landroid/support/v7/media/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/ab;

    invoke-virtual {v0}, Landroid/support/v7/media/ab;->c()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/support/v7/media/ad;
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/y;->j:Landroid/support/v7/media/bj;

    invoke-direct {p0, v0}, Landroid/support/v7/media/y;->c(Landroid/support/v7/media/f;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/ac;

    invoke-virtual {v0, p1}, Landroid/support/v7/media/ac;->a(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_0

    invoke-static {v0}, Landroid/support/v7/media/ac;->b(Landroid/support/v7/media/ac;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/ad;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)Landroid/support/v7/media/u;
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/y;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_1

    iget-object v0, p0, Landroid/support/v7/media/y;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/u;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/y;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v1

    goto :goto_0

    :cond_0
    iget-object v2, v0, Landroid/support/v7/media/u;->b:Landroid/content/Context;

    if-ne v2, p1, :cond_2

    :goto_1
    return-object v0

    :cond_1
    new-instance v0, Landroid/support/v7/media/u;

    invoke-direct {v0, p1}, Landroid/support/v7/media/u;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Landroid/support/v7/media/y;->b:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    new-instance v0, Landroid/support/v7/media/az;

    iget-object v1, p0, Landroid/support/v7/media/y;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Landroid/support/v7/media/az;-><init>(Landroid/content/Context;Landroid/support/v7/media/bc;)V

    iput-object v0, p0, Landroid/support/v7/media/y;->k:Landroid/support/v7/media/az;

    iget-object v0, p0, Landroid/support/v7/media/y;->k:Landroid/support/v7/media/az;

    invoke-virtual {v0}, Landroid/support/v7/media/az;->a()V

    return-void
.end method

.method public final a(Landroid/support/v7/media/ad;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/y;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring attempt to select removed route: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Landroid/support/v7/media/ad;->b(Landroid/support/v7/media/ad;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring attempt to select disabled route: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Landroid/support/v7/media/y;->c(Landroid/support/v7/media/ad;)V

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/media/ad;I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/y;->n:Landroid/support/v7/media/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/y;->n:Landroid/support/v7/media/j;

    invoke-virtual {v0, p2}, Landroid/support/v7/media/j;->a(I)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/media/f;)V
    .locals 4

    invoke-direct {p0, p1}, Landroid/support/v7/media/y;->c(Landroid/support/v7/media/f;)I

    move-result v0

    if-gez v0, :cond_1

    new-instance v0, Landroid/support/v7/media/ac;

    invoke-direct {v0, p1}, Landroid/support/v7/media/ac;-><init>(Landroid/support/v7/media/f;)V

    iget-object v1, p0, Landroid/support/v7/media/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Landroid/support/v7/media/u;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "MediaRouter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Provider added: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Landroid/support/v7/media/y;->h:Landroid/support/v7/media/z;

    const/16 v2, 0x201

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/media/z;->a(ILjava/lang/Object;)V

    invoke-virtual {p1}, Landroid/support/v7/media/f;->e()Landroid/support/v7/media/k;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Landroid/support/v7/media/y;->a(Landroid/support/v7/media/ac;Landroid/support/v7/media/k;)V

    iget-object v0, p0, Landroid/support/v7/media/y;->g:Landroid/support/v7/media/aa;

    invoke-virtual {p1, v0}, Landroid/support/v7/media/f;->a(Landroid/support/v7/media/g;)V

    iget-object v0, p0, Landroid/support/v7/media/y;->o:Landroid/support/v7/media/e;

    invoke-virtual {p1, v0}, Landroid/support/v7/media/f;->a(Landroid/support/v7/media/e;)V

    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/support/v7/media/y;->c(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_0

    new-instance v0, Landroid/support/v7/media/ab;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/media/ab;-><init>(Landroid/support/v7/media/y;Ljava/lang/Object;)V

    iget-object v1, p0, Landroid/support/v7/media/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/media/s;I)Z
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/media/y;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    iget-object v0, p0, Landroid/support/v7/media/y;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/ad;

    and-int/lit8 v4, p2, 0x1

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/media/ad;->f()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    invoke-virtual {v0, p1}, Landroid/support/v7/media/ad;->a(Landroid/support/v7/media/s;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final b()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/y;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final b(Landroid/support/v7/media/ad;I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/y;->n:Landroid/support/v7/media/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/y;->n:Landroid/support/v7/media/j;

    invoke-virtual {v0, p2}, Landroid/support/v7/media/j;->b(I)V

    :cond_0
    return-void
.end method

.method public final b(Landroid/support/v7/media/f;)V
    .locals 5

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Landroid/support/v7/media/y;->c(Landroid/support/v7/media/f;)I

    move-result v1

    if-ltz v1, :cond_1

    invoke-virtual {p1, v2}, Landroid/support/v7/media/f;->a(Landroid/support/v7/media/g;)V

    invoke-virtual {p1, v2}, Landroid/support/v7/media/f;->a(Landroid/support/v7/media/e;)V

    iget-object v0, p0, Landroid/support/v7/media/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/ac;

    invoke-direct {p0, v0, v2}, Landroid/support/v7/media/y;->a(Landroid/support/v7/media/ac;Landroid/support/v7/media/k;)V

    invoke-static {}, Landroid/support/v7/media/u;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "MediaRouter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Provider removed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v2, p0, Landroid/support/v7/media/y;->h:Landroid/support/v7/media/z;

    const/16 v3, 0x202

    invoke-virtual {v2, v3, v0}, Landroid/support/v7/media/z;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v7/media/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/support/v7/media/y;->c(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/y;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/ab;

    invoke-virtual {v0}, Landroid/support/v7/media/ab;->b()V

    :cond_0
    return-void
.end method

.method public final c()Landroid/support/v7/media/ad;
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/y;->l:Landroid/support/v7/media/ad;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no default route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/y;->l:Landroid/support/v7/media/ad;

    return-object v0
.end method

.method public final d()Landroid/support/v7/media/ad;
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There is no currently selected route.  The media router has not yet been fully initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/y;->m:Landroid/support/v7/media/ad;

    return-object v0
.end method

.method public final e()V
    .locals 11

    const/4 v3, 0x1

    const/4 v5, 0x0

    new-instance v8, Landroid/support/v7/media/t;

    invoke-direct {v8}, Landroid/support/v7/media/t;-><init>()V

    iget-object v0, p0, Landroid/support/v7/media/y;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v2, v5

    move v4, v5

    :goto_0
    add-int/lit8 v7, v0, -0x1

    if-ltz v7, :cond_4

    iget-object v0, p0, Landroid/support/v7/media/y;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/u;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/y;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v7

    goto :goto_0

    :cond_0
    iget-object v1, v0, Landroid/support/v7/media/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v6, v5

    :goto_1
    if-ge v6, v9, :cond_3

    iget-object v1, v0, Landroid/support/v7/media/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/media/w;

    iget-object v10, v1, Landroid/support/v7/media/w;->c:Landroid/support/v7/media/s;

    invoke-virtual {v8, v10}, Landroid/support/v7/media/t;->a(Landroid/support/v7/media/s;)Landroid/support/v7/media/t;

    iget v10, v1, Landroid/support/v7/media/w;->d:I

    and-int/lit8 v10, v10, 0x1

    if-eqz v10, :cond_1

    move v2, v3

    move v4, v3

    :cond_1
    iget v1, v1, Landroid/support/v7/media/w;->d:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    move v4, v3

    :cond_2
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_1

    :cond_3
    move v0, v7

    goto :goto_0

    :cond_4
    if-eqz v4, :cond_6

    invoke-virtual {v8}, Landroid/support/v7/media/t;->a()Landroid/support/v7/media/s;

    move-result-object v0

    :goto_2
    iget-object v1, p0, Landroid/support/v7/media/y;->o:Landroid/support/v7/media/e;

    if-eqz v1, :cond_7

    iget-object v1, p0, Landroid/support/v7/media/y;->o:Landroid/support/v7/media/e;

    invoke-virtual {v1}, Landroid/support/v7/media/e;->a()Landroid/support/v7/media/s;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v7/media/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Landroid/support/v7/media/y;->o:Landroid/support/v7/media/e;

    invoke-virtual {v1}, Landroid/support/v7/media/e;->b()Z

    move-result v1

    if-ne v1, v2, :cond_7

    :cond_5
    return-void

    :cond_6
    sget-object v0, Landroid/support/v7/media/s;->a:Landroid/support/v7/media/s;

    goto :goto_2

    :cond_7
    invoke-virtual {v0}, Landroid/support/v7/media/s;->b()Z

    move-result v1

    if-eqz v1, :cond_9

    if-nez v2, :cond_9

    iget-object v0, p0, Landroid/support/v7/media/y;->o:Landroid/support/v7/media/e;

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/media/y;->o:Landroid/support/v7/media/e;

    :goto_3
    invoke-static {}, Landroid/support/v7/media/u;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Updated discovery request: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroid/support/v7/media/y;->o:Landroid/support/v7/media/e;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-object v0, p0, Landroid/support/v7/media/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v5

    :goto_4
    if-ge v1, v2, :cond_5

    iget-object v0, p0, Landroid/support/v7/media/y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/ac;

    invoke-static {v0}, Landroid/support/v7/media/ac;->a(Landroid/support/v7/media/ac;)Landroid/support/v7/media/f;

    move-result-object v0

    iget-object v3, p0, Landroid/support/v7/media/y;->o:Landroid/support/v7/media/e;

    invoke-virtual {v0, v3}, Landroid/support/v7/media/f;->a(Landroid/support/v7/media/e;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_9
    new-instance v1, Landroid/support/v7/media/e;

    invoke-direct {v1, v0, v2}, Landroid/support/v7/media/e;-><init>(Landroid/support/v7/media/s;Z)V

    iput-object v1, p0, Landroid/support/v7/media/y;->o:Landroid/support/v7/media/e;

    goto :goto_3
.end method
