.class Landroid/support/v7/media/ag;
.super Landroid/media/MediaRouter$Callback;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/support/v7/media/af;


# direct methods
.method public constructor <init>(Landroid/support/v7/media/af;)V
    .locals 0

    invoke-direct {p0}, Landroid/media/MediaRouter$Callback;-><init>()V

    iput-object p1, p0, Landroid/support/v7/media/ag;->a:Landroid/support/v7/media/af;

    return-void
.end method


# virtual methods
.method public onRouteAdded(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ag;->a:Landroid/support/v7/media/af;

    invoke-interface {v0, p2}, Landroid/support/v7/media/af;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public onRouteChanged(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ag;->a:Landroid/support/v7/media/af;

    invoke-interface {v0, p2}, Landroid/support/v7/media/af;->d(Ljava/lang/Object;)V

    return-void
.end method

.method public onRouteGrouped(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteGroup;I)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ag;->a:Landroid/support/v7/media/af;

    return-void
.end method

.method public onRouteRemoved(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ag;->a:Landroid/support/v7/media/af;

    invoke-interface {v0, p2}, Landroid/support/v7/media/af;->c(Ljava/lang/Object;)V

    return-void
.end method

.method public onRouteSelected(Landroid/media/MediaRouter;ILandroid/media/MediaRouter$RouteInfo;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ag;->a:Landroid/support/v7/media/af;

    invoke-interface {v0, p3}, Landroid/support/v7/media/af;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public onRouteUngrouped(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;Landroid/media/MediaRouter$RouteGroup;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ag;->a:Landroid/support/v7/media/af;

    return-void
.end method

.method public onRouteUnselected(Landroid/media/MediaRouter;ILandroid/media/MediaRouter$RouteInfo;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ag;->a:Landroid/support/v7/media/af;

    return-void
.end method

.method public onRouteVolumeChanged(Landroid/media/MediaRouter;Landroid/media/MediaRouter$RouteInfo;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ag;->a:Landroid/support/v7/media/af;

    invoke-interface {v0, p2}, Landroid/support/v7/media/af;->e(Ljava/lang/Object;)V

    return-void
.end method
