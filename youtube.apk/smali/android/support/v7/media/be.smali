.class final Landroid/support/v7/media/be;
.super Landroid/support/v7/media/bd;
.source "SourceFile"


# instance fields
.field private final d:Ljava/lang/Object;

.field private final e:Ljava/lang/Object;

.field private final f:Ljava/lang/Object;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 3

    invoke-direct {p0, p1, p2}, Landroid/support/v7/media/bd;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    const-string v0, "media_router"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/be;->d:Ljava/lang/Object;

    iget-object v0, p0, Landroid/support/v7/media/be;->d:Ljava/lang/Object;

    const-string v1, ""

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/support/v7/media/ae;->a(Ljava/lang/Object;Ljava/lang/String;Z)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/be;->e:Ljava/lang/Object;

    iget-object v0, p0, Landroid/support/v7/media/be;->d:Ljava/lang/Object;

    iget-object v1, p0, Landroid/support/v7/media/be;->e:Ljava/lang/Object;

    invoke-static {v0, v1}, Landroid/support/v7/media/ae;->b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/be;->f:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/media/bh;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/be;->f:Ljava/lang/Object;

    iget v1, p1, Landroid/support/v7/media/bh;->a:I

    invoke-static {v0, v1}, Landroid/support/v7/media/ak;->c(Ljava/lang/Object;I)V

    iget-object v0, p0, Landroid/support/v7/media/be;->f:Ljava/lang/Object;

    iget v1, p1, Landroid/support/v7/media/bh;->b:I

    invoke-static {v0, v1}, Landroid/support/v7/media/ak;->d(Ljava/lang/Object;I)V

    iget-object v0, p0, Landroid/support/v7/media/be;->f:Ljava/lang/Object;

    iget v1, p1, Landroid/support/v7/media/bh;->c:I

    invoke-static {v0, v1}, Landroid/support/v7/media/ak;->e(Ljava/lang/Object;I)V

    iget-object v0, p0, Landroid/support/v7/media/be;->f:Ljava/lang/Object;

    iget v1, p1, Landroid/support/v7/media/bh;->d:I

    invoke-static {v0, v1}, Landroid/support/v7/media/ak;->b(Ljava/lang/Object;I)V

    iget-object v0, p0, Landroid/support/v7/media/be;->f:Ljava/lang/Object;

    iget v1, p1, Landroid/support/v7/media/bh;->e:I

    invoke-static {v0, v1}, Landroid/support/v7/media/ak;->a(Ljava/lang/Object;I)V

    iget-boolean v0, p0, Landroid/support/v7/media/be;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/media/be;->g:Z

    iget-object v0, p0, Landroid/support/v7/media/be;->f:Ljava/lang/Object;

    new-instance v1, Landroid/support/v7/media/bf;

    invoke-direct {v1, p0}, Landroid/support/v7/media/bf;-><init>(Landroid/support/v7/media/be;)V

    invoke-static {v1}, Landroid/support/v7/media/ae;->a(Landroid/support/v7/media/al;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/support/v7/media/ak;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Landroid/support/v7/media/be;->f:Ljava/lang/Object;

    iget-object v1, p0, Landroid/support/v7/media/be;->b:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    check-cast v1, Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setRemoteControlClient(Landroid/media/RemoteControlClient;)V

    :cond_0
    return-void
.end method
