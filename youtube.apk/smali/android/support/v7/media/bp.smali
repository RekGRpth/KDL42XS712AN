.class final Landroid/support/v7/media/bp;
.super Landroid/support/v7/media/bo;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/media/bt;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/media/bo;-><init>(Landroid/content/Context;Landroid/support/v7/media/bt;)V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/support/v7/media/bm;Landroid/support/v7/media/d;)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/support/v7/media/bo;->a(Landroid/support/v7/media/bm;Landroid/support/v7/media/d;)V

    iget-object v0, p1, Landroid/support/v7/media/bm;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getDescription()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v7/media/d;->a(Ljava/lang/String;)Landroid/support/v7/media/d;

    :cond_0
    return-void
.end method

.method protected final a(Landroid/support/v7/media/bn;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v7/media/bo;->a(Landroid/support/v7/media/bn;)V

    iget-object v0, p1, Landroid/support/v7/media/bn;->b:Ljava/lang/Object;

    iget-object v1, p1, Landroid/support/v7/media/bn;->a:Landroid/support/v7/media/ad;

    invoke-virtual {v1}, Landroid/support/v7/media/ad;->b()Ljava/lang/String;

    move-result-object v1

    check-cast v0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {v0, v1}, Landroid/media/MediaRouter$UserRouteInfo;->setDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected final a(Landroid/support/v7/media/bm;)Z
    .locals 1

    iget-object v0, p1, Landroid/support/v7/media/bm;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->isConnecting()Z

    move-result v0

    return v0
.end method

.method protected final g()V
    .locals 5

    const/4 v2, 0x1

    iget-boolean v0, p0, Landroid/support/v7/media/bp;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/bp;->a:Ljava/lang/Object;

    iget-object v1, p0, Landroid/support/v7/media/bp;->b:Ljava/lang/Object;

    invoke-static {v0, v1}, Landroid/support/v7/media/ae;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    iput-boolean v2, p0, Landroid/support/v7/media/bp;->g:Z

    iget-object v0, p0, Landroid/support/v7/media/bp;->a:Ljava/lang/Object;

    iget v3, p0, Landroid/support/v7/media/bp;->e:I

    iget-object v1, p0, Landroid/support/v7/media/bp;->b:Ljava/lang/Object;

    iget-boolean v4, p0, Landroid/support/v7/media/bp;->f:Z

    if-eqz v4, :cond_1

    :goto_0
    or-int/lit8 v2, v2, 0x2

    check-cast v0, Landroid/media/MediaRouter;

    check-cast v1, Landroid/media/MediaRouter$Callback;

    invoke-virtual {v0, v3, v1, v2}, Landroid/media/MediaRouter;->addCallback(ILandroid/media/MediaRouter$Callback;I)V

    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected final h(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/bp;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter;

    const v1, 0x800003

    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0, v1, p1}, Landroid/media/MediaRouter;->selectRoute(ILandroid/media/MediaRouter$RouteInfo;)V

    return-void
.end method

.method protected final i()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/bp;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter;

    invoke-virtual {v0}, Landroid/media/MediaRouter;->getDefaultRoute()Landroid/media/MediaRouter$RouteInfo;

    move-result-object v0

    return-object v0
.end method
