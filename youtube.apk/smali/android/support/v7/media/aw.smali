.class final Landroid/support/v7/media/aw;
.super Landroid/support/v7/media/j;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v7/media/as;

.field private final b:Ljava/lang/String;

.field private c:Z

.field private d:I

.field private e:I

.field private f:Landroid/support/v7/media/at;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/support/v7/media/as;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v7/media/aw;->a:Landroid/support/v7/media/as;

    invoke-direct {p0}, Landroid/support/v7/media/j;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/media/aw;->d:I

    iput-object p2, p0, Landroid/support/v7/media/aw;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/aw;->a:Landroid/support/v7/media/as;

    invoke-static {v0, p0}, Landroid/support/v7/media/as;->a(Landroid/support/v7/media/as;Landroid/support/v7/media/aw;)V

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/aw;->f:Landroid/support/v7/media/at;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/aw;->f:Landroid/support/v7/media/at;

    iget v1, p0, Landroid/support/v7/media/aw;->g:I

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/media/at;->a(II)V

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Landroid/support/v7/media/aw;->d:I

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/media/aw;->e:I

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/media/at;)V
    .locals 2

    iput-object p1, p0, Landroid/support/v7/media/aw;->f:Landroid/support/v7/media/at;

    iget-object v0, p0, Landroid/support/v7/media/aw;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/support/v7/media/at;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/support/v7/media/aw;->g:I

    iget-boolean v0, p0, Landroid/support/v7/media/aw;->c:Z

    if-eqz v0, :cond_1

    iget v0, p0, Landroid/support/v7/media/aw;->g:I

    invoke-virtual {p1, v0}, Landroid/support/v7/media/at;->c(I)V

    iget v0, p0, Landroid/support/v7/media/aw;->d:I

    if-ltz v0, :cond_0

    iget v0, p0, Landroid/support/v7/media/aw;->g:I

    iget v1, p0, Landroid/support/v7/media/aw;->d:I

    invoke-virtual {p1, v0, v1}, Landroid/support/v7/media/at;->a(II)V

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/media/aw;->d:I

    :cond_0
    iget v0, p0, Landroid/support/v7/media/aw;->e:I

    if-eqz v0, :cond_1

    iget v0, p0, Landroid/support/v7/media/aw;->g:I

    iget v1, p0, Landroid/support/v7/media/aw;->e:I

    invoke-virtual {p1, v0, v1}, Landroid/support/v7/media/at;->b(II)V

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/media/aw;->e:I

    :cond_1
    return-void
.end method

.method public final a(Landroid/content/Intent;Landroid/support/v7/media/x;)Z
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/aw;->f:Landroid/support/v7/media/at;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/aw;->f:Landroid/support/v7/media/at;

    iget v1, p0, Landroid/support/v7/media/aw;->g:I

    invoke-virtual {v0, v1, p1, p2}, Landroid/support/v7/media/at;->a(ILandroid/content/Intent;Landroid/support/v7/media/x;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/media/aw;->c:Z

    iget-object v0, p0, Landroid/support/v7/media/aw;->f:Landroid/support/v7/media/at;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/aw;->f:Landroid/support/v7/media/at;

    iget v1, p0, Landroid/support/v7/media/aw;->g:I

    invoke-virtual {v0, v1}, Landroid/support/v7/media/at;->c(I)V

    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/aw;->f:Landroid/support/v7/media/at;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/aw;->f:Landroid/support/v7/media/at;

    iget v1, p0, Landroid/support/v7/media/aw;->g:I

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/media/at;->b(II)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Landroid/support/v7/media/aw;->e:I

    add-int/2addr v0, p1

    iput v0, p0, Landroid/support/v7/media/aw;->e:I

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/media/aw;->c:Z

    iget-object v0, p0, Landroid/support/v7/media/aw;->f:Landroid/support/v7/media/at;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/aw;->f:Landroid/support/v7/media/at;

    iget v1, p0, Landroid/support/v7/media/aw;->g:I

    invoke-virtual {v0, v1}, Landroid/support/v7/media/at;->d(I)V

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/aw;->f:Landroid/support/v7/media/at;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/aw;->f:Landroid/support/v7/media/at;

    iget v1, p0, Landroid/support/v7/media/aw;->g:I

    invoke-virtual {v0, v1}, Landroid/support/v7/media/at;->b(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/media/aw;->f:Landroid/support/v7/media/at;

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/media/aw;->g:I

    :cond_0
    return-void
.end method
