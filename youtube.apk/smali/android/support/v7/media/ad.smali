.class public final Landroid/support/v7/media/ad;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/support/v7/media/ac;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Z

.field private final h:Ljava/util/ArrayList;

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:Landroid/view/Display;

.field private o:I

.field private p:Landroid/os/Bundle;

.field private q:Landroid/support/v7/media/c;


# direct methods
.method constructor <init>(Landroid/support/v7/media/ac;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/ad;->h:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/media/ad;->o:I

    iput-object p1, p0, Landroid/support/v7/media/ad;->a:Landroid/support/v7/media/ac;

    iput-object p2, p0, Landroid/support/v7/media/ad;->b:Ljava/lang/String;

    iput-object p3, p0, Landroid/support/v7/media/ad;->c:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Landroid/support/v7/media/ad;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ad;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Landroid/support/v7/media/ad;)Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/media/ad;->f:Z

    return v0
.end method

.method static synthetic c(Landroid/support/v7/media/ad;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ad;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Landroid/support/v7/media/ad;)Landroid/support/v7/media/c;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ad;->q:Landroid/support/v7/media/c;

    return-object v0
.end method


# virtual methods
.method final a(Landroid/support/v7/media/c;)I
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Landroid/support/v7/media/ad;->q:Landroid/support/v7/media/c;

    if-eq v2, p1, :cond_a

    iput-object p1, p0, Landroid/support/v7/media/ad;->q:Landroid/support/v7/media/c;

    if-eqz p1, :cond_a

    iget-object v2, p0, Landroid/support/v7/media/ad;->d:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/support/v7/media/c;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/support/v7/media/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/media/c;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/ad;->d:Ljava/lang/String;

    move v0, v1

    :cond_0
    iget-object v2, p0, Landroid/support/v7/media/ad;->e:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/support/v7/media/c;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/support/v7/media/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Landroid/support/v7/media/c;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/ad;->e:Ljava/lang/String;

    move v0, v1

    :cond_1
    iget-boolean v2, p0, Landroid/support/v7/media/ad;->f:Z

    invoke-virtual {p1}, Landroid/support/v7/media/c;->d()Z

    move-result v3

    if-eq v2, v3, :cond_c

    invoke-virtual {p1}, Landroid/support/v7/media/c;->d()Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/media/ad;->f:Z

    :goto_0
    iget-boolean v0, p0, Landroid/support/v7/media/ad;->g:Z

    invoke-virtual {p1}, Landroid/support/v7/media/c;->e()Z

    move-result v2

    if-eq v0, v2, :cond_2

    invoke-virtual {p1}, Landroid/support/v7/media/c;->e()Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/media/ad;->g:Z

    or-int/lit8 v1, v1, 0x1

    :cond_2
    iget-object v0, p0, Landroid/support/v7/media/ad;->h:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/support/v7/media/c;->f()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/media/ad;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Landroid/support/v7/media/ad;->h:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/support/v7/media/c;->f()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    or-int/lit8 v1, v1, 0x1

    :cond_3
    iget v0, p0, Landroid/support/v7/media/ad;->i:I

    invoke-virtual {p1}, Landroid/support/v7/media/c;->g()I

    move-result v2

    if-eq v0, v2, :cond_4

    invoke-virtual {p1}, Landroid/support/v7/media/c;->g()I

    move-result v0

    iput v0, p0, Landroid/support/v7/media/ad;->i:I

    or-int/lit8 v1, v1, 0x1

    :cond_4
    iget v0, p0, Landroid/support/v7/media/ad;->j:I

    invoke-virtual {p1}, Landroid/support/v7/media/c;->h()I

    move-result v2

    if-eq v0, v2, :cond_5

    invoke-virtual {p1}, Landroid/support/v7/media/c;->h()I

    move-result v0

    iput v0, p0, Landroid/support/v7/media/ad;->j:I

    or-int/lit8 v1, v1, 0x1

    :cond_5
    iget v0, p0, Landroid/support/v7/media/ad;->k:I

    invoke-virtual {p1}, Landroid/support/v7/media/c;->k()I

    move-result v2

    if-eq v0, v2, :cond_6

    invoke-virtual {p1}, Landroid/support/v7/media/c;->k()I

    move-result v0

    iput v0, p0, Landroid/support/v7/media/ad;->k:I

    or-int/lit8 v1, v1, 0x3

    :cond_6
    iget v0, p0, Landroid/support/v7/media/ad;->l:I

    invoke-virtual {p1}, Landroid/support/v7/media/c;->i()I

    move-result v2

    if-eq v0, v2, :cond_7

    invoke-virtual {p1}, Landroid/support/v7/media/c;->i()I

    move-result v0

    iput v0, p0, Landroid/support/v7/media/ad;->l:I

    or-int/lit8 v1, v1, 0x3

    :cond_7
    iget v0, p0, Landroid/support/v7/media/ad;->m:I

    invoke-virtual {p1}, Landroid/support/v7/media/c;->j()I

    move-result v2

    if-eq v0, v2, :cond_8

    invoke-virtual {p1}, Landroid/support/v7/media/c;->j()I

    move-result v0

    iput v0, p0, Landroid/support/v7/media/ad;->m:I

    or-int/lit8 v1, v1, 0x3

    :cond_8
    iget v0, p0, Landroid/support/v7/media/ad;->o:I

    invoke-virtual {p1}, Landroid/support/v7/media/c;->l()I

    move-result v2

    if-eq v0, v2, :cond_9

    invoke-virtual {p1}, Landroid/support/v7/media/c;->l()I

    move-result v0

    iput v0, p0, Landroid/support/v7/media/ad;->o:I

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/media/ad;->n:Landroid/view/Display;

    or-int/lit8 v1, v1, 0x5

    :cond_9
    iget-object v0, p0, Landroid/support/v7/media/ad;->p:Landroid/os/Bundle;

    invoke-virtual {p1}, Landroid/support/v7/media/c;->m()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/support/v7/media/u;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    invoke-virtual {p1}, Landroid/support/v7/media/c;->m()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/media/ad;->p:Landroid/os/Bundle;

    or-int/lit8 v0, v1, 0x1

    :cond_a
    :goto_1
    return v0

    :cond_b
    move v0, v1

    goto :goto_1

    :cond_c
    move v1, v0

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ad;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)V
    .locals 3

    invoke-static {}, Landroid/support/v7/media/u;->d()V

    sget-object v0, Landroid/support/v7/media/u;->a:Landroid/support/v7/media/y;

    iget v1, p0, Landroid/support/v7/media/ad;->m:I

    const/4 v2, 0x0

    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, p0, v1}, Landroid/support/v7/media/y;->a(Landroid/support/v7/media/ad;I)V

    return-void
.end method

.method public final a(Landroid/support/v7/media/s;)Z
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Landroid/support/v7/media/u;->d()V

    iget-object v0, p0, Landroid/support/v7/media/ad;->h:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/support/v7/media/s;->a(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 4

    const/4 v1, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "category must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Landroid/support/v7/media/u;->d()V

    iget-object v0, p0, Landroid/support/v7/media/ad;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    iget-object v0, p0, Landroid/support/v7/media/ad;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    invoke-virtual {v0, p1}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ad;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(I)V
    .locals 1

    invoke-static {}, Landroid/support/v7/media/u;->d()V

    if-eqz p1, :cond_0

    sget-object v0, Landroid/support/v7/media/u;->a:Landroid/support/v7/media/y;

    invoke-virtual {v0, p0, p1}, Landroid/support/v7/media/y;->b(Landroid/support/v7/media/ad;I)V

    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/media/ad;->f:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/media/ad;->g:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    invoke-static {}, Landroid/support/v7/media/u;->d()V

    sget-object v0, Landroid/support/v7/media/u;->a:Landroid/support/v7/media/y;

    invoke-virtual {v0}, Landroid/support/v7/media/y;->d()Landroid/support/v7/media/ad;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    invoke-static {}, Landroid/support/v7/media/u;->d()V

    sget-object v0, Landroid/support/v7/media/u;->a:Landroid/support/v7/media/y;

    invoke-virtual {v0}, Landroid/support/v7/media/y;->c()Landroid/support/v7/media/ad;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ad;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Landroid/support/v7/media/ad;->i:I

    return v0
.end method

.method public final i()I
    .locals 1

    iget v0, p0, Landroid/support/v7/media/ad;->j:I

    return v0
.end method

.method public final j()I
    .locals 1

    iget v0, p0, Landroid/support/v7/media/ad;->k:I

    return v0
.end method

.method public final k()I
    .locals 1

    iget v0, p0, Landroid/support/v7/media/ad;->l:I

    return v0
.end method

.method public final l()I
    .locals 1

    iget v0, p0, Landroid/support/v7/media/ad;->m:I

    return v0
.end method

.method public final m()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ad;->p:Landroid/os/Bundle;

    return-object v0
.end method

.method public final n()V
    .locals 1

    invoke-static {}, Landroid/support/v7/media/u;->d()V

    sget-object v0, Landroid/support/v7/media/u;->a:Landroid/support/v7/media/y;

    invoke-virtual {v0, p0}, Landroid/support/v7/media/y;->a(Landroid/support/v7/media/ad;)V

    return-void
.end method

.method final o()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ad;->b:Ljava/lang/String;

    return-object v0
.end method

.method final p()Landroid/support/v7/media/f;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ad;->a:Landroid/support/v7/media/ac;

    invoke-virtual {v0}, Landroid/support/v7/media/ac;->a()Landroid/support/v7/media/f;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediaRouter.RouteInfo{ uniqueId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v7/media/ad;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/media/ad;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/media/ad;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/media/ad;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", connecting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/media/ad;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", playbackType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/media/ad;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", playbackStream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/media/ad;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volumeHandling="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/media/ad;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volume="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/media/ad;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volumeMax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/media/ad;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", presentationDisplayId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/media/ad;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", extras="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/media/ad;->p:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", providerPackageName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/media/ad;->a:Landroid/support/v7/media/ac;

    invoke-virtual {v1}, Landroid/support/v7/media/ac;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
