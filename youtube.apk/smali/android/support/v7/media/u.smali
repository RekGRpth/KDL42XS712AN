.class public final Landroid/support/v7/media/u;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:Landroid/support/v7/media/y;

.field private static final d:Z


# instance fields
.field final b:Landroid/content/Context;

.field final c:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "MediaRouter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Landroid/support/v7/media/u;->d:Z

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/u;->c:Ljava/util/ArrayList;

    iput-object p1, p0, Landroid/support/v7/media/u;->b:Landroid/content/Context;

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/support/v7/media/u;
    .locals 2

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Landroid/support/v7/media/u;->d()V

    sget-object v0, Landroid/support/v7/media/u;->a:Landroid/support/v7/media/y;

    if-nez v0, :cond_1

    new-instance v0, Landroid/support/v7/media/y;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/media/y;-><init>(Landroid/content/Context;)V

    sput-object v0, Landroid/support/v7/media/u;->a:Landroid/support/v7/media/y;

    invoke-virtual {v0}, Landroid/support/v7/media/y;->a()V

    :cond_1
    sget-object v0, Landroid/support/v7/media/u;->a:Landroid/support/v7/media/y;

    invoke-virtual {v0, p0}, Landroid/support/v7/media/y;->a(Landroid/content/Context;)Landroid/support/v7/media/u;

    move-result-object v0

    return-object v0
.end method

.method public static a()Ljava/util/List;
    .locals 1

    invoke-static {}, Landroid/support/v7/media/u;->d()V

    sget-object v0, Landroid/support/v7/media/u;->a:Landroid/support/v7/media/y;

    invoke-virtual {v0}, Landroid/support/v7/media/y;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/support/v7/media/f;)V
    .locals 3

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "providerInstance must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Landroid/support/v7/media/u;->d()V

    sget-boolean v0, Landroid/support/v7/media/u;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addProvider: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    sget-object v0, Landroid/support/v7/media/u;->a:Landroid/support/v7/media/y;

    invoke-virtual {v0, p0}, Landroid/support/v7/media/y;->a(Landroid/support/v7/media/f;)V

    return-void
.end method

.method public static a(Ljava/lang/Object;)V
    .locals 3

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "remoteControlClient must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Landroid/support/v7/media/u;->d()V

    sget-boolean v0, Landroid/support/v7/media/u;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addRemoteControlClient: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    sget-object v0, Landroid/support/v7/media/u;->a:Landroid/support/v7/media/y;

    invoke-virtual {v0, p0}, Landroid/support/v7/media/y;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public static a(Landroid/support/v7/media/s;I)Z
    .locals 2

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Landroid/support/v7/media/u;->d()V

    sget-object v0, Landroid/support/v7/media/u;->a:Landroid/support/v7/media/y;

    invoke-virtual {v0, p0, p1}, Landroid/support/v7/media/y;->a(Landroid/support/v7/media/s;I)Z

    move-result v0

    return v0
.end method

.method static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_0

    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/support/v7/media/v;)I
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/media/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/w;

    iget-object v0, v0, Landroid/support/v7/media/w;->b:Landroid/support/v7/media/v;

    if-ne v0, p1, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static b()Landroid/support/v7/media/ad;
    .locals 1

    invoke-static {}, Landroid/support/v7/media/u;->d()V

    sget-object v0, Landroid/support/v7/media/u;->a:Landroid/support/v7/media/y;

    invoke-virtual {v0}, Landroid/support/v7/media/y;->c()Landroid/support/v7/media/ad;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Object;)V
    .locals 3

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "remoteControlClient must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-boolean v0, Landroid/support/v7/media/u;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removeRemoteControlClient: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    sget-object v0, Landroid/support/v7/media/u;->a:Landroid/support/v7/media/y;

    invoke-virtual {v0, p0}, Landroid/support/v7/media/y;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public static c()Landroid/support/v7/media/ad;
    .locals 1

    invoke-static {}, Landroid/support/v7/media/u;->d()V

    sget-object v0, Landroid/support/v7/media/u;->a:Landroid/support/v7/media/y;

    invoke-virtual {v0}, Landroid/support/v7/media/y;->d()Landroid/support/v7/media/ad;

    move-result-object v0

    return-object v0
.end method

.method static d()V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The media router service must only be accessed on the application\'s main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method static synthetic e()Z
    .locals 1

    sget-boolean v0, Landroid/support/v7/media/u;->d:Z

    return v0
.end method


# virtual methods
.method public final a(Landroid/support/v7/media/s;Landroid/support/v7/media/v;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/s;Landroid/support/v7/media/v;I)V

    return-void
.end method

.method public final a(Landroid/support/v7/media/s;Landroid/support/v7/media/v;I)V
    .locals 4

    const/4 v2, 0x1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {}, Landroid/support/v7/media/u;->d()V

    sget-boolean v0, Landroid/support/v7/media/u;->d:Z

    if-eqz v0, :cond_2

    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "addCallback: selector="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", callback="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", flags="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-direct {p0, p2}, Landroid/support/v7/media/u;->b(Landroid/support/v7/media/v;)I

    move-result v0

    if-gez v0, :cond_5

    new-instance v0, Landroid/support/v7/media/w;

    invoke-direct {v0, p0, p2}, Landroid/support/v7/media/w;-><init>(Landroid/support/v7/media/u;Landroid/support/v7/media/v;)V

    iget-object v1, p0, Landroid/support/v7/media/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    const/4 v1, 0x0

    iget v3, v0, Landroid/support/v7/media/w;->d:I

    xor-int/lit8 v3, v3, -0x1

    and-int/2addr v3, p3

    if-eqz v3, :cond_3

    iget v1, v0, Landroid/support/v7/media/w;->d:I

    or-int/2addr v1, p3

    iput v1, v0, Landroid/support/v7/media/w;->d:I

    move v1, v2

    :cond_3
    iget-object v3, v0, Landroid/support/v7/media/w;->c:Landroid/support/v7/media/s;

    invoke-virtual {v3, p1}, Landroid/support/v7/media/s;->a(Landroid/support/v7/media/s;)Z

    move-result v3

    if-nez v3, :cond_6

    new-instance v1, Landroid/support/v7/media/t;

    iget-object v3, v0, Landroid/support/v7/media/w;->c:Landroid/support/v7/media/s;

    invoke-direct {v1, v3}, Landroid/support/v7/media/t;-><init>(Landroid/support/v7/media/s;)V

    invoke-virtual {v1, p1}, Landroid/support/v7/media/t;->a(Landroid/support/v7/media/s;)Landroid/support/v7/media/t;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/media/t;->a()Landroid/support/v7/media/s;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/media/w;->c:Landroid/support/v7/media/s;

    :goto_1
    if-eqz v2, :cond_4

    sget-object v0, Landroid/support/v7/media/u;->a:Landroid/support/v7/media/y;

    invoke-virtual {v0}, Landroid/support/v7/media/y;->e()V

    :cond_4
    return-void

    :cond_5
    iget-object v1, p0, Landroid/support/v7/media/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/w;

    goto :goto_0

    :cond_6
    move v2, v1

    goto :goto_1
.end method

.method public final a(Landroid/support/v7/media/v;)V
    .locals 3

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Landroid/support/v7/media/u;->d()V

    sget-boolean v0, Landroid/support/v7/media/u;->d:Z

    if-eqz v0, :cond_1

    const-string v0, "MediaRouter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removeCallback: callback="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-direct {p0, p1}, Landroid/support/v7/media/u;->b(Landroid/support/v7/media/v;)I

    move-result v0

    if-ltz v0, :cond_2

    iget-object v1, p0, Landroid/support/v7/media/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    sget-object v0, Landroid/support/v7/media/u;->a:Landroid/support/v7/media/y;

    invoke-virtual {v0}, Landroid/support/v7/media/y;->e()V

    :cond_2
    return-void
.end method
