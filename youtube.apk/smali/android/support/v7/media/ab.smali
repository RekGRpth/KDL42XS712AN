.class final Landroid/support/v7/media/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/media/bi;


# instance fields
.field final synthetic a:Landroid/support/v7/media/y;

.field private final b:Landroid/support/v7/media/bd;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/support/v7/media/y;Ljava/lang/Object;)V
    .locals 3

    iput-object p1, p0, Landroid/support/v7/media/ab;->a:Landroid/support/v7/media/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Landroid/support/v7/media/y;->a(Landroid/support/v7/media/y;)Landroid/content/Context;

    move-result-object v1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_0

    new-instance v0, Landroid/support/v7/media/be;

    invoke-direct {v0, v1, p2}, Landroid/support/v7/media/be;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    :goto_0
    iput-object v0, p0, Landroid/support/v7/media/ab;->b:Landroid/support/v7/media/bd;

    iget-object v0, p0, Landroid/support/v7/media/ab;->b:Landroid/support/v7/media/bd;

    invoke-virtual {v0, p0}, Landroid/support/v7/media/bd;->a(Landroid/support/v7/media/bi;)V

    invoke-virtual {p0}, Landroid/support/v7/media/ab;->c()V

    return-void

    :cond_0
    new-instance v0, Landroid/support/v7/media/bg;

    invoke-direct {v0, v1, p2}, Landroid/support/v7/media/bg;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/ab;->b:Landroid/support/v7/media/bd;

    invoke-virtual {v0}, Landroid/support/v7/media/bd;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/media/ab;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/ab;->a:Landroid/support/v7/media/y;

    invoke-static {v0}, Landroid/support/v7/media/y;->c(Landroid/support/v7/media/y;)Landroid/support/v7/media/ad;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/ab;->a:Landroid/support/v7/media/y;

    invoke-static {v0}, Landroid/support/v7/media/y;->c(Landroid/support/v7/media/y;)Landroid/support/v7/media/ad;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/media/ad;->a(I)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/media/ab;->c:Z

    iget-object v0, p0, Landroid/support/v7/media/ab;->b:Landroid/support/v7/media/bd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/media/bd;->a(Landroid/support/v7/media/bi;)V

    return-void
.end method

.method public final b(I)V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/media/ab;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/ab;->a:Landroid/support/v7/media/y;

    invoke-static {v0}, Landroid/support/v7/media/y;->c(Landroid/support/v7/media/y;)Landroid/support/v7/media/ad;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/ab;->a:Landroid/support/v7/media/y;

    invoke-static {v0}, Landroid/support/v7/media/y;->c(Landroid/support/v7/media/y;)Landroid/support/v7/media/ad;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/media/ad;->b(I)V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/ab;->b:Landroid/support/v7/media/bd;

    iget-object v1, p0, Landroid/support/v7/media/ab;->a:Landroid/support/v7/media/y;

    invoke-static {v1}, Landroid/support/v7/media/y;->b(Landroid/support/v7/media/y;)Landroid/support/v7/media/bh;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/media/bd;->a(Landroid/support/v7/media/bh;)V

    return-void
.end method
