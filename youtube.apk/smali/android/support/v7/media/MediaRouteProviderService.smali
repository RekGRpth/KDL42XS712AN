.class public abstract Landroid/support/v7/media/MediaRouteProviderService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Z


# instance fields
.field private final b:Ljava/util/ArrayList;

.field private final c:Landroid/support/v7/media/r;

.field private final d:Landroid/os/Messenger;

.field private final e:Landroid/support/v7/media/p;

.field private final f:Landroid/support/v7/media/q;

.field private g:Landroid/support/v7/media/f;

.field private h:Landroid/support/v7/media/e;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "MediaRouteProviderSrv"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Landroid/support/v7/media/MediaRouteProviderService;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->b:Ljava/util/ArrayList;

    new-instance v0, Landroid/support/v7/media/r;

    invoke-direct {v0, p0}, Landroid/support/v7/media/r;-><init>(Landroid/support/v7/media/MediaRouteProviderService;)V

    iput-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->c:Landroid/support/v7/media/r;

    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Landroid/support/v7/media/MediaRouteProviderService;->c:Landroid/support/v7/media/r;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->d:Landroid/os/Messenger;

    new-instance v0, Landroid/support/v7/media/p;

    invoke-direct {v0, p0, v2}, Landroid/support/v7/media/p;-><init>(Landroid/support/v7/media/MediaRouteProviderService;B)V

    iput-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->e:Landroid/support/v7/media/p;

    new-instance v0, Landroid/support/v7/media/q;

    invoke-direct {v0, p0, v2}, Landroid/support/v7/media/q;-><init>(Landroid/support/v7/media/MediaRouteProviderService;B)V

    iput-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->f:Landroid/support/v7/media/q;

    return-void
.end method

.method static synthetic a(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;)I
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/v7/media/MediaRouteProviderService;->c(Landroid/os/Messenger;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Landroid/support/v7/media/MediaRouteProviderService;)Landroid/support/v7/media/f;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->g:Landroid/support/v7/media/f;

    return-object v0
.end method

.method static synthetic a(Landroid/os/Messenger;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Landroid/support/v7/media/MediaRouteProviderService;->d(Landroid/os/Messenger;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/os/Messenger;I)V
    .locals 6

    const/4 v4, 0x0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    move-object v0, p0

    move v2, p1

    move v3, v1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V
    .locals 6

    const/4 v3, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/media/MediaRouteProviderService;Landroid/support/v7/media/k;)V
    .locals 9

    const/4 v5, 0x0

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/support/v7/media/k;->c()Landroid/os/Bundle;

    move-result-object v4

    :goto_0
    iget-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v7, v2

    :goto_1
    if-ge v7, v8, :cond_2

    iget-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/support/v7/media/o;

    iget-object v0, v6, Landroid/support/v7/media/o;->a:Landroid/os/Messenger;

    const/4 v1, 0x5

    move v3, v2

    invoke-static/range {v0 .. v5}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    sget-boolean v0, Landroid/support/v7/media/MediaRouteProviderService;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaRouteProviderSrv"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ": Sent descriptor change event, descriptor="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    :cond_1
    move-object v4, v5

    goto :goto_0

    :cond_2
    return-void
.end method

.method private a(Landroid/os/Messenger;II)Z
    .locals 6

    const/4 v5, 0x0

    const/4 v3, 0x1

    if-lez p3, :cond_3

    invoke-direct {p0, p1}, Landroid/support/v7/media/MediaRouteProviderService;->c(Landroid/os/Messenger;)I

    move-result v0

    if-gez v0, :cond_3

    new-instance v0, Landroid/support/v7/media/o;

    invoke-direct {v0, p0, p1, p3}, Landroid/support/v7/media/o;-><init>(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;I)V

    invoke-virtual {v0}, Landroid/support/v7/media/o;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/media/MediaRouteProviderService;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-boolean v1, Landroid/support/v7/media/MediaRouteProviderService;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Registered, version="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->g:Landroid/support/v7/media/f;

    invoke-virtual {v0}, Landroid/support/v7/media/f;->e()Landroid/support/v7/media/k;

    move-result-object v0

    const/4 v1, 0x2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/media/k;->c()Landroid/os/Bundle;

    move-result-object v4

    :goto_0
    move-object v0, p1

    move v2, p2

    invoke-static/range {v0 .. v5}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    :cond_1
    :goto_1
    return v3

    :cond_2
    move-object v4, v5

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method static synthetic a(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;I)Z
    .locals 4

    invoke-direct {p0, p1}, Landroid/support/v7/media/MediaRouteProviderService;->c(Landroid/os/Messenger;)I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/media/MediaRouteProviderService;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/o;

    sget-boolean v1, Landroid/support/v7/media/MediaRouteProviderService;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": Unregistered"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/media/o;->b()V

    invoke-static {p1, p2}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;II)Z
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/media/MediaRouteProviderService;->a(Landroid/os/Messenger;II)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;III)Z
    .locals 3

    invoke-direct {p0, p1}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;)Landroid/support/v7/media/o;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Landroid/support/v7/media/o;->b(I)Landroid/support/v7/media/j;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, p4}, Landroid/support/v7/media/j;->a(I)V

    sget-boolean v1, Landroid/support/v7/media/MediaRouteProviderService;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route volume changed, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", volume="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;IILandroid/content/Intent;)Z
    .locals 8

    invoke-direct {p0, p1}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;)Landroid/support/v7/media/o;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2, p3}, Landroid/support/v7/media/o;->b(I)Landroid/support/v7/media/j;

    move-result-object v7

    if-eqz v7, :cond_2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    new-instance v0, Landroid/support/v7/media/n;

    move-object v1, p0

    move v3, p3

    move-object v4, p4

    move-object v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Landroid/support/v7/media/n;-><init>(Landroid/support/v7/media/MediaRouteProviderService;Landroid/support/v7/media/o;ILandroid/content/Intent;Landroid/os/Messenger;I)V

    :cond_0
    invoke-virtual {v7, p4, v0}, Landroid/support/v7/media/j;->a(Landroid/content/Intent;Landroid/support/v7/media/x;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-boolean v0, Landroid/support/v7/media/MediaRouteProviderService;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "MediaRouteProviderSrv"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Route control request delivered, controllerId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", intent="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;IILjava/lang/String;)Z
    .locals 3

    invoke-direct {p0, p1}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;)Landroid/support/v7/media/o;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p4, p3}, Landroid/support/v7/media/o;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-boolean v1, Landroid/support/v7/media/MediaRouteProviderService;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route controller created, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", routeId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;ILandroid/support/v7/media/e;)Z
    .locals 4

    invoke-direct {p0, p1}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;)Landroid/support/v7/media/o;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Landroid/support/v7/media/o;->a(Landroid/support/v7/media/e;)Z

    move-result v1

    sget-boolean v2, Landroid/support/v7/media/MediaRouteProviderService;->a:Z

    if-eqz v2, :cond_0

    const-string v2, "MediaRouteProviderSrv"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ": Set discovery request, request="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", actuallyChanged="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compositeDiscoveryRequest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/media/MediaRouteProviderService;->h:Landroid/support/v7/media/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/os/Messenger;)Landroid/support/v7/media/o;
    .locals 2

    invoke-direct {p0, p1}, Landroid/support/v7/media/MediaRouteProviderService;->c(Landroid/os/Messenger;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/MediaRouteProviderService;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/o;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/os/Messenger;I)V
    .locals 6

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    const/4 v3, 0x0

    move-object v0, p0

    move v2, p1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method private static b(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V
    .locals 4

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iput p2, v0, Landroid/os/Message;->arg1:I

    iput p3, v0, Landroid/os/Message;->arg2:I

    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, p5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    :try_start_0
    invoke-virtual {p0, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not send message to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Landroid/support/v7/media/MediaRouteProviderService;->d(Landroid/os/Messenger;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;)V
    .locals 4

    invoke-direct {p0, p1}, Landroid/support/v7/media/MediaRouteProviderService;->c(Landroid/os/Messenger;)I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/media/MediaRouteProviderService;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/o;

    sget-boolean v1, Landroid/support/v7/media/MediaRouteProviderService;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": Binder died"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/media/o;->b()V

    :cond_1
    return-void
.end method

.method static synthetic b()Z
    .locals 1

    sget-boolean v0, Landroid/support/v7/media/MediaRouteProviderService;->a:Z

    return v0
.end method

.method static synthetic b(Landroid/support/v7/media/MediaRouteProviderService;)Z
    .locals 8

    const/4 v2, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v6, v5

    move v1, v5

    move-object v3, v2

    :goto_0
    if-ge v6, v7, :cond_2

    iget-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/o;

    iget-object v4, v0, Landroid/support/v7/media/o;->c:Landroid/support/v7/media/e;

    if-eqz v4, :cond_7

    invoke-virtual {v4}, Landroid/support/v7/media/e;->a()Landroid/support/v7/media/s;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/s;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Landroid/support/v7/media/e;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_0
    invoke-virtual {v4}, Landroid/support/v7/media/e;->b()Z

    move-result v0

    or-int/2addr v0, v1

    if-nez v3, :cond_1

    move-object v1, v2

    move-object v2, v4

    :goto_1
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move-object v3, v2

    move-object v2, v1

    move v1, v0

    goto :goto_0

    :cond_1
    if-nez v2, :cond_6

    new-instance v1, Landroid/support/v7/media/t;

    invoke-virtual {v3}, Landroid/support/v7/media/e;->a()Landroid/support/v7/media/s;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/media/t;-><init>(Landroid/support/v7/media/s;)V

    :goto_2
    invoke-virtual {v4}, Landroid/support/v7/media/e;->a()Landroid/support/v7/media/s;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/media/t;->a(Landroid/support/v7/media/s;)Landroid/support/v7/media/t;

    move-object v2, v3

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_3

    new-instance v3, Landroid/support/v7/media/e;

    invoke-virtual {v2}, Landroid/support/v7/media/t;->a()Landroid/support/v7/media/s;

    move-result-object v0

    invoke-direct {v3, v0, v1}, Landroid/support/v7/media/e;-><init>(Landroid/support/v7/media/s;Z)V

    :cond_3
    iget-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->h:Landroid/support/v7/media/e;

    if-eq v0, v3, :cond_5

    iget-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->h:Landroid/support/v7/media/e;

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->h:Landroid/support/v7/media/e;

    invoke-virtual {v0, v3}, Landroid/support/v7/media/e;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    iput-object v3, p0, Landroid/support/v7/media/MediaRouteProviderService;->h:Landroid/support/v7/media/e;

    iget-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->g:Landroid/support/v7/media/f;

    invoke-virtual {v0, v3}, Landroid/support/v7/media/f;->a(Landroid/support/v7/media/e;)V

    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_5
    move v0, v5

    goto :goto_3

    :cond_6
    move-object v1, v2

    goto :goto_2

    :cond_7
    move v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_1
.end method

.method static synthetic b(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;II)Z
    .locals 3

    invoke-direct {p0, p1}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;)Landroid/support/v7/media/o;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Landroid/support/v7/media/o;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-boolean v1, Landroid/support/v7/media/MediaRouteProviderService;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route controller released, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;III)Z
    .locals 3

    invoke-direct {p0, p1}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;)Landroid/support/v7/media/o;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Landroid/support/v7/media/o;->b(I)Landroid/support/v7/media/j;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, p4}, Landroid/support/v7/media/j;->b(I)V

    sget-boolean v1, Landroid/support/v7/media/MediaRouteProviderService;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route volume updated, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", delta="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/os/Messenger;)I
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/o;

    invoke-virtual {v0, p1}, Landroid/support/v7/media/o;->a(Landroid/os/Messenger;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic c(Landroid/support/v7/media/MediaRouteProviderService;)Landroid/support/v7/media/p;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->e:Landroid/support/v7/media/p;

    return-object v0
.end method

.method static synthetic c(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;II)Z
    .locals 3

    invoke-direct {p0, p1}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;)Landroid/support/v7/media/o;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Landroid/support/v7/media/o;->b(I)Landroid/support/v7/media/j;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/support/v7/media/j;->b()V

    sget-boolean v1, Landroid/support/v7/media/MediaRouteProviderService;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route selected, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Landroid/os/Messenger;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Client connection "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Landroid/support/v7/media/MediaRouteProviderService;Landroid/os/Messenger;II)Z
    .locals 3

    invoke-direct {p0, p1}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;)Landroid/support/v7/media/o;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Landroid/support/v7/media/o;->b(I)Landroid/support/v7/media/j;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/support/v7/media/j;->c()V

    sget-boolean v1, Landroid/support/v7/media/MediaRouteProviderService;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route unselected, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Landroid/support/v7/media/MediaRouteProviderService;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a()Landroid/support/v7/media/f;
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.media.MediaRouteProviderService"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->g:Landroid/support/v7/media/f;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/media/MediaRouteProviderService;->a()Landroid/support/v7/media/f;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/media/f;->c()Landroid/support/v7/media/i;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/media/i;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/media/MediaRouteProviderService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onCreateMediaRouteProvider() returned a provider whose package name does not match the package name of the service.  A media route provider service can only export its own media route providers.  Provider package name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".  Service package name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/media/MediaRouteProviderService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->g:Landroid/support/v7/media/f;

    iget-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->g:Landroid/support/v7/media/f;

    iget-object v1, p0, Landroid/support/v7/media/MediaRouteProviderService;->f:Landroid/support/v7/media/q;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/f;->a(Landroid/support/v7/media/g;)V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->g:Landroid/support/v7/media/f;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/media/MediaRouteProviderService;->d:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
