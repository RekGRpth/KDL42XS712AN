.class abstract Landroid/support/v7/media/bj;
.super Landroid/support/v7/media/f;
.source "SourceFile"


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 4

    new-instance v0, Landroid/support/v7/media/i;

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "android"

    const-class v3, Landroid/support/v7/media/bj;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Landroid/support/v7/media/i;-><init>(Landroid/content/ComponentName;)V

    invoke-direct {p0, p1, v0}, Landroid/support/v7/media/f;-><init>(Landroid/content/Context;Landroid/support/v7/media/i;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/support/v7/media/bt;)Landroid/support/v7/media/bj;
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    new-instance v0, Landroid/support/v7/media/bp;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/media/bp;-><init>(Landroid/content/Context;Landroid/support/v7/media/bt;)V

    :goto_0
    return-object v0

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    new-instance v0, Landroid/support/v7/media/bo;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/media/bo;-><init>(Landroid/content/Context;Landroid/support/v7/media/bt;)V

    goto :goto_0

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_2

    new-instance v0, Landroid/support/v7/media/bk;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/media/bk;-><init>(Landroid/content/Context;Landroid/support/v7/media/bt;)V

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/support/v7/media/bq;

    invoke-direct {v0, p0}, Landroid/support/v7/media/bq;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/support/v7/media/ad;)V
    .locals 0

    return-void
.end method

.method public b(Landroid/support/v7/media/ad;)V
    .locals 0

    return-void
.end method

.method public c(Landroid/support/v7/media/ad;)V
    .locals 0

    return-void
.end method

.method public d(Landroid/support/v7/media/ad;)V
    .locals 0

    return-void
.end method
