.class public abstract Landroid/support/v7/media/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/support/v7/media/i;

.field private final c:Landroid/support/v7/media/h;

.field private d:Landroid/support/v7/media/g;

.field private e:Landroid/support/v7/media/e;

.field private f:Z

.field private g:Landroid/support/v7/media/k;

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/media/f;-><init>(Landroid/content/Context;Landroid/support/v7/media/i;)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/support/v7/media/i;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/support/v7/media/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v7/media/h;-><init>(Landroid/support/v7/media/f;B)V

    iput-object v0, p0, Landroid/support/v7/media/f;->c:Landroid/support/v7/media/h;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Landroid/support/v7/media/f;->a:Landroid/content/Context;

    if-nez p2, :cond_1

    new-instance v0, Landroid/support/v7/media/i;

    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v0, v1}, Landroid/support/v7/media/i;-><init>(Landroid/content/ComponentName;)V

    iput-object v0, p0, Landroid/support/v7/media/f;->b:Landroid/support/v7/media/i;

    :goto_0
    return-void

    :cond_1
    iput-object p2, p0, Landroid/support/v7/media/f;->b:Landroid/support/v7/media/i;

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v7/media/f;)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/media/f;->h:Z

    iget-object v0, p0, Landroid/support/v7/media/f;->d:Landroid/support/v7/media/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/f;->d:Landroid/support/v7/media/g;

    iget-object v1, p0, Landroid/support/v7/media/f;->g:Landroid/support/v7/media/k;

    invoke-virtual {v0, p0, v1}, Landroid/support/v7/media/g;->a(Landroid/support/v7/media/f;Landroid/support/v7/media/k;)V

    :cond_0
    return-void
.end method

.method static synthetic b(Landroid/support/v7/media/f;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/media/f;->f:Z

    iget-object v0, p0, Landroid/support/v7/media/f;->e:Landroid/support/v7/media/e;

    invoke-virtual {p0, v0}, Landroid/support/v7/media/f;->b(Landroid/support/v7/media/e;)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/f;->a:Landroid/content/Context;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Landroid/support/v7/media/j;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/support/v7/media/e;)V
    .locals 2

    invoke-static {}, Landroid/support/v7/media/u;->d()V

    iget-object v0, p0, Landroid/support/v7/media/f;->e:Landroid/support/v7/media/e;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/f;->e:Landroid/support/v7/media/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/media/f;->e:Landroid/support/v7/media/e;

    invoke-virtual {v0, p1}, Landroid/support/v7/media/e;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Landroid/support/v7/media/f;->e:Landroid/support/v7/media/e;

    iget-boolean v0, p0, Landroid/support/v7/media/f;->f:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/media/f;->f:Z

    iget-object v0, p0, Landroid/support/v7/media/f;->c:Landroid/support/v7/media/h;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v7/media/h;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/media/g;)V
    .locals 0

    invoke-static {}, Landroid/support/v7/media/u;->d()V

    iput-object p1, p0, Landroid/support/v7/media/f;->d:Landroid/support/v7/media/g;

    return-void
.end method

.method public final a(Landroid/support/v7/media/k;)V
    .locals 2

    const/4 v1, 0x1

    invoke-static {}, Landroid/support/v7/media/u;->d()V

    iget-object v0, p0, Landroid/support/v7/media/f;->g:Landroid/support/v7/media/k;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Landroid/support/v7/media/f;->g:Landroid/support/v7/media/k;

    iget-boolean v0, p0, Landroid/support/v7/media/f;->h:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Landroid/support/v7/media/f;->h:Z

    iget-object v0, p0, Landroid/support/v7/media/f;->c:Landroid/support/v7/media/h;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/h;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public final b()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/f;->c:Landroid/support/v7/media/h;

    return-object v0
.end method

.method public b(Landroid/support/v7/media/e;)V
    .locals 0

    return-void
.end method

.method public final c()Landroid/support/v7/media/i;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/f;->b:Landroid/support/v7/media/i;

    return-object v0
.end method

.method public final d()Landroid/support/v7/media/e;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/f;->e:Landroid/support/v7/media/e;

    return-object v0
.end method

.method public final e()Landroid/support/v7/media/k;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/f;->g:Landroid/support/v7/media/k;

    return-object v0
.end method
