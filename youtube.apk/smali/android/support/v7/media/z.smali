.class final Landroid/support/v7/media/z;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v7/media/y;

.field private final b:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>(Landroid/support/v7/media/y;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v7/media/z;->a:Landroid/support/v7/media/y;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/z;->b:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v7/media/y;B)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v7/media/z;-><init>(Landroid/support/v7/media/y;)V

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)V
    .locals 1

    invoke-virtual {p0, p1, p2}, Landroid/support/v7/media/z;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 10

    const/4 v5, 0x0

    iget v6, p1, Landroid/os/Message;->what:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    packed-switch v6, :pswitch_data_0

    :goto_0
    :pswitch_0
    :try_start_0
    iget-object v1, p0, Landroid/support/v7/media/z;->a:Landroid/support/v7/media/y;

    invoke-static {v1}, Landroid/support/v7/media/y;->d(Landroid/support/v7/media/y;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_1
    add-int/lit8 v3, v1, -0x1

    if-ltz v3, :cond_1

    iget-object v1, p0, Landroid/support/v7/media/z;->a:Landroid/support/v7/media/y;

    invoke-static {v1}, Landroid/support/v7/media/y;->d(Landroid/support/v7/media/y;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/media/u;

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/z;->a:Landroid/support/v7/media/y;

    invoke-static {v1}, Landroid/support/v7/media/y;->d(Landroid/support/v7/media/y;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v3

    goto :goto_1

    :pswitch_1
    iget-object v1, p0, Landroid/support/v7/media/z;->a:Landroid/support/v7/media/y;

    invoke-static {v1}, Landroid/support/v7/media/y;->e(Landroid/support/v7/media/y;)Landroid/support/v7/media/bj;

    move-result-object v3

    move-object v1, v2

    check-cast v1, Landroid/support/v7/media/ad;

    invoke-virtual {v3, v1}, Landroid/support/v7/media/bj;->a(Landroid/support/v7/media/ad;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Landroid/support/v7/media/z;->a:Landroid/support/v7/media/y;

    invoke-static {v1}, Landroid/support/v7/media/y;->e(Landroid/support/v7/media/y;)Landroid/support/v7/media/bj;

    move-result-object v3

    move-object v1, v2

    check-cast v1, Landroid/support/v7/media/ad;

    invoke-virtual {v3, v1}, Landroid/support/v7/media/bj;->b(Landroid/support/v7/media/ad;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Landroid/support/v7/media/z;->a:Landroid/support/v7/media/y;

    invoke-static {v1}, Landroid/support/v7/media/y;->e(Landroid/support/v7/media/y;)Landroid/support/v7/media/bj;

    move-result-object v3

    move-object v1, v2

    check-cast v1, Landroid/support/v7/media/ad;

    invoke-virtual {v3, v1}, Landroid/support/v7/media/bj;->c(Landroid/support/v7/media/ad;)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Landroid/support/v7/media/z;->a:Landroid/support/v7/media/y;

    invoke-static {v1}, Landroid/support/v7/media/y;->e(Landroid/support/v7/media/y;)Landroid/support/v7/media/bj;

    move-result-object v3

    move-object v1, v2

    check-cast v1, Landroid/support/v7/media/ad;

    invoke-virtual {v3, v1}, Landroid/support/v7/media/bj;->d(Landroid/support/v7/media/ad;)V

    goto :goto_0

    :cond_0
    :try_start_1
    iget-object v4, p0, Landroid/support/v7/media/z;->b:Ljava/util/ArrayList;

    iget-object v1, v1, Landroid/support/v7/media/u;->c:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move v1, v3

    goto :goto_1

    :cond_1
    iget-object v1, p0, Landroid/support/v7/media/z;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v4, v5

    :goto_2
    if-ge v4, v7, :cond_5

    iget-object v1, p0, Landroid/support/v7/media/z;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/support/v7/media/w;

    move-object v3, v0

    iget-object v1, v3, Landroid/support/v7/media/w;->a:Landroid/support/v7/media/u;

    iget-object v8, v3, Landroid/support/v7/media/w;->b:Landroid/support/v7/media/v;

    const v1, 0xff00

    and-int/2addr v1, v6

    sparse-switch v1, :sswitch_data_0

    :cond_2
    :goto_3
    :pswitch_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    :sswitch_0
    move-object v0, v2

    check-cast v0, Landroid/support/v7/media/ad;

    move-object v1, v0

    iget v9, v3, Landroid/support/v7/media/w;->d:I

    and-int/lit8 v9, v9, 0x2

    if-nez v9, :cond_3

    iget-object v3, v3, Landroid/support/v7/media/w;->c:Landroid/support/v7/media/s;

    invoke-virtual {v1, v3}, Landroid/support/v7/media/ad;->a(Landroid/support/v7/media/s;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    const/4 v3, 0x1

    :goto_4
    if-eqz v3, :cond_2

    packed-switch v6, :pswitch_data_1

    goto :goto_3

    :pswitch_6
    invoke-virtual {v8, v1}, Landroid/support/v7/media/v;->a(Landroid/support/v7/media/ad;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v1

    iget-object v2, p0, Landroid/support/v7/media/z;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    throw v1

    :cond_4
    move v3, v5

    goto :goto_4

    :pswitch_7
    :try_start_2
    invoke-virtual {v8, v1}, Landroid/support/v7/media/v;->b(Landroid/support/v7/media/ad;)V

    goto :goto_3

    :pswitch_8
    invoke-virtual {v8}, Landroid/support/v7/media/v;->a()V

    goto :goto_3

    :pswitch_9
    invoke-virtual {v8, v1}, Landroid/support/v7/media/v;->e(Landroid/support/v7/media/ad;)V

    goto :goto_3

    :pswitch_a
    invoke-virtual {v8, v1}, Landroid/support/v7/media/v;->c(Landroid/support/v7/media/ad;)V

    goto :goto_3

    :pswitch_b
    invoke-virtual {v8, v1}, Landroid/support/v7/media/v;->d(Landroid/support/v7/media/ad;)V

    goto :goto_3

    :sswitch_1
    packed-switch v6, :pswitch_data_2

    goto :goto_3

    :pswitch_c
    invoke-virtual {v8}, Landroid/support/v7/media/v;->b()V

    goto :goto_3

    :pswitch_d
    invoke-virtual {v8}, Landroid/support/v7/media/v;->c()V

    goto :goto_3

    :pswitch_e
    invoke-virtual {v8}, Landroid/support/v7/media/v;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :cond_5
    iget-object v1, p0, Landroid/support/v7/media/z;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    return-void

    :pswitch_data_0
    .packed-switch 0x101
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x100 -> :sswitch_0
        0x200 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x101
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_5
        :pswitch_a
        :pswitch_b
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x201
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method
