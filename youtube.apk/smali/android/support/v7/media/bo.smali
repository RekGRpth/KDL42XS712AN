.class Landroid/support/v7/media/bo;
.super Landroid/support/v7/media/bk;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/media/ap;


# instance fields
.field private j:Landroid/support/v7/media/ao;

.field private k:Landroid/support/v7/media/ar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/media/bt;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/support/v7/media/bk;-><init>(Landroid/content/Context;Landroid/support/v7/media/bt;)V

    return-void
.end method


# virtual methods
.method protected a(Landroid/support/v7/media/bm;Landroid/support/v7/media/d;)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/support/v7/media/bk;->a(Landroid/support/v7/media/bm;Landroid/support/v7/media/d;)V

    iget-object v0, p1, Landroid/support/v7/media/bm;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/support/v7/media/d;->a(Z)Landroid/support/v7/media/d;

    :cond_0
    invoke-virtual {p0, p1}, Landroid/support/v7/media/bo;->a(Landroid/support/v7/media/bm;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/support/v7/media/d;->b(Z)Landroid/support/v7/media/d;

    :cond_1
    iget-object v0, p1, Landroid/support/v7/media/bm;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/support/v7/media/d;->f(I)Landroid/support/v7/media/d;

    :cond_2
    return-void
.end method

.method protected a(Landroid/support/v7/media/bm;)Z
    .locals 2

    iget-object v0, p0, Landroid/support/v7/media/bo;->k:Landroid/support/v7/media/ar;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/media/ar;

    invoke-direct {v0}, Landroid/support/v7/media/ar;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/bo;->k:Landroid/support/v7/media/ar;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/bo;->k:Landroid/support/v7/media/ar;

    iget-object v1, p1, Landroid/support/v7/media/bm;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/ar;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final f(Ljava/lang/Object;)V
    .locals 4

    invoke-virtual {p0, p1}, Landroid/support/v7/media/bo;->g(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/bo;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/bm;

    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/Display;->getDisplayId()I

    move-result v1

    :goto_0
    iget-object v2, v0, Landroid/support/v7/media/bm;->c:Landroid/support/v7/media/c;

    invoke-virtual {v2}, Landroid/support/v7/media/c;->l()I

    move-result v2

    if-eq v1, v2, :cond_0

    new-instance v2, Landroid/support/v7/media/d;

    iget-object v3, v0, Landroid/support/v7/media/bm;->c:Landroid/support/v7/media/c;

    invoke-direct {v2, v3}, Landroid/support/v7/media/d;-><init>(Landroid/support/v7/media/c;)V

    invoke-virtual {v2, v1}, Landroid/support/v7/media/d;->f(I)Landroid/support/v7/media/d;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/media/d;->a()Landroid/support/v7/media/c;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/media/bm;->c:Landroid/support/v7/media/c;

    invoke-virtual {p0}, Landroid/support/v7/media/bo;->f()V

    :cond_0
    return-void

    :cond_1
    const/4 v1, -0x1

    goto :goto_0
.end method

.method protected g()V
    .locals 3

    invoke-super {p0}, Landroid/support/v7/media/bk;->g()V

    iget-object v0, p0, Landroid/support/v7/media/bo;->j:Landroid/support/v7/media/ao;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v7/media/ao;

    invoke-virtual {p0}, Landroid/support/v7/media/bo;->a()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/media/bo;->b()Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/support/v7/media/ao;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Landroid/support/v7/media/bo;->j:Landroid/support/v7/media/ao;

    :cond_0
    iget-object v1, p0, Landroid/support/v7/media/bo;->j:Landroid/support/v7/media/ao;

    iget-boolean v0, p0, Landroid/support/v7/media/bo;->f:Z

    if-eqz v0, :cond_1

    iget v0, p0, Landroid/support/v7/media/bo;->e:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v7/media/ao;->a(I)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final h()Ljava/lang/Object;
    .locals 1

    new-instance v0, Landroid/support/v7/media/aq;

    invoke-direct {v0, p0}, Landroid/support/v7/media/aq;-><init>(Landroid/support/v7/media/ap;)V

    return-object v0
.end method
