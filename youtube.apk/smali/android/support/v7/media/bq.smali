.class final Landroid/support/v7/media/bq;
.super Landroid/support/v7/media/bj;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/ArrayList;


# instance fields
.field private final b:Landroid/media/AudioManager;

.field private final c:Landroid/support/v7/media/bs;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    const-string v1, "android.media.intent.category.LIVE_VIDEO"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Landroid/support/v7/media/bq;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0, p1}, Landroid/support/v7/media/bj;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/media/bq;->d:I

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Landroid/support/v7/media/bq;->b:Landroid/media/AudioManager;

    new-instance v0, Landroid/support/v7/media/bs;

    invoke-direct {v0, p0}, Landroid/support/v7/media/bs;-><init>(Landroid/support/v7/media/bq;)V

    iput-object v0, p0, Landroid/support/v7/media/bq;->c:Landroid/support/v7/media/bs;

    iget-object v0, p0, Landroid/support/v7/media/bq;->c:Landroid/support/v7/media/bs;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.media.VOLUME_CHANGED_ACTION"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Landroid/support/v7/media/bq;->f()V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/media/bq;)Landroid/media/AudioManager;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/bq;->b:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic b(Landroid/support/v7/media/bq;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v7/media/bq;->f()V

    return-void
.end method

.method static synthetic c(Landroid/support/v7/media/bq;)I
    .locals 1

    iget v0, p0, Landroid/support/v7/media/bq;->d:I

    return v0
.end method

.method private f()V
    .locals 6

    const/4 v5, 0x3

    invoke-virtual {p0}, Landroid/support/v7/media/bq;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/media/bq;->b:Landroid/media/AudioManager;

    invoke-virtual {v1, v5}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    iget-object v2, p0, Landroid/support/v7/media/bq;->b:Landroid/media/AudioManager;

    invoke-virtual {v2, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v2

    iput v2, p0, Landroid/support/v7/media/bq;->d:I

    new-instance v2, Landroid/support/v7/media/d;

    const-string v3, "DEFAULT_ROUTE"

    sget v4, Landroid/support/v7/b/e;->b:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Landroid/support/v7/media/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Landroid/support/v7/media/bq;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Landroid/support/v7/media/d;->a(Ljava/util/Collection;)Landroid/support/v7/media/d;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/support/v7/media/d;->b(I)Landroid/support/v7/media/d;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/support/v7/media/d;->a(I)Landroid/support/v7/media/d;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/support/v7/media/d;->e(I)Landroid/support/v7/media/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v7/media/d;->d(I)Landroid/support/v7/media/d;

    move-result-object v0

    iget v1, p0, Landroid/support/v7/media/bq;->d:I

    invoke-virtual {v0, v1}, Landroid/support/v7/media/d;->c(I)Landroid/support/v7/media/d;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/d;->a()Landroid/support/v7/media/c;

    move-result-object v0

    new-instance v1, Landroid/support/v7/media/l;

    invoke-direct {v1}, Landroid/support/v7/media/l;-><init>()V

    invoke-virtual {v1, v0}, Landroid/support/v7/media/l;->a(Landroid/support/v7/media/c;)Landroid/support/v7/media/l;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/l;->a()Landroid/support/v7/media/k;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/media/bq;->a(Landroid/support/v7/media/k;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/support/v7/media/j;
    .locals 1

    const-string v0, "DEFAULT_ROUTE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/support/v7/media/br;

    invoke-direct {v0, p0}, Landroid/support/v7/media/br;-><init>(Landroid/support/v7/media/bq;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
