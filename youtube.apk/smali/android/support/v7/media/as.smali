.class final Landroid/support/v7/media/as;
.super Landroid/support/v7/media/f;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# static fields
.field private static final a:Z


# instance fields
.field private final b:Landroid/content/ComponentName;

.field private final c:Landroid/support/v7/media/ax;

.field private final d:Ljava/util/ArrayList;

.field private e:Z

.field private f:Z

.field private g:Landroid/support/v7/media/at;

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "MediaRouteProviderProxy"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Landroid/support/v7/media/as;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/ComponentName;)V
    .locals 2

    new-instance v0, Landroid/support/v7/media/i;

    invoke-direct {v0, p2}, Landroid/support/v7/media/i;-><init>(Landroid/content/ComponentName;)V

    invoke-direct {p0, p1, v0}, Landroid/support/v7/media/f;-><init>(Landroid/content/Context;Landroid/support/v7/media/i;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/media/as;->d:Ljava/util/ArrayList;

    iput-object p2, p0, Landroid/support/v7/media/as;->b:Landroid/content/ComponentName;

    new-instance v0, Landroid/support/v7/media/ax;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v7/media/ax;-><init>(Landroid/support/v7/media/as;B)V

    iput-object v0, p0, Landroid/support/v7/media/as;->c:Landroid/support/v7/media/ax;

    return-void
.end method

.method static synthetic a(Landroid/support/v7/media/as;)Landroid/support/v7/media/ax;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/as;->c:Landroid/support/v7/media/ax;

    return-object v0
.end method

.method static synthetic a(Landroid/support/v7/media/as;Landroid/support/v7/media/at;)V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/media/as;->g:Landroid/support/v7/media/at;

    if-ne v0, p1, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/media/as;->h:Z

    iget-object v0, p0, Landroid/support/v7/media/as;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/as;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/aw;

    iget-object v3, p0, Landroid/support/v7/media/as;->g:Landroid/support/v7/media/at;

    invoke-virtual {v0, v3}, Landroid/support/v7/media/aw;->a(Landroid/support/v7/media/at;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/media/as;->d()Landroid/support/v7/media/e;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/media/as;->g:Landroid/support/v7/media/at;

    invoke-virtual {v1, v0}, Landroid/support/v7/media/at;->a(Landroid/support/v7/media/e;)V

    :cond_1
    return-void
.end method

.method static synthetic a(Landroid/support/v7/media/as;Landroid/support/v7/media/at;Landroid/support/v7/media/k;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/as;->g:Landroid/support/v7/media/at;

    if-ne v0, p1, :cond_1

    sget-boolean v0, Landroid/support/v7/media/as;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaRouteProviderProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Descriptor changed, descriptor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0, p2}, Landroid/support/v7/media/as;->a(Landroid/support/v7/media/k;)V

    :cond_1
    return-void
.end method

.method static synthetic a(Landroid/support/v7/media/as;Landroid/support/v7/media/at;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/as;->g:Landroid/support/v7/media/at;

    if-ne v0, p1, :cond_1

    sget-boolean v0, Landroid/support/v7/media/as;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaRouteProviderProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Service connection error - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Landroid/support/v7/media/as;->m()V

    :cond_1
    return-void
.end method

.method static synthetic a(Landroid/support/v7/media/as;Landroid/support/v7/media/aw;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/as;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Landroid/support/v7/media/aw;->d()V

    invoke-direct {p0}, Landroid/support/v7/media/as;->j()V

    return-void
.end method

.method static synthetic b(Landroid/support/v7/media/as;Landroid/support/v7/media/at;)V
    .locals 3

    iget-object v0, p0, Landroid/support/v7/media/as;->g:Landroid/support/v7/media/at;

    if-ne v0, p1, :cond_1

    sget-boolean v0, Landroid/support/v7/media/as;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaRouteProviderProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Service connection died"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Landroid/support/v7/media/as;->n()V

    :cond_1
    return-void
.end method

.method static synthetic i()Z
    .locals 1

    sget-boolean v0, Landroid/support/v7/media/as;->a:Z

    return v0
.end method

.method private j()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/media/as;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Landroid/support/v7/media/as;->l()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Landroid/support/v7/media/as;->m()V

    goto :goto_0
.end method

.method private k()Z
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Landroid/support/v7/media/as;->e:Z

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Landroid/support/v7/media/as;->d()Landroid/support/v7/media/e;

    move-result-object v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Landroid/support/v7/media/as;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()V
    .locals 4

    iget-boolean v0, p0, Landroid/support/v7/media/as;->f:Z

    if-nez v0, :cond_1

    sget-boolean v0, Landroid/support/v7/media/as;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaRouteProviderProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Binding"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.MediaRouteProviderService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v7/media/as;->b:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0}, Landroid/support/v7/media/as;->a()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v7/media/as;->f:Z

    iget-boolean v0, p0, Landroid/support/v7/media/as;->f:Z

    if-nez v0, :cond_1

    sget-boolean v0, Landroid/support/v7/media/as;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "MediaRouteProviderProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Bind failed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-boolean v1, Landroid/support/v7/media/as;->a:Z

    if-eqz v1, :cond_1

    const-string v1, "MediaRouteProviderProxy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": Bind failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private m()V
    .locals 3

    iget-boolean v0, p0, Landroid/support/v7/media/as;->f:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Landroid/support/v7/media/as;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaRouteProviderProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Unbinding"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/media/as;->f:Z

    invoke-direct {p0}, Landroid/support/v7/media/as;->n()V

    invoke-virtual {p0}, Landroid/support/v7/media/as;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_1
    return-void
.end method

.method private n()V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Landroid/support/v7/media/as;->g:Landroid/support/v7/media/at;

    if-eqz v1, :cond_1

    invoke-virtual {p0, v3}, Landroid/support/v7/media/as;->a(Landroid/support/v7/media/k;)V

    iput-boolean v0, p0, Landroid/support/v7/media/as;->h:Z

    iget-object v1, p0, Landroid/support/v7/media/as;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/as;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/aw;

    invoke-virtual {v0}, Landroid/support/v7/media/aw;->d()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/media/as;->g:Landroid/support/v7/media/at;

    invoke-virtual {v0}, Landroid/support/v7/media/at;->b()V

    iput-object v3, p0, Landroid/support/v7/media/as;->g:Landroid/support/v7/media/at;

    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/support/v7/media/j;
    .locals 4

    invoke-virtual {p0}, Landroid/support/v7/media/as;->e()Landroid/support/v7/media/k;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/media/k;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/media/c;

    invoke-virtual {v0}, Landroid/support/v7/media/c;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/support/v7/media/aw;

    invoke-direct {v0, p0, p1}, Landroid/support/v7/media/aw;-><init>(Landroid/support/v7/media/as;Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v7/media/as;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-boolean v1, p0, Landroid/support/v7/media/as;->h:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/media/as;->g:Landroid/support/v7/media/at;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/aw;->a(Landroid/support/v7/media/at;)V

    :cond_0
    invoke-direct {p0}, Landroid/support/v7/media/as;->j()V

    :goto_1
    return-object v0

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/as;->b:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/as;->b:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/support/v7/media/e;)V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/media/as;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/media/as;->g:Landroid/support/v7/media/at;

    invoke-virtual {v0, p1}, Landroid/support/v7/media/at;->a(Landroid/support/v7/media/e;)V

    :cond_0
    invoke-direct {p0}, Landroid/support/v7/media/as;->j()V

    return-void
.end method

.method public final f()V
    .locals 3

    iget-boolean v0, p0, Landroid/support/v7/media/as;->e:Z

    if-nez v0, :cond_1

    sget-boolean v0, Landroid/support/v7/media/as;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaRouteProviderProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Starting"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/media/as;->e:Z

    invoke-direct {p0}, Landroid/support/v7/media/as;->j()V

    :cond_1
    return-void
.end method

.method public final g()V
    .locals 3

    iget-boolean v0, p0, Landroid/support/v7/media/as;->e:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Landroid/support/v7/media/as;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaRouteProviderProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Stopping"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/media/as;->e:Z

    invoke-direct {p0}, Landroid/support/v7/media/as;->j()V

    :cond_1
    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/media/as;->g:Landroid/support/v7/media/at;

    if-nez v0, :cond_0

    invoke-direct {p0}, Landroid/support/v7/media/as;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Landroid/support/v7/media/as;->m()V

    invoke-direct {p0}, Landroid/support/v7/media/as;->l()V

    :cond_0
    return-void
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    sget-boolean v0, Landroid/support/v7/media/as;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaRouteProviderProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Connected"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v0, p0, Landroid/support/v7/media/as;->f:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Landroid/support/v7/media/as;->n()V

    if-eqz p2, :cond_2

    new-instance v0, Landroid/os/Messenger;

    invoke-direct {v0, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    :goto_0
    invoke-static {v0}, Landroid/support/v7/media/m;->a(Landroid/os/Messenger;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Landroid/support/v7/media/at;

    invoke-direct {v1, p0, v0}, Landroid/support/v7/media/at;-><init>(Landroid/support/v7/media/as;Landroid/os/Messenger;)V

    invoke-virtual {v1}, Landroid/support/v7/media/at;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iput-object v1, p0, Landroid/support/v7/media/as;->g:Landroid/support/v7/media/at;

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    sget-boolean v0, Landroid/support/v7/media/as;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "MediaRouteProviderProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Registration failed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    const-string v0, "MediaRouteProviderProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Service returned invalid messenger binder"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    sget-boolean v0, Landroid/support/v7/media/as;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaRouteProviderProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Service disconnected"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Landroid/support/v7/media/as;->n()V

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Service connection "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/v7/media/as;->b:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
