.class final Landroid/support/v7/app/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Landroid/support/v7/app/u;

.field private final b:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Landroid/support/v7/app/u;)V
    .locals 1

    iput-object p1, p0, Landroid/support/v7/app/v;->a:Landroid/support/v7/app/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/support/v7/app/w;

    invoke-direct {v0, p0}, Landroid/support/v7/app/w;-><init>(Landroid/support/v7/app/v;)V

    iput-object v0, p0, Landroid/support/v7/app/v;->b:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1

    if-eqz p3, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/v;->a:Landroid/support/v7/app/u;

    invoke-static {v0}, Landroid/support/v7/app/u;->d(Landroid/support/v7/app/u;)Landroid/support/v7/media/ad;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v7/media/ad;->a(I)V

    :cond_0
    return-void
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/app/v;->a:Landroid/support/v7/app/u;

    invoke-static {v0}, Landroid/support/v7/app/u;->a(Landroid/support/v7/app/u;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/v;->a:Landroid/support/v7/app/u;

    invoke-static {v0}, Landroid/support/v7/app/u;->c(Landroid/support/v7/app/u;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/app/v;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/v;->a:Landroid/support/v7/app/u;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/support/v7/app/u;->a(Landroid/support/v7/app/u;Z)Z

    goto :goto_0
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4

    iget-object v0, p0, Landroid/support/v7/app/v;->a:Landroid/support/v7/app/u;

    invoke-static {v0}, Landroid/support/v7/app/u;->c(Landroid/support/v7/app/u;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/app/v;->b:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/SeekBar;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
