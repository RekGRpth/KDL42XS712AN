.class public final Landroid/support/v7/app/u;
.super Landroid/app/Dialog;
.source "SourceFile"


# instance fields
.field private final a:Landroid/support/v7/media/u;

.field private final b:Landroid/support/v7/app/y;

.field private final c:Landroid/support/v7/media/ad;

.field private d:Z

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:Z

.field private i:Landroid/widget/LinearLayout;

.field private j:Landroid/widget/SeekBar;

.field private k:Z

.field private l:Landroid/view/View;

.field private m:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/app/u;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {p1, v2}, Landroid/support/v7/app/ab;->a(Landroid/content/Context;Z)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-boolean v2, p0, Landroid/support/v7/app/u;->h:Z

    invoke-virtual {p0}, Landroid/support/v7/app/u;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/media/u;->a(Landroid/content/Context;)Landroid/support/v7/media/u;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/u;->a:Landroid/support/v7/media/u;

    new-instance v0, Landroid/support/v7/app/y;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/app/y;-><init>(Landroid/support/v7/app/u;B)V

    iput-object v0, p0, Landroid/support/v7/app/u;->b:Landroid/support/v7/app/y;

    iget-object v0, p0, Landroid/support/v7/app/u;->a:Landroid/support/v7/media/u;

    invoke-static {}, Landroid/support/v7/media/u;->c()Landroid/support/v7/media/ad;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/u;->c:Landroid/support/v7/media/ad;

    return-void
.end method

.method private a()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Landroid/support/v7/app/u;->c:Landroid/support/v7/media/ad;

    invoke-virtual {v0}, Landroid/support/v7/media/ad;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/u;->c:Landroid/support/v7/media/ad;

    invoke-virtual {v0}, Landroid/support/v7/media/ad;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/app/u;->dismiss()V

    move v0, v2

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Landroid/support/v7/app/u;->c:Landroid/support/v7/media/ad;

    invoke-virtual {v0}, Landroid/support/v7/media/ad;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v7/app/u;->setTitle(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Landroid/support/v7/app/u;->b()V

    iget-object v0, p0, Landroid/support/v7/app/u;->c:Landroid/support/v7/media/ad;

    invoke-virtual {v0}, Landroid/support/v7/media/ad;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/app/u;->e:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v7/app/u;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Landroid/support/v7/b/b;->c:I

    invoke-static {v0, v3}, Landroid/support/v7/app/ab;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/u;->e:Landroid/graphics/drawable/Drawable;

    :cond_2
    iget-object v0, p0, Landroid/support/v7/app/u;->e:Landroid/graphics/drawable/Drawable;

    :goto_1
    iget-object v3, p0, Landroid/support/v7/app/u;->g:Landroid/graphics/drawable/Drawable;

    if-eq v0, v3, :cond_3

    iput-object v0, p0, Landroid/support/v7/app/u;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    invoke-virtual {p0}, Landroid/support/v7/app/u;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v0}, Landroid/view/Window;->setFeatureDrawable(ILandroid/graphics/drawable/Drawable;)V

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Landroid/support/v7/app/u;->f:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_5

    invoke-virtual {p0}, Landroid/support/v7/app/u;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Landroid/support/v7/b/b;->e:I

    invoke-static {v0, v3}, Landroid/support/v7/app/ab;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/u;->f:Landroid/graphics/drawable/Drawable;

    :cond_5
    iget-object v0, p0, Landroid/support/v7/app/u;->f:Landroid/graphics/drawable/Drawable;

    goto :goto_1
.end method

.method static synthetic a(Landroid/support/v7/app/u;)Z
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/app/u;->k:Z

    return v0
.end method

.method static synthetic a(Landroid/support/v7/app/u;Z)Z
    .locals 0

    iput-boolean p1, p0, Landroid/support/v7/app/u;->k:Z

    return p1
.end method

.method private b()V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Landroid/support/v7/app/u;->k:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Landroid/support/v7/app/u;->h:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Landroid/support/v7/app/u;->c:Landroid/support/v7/media/ad;

    invoke-virtual {v2}, Landroid/support/v7/media/ad;->j()I

    move-result v2

    if-ne v2, v0, :cond_1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/app/u;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Landroid/support/v7/app/u;->j:Landroid/widget/SeekBar;

    iget-object v1, p0, Landroid/support/v7/app/u;->c:Landroid/support/v7/media/ad;

    invoke-virtual {v1}, Landroid/support/v7/media/ad;->l()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v0, p0, Landroid/support/v7/app/u;->j:Landroid/widget/SeekBar;

    iget-object v1, p0, Landroid/support/v7/app/u;->c:Landroid/support/v7/media/ad;

    invoke-virtual {v1}, Landroid/support/v7/media/ad;->k()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Landroid/support/v7/app/u;->i:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic b(Landroid/support/v7/app/u;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v7/app/u;->b()V

    return-void
.end method

.method static synthetic c(Landroid/support/v7/app/u;)Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/u;->j:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic d(Landroid/support/v7/app/u;)Landroid/support/v7/media/ad;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/u;->c:Landroid/support/v7/media/ad;

    return-object v0
.end method

.method static synthetic e(Landroid/support/v7/app/u;)Landroid/support/v7/media/u;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/u;->a:Landroid/support/v7/media/u;

    return-object v0
.end method

.method static synthetic f(Landroid/support/v7/app/u;)Z
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/app/u;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    iget-object v0, p0, Landroid/support/v7/app/u;->a:Landroid/support/v7/media/u;

    sget-object v1, Landroid/support/v7/media/s;->a:Landroid/support/v7/media/s;

    iget-object v2, p0, Landroid/support/v7/app/u;->b:Landroid/support/v7/app/y;

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/s;Landroid/support/v7/media/v;I)V

    invoke-direct {p0}, Landroid/support/v7/app/u;->a()Z

    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/support/v7/app/u;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    sget v0, Landroid/support/v7/b/d;->b:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/u;->setContentView(I)V

    sget v0, Landroid/support/v7/b/c;->d:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/u;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Landroid/support/v7/app/u;->i:Landroid/widget/LinearLayout;

    sget v0, Landroid/support/v7/b/c;->e:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/u;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Landroid/support/v7/app/u;->j:Landroid/widget/SeekBar;

    iget-object v0, p0, Landroid/support/v7/app/u;->j:Landroid/widget/SeekBar;

    new-instance v1, Landroid/support/v7/app/v;

    invoke-direct {v1, p0}, Landroid/support/v7/app/v;-><init>(Landroid/support/v7/app/u;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    sget v0, Landroid/support/v7/b/c;->b:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/u;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Landroid/support/v7/app/u;->m:Landroid/widget/Button;

    iget-object v0, p0, Landroid/support/v7/app/u;->m:Landroid/widget/Button;

    new-instance v1, Landroid/support/v7/app/x;

    invoke-direct {v1, p0}, Landroid/support/v7/app/x;-><init>(Landroid/support/v7/app/u;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/app/u;->d:Z

    invoke-direct {p0}, Landroid/support/v7/app/u;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/app/u;->l:Landroid/view/View;

    sget v0, Landroid/support/v7/b/c;->a:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/u;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Landroid/support/v7/app/u;->l:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/app/u;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onDetachedFromWindow()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/app/u;->a:Landroid/support/v7/media/u;

    iget-object v1, p0, Landroid/support/v7/app/u;->b:Landroid/support/v7/app/y;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/v;)V

    invoke-super {p0}, Landroid/app/Dialog;->onDetachedFromWindow()V

    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4

    const/16 v3, 0x19

    const/4 v1, 0x1

    if-eq p1, v3, :cond_0

    const/16 v0, 0x18

    if-ne p1, v0, :cond_2

    :cond_0
    iget-object v2, p0, Landroid/support/v7/app/u;->c:Landroid/support/v7/media/ad;

    if-ne p1, v3, :cond_1

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/support/v7/media/ad;->b(I)V

    :goto_1
    return v1

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_1
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/16 v0, 0x19

    if-eq p1, v0, :cond_0

    const/16 v0, 0x18

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
