.class public Landroid/support/v7/app/MediaRouteChooserDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "SourceFile"


# instance fields
.field private final Y:Ljava/lang/String;

.field private Z:Landroid/support/v7/media/s;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    const-string v0, "selector"

    iput-object v0, p0, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->Y:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->a(Z)V

    return-void
.end method

.method private E()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->Z:Landroid/support/v7/media/s;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->h()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "selector"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/media/s;->a(Landroid/os/Bundle;)Landroid/support/v7/media/s;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->Z:Landroid/support/v7/media/s;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->Z:Landroid/support/v7/media/s;

    if-nez v0, :cond_1

    sget-object v0, Landroid/support/v7/media/s;->a:Landroid/support/v7/media/s;

    iput-object v0, p0, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->Z:Landroid/support/v7/media/s;

    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/media/s;)V
    .locals 3

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0}, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->E()V

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->Z:Landroid/support/v7/media/s;

    invoke-virtual {v0, p1}, Landroid/support/v7/media/s;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iput-object p1, p0, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->Z:Landroid/support/v7/media/s;

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->h()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :cond_1
    const-string v1, "selector"

    invoke-virtual {p1}, Landroid/support/v7/media/s;->d()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->g(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->b()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/q;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Landroid/support/v7/app/q;->a(Landroid/support/v7/media/s;)V

    :cond_2
    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Landroid/support/v7/app/q;

    invoke-direct {v1, v0}, Landroid/support/v7/app/q;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->E()V

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->Z:Landroid/support/v7/media/s;

    invoke-virtual {v1, v0}, Landroid/support/v7/app/q;->a(Landroid/support/v7/media/s;)V

    return-object v1
.end method
