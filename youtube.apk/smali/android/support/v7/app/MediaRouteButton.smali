.class public Landroid/support/v7/app/MediaRouteButton;
.super Landroid/view/View;
.source "SourceFile"


# static fields
.field private static final l:[I

.field private static final m:[I


# instance fields
.field private final a:Landroid/support/v7/media/u;

.field private final b:Landroid/support/v7/app/p;

.field private c:Landroid/support/v7/media/s;

.field private d:Landroid/support/v7/app/z;

.field private e:Z

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:I

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [I

    const v1, 0x10100a0    # android.R.attr.state_checked

    aput v1, v0, v2

    sput-object v0, Landroid/support/v7/app/MediaRouteButton;->l:[I

    new-array v0, v3, [I

    const v1, 0x101009f    # android.R.attr.state_checkable

    aput v1, v0, v2

    sput-object v0, Landroid/support/v7/app/MediaRouteButton;->m:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/app/MediaRouteButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Landroid/support/v7/b/b;->b:I

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/app/MediaRouteButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1, v2}, Landroid/support/v7/app/ab;->a(Landroid/content/Context;Z)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v0, Landroid/support/v7/media/s;->a:Landroid/support/v7/media/s;

    iput-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->c:Landroid/support/v7/media/s;

    invoke-static {}, Landroid/support/v7/app/z;->a()Landroid/support/v7/app/z;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->d:Landroid/support/v7/app/z;

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/media/u;->a(Landroid/content/Context;)Landroid/support/v7/media/u;

    move-result-object v3

    iput-object v3, p0, Landroid/support/v7/app/MediaRouteButton;->a:Landroid/support/v7/media/u;

    new-instance v3, Landroid/support/v7/app/p;

    invoke-direct {v3, p0, v2}, Landroid/support/v7/app/p;-><init>(Landroid/support/v7/app/MediaRouteButton;B)V

    iput-object v3, p0, Landroid/support/v7/app/MediaRouteButton;->b:Landroid/support/v7/app/p;

    sget-object v3, Landroid/support/v7/b/g;->r:[I

    invoke-virtual {v0, p2, v3, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Landroid/support/v7/app/MediaRouteButton;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iput-object v4, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_1

    invoke-virtual {v4, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0, v2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->refreshDrawableState()V

    invoke-virtual {v3, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/app/MediaRouteButton;->j:I

    invoke-virtual {v3, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Landroid/support/v7/app/MediaRouteButton;->k:I

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0, v1}, Landroid/support/v7/app/MediaRouteButton;->setClickable(Z)V

    invoke-virtual {p0, v1}, Landroid/support/v7/app/MediaRouteButton;->setLongClickable(Z)V

    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method private a()V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Landroid/support/v7/app/MediaRouteButton;->e:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->a:Landroid/support/v7/media/u;

    invoke-static {}, Landroid/support/v7/media/u;->c()Landroid/support/v7/media/ad;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/ad;->f()Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Landroid/support/v7/app/MediaRouteButton;->c:Landroid/support/v7/media/s;

    invoke-virtual {v0, v3}, Landroid/support/v7/media/ad;->a(Landroid/support/v7/media/s;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v1

    :goto_0
    if-eqz v3, :cond_5

    invoke-virtual {v0}, Landroid/support/v7/media/ad;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    iget-boolean v4, p0, Landroid/support/v7/app/MediaRouteButton;->g:Z

    if-eq v4, v3, :cond_0

    iput-boolean v3, p0, Landroid/support/v7/app/MediaRouteButton;->g:Z

    move v2, v1

    :cond_0
    iget-boolean v3, p0, Landroid/support/v7/app/MediaRouteButton;->i:Z

    if-eq v3, v0, :cond_1

    iput-boolean v0, p0, Landroid/support/v7/app/MediaRouteButton;->i:Z

    move v2, v1

    :cond_1
    if-eqz v2, :cond_2

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->refreshDrawableState()V

    :cond_2
    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->a:Landroid/support/v7/media/u;

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->c:Landroid/support/v7/media/s;

    invoke-static {v0, v1}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/s;I)Z

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v7/app/MediaRouteButton;->setEnabled(Z)V

    :cond_3
    return-void

    :cond_4
    move v3, v2

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method static synthetic a(Landroid/support/v7/app/MediaRouteButton;)V
    .locals 0

    invoke-direct {p0}, Landroid/support/v7/app/MediaRouteButton;->a()V

    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 2

    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getDrawableState()[I

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->invalidate()V

    :cond_0
    return-void
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/a/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Landroid/support/v4/a/a/a;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 3

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/app/MediaRouteButton;->e:Z

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->c:Landroid/support/v7/media/s;

    invoke-virtual {v0}, Landroid/support/v7/media/s;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->a:Landroid/support/v7/media/u;

    iget-object v1, p0, Landroid/support/v7/app/MediaRouteButton;->c:Landroid/support/v7/media/s;

    iget-object v2, p0, Landroid/support/v7/app/MediaRouteButton;->b:Landroid/support/v7/app/p;

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/s;Landroid/support/v7/media/v;)V

    :cond_0
    invoke-direct {p0}, Landroid/support/v7/app/MediaRouteButton;->a()V

    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    add-int/lit8 v0, p1, 0x1

    invoke-super {p0, v0}, Landroid/view/View;->onCreateDrawableState(I)[I

    move-result-object v0

    iget-boolean v1, p0, Landroid/support/v7/app/MediaRouteButton;->i:Z

    if-eqz v1, :cond_1

    sget-object v1, Landroid/support/v7/app/MediaRouteButton;->m:[I

    invoke-static {v0, v1}, Landroid/support/v7/app/MediaRouteButton;->mergeDrawableStates([I[I)[I

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-boolean v1, p0, Landroid/support/v7/app/MediaRouteButton;->g:Z

    if-eqz v1, :cond_0

    sget-object v1, Landroid/support/v7/app/MediaRouteButton;->l:[I

    invoke-static {v0, v1}, Landroid/support/v7/app/MediaRouteButton;->mergeDrawableStates([I[I)[I

    goto :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/app/MediaRouteButton;->e:Z

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->c:Landroid/support/v7/media/s;

    invoke-virtual {v0}, Landroid/support/v7/media/s;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->a:Landroid/support/v7/media/u;

    iget-object v1, p0, Landroid/support/v7/app/MediaRouteButton;->b:Landroid/support/v7/app/p;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/v;)V

    :cond_0
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    iget-object v5, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    sub-int/2addr v1, v0

    sub-int/2addr v1, v4

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    sub-int v1, v3, v2

    sub-int/2addr v1, v5

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v2

    iget-object v2, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    add-int v3, v0, v4

    add-int v4, v1, v5

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    iget v6, p0, Landroid/support/v7/app/MediaRouteButton;->j:I

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    :goto_0
    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v6, p0, Landroid/support/v7/app/MediaRouteButton;->k:I

    iget-object v7, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v7, :cond_0

    iget-object v1, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    :cond_0
    invoke-static {v6, v1}, Ljava/lang/Math;->max(II)I

    move-result v6

    sparse-switch v4, :sswitch_data_0

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    move v1, v0

    :goto_1
    sparse-switch v5, :sswitch_data_1

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getPaddingTop()I

    move-result v0

    add-int/2addr v0, v6

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    :goto_2
    invoke-virtual {p0, v1, v0}, Landroid/support/v7/app/MediaRouteButton;->setMeasuredDimension(II)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :sswitch_0
    move v1, v3

    goto :goto_1

    :sswitch_1
    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v1, v0

    goto :goto_1

    :sswitch_2
    move v0, v2

    goto :goto_2

    :sswitch_3
    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getPaddingTop()I

    move-result v0

    add-int/2addr v0, v6

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getPaddingBottom()I

    move-result v3

    add-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x40000000 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_3
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method public performClick()Z
    .locals 6

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/view/View;->performClick()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0, v2}, Landroid/support/v7/app/MediaRouteButton;->playSoundEffect(I)V

    :cond_0
    iget-boolean v0, p0, Landroid/support/v7/app/MediaRouteButton;->e:Z

    if-nez v0, :cond_2

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    if-eqz v4, :cond_b

    :cond_1
    move v0, v3

    :goto_1
    return v0

    :cond_2
    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getContext()Landroid/content/Context;

    move-result-object v0

    :goto_2
    instance-of v5, v0, Landroid/content/ContextWrapper;

    if-eqz v5, :cond_5

    instance-of v5, v0, Landroid/app/Activity;

    if-eqz v5, :cond_4

    check-cast v0, Landroid/app/Activity;

    :goto_3
    instance-of v5, v0, Landroid/support/v4/app/FragmentActivity;

    if-eqz v5, :cond_3

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->c()Landroid/support/v4/app/l;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The activity must be a subclass of FragmentActivity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_3

    :cond_6
    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->a:Landroid/support/v7/media/u;

    invoke-static {}, Landroid/support/v7/media/u;->c()Landroid/support/v7/media/ad;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/ad;->f()Z

    move-result v5

    if-nez v5, :cond_7

    iget-object v5, p0, Landroid/support/v7/app/MediaRouteButton;->c:Landroid/support/v7/media/s;

    invoke-virtual {v0, v5}, Landroid/support/v7/media/ad;->a(Landroid/support/v7/media/s;)Z

    move-result v0

    if-nez v0, :cond_9

    :cond_7
    const-string v0, "android.support.v7.mediarouter:MediaRouteChooserDialogFragment"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/l;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_8

    const-string v0, "MediaRouteButton"

    const-string v1, "showDialog(): Route chooser dialog already showing!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    goto :goto_0

    :cond_8
    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->d:Landroid/support/v7/app/z;

    invoke-static {}, Landroid/support/v7/app/z;->b()Landroid/support/v7/app/MediaRouteChooserDialogFragment;

    move-result-object v0

    iget-object v5, p0, Landroid/support/v7/app/MediaRouteButton;->c:Landroid/support/v7/media/s;

    invoke-virtual {v0, v5}, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->a(Landroid/support/v7/media/s;)V

    const-string v5, "android.support.v7.mediarouter:MediaRouteChooserDialogFragment"

    invoke-virtual {v0, v1, v5}, Landroid/support/v7/app/MediaRouteChooserDialogFragment;->a(Landroid/support/v4/app/l;Ljava/lang/String;)V

    :goto_4
    move v0, v3

    goto :goto_0

    :cond_9
    const-string v0, "android.support.v7.mediarouter:MediaRouteControllerDialogFragment"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/l;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_a

    const-string v0, "MediaRouteButton"

    const-string v1, "showDialog(): Route controller dialog already showing!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    goto :goto_0

    :cond_a
    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->d:Landroid/support/v7/app/z;

    invoke-static {}, Landroid/support/v7/app/z;->c()Landroid/support/v7/app/MediaRouteControllerDialogFragment;

    move-result-object v0

    const-string v5, "android.support.v7.mediarouter:MediaRouteControllerDialogFragment"

    invoke-virtual {v0, v1, v5}, Landroid/support/v7/app/MediaRouteControllerDialogFragment;->a(Landroid/support/v4/app/l;Ljava/lang/String;)V

    goto :goto_4

    :cond_b
    move v0, v2

    goto/16 :goto_1
.end method

.method public performLongClick()Z
    .locals 10

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/view/View;->performLongClick()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-boolean v2, p0, Landroid/support/v7/app/MediaRouteButton;->h:Z

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v3, 0x2

    new-array v3, v3, [I

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0, v3}, Landroid/support/v7/app/MediaRouteButton;->getLocationOnScreen([I)V

    invoke-virtual {p0, v4}, Landroid/support/v7/app/MediaRouteButton;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getHeight()I

    move-result v7

    aget v8, v3, v0

    div-int/lit8 v9, v7, 0x2

    add-int/2addr v8, v9

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v9, v9, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v5, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    if-ge v8, v4, :cond_3

    const v4, 0x800035

    aget v3, v3, v1

    sub-int v3, v9, v3

    div-int/lit8 v5, v6, 0x2

    sub-int/2addr v3, v5

    invoke-virtual {v2, v4, v3, v7}, Landroid/widget/Toast;->setGravity(III)V

    :goto_1
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0, v1}, Landroid/support/v7/app/MediaRouteButton;->performHapticFeedback(I)Z

    goto :goto_0

    :cond_3
    const/16 v3, 0x51

    invoke-virtual {v2, v3, v1, v7}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_1
.end method

.method public setDialogFactory(Landroid/support/v7/app/z;)V
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "factory must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Landroid/support/v7/app/MediaRouteButton;->d:Landroid/support/v7/app/z;

    return-void
.end method

.method public setRouteSelector(Landroid/support/v7/media/s;)V
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->c:Landroid/support/v7/media/s;

    invoke-virtual {v0, p1}, Landroid/support/v7/media/s;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Landroid/support/v7/app/MediaRouteButton;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->c:Landroid/support/v7/media/s;

    invoke-virtual {v0}, Landroid/support/v7/media/s;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->a:Landroid/support/v7/media/u;

    iget-object v1, p0, Landroid/support/v7/app/MediaRouteButton;->b:Landroid/support/v7/app/p;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/v;)V

    :cond_1
    invoke-virtual {p1}, Landroid/support/v7/media/s;->b()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->a:Landroid/support/v7/media/u;

    iget-object v1, p0, Landroid/support/v7/app/MediaRouteButton;->b:Landroid/support/v7/app/p;

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/s;Landroid/support/v7/media/v;)V

    :cond_2
    iput-object p1, p0, Landroid/support/v7/app/MediaRouteButton;->c:Landroid/support/v7/media/s;

    invoke-direct {p0}, Landroid/support/v7/app/MediaRouteButton;->a()V

    :cond_3
    return-void
.end method

.method public setVisibility(I)V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v2, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteButton;->f:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
