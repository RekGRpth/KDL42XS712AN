.class public Landroid/support/v7/app/MediaRouteDiscoveryFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Landroid/support/v7/media/u;

.field private c:Landroid/support/v7/media/s;

.field private d:Landroid/support/v7/media/v;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const-string v0, "selector"

    iput-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final d()V
    .locals 4

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->d()V

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->c:Landroid/support/v7/media/s;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->h()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "selector"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/media/s;->a(Landroid/os/Bundle;)Landroid/support/v7/media/s;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->c:Landroid/support/v7/media/s;

    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->c:Landroid/support/v7/media/s;

    if-nez v0, :cond_1

    sget-object v0, Landroid/support/v7/media/s;->a:Landroid/support/v7/media/s;

    iput-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->c:Landroid/support/v7/media/s;

    :cond_1
    iget-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->b:Landroid/support/v7/media/u;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/media/u;->a(Landroid/content/Context;)Landroid/support/v7/media/u;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->b:Landroid/support/v7/media/u;

    :cond_2
    new-instance v0, Landroid/support/v7/app/aa;

    invoke-direct {v0, p0}, Landroid/support/v7/app/aa;-><init>(Landroid/support/v7/app/MediaRouteDiscoveryFragment;)V

    iput-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->d:Landroid/support/v7/media/v;

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->d:Landroid/support/v7/media/v;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->b:Landroid/support/v7/media/u;

    iget-object v1, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->c:Landroid/support/v7/media/s;

    iget-object v2, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->d:Landroid/support/v7/media/v;

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/s;Landroid/support/v7/media/v;I)V

    :cond_3
    return-void
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->d:Landroid/support/v7/media/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->b:Landroid/support/v7/media/u;

    iget-object v1, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->d:Landroid/support/v7/media/v;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/v;)V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/app/MediaRouteDiscoveryFragment;->d:Landroid/support/v7/media/v;

    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->e()V

    return-void
.end method
