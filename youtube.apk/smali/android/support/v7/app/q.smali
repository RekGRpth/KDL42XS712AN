.class public final Landroid/support/v7/app/q;
.super Landroid/app/Dialog;
.source "SourceFile"


# instance fields
.field private final a:Landroid/support/v7/media/u;

.field private final b:Landroid/support/v7/app/r;

.field private c:Landroid/support/v7/media/s;

.field private d:Landroid/support/v7/app/s;

.field private e:Landroid/widget/ListView;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/app/q;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-static {p1, v0}, Landroid/support/v7/app/ab;->a(Landroid/content/Context;Z)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    sget-object v0, Landroid/support/v7/media/s;->a:Landroid/support/v7/media/s;

    iput-object v0, p0, Landroid/support/v7/app/q;->c:Landroid/support/v7/media/s;

    invoke-virtual {p0}, Landroid/support/v7/app/q;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/media/u;->a(Landroid/content/Context;)Landroid/support/v7/media/u;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/app/q;->a:Landroid/support/v7/media/u;

    new-instance v0, Landroid/support/v7/app/r;

    invoke-direct {v0, p0, v1}, Landroid/support/v7/app/r;-><init>(Landroid/support/v7/app/q;B)V

    iput-object v0, p0, Landroid/support/v7/app/q;->b:Landroid/support/v7/app/r;

    return-void
.end method

.method static synthetic a(Landroid/support/v7/app/q;)Landroid/support/v7/media/u;
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/q;->a:Landroid/support/v7/media/u;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/app/q;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/q;->d:Landroid/support/v7/app/s;

    invoke-virtual {v0}, Landroid/support/v7/app/s;->a()V

    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/media/s;)V
    .locals 3

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/q;->c:Landroid/support/v7/media/s;

    invoke-virtual {v0, p1}, Landroid/support/v7/media/s;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iput-object p1, p0, Landroid/support/v7/app/q;->c:Landroid/support/v7/media/s;

    iget-boolean v0, p0, Landroid/support/v7/app/q;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/app/q;->a:Landroid/support/v7/media/u;

    iget-object v1, p0, Landroid/support/v7/app/q;->b:Landroid/support/v7/app/r;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/v;)V

    iget-object v0, p0, Landroid/support/v7/app/q;->a:Landroid/support/v7/media/u;

    iget-object v1, p0, Landroid/support/v7/app/q;->b:Landroid/support/v7/app/r;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/s;Landroid/support/v7/media/v;I)V

    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/app/q;->a()V

    :cond_2
    return-void
.end method

.method public final a(Landroid/support/v7/media/ad;)Z
    .locals 1

    invoke-virtual {p1}, Landroid/support/v7/media/ad;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/media/ad;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/q;->c:Landroid/support/v7/media/s;

    invoke-virtual {p1, v0}, Landroid/support/v7/media/ad;->a(Landroid/support/v7/media/s;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    iput-boolean v3, p0, Landroid/support/v7/app/q;->f:Z

    iget-object v0, p0, Landroid/support/v7/app/q;->a:Landroid/support/v7/media/u;

    iget-object v1, p0, Landroid/support/v7/app/q;->c:Landroid/support/v7/media/s;

    iget-object v2, p0, Landroid/support/v7/app/q;->b:Landroid/support/v7/app/r;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/s;Landroid/support/v7/media/v;I)V

    invoke-virtual {p0}, Landroid/support/v7/app/q;->a()V

    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x3

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/support/v7/app/q;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->requestFeature(I)Z

    sget v0, Landroid/support/v7/b/d;->a:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/q;->setContentView(I)V

    sget v0, Landroid/support/v7/b/e;->a:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/q;->setTitle(I)V

    invoke-virtual {p0}, Landroid/support/v7/app/q;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v7/app/q;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Landroid/support/v7/b/b;->d:I

    invoke-static {v1, v2}, Landroid/support/v7/app/ab;->a(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/view/Window;->setFeatureDrawableResource(II)V

    new-instance v0, Landroid/support/v7/app/s;

    invoke-virtual {p0}, Landroid/support/v7/app/q;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/support/v7/app/s;-><init>(Landroid/support/v7/app/q;Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/app/q;->d:Landroid/support/v7/app/s;

    sget v0, Landroid/support/v7/b/c;->c:I

    invoke-virtual {p0, v0}, Landroid/support/v7/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Landroid/support/v7/app/q;->e:Landroid/widget/ListView;

    iget-object v0, p0, Landroid/support/v7/app/q;->e:Landroid/widget/ListView;

    iget-object v1, p0, Landroid/support/v7/app/q;->d:Landroid/support/v7/app/s;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Landroid/support/v7/app/q;->e:Landroid/widget/ListView;

    iget-object v1, p0, Landroid/support/v7/app/q;->d:Landroid/support/v7/app/s;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Landroid/support/v7/app/q;->e:Landroid/widget/ListView;

    const v1, 0x1020004    # android.R.id.empty

    invoke-virtual {p0, v1}, Landroid/support/v7/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/app/q;->f:Z

    iget-object v0, p0, Landroid/support/v7/app/q;->a:Landroid/support/v7/media/u;

    iget-object v1, p0, Landroid/support/v7/app/q;->b:Landroid/support/v7/app/r;

    invoke-virtual {v0, v1}, Landroid/support/v7/media/u;->a(Landroid/support/v7/media/v;)V

    invoke-super {p0}, Landroid/app/Dialog;->onDetachedFromWindow()V

    return-void
.end method
