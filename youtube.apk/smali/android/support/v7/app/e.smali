.class Landroid/support/v7/app/e;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/internal/view/menu/ae;
.implements Landroid/support/v7/internal/view/menu/p;


# static fields
.field private static final d:[I


# instance fields
.field private e:Landroid/support/v7/internal/widget/ActionBarView;

.field private f:Landroid/support/v7/internal/view/menu/m;

.field private g:Landroid/support/v7/internal/view/menu/o;

.field private h:Landroid/support/v7/c/a;

.field private i:Z

.field private j:Ljava/lang/CharSequence;

.field private k:Z

.field private l:Z

.field private m:Z

.field private final n:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Landroid/support/v7/a/c;->i:I

    aput v2, v0, v1

    sput-object v0, Landroid/support/v7/app/e;->d:[I

    return-void
.end method

.method constructor <init>(Landroid/support/v7/app/ActionBarActivity;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/v7/app/d;-><init>(Landroid/support/v7/app/ActionBarActivity;)V

    new-instance v0, Landroid/support/v7/app/f;

    invoke-direct {v0, p0}, Landroid/support/v7/app/f;-><init>(Landroid/support/v7/app/e;)V

    iput-object v0, p0, Landroid/support/v7/app/e;->n:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Landroid/support/v7/app/e;)Landroid/support/v7/internal/view/menu/o;
    .locals 1

    invoke-direct {p0}, Landroid/support/v7/app/e;->l()Landroid/support/v7/internal/view/menu/o;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/support/v7/app/e;Landroid/support/v7/internal/view/menu/o;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/v7/app/e;->c(Landroid/support/v7/internal/view/menu/o;)V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/app/e;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/app/e;->m:Z

    return v0
.end method

.method private c(Landroid/support/v7/internal/view/menu/o;)V
    .locals 2

    iget-object v0, p0, Landroid/support/v7/app/e;->g:Landroid/support/v7/internal/view/menu/o;

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/app/e;->g:Landroid/support/v7/internal/view/menu/o;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/app/e;->g:Landroid/support/v7/internal/view/menu/o;

    iget-object v1, p0, Landroid/support/v7/app/e;->f:Landroid/support/v7/internal/view/menu/m;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/view/menu/o;->b(Landroid/support/v7/internal/view/menu/ad;)V

    :cond_2
    iput-object p1, p0, Landroid/support/v7/app/e;->g:Landroid/support/v7/internal/view/menu/o;

    if-eqz p1, :cond_3

    iget-object v0, p0, Landroid/support/v7/app/e;->f:Landroid/support/v7/internal/view/menu/m;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/app/e;->f:Landroid/support/v7/internal/view/menu/m;

    invoke-virtual {p1, v0}, Landroid/support/v7/internal/view/menu/o;->a(Landroid/support/v7/internal/view/menu/ad;)V

    :cond_3
    iget-object v0, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1, p0}, Landroid/support/v7/internal/widget/ActionBarView;->setMenu(Landroid/support/v4/c/a/a;Landroid/support/v7/internal/view/menu/ae;)V

    goto :goto_0
.end method

.method private l()Landroid/support/v7/internal/view/menu/o;
    .locals 2

    new-instance v0, Landroid/support/v7/internal/view/menu/o;

    invoke-virtual {p0}, Landroid/support/v7/app/e;->j()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/internal/view/menu/o;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/o;->a(Landroid/support/v7/internal/view/menu/p;)V

    return-object v0
.end method


# virtual methods
.method public a()Landroid/support/v7/app/ActionBar;
    .locals 3

    invoke-virtual {p0}, Landroid/support/v7/app/e;->k()V

    new-instance v0, Landroid/support/v7/app/k;

    iget-object v1, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    iget-object v2, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    invoke-direct {v0, v1, v2}, Landroid/support/v7/app/k;-><init>(Landroid/support/v7/app/ActionBarActivity;Landroid/support/v7/app/a;)V

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/app/e;->k()V

    iget-boolean v0, p0, Landroid/support/v7/app/e;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    const v1, 0x1020002    # android.R.id.content

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v1, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v1}, Landroid/support/v7/app/ActionBarActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    :goto_0
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBarActivity;->a(I)V

    goto :goto_0
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    iget-boolean v0, p0, Landroid/support/v7/app/e;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/app/e;->i:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/k;

    invoke-virtual {v0}, Landroid/support/v7/app/k;->c()V

    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/internal/view/menu/o;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->b()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->d()Z

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/support/v7/internal/view/menu/o;->close()V

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/internal/view/menu/o;Z)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBarActivity;->closeOptionsMenu()V

    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/app/e;->k()V

    iget-boolean v0, p0, Landroid/support/v7/app/e;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    const v1, 0x1020002    # android.R.id.content

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :goto_0
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBarActivity;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/app/e;->k()V

    iget-boolean v0, p0, Landroid/support/v7/app/e;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    const v1, 0x1020002    # android.R.id.content

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/ActionBarActivity;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarView;->setWindowTitle(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Landroid/support/v7/app/e;->j:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final a(ILandroid/view/Menu;)Z
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/ActionBarActivity;->a(ILandroid/view/Menu;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILandroid/view/MenuItem;)Z
    .locals 1

    if-nez p1, :cond_0

    invoke-static {p2}, Landroid/support/v7/internal/view/menu/ah;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object p2

    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/ActionBarActivity;->a(ILandroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/Menu;)Z
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v7/app/ActionBarActivity;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 2

    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/app/ActionBarActivity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final b(I)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    if-nez p1, :cond_2

    const/4 v2, 0x1

    iget-object v1, p0, Landroid/support/v7/app/e;->g:Landroid/support/v7/internal/view/menu/o;

    iget-object v3, p0, Landroid/support/v7/app/e;->h:Landroid/support/v7/c/a;

    if-nez v3, :cond_1

    if-nez v1, :cond_0

    invoke-direct {p0}, Landroid/support/v7/app/e;->l()Landroid/support/v7/internal/view/menu/o;

    move-result-object v1

    invoke-direct {p0, v1}, Landroid/support/v7/app/e;->c(Landroid/support/v7/internal/view/menu/o;)V

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/o;->f()V

    iget-object v2, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v2, v4, v1}, Landroid/support/v7/app/ActionBarActivity;->a(ILandroid/view/Menu;)Z

    move-result v2

    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/o;->f()V

    iget-object v2, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v2, v4, v0, v1}, Landroid/support/v7/app/ActionBarActivity;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v2

    :cond_1
    if-eqz v2, :cond_5

    iget-object v2, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    iget-object v3, p0, Landroid/support/v7/app/e;->g:Landroid/support/v7/internal/view/menu/o;

    if-nez v3, :cond_3

    :goto_0
    check-cast v0, Landroid/view/View;

    invoke-virtual {v1}, Landroid/support/v7/internal/view/menu/o;->g()V

    :cond_2
    :goto_1
    return-object v0

    :cond_3
    iget-object v0, p0, Landroid/support/v7/app/e;->f:Landroid/support/v7/internal/view/menu/m;

    if-nez v0, :cond_4

    sget-object v0, Landroid/support/v7/a/k;->I:[I

    invoke-virtual {v2, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v3, 0x4

    sget v4, Landroid/support/v7/a/j;->a:I

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Landroid/support/v7/internal/view/menu/m;

    sget v4, Landroid/support/v7/a/h;->o:I

    invoke-direct {v0, v4, v3}, Landroid/support/v7/internal/view/menu/m;-><init>(II)V

    iput-object v0, p0, Landroid/support/v7/app/e;->f:Landroid/support/v7/internal/view/menu/m;

    iget-object v0, p0, Landroid/support/v7/app/e;->f:Landroid/support/v7/internal/view/menu/m;

    invoke-virtual {v0, p0}, Landroid/support/v7/internal/view/menu/m;->a(Landroid/support/v7/internal/view/menu/ae;)V

    iget-object v0, p0, Landroid/support/v7/app/e;->g:Landroid/support/v7/internal/view/menu/o;

    iget-object v3, p0, Landroid/support/v7/app/e;->f:Landroid/support/v7/internal/view/menu/m;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/view/menu/o;->a(Landroid/support/v7/internal/view/menu/ad;)V

    :goto_2
    iget-object v0, p0, Landroid/support/v7/app/e;->f:Landroid/support/v7/internal/view/menu/m;

    new-instance v3, Landroid/widget/FrameLayout;

    invoke-direct {v3, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/view/menu/m;->a(Landroid/view/ViewGroup;)Landroid/support/v7/internal/view/menu/af;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Landroid/support/v7/app/e;->f:Landroid/support/v7/internal/view/menu/m;

    invoke-virtual {v0, v4}, Landroid/support/v7/internal/view/menu/m;->c(Z)V

    goto :goto_2

    :cond_5
    invoke-direct {p0, v0}, Landroid/support/v7/app/e;->c(Landroid/support/v7/internal/view/menu/o;)V

    goto :goto_1
.end method

.method public final b(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/app/e;->k()V

    iget-boolean v0, p0, Landroid/support/v7/app/e;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    const v1, 0x1020002    # android.R.id.content

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    return-void

    :cond_0
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/app/ActionBarActivity;->a(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public final b(Landroid/support/v7/internal/view/menu/o;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final d()V
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/k;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/k;->c(Z)V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    invoke-virtual {p0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/k;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/k;->c(Z)V

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 2

    iget-boolean v0, p0, Landroid/support/v7/app/e;->m:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/app/e;->m:Z

    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBarActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/app/e;->n:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public final g()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Landroid/support/v7/app/e;->h:Landroid/support/v7/c/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/app/e;->h:Landroid/support/v7/c/a;

    invoke-virtual {v1}, Landroid/support/v7/c/a;->a()V

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarView;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarView;->k()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 0

    return-void
.end method

.method final k()V
    .locals 6

    const v5, 0x1020002    # android.R.id.content

    iget-boolean v0, p0, Landroid/support/v7/app/e;->b:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Landroid/support/v7/app/e;->i:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Landroid/support/v7/app/e;->c:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    sget v1, Landroid/support/v7/a/h;->b:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarActivity;->a(I)V

    :goto_0
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    sget v1, Landroid/support/v7/a/f;->a:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarView;

    iput-object v0, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    iget-object v0, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    iget-object v1, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarView;->setWindowCallback(Landroid/view/Window$Callback;)V

    iget-boolean v0, p0, Landroid/support/v7/app/e;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->g()V

    :cond_0
    iget-boolean v0, p0, Landroid/support/v7/app/e;->l:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarView;->h()V

    :cond_1
    const-string v0, "splitActionBarWhenNarrow"

    invoke-virtual {p0}, Landroid/support/v7/app/e;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBarActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v7/a/d;->e:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    move v2, v0

    :goto_1
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    sget v1, Landroid/support/v7/a/f;->C:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_2

    iget-object v1, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarView;->setSplitView(Landroid/support/v7/internal/widget/ActionBarContainer;)V

    iget-object v1, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarView;->setSplitActionBar(Z)V

    iget-object v1, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    invoke-virtual {v1, v3}, Landroid/support/v7/internal/widget/ActionBarView;->setSplitWhenNarrow(Z)V

    iget-object v1, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    sget v4, Landroid/support/v7/a/f;->h:I

    invoke-virtual {v1, v4}, Landroid/support/v7/app/ActionBarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->setSplitView(Landroid/support/v7/internal/widget/ActionBarContainer;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarContextView;->setSplitActionBar(Z)V

    invoke-virtual {v1, v3}, Landroid/support/v7/internal/widget/ActionBarContextView;->setSplitWhenNarrow(Z)V

    :cond_2
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    invoke-virtual {v0, v5}, Landroid/support/v7/app/ActionBarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    sget v1, Landroid/support/v7/a/f;->b:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setId(I)V

    iget-object v0, p0, Landroid/support/v7/app/e;->j:Ljava/lang/CharSequence;

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/app/e;->e:Landroid/support/v7/internal/widget/ActionBarView;

    iget-object v1, p0, Landroid/support/v7/app/e;->j:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarView;->setWindowTitle(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/app/e;->j:Ljava/lang/CharSequence;

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/app/e;->i:Z

    invoke-virtual {p0}, Landroid/support/v7/app/e;->f()V

    :cond_4
    return-void

    :cond_5
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    sget v1, Landroid/support/v7/a/h;->a:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarActivity;->a(I)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Landroid/support/v7/app/e;->a:Landroid/support/v7/app/ActionBarActivity;

    sget-object v1, Landroid/support/v7/a/k;->c:[I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarActivity;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    const/4 v0, 0x2

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    move v2, v0

    goto :goto_1
.end method
