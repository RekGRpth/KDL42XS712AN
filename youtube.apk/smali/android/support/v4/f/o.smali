.class public final Landroid/support/v4/f/o;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Object;

.field private static b:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/support/v4/f/o;->a:Ljava/lang/Object;

    const/16 v0, 0x18

    new-array v0, v0, [C

    sput-object v0, Landroid/support/v4/f/o;->b:[C

    return-void
.end method

.method private static a([CICIZI)I
    .locals 4

    if-nez p4, :cond_0

    if-lez p1, :cond_6

    :cond_0
    if-eqz p4, :cond_1

    const/4 v0, 0x3

    if-ge p5, v0, :cond_2

    :cond_1
    const/16 v0, 0x63

    if-le p1, v0, :cond_7

    :cond_2
    div-int/lit8 v1, p1, 0x64

    add-int/lit8 v0, v1, 0x30

    int-to-char v0, v0

    aput-char v0, p0, p3

    add-int/lit8 v0, p3, 0x1

    mul-int/lit8 v1, v1, 0x64

    sub-int v1, p1, v1

    :goto_0
    if-eqz p4, :cond_3

    const/4 v2, 0x2

    if-ge p5, v2, :cond_4

    :cond_3
    const/16 v2, 0x9

    if-gt v1, v2, :cond_4

    if-eq p3, v0, :cond_5

    :cond_4
    div-int/lit8 v2, v1, 0xa

    add-int/lit8 v3, v2, 0x30

    int-to-char v3, v3

    aput-char v3, p0, v0

    add-int/lit8 v0, v0, 0x1

    mul-int/lit8 v2, v2, 0xa

    sub-int/2addr v1, v2

    :cond_5
    add-int/lit8 v1, v1, 0x30

    int-to-char v1, v1

    aput-char v1, p0, v0

    add-int/lit8 v0, v0, 0x1

    aput-char p2, p0, v0

    add-int/lit8 p3, v0, 0x1

    :cond_6
    return p3

    :cond_7
    move v0, p3

    move v1, p1

    goto :goto_0
.end method

.method public static a(JLjava/lang/StringBuilder;)V
    .locals 13

    const v5, 0x15180

    const/4 v6, 0x1

    const/4 v7, 0x0

    sget-object v11, Landroid/support/v4/f/o;->a:Ljava/lang/Object;

    monitor-enter v11

    :try_start_0
    sget-object v0, Landroid/support/v4/f/o;->b:[C

    array-length v0, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [C

    sput-object v0, Landroid/support/v4/f/o;->b:[C

    :cond_0
    sget-object v0, Landroid/support/v4/f/o;->b:[C

    const-wide/16 v1, 0x0

    cmp-long v1, p0, v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    const/16 v2, 0x30

    aput-char v2, v0, v1

    move v0, v6

    :goto_0
    sget-object v1, Landroid/support/v4/f/o;->b:[C

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    monitor-exit v11

    return-void

    :cond_1
    const-wide/16 v1, 0x0

    cmp-long v1, p0, v1

    if-lez v1, :cond_2

    const/16 v1, 0x2b

    move v4, v1

    :goto_1
    const-wide/16 v1, 0x3e8

    rem-long v1, p0, v1

    long-to-int v12, v1

    const-wide/16 v1, 0x3e8

    div-long v1, p0, v1

    long-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-int v3, v1

    if-le v3, v5, :cond_8

    const v1, 0x15180

    div-int v1, v3, v1

    mul-int v2, v1, v5

    sub-int/2addr v3, v2

    :goto_2
    const/16 v2, 0xe10

    if-le v3, v2, :cond_7

    div-int/lit16 v2, v3, 0xe10

    mul-int/lit16 v5, v2, 0xe10

    sub-int/2addr v3, v5

    move v10, v2

    :goto_3
    const/16 v2, 0x3c

    if-le v3, v2, :cond_6

    div-int/lit8 v2, v3, 0x3c

    mul-int/lit8 v5, v2, 0x3c

    sub-int/2addr v3, v5

    move v8, v2

    move v9, v3

    :goto_4
    const/4 v2, 0x0

    aput-char v4, v0, v2

    const/16 v2, 0x64

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/support/v4/f/o;->a([CICIZI)I

    move-result v3

    const/16 v2, 0x68

    if-eq v3, v6, :cond_3

    move v4, v6

    :goto_5
    const/4 v5, 0x0

    move v1, v10

    invoke-static/range {v0 .. v5}, Landroid/support/v4/f/o;->a([CICIZI)I

    move-result v3

    const/16 v2, 0x6d

    if-eq v3, v6, :cond_4

    move v4, v6

    :goto_6
    const/4 v5, 0x0

    move v1, v8

    invoke-static/range {v0 .. v5}, Landroid/support/v4/f/o;->a([CICIZI)I

    move-result v3

    const/16 v2, 0x73

    if-eq v3, v6, :cond_5

    move v4, v6

    :goto_7
    const/4 v5, 0x0

    move v1, v9

    invoke-static/range {v0 .. v5}, Landroid/support/v4/f/o;->a([CICIZI)I

    move-result v3

    const/16 v2, 0x6d

    const/4 v4, 0x1

    const/4 v5, 0x0

    move v1, v12

    invoke-static/range {v0 .. v5}, Landroid/support/v4/f/o;->a([CICIZI)I

    move-result v1

    const/16 v2, 0x73

    aput-char v2, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v1, 0x1

    goto :goto_0

    :cond_2
    const/16 v1, 0x2d

    neg-long p0, p0

    move v4, v1

    goto :goto_1

    :cond_3
    move v4, v7

    goto :goto_5

    :cond_4
    move v4, v7

    goto :goto_6

    :cond_5
    move v4, v7

    goto :goto_7

    :catchall_0
    move-exception v0

    monitor-exit v11

    throw v0

    :cond_6
    move v8, v7

    move v9, v3

    goto :goto_4

    :cond_7
    move v10, v7

    goto :goto_3

    :cond_8
    move v1, v7

    goto :goto_2
.end method
