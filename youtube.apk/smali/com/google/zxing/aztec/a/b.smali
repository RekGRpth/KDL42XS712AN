.class final Lcom/google/zxing/aztec/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method constructor <init>(II)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/zxing/aztec/a/b;->a:I

    iput p2, p0, Lcom/google/zxing/aztec/a/b;->b:I

    return-void
.end method


# virtual methods
.method final a()Lcom/google/zxing/g;
    .locals 3

    new-instance v0, Lcom/google/zxing/g;

    iget v1, p0, Lcom/google/zxing/aztec/a/b;->a:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/zxing/aztec/a/b;->b:I

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/g;-><init>(FF)V

    return-object v0
.end method

.method final b()I
    .locals 1

    iget v0, p0, Lcom/google/zxing/aztec/a/b;->a:I

    return v0
.end method

.method final c()I
    .locals 1

    iget v0, p0, Lcom/google/zxing/aztec/a/b;->b:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/zxing/aztec/a/b;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/zxing/aztec/a/b;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
