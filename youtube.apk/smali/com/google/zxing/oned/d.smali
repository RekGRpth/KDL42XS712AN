.class public final Lcom/google/zxing/oned/d;
.super Lcom/google/zxing/oned/k;
.source "SourceFile"


# static fields
.field private static final a:[C

.field private static final b:[I

.field private static final c:I


# instance fields
.field private final d:Ljava/lang/StringBuilder;

.field private final e:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/zxing/oned/d;->a:[C

    const/16 v0, 0x30

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/zxing/oned/d;->b:[I

    const/16 v1, 0x2f

    aget v0, v0, v1

    sput v0, Lcom/google/zxing/oned/d;->c:I

    return-void

    :array_0
    .array-data 4
        0x114
        0x148
        0x144
        0x142
        0x128
        0x124
        0x122
        0x150
        0x112
        0x10a
        0x1a8
        0x1a4
        0x1a2
        0x194
        0x192
        0x18a
        0x168
        0x164
        0x162
        0x134
        0x11a
        0x158
        0x14c
        0x146
        0x12c
        0x116
        0x1b4
        0x1b2
        0x1ac
        0x1a6
        0x196
        0x19a
        0x16c
        0x166
        0x136
        0x13a
        0x12e
        0x1d4
        0x1d2
        0x1ca
        0x16e
        0x176
        0x1ae
        0x126
        0x1da
        0x1d6
        0x132
        0x15e
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/zxing/oned/k;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/google/zxing/oned/d;->d:Ljava/lang/StringBuilder;

    const/4 v0, 0x6

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/zxing/oned/d;->e:[I

    return-void
.end method

.method private static a([I)I
    .locals 8

    const/4 v2, 0x0

    array-length v7, p0

    array-length v3, p0

    move v0, v2

    move v6, v2

    :goto_0
    if-ge v0, v3, :cond_0

    aget v1, p0, v0

    add-int/2addr v1, v6

    add-int/lit8 v0, v0, 0x1

    move v6, v1

    goto :goto_0

    :cond_0
    move v5, v2

    move v0, v2

    :goto_1
    if-ge v5, v7, :cond_2

    aget v1, p0, v5

    shl-int/lit8 v1, v1, 0x8

    mul-int/lit8 v1, v1, 0x9

    div-int v3, v1, v6

    shr-int/lit8 v1, v3, 0x8

    and-int/lit16 v3, v3, 0xff

    const/16 v4, 0x7f

    if-le v3, v4, :cond_6

    add-int/lit8 v1, v1, 0x1

    move v4, v1

    :goto_2
    if-lez v4, :cond_1

    const/4 v1, 0x4

    if-le v4, v1, :cond_3

    :cond_1
    const/4 v0, -0x1

    :cond_2
    return v0

    :cond_3
    and-int/lit8 v1, v5, 0x1

    if-nez v1, :cond_4

    move v1, v2

    :goto_3
    if-ge v1, v4, :cond_5

    shl-int/lit8 v0, v0, 0x1

    or-int/lit8 v3, v0, 0x1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v3

    goto :goto_3

    :cond_4
    shl-int/2addr v0, v4

    :cond_5
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    :cond_6
    move v4, v1

    goto :goto_2
.end method

.method private static a(Ljava/lang/CharSequence;II)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    add-int/lit8 v0, p1, -0x1

    move v4, v1

    move v6, v2

    move v2, v0

    move v0, v6

    :goto_0
    if-ltz v2, :cond_1

    const-string v3, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*"

    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    mul-int/2addr v3, v4

    add-int/2addr v3, v0

    add-int/lit8 v0, v4, 0x1

    if-le v0, p2, :cond_0

    move v0, v1

    :cond_0
    add-int/lit8 v2, v2, -0x1

    move v4, v0

    move v0, v3

    goto :goto_0

    :cond_1
    invoke-interface {p0, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    sget-object v2, Lcom/google/zxing/oned/d;->a:[C

    rem-int/lit8 v0, v0, 0x2f

    aget-char v0, v2, v0

    if-eq v1, v0, :cond_2

    invoke-static {}, Lcom/google/zxing/ChecksumException;->getChecksumInstance()Lcom/google/zxing/ChecksumException;

    move-result-object v0

    throw v0

    :cond_2
    return-void
.end method


# virtual methods
.method public final a(ILcom/google/zxing/common/a;Ljava/util/Map;)Lcom/google/zxing/f;
    .locals 10

    invoke-virtual {p2}, Lcom/google/zxing/common/a;->a()I

    move-result v4

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/google/zxing/common/a;->c(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/zxing/oned/d;->e:[I

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([II)V

    iget-object v5, p0, Lcom/google/zxing/oned/d;->e:[I

    const/4 v2, 0x0

    array-length v6, v5

    const/4 v1, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v4, :cond_5

    invoke-virtual {p2, v3}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v7

    xor-int/2addr v7, v2

    if-eqz v7, :cond_0

    aget v7, v5, v1

    add-int/lit8 v7, v7, 0x1

    aput v7, v5, v1

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v7, v6, -0x1

    if-ne v1, v7, :cond_3

    invoke-static {v5}, Lcom/google/zxing/oned/d;->a([I)I

    move-result v7

    sget v8, Lcom/google/zxing/oned/d;->c:I

    if-ne v7, v8, :cond_2

    const/4 v1, 0x2

    new-array v4, v1, [I

    const/4 v1, 0x0

    aput v0, v4, v1

    const/4 v0, 0x1

    aput v3, v4, v0

    const/4 v0, 0x1

    aget v0, v4, v0

    invoke-virtual {p2, v0}, Lcom/google/zxing/common/a;->c(I)I

    move-result v0

    invoke-virtual {p2}, Lcom/google/zxing/common/a;->a()I

    move-result v3

    iget-object v5, p0, Lcom/google/zxing/oned/d;->e:[I

    const/4 v1, 0x0

    invoke-static {v5, v1}, Ljava/util/Arrays;->fill([II)V

    iget-object v6, p0, Lcom/google/zxing/oned/d;->d:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    :cond_1
    move v1, v0

    invoke-static {p2, v1, v5}, Lcom/google/zxing/oned/d;->a(Lcom/google/zxing/common/a;I[I)V

    invoke-static {v5}, Lcom/google/zxing/oned/d;->a([I)I

    move-result v2

    if-gez v2, :cond_6

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_2
    const/4 v7, 0x0

    aget v7, v5, v7

    const/4 v8, 0x1

    aget v8, v5, v8

    add-int/2addr v7, v8

    add-int/2addr v0, v7

    const/4 v7, 0x2

    const/4 v8, 0x0

    add-int/lit8 v9, v6, -0x2

    invoke-static {v5, v7, v5, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v7, v6, -0x2

    const/4 v8, 0x0

    aput v8, v5, v7

    add-int/lit8 v7, v6, -0x1

    const/4 v8, 0x0

    aput v8, v5, v7

    add-int/lit8 v1, v1, -0x1

    :goto_2
    const/4 v7, 0x1

    aput v7, v5, v1

    if-nez v2, :cond_4

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    :cond_5
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_6
    const/4 v0, 0x0

    :goto_3
    sget-object v7, Lcom/google/zxing/oned/d;->b:[I

    array-length v7, v7

    if-ge v0, v7, :cond_8

    sget-object v7, Lcom/google/zxing/oned/d;->b:[I

    aget v7, v7, v0

    if-ne v7, v2, :cond_7

    sget-object v2, Lcom/google/zxing/oned/d;->a:[C

    aget-char v7, v2, v0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    array-length v8, v5

    const/4 v0, 0x0

    move v2, v1

    :goto_4
    if-ge v0, v8, :cond_9

    aget v9, v5, v0

    add-int/2addr v2, v9

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_8
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_9
    invoke-virtual {p2, v2}, Lcom/google/zxing/common/a;->c(I)I

    move-result v0

    const/16 v2, 0x2a

    if-ne v7, v2, :cond_1

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    if-eq v0, v3, :cond_a

    invoke-virtual {p2, v0}, Lcom/google/zxing/common/a;->a(I)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_a
    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_c

    invoke-static {}, Lcom/google/zxing/NotFoundException;->getNotFoundInstance()Lcom/google/zxing/NotFoundException;

    move-result-object v0

    throw v0

    :cond_c
    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v2

    add-int/lit8 v3, v2, -0x2

    const/16 v5, 0x14

    invoke-static {v6, v3, v5}, Lcom/google/zxing/oned/d;->a(Ljava/lang/CharSequence;II)V

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0xf

    invoke-static {v6, v2, v3}, Lcom/google/zxing/oned/d;->a(Ljava/lang/CharSequence;II)V

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v3, 0x0

    :goto_5
    if-ge v3, v5, :cond_15

    invoke-interface {v6, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v8

    const/16 v2, 0x61

    if-lt v8, v2, :cond_14

    const/16 v2, 0x64

    if-gt v8, v2, :cond_14

    add-int/lit8 v2, v5, -0x1

    if-lt v3, v2, :cond_d

    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_d
    add-int/lit8 v2, v3, 0x1

    invoke-interface {v6, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    const/4 v2, 0x0

    packed-switch v8, :pswitch_data_0

    :goto_6
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v3, 0x1

    :goto_7
    add-int/lit8 v3, v2, 0x1

    goto :goto_5

    :pswitch_0
    const/16 v2, 0x41

    if-lt v9, v2, :cond_e

    const/16 v2, 0x5a

    if-gt v9, v2, :cond_e

    add-int/lit8 v2, v9, 0x20

    int-to-char v2, v2

    goto :goto_6

    :cond_e
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :pswitch_1
    const/16 v2, 0x41

    if-lt v9, v2, :cond_f

    const/16 v2, 0x5a

    if-gt v9, v2, :cond_f

    add-int/lit8 v2, v9, -0x40

    int-to-char v2, v2

    goto :goto_6

    :cond_f
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :pswitch_2
    const/16 v2, 0x41

    if-lt v9, v2, :cond_10

    const/16 v2, 0x45

    if-gt v9, v2, :cond_10

    add-int/lit8 v2, v9, -0x26

    int-to-char v2, v2

    goto :goto_6

    :cond_10
    const/16 v2, 0x46

    if-lt v9, v2, :cond_11

    const/16 v2, 0x57

    if-gt v9, v2, :cond_11

    add-int/lit8 v2, v9, -0xb

    int-to-char v2, v2

    goto :goto_6

    :cond_11
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :pswitch_3
    const/16 v2, 0x41

    if-lt v9, v2, :cond_12

    const/16 v2, 0x4f

    if-gt v9, v2, :cond_12

    add-int/lit8 v2, v9, -0x20

    int-to-char v2, v2

    goto :goto_6

    :cond_12
    const/16 v2, 0x5a

    if-ne v9, v2, :cond_13

    const/16 v2, 0x3a

    goto :goto_6

    :cond_13
    invoke-static {}, Lcom/google/zxing/FormatException;->getFormatInstance()Lcom/google/zxing/FormatException;

    move-result-object v0

    throw v0

    :cond_14
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v2, v3

    goto :goto_7

    :cond_15
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aget v3, v4, v3

    const/4 v5, 0x0

    aget v4, v4, v5

    add-int/2addr v3, v4

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-int/2addr v0, v1

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    new-instance v1, Lcom/google/zxing/f;

    const/4 v4, 0x0

    const/4 v5, 0x2

    new-array v5, v5, [Lcom/google/zxing/g;

    const/4 v6, 0x0

    new-instance v7, Lcom/google/zxing/g;

    int-to-float v8, p1

    invoke-direct {v7, v3, v8}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v7, v5, v6

    const/4 v3, 0x1

    new-instance v6, Lcom/google/zxing/g;

    int-to-float v7, p1

    invoke-direct {v6, v0, v7}, Lcom/google/zxing/g;-><init>(FF)V

    aput-object v6, v5, v3

    sget-object v0, Lcom/google/zxing/BarcodeFormat;->CODE_93:Lcom/google/zxing/BarcodeFormat;

    invoke-direct {v1, v2, v4, v5, v0}, Lcom/google/zxing/f;-><init>(Ljava/lang/String;[B[Lcom/google/zxing/g;Lcom/google/zxing/BarcodeFormat;)V

    return-object v1

    :pswitch_data_0
    .packed-switch 0x61
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
