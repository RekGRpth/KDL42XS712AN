.class public final Lcom/google/zxing/client/android/CaptureActivityHandler;
.super Landroid/os/Handler;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/zxing/client/android/CaptureActivity;

.field private final c:Lcom/google/zxing/client/android/b;

.field private d:Lcom/google/zxing/client/android/CaptureActivityHandler$State;

.field private final e:Lcom/google/zxing/client/android/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/zxing/client/android/CaptureActivityHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/zxing/client/android/CaptureActivityHandler;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/zxing/client/android/CaptureActivity;Ljava/lang/String;Lcom/google/zxing/client/android/a/c;)V
    .locals 3

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->b:Lcom/google/zxing/client/android/CaptureActivity;

    new-instance v0, Lcom/google/zxing/client/android/b;

    new-instance v1, Lcom/google/zxing/client/android/m;

    invoke-virtual {p1}, Lcom/google/zxing/client/android/CaptureActivity;->a()Lcom/google/zxing/client/android/ViewfinderView;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/zxing/client/android/m;-><init>(Lcom/google/zxing/client/android/ViewfinderView;)V

    invoke-direct {v0, p1, p2, v1}, Lcom/google/zxing/client/android/b;-><init>(Lcom/google/zxing/client/android/CaptureActivity;Ljava/lang/String;Lcom/google/zxing/h;)V

    iput-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->c:Lcom/google/zxing/client/android/b;

    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->c:Lcom/google/zxing/client/android/b;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/b;->start()V

    sget-object v0, Lcom/google/zxing/client/android/CaptureActivityHandler$State;->SUCCESS:Lcom/google/zxing/client/android/CaptureActivityHandler$State;

    iput-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->d:Lcom/google/zxing/client/android/CaptureActivityHandler$State;

    iput-object p3, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->e:Lcom/google/zxing/client/android/a/c;

    invoke-virtual {p3}, Lcom/google/zxing/client/android/a/c;->b()V

    invoke-direct {p0}, Lcom/google/zxing/client/android/CaptureActivityHandler;->b()V

    return-void
.end method

.method private b()V
    .locals 3

    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->d:Lcom/google/zxing/client/android/CaptureActivityHandler$State;

    sget-object v1, Lcom/google/zxing/client/android/CaptureActivityHandler$State;->SUCCESS:Lcom/google/zxing/client/android/CaptureActivityHandler$State;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/zxing/client/android/CaptureActivityHandler$State;->PREVIEW:Lcom/google/zxing/client/android/CaptureActivityHandler$State;

    iput-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->d:Lcom/google/zxing/client/android/CaptureActivityHandler$State;

    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->e:Lcom/google/zxing/client/android/a/c;

    iget-object v1, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->c:Lcom/google/zxing/client/android/b;

    invoke-virtual {v1}, Lcom/google/zxing/client/android/b;->a()Landroid/os/Handler;

    move-result-object v1

    sget v2, Lcom/google/zxing/client/android/j;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/google/zxing/client/android/a/c;->a(Landroid/os/Handler;I)V

    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->e:Lcom/google/zxing/client/android/a/c;

    sget v1, Lcom/google/zxing/client/android/j;->a:I

    invoke-virtual {v0, p0, v1}, Lcom/google/zxing/client/android/a/c;->b(Landroid/os/Handler;I)V

    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->b:Lcom/google/zxing/client/android/CaptureActivity;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/CaptureActivity;->d()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    sget-object v0, Lcom/google/zxing/client/android/CaptureActivityHandler$State;->DONE:Lcom/google/zxing/client/android/CaptureActivityHandler$State;

    iput-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->d:Lcom/google/zxing/client/android/CaptureActivityHandler$State;

    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->e:Lcom/google/zxing/client/android/a/c;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/c;->c()V

    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->c:Lcom/google/zxing/client/android/b;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/b;->a()Landroid/os/Handler;

    move-result-object v0

    sget v1, Lcom/google/zxing/client/android/j;->g:I

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :try_start_0
    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->c:Lcom/google/zxing/client/android/b;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Lcom/google/zxing/client/android/b;->join(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    sget v0, Lcom/google/zxing/client/android/j;->d:I

    invoke-virtual {p0, v0}, Lcom/google/zxing/client/android/CaptureActivityHandler;->removeMessages(I)V

    sget v0, Lcom/google/zxing/client/android/j;->c:I

    invoke-virtual {p0, v0}, Lcom/google/zxing/client/android/CaptureActivityHandler;->removeMessages(I)V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lcom/google/zxing/client/android/j;->a:I

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->d:Lcom/google/zxing/client/android/CaptureActivityHandler$State;

    sget-object v1, Lcom/google/zxing/client/android/CaptureActivityHandler$State;->PREVIEW:Lcom/google/zxing/client/android/CaptureActivityHandler$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->e:Lcom/google/zxing/client/android/a/c;

    sget v1, Lcom/google/zxing/client/android/j;->a:I

    invoke-virtual {v0, p0, v1}, Lcom/google/zxing/client/android/a/c;->b(Landroid/os/Handler;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lcom/google/zxing/client/android/j;->h:I

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/google/zxing/client/android/CaptureActivityHandler;->a:Ljava/lang/String;

    const-string v1, "Got restart preview message"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/zxing/client/android/CaptureActivityHandler;->b()V

    goto :goto_0

    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lcom/google/zxing/client/android/j;->d:I

    if-ne v0, v1, :cond_4

    sget-object v0, Lcom/google/zxing/client/android/CaptureActivityHandler;->a:Ljava/lang/String;

    const-string v1, "Got decode succeeded message"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/google/zxing/client/android/CaptureActivityHandler$State;->SUCCESS:Lcom/google/zxing/client/android/CaptureActivityHandler$State;

    iput-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->d:Lcom/google/zxing/client/android/CaptureActivityHandler$State;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    move-object v1, v0

    :goto_1
    iget-object v2, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->b:Lcom/google/zxing/client/android/CaptureActivity;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/zxing/f;

    invoke-virtual {v2, v0, v1}, Lcom/google/zxing/client/android/CaptureActivity;->a(Lcom/google/zxing/f;Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_3
    const-string v1, "barcode_bitmap"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    goto :goto_1

    :cond_4
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lcom/google/zxing/client/android/j;->c:I

    if-ne v0, v1, :cond_5

    sget-object v0, Lcom/google/zxing/client/android/CaptureActivityHandler$State;->PREVIEW:Lcom/google/zxing/client/android/CaptureActivityHandler$State;

    iput-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->d:Lcom/google/zxing/client/android/CaptureActivityHandler$State;

    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->e:Lcom/google/zxing/client/android/a/c;

    iget-object v1, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->c:Lcom/google/zxing/client/android/b;

    invoke-virtual {v1}, Lcom/google/zxing/client/android/b;->a()Landroid/os/Handler;

    move-result-object v1

    sget v2, Lcom/google/zxing/client/android/j;->b:I

    invoke-virtual {v0, v1, v2}, Lcom/google/zxing/client/android/a/c;->a(Landroid/os/Handler;I)V

    goto :goto_0

    :cond_5
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lcom/google/zxing/client/android/j;->i:I

    if-ne v0, v1, :cond_6

    sget-object v0, Lcom/google/zxing/client/android/CaptureActivityHandler;->a:Ljava/lang/String;

    const-string v1, "Got return scan result message"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->b:Lcom/google/zxing/client/android/CaptureActivity;

    const/4 v2, -0x1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v1, v2, v0}, Lcom/google/zxing/client/android/CaptureActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->b:Lcom/google/zxing/client/android/CaptureActivity;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/CaptureActivity;->finish()V

    goto :goto_0

    :cond_6
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lcom/google/zxing/client/android/j;->e:I

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/zxing/client/android/CaptureActivityHandler;->a:Ljava/lang/String;

    const-string v1, "Got product query message"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v0, 0x80000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/zxing/client/android/CaptureActivityHandler;->b:Lcom/google/zxing/client/android/CaptureActivity;

    invoke-virtual {v0, v1}, Lcom/google/zxing/client/android/CaptureActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
