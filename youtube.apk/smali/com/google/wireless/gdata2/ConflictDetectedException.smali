.class public Lcom/google/wireless/gdata2/ConflictDetectedException;
.super Lcom/google/wireless/gdata2/GDataException;
.source "SourceFile"


# instance fields
.field private final conflictingEntry:Lcom/google/wireless/gdata2/a/a;


# direct methods
.method public constructor <init>(Lcom/google/wireless/gdata2/a/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/wireless/gdata2/GDataException;-><init>()V

    iput-object p1, p0, Lcom/google/wireless/gdata2/ConflictDetectedException;->conflictingEntry:Lcom/google/wireless/gdata2/a/a;

    return-void
.end method


# virtual methods
.method public getConflictingEntry()Lcom/google/wireless/gdata2/a/a;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/ConflictDetectedException;->conflictingEntry:Lcom/google/wireless/gdata2/a/a;

    return-object v0
.end method
