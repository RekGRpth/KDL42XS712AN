.class public final Lcom/google/a/a/a/a/pv;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/pv;


# instance fields
.field public b:Z

.field public c:Z

.field public d:Lcom/google/a/a/a/a/eo;

.field public e:Z

.field public f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/pv;

    sput-object v0, Lcom/google/a/a/a/a/pv;->a:[Lcom/google/a/a/a/a/pv;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-boolean v1, p0, Lcom/google/a/a/a/a/pv;->b:Z

    iput-boolean v1, p0, Lcom/google/a/a/a/a/pv;->c:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/a/a/a/a/pv;->d:Lcom/google/a/a/a/a/eo;

    iput-boolean v1, p0, Lcom/google/a/a/a/a/pv;->e:Z

    iput-boolean v1, p0, Lcom/google/a/a/a/a/pv;->f:Z

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/a/a/a/a/pv;->b:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/a/a/a/a/pv;->b:Z

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lcom/google/a/a/a/a/pv;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/a/a/a/a/pv;->c:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/pv;->d:Lcom/google/a/a/a/a/eo;

    if-eqz v1, :cond_2

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/a/a/a/a/pv;->d:Lcom/google/a/a/a/a/eo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Lcom/google/a/a/a/a/pv;->e:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/a/a/a/a/pv;->e:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Lcom/google/a/a/a/a/pv;->f:Z

    if-eqz v1, :cond_4

    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/google/a/a/a/a/pv;->f:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/pv;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/pv;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/pv;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/pv;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/pv;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/pv;->b:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/pv;->c:Z

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/eo;

    invoke-direct {v0}, Lcom/google/a/a/a/a/eo;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/pv;->d:Lcom/google/a/a/a/a/eo;

    iget-object v0, p0, Lcom/google/a/a/a/a/pv;->d:Lcom/google/a/a/a/a/eo;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/pv;->e:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/pv;->f:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x18 -> :sswitch_2
        0x2a -> :sswitch_3
        0x38 -> :sswitch_4
        0x48 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/a/a/a/a/pv;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/a/a/a/a/pv;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/a/a/a/a/pv;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/a/a/a/a/pv;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/pv;->d:Lcom/google/a/a/a/a/eo;

    if-eqz v0, :cond_2

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/a/a/a/a/pv;->d:Lcom/google/a/a/a/a/eo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-boolean v0, p0, Lcom/google/a/a/a/a/pv;->e:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/a/a/a/a/pv;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_3
    iget-boolean v0, p0, Lcom/google/a/a/a/a/pv;->f:Z

    if-eqz v0, :cond_4

    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/a/a/a/a/pv;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/pv;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
