.class public final Lcom/google/a/a/a/a/wn;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/wn;


# instance fields
.field public b:[I

.field public c:Lcom/google/a/a/a/a/rz;

.field public d:Ljava/lang/String;

.field public e:Lcom/google/a/a/a/a/gz;

.field public f:[Lcom/google/a/a/a/a/ex;

.field public g:[Lcom/google/a/a/a/a/ta;

.field public h:[Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Lcom/google/a/a/a/a/l;

.field public k:Ljava/lang/String;

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/wn;

    sput-object v0, Lcom/google/a/a/a/a/wn;->a:[Lcom/google/a/a/a/a/wn;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    sget-object v0, Lcom/google/protobuf/nano/f;->e:[I

    iput-object v0, p0, Lcom/google/a/a/a/a/wn;->b:[I

    iput-object v2, p0, Lcom/google/a/a/a/a/wn;->c:Lcom/google/a/a/a/a/rz;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/wn;->d:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/a/a/a/a/wn;->e:Lcom/google/a/a/a/a/gz;

    sget-object v0, Lcom/google/a/a/a/a/ex;->a:[Lcom/google/a/a/a/a/ex;

    iput-object v0, p0, Lcom/google/a/a/a/a/wn;->f:[Lcom/google/a/a/a/a/ex;

    sget-object v0, Lcom/google/a/a/a/a/ta;->a:[Lcom/google/a/a/a/a/ta;

    iput-object v0, p0, Lcom/google/a/a/a/a/wn;->g:[Lcom/google/a/a/a/a/ta;

    sget-object v0, Lcom/google/protobuf/nano/f;->j:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/a/a/a/a/wn;->h:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/wn;->i:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/a/a/a/a/wn;->j:Lcom/google/a/a/a/a/l;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/wn;->k:Ljava/lang/String;

    iput v1, p0, Lcom/google/a/a/a/a/wn;->l:I

    iput v1, p0, Lcom/google/a/a/a/a/wn;->m:I

    iput v1, p0, Lcom/google/a/a/a/a/wn;->n:I

    iput v1, p0, Lcom/google/a/a/a/a/wn;->o:I

    iput v1, p0, Lcom/google/a/a/a/a/wn;->p:I

    iput v1, p0, Lcom/google/a/a/a/a/wn;->q:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/wn;->r:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/wn;->s:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/a/a/a/a/wn;->b:[I

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/a/a/a/a/wn;->b:[I

    array-length v0, v0

    if-lez v0, :cond_13

    iget-object v3, p0, Lcom/google/a/a/a/a/wn;->b:[I

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget v5, v3, v0

    invoke-static {v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I)I

    move-result v5

    add-int/2addr v2, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v2, 0x0

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->b:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :goto_1
    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->c:Lcom/google/a/a/a/a/rz;

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/a/a/a/a/wn;->c:Lcom/google/a/a/a/a/rz;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/a/a/a/a/wn;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->e:Lcom/google/a/a/a/a/gz;

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/a/a/a/a/wn;->e:Lcom/google/a/a/a/a/gz;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->f:[Lcom/google/a/a/a/a/ex;

    if-eqz v2, :cond_4

    iget-object v4, p0, Lcom/google/a/a/a/a/wn;->f:[Lcom/google/a/a/a/a/ex;

    array-length v5, v4

    move v2, v1

    :goto_2
    if-ge v2, v5, :cond_4

    aget-object v3, v4, v2

    const/4 v6, 0x5

    invoke-static {v6, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->g:[Lcom/google/a/a/a/a/ta;

    if-eqz v2, :cond_5

    iget-object v4, p0, Lcom/google/a/a/a/a/wn;->g:[Lcom/google/a/a/a/a/ta;

    array-length v5, v4

    move v2, v1

    :goto_3
    if-ge v2, v5, :cond_5

    aget-object v3, v4, v2

    const/4 v6, 0x6

    invoke-static {v6, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_3

    :cond_5
    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->h:[Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->h:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_7

    iget-object v3, p0, Lcom/google/a/a/a/a/wn;->h:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_4
    if-ge v1, v4, :cond_6

    aget-object v5, v3, v1

    invoke-static {v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_6
    add-int/2addr v0, v2

    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->h:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->j:Lcom/google/a/a/a/a/l;

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->j:Lcom/google/a/a/a/a/l;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->k:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->k:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget v1, p0, Lcom/google/a/a/a/a/wn;->l:I

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    iget v2, p0, Lcom/google/a/a/a/a/wn;->l:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget v1, p0, Lcom/google/a/a/a/a/wn;->m:I

    if-eqz v1, :cond_c

    const/16 v1, 0xd

    iget v2, p0, Lcom/google/a/a/a/a/wn;->m:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget v1, p0, Lcom/google/a/a/a/a/wn;->n:I

    if-eqz v1, :cond_d

    const/16 v1, 0xe

    iget v2, p0, Lcom/google/a/a/a/a/wn;->n:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget v1, p0, Lcom/google/a/a/a/a/wn;->o:I

    if-eqz v1, :cond_e

    const/16 v1, 0xf

    iget v2, p0, Lcom/google/a/a/a/a/wn;->o:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget v1, p0, Lcom/google/a/a/a/a/wn;->p:I

    if-eqz v1, :cond_f

    const/16 v1, 0x10

    iget v2, p0, Lcom/google/a/a/a/a/wn;->p:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iget v1, p0, Lcom/google/a/a/a/a/wn;->q:I

    if-eqz v1, :cond_10

    const/16 v1, 0x11

    iget v2, p0, Lcom/google/a/a/a/a/wn;->q:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->r:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->r:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->s:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    const/16 v1, 0x13

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->s:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/wn;->dm:I

    return v0

    :cond_13
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->dl:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/a/a/a/a/wn;->dl:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->dl:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/wn;->b:[I

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [I

    iget-object v3, p0, Lcom/google/a/a/a/a/wn;->b:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lcom/google/a/a/a/a/wn;->b:[I

    :goto_1
    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->b:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->b:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->b:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v3

    aput v3, v2, v0

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/rz;

    invoke-direct {v0}, Lcom/google/a/a/a/a/rz;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wn;->c:Lcom/google/a/a/a/a/rz;

    iget-object v0, p0, Lcom/google/a/a/a/a/wn;->c:Lcom/google/a/a/a/a/rz;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/wn;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/gz;

    invoke-direct {v0}, Lcom/google/a/a/a/a/gz;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wn;->e:Lcom/google/a/a/a/a/gz;

    iget-object v0, p0, Lcom/google/a/a/a/a/wn;->e:Lcom/google/a/a/a/a/gz;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/wn;->f:[Lcom/google/a/a/a/a/ex;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/ex;

    iget-object v3, p0, Lcom/google/a/a/a/a/wn;->f:[Lcom/google/a/a/a/a/ex;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/a/a/a/a/wn;->f:[Lcom/google/a/a/a/a/ex;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lcom/google/a/a/a/a/wn;->f:[Lcom/google/a/a/a/a/ex;

    :goto_3
    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->f:[Lcom/google/a/a/a/a/ex;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->f:[Lcom/google/a/a/a/a/ex;

    new-instance v3, Lcom/google/a/a/a/a/ex;

    invoke-direct {v3}, Lcom/google/a/a/a/a/ex;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->f:[Lcom/google/a/a/a/a/ex;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/wn;->f:[Lcom/google/a/a/a/a/ex;

    array-length v0, v0

    goto :goto_2

    :cond_5
    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->f:[Lcom/google/a/a/a/a/ex;

    new-instance v3, Lcom/google/a/a/a/a/ex;

    invoke-direct {v3}, Lcom/google/a/a/a/a/ex;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->f:[Lcom/google/a/a/a/a/ex;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/wn;->g:[Lcom/google/a/a/a/a/ta;

    if-nez v0, :cond_7

    move v0, v1

    :goto_4
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/ta;

    iget-object v3, p0, Lcom/google/a/a/a/a/wn;->g:[Lcom/google/a/a/a/a/ta;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/a/a/a/a/wn;->g:[Lcom/google/a/a/a/a/ta;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Lcom/google/a/a/a/a/wn;->g:[Lcom/google/a/a/a/a/ta;

    :goto_5
    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->g:[Lcom/google/a/a/a/a/ta;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->g:[Lcom/google/a/a/a/a/ta;

    new-instance v3, Lcom/google/a/a/a/a/ta;

    invoke-direct {v3}, Lcom/google/a/a/a/a/ta;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->g:[Lcom/google/a/a/a/a/ta;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_7
    iget-object v0, p0, Lcom/google/a/a/a/a/wn;->g:[Lcom/google/a/a/a/a/ta;

    array-length v0, v0

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->g:[Lcom/google/a/a/a/a/ta;

    new-instance v3, Lcom/google/a/a/a/a/ta;

    invoke-direct {v3}, Lcom/google/a/a/a/a/ta;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->g:[Lcom/google/a/a/a/a/ta;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/wn;->h:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lcom/google/a/a/a/a/wn;->h:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lcom/google/a/a/a/a/wn;->h:[Ljava/lang/String;

    :goto_6
    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->h:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->h:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->h:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/wn;->i:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    new-instance v0, Lcom/google/a/a/a/a/l;

    invoke-direct {v0}, Lcom/google/a/a/a/a/l;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/wn;->j:Lcom/google/a/a/a/a/l;

    iget-object v0, p0, Lcom/google/a/a/a/a/wn;->j:Lcom/google/a/a/a/a/l;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/wn;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    iput v0, p0, Lcom/google/a/a/a/a/wn;->l:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    iput v0, p0, Lcom/google/a/a/a/a/wn;->m:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    iput v0, p0, Lcom/google/a/a/a/a/wn;->n:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    iput v0, p0, Lcom/google/a/a/a/a/wn;->o:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    iput v0, p0, Lcom/google/a/a/a/a/wn;->p:I

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    iput v0, p0, Lcom/google/a/a/a/a/wn;->q:I

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/wn;->r:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/wn;->s:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x60 -> :sswitch_b
        0x68 -> :sswitch_c
        0x70 -> :sswitch_d
        0x78 -> :sswitch_e
        0x80 -> :sswitch_f
        0x88 -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->b:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->b:[I

    array-length v1, v1

    if-lez v1, :cond_0

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->b:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->c:Lcom/google/a/a/a/a/rz;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->c:Lcom/google/a/a/a/a/rz;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->e:Lcom/google/a/a/a/a/gz;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->e:Lcom/google/a/a/a/a/gz;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->f:[Lcom/google/a/a/a/a/ex;

    if-eqz v1, :cond_4

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->f:[Lcom/google/a/a/a/a/ex;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    const/4 v5, 0x5

    invoke-virtual {p1, v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->g:[Lcom/google/a/a/a/a/ta;

    if-eqz v1, :cond_5

    iget-object v2, p0, Lcom/google/a/a/a/a/wn;->g:[Lcom/google/a/a/a/a/ta;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->h:[Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->h:[Ljava/lang/String;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    const/16 v4, 0x8

    invoke-virtual {p1, v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/wn;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_7
    iget-object v0, p0, Lcom/google/a/a/a/a/wn;->j:Lcom/google/a/a/a/a/l;

    if-eqz v0, :cond_8

    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->j:Lcom/google/a/a/a/a/l;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_8
    iget-object v0, p0, Lcom/google/a/a/a/a/wn;->k:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_9
    iget v0, p0, Lcom/google/a/a/a/a/wn;->l:I

    if-eqz v0, :cond_a

    const/16 v0, 0xc

    iget v1, p0, Lcom/google/a/a/a/a/wn;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_a
    iget v0, p0, Lcom/google/a/a/a/a/wn;->m:I

    if-eqz v0, :cond_b

    const/16 v0, 0xd

    iget v1, p0, Lcom/google/a/a/a/a/wn;->m:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_b
    iget v0, p0, Lcom/google/a/a/a/a/wn;->n:I

    if-eqz v0, :cond_c

    const/16 v0, 0xe

    iget v1, p0, Lcom/google/a/a/a/a/wn;->n:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_c
    iget v0, p0, Lcom/google/a/a/a/a/wn;->o:I

    if-eqz v0, :cond_d

    const/16 v0, 0xf

    iget v1, p0, Lcom/google/a/a/a/a/wn;->o:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_d
    iget v0, p0, Lcom/google/a/a/a/a/wn;->p:I

    if-eqz v0, :cond_e

    const/16 v0, 0x10

    iget v1, p0, Lcom/google/a/a/a/a/wn;->p:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_e
    iget v0, p0, Lcom/google/a/a/a/a/wn;->q:I

    if-eqz v0, :cond_f

    const/16 v0, 0x11

    iget v1, p0, Lcom/google/a/a/a/a/wn;->q:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_f
    iget-object v0, p0, Lcom/google/a/a/a/a/wn;->r:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_10
    iget-object v0, p0, Lcom/google/a/a/a/a/wn;->s:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/a/a/a/a/wn;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_11
    iget-object v0, p0, Lcom/google/a/a/a/a/wn;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
