.class public final Lcom/google/a/a/a/a/mx;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/mx;


# instance fields
.field public b:Lcom/google/a/a/a/a/do;

.field public c:Lcom/google/a/a/a/a/m;

.field public d:Lcom/google/a/a/a/a/pc;

.field public e:Lcom/google/a/a/a/a/qa;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/mx;

    sput-object v0, Lcom/google/a/a/a/a/mx;->a:[Lcom/google/a/a/a/a/mx;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/mx;->b:Lcom/google/a/a/a/a/do;

    iput-object v0, p0, Lcom/google/a/a/a/a/mx;->c:Lcom/google/a/a/a/a/m;

    iput-object v0, p0, Lcom/google/a/a/a/a/mx;->d:Lcom/google/a/a/a/a/pc;

    iput-object v0, p0, Lcom/google/a/a/a/a/mx;->e:Lcom/google/a/a/a/a/qa;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/mx;->b:Lcom/google/a/a/a/a/do;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/mx;->b:Lcom/google/a/a/a/a/do;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/mx;->c:Lcom/google/a/a/a/a/m;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/a/a/a/a/mx;->c:Lcom/google/a/a/a/a/m;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/mx;->d:Lcom/google/a/a/a/a/pc;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/a/a/a/a/mx;->d:Lcom/google/a/a/a/a/pc;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/mx;->e:Lcom/google/a/a/a/a/qa;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/a/a/a/a/mx;->e:Lcom/google/a/a/a/a/qa;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/mx;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/mx;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/mx;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/mx;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/mx;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/do;

    invoke-direct {v0}, Lcom/google/a/a/a/a/do;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/mx;->b:Lcom/google/a/a/a/a/do;

    iget-object v0, p0, Lcom/google/a/a/a/a/mx;->b:Lcom/google/a/a/a/a/do;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/m;

    invoke-direct {v0}, Lcom/google/a/a/a/a/m;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/mx;->c:Lcom/google/a/a/a/a/m;

    iget-object v0, p0, Lcom/google/a/a/a/a/mx;->c:Lcom/google/a/a/a/a/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/pc;

    invoke-direct {v0}, Lcom/google/a/a/a/a/pc;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/mx;->d:Lcom/google/a/a/a/a/pc;

    iget-object v0, p0, Lcom/google/a/a/a/a/mx;->d:Lcom/google/a/a/a/a/pc;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/qa;

    invoke-direct {v0}, Lcom/google/a/a/a/a/qa;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/mx;->e:Lcom/google/a/a/a/a/qa;

    iget-object v0, p0, Lcom/google/a/a/a/a/mx;->e:Lcom/google/a/a/a/a/qa;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/mx;->b:Lcom/google/a/a/a/a/do;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/mx;->b:Lcom/google/a/a/a/a/do;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/mx;->c:Lcom/google/a/a/a/a/m;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/a/a/a/a/mx;->c:Lcom/google/a/a/a/a/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/mx;->d:Lcom/google/a/a/a/a/pc;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/a/a/a/a/mx;->d:Lcom/google/a/a/a/a/pc;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/mx;->e:Lcom/google/a/a/a/a/qa;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/a/a/a/a/mx;->e:Lcom/google/a/a/a/a/qa;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/mx;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
