.class public final Lcom/google/a/a/a/a/vo;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/vo;


# instance fields
.field public b:Lcom/google/a/a/a/a/vu;

.field public c:Lcom/google/a/a/a/a/vl;

.field public d:Lcom/google/a/a/a/a/vs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/vo;

    sput-object v0, Lcom/google/a/a/a/a/vo;->a:[Lcom/google/a/a/a/a/vo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/vo;->b:Lcom/google/a/a/a/a/vu;

    iput-object v0, p0, Lcom/google/a/a/a/a/vo;->c:Lcom/google/a/a/a/a/vl;

    iput-object v0, p0, Lcom/google/a/a/a/a/vo;->d:Lcom/google/a/a/a/a/vs;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/vo;->b:Lcom/google/a/a/a/a/vu;

    if-eqz v1, :cond_0

    const v0, 0x3c57395

    iget-object v1, p0, Lcom/google/a/a/a/a/vo;->b:Lcom/google/a/a/a/a/vu;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/vo;->c:Lcom/google/a/a/a/a/vl;

    if-eqz v1, :cond_1

    const v1, 0x3c5a4e1

    iget-object v2, p0, Lcom/google/a/a/a/a/vo;->c:Lcom/google/a/a/a/a/vl;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/vo;->d:Lcom/google/a/a/a/a/vs;

    if-eqz v1, :cond_2

    const v1, 0x3c67185

    iget-object v2, p0, Lcom/google/a/a/a/a/vo;->d:Lcom/google/a/a/a/a/vs;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/vo;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/vo;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/vo;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/vo;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/vo;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/vu;

    invoke-direct {v0}, Lcom/google/a/a/a/a/vu;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/vo;->b:Lcom/google/a/a/a/a/vu;

    iget-object v0, p0, Lcom/google/a/a/a/a/vo;->b:Lcom/google/a/a/a/a/vu;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/vl;

    invoke-direct {v0}, Lcom/google/a/a/a/a/vl;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/vo;->c:Lcom/google/a/a/a/a/vl;

    iget-object v0, p0, Lcom/google/a/a/a/a/vo;->c:Lcom/google/a/a/a/a/vl;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/vs;

    invoke-direct {v0}, Lcom/google/a/a/a/a/vs;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/vo;->d:Lcom/google/a/a/a/a/vs;

    iget-object v0, p0, Lcom/google/a/a/a/a/vo;->d:Lcom/google/a/a/a/a/vs;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1e2b9caa -> :sswitch_1
        0x1e2d270a -> :sswitch_2
        0x1e338c2a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/vo;->b:Lcom/google/a/a/a/a/vu;

    if-eqz v0, :cond_0

    const v0, 0x3c57395

    iget-object v1, p0, Lcom/google/a/a/a/a/vo;->b:Lcom/google/a/a/a/a/vu;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/vo;->c:Lcom/google/a/a/a/a/vl;

    if-eqz v0, :cond_1

    const v0, 0x3c5a4e1

    iget-object v1, p0, Lcom/google/a/a/a/a/vo;->c:Lcom/google/a/a/a/a/vl;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/vo;->d:Lcom/google/a/a/a/a/vs;

    if-eqz v0, :cond_2

    const v0, 0x3c67185

    iget-object v1, p0, Lcom/google/a/a/a/a/vo;->d:Lcom/google/a/a/a/a/vs;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/vo;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
