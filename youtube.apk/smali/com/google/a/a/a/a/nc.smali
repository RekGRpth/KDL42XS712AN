.class public final Lcom/google/a/a/a/a/nc;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/nc;


# instance fields
.field public b:Lcom/google/a/a/a/a/vh;

.field public c:Lcom/google/a/a/a/a/q;

.field public d:Lcom/google/a/a/a/a/nl;

.field public e:Lcom/google/a/a/a/a/nk;

.field public f:Lcom/google/a/a/a/a/ua;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/nc;

    sput-object v0, Lcom/google/a/a/a/a/nc;->a:[Lcom/google/a/a/a/a/nc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nc;->b:Lcom/google/a/a/a/a/vh;

    iput-object v0, p0, Lcom/google/a/a/a/a/nc;->c:Lcom/google/a/a/a/a/q;

    iput-object v0, p0, Lcom/google/a/a/a/a/nc;->d:Lcom/google/a/a/a/a/nl;

    iput-object v0, p0, Lcom/google/a/a/a/a/nc;->e:Lcom/google/a/a/a/a/nk;

    iput-object v0, p0, Lcom/google/a/a/a/a/nc;->f:Lcom/google/a/a/a/a/ua;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/nc;->b:Lcom/google/a/a/a/a/vh;

    if-eqz v1, :cond_0

    const v0, 0x2e9418f

    iget-object v1, p0, Lcom/google/a/a/a/a/nc;->b:Lcom/google/a/a/a/a/vh;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/nc;->c:Lcom/google/a/a/a/a/q;

    if-eqz v1, :cond_1

    const v1, 0x34e3c37

    iget-object v2, p0, Lcom/google/a/a/a/a/nc;->c:Lcom/google/a/a/a/a/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/nc;->d:Lcom/google/a/a/a/a/nl;

    if-eqz v1, :cond_2

    const v1, 0x3545bb1

    iget-object v2, p0, Lcom/google/a/a/a/a/nc;->d:Lcom/google/a/a/a/a/nl;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/nc;->e:Lcom/google/a/a/a/a/nk;

    if-eqz v1, :cond_3

    const v1, 0x3c3067d

    iget-object v2, p0, Lcom/google/a/a/a/a/nc;->e:Lcom/google/a/a/a/a/nk;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/nc;->f:Lcom/google/a/a/a/a/ua;

    if-eqz v1, :cond_4

    const v1, 0x3c4062e

    iget-object v2, p0, Lcom/google/a/a/a/a/nc;->f:Lcom/google/a/a/a/a/ua;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/nc;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/nc;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/nc;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/nc;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/nc;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/vh;

    invoke-direct {v0}, Lcom/google/a/a/a/a/vh;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nc;->b:Lcom/google/a/a/a/a/vh;

    iget-object v0, p0, Lcom/google/a/a/a/a/nc;->b:Lcom/google/a/a/a/a/vh;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/q;

    invoke-direct {v0}, Lcom/google/a/a/a/a/q;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nc;->c:Lcom/google/a/a/a/a/q;

    iget-object v0, p0, Lcom/google/a/a/a/a/nc;->c:Lcom/google/a/a/a/a/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/nl;

    invoke-direct {v0}, Lcom/google/a/a/a/a/nl;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nc;->d:Lcom/google/a/a/a/a/nl;

    iget-object v0, p0, Lcom/google/a/a/a/a/nc;->d:Lcom/google/a/a/a/a/nl;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/nk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/nk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nc;->e:Lcom/google/a/a/a/a/nk;

    iget-object v0, p0, Lcom/google/a/a/a/a/nc;->e:Lcom/google/a/a/a/a/nk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/ua;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ua;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nc;->f:Lcom/google/a/a/a/a/ua;

    iget-object v0, p0, Lcom/google/a/a/a/a/nc;->f:Lcom/google/a/a/a/a/ua;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x174a0c7a -> :sswitch_1
        0x1a71e1ba -> :sswitch_2
        0x1aa2dd8a -> :sswitch_3
        0x1e1833ea -> :sswitch_4
        0x1e203172 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/nc;->b:Lcom/google/a/a/a/a/vh;

    if-eqz v0, :cond_0

    const v0, 0x2e9418f

    iget-object v1, p0, Lcom/google/a/a/a/a/nc;->b:Lcom/google/a/a/a/a/vh;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/nc;->c:Lcom/google/a/a/a/a/q;

    if-eqz v0, :cond_1

    const v0, 0x34e3c37

    iget-object v1, p0, Lcom/google/a/a/a/a/nc;->c:Lcom/google/a/a/a/a/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/nc;->d:Lcom/google/a/a/a/a/nl;

    if-eqz v0, :cond_2

    const v0, 0x3545bb1

    iget-object v1, p0, Lcom/google/a/a/a/a/nc;->d:Lcom/google/a/a/a/a/nl;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/nc;->e:Lcom/google/a/a/a/a/nk;

    if-eqz v0, :cond_3

    const v0, 0x3c3067d

    iget-object v1, p0, Lcom/google/a/a/a/a/nc;->e:Lcom/google/a/a/a/a/nk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/nc;->f:Lcom/google/a/a/a/a/ua;

    if-eqz v0, :cond_4

    const v0, 0x3c4062e

    iget-object v1, p0, Lcom/google/a/a/a/a/nc;->f:Lcom/google/a/a/a/a/ua;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/nc;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
