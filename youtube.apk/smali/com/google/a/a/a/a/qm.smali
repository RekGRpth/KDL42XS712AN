.class public final Lcom/google/a/a/a/a/qm;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/qm;


# instance fields
.field public b:Lcom/google/a/a/a/a/py;

.field public c:J

.field public d:Ljava/lang/String;

.field public e:Lcom/google/a/a/a/a/qn;

.field public f:Ljava/lang/String;

.field public g:Lcom/google/a/a/a/a/dq;

.field public h:Lcom/google/a/a/a/a/qi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/qm;

    sput-object v0, Lcom/google/a/a/a/a/qm;->a:[Lcom/google/a/a/a/a/qm;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v2, p0, Lcom/google/a/a/a/a/qm;->b:Lcom/google/a/a/a/a/py;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/a/a/a/a/qm;->c:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/qm;->d:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/a/a/a/a/qm;->e:Lcom/google/a/a/a/a/qn;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/qm;->f:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/a/a/a/a/qm;->g:Lcom/google/a/a/a/a/dq;

    iput-object v2, p0, Lcom/google/a/a/a/a/qm;->h:Lcom/google/a/a/a/a/qi;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/qm;->b:Lcom/google/a/a/a/a/py;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/qm;->b:Lcom/google/a/a/a/a/py;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-wide v1, p0, Lcom/google/a/a/a/a/qm;->c:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/a/a/a/a/qm;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/qm;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/a/a/a/a/qm;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/qm;->e:Lcom/google/a/a/a/a/qn;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/a/a/a/a/qm;->e:Lcom/google/a/a/a/a/qn;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/qm;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/a/a/a/a/qm;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/qm;->g:Lcom/google/a/a/a/a/dq;

    if-eqz v1, :cond_5

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/a/a/a/a/qm;->g:Lcom/google/a/a/a/a/dq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/qm;->h:Lcom/google/a/a/a/a/qi;

    if-eqz v1, :cond_6

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/a/a/a/a/qm;->h:Lcom/google/a/a/a/a/qi;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lcom/google/a/a/a/a/qm;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/qm;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/qm;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/qm;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/qm;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/py;

    invoke-direct {v0}, Lcom/google/a/a/a/a/py;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/qm;->b:Lcom/google/a/a/a/a/py;

    iget-object v0, p0, Lcom/google/a/a/a/a/qm;->b:Lcom/google/a/a/a/a/py;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/a/a/a/a/qm;->c:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/qm;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/qn;

    invoke-direct {v0}, Lcom/google/a/a/a/a/qn;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/qm;->e:Lcom/google/a/a/a/a/qn;

    iget-object v0, p0, Lcom/google/a/a/a/a/qm;->e:Lcom/google/a/a/a/a/qn;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/qm;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/a/a/a/a/dq;

    invoke-direct {v0}, Lcom/google/a/a/a/a/dq;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/qm;->g:Lcom/google/a/a/a/a/dq;

    iget-object v0, p0, Lcom/google/a/a/a/a/qm;->g:Lcom/google/a/a/a/a/dq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_7
    new-instance v0, Lcom/google/a/a/a/a/qi;

    invoke-direct {v0}, Lcom/google/a/a/a/a/qi;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/qm;->h:Lcom/google/a/a/a/a/qi;

    iget-object v0, p0, Lcom/google/a/a/a/a/qm;->h:Lcom/google/a/a/a/a/qi;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4

    iget-object v0, p0, Lcom/google/a/a/a/a/qm;->b:Lcom/google/a/a/a/a/py;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/qm;->b:Lcom/google/a/a/a/a/py;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-wide v0, p0, Lcom/google/a/a/a/a/qm;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-wide v1, p0, Lcom/google/a/a/a/a/qm;->c:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/qm;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/a/a/a/a/qm;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/qm;->e:Lcom/google/a/a/a/a/qn;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/a/a/a/a/qm;->e:Lcom/google/a/a/a/a/qn;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/qm;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/a/a/a/a/qm;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/qm;->g:Lcom/google/a/a/a/a/dq;

    if-eqz v0, :cond_5

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/a/a/a/a/qm;->g:Lcom/google/a/a/a/a/dq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/qm;->h:Lcom/google/a/a/a/a/qi;

    if-eqz v0, :cond_6

    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/a/a/a/a/qm;->h:Lcom/google/a/a/a/a/qi;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/qm;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
