.class public final Lcom/google/a/a/a/a/op;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/op;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Lcom/google/a/a/a/a/fk;

.field public d:[Lcom/google/a/a/a/a/sx;

.field public e:J

.field public f:Lcom/google/a/a/a/a/kz;

.field public g:Lcom/google/a/a/a/a/fk;

.field public h:Lcom/google/a/a/a/a/fk;

.field public i:Lcom/google/a/a/a/a/fk;

.field public j:[Lcom/google/a/a/a/a/ob;

.field public k:Lcom/google/a/a/a/a/s;

.field public l:Ljava/lang/String;

.field public m:Lcom/google/a/a/a/a/fk;

.field public n:Lcom/google/a/a/a/a/iz;

.field public o:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/op;

    sput-object v0, Lcom/google/a/a/a/a/op;->a:[Lcom/google/a/a/a/a/op;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/op;->b:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/a/a/a/a/op;->c:Lcom/google/a/a/a/a/fk;

    sget-object v0, Lcom/google/a/a/a/a/sx;->a:[Lcom/google/a/a/a/a/sx;

    iput-object v0, p0, Lcom/google/a/a/a/a/op;->d:[Lcom/google/a/a/a/a/sx;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/a/a/a/a/op;->e:J

    iput-object v2, p0, Lcom/google/a/a/a/a/op;->f:Lcom/google/a/a/a/a/kz;

    iput-object v2, p0, Lcom/google/a/a/a/a/op;->g:Lcom/google/a/a/a/a/fk;

    iput-object v2, p0, Lcom/google/a/a/a/a/op;->h:Lcom/google/a/a/a/a/fk;

    iput-object v2, p0, Lcom/google/a/a/a/a/op;->i:Lcom/google/a/a/a/a/fk;

    sget-object v0, Lcom/google/a/a/a/a/ob;->a:[Lcom/google/a/a/a/a/ob;

    iput-object v0, p0, Lcom/google/a/a/a/a/op;->j:[Lcom/google/a/a/a/a/ob;

    iput-object v2, p0, Lcom/google/a/a/a/a/op;->k:Lcom/google/a/a/a/a/s;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/op;->l:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/a/a/a/a/op;->m:Lcom/google/a/a/a/a/fk;

    iput-object v2, p0, Lcom/google/a/a/a/a/op;->n:Lcom/google/a/a/a/a/iz;

    sget-object v0, Lcom/google/protobuf/nano/f;->l:[B

    iput-object v0, p0, Lcom/google/a/a/a/a/op;->o:[B

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/a/a/a/a/op;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/a/a/a/a/op;->c:Lcom/google/a/a/a/a/fk;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/a/a/a/a/op;->c:Lcom/google/a/a/a/a/fk;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lcom/google/a/a/a/a/op;->d:[Lcom/google/a/a/a/a/sx;

    if-eqz v2, :cond_1

    iget-object v4, p0, Lcom/google/a/a/a/a/op;->d:[Lcom/google/a/a/a/a/sx;

    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_1

    aget-object v3, v4, v2

    const/4 v6, 0x3

    invoke-static {v6, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_1

    :cond_1
    iget-wide v2, p0, Lcom/google/a/a/a/a/op;->e:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-wide v3, p0, Lcom/google/a/a/a/a/op;->e:J

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lcom/google/a/a/a/a/op;->f:Lcom/google/a/a/a/a/kz;

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/a/a/a/a/op;->f:Lcom/google/a/a/a/a/kz;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lcom/google/a/a/a/a/op;->g:Lcom/google/a/a/a/a/fk;

    if-eqz v2, :cond_4

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/a/a/a/a/op;->g:Lcom/google/a/a/a/a/fk;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/op;->h:Lcom/google/a/a/a/a/fk;

    if-eqz v2, :cond_5

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/a/a/a/a/op;->h:Lcom/google/a/a/a/a/fk;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Lcom/google/a/a/a/a/op;->i:Lcom/google/a/a/a/a/fk;

    if-eqz v2, :cond_6

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/a/a/a/a/op;->i:Lcom/google/a/a/a/a/fk;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget-object v2, p0, Lcom/google/a/a/a/a/op;->j:[Lcom/google/a/a/a/a/ob;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->j:[Lcom/google/a/a/a/a/ob;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    const/16 v5, 0x9

    invoke-static {v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_7
    iget-object v1, p0, Lcom/google/a/a/a/a/op;->k:Lcom/google/a/a/a/a/s;

    if-eqz v1, :cond_8

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->k:Lcom/google/a/a/a/a/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lcom/google/a/a/a/a/op;->l:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->l:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lcom/google/a/a/a/a/op;->m:Lcom/google/a/a/a/a/fk;

    if-eqz v1, :cond_a

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->m:Lcom/google/a/a/a/a/fk;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Lcom/google/a/a/a/a/op;->n:Lcom/google/a/a/a/a/iz;

    if-eqz v1, :cond_b

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->n:Lcom/google/a/a/a/a/iz;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Lcom/google/a/a/a/a/op;->o:[B

    sget-object v2, Lcom/google/protobuf/nano/f;->l:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_c

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->o:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-object v1, p0, Lcom/google/a/a/a/a/op;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/op;->dm:I

    return v0

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->dl:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/a/a/a/a/op;->dl:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/op;->dl:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/op;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/op;->c:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/op;->c:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/op;->d:[Lcom/google/a/a/a/a/sx;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/sx;

    iget-object v3, p0, Lcom/google/a/a/a/a/op;->d:[Lcom/google/a/a/a/a/sx;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/a/a/a/a/op;->d:[Lcom/google/a/a/a/a/sx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lcom/google/a/a/a/a/op;->d:[Lcom/google/a/a/a/a/sx;

    :goto_2
    iget-object v2, p0, Lcom/google/a/a/a/a/op;->d:[Lcom/google/a/a/a/a/sx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->d:[Lcom/google/a/a/a/a/sx;

    new-instance v3, Lcom/google/a/a/a/a/sx;

    invoke-direct {v3}, Lcom/google/a/a/a/a/sx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->d:[Lcom/google/a/a/a/a/sx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/op;->d:[Lcom/google/a/a/a/a/sx;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/op;->d:[Lcom/google/a/a/a/a/sx;

    new-instance v3, Lcom/google/a/a/a/a/sx;

    invoke-direct {v3}, Lcom/google/a/a/a/a/sx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->d:[Lcom/google/a/a/a/a/sx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/a/a/a/a/op;->e:J

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/kz;

    invoke-direct {v0}, Lcom/google/a/a/a/a/kz;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/op;->f:Lcom/google/a/a/a/a/kz;

    iget-object v0, p0, Lcom/google/a/a/a/a/op;->f:Lcom/google/a/a/a/a/kz;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/op;->g:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/op;->g:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_7
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/op;->h:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/op;->h:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_8
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/op;->i:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/op;->i:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/op;->j:[Lcom/google/a/a/a/a/ob;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/ob;

    iget-object v3, p0, Lcom/google/a/a/a/a/op;->j:[Lcom/google/a/a/a/a/ob;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/a/a/a/a/op;->j:[Lcom/google/a/a/a/a/ob;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Lcom/google/a/a/a/a/op;->j:[Lcom/google/a/a/a/a/ob;

    :goto_4
    iget-object v2, p0, Lcom/google/a/a/a/a/op;->j:[Lcom/google/a/a/a/a/ob;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->j:[Lcom/google/a/a/a/a/ob;

    new-instance v3, Lcom/google/a/a/a/a/ob;

    invoke-direct {v3}, Lcom/google/a/a/a/a/ob;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->j:[Lcom/google/a/a/a/a/ob;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/op;->j:[Lcom/google/a/a/a/a/ob;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lcom/google/a/a/a/a/op;->j:[Lcom/google/a/a/a/a/ob;

    new-instance v3, Lcom/google/a/a/a/a/ob;

    invoke-direct {v3}, Lcom/google/a/a/a/a/ob;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->j:[Lcom/google/a/a/a/a/ob;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_a
    new-instance v0, Lcom/google/a/a/a/a/s;

    invoke-direct {v0}, Lcom/google/a/a/a/a/s;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/op;->k:Lcom/google/a/a/a/a/s;

    iget-object v0, p0, Lcom/google/a/a/a/a/op;->k:Lcom/google/a/a/a/a/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/op;->l:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/op;->m:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/op;->m:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_d
    new-instance v0, Lcom/google/a/a/a/a/iz;

    invoke-direct {v0}, Lcom/google/a/a/a/a/iz;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/op;->n:Lcom/google/a/a/a/a/iz;

    iget-object v0, p0, Lcom/google/a/a/a/a/op;->n:Lcom/google/a/a/a/a/iz;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/op;->o:[B

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/op;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/op;->c:Lcom/google/a/a/a/a/fk;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->c:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/op;->d:[Lcom/google/a/a/a/a/sx;

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->d:[Lcom/google/a/a/a/a/sx;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-wide v1, p0, Lcom/google/a/a/a/a/op;->e:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/a/a/a/a/op;->e:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)V

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/op;->f:Lcom/google/a/a/a/a/kz;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->f:Lcom/google/a/a/a/a/kz;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/op;->g:Lcom/google/a/a/a/a/fk;

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->g:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/op;->h:Lcom/google/a/a/a/a/fk;

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->h:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_6
    iget-object v1, p0, Lcom/google/a/a/a/a/op;->i:Lcom/google/a/a/a/a/fk;

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/a/a/a/a/op;->i:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_7
    iget-object v1, p0, Lcom/google/a/a/a/a/op;->j:[Lcom/google/a/a/a/a/ob;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/a/a/a/a/op;->j:[Lcom/google/a/a/a/a/ob;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    const/16 v4, 0x9

    invoke-virtual {p1, v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/google/a/a/a/a/op;->k:Lcom/google/a/a/a/a/s;

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/a/a/a/a/op;->k:Lcom/google/a/a/a/a/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_9
    iget-object v0, p0, Lcom/google/a/a/a/a/op;->l:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/a/a/a/a/op;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_a
    iget-object v0, p0, Lcom/google/a/a/a/a/op;->m:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_b

    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/a/a/a/a/op;->m:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_b
    iget-object v0, p0, Lcom/google/a/a/a/a/op;->n:Lcom/google/a/a/a/a/iz;

    if-eqz v0, :cond_c

    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/a/a/a/a/op;->n:Lcom/google/a/a/a/a/iz;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_c
    iget-object v0, p0, Lcom/google/a/a/a/a/op;->o:[B

    sget-object v1, Lcom/google/protobuf/nano/f;->l:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_d

    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/a/a/a/a/op;->o:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    :cond_d
    iget-object v0, p0, Lcom/google/a/a/a/a/op;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
