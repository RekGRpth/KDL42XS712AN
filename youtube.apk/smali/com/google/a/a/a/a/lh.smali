.class public final Lcom/google/a/a/a/a/lh;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/lh;


# instance fields
.field public b:Lcom/google/a/a/a/a/le;

.field public c:Lcom/google/a/a/a/a/ld;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/lh;

    sput-object v0, Lcom/google/a/a/a/a/lh;->a:[Lcom/google/a/a/a/a/lh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/lh;->b:Lcom/google/a/a/a/a/le;

    iput-object v0, p0, Lcom/google/a/a/a/a/lh;->c:Lcom/google/a/a/a/a/ld;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/lh;->b:Lcom/google/a/a/a/a/le;

    if-eqz v1, :cond_0

    const v0, 0x30c4f54

    iget-object v1, p0, Lcom/google/a/a/a/a/lh;->b:Lcom/google/a/a/a/a/le;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/lh;->c:Lcom/google/a/a/a/a/ld;

    if-eqz v1, :cond_1

    const v1, 0x38a4896

    iget-object v2, p0, Lcom/google/a/a/a/a/lh;->c:Lcom/google/a/a/a/a/ld;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/lh;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/lh;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/lh;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/lh;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/lh;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/le;

    invoke-direct {v0}, Lcom/google/a/a/a/a/le;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/lh;->b:Lcom/google/a/a/a/a/le;

    iget-object v0, p0, Lcom/google/a/a/a/a/lh;->b:Lcom/google/a/a/a/a/le;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/ld;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ld;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/lh;->c:Lcom/google/a/a/a/a/ld;

    iget-object v0, p0, Lcom/google/a/a/a/a/lh;->c:Lcom/google/a/a/a/a/ld;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18627aa2 -> :sswitch_1
        0x1c5244b2 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/lh;->b:Lcom/google/a/a/a/a/le;

    if-eqz v0, :cond_0

    const v0, 0x30c4f54

    iget-object v1, p0, Lcom/google/a/a/a/a/lh;->b:Lcom/google/a/a/a/a/le;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/lh;->c:Lcom/google/a/a/a/a/ld;

    if-eqz v0, :cond_1

    const v0, 0x38a4896

    iget-object v1, p0, Lcom/google/a/a/a/a/lh;->c:Lcom/google/a/a/a/a/ld;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/lh;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
