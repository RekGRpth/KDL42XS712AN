.class public final Lcom/google/a/a/a/a/xh;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/xh;


# instance fields
.field public b:Lcom/google/a/a/a/a/sk;

.field public c:Lcom/google/a/a/a/a/kb;

.field public d:Lcom/google/a/a/a/a/rv;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/xh;

    sput-object v0, Lcom/google/a/a/a/a/xh;->a:[Lcom/google/a/a/a/a/xh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/xh;->b:Lcom/google/a/a/a/a/sk;

    iput-object v0, p0, Lcom/google/a/a/a/a/xh;->c:Lcom/google/a/a/a/a/kb;

    iput-object v0, p0, Lcom/google/a/a/a/a/xh;->d:Lcom/google/a/a/a/a/rv;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/xh;->b:Lcom/google/a/a/a/a/sk;

    if-eqz v1, :cond_0

    const v0, 0x363999a

    iget-object v1, p0, Lcom/google/a/a/a/a/xh;->b:Lcom/google/a/a/a/a/sk;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/xh;->c:Lcom/google/a/a/a/a/kb;

    if-eqz v1, :cond_1

    const v1, 0x39255a1

    iget-object v2, p0, Lcom/google/a/a/a/a/xh;->c:Lcom/google/a/a/a/a/kb;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/xh;->d:Lcom/google/a/a/a/a/rv;

    if-eqz v1, :cond_2

    const v1, 0x3b0b8f0

    iget-object v2, p0, Lcom/google/a/a/a/a/xh;->d:Lcom/google/a/a/a/a/rv;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/xh;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/xh;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/xh;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/xh;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/xh;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/sk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/sk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/xh;->b:Lcom/google/a/a/a/a/sk;

    iget-object v0, p0, Lcom/google/a/a/a/a/xh;->b:Lcom/google/a/a/a/a/sk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/kb;

    invoke-direct {v0}, Lcom/google/a/a/a/a/kb;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/xh;->c:Lcom/google/a/a/a/a/kb;

    iget-object v0, p0, Lcom/google/a/a/a/a/xh;->c:Lcom/google/a/a/a/a/kb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/rv;

    invoke-direct {v0}, Lcom/google/a/a/a/a/rv;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/xh;->d:Lcom/google/a/a/a/a/rv;

    iget-object v0, p0, Lcom/google/a/a/a/a/xh;->d:Lcom/google/a/a/a/a/rv;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1b1cccd2 -> :sswitch_1
        0x1c92ad0a -> :sswitch_2
        0x1d85c782 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/xh;->b:Lcom/google/a/a/a/a/sk;

    if-eqz v0, :cond_0

    const v0, 0x363999a

    iget-object v1, p0, Lcom/google/a/a/a/a/xh;->b:Lcom/google/a/a/a/a/sk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/xh;->c:Lcom/google/a/a/a/a/kb;

    if-eqz v0, :cond_1

    const v0, 0x39255a1

    iget-object v1, p0, Lcom/google/a/a/a/a/xh;->c:Lcom/google/a/a/a/a/kb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/xh;->d:Lcom/google/a/a/a/a/rv;

    if-eqz v0, :cond_2

    const v0, 0x3b0b8f0

    iget-object v1, p0, Lcom/google/a/a/a/a/xh;->d:Lcom/google/a/a/a/a/rv;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/xh;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
