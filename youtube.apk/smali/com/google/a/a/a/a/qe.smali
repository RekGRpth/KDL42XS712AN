.class public final Lcom/google/a/a/a/a/qe;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/qe;


# instance fields
.field public b:[[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/qe;

    sput-object v0, Lcom/google/a/a/a/a/qe;->a:[Lcom/google/a/a/a/a/qe;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    sget-object v0, Lcom/google/protobuf/nano/f;->k:[[B

    iput-object v0, p0, Lcom/google/a/a/a/a/qe;->b:[[B

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/qe;->b:[[B

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/a/a/a/a/qe;->b:[[B

    array-length v1, v1

    if-lez v1, :cond_1

    iget-object v2, p0, Lcom/google/a/a/a/a/qe;->b:[[B

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    invoke-static {v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a([B)I

    move-result v4

    add-int/2addr v1, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v1, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/qe;->b:[[B

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/qe;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/qe;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 4

    const/4 v3, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/qe;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/qe;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/qe;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v1

    iget-object v0, p0, Lcom/google/a/a/a/a/qe;->b:[[B

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [[B

    iget-object v2, p0, Lcom/google/a/a/a/a/qe;->b:[[B

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lcom/google/a/a/a/a/qe;->b:[[B

    :goto_1
    iget-object v1, p0, Lcom/google/a/a/a/a/qe;->b:[[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/a/a/a/a/qe;->b:[[B

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()[B

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/qe;->b:[[B

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()[B

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5

    iget-object v0, p0, Lcom/google/a/a/a/a/qe;->b:[[B

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/a/a/a/a/qe;->b:[[B

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/qe;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
