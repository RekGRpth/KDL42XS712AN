.class public final Lcom/google/a/a/a/a/uv;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/uv;


# instance fields
.field public b:Lcom/google/a/a/a/a/fk;

.field public c:Lcom/google/a/a/a/a/fk;

.field public d:Lcom/google/a/a/a/a/fk;

.field public e:Lcom/google/a/a/a/a/fk;

.field public f:Lcom/google/a/a/a/a/fk;

.field public g:Lcom/google/a/a/a/a/fk;

.field public h:Lcom/google/a/a/a/a/fk;

.field public i:Z

.field public j:Z

.field public k:Lcom/google/a/a/a/a/fk;

.field public l:I

.field public m:Ljava/lang/String;

.field public n:Lcom/google/a/a/a/a/iz;

.field public o:[Lcom/google/a/a/a/a/ak;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/uv;

    sput-object v0, Lcom/google/a/a/a/a/uv;->a:[Lcom/google/a/a/a/a/uv;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/uv;->b:Lcom/google/a/a/a/a/fk;

    iput-object v1, p0, Lcom/google/a/a/a/a/uv;->c:Lcom/google/a/a/a/a/fk;

    iput-object v1, p0, Lcom/google/a/a/a/a/uv;->d:Lcom/google/a/a/a/a/fk;

    iput-object v1, p0, Lcom/google/a/a/a/a/uv;->e:Lcom/google/a/a/a/a/fk;

    iput-object v1, p0, Lcom/google/a/a/a/a/uv;->f:Lcom/google/a/a/a/a/fk;

    iput-object v1, p0, Lcom/google/a/a/a/a/uv;->g:Lcom/google/a/a/a/a/fk;

    iput-object v1, p0, Lcom/google/a/a/a/a/uv;->h:Lcom/google/a/a/a/a/fk;

    iput-boolean v0, p0, Lcom/google/a/a/a/a/uv;->i:Z

    iput-boolean v0, p0, Lcom/google/a/a/a/a/uv;->j:Z

    iput-object v1, p0, Lcom/google/a/a/a/a/uv;->k:Lcom/google/a/a/a/a/fk;

    iput v0, p0, Lcom/google/a/a/a/a/uv;->l:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/uv;->m:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/a/a/a/a/uv;->n:Lcom/google/a/a/a/a/iz;

    sget-object v0, Lcom/google/a/a/a/a/ak;->a:[Lcom/google/a/a/a/a/ak;

    iput-object v0, p0, Lcom/google/a/a/a/a/uv;->o:[Lcom/google/a/a/a/a/ak;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->b:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->b:Lcom/google/a/a/a/a/fk;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->c:Lcom/google/a/a/a/a/fk;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/a/a/a/a/uv;->c:Lcom/google/a/a/a/a/fk;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->d:Lcom/google/a/a/a/a/fk;

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/a/a/a/a/uv;->d:Lcom/google/a/a/a/a/fk;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->e:Lcom/google/a/a/a/a/fk;

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/a/a/a/a/uv;->e:Lcom/google/a/a/a/a/fk;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->f:Lcom/google/a/a/a/a/fk;

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/a/a/a/a/uv;->f:Lcom/google/a/a/a/a/fk;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->g:Lcom/google/a/a/a/a/fk;

    if-eqz v2, :cond_4

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/a/a/a/a/uv;->g:Lcom/google/a/a/a/a/fk;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->h:Lcom/google/a/a/a/a/fk;

    if-eqz v2, :cond_5

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/a/a/a/a/uv;->h:Lcom/google/a/a/a/a/fk;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-boolean v2, p0, Lcom/google/a/a/a/a/uv;->i:Z

    if-eqz v2, :cond_6

    const/16 v2, 0x8

    iget-boolean v3, p0, Lcom/google/a/a/a/a/uv;->i:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget-boolean v2, p0, Lcom/google/a/a/a/a/uv;->j:Z

    if-eqz v2, :cond_7

    const/16 v2, 0x9

    iget-boolean v3, p0, Lcom/google/a/a/a/a/uv;->j:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->k:Lcom/google/a/a/a/a/fk;

    if-eqz v2, :cond_8

    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/a/a/a/a/uv;->k:Lcom/google/a/a/a/a/fk;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget v2, p0, Lcom/google/a/a/a/a/uv;->l:I

    if-eqz v2, :cond_9

    const/16 v2, 0xb

    iget v3, p0, Lcom/google/a/a/a/a/uv;->l:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_9
    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->m:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/a/a/a/a/uv;->m:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_a
    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->n:Lcom/google/a/a/a/a/iz;

    if-eqz v2, :cond_b

    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/a/a/a/a/uv;->n:Lcom/google/a/a/a/a/iz;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_b
    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->o:[Lcom/google/a/a/a/a/ak;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->o:[Lcom/google/a/a/a/a/ak;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    const/16 v5, 0xe

    invoke-static {v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_c
    iget-object v1, p0, Lcom/google/a/a/a/a/uv;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/uv;->dm:I

    return v0

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->dl:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/a/a/a/a/uv;->dl:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->dl:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uv;->b:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->b:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uv;->c:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->c:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uv;->d:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->d:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uv;->e:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->e:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uv;->f:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->f:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uv;->g:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->g:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_7
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uv;->h:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->h:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/uv;->i:Z

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/uv;->j:Z

    goto/16 :goto_0

    :sswitch_a
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uv;->k:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->k:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    :cond_2
    iput v0, p0, Lcom/google/a/a/a/a/uv;->l:I

    goto/16 :goto_0

    :cond_3
    iput v1, p0, Lcom/google/a/a/a/a/uv;->l:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/uv;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    new-instance v0, Lcom/google/a/a/a/a/iz;

    invoke-direct {v0}, Lcom/google/a/a/a/a/iz;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/uv;->n:Lcom/google/a/a/a/a/iz;

    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->n:Lcom/google/a/a/a/a/iz;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_e
    const/16 v0, 0x72

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->o:[Lcom/google/a/a/a/a/ak;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/ak;

    iget-object v3, p0, Lcom/google/a/a/a/a/uv;->o:[Lcom/google/a/a/a/a/ak;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/a/a/a/a/uv;->o:[Lcom/google/a/a/a/a/ak;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Lcom/google/a/a/a/a/uv;->o:[Lcom/google/a/a/a/a/ak;

    :goto_2
    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->o:[Lcom/google/a/a/a/a/ak;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->o:[Lcom/google/a/a/a/a/ak;

    new-instance v3, Lcom/google/a/a/a/a/ak;

    invoke-direct {v3}, Lcom/google/a/a/a/a/ak;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->o:[Lcom/google/a/a/a/a/ak;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->o:[Lcom/google/a/a/a/a/ak;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->o:[Lcom/google/a/a/a/a/ak;

    new-instance v3, Lcom/google/a/a/a/a/ak;

    invoke-direct {v3}, Lcom/google/a/a/a/a/ak;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/uv;->o:[Lcom/google/a/a/a/a/ak;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5

    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->b:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/uv;->b:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->c:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/a/a/a/a/uv;->c:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->d:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/a/a/a/a/uv;->d:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->e:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/a/a/a/a/uv;->e:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->f:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/a/a/a/a/uv;->f:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->g:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/a/a/a/a/uv;->g:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->h:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/a/a/a/a/uv;->h:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_6
    iget-boolean v0, p0, Lcom/google/a/a/a/a/uv;->i:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/a/a/a/a/uv;->i:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_7
    iget-boolean v0, p0, Lcom/google/a/a/a/a/uv;->j:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/a/a/a/a/uv;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_8
    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->k:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/a/a/a/a/uv;->k:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_9
    iget v0, p0, Lcom/google/a/a/a/a/uv;->l:I

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget v1, p0, Lcom/google/a/a/a/a/uv;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_a
    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->m:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/a/a/a/a/uv;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_b
    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->n:Lcom/google/a/a/a/a/iz;

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/a/a/a/a/uv;->n:Lcom/google/a/a/a/a/iz;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_c
    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->o:[Lcom/google/a/a/a/a/ak;

    if-eqz v0, :cond_d

    iget-object v1, p0, Lcom/google/a/a/a/a/uv;->o:[Lcom/google/a/a/a/a/ak;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_d

    aget-object v3, v1, v0

    const/16 v4, 0xe

    invoke-virtual {p1, v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_d
    iget-object v0, p0, Lcom/google/a/a/a/a/uv;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
