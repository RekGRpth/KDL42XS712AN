.class public final Lcom/google/a/a/a/a/dp;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/dp;


# instance fields
.field public b:Lcom/google/a/a/a/a/ly;

.field public c:Lcom/google/a/a/a/a/li;

.field public d:Lcom/google/a/a/a/a/pd;

.field public e:Lcom/google/a/a/a/a/mq;

.field public f:Lcom/google/a/a/a/a/pr;

.field public g:Lcom/google/a/a/a/a/pe;

.field public h:Lcom/google/a/a/a/a/lj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/dp;

    sput-object v0, Lcom/google/a/a/a/a/dp;->a:[Lcom/google/a/a/a/a/dp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dp;->b:Lcom/google/a/a/a/a/ly;

    iput-object v0, p0, Lcom/google/a/a/a/a/dp;->c:Lcom/google/a/a/a/a/li;

    iput-object v0, p0, Lcom/google/a/a/a/a/dp;->d:Lcom/google/a/a/a/a/pd;

    iput-object v0, p0, Lcom/google/a/a/a/a/dp;->e:Lcom/google/a/a/a/a/mq;

    iput-object v0, p0, Lcom/google/a/a/a/a/dp;->f:Lcom/google/a/a/a/a/pr;

    iput-object v0, p0, Lcom/google/a/a/a/a/dp;->g:Lcom/google/a/a/a/a/pe;

    iput-object v0, p0, Lcom/google/a/a/a/a/dp;->h:Lcom/google/a/a/a/a/lj;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->b:Lcom/google/a/a/a/a/ly;

    if-eqz v1, :cond_0

    const v0, 0x3120359

    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->b:Lcom/google/a/a/a/a/ly;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->c:Lcom/google/a/a/a/a/li;

    if-eqz v1, :cond_1

    const v1, 0x31a2ee9

    iget-object v2, p0, Lcom/google/a/a/a/a/dp;->c:Lcom/google/a/a/a/a/li;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->d:Lcom/google/a/a/a/a/pd;

    if-eqz v1, :cond_2

    const v1, 0x31a2eed

    iget-object v2, p0, Lcom/google/a/a/a/a/dp;->d:Lcom/google/a/a/a/a/pd;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->e:Lcom/google/a/a/a/a/mq;

    if-eqz v1, :cond_3

    const v1, 0x31a2ef1

    iget-object v2, p0, Lcom/google/a/a/a/a/dp;->e:Lcom/google/a/a/a/a/mq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->f:Lcom/google/a/a/a/a/pr;

    if-eqz v1, :cond_4

    const v1, 0x39af697

    iget-object v2, p0, Lcom/google/a/a/a/a/dp;->f:Lcom/google/a/a/a/a/pr;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->g:Lcom/google/a/a/a/a/pe;

    if-eqz v1, :cond_5

    const v1, 0x3af2bec

    iget-object v2, p0, Lcom/google/a/a/a/a/dp;->g:Lcom/google/a/a/a/a/pe;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->h:Lcom/google/a/a/a/a/lj;

    if-eqz v1, :cond_6

    const v1, 0x3af2c08

    iget-object v2, p0, Lcom/google/a/a/a/a/dp;->h:Lcom/google/a/a/a/a/lj;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/dp;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/dp;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/ly;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ly;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dp;->b:Lcom/google/a/a/a/a/ly;

    iget-object v0, p0, Lcom/google/a/a/a/a/dp;->b:Lcom/google/a/a/a/a/ly;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/li;

    invoke-direct {v0}, Lcom/google/a/a/a/a/li;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dp;->c:Lcom/google/a/a/a/a/li;

    iget-object v0, p0, Lcom/google/a/a/a/a/dp;->c:Lcom/google/a/a/a/a/li;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/pd;

    invoke-direct {v0}, Lcom/google/a/a/a/a/pd;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dp;->d:Lcom/google/a/a/a/a/pd;

    iget-object v0, p0, Lcom/google/a/a/a/a/dp;->d:Lcom/google/a/a/a/a/pd;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/mq;

    invoke-direct {v0}, Lcom/google/a/a/a/a/mq;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dp;->e:Lcom/google/a/a/a/a/mq;

    iget-object v0, p0, Lcom/google/a/a/a/a/dp;->e:Lcom/google/a/a/a/a/mq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/pr;

    invoke-direct {v0}, Lcom/google/a/a/a/a/pr;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dp;->f:Lcom/google/a/a/a/a/pr;

    iget-object v0, p0, Lcom/google/a/a/a/a/dp;->f:Lcom/google/a/a/a/a/pr;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/a/a/a/a/pe;

    invoke-direct {v0}, Lcom/google/a/a/a/a/pe;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dp;->g:Lcom/google/a/a/a/a/pe;

    iget-object v0, p0, Lcom/google/a/a/a/a/dp;->g:Lcom/google/a/a/a/a/pe;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_7
    new-instance v0, Lcom/google/a/a/a/a/lj;

    invoke-direct {v0}, Lcom/google/a/a/a/a/lj;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/dp;->h:Lcom/google/a/a/a/a/lj;

    iget-object v0, p0, Lcom/google/a/a/a/a/dp;->h:Lcom/google/a/a/a/a/lj;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18901aca -> :sswitch_1
        0x18d1774a -> :sswitch_2
        0x18d1776a -> :sswitch_3
        0x18d1778a -> :sswitch_4
        0x1cd7b4ba -> :sswitch_5
        0x1d795f62 -> :sswitch_6
        0x1d796042 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/dp;->b:Lcom/google/a/a/a/a/ly;

    if-eqz v0, :cond_0

    const v0, 0x3120359

    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->b:Lcom/google/a/a/a/a/ly;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/dp;->c:Lcom/google/a/a/a/a/li;

    if-eqz v0, :cond_1

    const v0, 0x31a2ee9

    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->c:Lcom/google/a/a/a/a/li;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/dp;->d:Lcom/google/a/a/a/a/pd;

    if-eqz v0, :cond_2

    const v0, 0x31a2eed

    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->d:Lcom/google/a/a/a/a/pd;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/dp;->e:Lcom/google/a/a/a/a/mq;

    if-eqz v0, :cond_3

    const v0, 0x31a2ef1

    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->e:Lcom/google/a/a/a/a/mq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/dp;->f:Lcom/google/a/a/a/a/pr;

    if-eqz v0, :cond_4

    const v0, 0x39af697

    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->f:Lcom/google/a/a/a/a/pr;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/dp;->g:Lcom/google/a/a/a/a/pe;

    if-eqz v0, :cond_5

    const v0, 0x3af2bec

    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->g:Lcom/google/a/a/a/a/pe;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/dp;->h:Lcom/google/a/a/a/a/lj;

    if-eqz v0, :cond_6

    const v0, 0x3af2c08

    iget-object v1, p0, Lcom/google/a/a/a/a/dp;->h:Lcom/google/a/a/a/a/lj;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/dp;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
