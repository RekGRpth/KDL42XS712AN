.class public final Lcom/google/a/a/a/a/qt;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/qt;


# instance fields
.field public b:Lcom/google/a/a/a/a/it;

.field public c:Lcom/google/a/a/a/a/ev;

.field public d:Lcom/google/a/a/a/a/iq;

.field public e:Lcom/google/a/a/a/a/rd;

.field public f:Lcom/google/a/a/a/a/da;

.field public g:Lcom/google/a/a/a/a/os;

.field public h:Lcom/google/a/a/a/a/dl;

.field public i:Lcom/google/a/a/a/a/lt;

.field public j:Lcom/google/a/a/a/a/lw;

.field public k:Lcom/google/a/a/a/a/cx;

.field public l:Lcom/google/a/a/a/a/mu;

.field public m:Lcom/google/a/a/a/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/qt;

    sput-object v0, Lcom/google/a/a/a/a/qt;->a:[Lcom/google/a/a/a/a/qt;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->b:Lcom/google/a/a/a/a/it;

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->c:Lcom/google/a/a/a/a/ev;

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->d:Lcom/google/a/a/a/a/iq;

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->e:Lcom/google/a/a/a/a/rd;

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->f:Lcom/google/a/a/a/a/da;

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->g:Lcom/google/a/a/a/a/os;

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->h:Lcom/google/a/a/a/a/dl;

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->i:Lcom/google/a/a/a/a/lt;

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->j:Lcom/google/a/a/a/a/lw;

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->k:Lcom/google/a/a/a/a/cx;

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->l:Lcom/google/a/a/a/a/mu;

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->m:Lcom/google/a/a/a/a/d;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->b:Lcom/google/a/a/a/a/it;

    if-eqz v1, :cond_0

    const v0, 0x2fdec06

    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->b:Lcom/google/a/a/a/a/it;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->c:Lcom/google/a/a/a/a/ev;

    if-eqz v1, :cond_1

    const v1, 0x2ff8ca1

    iget-object v2, p0, Lcom/google/a/a/a/a/qt;->c:Lcom/google/a/a/a/a/ev;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->d:Lcom/google/a/a/a/a/iq;

    if-eqz v1, :cond_2

    const v1, 0x3161856

    iget-object v2, p0, Lcom/google/a/a/a/a/qt;->d:Lcom/google/a/a/a/a/iq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->e:Lcom/google/a/a/a/a/rd;

    if-eqz v1, :cond_3

    const v1, 0x31717cb

    iget-object v2, p0, Lcom/google/a/a/a/a/qt;->e:Lcom/google/a/a/a/a/rd;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->f:Lcom/google/a/a/a/a/da;

    if-eqz v1, :cond_4

    const v1, 0x317f2bb

    iget-object v2, p0, Lcom/google/a/a/a/a/qt;->f:Lcom/google/a/a/a/a/da;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->g:Lcom/google/a/a/a/a/os;

    if-eqz v1, :cond_5

    const v1, 0x3425de4

    iget-object v2, p0, Lcom/google/a/a/a/a/qt;->g:Lcom/google/a/a/a/a/os;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->h:Lcom/google/a/a/a/a/dl;

    if-eqz v1, :cond_6

    const v1, 0x34f1549

    iget-object v2, p0, Lcom/google/a/a/a/a/qt;->h:Lcom/google/a/a/a/a/dl;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->i:Lcom/google/a/a/a/a/lt;

    if-eqz v1, :cond_7

    const v1, 0x396e0c9

    iget-object v2, p0, Lcom/google/a/a/a/a/qt;->i:Lcom/google/a/a/a/a/lt;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->j:Lcom/google/a/a/a/a/lw;

    if-eqz v1, :cond_8

    const v1, 0x399c073

    iget-object v2, p0, Lcom/google/a/a/a/a/qt;->j:Lcom/google/a/a/a/a/lw;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->k:Lcom/google/a/a/a/a/cx;

    if-eqz v1, :cond_9

    const v1, 0x3b67ec1

    iget-object v2, p0, Lcom/google/a/a/a/a/qt;->k:Lcom/google/a/a/a/a/cx;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->l:Lcom/google/a/a/a/a/mu;

    if-eqz v1, :cond_a

    const v1, 0x3b6c655

    iget-object v2, p0, Lcom/google/a/a/a/a/qt;->l:Lcom/google/a/a/a/a/mu;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->m:Lcom/google/a/a/a/a/d;

    if-eqz v1, :cond_b

    const v1, 0x3c7eeec

    iget-object v2, p0, Lcom/google/a/a/a/a/qt;->m:Lcom/google/a/a/a/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/qt;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/qt;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/it;

    invoke-direct {v0}, Lcom/google/a/a/a/a/it;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->b:Lcom/google/a/a/a/a/it;

    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->b:Lcom/google/a/a/a/a/it;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/ev;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ev;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->c:Lcom/google/a/a/a/a/ev;

    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->c:Lcom/google/a/a/a/a/ev;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/iq;

    invoke-direct {v0}, Lcom/google/a/a/a/a/iq;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->d:Lcom/google/a/a/a/a/iq;

    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->d:Lcom/google/a/a/a/a/iq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/rd;

    invoke-direct {v0}, Lcom/google/a/a/a/a/rd;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->e:Lcom/google/a/a/a/a/rd;

    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->e:Lcom/google/a/a/a/a/rd;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/da;

    invoke-direct {v0}, Lcom/google/a/a/a/a/da;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->f:Lcom/google/a/a/a/a/da;

    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->f:Lcom/google/a/a/a/a/da;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/a/a/a/a/os;

    invoke-direct {v0}, Lcom/google/a/a/a/a/os;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->g:Lcom/google/a/a/a/a/os;

    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->g:Lcom/google/a/a/a/a/os;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_7
    new-instance v0, Lcom/google/a/a/a/a/dl;

    invoke-direct {v0}, Lcom/google/a/a/a/a/dl;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->h:Lcom/google/a/a/a/a/dl;

    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->h:Lcom/google/a/a/a/a/dl;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_8
    new-instance v0, Lcom/google/a/a/a/a/lt;

    invoke-direct {v0}, Lcom/google/a/a/a/a/lt;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->i:Lcom/google/a/a/a/a/lt;

    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->i:Lcom/google/a/a/a/a/lt;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_9
    new-instance v0, Lcom/google/a/a/a/a/lw;

    invoke-direct {v0}, Lcom/google/a/a/a/a/lw;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->j:Lcom/google/a/a/a/a/lw;

    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->j:Lcom/google/a/a/a/a/lw;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_a
    new-instance v0, Lcom/google/a/a/a/a/cx;

    invoke-direct {v0}, Lcom/google/a/a/a/a/cx;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->k:Lcom/google/a/a/a/a/cx;

    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->k:Lcom/google/a/a/a/a/cx;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_b
    new-instance v0, Lcom/google/a/a/a/a/mu;

    invoke-direct {v0}, Lcom/google/a/a/a/a/mu;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->l:Lcom/google/a/a/a/a/mu;

    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->l:Lcom/google/a/a/a/a/mu;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_c
    new-instance v0, Lcom/google/a/a/a/a/d;

    invoke-direct {v0}, Lcom/google/a/a/a/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/qt;->m:Lcom/google/a/a/a/a/d;

    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->m:Lcom/google/a/a/a/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x17ef6032 -> :sswitch_1
        0x17fc650a -> :sswitch_2
        0x18b0c2b2 -> :sswitch_3
        0x18b8be5a -> :sswitch_4
        0x18bf95da -> :sswitch_5
        0x1a12ef22 -> :sswitch_6
        0x1a78aa4a -> :sswitch_7
        0x1cb7064a -> :sswitch_8
        0x1cce039a -> :sswitch_9
        0x1db3f60a -> :sswitch_a
        0x1db632aa -> :sswitch_b
        0x1e3f7762 -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->b:Lcom/google/a/a/a/a/it;

    if-eqz v0, :cond_0

    const v0, 0x2fdec06

    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->b:Lcom/google/a/a/a/a/it;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->c:Lcom/google/a/a/a/a/ev;

    if-eqz v0, :cond_1

    const v0, 0x2ff8ca1

    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->c:Lcom/google/a/a/a/a/ev;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->d:Lcom/google/a/a/a/a/iq;

    if-eqz v0, :cond_2

    const v0, 0x3161856

    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->d:Lcom/google/a/a/a/a/iq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->e:Lcom/google/a/a/a/a/rd;

    if-eqz v0, :cond_3

    const v0, 0x31717cb

    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->e:Lcom/google/a/a/a/a/rd;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->f:Lcom/google/a/a/a/a/da;

    if-eqz v0, :cond_4

    const v0, 0x317f2bb

    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->f:Lcom/google/a/a/a/a/da;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->g:Lcom/google/a/a/a/a/os;

    if-eqz v0, :cond_5

    const v0, 0x3425de4

    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->g:Lcom/google/a/a/a/a/os;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->h:Lcom/google/a/a/a/a/dl;

    if-eqz v0, :cond_6

    const v0, 0x34f1549

    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->h:Lcom/google/a/a/a/a/dl;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->i:Lcom/google/a/a/a/a/lt;

    if-eqz v0, :cond_7

    const v0, 0x396e0c9

    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->i:Lcom/google/a/a/a/a/lt;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_7
    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->j:Lcom/google/a/a/a/a/lw;

    if-eqz v0, :cond_8

    const v0, 0x399c073

    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->j:Lcom/google/a/a/a/a/lw;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_8
    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->k:Lcom/google/a/a/a/a/cx;

    if-eqz v0, :cond_9

    const v0, 0x3b67ec1

    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->k:Lcom/google/a/a/a/a/cx;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_9
    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->l:Lcom/google/a/a/a/a/mu;

    if-eqz v0, :cond_a

    const v0, 0x3b6c655

    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->l:Lcom/google/a/a/a/a/mu;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_a
    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->m:Lcom/google/a/a/a/a/d;

    if-eqz v0, :cond_b

    const v0, 0x3c7eeec

    iget-object v1, p0, Lcom/google/a/a/a/a/qt;->m:Lcom/google/a/a/a/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_b
    iget-object v0, p0, Lcom/google/a/a/a/a/qt;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
