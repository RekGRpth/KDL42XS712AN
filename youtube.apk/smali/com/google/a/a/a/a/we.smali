.class public final Lcom/google/a/a/a/a/we;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/we;


# instance fields
.field public b:Lcom/google/a/a/a/a/em;

.field public c:Lcom/google/a/a/a/a/en;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/we;

    sput-object v0, Lcom/google/a/a/a/a/we;->a:[Lcom/google/a/a/a/a/we;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/we;->b:Lcom/google/a/a/a/a/em;

    iput-object v0, p0, Lcom/google/a/a/a/a/we;->c:Lcom/google/a/a/a/a/en;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/we;->b:Lcom/google/a/a/a/a/em;

    if-eqz v1, :cond_0

    const v0, 0x367b478

    iget-object v1, p0, Lcom/google/a/a/a/a/we;->b:Lcom/google/a/a/a/a/em;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/we;->c:Lcom/google/a/a/a/a/en;

    if-eqz v1, :cond_1

    const v1, 0x36815f9

    iget-object v2, p0, Lcom/google/a/a/a/a/we;->c:Lcom/google/a/a/a/a/en;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/we;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/we;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/we;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/we;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/we;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/em;

    invoke-direct {v0}, Lcom/google/a/a/a/a/em;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/we;->b:Lcom/google/a/a/a/a/em;

    iget-object v0, p0, Lcom/google/a/a/a/a/we;->b:Lcom/google/a/a/a/a/em;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/en;

    invoke-direct {v0}, Lcom/google/a/a/a/a/en;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/we;->c:Lcom/google/a/a/a/a/en;

    iget-object v0, p0, Lcom/google/a/a/a/a/we;->c:Lcom/google/a/a/a/a/en;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1b3da3c2 -> :sswitch_1
        0x1b40afca -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/we;->b:Lcom/google/a/a/a/a/em;

    if-eqz v0, :cond_0

    const v0, 0x367b478

    iget-object v1, p0, Lcom/google/a/a/a/a/we;->b:Lcom/google/a/a/a/a/em;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/we;->c:Lcom/google/a/a/a/a/en;

    if-eqz v0, :cond_1

    const v0, 0x36815f9

    iget-object v1, p0, Lcom/google/a/a/a/a/we;->c:Lcom/google/a/a/a/a/en;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/we;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
