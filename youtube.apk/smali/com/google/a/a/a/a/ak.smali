.class public final Lcom/google/a/a/a/a/ak;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/ak;


# instance fields
.field public b:Lcom/google/a/a/a/a/ub;

.field public c:Lcom/google/a/a/a/a/xi;

.field public d:Lcom/google/a/a/a/a/xc;

.field public e:Lcom/google/a/a/a/a/je;

.field public f:Lcom/google/a/a/a/a/ss;

.field public g:Lcom/google/a/a/a/a/sz;

.field public h:Lcom/google/a/a/a/a/kj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/ak;

    sput-object v0, Lcom/google/a/a/a/a/ak;->a:[Lcom/google/a/a/a/a/ak;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ak;->b:Lcom/google/a/a/a/a/ub;

    iput-object v0, p0, Lcom/google/a/a/a/a/ak;->c:Lcom/google/a/a/a/a/xi;

    iput-object v0, p0, Lcom/google/a/a/a/a/ak;->d:Lcom/google/a/a/a/a/xc;

    iput-object v0, p0, Lcom/google/a/a/a/a/ak;->e:Lcom/google/a/a/a/a/je;

    iput-object v0, p0, Lcom/google/a/a/a/a/ak;->f:Lcom/google/a/a/a/a/ss;

    iput-object v0, p0, Lcom/google/a/a/a/a/ak;->g:Lcom/google/a/a/a/a/sz;

    iput-object v0, p0, Lcom/google/a/a/a/a/ak;->h:Lcom/google/a/a/a/a/kj;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->b:Lcom/google/a/a/a/a/ub;

    if-eqz v1, :cond_0

    const v0, 0x3080b8a

    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->b:Lcom/google/a/a/a/a/ub;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->c:Lcom/google/a/a/a/a/xi;

    if-eqz v1, :cond_1

    const v1, 0x3084dbb

    iget-object v2, p0, Lcom/google/a/a/a/a/ak;->c:Lcom/google/a/a/a/a/xi;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->d:Lcom/google/a/a/a/a/xc;

    if-eqz v1, :cond_2

    const v1, 0x308c7d2

    iget-object v2, p0, Lcom/google/a/a/a/a/ak;->d:Lcom/google/a/a/a/a/xc;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->e:Lcom/google/a/a/a/a/je;

    if-eqz v1, :cond_3

    const v1, 0x308ffc6

    iget-object v2, p0, Lcom/google/a/a/a/a/ak;->e:Lcom/google/a/a/a/a/je;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->f:Lcom/google/a/a/a/a/ss;

    if-eqz v1, :cond_4

    const v1, 0x30905d8

    iget-object v2, p0, Lcom/google/a/a/a/a/ak;->f:Lcom/google/a/a/a/a/ss;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->g:Lcom/google/a/a/a/a/sz;

    if-eqz v1, :cond_5

    const v1, 0x3326ee1

    iget-object v2, p0, Lcom/google/a/a/a/a/ak;->g:Lcom/google/a/a/a/a/sz;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->h:Lcom/google/a/a/a/a/kj;

    if-eqz v1, :cond_6

    const v1, 0x396214a

    iget-object v2, p0, Lcom/google/a/a/a/a/ak;->h:Lcom/google/a/a/a/a/kj;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/ak;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/ak;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/ub;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ub;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ak;->b:Lcom/google/a/a/a/a/ub;

    iget-object v0, p0, Lcom/google/a/a/a/a/ak;->b:Lcom/google/a/a/a/a/ub;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/xi;

    invoke-direct {v0}, Lcom/google/a/a/a/a/xi;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ak;->c:Lcom/google/a/a/a/a/xi;

    iget-object v0, p0, Lcom/google/a/a/a/a/ak;->c:Lcom/google/a/a/a/a/xi;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/xc;

    invoke-direct {v0}, Lcom/google/a/a/a/a/xc;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ak;->d:Lcom/google/a/a/a/a/xc;

    iget-object v0, p0, Lcom/google/a/a/a/a/ak;->d:Lcom/google/a/a/a/a/xc;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/je;

    invoke-direct {v0}, Lcom/google/a/a/a/a/je;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ak;->e:Lcom/google/a/a/a/a/je;

    iget-object v0, p0, Lcom/google/a/a/a/a/ak;->e:Lcom/google/a/a/a/a/je;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/ss;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ss;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ak;->f:Lcom/google/a/a/a/a/ss;

    iget-object v0, p0, Lcom/google/a/a/a/a/ak;->f:Lcom/google/a/a/a/a/ss;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/a/a/a/a/sz;

    invoke-direct {v0}, Lcom/google/a/a/a/a/sz;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ak;->g:Lcom/google/a/a/a/a/sz;

    iget-object v0, p0, Lcom/google/a/a/a/a/ak;->g:Lcom/google/a/a/a/a/sz;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_7
    new-instance v0, Lcom/google/a/a/a/a/kj;

    invoke-direct {v0}, Lcom/google/a/a/a/a/kj;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ak;->h:Lcom/google/a/a/a/a/kj;

    iget-object v0, p0, Lcom/google/a/a/a/a/ak;->h:Lcom/google/a/a/a/a/kj;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18405c52 -> :sswitch_1
        0x18426dda -> :sswitch_2
        0x18463e92 -> :sswitch_3
        0x1847fe32 -> :sswitch_4
        0x18482ec2 -> :sswitch_5
        0x1993770a -> :sswitch_6
        0x1cb10a52 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/ak;->b:Lcom/google/a/a/a/a/ub;

    if-eqz v0, :cond_0

    const v0, 0x3080b8a

    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->b:Lcom/google/a/a/a/a/ub;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/ak;->c:Lcom/google/a/a/a/a/xi;

    if-eqz v0, :cond_1

    const v0, 0x3084dbb

    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->c:Lcom/google/a/a/a/a/xi;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/ak;->d:Lcom/google/a/a/a/a/xc;

    if-eqz v0, :cond_2

    const v0, 0x308c7d2

    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->d:Lcom/google/a/a/a/a/xc;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/ak;->e:Lcom/google/a/a/a/a/je;

    if-eqz v0, :cond_3

    const v0, 0x308ffc6

    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->e:Lcom/google/a/a/a/a/je;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/ak;->f:Lcom/google/a/a/a/a/ss;

    if-eqz v0, :cond_4

    const v0, 0x30905d8

    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->f:Lcom/google/a/a/a/a/ss;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/ak;->g:Lcom/google/a/a/a/a/sz;

    if-eqz v0, :cond_5

    const v0, 0x3326ee1

    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->g:Lcom/google/a/a/a/a/sz;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/ak;->h:Lcom/google/a/a/a/a/kj;

    if-eqz v0, :cond_6

    const v0, 0x396214a

    iget-object v1, p0, Lcom/google/a/a/a/a/ak;->h:Lcom/google/a/a/a/a/kj;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/ak;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
