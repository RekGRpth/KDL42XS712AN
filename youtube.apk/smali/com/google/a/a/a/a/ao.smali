.class public final Lcom/google/a/a/a/a/ao;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/ao;


# instance fields
.field public b:Lcom/google/a/a/a/a/jh;

.field public c:Lcom/google/a/a/a/a/ji;

.field public d:Lcom/google/a/a/a/a/uu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/ao;

    sput-object v0, Lcom/google/a/a/a/a/ao;->a:[Lcom/google/a/a/a/a/ao;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ao;->b:Lcom/google/a/a/a/a/jh;

    iput-object v0, p0, Lcom/google/a/a/a/a/ao;->c:Lcom/google/a/a/a/a/ji;

    iput-object v0, p0, Lcom/google/a/a/a/a/ao;->d:Lcom/google/a/a/a/a/uu;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/ao;->b:Lcom/google/a/a/a/a/jh;

    if-eqz v1, :cond_0

    const v0, 0x2f5d38e

    iget-object v1, p0, Lcom/google/a/a/a/a/ao;->b:Lcom/google/a/a/a/a/jh;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/ao;->c:Lcom/google/a/a/a/a/ji;

    if-eqz v1, :cond_1

    const v1, 0x2f5d39b

    iget-object v2, p0, Lcom/google/a/a/a/a/ao;->c:Lcom/google/a/a/a/a/ji;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/ao;->d:Lcom/google/a/a/a/a/uu;

    if-eqz v1, :cond_2

    const v1, 0x2f5d66a

    iget-object v2, p0, Lcom/google/a/a/a/a/ao;->d:Lcom/google/a/a/a/a/uu;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/ao;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/ao;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/ao;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/ao;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/ao;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/jh;

    invoke-direct {v0}, Lcom/google/a/a/a/a/jh;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ao;->b:Lcom/google/a/a/a/a/jh;

    iget-object v0, p0, Lcom/google/a/a/a/a/ao;->b:Lcom/google/a/a/a/a/jh;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/ji;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ji;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ao;->c:Lcom/google/a/a/a/a/ji;

    iget-object v0, p0, Lcom/google/a/a/a/a/ao;->c:Lcom/google/a/a/a/a/ji;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/uu;

    invoke-direct {v0}, Lcom/google/a/a/a/a/uu;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ao;->d:Lcom/google/a/a/a/a/uu;

    iget-object v0, p0, Lcom/google/a/a/a/a/ao;->d:Lcom/google/a/a/a/a/uu;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x17ae9c72 -> :sswitch_1
        0x17ae9cda -> :sswitch_2
        0x17aeb352 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/ao;->b:Lcom/google/a/a/a/a/jh;

    if-eqz v0, :cond_0

    const v0, 0x2f5d38e

    iget-object v1, p0, Lcom/google/a/a/a/a/ao;->b:Lcom/google/a/a/a/a/jh;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/ao;->c:Lcom/google/a/a/a/a/ji;

    if-eqz v0, :cond_1

    const v0, 0x2f5d39b

    iget-object v1, p0, Lcom/google/a/a/a/a/ao;->c:Lcom/google/a/a/a/a/ji;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/ao;->d:Lcom/google/a/a/a/a/uu;

    if-eqz v0, :cond_2

    const v0, 0x2f5d66a

    iget-object v1, p0, Lcom/google/a/a/a/a/ao;->d:Lcom/google/a/a/a/a/uu;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/ao;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
