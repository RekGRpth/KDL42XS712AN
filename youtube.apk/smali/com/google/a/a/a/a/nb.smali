.class public final Lcom/google/a/a/a/a/nb;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/nb;


# instance fields
.field public b:Lcom/google/a/a/a/a/tb;

.field public c:Lcom/google/a/a/a/a/tb;

.field public d:Lcom/google/a/a/a/a/tb;

.field public e:Lcom/google/a/a/a/a/tb;

.field public f:Lcom/google/a/a/a/a/tb;

.field public g:Lcom/google/a/a/a/a/tb;

.field public h:Lcom/google/a/a/a/a/tb;

.field public i:Lcom/google/a/a/a/a/tb;

.field public j:Lcom/google/a/a/a/a/tb;

.field public k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/nb;

    sput-object v0, Lcom/google/a/a/a/a/nb;->a:[Lcom/google/a/a/a/a/nb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->b:Lcom/google/a/a/a/a/tb;

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->c:Lcom/google/a/a/a/a/tb;

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->d:Lcom/google/a/a/a/a/tb;

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->e:Lcom/google/a/a/a/a/tb;

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->f:Lcom/google/a/a/a/a/tb;

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->g:Lcom/google/a/a/a/a/tb;

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->h:Lcom/google/a/a/a/a/tb;

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->i:Lcom/google/a/a/a/a/tb;

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->j:Lcom/google/a/a/a/a/tb;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/nb;->k:Z

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->b:Lcom/google/a/a/a/a/tb;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->b:Lcom/google/a/a/a/a/tb;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->c:Lcom/google/a/a/a/a/tb;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/a/a/a/a/nb;->c:Lcom/google/a/a/a/a/tb;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->d:Lcom/google/a/a/a/a/tb;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/a/a/a/a/nb;->d:Lcom/google/a/a/a/a/tb;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->e:Lcom/google/a/a/a/a/tb;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/a/a/a/a/nb;->e:Lcom/google/a/a/a/a/tb;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->f:Lcom/google/a/a/a/a/tb;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/a/a/a/a/nb;->f:Lcom/google/a/a/a/a/tb;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->g:Lcom/google/a/a/a/a/tb;

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/a/a/a/a/nb;->g:Lcom/google/a/a/a/a/tb;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->h:Lcom/google/a/a/a/a/tb;

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/a/a/a/a/nb;->h:Lcom/google/a/a/a/a/tb;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->i:Lcom/google/a/a/a/a/tb;

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/a/a/a/a/nb;->i:Lcom/google/a/a/a/a/tb;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->j:Lcom/google/a/a/a/a/tb;

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/a/a/a/a/nb;->j:Lcom/google/a/a/a/a/tb;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-boolean v1, p0, Lcom/google/a/a/a/a/nb;->k:Z

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/a/a/a/a/nb;->k:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/nb;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/nb;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/tb;

    invoke-direct {v0}, Lcom/google/a/a/a/a/tb;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->b:Lcom/google/a/a/a/a/tb;

    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->b:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/tb;

    invoke-direct {v0}, Lcom/google/a/a/a/a/tb;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->c:Lcom/google/a/a/a/a/tb;

    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->c:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/tb;

    invoke-direct {v0}, Lcom/google/a/a/a/a/tb;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->d:Lcom/google/a/a/a/a/tb;

    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->d:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/tb;

    invoke-direct {v0}, Lcom/google/a/a/a/a/tb;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->e:Lcom/google/a/a/a/a/tb;

    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->e:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/tb;

    invoke-direct {v0}, Lcom/google/a/a/a/a/tb;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->f:Lcom/google/a/a/a/a/tb;

    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->f:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/a/a/a/a/tb;

    invoke-direct {v0}, Lcom/google/a/a/a/a/tb;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->g:Lcom/google/a/a/a/a/tb;

    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->g:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_7
    new-instance v0, Lcom/google/a/a/a/a/tb;

    invoke-direct {v0}, Lcom/google/a/a/a/a/tb;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->h:Lcom/google/a/a/a/a/tb;

    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->h:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_8
    new-instance v0, Lcom/google/a/a/a/a/tb;

    invoke-direct {v0}, Lcom/google/a/a/a/a/tb;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->i:Lcom/google/a/a/a/a/tb;

    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->i:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_9
    new-instance v0, Lcom/google/a/a/a/a/tb;

    invoke-direct {v0}, Lcom/google/a/a/a/a/tb;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/nb;->j:Lcom/google/a/a/a/a/tb;

    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->j:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/nb;->k:Z

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->b:Lcom/google/a/a/a/a/tb;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->b:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->c:Lcom/google/a/a/a/a/tb;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->c:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->d:Lcom/google/a/a/a/a/tb;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->d:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->e:Lcom/google/a/a/a/a/tb;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->e:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->f:Lcom/google/a/a/a/a/tb;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->f:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->g:Lcom/google/a/a/a/a/tb;

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->g:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->h:Lcom/google/a/a/a/a/tb;

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->h:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->i:Lcom/google/a/a/a/a/tb;

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->i:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_7
    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->j:Lcom/google/a/a/a/a/tb;

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/a/a/a/a/nb;->j:Lcom/google/a/a/a/a/tb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_8
    iget-boolean v0, p0, Lcom/google/a/a/a/a/nb;->k:Z

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/a/a/a/a/nb;->k:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_9
    iget-object v0, p0, Lcom/google/a/a/a/a/nb;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
