.class public final Lcom/google/a/a/a/a/ba;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/ba;


# instance fields
.field public b:[Lcom/google/a/a/a/a/jk;

.field public c:Lcom/google/a/a/a/a/ay;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Lcom/google/a/a/a/a/kz;

.field public g:Lcom/google/a/a/a/a/sx;

.field public h:Lcom/google/a/a/a/a/sx;

.field public i:I

.field public j:[Lcom/google/a/a/a/a/ak;

.field public k:Lcom/google/a/a/a/a/aw;

.field public l:Lcom/google/a/a/a/a/ax;

.field public m:I

.field public n:Lcom/google/a/a/a/a/bc;

.field public o:[Lcom/google/a/a/a/a/bd;

.field public p:Lcom/google/a/a/a/a/vc;

.field public q:Lcom/google/a/a/a/a/az;

.field public r:Lcom/google/a/a/a/a/fk;

.field public s:Lcom/google/a/a/a/a/bb;

.field public t:Lcom/google/a/a/a/a/sx;

.field public u:Lcom/google/a/a/a/a/sx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/ba;

    sput-object v0, Lcom/google/a/a/a/a/ba;->a:[Lcom/google/a/a/a/a/ba;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    sget-object v0, Lcom/google/a/a/a/a/jk;->a:[Lcom/google/a/a/a/a/jk;

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->b:[Lcom/google/a/a/a/a/jk;

    iput-object v1, p0, Lcom/google/a/a/a/a/ba;->c:Lcom/google/a/a/a/a/ay;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->e:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/a/a/a/a/ba;->f:Lcom/google/a/a/a/a/kz;

    iput-object v1, p0, Lcom/google/a/a/a/a/ba;->g:Lcom/google/a/a/a/a/sx;

    iput-object v1, p0, Lcom/google/a/a/a/a/ba;->h:Lcom/google/a/a/a/a/sx;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/a/a/a/a/ba;->i:I

    sget-object v0, Lcom/google/a/a/a/a/ak;->a:[Lcom/google/a/a/a/a/ak;

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->j:[Lcom/google/a/a/a/a/ak;

    iput-object v1, p0, Lcom/google/a/a/a/a/ba;->k:Lcom/google/a/a/a/a/aw;

    iput-object v1, p0, Lcom/google/a/a/a/a/ba;->l:Lcom/google/a/a/a/a/ax;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/a/a/a/a/ba;->m:I

    iput-object v1, p0, Lcom/google/a/a/a/a/ba;->n:Lcom/google/a/a/a/a/bc;

    sget-object v0, Lcom/google/a/a/a/a/bd;->a:[Lcom/google/a/a/a/a/bd;

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->o:[Lcom/google/a/a/a/a/bd;

    iput-object v1, p0, Lcom/google/a/a/a/a/ba;->p:Lcom/google/a/a/a/a/vc;

    iput-object v1, p0, Lcom/google/a/a/a/a/ba;->q:Lcom/google/a/a/a/a/az;

    iput-object v1, p0, Lcom/google/a/a/a/a/ba;->r:Lcom/google/a/a/a/a/fk;

    iput-object v1, p0, Lcom/google/a/a/a/a/ba;->s:Lcom/google/a/a/a/a/bb;

    iput-object v1, p0, Lcom/google/a/a/a/a/ba;->t:Lcom/google/a/a/a/a/sx;

    iput-object v1, p0, Lcom/google/a/a/a/a/ba;->u:Lcom/google/a/a/a/a/sx;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->b:[Lcom/google/a/a/a/a/jk;

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/google/a/a/a/a/ba;->b:[Lcom/google/a/a/a/a/jk;

    array-length v5, v4

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v3, v4, v2

    const/4 v6, 0x2

    invoke-static {v6, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    :cond_0
    move v0, v1

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->c:Lcom/google/a/a/a/a/ay;

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/a/a/a/a/ba;->c:Lcom/google/a/a/a/a/ay;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/a/a/a/a/ba;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->e:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/a/a/a/a/ba;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->f:Lcom/google/a/a/a/a/kz;

    if-eqz v2, :cond_5

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/a/a/a/a/ba;->f:Lcom/google/a/a/a/a/kz;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->g:Lcom/google/a/a/a/a/sx;

    if-eqz v2, :cond_6

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/a/a/a/a/ba;->g:Lcom/google/a/a/a/a/sx;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->h:Lcom/google/a/a/a/a/sx;

    if-eqz v2, :cond_7

    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/a/a/a/a/ba;->h:Lcom/google/a/a/a/a/sx;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget v2, p0, Lcom/google/a/a/a/a/ba;->i:I

    if-eqz v2, :cond_8

    const/16 v2, 0xc

    iget v3, p0, Lcom/google/a/a/a/a/ba;->i:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->j:[Lcom/google/a/a/a/a/ak;

    if-eqz v2, :cond_9

    iget-object v4, p0, Lcom/google/a/a/a/a/ba;->j:[Lcom/google/a/a/a/a/ak;

    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_9

    aget-object v3, v4, v2

    const/16 v6, 0xd

    invoke-static {v6, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_1

    :cond_9
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->k:Lcom/google/a/a/a/a/aw;

    if-eqz v2, :cond_a

    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/a/a/a/a/ba;->k:Lcom/google/a/a/a/a/aw;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_a
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->l:Lcom/google/a/a/a/a/ax;

    if-eqz v2, :cond_b

    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/a/a/a/a/ba;->l:Lcom/google/a/a/a/a/ax;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_b
    iget v2, p0, Lcom/google/a/a/a/a/ba;->m:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_c

    const/16 v2, 0x11

    iget v3, p0, Lcom/google/a/a/a/a/ba;->m:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_c
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->n:Lcom/google/a/a/a/a/bc;

    if-eqz v2, :cond_d

    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/a/a/a/a/ba;->n:Lcom/google/a/a/a/a/bc;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_d
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->o:[Lcom/google/a/a/a/a/bd;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->o:[Lcom/google/a/a/a/a/bd;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_e

    aget-object v4, v2, v1

    const/16 v5, 0x13

    invoke-static {v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_e
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->p:Lcom/google/a/a/a/a/vc;

    if-eqz v1, :cond_f

    const/16 v1, 0x14

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->p:Lcom/google/a/a/a/a/vc;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->q:Lcom/google/a/a/a/a/az;

    if-eqz v1, :cond_10

    const/16 v1, 0x15

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->q:Lcom/google/a/a/a/a/az;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->r:Lcom/google/a/a/a/a/fk;

    if-eqz v1, :cond_11

    const/16 v1, 0x16

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->r:Lcom/google/a/a/a/a/fk;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->s:Lcom/google/a/a/a/a/bb;

    if-eqz v1, :cond_12

    const/16 v1, 0x17

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->s:Lcom/google/a/a/a/a/bb;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->t:Lcom/google/a/a/a/a/sx;

    if-eqz v1, :cond_13

    const/16 v1, 0x18

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->t:Lcom/google/a/a/a/a/sx;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_13
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->u:Lcom/google/a/a/a/a/sx;

    if-eqz v1, :cond_14

    const/16 v1, 0x19

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->u:Lcom/google/a/a/a/a/sx;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_14
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/ba;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->dl:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/a/a/a/a/ba;->dl:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->dl:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->b:[Lcom/google/a/a/a/a/jk;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/jk;

    iget-object v3, p0, Lcom/google/a/a/a/a/ba;->b:[Lcom/google/a/a/a/a/jk;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/a/a/a/a/ba;->b:[Lcom/google/a/a/a/a/jk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lcom/google/a/a/a/a/ba;->b:[Lcom/google/a/a/a/a/jk;

    :goto_2
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->b:[Lcom/google/a/a/a/a/jk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->b:[Lcom/google/a/a/a/a/jk;

    new-instance v3, Lcom/google/a/a/a/a/jk;

    invoke-direct {v3}, Lcom/google/a/a/a/a/jk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->b:[Lcom/google/a/a/a/a/jk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->b:[Lcom/google/a/a/a/a/jk;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->b:[Lcom/google/a/a/a/a/jk;

    new-instance v3, Lcom/google/a/a/a/a/jk;

    invoke-direct {v3}, Lcom/google/a/a/a/a/jk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->b:[Lcom/google/a/a/a/a/jk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/ay;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->c:Lcom/google/a/a/a/a/ay;

    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->c:Lcom/google/a/a/a/a/ay;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/kz;

    invoke-direct {v0}, Lcom/google/a/a/a/a/kz;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->f:Lcom/google/a/a/a/a/kz;

    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->f:Lcom/google/a/a/a/a/kz;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/a/a/a/a/sx;

    invoke-direct {v0}, Lcom/google/a/a/a/a/sx;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->g:Lcom/google/a/a/a/a/sx;

    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->g:Lcom/google/a/a/a/a/sx;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_7
    new-instance v0, Lcom/google/a/a/a/a/sx;

    invoke-direct {v0}, Lcom/google/a/a/a/a/sx;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->h:Lcom/google/a/a/a/a/sx;

    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->h:Lcom/google/a/a/a/a/sx;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    iput v0, p0, Lcom/google/a/a/a/a/ba;->i:I

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x6a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->j:[Lcom/google/a/a/a/a/ak;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/ak;

    iget-object v3, p0, Lcom/google/a/a/a/a/ba;->j:[Lcom/google/a/a/a/a/ak;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/a/a/a/a/ba;->j:[Lcom/google/a/a/a/a/ak;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Lcom/google/a/a/a/a/ba;->j:[Lcom/google/a/a/a/a/ak;

    :goto_4
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->j:[Lcom/google/a/a/a/a/ak;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->j:[Lcom/google/a/a/a/a/ak;

    new-instance v3, Lcom/google/a/a/a/a/ak;

    invoke-direct {v3}, Lcom/google/a/a/a/a/ak;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->j:[Lcom/google/a/a/a/a/ak;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->j:[Lcom/google/a/a/a/a/ak;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->j:[Lcom/google/a/a/a/a/ak;

    new-instance v3, Lcom/google/a/a/a/a/ak;

    invoke-direct {v3}, Lcom/google/a/a/a/a/ak;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->j:[Lcom/google/a/a/a/a/ak;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_a
    new-instance v0, Lcom/google/a/a/a/a/aw;

    invoke-direct {v0}, Lcom/google/a/a/a/a/aw;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->k:Lcom/google/a/a/a/a/aw;

    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->k:Lcom/google/a/a/a/a/aw;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_b
    new-instance v0, Lcom/google/a/a/a/a/ax;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ax;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->l:Lcom/google/a/a/a/a/ax;

    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->l:Lcom/google/a/a/a/a/ax;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    if-eq v0, v4, :cond_8

    const/4 v2, 0x2

    if-ne v0, v2, :cond_9

    :cond_8
    iput v0, p0, Lcom/google/a/a/a/a/ba;->m:I

    goto/16 :goto_0

    :cond_9
    iput v4, p0, Lcom/google/a/a/a/a/ba;->m:I

    goto/16 :goto_0

    :sswitch_d
    new-instance v0, Lcom/google/a/a/a/a/bc;

    invoke-direct {v0}, Lcom/google/a/a/a/a/bc;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->n:Lcom/google/a/a/a/a/bc;

    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->n:Lcom/google/a/a/a/a/bc;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_e
    const/16 v0, 0x9a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->o:[Lcom/google/a/a/a/a/bd;

    if-nez v0, :cond_b

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/bd;

    iget-object v3, p0, Lcom/google/a/a/a/a/ba;->o:[Lcom/google/a/a/a/a/bd;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/a/a/a/a/ba;->o:[Lcom/google/a/a/a/a/bd;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    iput-object v2, p0, Lcom/google/a/a/a/a/ba;->o:[Lcom/google/a/a/a/a/bd;

    :goto_6
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->o:[Lcom/google/a/a/a/a/bd;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->o:[Lcom/google/a/a/a/a/bd;

    new-instance v3, Lcom/google/a/a/a/a/bd;

    invoke-direct {v3}, Lcom/google/a/a/a/a/bd;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->o:[Lcom/google/a/a/a/a/bd;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_b
    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->o:[Lcom/google/a/a/a/a/bd;

    array-length v0, v0

    goto :goto_5

    :cond_c
    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->o:[Lcom/google/a/a/a/a/bd;

    new-instance v3, Lcom/google/a/a/a/a/bd;

    invoke-direct {v3}, Lcom/google/a/a/a/a/bd;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->o:[Lcom/google/a/a/a/a/bd;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_f
    new-instance v0, Lcom/google/a/a/a/a/vc;

    invoke-direct {v0}, Lcom/google/a/a/a/a/vc;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->p:Lcom/google/a/a/a/a/vc;

    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->p:Lcom/google/a/a/a/a/vc;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_10
    new-instance v0, Lcom/google/a/a/a/a/az;

    invoke-direct {v0}, Lcom/google/a/a/a/a/az;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->q:Lcom/google/a/a/a/a/az;

    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->q:Lcom/google/a/a/a/a/az;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_11
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->r:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->r:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_12
    new-instance v0, Lcom/google/a/a/a/a/bb;

    invoke-direct {v0}, Lcom/google/a/a/a/a/bb;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->s:Lcom/google/a/a/a/a/bb;

    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->s:Lcom/google/a/a/a/a/bb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_13
    new-instance v0, Lcom/google/a/a/a/a/sx;

    invoke-direct {v0}, Lcom/google/a/a/a/a/sx;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->t:Lcom/google/a/a/a/a/sx;

    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->t:Lcom/google/a/a/a/a/sx;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_14
    new-instance v0, Lcom/google/a/a/a/a/sx;

    invoke-direct {v0}, Lcom/google/a/a/a/a/sx;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ba;->u:Lcom/google/a/a/a/a/sx;

    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->u:Lcom/google/a/a/a/a/sx;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
        0x4a -> :sswitch_6
        0x52 -> :sswitch_7
        0x60 -> :sswitch_8
        0x6a -> :sswitch_9
        0x72 -> :sswitch_a
        0x7a -> :sswitch_b
        0x88 -> :sswitch_c
        0x92 -> :sswitch_d
        0x9a -> :sswitch_e
        0xa2 -> :sswitch_f
        0xaa -> :sswitch_10
        0xb2 -> :sswitch_11
        0xba -> :sswitch_12
        0xc2 -> :sswitch_13
        0xca -> :sswitch_14
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->b:[Lcom/google/a/a/a/a/jk;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->b:[Lcom/google/a/a/a/a/jk;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->c:Lcom/google/a/a/a/a/ay;

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->c:Lcom/google/a/a/a/a/ay;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->e:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->f:Lcom/google/a/a/a/a/kz;

    if-eqz v1, :cond_4

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->f:Lcom/google/a/a/a/a/kz;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->g:Lcom/google/a/a/a/a/sx;

    if-eqz v1, :cond_5

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->g:Lcom/google/a/a/a/a/sx;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->h:Lcom/google/a/a/a/a/sx;

    if-eqz v1, :cond_6

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->h:Lcom/google/a/a/a/a/sx;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_6
    iget v1, p0, Lcom/google/a/a/a/a/ba;->i:I

    if-eqz v1, :cond_7

    const/16 v1, 0xc

    iget v2, p0, Lcom/google/a/a/a/a/ba;->i:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_7
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->j:[Lcom/google/a/a/a/a/ak;

    if-eqz v1, :cond_8

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->j:[Lcom/google/a/a/a/a/ak;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    const/16 v5, 0xd

    invoke-virtual {p1, v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_8
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->k:Lcom/google/a/a/a/a/aw;

    if-eqz v1, :cond_9

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->k:Lcom/google/a/a/a/a/aw;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_9
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->l:Lcom/google/a/a/a/a/ax;

    if-eqz v1, :cond_a

    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->l:Lcom/google/a/a/a/a/ax;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_a
    iget v1, p0, Lcom/google/a/a/a/a/ba;->m:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_b

    const/16 v1, 0x11

    iget v2, p0, Lcom/google/a/a/a/a/ba;->m:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_b
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->n:Lcom/google/a/a/a/a/bc;

    if-eqz v1, :cond_c

    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/a/a/a/a/ba;->n:Lcom/google/a/a/a/a/bc;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_c
    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->o:[Lcom/google/a/a/a/a/bd;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->o:[Lcom/google/a/a/a/a/bd;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_d

    aget-object v3, v1, v0

    const/16 v4, 0x13

    invoke-virtual {p1, v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_d
    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->p:Lcom/google/a/a/a/a/vc;

    if-eqz v0, :cond_e

    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->p:Lcom/google/a/a/a/a/vc;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_e
    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->q:Lcom/google/a/a/a/a/az;

    if-eqz v0, :cond_f

    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->q:Lcom/google/a/a/a/a/az;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_f
    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->r:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_10

    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->r:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_10
    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->s:Lcom/google/a/a/a/a/bb;

    if-eqz v0, :cond_11

    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->s:Lcom/google/a/a/a/a/bb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_11
    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->t:Lcom/google/a/a/a/a/sx;

    if-eqz v0, :cond_12

    const/16 v0, 0x18

    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->t:Lcom/google/a/a/a/a/sx;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_12
    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->u:Lcom/google/a/a/a/a/sx;

    if-eqz v0, :cond_13

    const/16 v0, 0x19

    iget-object v1, p0, Lcom/google/a/a/a/a/ba;->u:Lcom/google/a/a/a/a/sx;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_13
    iget-object v0, p0, Lcom/google/a/a/a/a/ba;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
