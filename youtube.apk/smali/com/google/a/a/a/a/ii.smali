.class public final Lcom/google/a/a/a/a/ii;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/ii;


# instance fields
.field public b:Lcom/google/a/a/a/a/ct;

.field public c:Lcom/google/a/a/a/a/tx;

.field public d:Lcom/google/a/a/a/a/bg;

.field public e:Lcom/google/a/a/a/a/pv;

.field public f:Lcom/google/a/a/a/a/cs;

.field public g:Lcom/google/a/a/a/a/su;

.field public h:Lcom/google/a/a/a/a/ct;

.field public i:Lcom/google/a/a/a/a/o;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/ii;

    sput-object v0, Lcom/google/a/a/a/a/ii;->a:[Lcom/google/a/a/a/a/ii;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ii;->b:Lcom/google/a/a/a/a/ct;

    iput-object v0, p0, Lcom/google/a/a/a/a/ii;->c:Lcom/google/a/a/a/a/tx;

    iput-object v0, p0, Lcom/google/a/a/a/a/ii;->d:Lcom/google/a/a/a/a/bg;

    iput-object v0, p0, Lcom/google/a/a/a/a/ii;->e:Lcom/google/a/a/a/a/pv;

    iput-object v0, p0, Lcom/google/a/a/a/a/ii;->f:Lcom/google/a/a/a/a/cs;

    iput-object v0, p0, Lcom/google/a/a/a/a/ii;->g:Lcom/google/a/a/a/a/su;

    iput-object v0, p0, Lcom/google/a/a/a/a/ii;->h:Lcom/google/a/a/a/a/ct;

    iput-object v0, p0, Lcom/google/a/a/a/a/ii;->i:Lcom/google/a/a/a/a/o;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->b:Lcom/google/a/a/a/a/ct;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->b:Lcom/google/a/a/a/a/ct;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->c:Lcom/google/a/a/a/a/tx;

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/a/a/a/a/ii;->c:Lcom/google/a/a/a/a/tx;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->d:Lcom/google/a/a/a/a/bg;

    if-eqz v1, :cond_2

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/a/a/a/a/ii;->d:Lcom/google/a/a/a/a/bg;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->e:Lcom/google/a/a/a/a/pv;

    if-eqz v1, :cond_3

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/a/a/a/a/ii;->e:Lcom/google/a/a/a/a/pv;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->f:Lcom/google/a/a/a/a/cs;

    if-eqz v1, :cond_4

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/a/a/a/a/ii;->f:Lcom/google/a/a/a/a/cs;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->g:Lcom/google/a/a/a/a/su;

    if-eqz v1, :cond_5

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/a/a/a/a/ii;->g:Lcom/google/a/a/a/a/su;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->h:Lcom/google/a/a/a/a/ct;

    if-eqz v1, :cond_6

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/a/a/a/a/ii;->h:Lcom/google/a/a/a/a/ct;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->i:Lcom/google/a/a/a/a/o;

    if-eqz v1, :cond_7

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/a/a/a/a/ii;->i:Lcom/google/a/a/a/a/o;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/ii;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/ii;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/ct;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ct;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ii;->b:Lcom/google/a/a/a/a/ct;

    iget-object v0, p0, Lcom/google/a/a/a/a/ii;->b:Lcom/google/a/a/a/a/ct;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/tx;

    invoke-direct {v0}, Lcom/google/a/a/a/a/tx;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ii;->c:Lcom/google/a/a/a/a/tx;

    iget-object v0, p0, Lcom/google/a/a/a/a/ii;->c:Lcom/google/a/a/a/a/tx;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/bg;

    invoke-direct {v0}, Lcom/google/a/a/a/a/bg;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ii;->d:Lcom/google/a/a/a/a/bg;

    iget-object v0, p0, Lcom/google/a/a/a/a/ii;->d:Lcom/google/a/a/a/a/bg;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/pv;

    invoke-direct {v0}, Lcom/google/a/a/a/a/pv;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ii;->e:Lcom/google/a/a/a/a/pv;

    iget-object v0, p0, Lcom/google/a/a/a/a/ii;->e:Lcom/google/a/a/a/a/pv;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/cs;

    invoke-direct {v0}, Lcom/google/a/a/a/a/cs;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ii;->f:Lcom/google/a/a/a/a/cs;

    iget-object v0, p0, Lcom/google/a/a/a/a/ii;->f:Lcom/google/a/a/a/a/cs;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/a/a/a/a/su;

    invoke-direct {v0}, Lcom/google/a/a/a/a/su;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ii;->g:Lcom/google/a/a/a/a/su;

    iget-object v0, p0, Lcom/google/a/a/a/a/ii;->g:Lcom/google/a/a/a/a/su;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_7
    new-instance v0, Lcom/google/a/a/a/a/ct;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ct;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ii;->h:Lcom/google/a/a/a/a/ct;

    iget-object v0, p0, Lcom/google/a/a/a/a/ii;->h:Lcom/google/a/a/a/a/ct;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_8
    new-instance v0, Lcom/google/a/a/a/a/o;

    invoke-direct {v0}, Lcom/google/a/a/a/a/o;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ii;->i:Lcom/google/a/a/a/a/o;

    iget-object v0, p0, Lcom/google/a/a/a/a/ii;->i:Lcom/google/a/a/a/a/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/ii;->b:Lcom/google/a/a/a/a/ct;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->b:Lcom/google/a/a/a/a/ct;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/ii;->c:Lcom/google/a/a/a/a/tx;

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->c:Lcom/google/a/a/a/a/tx;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/ii;->d:Lcom/google/a/a/a/a/bg;

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->d:Lcom/google/a/a/a/a/bg;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/ii;->e:Lcom/google/a/a/a/a/pv;

    if-eqz v0, :cond_3

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->e:Lcom/google/a/a/a/a/pv;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/ii;->f:Lcom/google/a/a/a/a/cs;

    if-eqz v0, :cond_4

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->f:Lcom/google/a/a/a/a/cs;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/ii;->g:Lcom/google/a/a/a/a/su;

    if-eqz v0, :cond_5

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->g:Lcom/google/a/a/a/a/su;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/ii;->h:Lcom/google/a/a/a/a/ct;

    if-eqz v0, :cond_6

    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->h:Lcom/google/a/a/a/a/ct;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/ii;->i:Lcom/google/a/a/a/a/o;

    if-eqz v0, :cond_7

    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/a/a/a/a/ii;->i:Lcom/google/a/a/a/a/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_7
    iget-object v0, p0, Lcom/google/a/a/a/a/ii;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
