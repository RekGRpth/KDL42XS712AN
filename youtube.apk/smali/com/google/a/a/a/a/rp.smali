.class public final Lcom/google/a/a/a/a/rp;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/rp;


# instance fields
.field public b:Lcom/google/a/a/a/a/rt;

.field public c:Lcom/google/a/a/a/a/rs;

.field public d:Lcom/google/a/a/a/a/rq;

.field public e:Lcom/google/a/a/a/a/rr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/rp;

    sput-object v0, Lcom/google/a/a/a/a/rp;->a:[Lcom/google/a/a/a/a/rp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/rp;->b:Lcom/google/a/a/a/a/rt;

    iput-object v0, p0, Lcom/google/a/a/a/a/rp;->c:Lcom/google/a/a/a/a/rs;

    iput-object v0, p0, Lcom/google/a/a/a/a/rp;->d:Lcom/google/a/a/a/a/rq;

    iput-object v0, p0, Lcom/google/a/a/a/a/rp;->e:Lcom/google/a/a/a/a/rr;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/rp;->b:Lcom/google/a/a/a/a/rt;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/rp;->b:Lcom/google/a/a/a/a/rt;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/rp;->c:Lcom/google/a/a/a/a/rs;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/a/a/a/a/rp;->c:Lcom/google/a/a/a/a/rs;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/rp;->d:Lcom/google/a/a/a/a/rq;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/a/a/a/a/rp;->d:Lcom/google/a/a/a/a/rq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/rp;->e:Lcom/google/a/a/a/a/rr;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/a/a/a/a/rp;->e:Lcom/google/a/a/a/a/rr;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/rp;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/rp;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/rp;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/rp;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/rp;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/rt;

    invoke-direct {v0}, Lcom/google/a/a/a/a/rt;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/rp;->b:Lcom/google/a/a/a/a/rt;

    iget-object v0, p0, Lcom/google/a/a/a/a/rp;->b:Lcom/google/a/a/a/a/rt;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/rs;

    invoke-direct {v0}, Lcom/google/a/a/a/a/rs;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/rp;->c:Lcom/google/a/a/a/a/rs;

    iget-object v0, p0, Lcom/google/a/a/a/a/rp;->c:Lcom/google/a/a/a/a/rs;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/rq;

    invoke-direct {v0}, Lcom/google/a/a/a/a/rq;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/rp;->d:Lcom/google/a/a/a/a/rq;

    iget-object v0, p0, Lcom/google/a/a/a/a/rp;->d:Lcom/google/a/a/a/a/rq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/rr;

    invoke-direct {v0}, Lcom/google/a/a/a/a/rr;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/rp;->e:Lcom/google/a/a/a/a/rr;

    iget-object v0, p0, Lcom/google/a/a/a/a/rp;->e:Lcom/google/a/a/a/a/rr;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/rp;->b:Lcom/google/a/a/a/a/rt;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/rp;->b:Lcom/google/a/a/a/a/rt;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/rp;->c:Lcom/google/a/a/a/a/rs;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/a/a/a/a/rp;->c:Lcom/google/a/a/a/a/rs;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/rp;->d:Lcom/google/a/a/a/a/rq;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/a/a/a/a/rp;->d:Lcom/google/a/a/a/a/rq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/rp;->e:Lcom/google/a/a/a/a/rr;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/a/a/a/a/rp;->e:Lcom/google/a/a/a/a/rr;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/rp;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
