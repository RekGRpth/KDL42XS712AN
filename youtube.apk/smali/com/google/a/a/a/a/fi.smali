.class public final Lcom/google/a/a/a/a/fi;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/fi;


# instance fields
.field public b:I

.field public c:I

.field public d:I

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:[Ljava/lang/String;

.field public l:I

.field public m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/fi;

    sput-object v0, Lcom/google/a/a/a/a/fi;->a:[Lcom/google/a/a/a/a/fi;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput v1, p0, Lcom/google/a/a/a/a/fi;->b:I

    iput v1, p0, Lcom/google/a/a/a/a/fi;->c:I

    iput v1, p0, Lcom/google/a/a/a/a/fi;->d:I

    iput-boolean v1, p0, Lcom/google/a/a/a/a/fi;->e:Z

    iput-boolean v1, p0, Lcom/google/a/a/a/a/fi;->f:Z

    iput-boolean v1, p0, Lcom/google/a/a/a/a/fi;->g:Z

    iput-boolean v1, p0, Lcom/google/a/a/a/a/fi;->h:Z

    iput-boolean v1, p0, Lcom/google/a/a/a/a/fi;->i:Z

    iput-boolean v1, p0, Lcom/google/a/a/a/a/fi;->j:Z

    sget-object v0, Lcom/google/protobuf/nano/f;->j:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/a/a/a/a/fi;->k:[Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/a/a/a/a/fi;->l:I

    iput-boolean v1, p0, Lcom/google/a/a/a/a/fi;->m:Z

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v6, 0x1

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/a/a/a/a/fi;->b:I

    if-eqz v0, :cond_c

    iget v0, p0, Lcom/google/a/a/a/a/fi;->b:I

    invoke-static {v6, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget v2, p0, Lcom/google/a/a/a/a/fi;->c:I

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget v3, p0, Lcom/google/a/a/a/a/fi;->c:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget v2, p0, Lcom/google/a/a/a/a/fi;->d:I

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget v3, p0, Lcom/google/a/a/a/a/fi;->d:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-boolean v2, p0, Lcom/google/a/a/a/a/fi;->e:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-boolean v3, p0, Lcom/google/a/a/a/a/fi;->e:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-boolean v2, p0, Lcom/google/a/a/a/a/fi;->f:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/a/a/a/a/fi;->f:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-boolean v2, p0, Lcom/google/a/a/a/a/fi;->g:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/a/a/a/a/fi;->g:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-boolean v2, p0, Lcom/google/a/a/a/a/fi;->h:Z

    if-eqz v2, :cond_5

    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/a/a/a/a/fi;->h:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-boolean v2, p0, Lcom/google/a/a/a/a/fi;->i:Z

    if-eqz v2, :cond_6

    const/16 v2, 0x8

    iget-boolean v3, p0, Lcom/google/a/a/a/a/fi;->i:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget-boolean v2, p0, Lcom/google/a/a/a/a/fi;->j:Z

    if-eqz v2, :cond_7

    const/16 v2, 0x9

    iget-boolean v3, p0, Lcom/google/a/a/a/a/fi;->j:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget-object v2, p0, Lcom/google/a/a/a/a/fi;->k:[Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/a/a/a/a/fi;->k:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_9

    iget-object v3, p0, Lcom/google/a/a/a/a/fi;->k:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_8

    aget-object v5, v3, v1

    invoke-static {v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_8
    add-int/2addr v0, v2

    iget-object v1, p0, Lcom/google/a/a/a/a/fi;->k:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_9
    iget v1, p0, Lcom/google/a/a/a/a/fi;->l:I

    if-eq v1, v6, :cond_a

    const/16 v1, 0xb

    iget v2, p0, Lcom/google/a/a/a/a/fi;->l:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-boolean v1, p0, Lcom/google/a/a/a/a/fi;->m:Z

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/google/a/a/a/a/fi;->m:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Lcom/google/a/a/a/a/fi;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/fi;->dm:I

    return v0

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/fi;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/fi;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/fi;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    if-eq v0, v7, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lcom/google/a/a/a/a/fi;->b:I

    goto :goto_0

    :cond_3
    iput v3, p0, Lcom/google/a/a/a/a/fi;->b:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    if-eqz v0, :cond_4

    if-eq v0, v4, :cond_4

    if-eq v0, v5, :cond_4

    if-eq v0, v6, :cond_4

    if-eq v0, v7, :cond_4

    const/4 v1, 0x5

    if-ne v0, v1, :cond_5

    :cond_4
    iput v0, p0, Lcom/google/a/a/a/a/fi;->c:I

    goto :goto_0

    :cond_5
    iput v3, p0, Lcom/google/a/a/a/a/fi;->c:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    if-eqz v0, :cond_6

    if-eq v0, v4, :cond_6

    if-eq v0, v5, :cond_6

    if-ne v0, v6, :cond_7

    :cond_6
    iput v0, p0, Lcom/google/a/a/a/a/fi;->d:I

    goto :goto_0

    :cond_7
    iput v3, p0, Lcom/google/a/a/a/a/fi;->d:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/fi;->e:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/fi;->f:Z

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/fi;->g:Z

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/fi;->h:Z

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/fi;->i:Z

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/fi;->j:Z

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x52

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v1

    iget-object v0, p0, Lcom/google/a/a/a/a/fi;->k:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/a/a/a/a/fi;->k:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lcom/google/a/a/a/a/fi;->k:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lcom/google/a/a/a/a/fi;->k:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_8

    iget-object v1, p0, Lcom/google/a/a/a/a/fi;->k:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_8
    iget-object v1, p0, Lcom/google/a/a/a/a/fi;->k:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    if-eq v0, v4, :cond_9

    if-eq v0, v5, :cond_9

    if-ne v0, v6, :cond_a

    :cond_9
    iput v0, p0, Lcom/google/a/a/a/a/fi;->l:I

    goto/16 :goto_0

    :cond_a
    iput v4, p0, Lcom/google/a/a/a/a/fi;->l:I

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/a/a/a/a/fi;->m:Z

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6

    const/4 v5, 0x1

    iget v0, p0, Lcom/google/a/a/a/a/fi;->b:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/a/a/a/a/fi;->b:I

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_0
    iget v0, p0, Lcom/google/a/a/a/a/fi;->c:I

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Lcom/google/a/a/a/a/fi;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_1
    iget v0, p0, Lcom/google/a/a/a/a/fi;->d:I

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Lcom/google/a/a/a/a/fi;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_2
    iget-boolean v0, p0, Lcom/google/a/a/a/a/fi;->e:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/a/a/a/a/fi;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_3
    iget-boolean v0, p0, Lcom/google/a/a/a/a/fi;->f:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/a/a/a/a/fi;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_4
    iget-boolean v0, p0, Lcom/google/a/a/a/a/fi;->g:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/a/a/a/a/fi;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_5
    iget-boolean v0, p0, Lcom/google/a/a/a/a/fi;->h:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/a/a/a/a/fi;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_6
    iget-boolean v0, p0, Lcom/google/a/a/a/a/fi;->i:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/a/a/a/a/fi;->i:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_7
    iget-boolean v0, p0, Lcom/google/a/a/a/a/fi;->j:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/a/a/a/a/fi;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_8
    iget-object v0, p0, Lcom/google/a/a/a/a/fi;->k:[Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v1, p0, Lcom/google/a/a/a/a/fi;->k:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    const/16 v4, 0xa

    invoke-virtual {p1, v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_9
    iget v0, p0, Lcom/google/a/a/a/a/fi;->l:I

    if-eq v0, v5, :cond_a

    const/16 v0, 0xb

    iget v1, p0, Lcom/google/a/a/a/a/fi;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_a
    iget-boolean v0, p0, Lcom/google/a/a/a/a/fi;->m:Z

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/google/a/a/a/a/fi;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(IZ)V

    :cond_b
    iget-object v0, p0, Lcom/google/a/a/a/a/fi;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
