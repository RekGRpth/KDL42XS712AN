.class public final Lcom/google/a/a/a/a/ho;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/ho;


# instance fields
.field public b:I

.field public c:Lcom/google/a/a/a/a/hp;

.field public d:I

.field public e:J

.field public f:J

.field public g:[Lcom/google/a/a/a/a/hq;

.field public h:[Lcom/google/a/a/a/a/ie;

.field public i:Lcom/google/a/a/a/a/hr;

.field public j:Lcom/google/a/a/a/a/hz;

.field public k:Lcom/google/a/a/a/a/hs;

.field public l:I

.field public m:Lcom/google/a/a/a/a/ib;

.field public n:Lcom/google/a/a/a/a/if;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/ho;

    sput-object v0, Lcom/google/a/a/a/a/ho;->a:[Lcom/google/a/a/a/a/ho;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput v2, p0, Lcom/google/a/a/a/a/ho;->b:I

    iput-object v1, p0, Lcom/google/a/a/a/a/ho;->c:Lcom/google/a/a/a/a/hp;

    iput v2, p0, Lcom/google/a/a/a/a/ho;->d:I

    iput-wide v3, p0, Lcom/google/a/a/a/a/ho;->e:J

    iput-wide v3, p0, Lcom/google/a/a/a/a/ho;->f:J

    sget-object v0, Lcom/google/a/a/a/a/hq;->a:[Lcom/google/a/a/a/a/hq;

    iput-object v0, p0, Lcom/google/a/a/a/a/ho;->g:[Lcom/google/a/a/a/a/hq;

    sget-object v0, Lcom/google/a/a/a/a/ie;->a:[Lcom/google/a/a/a/a/ie;

    iput-object v0, p0, Lcom/google/a/a/a/a/ho;->h:[Lcom/google/a/a/a/a/ie;

    iput-object v1, p0, Lcom/google/a/a/a/a/ho;->i:Lcom/google/a/a/a/a/hr;

    iput-object v1, p0, Lcom/google/a/a/a/a/ho;->j:Lcom/google/a/a/a/a/hz;

    iput-object v1, p0, Lcom/google/a/a/a/a/ho;->k:Lcom/google/a/a/a/a/hs;

    iput v2, p0, Lcom/google/a/a/a/a/ho;->l:I

    iput-object v1, p0, Lcom/google/a/a/a/a/ho;->m:Lcom/google/a/a/a/a/ib;

    iput-object v1, p0, Lcom/google/a/a/a/a/ho;->n:Lcom/google/a/a/a/a/if;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const-wide/16 v5, 0x0

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/a/a/a/a/ho;->b:I

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    iget v2, p0, Lcom/google/a/a/a/a/ho;->b:I

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->c:Lcom/google/a/a/a/a/hp;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/a/a/a/a/ho;->c:Lcom/google/a/a/a/a/hp;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget v2, p0, Lcom/google/a/a/a/a/ho;->d:I

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget v3, p0, Lcom/google/a/a/a/a/ho;->d:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-wide v2, p0, Lcom/google/a/a/a/a/ho;->e:J

    cmp-long v2, v2, v5

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-wide v3, p0, Lcom/google/a/a/a/a/ho;->e:J

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-wide v2, p0, Lcom/google/a/a/a/a/ho;->f:J

    cmp-long v2, v2, v5

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iget-wide v3, p0, Lcom/google/a/a/a/a/ho;->f:J

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->g:[Lcom/google/a/a/a/a/hq;

    if-eqz v2, :cond_4

    iget-object v4, p0, Lcom/google/a/a/a/a/ho;->g:[Lcom/google/a/a/a/a/hq;

    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_4

    aget-object v3, v4, v2

    const/4 v6, 0x6

    invoke-static {v6, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->h:[Lcom/google/a/a/a/a/ie;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->h:[Lcom/google/a/a/a/a/ie;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    const/4 v5, 0x7

    invoke-static {v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/ho;->i:Lcom/google/a/a/a/a/hr;

    if-eqz v1, :cond_6

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->i:Lcom/google/a/a/a/a/hr;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lcom/google/a/a/a/a/ho;->j:Lcom/google/a/a/a/a/hz;

    if-eqz v1, :cond_7

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->j:Lcom/google/a/a/a/a/hz;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lcom/google/a/a/a/a/ho;->k:Lcom/google/a/a/a/a/hs;

    if-eqz v1, :cond_8

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->k:Lcom/google/a/a/a/a/hs;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget v1, p0, Lcom/google/a/a/a/a/ho;->l:I

    if-eqz v1, :cond_9

    const/16 v1, 0xb

    iget v2, p0, Lcom/google/a/a/a/a/ho;->l:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lcom/google/a/a/a/a/ho;->m:Lcom/google/a/a/a/a/ib;

    if-eqz v1, :cond_a

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->m:Lcom/google/a/a/a/a/ib;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Lcom/google/a/a/a/a/ho;->n:Lcom/google/a/a/a/a/if;

    if-eqz v1, :cond_b

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->n:Lcom/google/a/a/a/a/if;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Lcom/google/a/a/a/a/ho;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/ho;->dm:I

    return v0

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->dl:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/a/a/a/a/ho;->dl:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->dl:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    :cond_2
    iput v0, p0, Lcom/google/a/a/a/a/ho;->b:I

    goto :goto_0

    :cond_3
    iput v1, p0, Lcom/google/a/a/a/a/ho;->b:I

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/hp;

    invoke-direct {v0}, Lcom/google/a/a/a/a/hp;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ho;->c:Lcom/google/a/a/a/a/hp;

    iget-object v0, p0, Lcom/google/a/a/a/a/ho;->c:Lcom/google/a/a/a/a/hp;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    iput v0, p0, Lcom/google/a/a/a/a/ho;->d:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/a/a/a/a/ho;->e:J

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/a/a/a/a/ho;->f:J

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/ho;->g:[Lcom/google/a/a/a/a/hq;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/hq;

    iget-object v3, p0, Lcom/google/a/a/a/a/ho;->g:[Lcom/google/a/a/a/a/hq;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/a/a/a/a/ho;->g:[Lcom/google/a/a/a/a/hq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Lcom/google/a/a/a/a/ho;->g:[Lcom/google/a/a/a/a/hq;

    :goto_2
    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->g:[Lcom/google/a/a/a/a/hq;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->g:[Lcom/google/a/a/a/a/hq;

    new-instance v3, Lcom/google/a/a/a/a/hq;

    invoke-direct {v3}, Lcom/google/a/a/a/a/hq;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->g:[Lcom/google/a/a/a/a/hq;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/ho;->g:[Lcom/google/a/a/a/a/hq;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->g:[Lcom/google/a/a/a/a/hq;

    new-instance v3, Lcom/google/a/a/a/a/hq;

    invoke-direct {v3}, Lcom/google/a/a/a/a/hq;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->g:[Lcom/google/a/a/a/a/hq;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/ho;->h:[Lcom/google/a/a/a/a/ie;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/ie;

    iget-object v3, p0, Lcom/google/a/a/a/a/ho;->h:[Lcom/google/a/a/a/a/ie;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/a/a/a/a/ho;->h:[Lcom/google/a/a/a/a/ie;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Lcom/google/a/a/a/a/ho;->h:[Lcom/google/a/a/a/a/ie;

    :goto_4
    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->h:[Lcom/google/a/a/a/a/ie;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->h:[Lcom/google/a/a/a/a/ie;

    new-instance v3, Lcom/google/a/a/a/a/ie;

    invoke-direct {v3}, Lcom/google/a/a/a/a/ie;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->h:[Lcom/google/a/a/a/a/ie;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lcom/google/a/a/a/a/ho;->h:[Lcom/google/a/a/a/a/ie;

    array-length v0, v0

    goto :goto_3

    :cond_9
    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->h:[Lcom/google/a/a/a/a/ie;

    new-instance v3, Lcom/google/a/a/a/a/ie;

    invoke-direct {v3}, Lcom/google/a/a/a/a/ie;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->h:[Lcom/google/a/a/a/a/ie;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_8
    new-instance v0, Lcom/google/a/a/a/a/hr;

    invoke-direct {v0}, Lcom/google/a/a/a/a/hr;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ho;->i:Lcom/google/a/a/a/a/hr;

    iget-object v0, p0, Lcom/google/a/a/a/a/ho;->i:Lcom/google/a/a/a/a/hr;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_9
    new-instance v0, Lcom/google/a/a/a/a/hz;

    invoke-direct {v0}, Lcom/google/a/a/a/a/hz;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ho;->j:Lcom/google/a/a/a/a/hz;

    iget-object v0, p0, Lcom/google/a/a/a/a/ho;->j:Lcom/google/a/a/a/a/hz;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_a
    new-instance v0, Lcom/google/a/a/a/a/hs;

    invoke-direct {v0}, Lcom/google/a/a/a/a/hs;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ho;->k:Lcom/google/a/a/a/a/hs;

    iget-object v0, p0, Lcom/google/a/a/a/a/ho;->k:Lcom/google/a/a/a/a/hs;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    if-eqz v0, :cond_a

    if-ne v0, v4, :cond_b

    :cond_a
    iput v0, p0, Lcom/google/a/a/a/a/ho;->l:I

    goto/16 :goto_0

    :cond_b
    iput v1, p0, Lcom/google/a/a/a/a/ho;->l:I

    goto/16 :goto_0

    :sswitch_c
    new-instance v0, Lcom/google/a/a/a/a/ib;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ib;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ho;->m:Lcom/google/a/a/a/a/ib;

    iget-object v0, p0, Lcom/google/a/a/a/a/ho;->m:Lcom/google/a/a/a/a/ib;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_d
    new-instance v0, Lcom/google/a/a/a/a/if;

    invoke-direct {v0}, Lcom/google/a/a/a/a/if;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ho;->n:Lcom/google/a/a/a/a/if;

    iget-object v0, p0, Lcom/google/a/a/a/a/ho;->n:Lcom/google/a/a/a/a/if;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/a/a/a/a/ho;->b:I

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/a/a/a/a/ho;->b:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/ho;->c:Lcom/google/a/a/a/a/hp;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->c:Lcom/google/a/a/a/a/hp;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget v1, p0, Lcom/google/a/a/a/a/ho;->d:I

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/a/a/a/a/ho;->d:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_2
    iget-wide v1, p0, Lcom/google/a/a/a/a/ho;->e:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/a/a/a/a/ho;->e:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)V

    :cond_3
    iget-wide v1, p0, Lcom/google/a/a/a/a/ho;->f:J

    cmp-long v1, v1, v4

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/a/a/a/a/ho;->f:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)V

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/ho;->g:[Lcom/google/a/a/a/a/hq;

    if-eqz v1, :cond_5

    iget-object v2, p0, Lcom/google/a/a/a/a/ho;->g:[Lcom/google/a/a/a/a/hq;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/ho;->h:[Lcom/google/a/a/a/a/ie;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/a/a/a/a/ho;->h:[Lcom/google/a/a/a/a/ie;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/ho;->i:Lcom/google/a/a/a/a/hr;

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/a/a/a/a/ho;->i:Lcom/google/a/a/a/a/hr;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_7
    iget-object v0, p0, Lcom/google/a/a/a/a/ho;->j:Lcom/google/a/a/a/a/hz;

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/a/a/a/a/ho;->j:Lcom/google/a/a/a/a/hz;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_8
    iget-object v0, p0, Lcom/google/a/a/a/a/ho;->k:Lcom/google/a/a/a/a/hs;

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/a/a/a/a/ho;->k:Lcom/google/a/a/a/a/hs;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_9
    iget v0, p0, Lcom/google/a/a/a/a/ho;->l:I

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget v1, p0, Lcom/google/a/a/a/a/ho;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_a
    iget-object v0, p0, Lcom/google/a/a/a/a/ho;->m:Lcom/google/a/a/a/a/ib;

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/a/a/a/a/ho;->m:Lcom/google/a/a/a/a/ib;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_b
    iget-object v0, p0, Lcom/google/a/a/a/a/ho;->n:Lcom/google/a/a/a/a/if;

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/a/a/a/a/ho;->n:Lcom/google/a/a/a/a/if;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_c
    iget-object v0, p0, Lcom/google/a/a/a/a/ho;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
