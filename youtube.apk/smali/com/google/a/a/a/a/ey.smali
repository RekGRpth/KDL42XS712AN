.class public final Lcom/google/a/a/a/a/ey;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/ey;


# instance fields
.field public b:I

.field public c:[Lcom/google/a/a/a/a/sh;

.field public d:Lcom/google/a/a/a/a/fk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/ey;

    sput-object v0, Lcom/google/a/a/a/a/ey;->a:[Lcom/google/a/a/a/a/ey;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/a/a/a/a/ey;->b:I

    sget-object v0, Lcom/google/a/a/a/a/sh;->a:[Lcom/google/a/a/a/a/sh;

    iput-object v0, p0, Lcom/google/a/a/a/a/ey;->c:[Lcom/google/a/a/a/a/sh;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/a/a/a/a/ey;->d:Lcom/google/a/a/a/a/fk;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/a/a/a/a/ey;->b:I

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    iget v2, p0, Lcom/google/a/a/a/a/ey;->b:I

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/a/a/a/a/ey;->c:[Lcom/google/a/a/a/a/sh;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/a/a/a/a/ey;->c:[Lcom/google/a/a/a/a/sh;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    const/4 v5, 0x2

    invoke-static {v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/ey;->d:Lcom/google/a/a/a/a/fk;

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/a/a/a/a/ey;->d:Lcom/google/a/a/a/a/fk;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/ey;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/ey;->dm:I

    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lcom/google/a/a/a/a/ey;->dl:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/a/a/a/a/ey;->dl:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/ey;->dl:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->h()I

    move-result v0

    iput v0, p0, Lcom/google/a/a/a/a/ey;->b:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/ey;->c:[Lcom/google/a/a/a/a/sh;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/sh;

    iget-object v3, p0, Lcom/google/a/a/a/a/ey;->c:[Lcom/google/a/a/a/a/sh;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/a/a/a/a/ey;->c:[Lcom/google/a/a/a/a/sh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lcom/google/a/a/a/a/ey;->c:[Lcom/google/a/a/a/a/sh;

    :goto_2
    iget-object v2, p0, Lcom/google/a/a/a/a/ey;->c:[Lcom/google/a/a/a/a/sh;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lcom/google/a/a/a/a/ey;->c:[Lcom/google/a/a/a/a/sh;

    new-instance v3, Lcom/google/a/a/a/a/sh;

    invoke-direct {v3}, Lcom/google/a/a/a/a/sh;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ey;->c:[Lcom/google/a/a/a/a/sh;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/ey;->c:[Lcom/google/a/a/a/a/sh;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/ey;->c:[Lcom/google/a/a/a/a/sh;

    new-instance v3, Lcom/google/a/a/a/a/sh;

    invoke-direct {v3}, Lcom/google/a/a/a/a/sh;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/ey;->c:[Lcom/google/a/a/a/a/sh;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/ey;->d:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/ey;->d:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5

    iget v0, p0, Lcom/google/a/a/a/a/ey;->b:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/a/a/a/a/ey;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->c(II)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/ey;->c:[Lcom/google/a/a/a/a/sh;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/a/a/a/a/ey;->c:[Lcom/google/a/a/a/a/sh;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/ey;->d:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/a/a/a/a/ey;->d:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/ey;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
