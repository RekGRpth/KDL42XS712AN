.class public final Lcom/google/a/a/a/a/js;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/js;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Lcom/google/a/a/a/a/fk;

.field public d:Lcom/google/a/a/a/a/sx;

.field public e:J

.field public f:Lcom/google/a/a/a/a/kz;

.field public g:Lcom/google/a/a/a/a/fk;

.field public h:Lcom/google/a/a/a/a/fk;

.field public i:[Lcom/google/a/a/a/a/jt;

.field public j:Lcom/google/a/a/a/a/fk;

.field public k:Lcom/google/a/a/a/a/fk;

.field public l:Lcom/google/a/a/a/a/kz;

.field public m:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/js;

    sput-object v0, Lcom/google/a/a/a/a/js;->a:[Lcom/google/a/a/a/a/js;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/js;->b:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/a/a/a/a/js;->c:Lcom/google/a/a/a/a/fk;

    iput-object v2, p0, Lcom/google/a/a/a/a/js;->d:Lcom/google/a/a/a/a/sx;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/a/a/a/a/js;->e:J

    iput-object v2, p0, Lcom/google/a/a/a/a/js;->f:Lcom/google/a/a/a/a/kz;

    iput-object v2, p0, Lcom/google/a/a/a/a/js;->g:Lcom/google/a/a/a/a/fk;

    iput-object v2, p0, Lcom/google/a/a/a/a/js;->h:Lcom/google/a/a/a/a/fk;

    sget-object v0, Lcom/google/a/a/a/a/jt;->a:[Lcom/google/a/a/a/a/jt;

    iput-object v0, p0, Lcom/google/a/a/a/a/js;->i:[Lcom/google/a/a/a/a/jt;

    iput-object v2, p0, Lcom/google/a/a/a/a/js;->j:Lcom/google/a/a/a/a/fk;

    iput-object v2, p0, Lcom/google/a/a/a/a/js;->k:Lcom/google/a/a/a/a/fk;

    iput-object v2, p0, Lcom/google/a/a/a/a/js;->l:Lcom/google/a/a/a/a/kz;

    sget-object v0, Lcom/google/protobuf/nano/f;->l:[B

    iput-object v0, p0, Lcom/google/a/a/a/a/js;->m:[B

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/a/a/a/a/js;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/a/a/a/a/js;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/a/a/a/a/js;->c:Lcom/google/a/a/a/a/fk;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/a/a/a/a/js;->c:Lcom/google/a/a/a/a/fk;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lcom/google/a/a/a/a/js;->d:Lcom/google/a/a/a/a/sx;

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/a/a/a/a/js;->d:Lcom/google/a/a/a/a/sx;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-wide v2, p0, Lcom/google/a/a/a/a/js;->e:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-wide v3, p0, Lcom/google/a/a/a/a/js;->e:J

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lcom/google/a/a/a/a/js;->f:Lcom/google/a/a/a/a/kz;

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/a/a/a/a/js;->f:Lcom/google/a/a/a/a/kz;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lcom/google/a/a/a/a/js;->g:Lcom/google/a/a/a/a/fk;

    if-eqz v2, :cond_4

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/a/a/a/a/js;->g:Lcom/google/a/a/a/a/fk;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/js;->h:Lcom/google/a/a/a/a/fk;

    if-eqz v2, :cond_5

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/a/a/a/a/js;->h:Lcom/google/a/a/a/a/fk;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Lcom/google/a/a/a/a/js;->i:[Lcom/google/a/a/a/a/jt;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/a/a/a/a/js;->i:[Lcom/google/a/a/a/a/jt;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    const/16 v5, 0x9

    invoke-static {v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    iget-object v1, p0, Lcom/google/a/a/a/a/js;->j:Lcom/google/a/a/a/a/fk;

    if-eqz v1, :cond_7

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/a/a/a/a/js;->j:Lcom/google/a/a/a/a/fk;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lcom/google/a/a/a/a/js;->k:Lcom/google/a/a/a/a/fk;

    if-eqz v1, :cond_8

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/a/a/a/a/js;->k:Lcom/google/a/a/a/a/fk;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lcom/google/a/a/a/a/js;->l:Lcom/google/a/a/a/a/kz;

    if-eqz v1, :cond_9

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/a/a/a/a/js;->l:Lcom/google/a/a/a/a/kz;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lcom/google/a/a/a/a/js;->m:[B

    sget-object v2, Lcom/google/protobuf/nano/f;->l:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_a

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/a/a/a/a/js;->m:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Lcom/google/a/a/a/a/js;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/js;->dm:I

    return v0

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lcom/google/a/a/a/a/js;->dl:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/a/a/a/a/js;->dl:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/js;->dl:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/js;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/js;->c:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/js;->c:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/sx;

    invoke-direct {v0}, Lcom/google/a/a/a/a/sx;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/js;->d:Lcom/google/a/a/a/a/sx;

    iget-object v0, p0, Lcom/google/a/a/a/a/js;->d:Lcom/google/a/a/a/a/sx;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->c()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/a/a/a/a/js;->e:J

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/kz;

    invoke-direct {v0}, Lcom/google/a/a/a/a/kz;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/js;->f:Lcom/google/a/a/a/a/kz;

    iget-object v0, p0, Lcom/google/a/a/a/a/js;->f:Lcom/google/a/a/a/a/kz;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/js;->g:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/js;->g:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_7
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/js;->h:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/js;->h:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/a/a/a/a/js;->i:[Lcom/google/a/a/a/a/jt;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/a/a/a/a/jt;

    iget-object v3, p0, Lcom/google/a/a/a/a/js;->i:[Lcom/google/a/a/a/a/jt;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/a/a/a/a/js;->i:[Lcom/google/a/a/a/a/jt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lcom/google/a/a/a/a/js;->i:[Lcom/google/a/a/a/a/jt;

    :goto_2
    iget-object v2, p0, Lcom/google/a/a/a/a/js;->i:[Lcom/google/a/a/a/a/jt;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lcom/google/a/a/a/a/js;->i:[Lcom/google/a/a/a/a/jt;

    new-instance v3, Lcom/google/a/a/a/a/jt;

    invoke-direct {v3}, Lcom/google/a/a/a/a/jt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/js;->i:[Lcom/google/a/a/a/a/jt;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/js;->i:[Lcom/google/a/a/a/a/jt;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/js;->i:[Lcom/google/a/a/a/a/jt;

    new-instance v3, Lcom/google/a/a/a/a/jt;

    invoke-direct {v3}, Lcom/google/a/a/a/a/jt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/a/a/a/a/js;->i:[Lcom/google/a/a/a/a/jt;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_9
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/js;->j:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/js;->j:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_a
    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/js;->k:Lcom/google/a/a/a/a/fk;

    iget-object v0, p0, Lcom/google/a/a/a/a/js;->k:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_b
    new-instance v0, Lcom/google/a/a/a/a/kz;

    invoke-direct {v0}, Lcom/google/a/a/a/a/kz;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/js;->l:Lcom/google/a/a/a/a/kz;

    iget-object v0, p0, Lcom/google/a/a/a/a/js;->l:Lcom/google/a/a/a/a/kz;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->g()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/js;->m:[B

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x6a -> :sswitch_b
        0x72 -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5

    iget-object v0, p0, Lcom/google/a/a/a/a/js;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/a/a/a/js;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/js;->c:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/a/a/a/a/js;->c:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/js;->d:Lcom/google/a/a/a/a/sx;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/a/a/a/a/js;->d:Lcom/google/a/a/a/a/sx;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-wide v0, p0, Lcom/google/a/a/a/a/js;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-wide v1, p0, Lcom/google/a/a/a/a/js;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(IJ)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/js;->f:Lcom/google/a/a/a/a/kz;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/a/a/a/a/js;->f:Lcom/google/a/a/a/a/kz;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/js;->g:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/a/a/a/a/js;->g:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/js;->h:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/a/a/a/a/js;->h:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_6
    iget-object v0, p0, Lcom/google/a/a/a/a/js;->i:[Lcom/google/a/a/a/a/jt;

    if-eqz v0, :cond_7

    iget-object v1, p0, Lcom/google/a/a/a/a/js;->i:[Lcom/google/a/a/a/a/jt;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    const/16 v4, 0x9

    invoke-virtual {p1, v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/a/a/a/a/js;->j:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_8

    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/a/a/a/a/js;->j:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_8
    iget-object v0, p0, Lcom/google/a/a/a/a/js;->k:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_9

    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/a/a/a/a/js;->k:Lcom/google/a/a/a/a/fk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_9
    iget-object v0, p0, Lcom/google/a/a/a/a/js;->l:Lcom/google/a/a/a/a/kz;

    if-eqz v0, :cond_a

    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/a/a/a/a/js;->l:Lcom/google/a/a/a/a/kz;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_a
    iget-object v0, p0, Lcom/google/a/a/a/a/js;->m:[B

    sget-object v1, Lcom/google/protobuf/nano/f;->l:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_b

    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/a/a/a/a/js;->m:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I[B)V

    :cond_b
    iget-object v0, p0, Lcom/google/a/a/a/a/js;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
