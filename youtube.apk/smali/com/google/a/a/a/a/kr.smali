.class public final Lcom/google/a/a/a/a/kr;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/kr;


# instance fields
.field public b:Lcom/google/a/a/a/a/ks;

.field public c:Lcom/google/a/a/a/a/kn;

.field public d:Lcom/google/a/a/a/a/kl;

.field public e:Lcom/google/a/a/a/a/ko;

.field public f:Lcom/google/a/a/a/a/kk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/kr;

    sput-object v0, Lcom/google/a/a/a/a/kr;->a:[Lcom/google/a/a/a/a/kr;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/kr;->b:Lcom/google/a/a/a/a/ks;

    iput-object v0, p0, Lcom/google/a/a/a/a/kr;->c:Lcom/google/a/a/a/a/kn;

    iput-object v0, p0, Lcom/google/a/a/a/a/kr;->d:Lcom/google/a/a/a/a/kl;

    iput-object v0, p0, Lcom/google/a/a/a/a/kr;->e:Lcom/google/a/a/a/a/ko;

    iput-object v0, p0, Lcom/google/a/a/a/a/kr;->f:Lcom/google/a/a/a/a/kk;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/a/a/a/a/kr;->b:Lcom/google/a/a/a/a/ks;

    if-eqz v1, :cond_0

    const v0, 0x3ba452a

    iget-object v1, p0, Lcom/google/a/a/a/a/kr;->b:Lcom/google/a/a/a/a/ks;

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/a/a/a/a/kr;->c:Lcom/google/a/a/a/a/kn;

    if-eqz v1, :cond_1

    const v1, 0x3bbc7d4

    iget-object v2, p0, Lcom/google/a/a/a/a/kr;->c:Lcom/google/a/a/a/a/kn;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/kr;->d:Lcom/google/a/a/a/a/kl;

    if-eqz v1, :cond_2

    const v1, 0x3c0ddb7

    iget-object v2, p0, Lcom/google/a/a/a/a/kr;->d:Lcom/google/a/a/a/a/kl;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lcom/google/a/a/a/a/kr;->e:Lcom/google/a/a/a/a/ko;

    if-eqz v1, :cond_3

    const v1, 0x3c0de10

    iget-object v2, p0, Lcom/google/a/a/a/a/kr;->e:Lcom/google/a/a/a/a/ko;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/a/a/a/a/kr;->f:Lcom/google/a/a/a/a/kk;

    if-eqz v1, :cond_4

    const v1, 0x3c0df25

    iget-object v2, p0, Lcom/google/a/a/a/a/kr;->f:Lcom/google/a/a/a/a/kk;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/kr;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/kr;->dm:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/kr;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/kr;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/kr;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/a/a/a/a/ks;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ks;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/kr;->b:Lcom/google/a/a/a/a/ks;

    iget-object v0, p0, Lcom/google/a/a/a/a/kr;->b:Lcom/google/a/a/a/a/ks;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/kn;

    invoke-direct {v0}, Lcom/google/a/a/a/a/kn;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/kr;->c:Lcom/google/a/a/a/a/kn;

    iget-object v0, p0, Lcom/google/a/a/a/a/kr;->c:Lcom/google/a/a/a/a/kn;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/a/a/a/a/kl;

    invoke-direct {v0}, Lcom/google/a/a/a/a/kl;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/kr;->d:Lcom/google/a/a/a/a/kl;

    iget-object v0, p0, Lcom/google/a/a/a/a/kr;->d:Lcom/google/a/a/a/a/kl;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/ko;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ko;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/kr;->e:Lcom/google/a/a/a/a/ko;

    iget-object v0, p0, Lcom/google/a/a/a/a/kr;->e:Lcom/google/a/a/a/a/ko;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/kk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/kk;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/kr;->f:Lcom/google/a/a/a/a/kk;

    iget-object v0, p0, Lcom/google/a/a/a/a/kr;->f:Lcom/google/a/a/a/a/kk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1dd22952 -> :sswitch_1
        0x1dde3ea2 -> :sswitch_2
        0x1e06edba -> :sswitch_3
        0x1e06f082 -> :sswitch_4
        0x1e06f92a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2

    iget-object v0, p0, Lcom/google/a/a/a/a/kr;->b:Lcom/google/a/a/a/a/ks;

    if-eqz v0, :cond_0

    const v0, 0x3ba452a

    iget-object v1, p0, Lcom/google/a/a/a/a/kr;->b:Lcom/google/a/a/a/a/ks;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/kr;->c:Lcom/google/a/a/a/a/kn;

    if-eqz v0, :cond_1

    const v0, 0x3bbc7d4

    iget-object v1, p0, Lcom/google/a/a/a/a/kr;->c:Lcom/google/a/a/a/a/kn;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/kr;->d:Lcom/google/a/a/a/a/kl;

    if-eqz v0, :cond_2

    const v0, 0x3c0ddb7

    iget-object v1, p0, Lcom/google/a/a/a/a/kr;->d:Lcom/google/a/a/a/a/kl;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/kr;->e:Lcom/google/a/a/a/a/ko;

    if-eqz v0, :cond_3

    const v0, 0x3c0de10

    iget-object v1, p0, Lcom/google/a/a/a/a/kr;->e:Lcom/google/a/a/a/a/ko;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/kr;->f:Lcom/google/a/a/a/a/kk;

    if-eqz v0, :cond_4

    const v0, 0x3c0df25

    iget-object v1, p0, Lcom/google/a/a/a/a/kr;->f:Lcom/google/a/a/a/a/kk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/kr;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
