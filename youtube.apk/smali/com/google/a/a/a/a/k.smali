.class public final Lcom/google/a/a/a/a/k;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/a/a/a/a/k;


# instance fields
.field public b:I

.field public c:Lcom/google/a/a/a/a/rz;

.field public d:Ljava/lang/String;

.field public e:Lcom/google/a/a/a/a/gz;

.field public f:Lcom/google/a/a/a/a/hd;

.field public g:[Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:I

.field public j:I

.field public k:Lcom/google/a/a/a/a/wu;

.field public l:Lcom/google/a/a/a/a/xb;

.field public m:Lcom/google/a/a/a/a/mp;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/a/a/a/a/k;

    sput-object v0, Lcom/google/a/a/a/a/k;->a:[Lcom/google/a/a/a/a/k;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    iput v2, p0, Lcom/google/a/a/a/a/k;->b:I

    iput-object v1, p0, Lcom/google/a/a/a/a/k;->c:Lcom/google/a/a/a/a/rz;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/k;->d:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/a/a/a/a/k;->e:Lcom/google/a/a/a/a/gz;

    iput-object v1, p0, Lcom/google/a/a/a/a/k;->f:Lcom/google/a/a/a/a/hd;

    sget-object v0, Lcom/google/protobuf/nano/f;->j:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/a/a/a/a/k;->g:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/k;->h:Ljava/lang/String;

    iput v2, p0, Lcom/google/a/a/a/a/k;->i:I

    iput v2, p0, Lcom/google/a/a/a/a/k;->j:I

    iput-object v1, p0, Lcom/google/a/a/a/a/k;->k:Lcom/google/a/a/a/a/wu;

    iput-object v1, p0, Lcom/google/a/a/a/a/k;->l:Lcom/google/a/a/a/a/xb;

    iput-object v1, p0, Lcom/google/a/a/a/a/k;->m:Lcom/google/a/a/a/a/mp;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/k;->n:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/a/a/a/a/k;->o:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/a/a/a/a/k;->b:I

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    iget v2, p0, Lcom/google/a/a/a/a/k;->b:I

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/a/a/a/a/k;->c:Lcom/google/a/a/a/a/rz;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/a/a/a/a/k;->c:Lcom/google/a/a/a/a/rz;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lcom/google/a/a/a/a/k;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/a/a/a/a/k;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lcom/google/a/a/a/a/k;->e:Lcom/google/a/a/a/a/gz;

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/a/a/a/a/k;->e:Lcom/google/a/a/a/a/gz;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lcom/google/a/a/a/a/k;->f:Lcom/google/a/a/a/a/hd;

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/a/a/a/a/k;->f:Lcom/google/a/a/a/a/hd;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lcom/google/a/a/a/a/k;->g:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/a/a/a/a/k;->g:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    iget-object v3, p0, Lcom/google/a/a/a/a/k;->g:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    invoke-static {v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    add-int/2addr v0, v2

    iget-object v1, p0, Lcom/google/a/a/a/a/k;->g:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lcom/google/a/a/a/a/k;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/a/a/a/a/k;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Lcom/google/a/a/a/a/k;->i:I

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget v2, p0, Lcom/google/a/a/a/a/k;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget v1, p0, Lcom/google/a/a/a/a/k;->j:I

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget v2, p0, Lcom/google/a/a/a/a/k;->j:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lcom/google/a/a/a/a/k;->k:Lcom/google/a/a/a/a/wu;

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/a/a/a/a/k;->k:Lcom/google/a/a/a/a/wu;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lcom/google/a/a/a/a/k;->l:Lcom/google/a/a/a/a/xb;

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/a/a/a/a/k;->l:Lcom/google/a/a/a/a/xb;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Lcom/google/a/a/a/a/k;->m:Lcom/google/a/a/a/a/mp;

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/a/a/a/a/k;->m:Lcom/google/a/a/a/a/mp;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILcom/google/protobuf/nano/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Lcom/google/a/a/a/a/k;->n:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/a/a/a/a/k;->n:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-object v1, p0, Lcom/google/a/a/a/a/k;->o:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/a/a/a/a/k;->o:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget-object v1, p0, Lcom/google/a/a/a/a/k;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/a/a/a/a/k;->dm:I

    return v0

    :cond_e
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 4

    const/4 v3, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/a/a/a/a/k;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/a/a/a/a/k;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/a/a/a/a/k;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lcom/google/a/a/a/a/k;->b:I

    goto :goto_0

    :cond_3
    iput v3, p0, Lcom/google/a/a/a/a/k;->b:I

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/a/a/a/a/rz;

    invoke-direct {v0}, Lcom/google/a/a/a/a/rz;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/k;->c:Lcom/google/a/a/a/a/rz;

    iget-object v0, p0, Lcom/google/a/a/a/a/k;->c:Lcom/google/a/a/a/a/rz;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/k;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/a/a/a/a/gz;

    invoke-direct {v0}, Lcom/google/a/a/a/a/gz;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/k;->e:Lcom/google/a/a/a/a/gz;

    iget-object v0, p0, Lcom/google/a/a/a/a/k;->e:Lcom/google/a/a/a/a/gz;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/a/a/a/a/hd;

    invoke-direct {v0}, Lcom/google/a/a/a/a/hd;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/k;->f:Lcom/google/a/a/a/a/hd;

    iget-object v0, p0, Lcom/google/a/a/a/a/k;->f:Lcom/google/a/a/a/a/hd;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v1

    iget-object v0, p0, Lcom/google/a/a/a/a/k;->g:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/a/a/a/a/k;->g:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lcom/google/a/a/a/a/k;->g:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lcom/google/a/a/a/a/k;->g:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Lcom/google/a/a/a/a/k;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/a/a/a/a/k;->g:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/k;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    iput v0, p0, Lcom/google/a/a/a/a/k;->i:I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    iput v0, p0, Lcom/google/a/a/a/a/k;->j:I

    goto/16 :goto_0

    :sswitch_a
    new-instance v0, Lcom/google/a/a/a/a/wu;

    invoke-direct {v0}, Lcom/google/a/a/a/a/wu;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/k;->k:Lcom/google/a/a/a/a/wu;

    iget-object v0, p0, Lcom/google/a/a/a/a/k;->k:Lcom/google/a/a/a/a/wu;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_b
    new-instance v0, Lcom/google/a/a/a/a/xb;

    invoke-direct {v0}, Lcom/google/a/a/a/a/xb;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/k;->l:Lcom/google/a/a/a/a/xb;

    iget-object v0, p0, Lcom/google/a/a/a/a/k;->l:Lcom/google/a/a/a/a/xb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_c
    new-instance v0, Lcom/google/a/a/a/a/mp;

    invoke-direct {v0}, Lcom/google/a/a/a/a/mp;-><init>()V

    iput-object v0, p0, Lcom/google/a/a/a/a/k;->m:Lcom/google/a/a/a/a/mp;

    iget-object v0, p0, Lcom/google/a/a/a/a/k;->m:Lcom/google/a/a/a/a/mp;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/c;)V

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/k;->n:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/a/a/a/k;->o:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5

    iget v0, p0, Lcom/google/a/a/a/a/k;->b:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/a/a/a/a/k;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_0
    iget-object v0, p0, Lcom/google/a/a/a/a/k;->c:Lcom/google/a/a/a/a/rz;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/a/a/a/a/k;->c:Lcom/google/a/a/a/a/rz;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_1
    iget-object v0, p0, Lcom/google/a/a/a/a/k;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/a/a/a/a/k;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/google/a/a/a/a/k;->e:Lcom/google/a/a/a/a/gz;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/a/a/a/a/k;->e:Lcom/google/a/a/a/a/gz;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/a/a/a/a/k;->f:Lcom/google/a/a/a/a/hd;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/a/a/a/a/k;->f:Lcom/google/a/a/a/a/hd;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_4
    iget-object v0, p0, Lcom/google/a/a/a/a/k;->g:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/google/a/a/a/a/k;->g:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/k;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/a/a/a/a/k;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_6
    iget v0, p0, Lcom/google/a/a/a/a/k;->i:I

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget v1, p0, Lcom/google/a/a/a/a/k;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_7
    iget v0, p0, Lcom/google/a/a/a/a/k;->j:I

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget v1, p0, Lcom/google/a/a/a/a/k;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_8
    iget-object v0, p0, Lcom/google/a/a/a/a/k;->k:Lcom/google/a/a/a/a/wu;

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/a/a/a/a/k;->k:Lcom/google/a/a/a/a/wu;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_9
    iget-object v0, p0, Lcom/google/a/a/a/a/k;->l:Lcom/google/a/a/a/a/xb;

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/a/a/a/a/k;->l:Lcom/google/a/a/a/a/xb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_a
    iget-object v0, p0, Lcom/google/a/a/a/a/k;->m:Lcom/google/a/a/a/a/mp;

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/a/a/a/a/k;->m:Lcom/google/a/a/a/a/mp;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILcom/google/protobuf/nano/c;)V

    :cond_b
    iget-object v0, p0, Lcom/google/a/a/a/a/k;->n:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/a/a/a/a/k;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_c
    iget-object v0, p0, Lcom/google/a/a/a/a/k;->o:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/a/a/a/a/k;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_d
    iget-object v0, p0, Lcom/google/a/a/a/a/k;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
