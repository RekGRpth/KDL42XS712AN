.class public Lcom/google/android/gtalkservice/GroupChatInvitation;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private mGroupContactId:J

.field private mInviter:Ljava/lang/String;

.field private mPassword:Ljava/lang/String;

.field private mReason:Ljava/lang/String;

.field private mRoomAddress:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gtalkservice/c;

    invoke-direct {v0}, Lcom/google/android/gtalkservice/c;-><init>()V

    sput-object v0, Lcom/google/android/gtalkservice/GroupChatInvitation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mRoomAddress:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mInviter:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mReason:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mPassword:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mGroupContactId:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mRoomAddress:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mInviter:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mReason:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mPassword:Ljava/lang/String;

    iput-wide p5, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mGroupContactId:J

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getGroupContactId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mGroupContactId:J

    return-wide v0
.end method

.method public getInviter()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mInviter:Ljava/lang/String;

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getReason()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mReason:Ljava/lang/String;

    return-object v0
.end method

.method public getRoomAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mRoomAddress:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mRoomAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mInviter:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mReason:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mPassword:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/google/android/gtalkservice/GroupChatInvitation;->mGroupContactId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
