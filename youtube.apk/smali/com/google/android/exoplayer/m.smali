.class public final Lcom/google/android/exoplayer/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/exoplayer/upstream/b;

.field private final b:Ljava/util/List;

.field private final c:Ljava/util/HashMap;

.field private final d:Landroid/os/Handler;

.field private final e:Lcom/google/android/exoplayer/o;

.field private final f:J

.field private final g:J

.field private final h:F

.field private final i:F

.field private j:I

.field private k:J

.field private l:I

.field private m:Z

.field private n:Z


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/b;Landroid/os/Handler;Lcom/google/android/exoplayer/o;IIFF)V
    .locals 4

    const-wide/16 v2, 0x3e8

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer/m;->a:Lcom/google/android/exoplayer/upstream/b;

    iput-object v0, p0, Lcom/google/android/exoplayer/m;->d:Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/exoplayer/m;->e:Lcom/google/android/exoplayer/o;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/m;->b:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/m;->c:Ljava/util/HashMap;

    int-to-long v0, p4

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer/m;->f:J

    int-to-long v0, p5

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer/m;->g:J

    iput p6, p0, Lcom/google/android/exoplayer/m;->h:F

    iput p7, p0, Lcom/google/android/exoplayer/m;->i:F

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer/m;)Lcom/google/android/exoplayer/o;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/m;->e:Lcom/google/android/exoplayer/o;

    return-object v0
.end method

.method private a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/m;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/m;->e:Lcom/google/android/exoplayer/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/m;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/n;

    invoke-direct {v1, p0, p1}, Lcom/google/android/exoplayer/n;-><init>(Lcom/google/android/exoplayer/m;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private c()V
    .locals 10

    const-wide/16 v8, -0x1

    const/4 v6, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/exoplayer/m;->l:I

    move v1, v2

    move v3, v0

    move v4, v2

    move v5, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer/m;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/m;->c:Ljava/util/HashMap;

    iget-object v7, p0, Lcom/google/android/exoplayer/m;->b:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/p;

    iget-boolean v7, v0, Lcom/google/android/exoplayer/p;->c:Z

    or-int/2addr v5, v7

    iget-boolean v7, v0, Lcom/google/android/exoplayer/p;->d:Z

    or-int/2addr v4, v7

    iget v0, v0, Lcom/google/android/exoplayer/p;->b:I

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/m;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    if-nez v4, :cond_5

    const/4 v0, 0x2

    if-eq v3, v0, :cond_1

    if-ne v3, v6, :cond_5

    iget-boolean v0, p0, Lcom/google/android/exoplayer/m;->m:Z

    if-eqz v0, :cond_5

    :cond_1
    move v0, v6

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/exoplayer/m;->m:Z

    iget-boolean v0, p0, Lcom/google/android/exoplayer/m;->m:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/exoplayer/m;->n:Z

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/android/exoplayer/upstream/NetworkLock;->a:Lcom/google/android/exoplayer/upstream/NetworkLock;

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/upstream/NetworkLock;->a(I)V

    iput-boolean v6, p0, Lcom/google/android/exoplayer/m;->n:Z

    invoke-direct {p0, v6}, Lcom/google/android/exoplayer/m;->a(Z)V

    :cond_2
    :goto_2
    iput-wide v8, p0, Lcom/google/android/exoplayer/m;->k:J

    iget-boolean v0, p0, Lcom/google/android/exoplayer/m;->m:Z

    if-eqz v0, :cond_7

    :goto_3
    iget-object v0, p0, Lcom/google/android/exoplayer/m;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    iget-object v0, p0, Lcom/google/android/exoplayer/m;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer/m;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/p;

    iget-wide v0, v0, Lcom/google/android/exoplayer/p;->e:J

    cmp-long v3, v0, v8

    if-eqz v3, :cond_4

    iget-wide v3, p0, Lcom/google/android/exoplayer/m;->k:J

    cmp-long v3, v3, v8

    if-eqz v3, :cond_3

    iget-wide v3, p0, Lcom/google/android/exoplayer/m;->k:J

    cmp-long v3, v0, v3

    if-gez v3, :cond_4

    :cond_3
    iput-wide v0, p0, Lcom/google/android/exoplayer/m;->k:J

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/exoplayer/m;->m:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/exoplayer/m;->n:Z

    if-eqz v0, :cond_2

    if-nez v5, :cond_2

    sget-object v0, Lcom/google/android/exoplayer/upstream/NetworkLock;->a:Lcom/google/android/exoplayer/upstream/NetworkLock;

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/upstream/NetworkLock;->b(I)V

    iput-boolean v2, p0, Lcom/google/android/exoplayer/m;->n:Z

    invoke-direct {p0, v2}, Lcom/google/android/exoplayer/m;->a(Z)V

    goto :goto_2

    :cond_7
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/m;->a:Lcom/google/android/exoplayer/upstream/b;

    iget v1, p0, Lcom/google/android/exoplayer/m;->j:I

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/upstream/b;->b(I)V

    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/m;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/exoplayer/m;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/p;

    iget v1, p0, Lcom/google/android/exoplayer/m;->j:I

    iget v0, v0, Lcom/google/android/exoplayer/p;->a:I

    sub-int v0, v1, v0

    iput v0, p0, Lcom/google/android/exoplayer/m;->j:I

    invoke-direct {p0}, Lcom/google/android/exoplayer/m;->c()V

    return-void
.end method

.method public final a(Ljava/lang/Object;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/m;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/exoplayer/m;->c:Ljava/util/HashMap;

    new-instance v1, Lcom/google/android/exoplayer/p;

    invoke-direct {v1, p2}, Lcom/google/android/exoplayer/p;-><init>(I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Lcom/google/android/exoplayer/m;->j:I

    add-int/2addr v0, p2

    iput v0, p0, Lcom/google/android/exoplayer/m;->j:I

    return-void
.end method

.method public final a(Ljava/lang/Object;JJZZ)Z
    .locals 7

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    const-wide/16 v0, -0x1

    cmp-long v0, p4, v0

    if-nez v0, :cond_6

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer/m;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/p;

    iget v5, v0, Lcom/google/android/exoplayer/p;->b:I

    if-ne v5, v1, :cond_0

    iget-wide v5, v0, Lcom/google/android/exoplayer/p;->e:J

    cmp-long v5, v5, p4

    if-nez v5, :cond_0

    iget-boolean v5, v0, Lcom/google/android/exoplayer/p;->c:Z

    if-ne v5, p6, :cond_0

    iget-boolean v5, v0, Lcom/google/android/exoplayer/p;->d:Z

    if-eq v5, p7, :cond_9

    :cond_0
    move v5, v4

    :goto_1
    if-eqz v5, :cond_1

    iput v1, v0, Lcom/google/android/exoplayer/p;->b:I

    iput-wide p4, v0, Lcom/google/android/exoplayer/p;->e:J

    iput-boolean p6, v0, Lcom/google/android/exoplayer/p;->c:Z

    iput-boolean p7, v0, Lcom/google/android/exoplayer/p;->d:Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/m;->a:Lcom/google/android/exoplayer/upstream/b;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/b;->a()I

    move-result v1

    int-to-float v0, v1

    iget v6, p0, Lcom/google/android/exoplayer/m;->j:I

    int-to-float v6, v6

    div-float/2addr v0, v6

    iget v6, p0, Lcom/google/android/exoplayer/m;->i:F

    cmpl-float v6, v0, v6

    if-lez v6, :cond_a

    move v3, v2

    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/android/exoplayer/m;->l:I

    if-eq v0, v3, :cond_b

    move v0, v4

    :goto_3
    if-eqz v0, :cond_3

    iput v3, p0, Lcom/google/android/exoplayer/m;->l:I

    :cond_3
    if-nez v5, :cond_4

    if-eqz v0, :cond_5

    :cond_4
    invoke-direct {p0}, Lcom/google/android/exoplayer/m;->c()V

    :cond_5
    iget v0, p0, Lcom/google/android/exoplayer/m;->j:I

    if-ge v1, v0, :cond_c

    iget-wide v0, p0, Lcom/google/android/exoplayer/m;->k:J

    cmp-long v0, p4, v0

    if-gtz v0, :cond_c

    :goto_4
    return v4

    :cond_6
    sub-long v0, p4, p2

    iget-wide v5, p0, Lcom/google/android/exoplayer/m;->g:J

    cmp-long v5, v0, v5

    if-lez v5, :cond_7

    move v1, v2

    goto :goto_0

    :cond_7
    iget-wide v5, p0, Lcom/google/android/exoplayer/m;->f:J

    cmp-long v0, v0, v5

    if-gez v0, :cond_8

    move v1, v3

    goto :goto_0

    :cond_8
    move v1, v4

    goto :goto_0

    :cond_9
    move v5, v2

    goto :goto_1

    :cond_a
    iget v6, p0, Lcom/google/android/exoplayer/m;->h:F

    cmpg-float v0, v0, v6

    if-ltz v0, :cond_2

    move v3, v4

    goto :goto_2

    :cond_b
    move v0, v2

    goto :goto_3

    :cond_c
    move v4, v2

    goto :goto_4
.end method

.method public final b()Lcom/google/android/exoplayer/upstream/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/m;->a:Lcom/google/android/exoplayer/upstream/b;

    return-object v0
.end method
