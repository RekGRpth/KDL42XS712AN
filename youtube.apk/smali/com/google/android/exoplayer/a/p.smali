.class public Lcom/google/android/exoplayer/a/p;
.super Lcom/google/android/exoplayer/a/q;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/exoplayer/upstream/c;

.field private final b:I

.field private final c:J

.field private final d:J

.field private final e:J

.field private final f:F


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/c;IIIIF)V
    .locals 4

    const-wide/16 v2, 0x3e8

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/q;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer/a/p;->a:Lcom/google/android/exoplayer/upstream/c;

    iput p2, p0, Lcom/google/android/exoplayer/a/p;->b:I

    int-to-long v0, p3

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer/a/p;->c:J

    int-to-long v0, p4

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer/a/p;->d:J

    int-to-long v0, p5

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer/a/p;->e:J

    iput p6, p0, Lcom/google/android/exoplayer/a/p;->f:F

    return-void
.end method


# virtual methods
.method protected a([Lcom/google/android/exoplayer/a/m;J)Lcom/google/android/exoplayer/a/m;
    .locals 6

    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer/a/p;->b:I

    int-to-long v0, v0

    :goto_0
    const/4 v2, 0x0

    :goto_1
    array-length v3, p1

    if-ge v2, v3, :cond_2

    aget-object v3, p1, v2

    iget v4, v3, Lcom/google/android/exoplayer/a/m;->g:I

    int-to-long v4, v4

    cmp-long v4, v4, v0

    if-gtz v4, :cond_1

    move-object v0, v3

    :goto_2
    return-object v0

    :cond_0
    long-to-float v0, p2

    iget v1, p0, Lcom/google/android/exoplayer/a/p;->f:F

    mul-float/2addr v0, v1

    float-to-long v0, v0

    goto :goto_0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p1, v0

    goto :goto_2
.end method

.method public final a(Ljava/util/List;J[Lcom/google/android/exoplayer/a/m;Lcom/google/android/exoplayer/a/r;)V
    .locals 9

    const/4 v5, 0x1

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x0

    :goto_0
    iget-object v3, p5, Lcom/google/android/exoplayer/a/r;->c:Lcom/google/android/exoplayer/a/m;

    iget-object v4, p0, Lcom/google/android/exoplayer/a/p;->a:Lcom/google/android/exoplayer/upstream/c;

    invoke-virtual {v4}, Lcom/google/android/exoplayer/upstream/c;->c()J

    move-result-wide v6

    invoke-virtual {p0, p4, v6, v7}, Lcom/google/android/exoplayer/a/p;->a([Lcom/google/android/exoplayer/a/m;J)Lcom/google/android/exoplayer/a/m;

    move-result-object v4

    if-eqz v4, :cond_2

    if-eqz v3, :cond_2

    iget v6, v4, Lcom/google/android/exoplayer/a/m;->g:I

    iget v7, v3, Lcom/google/android/exoplayer/a/m;->g:I

    if-le v6, v7, :cond_2

    move v6, v5

    :goto_1
    if-eqz v4, :cond_3

    if-eqz v3, :cond_3

    iget v7, v4, Lcom/google/android/exoplayer/a/m;->g:I

    iget v8, v3, Lcom/google/android/exoplayer/a/m;->g:I

    if-ge v7, v8, :cond_3

    :goto_2
    if-eqz v6, :cond_7

    iget-wide v5, p0, Lcom/google/android/exoplayer/a/p;->c:J

    cmp-long v5, v0, v5

    if-gez v5, :cond_4

    move-object v0, v3

    :goto_3
    if-eqz v3, :cond_0

    if-eq v0, v3, :cond_0

    const/4 v1, 0x2

    iput v1, p5, Lcom/google/android/exoplayer/a/r;->b:I

    :cond_0
    iput-object v0, p5, Lcom/google/android/exoplayer/a/r;->c:Lcom/google/android/exoplayer/a/m;

    return-void

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/a/t;

    iget-wide v0, v0, Lcom/google/android/exoplayer/a/t;->e:J

    sub-long/2addr v0, p2

    goto :goto_0

    :cond_2
    move v6, v2

    goto :goto_1

    :cond_3
    move v5, v2

    goto :goto_2

    :cond_4
    iget-wide v5, p0, Lcom/google/android/exoplayer/a/p;->e:J

    cmp-long v0, v0, v5

    if-ltz v0, :cond_8

    move v1, v2

    :goto_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/a/t;

    iget-wide v5, v0, Lcom/google/android/exoplayer/a/t;->d:J

    sub-long/2addr v5, p2

    iget-wide v7, p0, Lcom/google/android/exoplayer/a/p;->e:J

    cmp-long v2, v5, v7

    if-ltz v2, :cond_5

    iget-object v2, v0, Lcom/google/android/exoplayer/a/t;->a:Lcom/google/android/exoplayer/a/m;

    iget v2, v2, Lcom/google/android/exoplayer/a/m;->g:I

    iget v5, v4, Lcom/google/android/exoplayer/a/m;->g:I

    if-ge v2, v5, :cond_5

    iget-object v2, v0, Lcom/google/android/exoplayer/a/t;->a:Lcom/google/android/exoplayer/a/m;

    iget v2, v2, Lcom/google/android/exoplayer/a/m;->d:I

    iget v5, v4, Lcom/google/android/exoplayer/a/m;->d:I

    if-ge v2, v5, :cond_5

    iget-object v0, v0, Lcom/google/android/exoplayer/a/t;->a:Lcom/google/android/exoplayer/a/m;

    iget v0, v0, Lcom/google/android/exoplayer/a/m;->d:I

    const/16 v2, 0x2d0

    if-ge v0, v2, :cond_5

    iput v1, p5, Lcom/google/android/exoplayer/a/r;->a:I

    move-object v0, v4

    goto :goto_3

    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_6
    move-object v0, v4

    goto :goto_3

    :cond_7
    if-eqz v5, :cond_8

    if-eqz v3, :cond_8

    iget-wide v5, p0, Lcom/google/android/exoplayer/a/p;->d:J

    cmp-long v0, v0, v5

    if-ltz v0, :cond_8

    move-object v0, v3

    goto :goto_3

    :cond_8
    move-object v0, v4

    goto :goto_3
.end method
