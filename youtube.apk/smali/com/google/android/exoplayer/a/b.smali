.class public final Lcom/google/android/exoplayer/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/ai;
.implements Lcom/google/android/exoplayer/upstream/m;


# instance fields
.field private final a:I

.field private final b:Lcom/google/android/exoplayer/m;

.field private final c:Lcom/google/android/exoplayer/a/l;

.field private final d:Ljava/util/LinkedList;

.field private final e:Ljava/util/List;

.field private final f:I

.field private final g:Z

.field private final h:Landroid/os/Handler;

.field private final i:Lcom/google/android/exoplayer/a/k;

.field private j:I

.field private k:J

.field private l:J

.field private m:J

.field private n:J

.field private o:Lcom/google/android/exoplayer/upstream/Loader;

.field private p:Lcom/google/android/exoplayer/a/a;

.field private q:Ljava/io/IOException;

.field private r:Z

.field private s:I

.field private t:J

.field private volatile u:Lcom/google/android/exoplayer/a/m;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/a/l;Lcom/google/android/exoplayer/m;IZ)V
    .locals 8

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v6, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/exoplayer/a/b;-><init>(Lcom/google/android/exoplayer/a/l;Lcom/google/android/exoplayer/m;IZLandroid/os/Handler;Lcom/google/android/exoplayer/a/k;I)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/exoplayer/a/l;Lcom/google/android/exoplayer/m;IZLandroid/os/Handler;Lcom/google/android/exoplayer/a/k;I)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer/a/b;->c:Lcom/google/android/exoplayer/a/l;

    iput-object p2, p0, Lcom/google/android/exoplayer/a/b;->b:Lcom/google/android/exoplayer/m;

    iput p3, p0, Lcom/google/android/exoplayer/a/b;->f:I

    iput-boolean p4, p0, Lcom/google/android/exoplayer/a/b;->g:Z

    iput-object v0, p0, Lcom/google/android/exoplayer/a/b;->h:Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/exoplayer/a/b;->i:Lcom/google/android/exoplayer/a/k;

    iput v1, p0, Lcom/google/android/exoplayer/a/b;->a:I

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/a/b;->e:Ljava/util/List;

    iput v1, p0, Lcom/google/android/exoplayer/a/b;->j:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer/a/b;)I
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer/a/b;->a:I

    return v0
.end method

.method private a(IIZJJJ)V
    .locals 12

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->h:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->i:Lcom/google/android/exoplayer/a/k;

    if-eqz v0, :cond_0

    iget-object v11, p0, Lcom/google/android/exoplayer/a/b;->h:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/exoplayer/a/c;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-wide/from16 v5, p4

    move-wide/from16 v7, p6

    move-wide/from16 v9, p8

    invoke-direct/range {v0 .. v10}, Lcom/google/android/exoplayer/a/c;-><init>(Lcom/google/android/exoplayer/a/b;IIZJJJ)V

    invoke-virtual {v11, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private a(JJJ)V
    .locals 9

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->h:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->i:Lcom/google/android/exoplayer/a/k;

    if-eqz v0, :cond_0

    iget-object v8, p0, Lcom/google/android/exoplayer/a/b;->h:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/exoplayer/a/j;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/exoplayer/a/j;-><init>(Lcom/google/android/exoplayer/a/b;JJJ)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/exoplayer/a/t;)V
    .locals 7

    const-wide/16 v3, 0x0

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/a/t;

    iget-wide v1, v0, Lcom/google/android/exoplayer/a/t;->d:J

    move-wide v5, v3

    :goto_1
    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/a/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/a/t;->c()J

    move-result-wide v3

    add-long/2addr v5, v3

    iget-wide v3, v0, Lcom/google/android/exoplayer/a/t;->e:J

    invoke-virtual {v0}, Lcom/google/android/exoplayer/a/t;->a()V

    goto :goto_1

    :cond_2
    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/a/b;->a(JJJ)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/exoplayer/a/b;)Lcom/google/android/exoplayer/a/k;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->i:Lcom/google/android/exoplayer/a/k;

    return-object v0
.end method

.method private c(I)V
    .locals 9

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/a/t;

    iget-wide v4, v0, Lcom/google/android/exoplayer/a/t;->e:J

    move-wide v6, v2

    :goto_1
    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, p1, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/a/t;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/a/t;->c()J

    move-result-wide v1

    add-long/2addr v6, v1

    iget-wide v2, v0, Lcom/google/android/exoplayer/a/t;->d:J

    invoke-virtual {v0}, Lcom/google/android/exoplayer/a/t;->a()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->h:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->i:Lcom/google/android/exoplayer/a/k;

    if-eqz v0, :cond_0

    iget-object v8, p0, Lcom/google/android/exoplayer/a/b;->h:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/exoplayer/a/h;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/exoplayer/a/h;-><init>(Lcom/google/android/exoplayer/a/b;JJJ)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private c(J)V
    .locals 1

    iput-wide p1, p0, Lcom/google/android/exoplayer/a/b;->m:J

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->o:Lcom/google/android/exoplayer/upstream/Loader;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/upstream/Loader;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->o:Lcom/google/android/exoplayer/upstream/Loader;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/upstream/Loader;->b()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/a/b;->a(Lcom/google/android/exoplayer/a/t;)V

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->g()V

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->h()V

    goto :goto_0
.end method

.method private g()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v1, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    iput-object v1, p0, Lcom/google/android/exoplayer/a/b;->q:Ljava/io/IOException;

    iput v0, p0, Lcom/google/android/exoplayer/a/b;->s:I

    iput-boolean v0, p0, Lcom/google/android/exoplayer/a/b;->r:Z

    return-void
.end method

.method private h()V
    .locals 13

    const-wide/16 v11, 0x3e8

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v4, p0, Lcom/google/android/exoplayer/a/b;->m:J

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->q:Ljava/io/IOException;

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/exoplayer/a/b;->r:Z

    if-nez v0, :cond_4

    move v8, v9

    :goto_1
    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->b:Lcom/google/android/exoplayer/m;

    iget-wide v2, p0, Lcom/google/android/exoplayer/a/b;->k:J

    if-nez v8, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer/a/b;->o:Lcom/google/android/exoplayer/upstream/Loader;

    invoke-virtual {v1}, Lcom/google/android/exoplayer/upstream/Loader;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_0
    move v6, v9

    :goto_2
    iget-boolean v7, p0, Lcom/google/android/exoplayer/a/b;->r:Z

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/exoplayer/m;->a(Ljava/lang/Object;JJZZ)Z

    move-result v6

    iget-boolean v0, p0, Lcom/google/android/exoplayer/a/b;->r:Z

    if-eqz v0, :cond_6

    :cond_1
    :goto_3
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/a/t;

    iget v1, v0, Lcom/google/android/exoplayer/a/t;->f:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    const-wide/16 v4, -0x1

    goto :goto_0

    :cond_3
    iget-wide v4, v0, Lcom/google/android/exoplayer/a/t;->e:J

    goto :goto_0

    :cond_4
    move v8, v10

    goto :goto_1

    :cond_5
    move v6, v10

    goto :goto_2

    :cond_6
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    if-eqz v8, :cond_d

    iget-wide v2, p0, Lcom/google/android/exoplayer/a/b;->t:J

    sub-long/2addr v0, v2

    iget v2, p0, Lcom/google/android/exoplayer/a/b;->s:I

    int-to-long v2, v2

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    mul-long/2addr v2, v11

    const-wide/16 v4, 0x1388

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/a/b;->q:Ljava/io/IOException;

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    instance-of v0, v0, Lcom/google/android/exoplayer/a/t;

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->c:Lcom/google/android/exoplayer/a/l;

    iget-object v1, p0, Lcom/google/android/exoplayer/a/b;->e:Ljava/util/List;

    iget-wide v2, p0, Lcom/google/android/exoplayer/a/b;->m:J

    iget-wide v4, p0, Lcom/google/android/exoplayer/a/b;->k:J

    invoke-interface/range {v0 .. v5}, Lcom/google/android/exoplayer/a/l;->a(Ljava/util/List;JJ)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/a/b;->c(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/a/a;->a()V

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer/a/a;

    iput-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->i()V

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    iget-object v1, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v1

    if-ne v0, v1, :cond_8

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->o:Lcom/google/android/exoplayer/upstream/Loader;

    iget-object v1, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer/upstream/Loader;->a(Lcom/google/android/exoplayer/upstream/o;)V

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/exoplayer/a/t;

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    if-ne v0, v6, :cond_a

    move v0, v9

    :goto_4
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->c:Lcom/google/android/exoplayer/a/l;

    iget-object v1, p0, Lcom/google/android/exoplayer/a/b;->e:Ljava/util/List;

    iget-wide v2, p0, Lcom/google/android/exoplayer/a/b;->m:J

    iget-wide v4, p0, Lcom/google/android/exoplayer/a/b;->k:J

    invoke-interface/range {v0 .. v5}, Lcom/google/android/exoplayer/a/l;->a(Ljava/util/List;JJ)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer/a/a;

    if-eqz v0, :cond_9

    instance-of v2, v0, Lcom/google/android/exoplayer/a/t;

    if-nez v2, :cond_b

    :cond_9
    :goto_5
    if-eqz v10, :cond_c

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->o:Lcom/google/android/exoplayer/upstream/Loader;

    iget-object v1, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer/upstream/Loader;->a(Lcom/google/android/exoplayer/upstream/o;)V

    goto/16 :goto_3

    :cond_a
    move v0, v10

    goto :goto_4

    :cond_b
    check-cast v0, Lcom/google/android/exoplayer/a/t;

    iget-object v2, v6, Lcom/google/android/exoplayer/a/t;->a:Lcom/google/android/exoplayer/a/m;

    iget v2, v2, Lcom/google/android/exoplayer/a/m;->a:I

    iget-object v3, v0, Lcom/google/android/exoplayer/a/t;->a:Lcom/google/android/exoplayer/a/m;

    iget v3, v3, Lcom/google/android/exoplayer/a/m;->a:I

    if-ne v2, v3, :cond_9

    iget-wide v2, v6, Lcom/google/android/exoplayer/a/t;->d:J

    iget-wide v4, v0, Lcom/google/android/exoplayer/a/t;->d:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_9

    iget-wide v2, v6, Lcom/google/android/exoplayer/a/t;->e:J

    iget-wide v4, v0, Lcom/google/android/exoplayer/a/t;->e:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_9

    move v10, v9

    goto :goto_5

    :cond_c
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/a/b;->c(I)V

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->g()V

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer/a/a;

    iput-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->i()V

    goto/16 :goto_3

    :cond_d
    iget-object v2, p0, Lcom/google/android/exoplayer/a/b;->o:Lcom/google/android/exoplayer/upstream/Loader;

    invoke-virtual {v2}, Lcom/google/android/exoplayer/upstream/Loader;->a()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    if-eqz v2, :cond_e

    iget-wide v2, p0, Lcom/google/android/exoplayer/a/b;->n:J

    sub-long v2, v0, v2

    cmp-long v2, v2, v11

    if-lez v2, :cond_f

    :cond_e
    iput-wide v0, p0, Lcom/google/android/exoplayer/a/b;->n:J

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->c:Lcom/google/android/exoplayer/a/l;

    iget-object v1, p0, Lcom/google/android/exoplayer/a/b;->e:Ljava/util/List;

    iget-wide v2, p0, Lcom/google/android/exoplayer/a/b;->m:J

    iget-wide v4, p0, Lcom/google/android/exoplayer/a/b;->k:J

    invoke-interface/range {v0 .. v5}, Lcom/google/android/exoplayer/a/l;->a(Ljava/util/List;JJ)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/a/b;->c(I)V

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer/a/a;

    iput-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    :cond_f
    if-eqz v6, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->i()V

    goto/16 :goto_3
.end method

.method private i()V
    .locals 10

    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    iget-object v1, p0, Lcom/google/android/exoplayer/a/b;->b:Lcom/google/android/exoplayer/m;

    invoke-virtual {v1}, Lcom/google/android/exoplayer/m;->b()Lcom/google/android/exoplayer/upstream/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer/a/a;->a(Lcom/google/android/exoplayer/upstream/b;)V

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    instance-of v0, v0, Lcom/google/android/exoplayer/a/t;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    check-cast v0, Lcom/google/android/exoplayer/a/t;

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v1, p0, Lcom/google/android/exoplayer/a/b;->m:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/exoplayer/a/t;->a(JZ)Z

    iput-wide v4, p0, Lcom/google/android/exoplayer/a/b;->m:J

    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v1, v0, Lcom/google/android/exoplayer/a/t;->a:Lcom/google/android/exoplayer/a/m;

    iget v1, v1, Lcom/google/android/exoplayer/a/m;->a:I

    iget v2, v0, Lcom/google/android/exoplayer/a/t;->b:I

    iget-wide v4, v0, Lcom/google/android/exoplayer/a/t;->d:J

    iget-wide v6, v0, Lcom/google/android/exoplayer/a/t;->e:J

    iget-wide v8, v0, Lcom/google/android/exoplayer/a/t;->c:J

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/exoplayer/a/b;->a(IIZJJJ)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->o:Lcom/google/android/exoplayer/upstream/Loader;

    iget-object v1, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer/upstream/Loader;->a(Lcom/google/android/exoplayer/upstream/o;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    iget-object v0, v0, Lcom/google/android/exoplayer/a/a;->a:Lcom/google/android/exoplayer/a/m;

    iget v1, v0, Lcom/google/android/exoplayer/a/m;->a:I

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    iget v2, v0, Lcom/google/android/exoplayer/a/a;->b:I

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    iget-wide v8, v0, Lcom/google/android/exoplayer/a/a;->c:J

    move-object v0, p0

    move-wide v6, v4

    invoke-direct/range {v0 .. v9}, Lcom/google/android/exoplayer/a/b;->a(IIZJJJ)V

    goto :goto_0
.end method

.method private j()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/exoplayer/a/b;->m:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->h:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->i:Lcom/google/android/exoplayer/a/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->h:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/a/d;

    invoke-direct {v1, p0}, Lcom/google/android/exoplayer/a/d;-><init>(Lcom/google/android/exoplayer/a/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(IJLcom/google/android/exoplayer/l;Lcom/google/android/exoplayer/ah;)I
    .locals 9

    move-object v0, p0

    :goto_0
    iget v1, v0, Lcom/google/android/exoplayer/a/b;->j:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    if-nez p1, :cond_1

    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iput-wide p2, v0, Lcom/google/android/exoplayer/a/b;->k:J

    invoke-direct {v0}, Lcom/google/android/exoplayer/a/b;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/exoplayer/a/b;->q:Ljava/io/IOException;

    if-eqz v1, :cond_2

    iget-object v0, v0, Lcom/google/android/exoplayer/a/b;->q:Ljava/io/IOException;

    throw v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    const/4 v0, -0x2

    :goto_3
    return v0

    :cond_3
    iget-object v1, v0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/google/android/exoplayer/a/t;

    invoke-virtual {v7}, Lcom/google/android/exoplayer/a/t;->b()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, v0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_4

    iget-object v1, v0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/google/android/exoplayer/a/t;

    invoke-virtual {v3}, Lcom/google/android/exoplayer/a/t;->c()J

    move-result-wide v5

    invoke-virtual {v3}, Lcom/google/android/exoplayer/a/t;->a()V

    iget-wide v1, v3, Lcom/google/android/exoplayer/a/t;->d:J

    iget-wide v3, v3, Lcom/google/android/exoplayer/a/t;->e:J

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/a/b;->a(JJJ)V

    iget-object v1, v0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer/a/t;

    iget-wide v2, v1, Lcom/google/android/exoplayer/a/t;->d:J

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/exoplayer/a/t;->a(JZ)Z

    goto :goto_0

    :cond_4
    invoke-virtual {v7}, Lcom/google/android/exoplayer/a/t;->j()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, -0x1

    goto :goto_3

    :cond_5
    const/4 v0, -0x2

    goto :goto_3

    :cond_6
    iget-object v1, v0, Lcom/google/android/exoplayer/a/b;->u:Lcom/google/android/exoplayer/a/m;

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/google/android/exoplayer/a/b;->u:Lcom/google/android/exoplayer/a/m;

    iget v1, v1, Lcom/google/android/exoplayer/a/m;->a:I

    iget-object v2, v7, Lcom/google/android/exoplayer/a/t;->a:Lcom/google/android/exoplayer/a/m;

    iget v2, v2, Lcom/google/android/exoplayer/a/m;->a:I

    if-eq v1, v2, :cond_9

    :cond_7
    iget-object v1, v7, Lcom/google/android/exoplayer/a/t;->a:Lcom/google/android/exoplayer/a/m;

    iget v3, v1, Lcom/google/android/exoplayer/a/m;->a:I

    iget v4, v7, Lcom/google/android/exoplayer/a/t;->b:I

    iget-wide v5, v7, Lcom/google/android/exoplayer/a/t;->d:J

    iget-object v1, v0, Lcom/google/android/exoplayer/a/b;->h:Landroid/os/Handler;

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/google/android/exoplayer/a/b;->i:Lcom/google/android/exoplayer/a/k;

    if-eqz v1, :cond_8

    iget-object v8, v0, Lcom/google/android/exoplayer/a/b;->h:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/a/i;

    move-object v2, v0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer/a/i;-><init>(Lcom/google/android/exoplayer/a/b;IIJ)V

    invoke-virtual {v8, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_8
    invoke-virtual {v7}, Lcom/google/android/exoplayer/a/t;->k()Lcom/google/android/exoplayer/ag;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/exoplayer/a/b;->c:Lcom/google/android/exoplayer/a/l;

    invoke-interface {v2, v1}, Lcom/google/android/exoplayer/a/l;->a(Lcom/google/android/exoplayer/ag;)V

    iput-object v1, p4, Lcom/google/android/exoplayer/l;->a:Lcom/google/android/exoplayer/ag;

    invoke-virtual {v7}, Lcom/google/android/exoplayer/a/t;->l()Ljava/util/Map;

    move-result-object v1

    iput-object v1, p4, Lcom/google/android/exoplayer/l;->b:Ljava/util/Map;

    iget-object v1, v7, Lcom/google/android/exoplayer/a/t;->a:Lcom/google/android/exoplayer/a/m;

    iput-object v1, v0, Lcom/google/android/exoplayer/a/b;->u:Lcom/google/android/exoplayer/a/m;

    const/4 v0, -0x4

    goto/16 :goto_3

    :cond_9
    invoke-virtual {v7, p5}, Lcom/google/android/exoplayer/a/t;->a(Lcom/google/android/exoplayer/ah;)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-boolean v1, v0, Lcom/google/android/exoplayer/a/b;->g:Z

    if-eqz v1, :cond_a

    iget-wide v1, p5, Lcom/google/android/exoplayer/ah;->f:J

    iget-wide v3, v0, Lcom/google/android/exoplayer/a/b;->l:J

    cmp-long v0, v1, v3

    if-gez v0, :cond_a

    const/4 v0, 0x1

    :goto_4
    iput-boolean v0, p5, Lcom/google/android/exoplayer/ah;->g:Z

    const/4 v0, -0x3

    goto/16 :goto_3

    :cond_a
    const/4 v0, 0x0

    goto :goto_4

    :cond_b
    iget-object v1, v0, Lcom/google/android/exoplayer/a/b;->q:Ljava/io/IOException;

    if-eqz v1, :cond_c

    iget-object v0, v0, Lcom/google/android/exoplayer/a/b;->q:Ljava/io/IOException;

    throw v0

    :cond_c
    const/4 v0, -0x2

    goto/16 :goto_3
.end method

.method public final a(I)Lcom/google/android/exoplayer/aj;
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/exoplayer/a/b;->j:I

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    if-nez p1, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->c:Lcom/google/android/exoplayer/a/l;

    invoke-interface {v0}, Lcom/google/android/exoplayer/a/l;->a()Lcom/google/android/exoplayer/aj;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final a(IJ)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget v0, p0, Lcom/google/android/exoplayer/a/b;->j:I

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    if-nez p1, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/exoplayer/a/b;->j:I

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->c:Lcom/google/android/exoplayer/a/l;

    invoke-interface {v0}, Lcom/google/android/exoplayer/a/l;->b()V

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->b:Lcom/google/android/exoplayer/m;

    iget v1, p0, Lcom/google/android/exoplayer/a/b;->f:I

    invoke-virtual {v0, p0, v1}, Lcom/google/android/exoplayer/m;->a(Ljava/lang/Object;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/a/b;->u:Lcom/google/android/exoplayer/a/m;

    iput-wide p2, p0, Lcom/google/android/exoplayer/a/b;->k:J

    iput-wide p2, p0, Lcom/google/android/exoplayer/a/b;->l:J

    invoke-direct {p0, p2, p3}, Lcom/google/android/exoplayer/a/b;->c(J)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final a(J)V
    .locals 2

    iget v0, p0, Lcom/google/android/exoplayer/a/b;->j:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iput-wide p1, p0, Lcom/google/android/exoplayer/a/b;->k:J

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->h()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/io/IOException;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/exoplayer/a/b;->q:Ljava/io/IOException;

    iget v0, p0, Lcom/google/android/exoplayer/a/b;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer/a/b;->s:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer/a/b;->t:J

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->h:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->i:Lcom/google/android/exoplayer/a/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->h:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/a/f;

    invoke-direct {v1, p0, p1}, Lcom/google/android/exoplayer/a/f;-><init>(Lcom/google/android/exoplayer/a/b;Ljava/io/IOException;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->h()V

    return-void
.end method

.method public final a()Z
    .locals 4

    const/4 v1, 0x1

    iget v0, p0, Lcom/google/android/exoplayer/a/b;->j:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    new-instance v0, Lcom/google/android/exoplayer/upstream/Loader;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Loader:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/exoplayer/a/b;->c:Lcom/google/android/exoplayer/a/l;

    invoke-interface {v3}, Lcom/google/android/exoplayer/a/l;->a()Lcom/google/android/exoplayer/aj;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/exoplayer/aj;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, p0}, Lcom/google/android/exoplayer/upstream/Loader;-><init>(Ljava/lang/String;Lcom/google/android/exoplayer/upstream/m;)V

    iput-object v0, p0, Lcom/google/android/exoplayer/a/b;->o:Lcom/google/android/exoplayer/upstream/Loader;

    iput v1, p0, Lcom/google/android/exoplayer/a/b;->j:I

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/google/android/exoplayer/a/b;->j:I

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget v0, p0, Lcom/google/android/exoplayer/a/b;->j:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    if-nez p1, :cond_0

    move v2, v1

    :cond_0
    invoke-static {v2}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iput v1, p0, Lcom/google/android/exoplayer/a/b;->j:I

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->b:Lcom/google/android/exoplayer/m;

    invoke-virtual {v0, p0}, Lcom/google/android/exoplayer/m;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->c:Lcom/google/android/exoplayer/a/l;

    invoke-interface {v0}, Lcom/google/android/exoplayer/a/l;->c()V

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->o:Lcom/google/android/exoplayer/upstream/Loader;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/upstream/Loader;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->o:Lcom/google/android/exoplayer/upstream/Loader;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/upstream/Loader;->b()V

    :goto_1
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/a/b;->a(Lcom/google/android/exoplayer/a/t;)V

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->g()V

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->b:Lcom/google/android/exoplayer/m;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/m;->a()V

    goto :goto_1
.end method

.method public final b(J)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/exoplayer/a/b;->j:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iput-wide p1, p0, Lcom/google/android/exoplayer/a/b;->k:J

    iput-wide p1, p0, Lcom/google/android/exoplayer/a/b;->l:J

    iget-wide v3, p0, Lcom/google/android/exoplayer/a/b;->m:J

    cmp-long v0, v3, p1

    if-nez v0, :cond_1

    :goto_1
    return v2

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/a/t;

    iget-wide v4, v0, Lcom/google/android/exoplayer/a/t;->d:J

    cmp-long v4, p1, v4

    if-ltz v4, :cond_4

    invoke-virtual {v0}, Lcom/google/android/exoplayer/a/t;->j()Z

    move-result v4

    if-nez v4, :cond_3

    iget-wide v4, v0, Lcom/google/android/exoplayer/a/t;->e:J

    cmp-long v4, p1, v4

    if-gez v4, :cond_2

    :cond_3
    :goto_2
    if-nez v0, :cond_5

    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer/a/b;->c(J)V

    move v2, v1

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    iget-object v3, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v3

    if-ne v0, v3, :cond_6

    :goto_3
    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/exoplayer/a/t;->a(JZ)Z

    move-result v2

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/a/b;->a(Lcom/google/android/exoplayer/a/t;)V

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->h()V

    goto :goto_1

    :cond_6
    move v1, v2

    goto :goto_3
.end method

.method public final c()J
    .locals 7

    iget v0, p0, Lcom/google/android/exoplayer/a/b;->j:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/exoplayer/a/b;->m:J

    :goto_1
    return-wide v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/a/t;

    iget-object v1, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    if-ne v0, v1, :cond_2

    iget-wide v1, v0, Lcom/google/android/exoplayer/a/t;->d:J

    iget-wide v3, v0, Lcom/google/android/exoplayer/a/t;->e:J

    iget-wide v5, v0, Lcom/google/android/exoplayer/a/t;->d:J

    sub-long/2addr v3, v5

    invoke-virtual {v0}, Lcom/google/android/exoplayer/a/t;->c()J

    move-result-wide v5

    mul-long/2addr v3, v5

    iget-wide v5, v0, Lcom/google/android/exoplayer/a/t;->c:J

    div-long/2addr v3, v5

    add-long v0, v1, v3

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/exoplayer/a/t;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    const-wide/16 v0, -0x3

    goto :goto_1

    :cond_3
    iget-wide v0, v0, Lcom/google/android/exoplayer/a/t;->e:J

    goto :goto_1
.end method

.method public final d()V
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/exoplayer/a/b;->j:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->o:Lcom/google/android/exoplayer/upstream/Loader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->o:Lcom/google/android/exoplayer/upstream/Loader;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/upstream/Loader;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/a/b;->o:Lcom/google/android/exoplayer/upstream/Loader;

    :cond_0
    iput v1, p0, Lcom/google/android/exoplayer/a/b;->j:I

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/a/a;->d()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    instance-of v0, v0, Lcom/google/android/exoplayer/a/t;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/a/a;->a()V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer/a/b;->r:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->g()V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->k()V

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->h()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    iput-object v0, p0, Lcom/google/android/exoplayer/a/b;->q:Ljava/io/IOException;

    iget v1, p0, Lcom/google/android/exoplayer/a/b;->s:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/exoplayer/a/b;->s:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/exoplayer/a/b;->t:J

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/exoplayer/a/b;->r:Z

    iget-object v1, p0, Lcom/google/android/exoplayer/a/b;->h:Landroid/os/Handler;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/exoplayer/a/b;->i:Lcom/google/android/exoplayer/a/k;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/exoplayer/a/b;->h:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/exoplayer/a/g;

    invoke-direct {v2, p0, v0}, Lcom/google/android/exoplayer/a/g;-><init>(Lcom/google/android/exoplayer/a/b;Ljava/io/IOException;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    instance-of v0, v0, Lcom/google/android/exoplayer/a/t;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/a/a;->a()V

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/exoplayer/a/b;->r:Z

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->g()V

    :cond_4
    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->k()V

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->h()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    instance-of v1, v1, Lcom/google/android/exoplayer/a/t;

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    invoke-virtual {v1}, Lcom/google/android/exoplayer/a/a;->a()V

    :cond_5
    iget-boolean v1, p0, Lcom/google/android/exoplayer/a/b;->r:Z

    if-nez v1, :cond_6

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->g()V

    :cond_6
    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->k()V

    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->h()V

    throw v0
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    instance-of v0, v0, Lcom/google/android/exoplayer/a/t;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->p:Lcom/google/android/exoplayer/a/a;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/a/a;->a()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/exoplayer/a/b;->g()V

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->h:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->i:Lcom/google/android/exoplayer/a/k;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->h:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/a/e;

    invoke-direct {v1, p0}, Lcom/google/android/exoplayer/a/e;-><init>(Lcom/google/android/exoplayer/a/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    iget v0, p0, Lcom/google/android/exoplayer/a/b;->j:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-wide v0, p0, Lcom/google/android/exoplayer/a/b;->m:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer/a/b;->c(J)V

    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/a/b;->a(Lcom/google/android/exoplayer/a/t;)V

    iget-object v0, p0, Lcom/google/android/exoplayer/a/b;->b:Lcom/google/android/exoplayer/m;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/m;->a()V

    goto :goto_0
.end method
