.class final Lcom/google/android/exoplayer/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/os/Handler;

.field private final c:Landroid/os/HandlerThread;

.field private final d:Landroid/os/Handler;

.field private final e:Lcom/google/android/exoplayer/q;

.field private final f:[Z

.field private final g:J

.field private final h:J

.field private final i:Ljava/util/List;

.field private j:[Lcom/google/android/exoplayer/ak;

.field private k:Lcom/google/android/exoplayer/ak;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:I

.field private p:I

.field private q:I

.field private volatile r:J

.field private volatile s:J

.field private volatile t:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/exoplayer/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer/j;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Z[ZII)V
    .locals 7

    const-wide/16 v5, 0x3e8

    const-wide/16 v3, -0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/exoplayer/j;->p:I

    iput v0, p0, Lcom/google/android/exoplayer/j;->q:I

    iput-object p1, p0, Lcom/google/android/exoplayer/j;->d:Landroid/os/Handler;

    iput-boolean p2, p0, Lcom/google/android/exoplayer/j;->m:Z

    array-length v1, p3

    new-array v1, v1, [Z

    iput-object v1, p0, Lcom/google/android/exoplayer/j;->f:[Z

    int-to-long v1, p4

    mul-long/2addr v1, v5

    iput-wide v1, p0, Lcom/google/android/exoplayer/j;->g:J

    int-to-long v1, p5

    mul-long/2addr v1, v5

    iput-wide v1, p0, Lcom/google/android/exoplayer/j;->h:J

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer/j;->f:[Z

    aget-boolean v2, p3, v0

    aput-boolean v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer/j;->o:I

    iput-wide v3, p0, Lcom/google/android/exoplayer/j;->r:J

    iput-wide v3, p0, Lcom/google/android/exoplayer/j;->t:J

    new-instance v0, Lcom/google/android/exoplayer/q;

    invoke-direct {v0}, Lcom/google/android/exoplayer/q;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/j;->e:Lcom/google/android/exoplayer/q;

    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/j;->i:Ljava/util/List;

    new-instance v0, Lcom/google/android/exoplayer/k;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":Handler"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/exoplayer/k;-><init>(Lcom/google/android/exoplayer/j;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/exoplayer/j;->c:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/exoplayer/j;->c:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    return-void
.end method

.method private a(IJJ)V
    .locals 4

    add-long v0, p2, p4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    invoke-virtual {v2, p1, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private a(Lcom/google/android/exoplayer/ak;)Z
    .locals 12

    const-wide/16 v10, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/google/android/exoplayer/ak;->i()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/exoplayer/ak;->j()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/google/android/exoplayer/j;->o:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    invoke-virtual {p1}, Lcom/google/android/exoplayer/ak;->e()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/google/android/exoplayer/ak;->f()J

    move-result-wide v6

    iget-boolean v2, p0, Lcom/google/android/exoplayer/j;->n:Z

    if-eqz v2, :cond_4

    iget-wide v2, p0, Lcom/google/android/exoplayer/j;->h:J

    :goto_1
    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-lez v8, :cond_0

    cmp-long v8, v6, v10

    if-eqz v8, :cond_0

    const-wide/16 v8, -0x3

    cmp-long v8, v6, v8

    if-eqz v8, :cond_0

    iget-wide v8, p0, Lcom/google/android/exoplayer/j;->s:J

    add-long/2addr v2, v8

    cmp-long v2, v6, v2

    if-gez v2, :cond_0

    cmp-long v2, v4, v10

    if-eqz v2, :cond_3

    const-wide/16 v2, -0x2

    cmp-long v2, v4, v2

    if-eqz v2, :cond_3

    cmp-long v2, v6, v4

    if-gez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-wide v2, p0, Lcom/google/android/exoplayer/j;->g:J

    goto :goto_1
.end method

.method private b(I)V
    .locals 3

    iget v0, p0, Lcom/google/android/exoplayer/j;->o:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/exoplayer/j;->o:I

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->d:Landroid/os/Handler;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method

.method private static b(Lcom/google/android/exoplayer/ak;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/exoplayer/ak;->p()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer/ak;->s()V

    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer/j;->n:Z

    iget-object v1, p0, Lcom/google/android/exoplayer/j;->e:Lcom/google/android/exoplayer/q;

    invoke-virtual {v1}, Lcom/google/android/exoplayer/q;->a()V

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer/j;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/ak;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/ak;->r()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->e:Lcom/google/android/exoplayer/q;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/q;->b()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer/j;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/ak;

    invoke-static {v0}, Lcom/google/android/exoplayer/j;->b(Lcom/google/android/exoplayer/ak;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->k:Lcom/google/android/exoplayer/ak;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->i:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/exoplayer/j;->k:Lcom/google/android/exoplayer/ak;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->k:Lcom/google/android/exoplayer/ak;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/ak;->d()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lcom/google/android/exoplayer/j;->s:J

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/j;->e:Lcom/google/android/exoplayer/q;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/q;->c()J

    move-result-wide v0

    goto :goto_0
.end method

.method private h()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer/j;->n:Z

    invoke-direct {p0}, Lcom/google/android/exoplayer/j;->i()V

    return-void
.end method

.method private i()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->e:Lcom/google/android/exoplayer/q;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/q;->b()V

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->j:[Lcom/google/android/exoplayer/ak;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/exoplayer/j;->j:[Lcom/google/android/exoplayer/ak;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    :try_start_0
    iget-object v1, p0, Lcom/google/android/exoplayer/j;->j:[Lcom/google/android/exoplayer/ak;

    aget-object v1, v1, v0

    invoke-static {v1}, Lcom/google/android/exoplayer/j;->b(Lcom/google/android/exoplayer/ak;)V

    invoke-virtual {v1}, Lcom/google/android/exoplayer/ak;->p()I

    move-result v2

    if-ne v2, v4, :cond_1

    invoke-virtual {v1}, Lcom/google/android/exoplayer/ak;->t()V

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/exoplayer/ak;->u()V
    :try_end_0
    .catch Lcom/google/android/exoplayer/ExoPlaybackException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    sget-object v2, Lcom/google/android/exoplayer/j;->a:Ljava/lang/String;

    const-string v3, "Stop failed."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :catch_1
    move-exception v1

    sget-object v2, Lcom/google/android/exoplayer/j;->a:Ljava/lang/String;

    const-string v3, "Stop failed."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_2
    iput-object v5, p0, Lcom/google/android/exoplayer/j;->j:[Lcom/google/android/exoplayer/ak;

    iput-object v5, p0, Lcom/google/android/exoplayer/j;->k:Lcom/google/android/exoplayer/ak;

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/j;->b(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 4

    iget-wide v0, p0, Lcom/google/android/exoplayer/j;->s:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public final a(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public final a(IZ)V
    .locals 3

    iget-object v1, p0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    const/16 v2, 0x8

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, p1, v0}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/exoplayer/e;ILjava/lang/Object;)V
    .locals 4

    iget v0, p0, Lcom/google/android/exoplayer/j;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer/j;->p:I

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    const/16 v1, 0x9

    const/4 v2, 0x0

    invoke-static {p1, p3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-virtual {v0, v1, p2, v2, v3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public final a(Z)V
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    const/4 v3, 0x3

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final varargs a([Lcom/google/android/exoplayer/ak;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public final b()I
    .locals 4

    iget-wide v0, p0, Lcom/google/android/exoplayer/j;->t:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/exoplayer/j;->t:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method

.method public final declared-synchronized b(Lcom/google/android/exoplayer/e;ILjava/lang/Object;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/exoplayer/j;->p:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/exoplayer/j;->p:I

    iget-object v1, p0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    const/16 v2, 0x9

    const/4 v3, 0x0

    invoke-static {p1, p3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-virtual {v1, v2, p2, v3, v4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    :goto_0
    iget v1, p0, Lcom/google/android/exoplayer/j;->q:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gt v1, v0, :cond_0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public final c()I
    .locals 4

    iget-wide v0, p0, Lcom/google/android/exoplayer/j;->r:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/exoplayer/j;->r:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0
.end method

.method public final declared-synchronized d()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer/j;->l:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/exoplayer/j;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_3
    iget-object v0, p0, Lcom/google/android/exoplayer/j;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 17

    :try_start_0
    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :pswitch_0
    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, [Lcom/google/android/exoplayer/ak;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/exoplayer/j;->n:Z

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/exoplayer/j;->j:[Lcom/google/android/exoplayer/ak;

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    array-length v2, v1

    if-ge v3, v2, :cond_2

    aget-object v2, v1, v3

    invoke-virtual {v2}, Lcom/google/android/exoplayer/ak;->k()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/j;->k:Lcom/google/android/exoplayer/ak;

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    aget-object v2, v1, v3

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer/j;->k:Lcom/google/android/exoplayer/ak;

    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    :cond_2
    const/4 v1, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/j;->b(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    const/4 v2, 0x1

    const/4 v1, 0x0

    move v14, v1

    move v1, v2

    move v2, v14

    :goto_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/exoplayer/j;->j:[Lcom/google/android/exoplayer/ak;

    array-length v5, v5

    if-ge v2, v5, :cond_4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/exoplayer/j;->j:[Lcom/google/android/exoplayer/ak;

    aget-object v5, v5, v2

    invoke-virtual {v5}, Lcom/google/android/exoplayer/ak;->p()I

    move-result v5

    if-nez v5, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/exoplayer/j;->j:[Lcom/google/android/exoplayer/ak;

    aget-object v5, v5, v2

    invoke-virtual {v5}, Lcom/google/android/exoplayer/ak;->q()I

    move-result v5

    if-nez v5, :cond_3

    const/4 v1, 0x0

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    if-nez v1, :cond_5

    const/4 v2, 0x2

    const-wide/16 v5, 0xa

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer/j;->a(IJJ)V

    :goto_4
    const/4 v1, 0x1

    goto :goto_0

    :cond_5
    const-wide/16 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x1

    const/4 v1, 0x0

    move v14, v1

    move-wide v15, v2

    move-wide v1, v15

    move v3, v4

    move v4, v5

    move v5, v14

    :goto_5
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/exoplayer/j;->j:[Lcom/google/android/exoplayer/ak;

    array-length v6, v6

    if-ge v5, v6, :cond_a

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/exoplayer/j;->j:[Lcom/google/android/exoplayer/ak;

    aget-object v6, v6, v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/exoplayer/j;->f:[Z

    aget-boolean v7, v7, v5

    if-eqz v7, :cond_6

    invoke-virtual {v6}, Lcom/google/android/exoplayer/ak;->p()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_6

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/google/android/exoplayer/j;->s:J

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v8, v9}, Lcom/google/android/exoplayer/ak;->b(JZ)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/exoplayer/j;->i:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v4, :cond_7

    invoke-virtual {v6}, Lcom/google/android/exoplayer/ak;->i()Z

    move-result v4

    if-eqz v4, :cond_7

    const/4 v4, 0x1

    :goto_6
    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/exoplayer/j;->a(Lcom/google/android/exoplayer/ak;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v3, 0x1

    :goto_7
    const-wide/16 v7, -0x1

    cmp-long v7, v1, v7

    if-eqz v7, :cond_6

    invoke-virtual {v6}, Lcom/google/android/exoplayer/ak;->e()J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v8, v6, v8

    if-nez v8, :cond_9

    const-wide/16 v1, -0x1

    :cond_6
    :goto_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_7
    const/4 v4, 0x0

    goto :goto_6

    :cond_8
    const/4 v3, 0x0

    goto :goto_7

    :cond_9
    const-wide/16 v8, -0x2

    cmp-long v8, v6, v8

    if-eqz v8, :cond_6

    invoke-static {v1, v2, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    goto :goto_8

    :cond_a
    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/google/android/exoplayer/j;->r:J

    if-eqz v4, :cond_c

    const/4 v1, 0x5

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/j;->b(I)V

    :cond_b
    :goto_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lcom/google/android/exoplayer/ExoPlaybackException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_4

    :catch_0
    move-exception v1

    sget-object v2, Lcom/google/android/exoplayer/j;->a:Ljava/lang/String;

    const-string v3, "Internal track renderer error."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/j;->d:Landroid/os/Handler;

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer/j;->h()V

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_c
    if-eqz v3, :cond_d

    const/4 v1, 0x4

    :goto_a
    :try_start_1
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/j;->b(I)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/exoplayer/j;->m:Z

    if-eqz v1, :cond_b

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/j;->o:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_b

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer/j;->e()V
    :try_end_1
    .catch Lcom/google/android/exoplayer/ExoPlaybackException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_9

    :catch_1
    move-exception v1

    sget-object v2, Lcom/google/android/exoplayer/j;->a:Ljava/lang/String;

    const-string v3, "Internal runtime error."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/j;->d:Landroid/os/Handler;

    const/4 v3, 0x3

    new-instance v4, Lcom/google/android/exoplayer/ExoPlaybackException;

    invoke-direct {v4, v1}, Lcom/google/android/exoplayer/ExoPlaybackException;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {v2, v3, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer/j;->h()V

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_d
    const/4 v1, 0x3

    goto :goto_a

    :pswitch_2
    :try_start_2
    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I
    :try_end_2
    .catch Lcom/google/android/exoplayer/ExoPlaybackException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    if-eqz v1, :cond_f

    const/4 v1, 0x1

    :goto_b
    const/4 v2, 0x0

    :try_start_3
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/exoplayer/j;->n:Z

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/exoplayer/j;->m:Z

    if-nez v1, :cond_10

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer/j;->f()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer/j;->g()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_e
    :goto_c
    :try_start_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->d:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
    :try_end_4
    .catch Lcom/google/android/exoplayer/ExoPlaybackException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_f
    const/4 v1, 0x0

    goto :goto_b

    :cond_10
    :try_start_5
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/j;->o:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_11

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer/j;->e()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_c

    :catchall_0
    move-exception v1

    :try_start_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/j;->d:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    throw v1
    :try_end_6
    .catch Lcom/google/android/exoplayer/ExoPlaybackException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_1

    :cond_11
    :try_start_7
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/j;->o:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_e

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_c

    :pswitch_3
    :try_start_8
    const-string v1, "doSomeWork"

    invoke-static {v1}, Lcom/google/android/exoplayer/e/j;->a(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/google/android/exoplayer/j;->r:J

    const-wide/16 v5, -0x1

    cmp-long v1, v1, v5

    if-eqz v1, :cond_12

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/exoplayer/j;->r:J

    :goto_d
    const/4 v8, 0x1

    const/4 v7, 0x1

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer/j;->g()V

    const/4 v1, 0x0

    move v9, v1

    :goto_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v9, v1, :cond_17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->i:Ljava/util/List;

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer/ak;

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/exoplayer/j;->s:J

    invoke-virtual {v1, v10, v11}, Lcom/google/android/exoplayer/ak;->b(J)V

    if-eqz v8, :cond_13

    invoke-virtual {v1}, Lcom/google/android/exoplayer/ak;->i()Z

    move-result v2

    if-eqz v2, :cond_13

    const/4 v2, 0x1

    move v8, v2

    :goto_f
    if-eqz v7, :cond_14

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/j;->a(Lcom/google/android/exoplayer/ak;)Z

    move-result v2

    if-eqz v2, :cond_14

    const/4 v2, 0x1

    move v7, v2

    :goto_10
    const-wide/16 v10, -0x1

    cmp-long v2, v5, v10

    if-eqz v2, :cond_2b

    invoke-virtual {v1}, Lcom/google/android/exoplayer/ak;->e()J

    move-result-wide v10

    invoke-virtual {v1}, Lcom/google/android/exoplayer/ak;->f()J

    move-result-wide v1

    const-wide/16 v12, -0x1

    cmp-long v12, v1, v12

    if-nez v12, :cond_15

    const-wide/16 v1, -0x1

    :goto_11
    add-int/lit8 v5, v9, 0x1

    move v9, v5

    move-wide v5, v1

    goto :goto_e

    :cond_12
    const-wide v5, 0x7fffffffffffffffL

    goto :goto_d

    :cond_13
    const/4 v2, 0x0

    move v8, v2

    goto :goto_f

    :cond_14
    const/4 v2, 0x0

    move v7, v2

    goto :goto_10

    :cond_15
    const-wide/16 v12, -0x3

    cmp-long v12, v1, v12

    if-eqz v12, :cond_2b

    const-wide/16 v12, -0x1

    cmp-long v12, v10, v12

    if-eqz v12, :cond_16

    const-wide/16 v12, -0x2

    cmp-long v12, v10, v12

    if-eqz v12, :cond_16

    cmp-long v10, v1, v10

    if-gez v10, :cond_2b

    :cond_16
    invoke-static {v5, v6, v1, v2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v1

    goto :goto_11

    :cond_17
    move-object/from16 v0, p0

    iput-wide v5, v0, Lcom/google/android/exoplayer/j;->t:J

    if-eqz v8, :cond_1c

    const/4 v1, 0x5

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/j;->b(I)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer/j;->f()V

    :cond_18
    :goto_12
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/exoplayer/j;->m:Z

    if-eqz v1, :cond_19

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/j;->o:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1a

    :cond_19
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/j;->o:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1e

    :cond_1a
    const/4 v2, 0x7

    const-wide/16 v5, 0xa

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer/j;->a(IJJ)V

    :cond_1b
    :goto_13
    invoke-static {}, Lcom/google/android/exoplayer/e/j;->a()V

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_1c
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/j;->o:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1d

    if-eqz v7, :cond_1d

    const/4 v1, 0x4

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/j;->b(I)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/exoplayer/j;->m:Z

    if-eqz v1, :cond_18

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer/j;->e()V

    goto :goto_12

    :cond_1d
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/j;->o:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_18

    if-nez v7, :cond_18

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/exoplayer/j;->m:Z

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/exoplayer/j;->n:Z

    const/4 v1, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/j;->b(I)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer/j;->f()V

    goto :goto_12

    :cond_1e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1b

    const/4 v2, 0x7

    const-wide/16 v5, 0x3e8

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer/j;->a(IJJ)V

    goto :goto_13

    :pswitch_4
    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/exoplayer/j;->n:Z

    int-to-long v1, v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    move-object/from16 v0, p0

    iput-wide v1, v0, Lcom/google/android/exoplayer/j;->s:J

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->e:Lcom/google/android/exoplayer/q;

    invoke-virtual {v1}, Lcom/google/android/exoplayer/q;->b()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->e:Lcom/google/android/exoplayer/q;

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/exoplayer/j;->s:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/exoplayer/q;->a(J)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/j;->o:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1f

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/j;->o:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_20

    :cond_1f
    :goto_14
    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_20
    const/4 v1, 0x0

    move v2, v1

    :goto_15
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_21

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->i:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer/ak;

    invoke-static {v1}, Lcom/google/android/exoplayer/j;->b(Lcom/google/android/exoplayer/ak;)V

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/exoplayer/j;->s:J

    invoke-virtual {v1, v3, v4}, Lcom/google/android/exoplayer/ak;->a(J)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_15

    :cond_21
    const/4 v1, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/j;->b(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_14

    :pswitch_5
    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer/j;->h()V

    const/4 v1, 0x1

    goto/16 :goto_0

    :pswitch_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer/j;->i()V

    monitor-enter p0
    :try_end_8
    .catch Lcom/google/android/exoplayer/ExoPlaybackException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_1

    const/4 v1, 0x1

    :try_start_9
    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/exoplayer/j;->l:Z

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    const/4 v1, 0x1

    goto/16 :goto_0

    :catchall_1
    move-exception v1

    :try_start_a
    monitor-exit p0

    throw v1

    :pswitch_7
    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;
    :try_end_a
    .catch Lcom/google/android/exoplayer/ExoPlaybackException; {:try_start_a .. :try_end_a} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_1

    :try_start_b
    check-cast v1, Landroid/util/Pair;

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/exoplayer/e;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {v2, v3, v1}, Lcom/google/android/exoplayer/e;->a(ILjava/lang/Object;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    :try_start_c
    monitor-enter p0
    :try_end_c
    .catch Lcom/google/android/exoplayer/ExoPlaybackException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_1

    :try_start_d
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/j;->q:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/exoplayer/j;->q:I

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    :try_start_e
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/j;->o:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_22

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_22
    const/4 v1, 0x1

    goto/16 :goto_0

    :catchall_2
    move-exception v1

    monitor-exit p0

    throw v1

    :catchall_3
    move-exception v1

    monitor-enter p0
    :try_end_e
    .catch Lcom/google/android/exoplayer/ExoPlaybackException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_e} :catch_1

    :try_start_f
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/j;->q:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/exoplayer/j;->q:I

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    :try_start_10
    throw v1

    :catchall_4
    move-exception v1

    monitor-exit p0

    throw v1

    :pswitch_8
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg2:I

    if-eqz v1, :cond_24

    const/4 v1, 0x1

    :goto_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/j;->f:[Z

    aget-boolean v3, v3, v2

    if-eq v3, v1, :cond_23

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/j;->f:[Z

    aput-boolean v1, v3, v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/exoplayer/j;->o:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_23

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/exoplayer/j;->o:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_25

    :cond_23
    :goto_17
    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_24
    const/4 v1, 0x0

    goto :goto_16

    :cond_25
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/j;->j:[Lcom/google/android/exoplayer/ak;

    aget-object v2, v3, v2

    invoke-virtual {v2}, Lcom/google/android/exoplayer/ak;->p()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_26

    const/4 v4, 0x2

    if-eq v3, v4, :cond_26

    const/4 v4, 0x3

    if-ne v3, v4, :cond_23

    :cond_26
    if-eqz v1, :cond_29

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/exoplayer/j;->m:Z

    if-eqz v1, :cond_28

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/j;->o:I

    const/4 v3, 0x4

    if-ne v1, v3, :cond_28

    const/4 v1, 0x1

    :goto_18
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/exoplayer/j;->s:J

    invoke-virtual {v2, v3, v4, v1}, Lcom/google/android/exoplayer/ak;->b(JZ)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/j;->i:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v1, :cond_27

    invoke-virtual {v2}, Lcom/google/android/exoplayer/ak;->r()V

    :cond_27
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->b:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_17

    :cond_28
    const/4 v1, 0x0

    goto :goto_18

    :cond_29
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->k:Lcom/google/android/exoplayer/ak;

    if-ne v2, v1, :cond_2a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->e:Lcom/google/android/exoplayer/q;

    invoke-virtual {v2}, Lcom/google/android/exoplayer/ak;->d()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, Lcom/google/android/exoplayer/q;->a(J)V

    :cond_2a
    invoke-static {v2}, Lcom/google/android/exoplayer/j;->b(Lcom/google/android/exoplayer/ak;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/j;->i:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lcom/google/android/exoplayer/ak;->t()V
    :try_end_10
    .catch Lcom/google/android/exoplayer/ExoPlaybackException; {:try_start_10 .. :try_end_10} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_1

    goto :goto_17

    :cond_2b
    move-wide v1, v5

    goto/16 :goto_11

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_3
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method
