.class public final Lcom/google/android/exoplayer/upstream/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/l;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lcom/google/android/exoplayer/upstream/e;

.field private final c:Lcom/google/android/exoplayer/e/b;

.field private final d:Lcom/google/android/exoplayer/e/e;

.field private e:J

.field private f:J

.field private g:J

.field private h:I


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/exoplayer/upstream/e;)V
    .locals 1

    new-instance v0, Lcom/google/android/exoplayer/e/i;

    invoke-direct {v0}, Lcom/google/android/exoplayer/e/i;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/exoplayer/upstream/c;-><init>(Landroid/os/Handler;Lcom/google/android/exoplayer/upstream/e;Lcom/google/android/exoplayer/e/b;)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;Lcom/google/android/exoplayer/upstream/e;Lcom/google/android/exoplayer/e/b;)V
    .locals 1

    const/16 v0, 0x7d0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/exoplayer/upstream/c;-><init>(Landroid/os/Handler;Lcom/google/android/exoplayer/upstream/e;Lcom/google/android/exoplayer/e/b;I)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;Lcom/google/android/exoplayer/upstream/e;Lcom/google/android/exoplayer/e/b;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer/upstream/c;->a:Landroid/os/Handler;

    iput-object p2, p0, Lcom/google/android/exoplayer/upstream/c;->b:Lcom/google/android/exoplayer/upstream/e;

    iput-object p3, p0, Lcom/google/android/exoplayer/upstream/c;->c:Lcom/google/android/exoplayer/e/b;

    new-instance v0, Lcom/google/android/exoplayer/e/e;

    const/16 v1, 0x7d0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/e/e;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/c;->d:Lcom/google/android/exoplayer/e/e;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/c;->g:J

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer/upstream/c;)Lcom/google/android/exoplayer/upstream/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/c;->b:Lcom/google/android/exoplayer/upstream/e;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/exoplayer/upstream/c;->h:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/c;->c:Lcom/google/android/exoplayer/e/b;

    invoke-interface {v0}, Lcom/google/android/exoplayer/e/b;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/c;->f:J

    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer/upstream/c;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer/upstream/c;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/exoplayer/upstream/c;->e:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/c;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 10

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/exoplayer/upstream/c;->h:I

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/c;->c:Lcom/google/android/exoplayer/e/b;

    invoke-interface {v0}, Lcom/google/android/exoplayer/e/b;->a()J

    move-result-wide v7

    iget-wide v0, p0, Lcom/google/android/exoplayer/upstream/c;->f:J

    sub-long v0, v7, v0

    long-to-int v2, v0

    if-lez v2, :cond_0

    iget-wide v0, p0, Lcom/google/android/exoplayer/upstream/c;->e:J

    const-wide/16 v3, 0x3e8

    mul-long/2addr v0, v3

    int-to-long v3, v2

    div-long/2addr v0, v3

    long-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/c;->d:Lcom/google/android/exoplayer/e/e;

    iget-wide v3, p0, Lcom/google/android/exoplayer/upstream/c;->e:J

    long-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    double-to-int v3, v3

    invoke-virtual {v1, v3, v0}, Lcom/google/android/exoplayer/e/e;->a(IF)V

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/c;->d:Lcom/google/android/exoplayer/e/e;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer/e/e;->a(F)F

    move-result v0

    const/high16 v1, 0x7fc00000    # NaNf

    cmpl-float v1, v0, v1

    if-nez v1, :cond_3

    const-wide/16 v0, -0x1

    :goto_1
    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/c;->g:J

    iget-wide v3, p0, Lcom/google/android/exoplayer/upstream/c;->e:J

    iget-wide v5, p0, Lcom/google/android/exoplayer/upstream/c;->g:J

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/c;->a:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/c;->b:Lcom/google/android/exoplayer/upstream/e;

    if-eqz v0, :cond_0

    iget-object v9, p0, Lcom/google/android/exoplayer/upstream/c;->a:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/exoplayer/upstream/d;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/upstream/d;-><init>(Lcom/google/android/exoplayer/upstream/c;IJJ)V

    invoke-virtual {v9, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer/upstream/c;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer/upstream/c;->h:I

    iget v0, p0, Lcom/google/android/exoplayer/upstream/c;->h:I

    if-lez v0, :cond_1

    iput-wide v7, p0, Lcom/google/android/exoplayer/upstream/c;->f:J

    :cond_1
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/c;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    float-to-long v0, v0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/exoplayer/upstream/c;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
