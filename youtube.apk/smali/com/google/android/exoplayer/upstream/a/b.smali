.class public final Lcom/google/android/exoplayer/upstream/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/i;


# instance fields
.field private final a:Lcom/google/android/exoplayer/upstream/i;

.field private final b:[B

.field private c:Lcom/google/android/exoplayer/upstream/a/c;


# direct methods
.method public constructor <init>([BLcom/google/android/exoplayer/upstream/i;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/exoplayer/upstream/a/b;->a:Lcom/google/android/exoplayer/upstream/i;

    iput-object p1, p0, Lcom/google/android/exoplayer/upstream/a/b;->b:[B

    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/a/b;->a:Lcom/google/android/exoplayer/upstream/i;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/exoplayer/upstream/i;->a([BII)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/a/b;->c:Lcom/google/android/exoplayer/upstream/a/c;

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/exoplayer/upstream/a/c;->a([BII)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/exoplayer/upstream/j;)J
    .locals 9

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/a/b;->a:Lcom/google/android/exoplayer/upstream/i;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer/upstream/i;->a(Lcom/google/android/exoplayer/upstream/j;)J

    move-result-wide v7

    iget-object v0, p1, Lcom/google/android/exoplayer/upstream/j;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/exoplayer/upstream/a/d;->a(Ljava/lang/String;)J

    move-result-wide v3

    new-instance v0, Lcom/google/android/exoplayer/upstream/a/c;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/exoplayer/upstream/a/b;->b:[B

    iget-wide v5, p1, Lcom/google/android/exoplayer/upstream/j;->c:J

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/upstream/a/c;-><init>(I[BJJ)V

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/a/b;->c:Lcom/google/android/exoplayer/upstream/a/c;

    return-wide v7
.end method

.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/a/b;->c:Lcom/google/android/exoplayer/upstream/a/c;

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/a/b;->a:Lcom/google/android/exoplayer/upstream/i;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/i;->a()V

    return-void
.end method
