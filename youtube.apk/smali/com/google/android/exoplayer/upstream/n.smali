.class final Lcom/google/android/exoplayer/upstream/n;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/exoplayer/upstream/Loader;

.field private final b:Lcom/google/android/exoplayer/upstream/o;

.field private volatile c:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/Loader;Lcom/google/android/exoplayer/upstream/o;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/exoplayer/upstream/n;->a:Lcom/google/android/exoplayer/upstream/Loader;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    iput-object p2, p0, Lcom/google/android/exoplayer/upstream/n;->b:Lcom/google/android/exoplayer/upstream/o;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/n;->b:Lcom/google/android/exoplayer/upstream/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/o;->g()V

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/n;->c:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/n;->c:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_0
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/n;->a:Lcom/google/android/exoplayer/upstream/Loader;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/exoplayer/upstream/Loader;->a(Lcom/google/android/exoplayer/upstream/Loader;Z)Z

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/n;->a:Lcom/google/android/exoplayer/upstream/Loader;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/exoplayer/upstream/Loader;->a(Lcom/google/android/exoplayer/upstream/Loader;Lcom/google/android/exoplayer/upstream/n;)Lcom/google/android/exoplayer/upstream/n;

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/n;->b:Lcom/google/android/exoplayer/upstream/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/o;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/n;->a:Lcom/google/android/exoplayer/upstream/Loader;

    invoke-static {v0}, Lcom/google/android/exoplayer/upstream/Loader;->a(Lcom/google/android/exoplayer/upstream/Loader;)Lcom/google/android/exoplayer/upstream/m;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/m;->f()V

    :goto_0
    return-void

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/n;->a:Lcom/google/android/exoplayer/upstream/Loader;

    invoke-static {v0}, Lcom/google/android/exoplayer/upstream/Loader;->a(Lcom/google/android/exoplayer/upstream/Loader;)Lcom/google/android/exoplayer/upstream/m;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/m;->e()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/n;->a:Lcom/google/android/exoplayer/upstream/Loader;

    invoke-static {v0}, Lcom/google/android/exoplayer/upstream/Loader;->a(Lcom/google/android/exoplayer/upstream/Loader;)Lcom/google/android/exoplayer/upstream/m;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/io/IOException;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer/upstream/m;->a(Ljava/io/IOException;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final run()V
    .locals 4

    const/4 v3, 0x1

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/n;->c:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/n;->b:Lcom/google/android/exoplayer/upstream/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/o;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/n;->b:Lcom/google/android/exoplayer/upstream/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/o;->i()V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/upstream/n;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0, v3, v0}, Lcom/google/android/exoplayer/upstream/n;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/n;->b:Lcom/google/android/exoplayer/upstream/o;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/o;->h()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer/upstream/n;->sendEmptyMessage(I)Z

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v1, "LoadTask"

    const-string v2, "Unexpected error loading stream"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v1, Lcom/google/android/exoplayer/upstream/Loader$UnexpectedLoaderException;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer/upstream/Loader$UnexpectedLoaderException;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {p0, v3, v1}, Lcom/google/android/exoplayer/upstream/n;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method
