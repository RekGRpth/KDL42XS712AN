.class public final Lcom/google/android/exoplayer/upstream/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/b;


# instance fields
.field public final a:I

.field private b:I

.field private c:I

.field private d:[[B


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->a(Z)V

    iput p1, p0, Lcom/google/android/exoplayer/upstream/f;->a:I

    const/16 v0, 0x64

    new-array v0, v0, [[B

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/f;->d:[[B

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()I
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/exoplayer/upstream/f;->b:I

    iget v1, p0, Lcom/google/android/exoplayer/upstream/f;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    mul-int/2addr v0, v1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)Lcom/google/android/exoplayer/upstream/a;
    .locals 5

    monitor-enter p0

    int-to-long v0, p1

    :try_start_0
    iget v2, p0, Lcom/google/android/exoplayer/upstream/f;->a:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iget v2, p0, Lcom/google/android/exoplayer/upstream/f;->a:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v2, v0

    iget v0, p0, Lcom/google/android/exoplayer/upstream/f;->b:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/exoplayer/upstream/f;->b:I

    new-array v3, v2, [[B

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget v0, p0, Lcom/google/android/exoplayer/upstream/f;->c:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/f;->d:[[B

    iget v4, p0, Lcom/google/android/exoplayer/upstream/f;->c:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/google/android/exoplayer/upstream/f;->c:I

    aget-object v0, v0, v4

    :goto_1
    aput-object v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer/upstream/f;->a:I

    new-array v0, v0, [B

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/google/android/exoplayer/upstream/g;

    invoke-direct {v0, p0, v3}, Lcom/google/android/exoplayer/upstream/g;-><init>(Lcom/google/android/exoplayer/upstream/f;[[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/exoplayer/upstream/g;)V
    .locals 7

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/exoplayer/upstream/g;->a()[[B

    move-result-object v0

    iget v1, p0, Lcom/google/android/exoplayer/upstream/f;->b:I

    array-length v2, v0

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/exoplayer/upstream/f;->b:I

    iget v1, p0, Lcom/google/android/exoplayer/upstream/f;->c:I

    array-length v2, v0

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/exoplayer/upstream/f;->d:[[B

    array-length v2, v2

    if-ge v2, v1, :cond_1

    mul-int/lit8 v2, v1, 0x2

    new-array v2, v2, [[B

    iget v3, p0, Lcom/google/android/exoplayer/upstream/f;->c:I

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/exoplayer/upstream/f;->d:[[B

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/exoplayer/upstream/f;->c:I

    invoke-static {v3, v4, v2, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_0
    iput-object v2, p0, Lcom/google/android/exoplayer/upstream/f;->d:[[B

    :cond_1
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/exoplayer/upstream/f;->d:[[B

    iget v4, p0, Lcom/google/android/exoplayer/upstream/f;->c:I

    array-length v5, v0

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput v1, p0, Lcom/google/android/exoplayer/upstream/f;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(I)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/exoplayer/upstream/f;->a:I

    add-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/google/android/exoplayer/upstream/f;->a:I

    div-int/2addr v0, v1

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/exoplayer/upstream/f;->b:I

    sub-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p0, Lcom/google/android/exoplayer/upstream/f;->c:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/f;->d:[[B

    iget v2, p0, Lcom/google/android/exoplayer/upstream/f;->c:I

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    iput v0, p0, Lcom/google/android/exoplayer/upstream/f;->c:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
