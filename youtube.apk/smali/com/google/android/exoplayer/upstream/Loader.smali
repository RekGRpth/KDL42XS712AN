.class public final Lcom/google/android/exoplayer/upstream/Loader;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:Lcom/google/android/exoplayer/upstream/m;

.field private c:Lcom/google/android/exoplayer/upstream/n;

.field private d:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/exoplayer/upstream/m;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/exoplayer/e/k;->c(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/Loader;->a:Ljava/util/concurrent/ExecutorService;

    iput-object p2, p0, Lcom/google/android/exoplayer/upstream/Loader;->b:Lcom/google/android/exoplayer/upstream/m;

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer/upstream/Loader;)Lcom/google/android/exoplayer/upstream/m;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/Loader;->b:Lcom/google/android/exoplayer/upstream/m;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/exoplayer/upstream/Loader;Lcom/google/android/exoplayer/upstream/n;)Lcom/google/android/exoplayer/upstream/n;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/Loader;->c:Lcom/google/android/exoplayer/upstream/n;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/exoplayer/upstream/Loader;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer/upstream/Loader;->d:Z

    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/exoplayer/upstream/o;)V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/exoplayer/upstream/Loader;->d:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iput-boolean v1, p0, Lcom/google/android/exoplayer/upstream/Loader;->d:Z

    new-instance v0, Lcom/google/android/exoplayer/upstream/n;

    invoke-direct {v0, p0, p1}, Lcom/google/android/exoplayer/upstream/n;-><init>(Lcom/google/android/exoplayer/upstream/Loader;Lcom/google/android/exoplayer/upstream/o;)V

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/Loader;->c:Lcom/google/android/exoplayer/upstream/n;

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/Loader;->a:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/Loader;->c:Lcom/google/android/exoplayer/upstream/n;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer/upstream/Loader;->d:Z

    return v0
.end method

.method public final b()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer/upstream/Loader;->d:Z

    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/Loader;->c:Lcom/google/android/exoplayer/upstream/n;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/upstream/n;->a()V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer/upstream/Loader;->d:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer/upstream/Loader;->b()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/Loader;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    return-void
.end method
