.class public final Lcom/google/android/exoplayer/upstream/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/i;


# instance fields
.field private final a:Lcom/google/android/exoplayer/upstream/i;

.field private final b:Lcom/google/android/exoplayer/upstream/h;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/h;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/exoplayer/e/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/upstream/i;

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/q;->a:Lcom/google/android/exoplayer/upstream/i;

    invoke-static {p2}, Lcom/google/android/exoplayer/e/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/upstream/h;

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/q;->b:Lcom/google/android/exoplayer/upstream/h;

    return-void
.end method


# virtual methods
.method public final a([BII)I
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/q;->a:Lcom/google/android/exoplayer/upstream/i;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/exoplayer/upstream/i;->a([BII)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/q;->b:Lcom/google/android/exoplayer/upstream/h;

    invoke-interface {v1, p1, p2, v0}, Lcom/google/android/exoplayer/upstream/h;->a([BII)V

    :cond_0
    return v0
.end method

.method public final a(Lcom/google/android/exoplayer/upstream/j;)J
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/q;->a:Lcom/google/android/exoplayer/upstream/i;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer/upstream/i;->a(Lcom/google/android/exoplayer/upstream/j;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/exoplayer/upstream/q;->b:Lcom/google/android/exoplayer/upstream/h;

    invoke-interface {v2, p1}, Lcom/google/android/exoplayer/upstream/h;->a(Lcom/google/android/exoplayer/upstream/j;)Lcom/google/android/exoplayer/upstream/h;

    return-wide v0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/q;->a:Lcom/google/android/exoplayer/upstream/i;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/i;->a()V

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/q;->b:Lcom/google/android/exoplayer/upstream/h;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/h;->a()V

    return-void
.end method
