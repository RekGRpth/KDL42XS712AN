.class public final Lcom/google/android/exoplayer/upstream/cache/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/cache/c;
.implements Ljava/util/Comparator;


# instance fields
.field private final a:J

.field private final b:Ljava/util/TreeSet;

.field private c:J


# direct methods
.method public constructor <init>(J)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/32 v0, 0x4000000

    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/cache/e;->a:J

    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0, p0}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/e;->b:Ljava/util/TreeSet;

    return-void
.end method

.method private b(Lcom/google/android/exoplayer/upstream/cache/a;J)V
    .locals 4

    :goto_0
    iget-wide v0, p0, Lcom/google/android/exoplayer/upstream/cache/e;->c:J

    add-long/2addr v0, p2

    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/cache/e;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/e;->b:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/upstream/cache/d;

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer/upstream/cache/a;->b(Lcom/google/android/exoplayer/upstream/cache/d;)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/exoplayer/upstream/cache/a;J)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/exoplayer/upstream/cache/e;->b(Lcom/google/android/exoplayer/upstream/cache/a;J)V

    return-void
.end method

.method public final a(Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/cache/d;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/e;->b:Ljava/util/TreeSet;

    invoke-virtual {v0, p2}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    iget-wide v0, p0, Lcom/google/android/exoplayer/upstream/cache/e;->c:J

    iget-wide v2, p2, Lcom/google/android/exoplayer/upstream/cache/d;->c:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/cache/e;->c:J

    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/exoplayer/upstream/cache/e;->b(Lcom/google/android/exoplayer/upstream/cache/a;J)V

    return-void
.end method

.method public final a(Lcom/google/android/exoplayer/upstream/cache/d;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/e;->b:Ljava/util/TreeSet;

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    iget-wide v0, p0, Lcom/google/android/exoplayer/upstream/cache/e;->c:J

    iget-wide v2, p1, Lcom/google/android/exoplayer/upstream/cache/d;->c:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/cache/e;->c:J

    return-void
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    check-cast p1, Lcom/google/android/exoplayer/upstream/cache/d;

    check-cast p2, Lcom/google/android/exoplayer/upstream/cache/d;

    iget-wide v0, p1, Lcom/google/android/exoplayer/upstream/cache/d;->f:J

    iget-wide v2, p2, Lcom/google/android/exoplayer/upstream/cache/d;->f:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p1, p2}, Lcom/google/android/exoplayer/upstream/cache/d;->a(Lcom/google/android/exoplayer/upstream/cache/d;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p1, Lcom/google/android/exoplayer/upstream/cache/d;->f:J

    iget-wide v2, p2, Lcom/google/android/exoplayer/upstream/cache/d;->f:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
