.class public final Lcom/google/android/exoplayer/upstream/cache/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/cache/a;


# instance fields
.field private final a:Ljava/io/File;

.field private final b:Lcom/google/android/exoplayer/upstream/cache/c;

.field private final c:Ljava/util/HashMap;

.field private final d:Ljava/util/HashMap;

.field private e:J


# direct methods
.method public constructor <init>(Ljava/io/File;Lcom/google/android/exoplayer/upstream/cache/c;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->e:J

    iput-object p1, p0, Lcom/google/android/exoplayer/upstream/cache/g;->a:Ljava/io/File;

    iput-object p2, p0, Lcom/google/android/exoplayer/upstream/cache/g;->b:Lcom/google/android/exoplayer/upstream/cache/c;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->c:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->d:Ljava/util/HashMap;

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    new-instance v1, Lcom/google/android/exoplayer/upstream/cache/h;

    invoke-direct {v1, p0, v0}, Lcom/google/android/exoplayer/upstream/cache/h;-><init>(Lcom/google/android/exoplayer/upstream/cache/g;Landroid/os/ConditionVariable;)V

    invoke-virtual {v1}, Lcom/google/android/exoplayer/upstream/cache/h;->start()V

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/android/exoplayer/upstream/cache/d;)Lcom/google/android/exoplayer/upstream/cache/d;
    .locals 6

    invoke-direct {p0, p2}, Lcom/google/android/exoplayer/upstream/cache/g;->c(Lcom/google/android/exoplayer/upstream/cache/d;)Lcom/google/android/exoplayer/upstream/cache/d;

    move-result-object v1

    iget-boolean v0, v1, Lcom/google/android/exoplayer/upstream/cache/d;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->d:Ljava/util/HashMap;

    iget-object v2, v1, Lcom/google/android/exoplayer/upstream/cache/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v2}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/cache/g;->e:J

    iget-wide v4, v1, Lcom/google/android/exoplayer/upstream/cache/d;->c:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/exoplayer/upstream/cache/g;->e:J

    iget-object v2, p0, Lcom/google/android/exoplayer/upstream/cache/g;->b:Lcom/google/android/exoplayer/upstream/cache/c;

    invoke-interface {v2, v1}, Lcom/google/android/exoplayer/upstream/cache/c;->a(Lcom/google/android/exoplayer/upstream/cache/d;)V

    invoke-virtual {v1}, Lcom/google/android/exoplayer/upstream/cache/d;->b()Lcom/google/android/exoplayer/upstream/cache/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    iget-wide v2, p0, Lcom/google/android/exoplayer/upstream/cache/g;->e:J

    iget-wide v4, v1, Lcom/google/android/exoplayer/upstream/cache/d;->c:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/exoplayer/upstream/cache/g;->e:J

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->b:Lcom/google/android/exoplayer/upstream/cache/c;

    invoke-interface {v0, p0, v1}, Lcom/google/android/exoplayer/upstream/cache/c;->a(Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/cache/d;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/exoplayer/upstream/cache/g;)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_3

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_3

    aget-object v2, v1, v0

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-static {v2}, Lcom/google/android/exoplayer/upstream/cache/d;->a(Ljava/io/File;)Lcom/google/android/exoplayer/upstream/cache/d;

    move-result-object v3

    if-nez v3, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_1

    :cond_2
    invoke-direct {p0, v3}, Lcom/google/android/exoplayer/upstream/cache/g;->d(Lcom/google/android/exoplayer/upstream/cache/d;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method private c(Lcom/google/android/exoplayer/upstream/cache/d;)Lcom/google/android/exoplayer/upstream/cache/d;
    .locals 9

    :goto_0
    iget-object v2, p1, Lcom/google/android/exoplayer/upstream/cache/d;->a:Ljava/lang/String;

    iget-wide v3, p1, Lcom/google/android/exoplayer/upstream/cache/d;->b:J

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->d:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    if-nez v0, :cond_1

    iget-wide v0, p1, Lcom/google/android/exoplayer/upstream/cache/d;->b:J

    invoke-static {v2, v0, v1}, Lcom/google/android/exoplayer/upstream/cache/d;->b(Ljava/lang/String;J)Lcom/google/android/exoplayer/upstream/cache/d;

    move-result-object v1

    :cond_0
    :goto_1
    return-object v1

    :cond_1
    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer/upstream/cache/d;

    if-eqz v1, :cond_2

    iget-wide v5, v1, Lcom/google/android/exoplayer/upstream/cache/d;->b:J

    cmp-long v5, v5, v3

    if-gtz v5, :cond_2

    iget-wide v5, v1, Lcom/google/android/exoplayer/upstream/cache/d;->b:J

    iget-wide v7, v1, Lcom/google/android/exoplayer/upstream/cache/d;->c:J

    add-long/2addr v5, v7

    cmp-long v3, v3, v5

    if-gez v3, :cond_2

    iget-object v0, v1, Lcom/google/android/exoplayer/upstream/cache/d;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer/upstream/cache/g;->c()V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->ceiling(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/upstream/cache/d;

    if-nez v0, :cond_3

    iget-wide v0, p1, Lcom/google/android/exoplayer/upstream/cache/d;->b:J

    invoke-static {v2, v0, v1}, Lcom/google/android/exoplayer/upstream/cache/d;->b(Ljava/lang/String;J)Lcom/google/android/exoplayer/upstream/cache/d;

    move-result-object v1

    goto :goto_1

    :cond_3
    iget-wide v3, p1, Lcom/google/android/exoplayer/upstream/cache/d;->b:J

    iget-wide v0, v0, Lcom/google/android/exoplayer/upstream/cache/d;->b:J

    iget-wide v5, p1, Lcom/google/android/exoplayer/upstream/cache/d;->b:J

    sub-long/2addr v0, v5

    invoke-static {v2, v3, v4, v0, v1}, Lcom/google/android/exoplayer/upstream/cache/d;->a(Ljava/lang/String;JJ)Lcom/google/android/exoplayer/upstream/cache/d;

    move-result-object v1

    goto :goto_1
.end method

.method private c()V
    .locals 8

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v0, 0x1

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/upstream/cache/d;

    iget-object v4, v0, Lcom/google/android/exoplayer/upstream/cache/d;->e:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    iget-boolean v4, v0, Lcom/google/android/exoplayer/upstream/cache/d;->d:Z

    if-eqz v4, :cond_1

    iget-wide v4, p0, Lcom/google/android/exoplayer/upstream/cache/g;->e:J

    iget-wide v6, v0, Lcom/google/android/exoplayer/upstream/cache/d;->c:J

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lcom/google/android/exoplayer/upstream/cache/g;->e:J

    :cond_1
    iget-object v4, p0, Lcom/google/android/exoplayer/upstream/cache/g;->b:Lcom/google/android/exoplayer/upstream/cache/c;

    invoke-interface {v4, v0}, Lcom/google/android/exoplayer/upstream/cache/c;->a(Lcom/google/android/exoplayer/upstream/cache/d;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    :cond_3
    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_4
    return-void
.end method

.method private d(Lcom/google/android/exoplayer/upstream/cache/d;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->d:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/google/android/exoplayer/upstream/cache/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/g;->d:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/google/android/exoplayer/upstream/cache/d;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    iget-wide v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->e:J

    iget-wide v2, p1, Lcom/google/android/exoplayer/upstream/cache/d;->c:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->e:J

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->b:Lcom/google/android/exoplayer/upstream/cache/c;

    invoke-interface {v0, p0, p1}, Lcom/google/android/exoplayer/upstream/cache/c;->a(Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/cache/d;)V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;J)Lcom/google/android/exoplayer/upstream/cache/d;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {p1, p2, p3}, Lcom/google/android/exoplayer/upstream/cache/d;->a(Ljava/lang/String;J)Lcom/google/android/exoplayer/upstream/cache/d;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/g;->c:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer/upstream/cache/g;->a(Ljava/lang/String;Lcom/google/android/exoplayer/upstream/cache/d;)Lcom/google/android/exoplayer/upstream/cache/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;JJ)Ljava/io/File;
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer/upstream/cache/g;->c()V

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->b:Lcom/google/android/exoplayer/upstream/cache/c;

    invoke-interface {v0, p0, p4, p5}, Lcom/google/android/exoplayer/upstream/cache/c;->a(Lcom/google/android/exoplayer/upstream/cache/a;J)V

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->a:Ljava/io/File;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object v1, p1

    move-wide v2, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/exoplayer/upstream/cache/d;->a(Ljava/io/File;Ljava/lang/String;JJ)Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Ljava/util/Set;
    .locals 2

    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/g;->d:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Ljava/util/SortedSet;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/SortedSet;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/exoplayer/upstream/cache/d;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->c:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/google/android/exoplayer/upstream/cache/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/io/File;)V
    .locals 6

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/exoplayer/upstream/cache/d;->a(Ljava/io/File;)Lcom/google/android/exoplayer/upstream/cache/d;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->c:Ljava/util/HashMap;

    iget-object v2, v1, Lcom/google/android/exoplayer/upstream/cache/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    invoke-virtual {p1}, Ljava/io/File;->exists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    invoke-virtual {p1}, Ljava/io/File;->delete()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    invoke-direct {p0, v1}, Lcom/google/android/exoplayer/upstream/cache/g;->d(Lcom/google/android/exoplayer/upstream/cache/d;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized b()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;J)Lcom/google/android/exoplayer/upstream/cache/d;
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {p1, p2, p3}, Lcom/google/android/exoplayer/upstream/cache/d;->a(Ljava/lang/String;J)Lcom/google/android/exoplayer/upstream/cache/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer/upstream/cache/g;->c:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer/upstream/cache/g;->a(Ljava/lang/String;Lcom/google/android/exoplayer/upstream/cache/d;)Lcom/google/android/exoplayer/upstream/cache/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/exoplayer/upstream/cache/d;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->d:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/google/android/exoplayer/upstream/cache/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    iget-wide v1, p0, Lcom/google/android/exoplayer/upstream/cache/g;->e:J

    iget-wide v3, p1, Lcom/google/android/exoplayer/upstream/cache/d;->c:J

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/android/exoplayer/upstream/cache/g;->e:J

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iget-object v1, p1, Lcom/google/android/exoplayer/upstream/cache/d;->e:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->d:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/google/android/exoplayer/upstream/cache/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/upstream/cache/g;->b:Lcom/google/android/exoplayer/upstream/cache/c;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer/upstream/cache/c;->a(Lcom/google/android/exoplayer/upstream/cache/d;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
