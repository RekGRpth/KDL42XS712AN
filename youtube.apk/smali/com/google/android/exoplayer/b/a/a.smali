.class public final Lcom/google/android/exoplayer/b/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Lcom/google/android/exoplayer/a/m;

.field public final d:J

.field public final e:J

.field public final f:J

.field public final g:J

.field public final h:J

.field public final i:J

.field public final j:J

.field public final k:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/lang/String;JLcom/google/android/exoplayer/a/m;Landroid/net/Uri;JJJJJJJ)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer/b/a/a;->a:Ljava/lang/String;

    iput-wide p2, p0, Lcom/google/android/exoplayer/b/a/a;->b:J

    iput-object p4, p0, Lcom/google/android/exoplayer/b/a/a;->c:Lcom/google/android/exoplayer/a/m;

    iput-wide p6, p0, Lcom/google/android/exoplayer/b/a/a;->d:J

    iput-wide p8, p0, Lcom/google/android/exoplayer/b/a/a;->e:J

    iput-wide p10, p0, Lcom/google/android/exoplayer/b/a/a;->f:J

    move-wide/from16 v0, p12

    iput-wide v0, p0, Lcom/google/android/exoplayer/b/a/a;->g:J

    move-wide/from16 v0, p14

    iput-wide v0, p0, Lcom/google/android/exoplayer/b/a/a;->h:J

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/exoplayer/b/a/a;->i:J

    move-wide/from16 v0, p18

    iput-wide v0, p0, Lcom/google/android/exoplayer/b/a/a;->j:J

    iput-object p5, p0, Lcom/google/android/exoplayer/b/a/a;->k:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/exoplayer/b/a/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer/b/a/a;->c:Lcom/google/android/exoplayer/a/m;

    iget v1, v1, Lcom/google/android/exoplayer/a/m;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/exoplayer/b/a/a;->b:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
