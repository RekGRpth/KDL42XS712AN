.class final Lcom/google/android/exoplayer/b/b;
.super Lcom/google/android/exoplayer/a/a;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/android/exoplayer/b/a/a;

.field private final e:Lcom/google/android/exoplayer/d/a/f;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/j;ILcom/google/android/exoplayer/d/a/f;Lcom/google/android/exoplayer/b/a/a;)V
    .locals 1

    iget-object v0, p5, Lcom/google/android/exoplayer/b/a/a;->c:Lcom/google/android/exoplayer/a/m;

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/exoplayer/a/a;-><init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/j;Lcom/google/android/exoplayer/a/m;I)V

    iput-object p4, p0, Lcom/google/android/exoplayer/b/b;->e:Lcom/google/android/exoplayer/d/a/f;

    iput-object p5, p0, Lcom/google/android/exoplayer/b/b;->d:Lcom/google/android/exoplayer/b/a/a;

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/exoplayer/upstream/p;)V
    .locals 8

    const-wide/16 v6, 0x1

    iget-object v0, p0, Lcom/google/android/exoplayer/b/b;->e:Lcom/google/android/exoplayer/d/a/f;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/exoplayer/d/a/f;->a(Lcom/google/android/exoplayer/upstream/p;Lcom/google/android/exoplayer/ah;)I

    move-result v0

    const/16 v1, 0x32

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/android/exoplayer/MalformedMediaDataException;

    const-string v1, "Invalid initialization data"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/MalformedMediaDataException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/b/b;->e:Lcom/google/android/exoplayer/d/a/f;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/d/a/f;->a()Lcom/google/android/exoplayer/d/a/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer/b/b;->d:Lcom/google/android/exoplayer/b/a/a;

    iget-wide v1, v1, Lcom/google/android/exoplayer/b/a/a;->h:J

    iget-object v3, p0, Lcom/google/android/exoplayer/b/b;->d:Lcom/google/android/exoplayer/b/a/a;

    iget-wide v3, v3, Lcom/google/android/exoplayer/b/a/a;->g:J

    sub-long/2addr v1, v3

    add-long/2addr v1, v6

    iget v3, v0, Lcom/google/android/exoplayer/d/a/h;->a:I

    int-to-long v3, v3

    cmp-long v3, v3, v1

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/google/android/exoplayer/b/a;->d()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Sidx length mismatch: sidxLen = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v0, Lcom/google/android/exoplayer/d/a/h;->a:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ExpectedLen = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/h;->d:[J

    iget v2, v0, Lcom/google/android/exoplayer/d/a/h;->b:I

    add-int/lit8 v2, v2, -0x1

    aget-wide v1, v1, v2

    iget-object v3, v0, Lcom/google/android/exoplayer/d/a/h;->c:[I

    iget v0, v0, Lcom/google/android/exoplayer/d/a/h;->b:I

    add-int/lit8 v0, v0, -0x1

    aget v0, v3, v0

    int-to-long v3, v0

    add-long v0, v1, v3

    iget-object v2, p0, Lcom/google/android/exoplayer/b/b;->d:Lcom/google/android/exoplayer/b/a/a;

    iget-wide v2, v2, Lcom/google/android/exoplayer/b/a/a;->h:J

    add-long/2addr v0, v2

    add-long/2addr v0, v6

    iget-object v2, p0, Lcom/google/android/exoplayer/b/b;->d:Lcom/google/android/exoplayer/b/a/a;

    iget-wide v2, v2, Lcom/google/android/exoplayer/b/a/a;->d:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/google/android/exoplayer/b/a;->d()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ContentLength mismatch: Actual = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Expected = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/exoplayer/b/b;->d:Lcom/google/android/exoplayer/b/a/a;

    iget-wide v3, v1, Lcom/google/android/exoplayer/b/a/a;->d:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method
