.class public Lcom/google/android/exoplayer/b/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/a/l;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/exoplayer/aj;

.field private final c:Lcom/google/android/exoplayer/upstream/i;

.field private final d:Lcom/google/android/exoplayer/a/o;

.field private final e:Lcom/google/android/exoplayer/a/r;

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:[Lcom/google/android/exoplayer/a/m;

.field private final j:Landroid/util/SparseArray;

.field private final k:Landroid/util/SparseArray;

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/exoplayer/b/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer/b/a;->a:Ljava/lang/String;

    return-void
.end method

.method private varargs constructor <init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/a/o;I[Lcom/google/android/exoplayer/b/a/a;)V
    .locals 7

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/exoplayer/b/a;->c:Lcom/google/android/exoplayer/upstream/i;

    iput-object p2, p0, Lcom/google/android/exoplayer/b/a;->d:Lcom/google/android/exoplayer/a/o;

    const/4 v1, 0x1

    iput v1, p0, Lcom/google/android/exoplayer/b/a;->h:I

    array-length v1, p4

    new-array v1, v1, [Lcom/google/android/exoplayer/a/m;

    iput-object v1, p0, Lcom/google/android/exoplayer/b/a;->i:[Lcom/google/android/exoplayer/a/m;

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/google/android/exoplayer/b/a;->k:Landroid/util/SparseArray;

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/google/android/exoplayer/b/a;->j:Landroid/util/SparseArray;

    new-instance v1, Lcom/google/android/exoplayer/aj;

    aget-object v2, p4, v0

    iget-object v2, v2, Lcom/google/android/exoplayer/b/a/a;->c:Lcom/google/android/exoplayer/a/m;

    iget-object v2, v2, Lcom/google/android/exoplayer/a/m;->b:Ljava/lang/String;

    aget-object v3, p4, v0

    iget-wide v3, v3, Lcom/google/android/exoplayer/b/a/a;->j:J

    const-wide/16 v5, 0x3e8

    mul-long/2addr v3, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/exoplayer/aj;-><init>(Ljava/lang/String;J)V

    iput-object v1, p0, Lcom/google/android/exoplayer/b/a;->b:Lcom/google/android/exoplayer/aj;

    new-instance v1, Lcom/google/android/exoplayer/a/r;

    invoke-direct {v1}, Lcom/google/android/exoplayer/a/r;-><init>()V

    iput-object v1, p0, Lcom/google/android/exoplayer/b/a;->e:Lcom/google/android/exoplayer/a/r;

    move v1, v0

    move v2, v0

    :goto_0
    array-length v3, p4

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/exoplayer/b/a;->i:[Lcom/google/android/exoplayer/a/m;

    aget-object v4, p4, v0

    iget-object v4, v4, Lcom/google/android/exoplayer/b/a/a;->c:Lcom/google/android/exoplayer/a/m;

    aput-object v4, v3, v0

    iget-object v3, p0, Lcom/google/android/exoplayer/b/a;->i:[Lcom/google/android/exoplayer/a/m;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/google/android/exoplayer/a/m;->c:I

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/exoplayer/b/a;->i:[Lcom/google/android/exoplayer/a/m;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/google/android/exoplayer/a/m;->d:I

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v3, p0, Lcom/google/android/exoplayer/b/a;->k:Landroid/util/SparseArray;

    iget-object v4, p0, Lcom/google/android/exoplayer/b/a;->i:[Lcom/google/android/exoplayer/a/m;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/google/android/exoplayer/a/m;->a:I

    new-instance v5, Lcom/google/android/exoplayer/d/a/f;

    invoke-direct {v5}, Lcom/google/android/exoplayer/d/a/f;-><init>()V

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/exoplayer/b/a;->j:Landroid/util/SparseArray;

    iget-object v4, p0, Lcom/google/android/exoplayer/b/a;->i:[Lcom/google/android/exoplayer/a/m;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/google/android/exoplayer/a/m;->a:I

    aget-object v5, p4, v0

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput v2, p0, Lcom/google/android/exoplayer/b/a;->f:I

    iput v1, p0, Lcom/google/android/exoplayer/b/a;->g:I

    iget-object v0, p0, Lcom/google/android/exoplayer/b/a;->i:[Lcom/google/android/exoplayer/a/m;

    new-instance v1, Lcom/google/android/exoplayer/a/n;

    invoke-direct {v1}, Lcom/google/android/exoplayer/a/n;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    return-void
.end method

.method public varargs constructor <init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/a/o;[Lcom/google/android/exoplayer/b/a/a;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/exoplayer/b/a;-><init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/a/o;I[Lcom/google/android/exoplayer/b/a/a;)V

    return-void
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/exoplayer/b/a;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;JJ)Landroid/util/Pair;
    .locals 21

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/b/a;->e:Lcom/google/android/exoplayer/a/r;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    iput v3, v2, Lcom/google/android/exoplayer/a/r;->a:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/b/a;->e:Lcom/google/android/exoplayer/a/r;

    iget-object v2, v2, Lcom/google/android/exoplayer/a/r;->c:Lcom/google/android/exoplayer/a/m;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/exoplayer/b/a;->l:Z

    if-nez v2, :cond_1

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/b/a;->d:Lcom/google/android/exoplayer/a/o;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/exoplayer/b/a;->i:[Lcom/google/android/exoplayer/a/m;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/exoplayer/b/a;->e:Lcom/google/android/exoplayer/a/r;

    move-object/from16 v3, p1

    move-wide/from16 v4, p4

    invoke-interface/range {v2 .. v7}, Lcom/google/android/exoplayer/a/o;->a(Ljava/util/List;J[Lcom/google/android/exoplayer/a/m;Lcom/google/android/exoplayer/a/r;)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/b/a;->e:Lcom/google/android/exoplayer/a/r;

    iget v0, v2, Lcom/google/android/exoplayer/a/r;->a:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/b/a;->e:Lcom/google/android/exoplayer/a/r;

    iget-object v2, v2, Lcom/google/android/exoplayer/a/r;->c:Lcom/google/android/exoplayer/a/m;

    if-nez v2, :cond_2

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/b/a;->j:Landroid/util/SparseArray;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/b/a;->e:Lcom/google/android/exoplayer/a/r;

    iget-object v3, v3, Lcom/google/android/exoplayer/a/r;->c:Lcom/google/android/exoplayer/a/m;

    iget v3, v3, Lcom/google/android/exoplayer/a/m;->a:I

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v14, v2

    check-cast v14, Lcom/google/android/exoplayer/b/a/a;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/b/a;->k:Landroid/util/SparseArray;

    iget-object v3, v14, Lcom/google/android/exoplayer/b/a/a;->c:Lcom/google/android/exoplayer/a/m;

    iget v3, v3, Lcom/google/android/exoplayer/a/m;->a:I

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v15, v2

    check-cast v15, Lcom/google/android/exoplayer/d/a/f;

    invoke-virtual {v15}, Lcom/google/android/exoplayer/d/a/f;->d()Lcom/google/android/exoplayer/d/a/i;

    move-result-object v2

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/exoplayer/b/a;->c:Lcom/google/android/exoplayer/upstream/i;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/b/a;->e:Lcom/google/android/exoplayer/a/r;

    iget v10, v2, Lcom/google/android/exoplayer/a/r;->b:I

    new-instance v2, Lcom/google/android/exoplayer/upstream/j;

    iget-object v3, v14, Lcom/google/android/exoplayer/b/a/a;->k:Landroid/net/Uri;

    const-wide/16 v4, 0x0

    iget-wide v6, v14, Lcom/google/android/exoplayer/b/a/a;->h:J

    const-wide/16 v11, 0x1

    add-long/2addr v6, v11

    invoke-virtual {v14}, Lcom/google/android/exoplayer/b/a/a;->a()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/google/android/exoplayer/upstream/j;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    new-instance v3, Lcom/google/android/exoplayer/b/b;

    move-object v4, v9

    move-object v5, v2

    move v6, v10

    move-object v7, v15

    move-object v8, v14

    invoke-direct/range {v3 .. v8}, Lcom/google/android/exoplayer/b/b;-><init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/j;ILcom/google/android/exoplayer/d/a/f;Lcom/google/android/exoplayer/b/a/a;)V

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/exoplayer/b/a;->l:Z

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    goto :goto_0

    :cond_3
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v15}, Lcom/google/android/exoplayer/d/a/f;->a()Lcom/google/android/exoplayer/d/a/h;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/exoplayer/d/a/h;->f:[J

    move-wide/from16 v0, p2

    invoke-static {v2, v0, v1}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v2

    if-gez v2, :cond_4

    neg-int v2, v2

    add-int/lit8 v2, v2, -0x2

    :cond_4
    :goto_1
    const/4 v3, -0x1

    if-ne v2, v3, :cond_6

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    goto :goto_0

    :cond_5
    add-int/lit8 v2, v16, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer/a/t;

    iget v2, v2, Lcom/google/android/exoplayer/a/t;->f:I

    goto :goto_1

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/exoplayer/b/a;->c:Lcom/google/android/exoplayer/upstream/i;

    move-object/from16 v17, v0

    invoke-virtual {v15}, Lcom/google/android/exoplayer/d/a/f;->a()Lcom/google/android/exoplayer/d/a/h;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/exoplayer/b/a;->e:Lcom/google/android/exoplayer/a/r;

    iget v0, v4, Lcom/google/android/exoplayer/a/r;->b:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/exoplayer/b/a;->h:I

    iget v5, v3, Lcom/google/android/exoplayer/d/a/h;->b:I

    sub-int/2addr v5, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    add-int/2addr v4, v2

    add-int/lit8 v8, v4, -0x1

    iget v4, v3, Lcom/google/android/exoplayer/d/a/h;->b:I

    add-int/lit8 v4, v4, -0x1

    if-ne v8, v4, :cond_7

    const/4 v13, -0x1

    :goto_2
    iget-object v4, v3, Lcom/google/android/exoplayer/d/a/h;->f:[J

    aget-wide v9, v4, v2

    const/4 v4, -0x1

    if-ne v13, v4, :cond_8

    iget-object v4, v3, Lcom/google/android/exoplayer/d/a/h;->f:[J

    aget-wide v4, v4, v8

    iget-object v6, v3, Lcom/google/android/exoplayer/d/a/h;->e:[J

    aget-wide v6, v6, v8

    add-long v11, v4, v6

    :goto_3
    iget-wide v4, v14, Lcom/google/android/exoplayer/b/a/a;->h:J

    long-to-int v4, v4

    add-int/lit8 v4, v4, 0x1

    int-to-long v4, v4

    iget-object v6, v3, Lcom/google/android/exoplayer/d/a/h;->d:[J

    aget-wide v6, v6, v2

    add-long/2addr v4, v6

    const-wide/16 v6, 0x0

    :goto_4
    if-gt v2, v8, :cond_9

    iget-object v0, v3, Lcom/google/android/exoplayer/d/a/h;->c:[I

    move-object/from16 v19, v0

    aget v19, v19, v2

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v19, v0

    add-long v6, v6, v19

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_7
    add-int/lit8 v13, v8, 0x1

    goto :goto_2

    :cond_8
    iget-object v4, v3, Lcom/google/android/exoplayer/d/a/h;->f:[J

    aget-wide v11, v4, v13

    goto :goto_3

    :cond_9
    new-instance v2, Lcom/google/android/exoplayer/upstream/j;

    iget-object v3, v14, Lcom/google/android/exoplayer/b/a/a;->k:Landroid/net/Uri;

    invoke-virtual {v14}, Lcom/google/android/exoplayer/b/a/a;->a()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/google/android/exoplayer/upstream/j;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    new-instance v3, Lcom/google/android/exoplayer/a/u;

    iget-object v6, v14, Lcom/google/android/exoplayer/b/a/a;->c:Lcom/google/android/exoplayer/a/m;

    const/4 v14, 0x0

    move-object/from16 v4, v17

    move-object v5, v2

    move/from16 v7, v18

    move-object v8, v15

    invoke-direct/range {v3 .. v14}, Lcom/google/android/exoplayer/a/u;-><init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/j;Lcom/google/android/exoplayer/a/m;ILcom/google/android/exoplayer/d/a/f;JJIZ)V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/exoplayer/b/a;->l:Z

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public final a()Lcom/google/android/exoplayer/aj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/b/a;->b:Lcom/google/android/exoplayer/aj;

    return-object v0
.end method

.method public final a(Lcom/google/android/exoplayer/ag;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/b/a;->b:Lcom/google/android/exoplayer/aj;

    iget-object v0, v0, Lcom/google/android/exoplayer/aj;->a:Ljava/lang/String;

    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer/b/a;->f:I

    iget v1, p0, Lcom/google/android/exoplayer/b/a;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/exoplayer/ag;->a(II)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/b/a;->d:Lcom/google/android/exoplayer/a/o;

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/b/a;->d:Lcom/google/android/exoplayer/a/o;

    return-void
.end method
