.class final Lcom/google/android/exoplayer/d/a/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:[B

.field private b:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, p1, [B

    iput-object v0, p0, Lcom/google/android/exoplayer/d/a/g;->a:[B

    return-void
.end method

.method private static b([BII)I
    .locals 3

    aget-byte v0, p0, p1

    and-int/lit16 v1, v0, 0xff

    add-int/lit8 v0, p1, 0x1

    :goto_0
    add-int v2, p1, p2

    if-ge v0, v2, :cond_0

    shl-int/lit8 v1, v1, 0x8

    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method private static c([BII)J
    .locals 5

    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    int-to-long v1, v0

    add-int/lit8 v0, p1, 0x1

    :goto_0
    add-int v3, p1, p2

    if-ge v0, v3, :cond_0

    const/16 v3, 0x8

    shl-long/2addr v1, v3

    aget-byte v3, p0, v0

    and-int/lit16 v3, v3, 0xff

    int-to-long v3, v3

    or-long/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-wide v1
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    return-void
.end method

.method public final a([BII)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/g;->a:[B

    iget v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    return-void
.end method

.method public final a()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/g;->a:[B

    return-object v0
.end method

.method public final b()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/g;->a:[B

    array-length v0, v0

    return v0
.end method

.method public final b(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    return-void
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    return v0
.end method

.method public final d()I
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/g;->a:[B

    iget v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer/d/a/g;->b([BII)I

    move-result v0

    iget v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    return v0
.end method

.method public final e()I
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/g;->a:[B

    iget v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer/d/a/g;->b([BII)I

    move-result v0

    iget v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    return v0
.end method

.method public final f()J
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/g;->a:[B

    iget v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer/d/a/g;->c([BII)J

    move-result-wide v0

    iget v2, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    add-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    return-wide v0
.end method

.method public final g()I
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/g;->a:[B

    iget v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer/d/a/g;->b([BII)I

    move-result v0

    iget v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    return v0
.end method

.method public final h()J
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/g;->a:[B

    iget v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    const/16 v2, 0x8

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer/d/a/g;->c([BII)J

    move-result-wide v0

    iget v2, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    add-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    return-wide v0
.end method

.method public final i()I
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/g;->a:[B

    iget v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer/d/a/g;->b([BII)I

    move-result v0

    iget v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    return v0
.end method

.method public final j()I
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/g;->a:[B

    iget v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer/d/a/g;->b([BII)I

    move-result v0

    iget v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    if-gez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Top bit not zero: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    return v0
.end method

.method public final k()J
    .locals 5

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/g;->a:[B

    iget v1, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    const/16 v2, 0x8

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer/d/a/g;->c([BII)J

    move-result-wide v0

    iget v2, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    add-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/android/exoplayer/d/a/g;->b:I

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Top bit not zero: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    return-wide v0
.end method
