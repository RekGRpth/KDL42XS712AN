.class public final Lcom/google/android/exoplayer/d/a/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[B

.field private static final b:[B

.field private static final c:Ljava/util/Set;

.field private static final d:Ljava/util/Set;


# instance fields
.field private final e:Z

.field private final f:Lcom/google/android/exoplayer/d/a/g;

.field private final g:Ljava/util/Stack;

.field private final h:Ljava/util/Stack;

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:Lcom/google/android/exoplayer/d/a/g;

.field private o:Lcom/google/android/exoplayer/d/a/g;

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private final v:Ljava/util/HashMap;

.field private w:Lcom/google/android/exoplayer/d/a/h;

.field private x:Lcom/google/android/exoplayer/d/a/i;

.field private y:Lcom/google/android/exoplayer/d/a/e;

.field private z:Lcom/google/android/exoplayer/d/a/k;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const v6, 0x6d766578

    const v5, 0x6d6f6f76

    const v4, 0x6d6f6f66

    const v3, 0x6d696e66

    const v2, 0x6d646961

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/exoplayer/d/a/f;->a:[B

    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/exoplayer/d/a/f;->b:[B

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const v1, 0x61766331

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x65736473

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x68646c72    # 4.3148E24f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x6d646174

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x6d646864

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x6d666864

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x6d703461

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x73696478

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x73747364

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x74666474

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x74666864

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x746b6864

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x74726166

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x7472616b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x74726578

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x7472756e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x7374626c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x70737368    # 3.013775E29f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x7361697a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x75756964

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer/d/a/f;->c:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x7472616b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x7374626c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x61766343

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const v1, 0x74726166

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/exoplayer/d/a/f;->d:Ljava/util/Set;

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    :array_1
    .array-data 1
        -0x5et
        0x39t
        0x4ft
        0x52t
        0x5at
        -0x65t
        0x4ft
        0x14t
        -0x5et
        0x44t
        0x6ct
        0x42t
        0x7ct
        0x64t
        -0x73t
        -0xct
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/d/a/f;-><init>(Z)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/exoplayer/d/a/f;->e:Z

    iput v0, p0, Lcom/google/android/exoplayer/d/a/f;->i:I

    new-instance v0, Lcom/google/android/exoplayer/d/a/g;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/d/a/g;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->f:Lcom/google/android/exoplayer/d/a/g;

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->g:Ljava/util/Stack;

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->h:Ljava/util/Stack;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->v:Ljava/util/HashMap;

    return-void
.end method

.method private static a(Lcom/google/android/exoplayer/d/a/g;)Landroid/util/Pair;
    .locals 14

    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v5

    const/4 v1, 0x0

    new-array v6, v5, [Lcom/google/android/exoplayer/d/a/j;

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_9

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->c()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v0

    const v3, 0x61766331

    if-eq v0, v3, :cond_0

    const v3, 0x656e6376

    if-ne v0, v3, :cond_7

    :cond_0
    add-int/lit8 v0, v7, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    const/16 v0, 0x18

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->e()I

    move-result v9

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->e()I

    move-result v10

    const/16 v0, 0x32

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->c()I

    move-result v0

    move v4, v0

    move-object v0, v1

    move-object v1, v3

    :goto_1
    sub-int v3, v4, v7

    if-ge v3, v8, :cond_5

    invoke-virtual {p0, v4}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->c()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v11

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v12

    const v13, 0x61766343

    if-ne v12, v13, :cond_3

    add-int/lit8 v1, v3, 0x8

    add-int/lit8 v1, v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->d()I

    move-result v1

    and-int/lit8 v1, v1, 0x3

    add-int/lit8 v1, v1, 0x1

    const/4 v3, 0x4

    if-eq v1, v3, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->d()I

    move-result v3

    and-int/lit8 v12, v3, 0x1f

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v12, :cond_2

    invoke-static {p0}, Lcom/google/android/exoplayer/d/a/f;->b(Lcom/google/android/exoplayer/d/a/g;)[B

    move-result-object v13

    invoke-interface {v1, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->d()I

    move-result v12

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v12, :cond_4

    invoke-static {p0}, Lcom/google/android/exoplayer/d/a/f;->b(Lcom/google/android/exoplayer/d/a/g;)[B

    move-result-object v13

    invoke-interface {v1, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    const v13, 0x73696e66

    if-ne v12, v13, :cond_4

    invoke-static {p0, v3, v11}, Lcom/google/android/exoplayer/d/a/f;->b(Lcom/google/android/exoplayer/d/a/g;II)Lcom/google/android/exoplayer/d/a/j;

    move-result-object v0

    :cond_4
    add-int v3, v4, v11

    move v4, v3

    goto :goto_1

    :cond_5
    const-string v3, "video/avc"

    const/4 v4, -0x1

    invoke-static {v3, v4, v9, v10, v1}, Lcom/google/android/exoplayer/ag;->a(Ljava/lang/String;IIILjava/util/List;)Lcom/google/android/exoplayer/ag;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer/ag;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/exoplayer/d/a/j;

    aput-object v1, v6, v2

    move-object v1, v0

    :cond_6
    :goto_4
    add-int v0, v7, v8

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    :cond_7
    const v3, 0x6d703461

    if-eq v0, v3, :cond_8

    const v3, 0x656e6361

    if-ne v0, v3, :cond_6

    :cond_8
    invoke-static {p0, v7, v8}, Lcom/google/android/exoplayer/d/a/f;->a(Lcom/google/android/exoplayer/d/a/g;II)Landroid/util/Pair;

    move-result-object v3

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object v1, v0

    check-cast v1, Lcom/google/android/exoplayer/ag;

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer/d/a/j;

    aput-object v0, v6, v2

    goto :goto_4

    :cond_9
    invoke-static {v1, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/exoplayer/d/a/g;II)Landroid/util/Pair;
    .locals 11

    add-int/lit8 v0, p1, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->e()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->e()I

    move-result v6

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->i()I

    move-result v3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->c()I

    move-result v0

    move v5, v0

    :goto_0
    sub-int v0, v5, p1

    if-ge v0, p2, :cond_7

    invoke-virtual {p0, v5}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->c()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v8

    const v9, 0x65736473

    if-ne v8, v9, :cond_6

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->d()I

    move-result v0

    :goto_1
    const/16 v2, 0x7f

    if-le v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->d()I

    move-result v0

    goto :goto_1

    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->d()I

    move-result v0

    and-int/lit16 v2, v0, 0x80

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    :cond_1
    and-int/lit8 v2, v0, 0x40

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->e()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    :cond_2
    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->d()I

    move-result v0

    :goto_2
    const/16 v2, 0x7f

    if-le v0, v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->d()I

    move-result v0

    goto :goto_2

    :cond_4
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->d()I

    move-result v2

    and-int/lit8 v0, v2, 0x7f

    :goto_3
    const/16 v3, 0x7f

    if-le v2, v3, :cond_5

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->d()I

    move-result v2

    shl-int/lit8 v0, v0, 0x8

    and-int/lit8 v3, v2, 0x7f

    or-int/2addr v0, v3

    goto :goto_3

    :cond_5
    new-array v2, v0, [B

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, v0}, Lcom/google/android/exoplayer/d/a/g;->a([BII)V

    invoke-static {v2}, Lcom/google/android/exoplayer/d/a/d;->a([B)Landroid/util/Pair;

    move-result-object v4

    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v0, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move-object v10, v1

    move-object v1, v2

    move v2, v3

    move v3, v0

    move-object v0, v10

    :goto_4
    add-int v4, v5, v7

    move v5, v4

    move v4, v3

    move v3, v2

    move-object v2, v1

    move-object v1, v0

    goto/16 :goto_0

    :cond_6
    const v9, 0x73696e66

    if-ne v8, v9, :cond_8

    invoke-static {p0, v0, v7}, Lcom/google/android/exoplayer/d/a/f;->b(Lcom/google/android/exoplayer/d/a/g;II)Lcom/google/android/exoplayer/d/a/j;

    move-result-object v0

    move-object v1, v2

    move v2, v3

    move v3, v4

    goto :goto_4

    :cond_7
    const-string v0, "audio/mp4a-latm"

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-static {v0, v6, v4, v3, v2}, Lcom/google/android/exoplayer/ag;->b(Ljava/lang/String;IIILjava/util/List;)Lcom/google/android/exoplayer/ag;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    :cond_8
    move-object v0, v1

    move-object v1, v2

    move v2, v3

    move v3, v4

    goto :goto_4
.end method

.method private a(I)V
    .locals 2

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    iput p1, p0, Lcom/google/android/exoplayer/d/a/f;->i:I

    return-void

    :pswitch_1
    iput v1, p0, Lcom/google/android/exoplayer/d/a/f;->j:I

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iput v1, p0, Lcom/google/android/exoplayer/d/a/f;->k:I

    goto :goto_0

    :pswitch_2
    iput v1, p0, Lcom/google/android/exoplayer/d/a/f;->p:I

    goto :goto_0

    :pswitch_3
    iput v1, p0, Lcom/google/android/exoplayer/d/a/f;->q:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lcom/google/android/exoplayer/d/a/b;)V
    .locals 11

    const/4 v3, 0x0

    const/16 v5, 0x10

    const/16 v4, 0x8

    iget-object v2, p1, Lcom/google/android/exoplayer/d/a/b;->b:Ljava/util/ArrayList;

    move v1, v3

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/d/a/a;

    iget v6, v0, Lcom/google/android/exoplayer/d/a/a;->a:I

    const v7, 0x70737368    # 3.013775E29f

    if-ne v6, v7, :cond_0

    check-cast v0, Lcom/google/android/exoplayer/d/a/c;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/d/a/c;->a()Lcom/google/android/exoplayer/d/a/g;

    move-result-object v0

    const/16 v6, 0xc

    invoke-virtual {v0, v6}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    new-instance v6, Ljava/util/UUID;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/d/a/g;->h()J

    move-result-wide v7

    invoke-virtual {v0}, Lcom/google/android/exoplayer/d/a/g;->h()J

    move-result-wide v9

    invoke-direct {v6, v7, v8, v9, v10}, Ljava/util/UUID;-><init>(JJ)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v7

    new-array v8, v7, [B

    invoke-virtual {v0, v8, v3, v7}, Lcom/google/android/exoplayer/d/a/g;->a([BII)V

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->v:Ljava/util/HashMap;

    invoke-virtual {v0, v6, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const v0, 0x6d766578

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer/d/a/b;->b(I)Lcom/google/android/exoplayer/d/a/b;

    move-result-object v0

    const v1, 0x74726578

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer/d/a/b;->a(I)Lcom/google/android/exoplayer/d/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer/d/a/c;->a()Lcom/google/android/exoplayer/d/a/g;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer/d/a/g;->j()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0}, Lcom/google/android/exoplayer/d/a/g;->j()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/exoplayer/d/a/g;->j()I

    move-result v6

    invoke-virtual {v0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v0

    new-instance v7, Lcom/google/android/exoplayer/d/a/e;

    invoke-direct {v7, v1, v2, v6, v0}, Lcom/google/android/exoplayer/d/a/e;-><init>(IIII)V

    iput-object v7, p0, Lcom/google/android/exoplayer/d/a/f;->y:Lcom/google/android/exoplayer/d/a/e;

    const v0, 0x7472616b

    invoke-virtual {p1, v0}, Lcom/google/android/exoplayer/d/a/b;->b(I)Lcom/google/android/exoplayer/d/a/b;

    move-result-object v0

    const v1, 0x6d646961

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer/d/a/b;->b(I)Lcom/google/android/exoplayer/d/a/b;

    move-result-object v6

    const v1, 0x68646c72    # 4.3148E24f

    invoke-virtual {v6, v1}, Lcom/google/android/exoplayer/d/a/b;->a(I)Lcom/google/android/exoplayer/d/a/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/exoplayer/d/a/c;->a()Lcom/google/android/exoplayer/d/a/g;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    invoke-virtual {v1}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v2

    const v1, 0x736f756e

    if-eq v2, v1, :cond_2

    const v1, 0x76696465

    if-ne v2, v1, :cond_3

    :cond_2
    const/4 v3, 0x1

    :cond_3
    invoke-static {v3}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    const v1, 0x746b6864

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer/d/a/b;->a(I)Lcom/google/android/exoplayer/d/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer/d/a/c;->a()Lcom/google/android/exoplayer/d/a/g;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    invoke-virtual {v1}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/exoplayer/d/a/f;->b(I)I

    move-result v3

    if-nez v3, :cond_4

    move v0, v4

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    invoke-virtual {v1}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v7

    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    if-nez v3, :cond_5

    invoke-virtual {v1}, Lcom/google/android/exoplayer/d/a/g;->f()J

    move-result-wide v0

    :goto_2
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const v0, 0x6d646864

    invoke-virtual {v6, v0}, Lcom/google/android/exoplayer/d/a/b;->a(I)Lcom/google/android/exoplayer/d/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer/d/a/c;->a()Lcom/google/android/exoplayer/d/a/g;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/exoplayer/d/a/f;->b(I)I

    move-result v3

    if-nez v3, :cond_6

    :goto_3
    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    invoke-virtual {v0}, Lcom/google/android/exoplayer/d/a/g;->f()J

    move-result-wide v3

    const v0, 0x6d696e66

    invoke-virtual {v6, v0}, Lcom/google/android/exoplayer/d/a/b;->b(I)Lcom/google/android/exoplayer/d/a/b;

    move-result-object v0

    const v5, 0x7374626c

    invoke-virtual {v0, v5}, Lcom/google/android/exoplayer/d/a/b;->b(I)Lcom/google/android/exoplayer/d/a/b;

    move-result-object v0

    const v5, 0x73747364

    invoke-virtual {v0, v5}, Lcom/google/android/exoplayer/d/a/b;->a(I)Lcom/google/android/exoplayer/d/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/exoplayer/d/a/c;->a()Lcom/google/android/exoplayer/d/a/g;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/exoplayer/d/a/f;->a(Lcom/google/android/exoplayer/d/a/g;)Landroid/util/Pair;

    move-result-object v6

    new-instance v0, Lcom/google/android/exoplayer/d/a/i;

    iget-object v5, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Lcom/google/android/exoplayer/ag;

    iget-object v6, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, [Lcom/google/android/exoplayer/d/a/j;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/d/a/i;-><init>(IIJLcom/google/android/exoplayer/ag;[Lcom/google/android/exoplayer/d/a/j;)V

    iput-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->x:Lcom/google/android/exoplayer/d/a/i;

    return-void

    :cond_4
    move v0, v5

    goto :goto_1

    :cond_5
    invoke-virtual {v1}, Lcom/google/android/exoplayer/d/a/g;->k()J

    move-result-wide v0

    goto :goto_2

    :cond_6
    move v4, v5

    goto :goto_3
.end method

.method private static b(I)I
    .locals 1

    shr-int/lit8 v0, p0, 0x18

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method private b(Lcom/google/android/exoplayer/upstream/p;Lcom/google/android/exoplayer/ah;)I
    .locals 11

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget-object v0, v0, Lcom/google/android/exoplayer/d/a/k;->c:[I

    iget v1, p0, Lcom/google/android/exoplayer/d/a/f;->s:I

    aget v8, v0, v1

    iget-object v0, p2, Lcom/google/android/exoplayer/ah;->c:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/google/android/exoplayer/d/a/f;->i:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_19

    iget-object v1, p0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget v2, p0, Lcom/google/android/exoplayer/d/a/f;->s:I

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer/d/a/k;->a(I)I

    move-result v1

    int-to-long v1, v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    iput-wide v1, p2, Lcom/google/android/exoplayer/ah;->f:J

    const/4 v1, 0x0

    iput v1, p2, Lcom/google/android/exoplayer/ah;->e:I

    iget-object v1, p0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget-object v1, v1, Lcom/google/android/exoplayer/d/a/k;->f:[Z

    iget v2, p0, Lcom/google/android/exoplayer/d/a/f;->s:I

    aget-boolean v1, v1, v2

    if-eqz v1, :cond_0

    iget v1, p2, Lcom/google/android/exoplayer/ah;->e:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p2, Lcom/google/android/exoplayer/ah;->e:I

    iget v1, p0, Lcom/google/android/exoplayer/d/a/f;->s:I

    iput v1, p0, Lcom/google/android/exoplayer/d/a/f;->u:I

    :cond_0
    iget-boolean v1, p2, Lcom/google/android/exoplayer/ah;->a:Z

    if-eqz v1, :cond_2

    iget-object v1, p2, Lcom/google/android/exoplayer/ah;->c:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_1

    iget-object v1, p2, Lcom/google/android/exoplayer/ah;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    if-ge v1, v8, :cond_2

    :cond_1
    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p2, Lcom/google/android/exoplayer/ah;->c:Ljava/nio/ByteBuffer;

    :cond_2
    move-object v7, v0

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->o:Lcom/google/android/exoplayer/d/a/g;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->o:Lcom/google/android/exoplayer/d/a/g;

    move-object v6, v0

    :goto_0
    if-eqz v6, :cond_f

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->x:Lcom/google/android/exoplayer/d/a/i;

    iget-object v0, v0, Lcom/google/android/exoplayer/d/a/i;->e:[Lcom/google/android/exoplayer/d/a/j;

    iget-object v1, p0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget v1, v1, Lcom/google/android/exoplayer/d/a/k;->a:I

    aget-object v0, v0, v1

    iget-object v4, v0, Lcom/google/android/exoplayer/d/a/j;->c:[B

    iget-boolean v9, v0, Lcom/google/android/exoplayer/d/a/j;->a:Z

    iget v1, v0, Lcom/google/android/exoplayer/d/a/j;->b:I

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->o:Lcom/google/android/exoplayer/d/a/g;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget-object v0, v0, Lcom/google/android/exoplayer/d/a/k;->h:[I

    iget v2, p0, Lcom/google/android/exoplayer/d/a/f;->s:I

    aget v0, v0, v2

    if-le v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_1
    iget-object v2, p2, Lcom/google/android/exoplayer/ah;->b:Lcom/google/android/exoplayer/b;

    iget-object v5, v2, Lcom/google/android/exoplayer/b;->a:[B

    if-eqz v5, :cond_3

    array-length v2, v5

    const/16 v3, 0x10

    if-eq v2, v3, :cond_4

    :cond_3
    const/16 v2, 0x10

    new-array v5, v2, [B

    :cond_4
    const/4 v2, 0x0

    invoke-virtual {v6, v5, v2, v1}, Lcom/google/android/exoplayer/d/a/g;->a([BII)V

    if-eqz v0, :cond_c

    invoke-virtual {v6}, Lcom/google/android/exoplayer/d/a/g;->e()I

    move-result v1

    :goto_2
    iget-object v2, p2, Lcom/google/android/exoplayer/ah;->b:Lcom/google/android/exoplayer/b;

    iget-object v2, v2, Lcom/google/android/exoplayer/b;->d:[I

    if-eqz v2, :cond_5

    array-length v3, v2

    if-ge v3, v1, :cond_6

    :cond_5
    new-array v2, v1, [I

    :cond_6
    iget-object v3, p2, Lcom/google/android/exoplayer/ah;->b:Lcom/google/android/exoplayer/b;

    iget-object v3, v3, Lcom/google/android/exoplayer/b;->e:[I

    if-eqz v3, :cond_7

    array-length v10, v3

    if-ge v10, v1, :cond_8

    :cond_7
    new-array v3, v1, [I

    :cond_8
    if-eqz v0, :cond_d

    const/4 v0, 0x0

    :goto_3
    if-ge v0, v1, :cond_e

    invoke-virtual {v6}, Lcom/google/android/exoplayer/d/a/g;->e()I

    move-result v10

    aput v10, v2, v0

    invoke-virtual {v6}, Lcom/google/android/exoplayer/d/a/g;->j()I

    move-result v10

    aput v10, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_9
    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget-object v0, v0, Lcom/google/android/exoplayer/d/a/k;->j:Lcom/google/android/exoplayer/d/a/g;

    move-object v6, v0

    goto :goto_0

    :cond_a
    const/4 v0, 0x0

    goto :goto_1

    :cond_b
    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget-boolean v0, v0, Lcom/google/android/exoplayer/d/a/k;->i:Z

    goto :goto_1

    :cond_c
    const/4 v1, 0x1

    goto :goto_2

    :cond_d
    const/4 v0, 0x0

    const/4 v6, 0x0

    aput v6, v2, v0

    const/4 v0, 0x0

    iget-object v6, p0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget-object v6, v6, Lcom/google/android/exoplayer/d/a/k;->c:[I

    iget v10, p0, Lcom/google/android/exoplayer/d/a/f;->s:I

    aget v6, v6, v10

    aput v6, v3, v0

    :cond_e
    iget-object v0, p2, Lcom/google/android/exoplayer/ah;->b:Lcom/google/android/exoplayer/b;

    if-eqz v9, :cond_10

    const/4 v6, 0x1

    :goto_4
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/exoplayer/b;->a(I[I[I[B[BI)V

    if-eqz v9, :cond_f

    iget v0, p2, Lcom/google/android/exoplayer/ah;->e:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p2, Lcom/google/android/exoplayer/ah;->e:I

    :cond_f
    :goto_5
    if-nez v7, :cond_11

    iget v0, p0, Lcom/google/android/exoplayer/d/a/f;->q:I

    sub-int v0, v8, v0

    invoke-interface {p1, v0}, Lcom/google/android/exoplayer/upstream/p;->a(I)I

    move-result v0

    :goto_6
    const/4 v1, -0x1

    if-ne v0, v1, :cond_12

    const/4 v0, 0x2

    :goto_7
    return v0

    :cond_10
    const/4 v6, 0x0

    goto :goto_4

    :cond_11
    iget v0, p0, Lcom/google/android/exoplayer/d/a/f;->q:I

    sub-int v0, v8, v0

    invoke-interface {p1, v7, v0}, Lcom/google/android/exoplayer/upstream/p;->a(Ljava/nio/ByteBuffer;I)I

    move-result v0

    goto :goto_6

    :cond_12
    iget v1, p0, Lcom/google/android/exoplayer/d/a/f;->q:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/exoplayer/d/a/f;->q:I

    iget v0, p0, Lcom/google/android/exoplayer/d/a/f;->q:I

    if-eq v8, v0, :cond_13

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/d/a/f;->a(I)V

    const/16 v0, 0x9

    goto :goto_7

    :cond_13
    if-eqz v7, :cond_18

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->x:Lcom/google/android/exoplayer/d/a/i;

    iget v0, v0, Lcom/google/android/exoplayer/d/a/i;->b:I

    const v1, 0x76696465

    if-ne v0, v1, :cond_17

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    sub-int v3, v0, v8

    move v2, v3

    :goto_8
    add-int v0, v3, v8

    if-ge v2, v0, :cond_16

    invoke-virtual {v7, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    and-int/lit16 v1, v0, 0xff

    const/4 v0, 0x1

    :goto_9
    const/4 v4, 0x4

    if-ge v0, v4, :cond_14

    shl-int/lit8 v1, v1, 0x8

    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v1, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_14
    if-gez v1, :cond_15

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Top bit not zero: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_15
    invoke-virtual {v7, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    sget-object v0, Lcom/google/android/exoplayer/d/a/f;->a:[B

    invoke-virtual {v7, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v1, 0x4

    add-int/2addr v0, v2

    move v2, v0

    goto :goto_8

    :cond_16
    add-int v0, v3, v8

    invoke-virtual {v7, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    :cond_17
    iput v8, p2, Lcom/google/android/exoplayer/ah;->d:I

    :goto_a
    iget v0, p0, Lcom/google/android/exoplayer/d/a/f;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer/d/a/f;->s:I

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/d/a/f;->a(I)V

    const/4 v0, 0x4

    goto/16 :goto_7

    :cond_18
    const/4 v0, 0x0

    iput v0, p2, Lcom/google/android/exoplayer/ah;->d:I

    goto :goto_a

    :cond_19
    move-object v7, v0

    goto/16 :goto_5
.end method

.method private static b(Lcom/google/android/exoplayer/d/a/g;II)Lcom/google/android/exoplayer/d/a/j;
    .locals 10

    const/4 v4, 0x0

    const/4 v9, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    add-int/lit8 v0, p1, 0x8

    move v5, v0

    move-object v0, v4

    :goto_0
    sub-int v3, v5, p1

    if-ge v3, p2, :cond_6

    invoke-virtual {p0, v5}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v3

    const v7, 0x66726d61

    if-ne v3, v7, :cond_1

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    :cond_0
    :goto_1
    add-int v3, v5, v6

    move v5, v3

    goto :goto_0

    :cond_1
    const v7, 0x7363686d

    if-ne v3, v7, :cond_2

    invoke-virtual {p0, v9}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    goto :goto_1

    :cond_2
    const v7, 0x73636869

    if-ne v3, v7, :cond_0

    add-int/lit8 v0, v5, 0x8

    :goto_2
    sub-int v3, v0, v5

    if-ge v3, v6, :cond_5

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v7

    const v8, 0x74656e63

    if-ne v7, v8, :cond_4

    invoke-virtual {p0, v9}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v3

    shr-int/lit8 v0, v3, 0x8

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    and-int/lit16 v7, v3, 0xff

    const/16 v3, 0x10

    new-array v8, v3, [B

    array-length v3, v8

    invoke-virtual {p0, v8, v2, v3}, Lcom/google/android/exoplayer/d/a/g;->a([BII)V

    new-instance v3, Lcom/google/android/exoplayer/d/a/j;

    invoke-direct {v3, v0, v7, v8}, Lcom/google/android/exoplayer/d/a/j;-><init>(ZI[B)V

    move-object v0, v3

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    add-int/2addr v0, v3

    goto :goto_2

    :cond_5
    move-object v0, v4

    goto :goto_1

    :cond_6
    return-object v0
.end method

.method private b(Lcom/google/android/exoplayer/d/a/b;)V
    .locals 32

    new-instance v2, Lcom/google/android/exoplayer/d/a/k;

    invoke-direct {v2}, Lcom/google/android/exoplayer/d/a/k;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/exoplayer/d/a/f;->x:Lcom/google/android/exoplayer/d/a/i;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/exoplayer/d/a/f;->y:Lcom/google/android/exoplayer/d/a/e;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/exoplayer/d/a/f;->e:Z

    move/from16 v17, v0

    const v2, 0x6d666864

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/d/a/b;->a(I)Lcom/google/android/exoplayer/d/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/exoplayer/d/a/c;->a()Lcom/google/android/exoplayer/d/a/g;

    move-result-object v2

    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    invoke-virtual {v2}, Lcom/google/android/exoplayer/d/a/g;->j()I

    const v2, 0x74726166

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/d/a/b;->b(I)Lcom/google/android/exoplayer/d/a/b;

    move-result-object v18

    const v2, 0x7361697a

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/d/a/b;->a(I)Lcom/google/android/exoplayer/d/a/c;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/google/android/exoplayer/d/a/c;->a()Lcom/google/android/exoplayer/d/a/g;

    move-result-object v5

    const/16 v2, 0x8

    invoke-virtual {v5, v2}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    invoke-virtual {v5}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v2

    const v3, 0xffffff

    and-int/2addr v2, v3

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const/16 v2, 0x8

    invoke-virtual {v5, v2}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    :cond_0
    invoke-virtual {v5}, Lcom/google/android/exoplayer/d/a/g;->d()I

    move-result v6

    invoke-virtual {v5}, Lcom/google/android/exoplayer/d/a/g;->j()I

    move-result v7

    const/4 v3, 0x0

    new-array v9, v7, [I

    if-nez v6, :cond_1

    const/4 v2, 0x0

    move/from16 v31, v2

    move v2, v3

    move/from16 v3, v31

    :goto_0
    if-ge v3, v7, :cond_2

    invoke-virtual {v5}, Lcom/google/android/exoplayer/d/a/g;->d()I

    move-result v4

    aput v4, v9, v3

    aget v4, v9, v3

    add-int/2addr v4, v2

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    move/from16 v31, v2

    move v2, v3

    move/from16 v3, v31

    :goto_1
    if-ge v3, v7, :cond_2

    aput v6, v9, v3

    add-int v4, v2, v6

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_1

    :cond_2
    move-object/from16 v0, v16

    iput v2, v0, Lcom/google/android/exoplayer/d/a/k;->g:I

    move-object/from16 v0, v16

    iput-object v9, v0, Lcom/google/android/exoplayer/d/a/k;->h:[I

    :cond_3
    const v2, 0x74666474

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/d/a/b;->a(I)Lcom/google/android/exoplayer/d/a/c;

    move-result-object v2

    if-nez v2, :cond_a

    const-wide/16 v2, 0x0

    :goto_2
    const v4, 0x74666864

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer/d/a/b;->a(I)Lcom/google/android/exoplayer/d/a/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/exoplayer/d/a/c;->a()Lcom/google/android/exoplayer/d/a/g;

    move-result-object v9

    const/16 v4, 0x8

    invoke-virtual {v9, v4}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    invoke-virtual {v9}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v4

    const v5, 0xffffff

    and-int v10, v5, v4

    const/4 v4, 0x4

    invoke-virtual {v9, v4}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    and-int/lit8 v4, v10, 0x1

    if-eqz v4, :cond_4

    const/16 v4, 0x8

    invoke-virtual {v9, v4}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    :cond_4
    and-int/lit8 v4, v10, 0x2

    if-eqz v4, :cond_c

    invoke-virtual {v9}, Lcom/google/android/exoplayer/d/a/g;->j()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move v7, v4

    :goto_3
    and-int/lit8 v4, v10, 0x8

    if-eqz v4, :cond_d

    invoke-virtual {v9}, Lcom/google/android/exoplayer/d/a/g;->j()I

    move-result v4

    move v6, v4

    :goto_4
    and-int/lit8 v4, v10, 0x10

    if-eqz v4, :cond_e

    invoke-virtual {v9}, Lcom/google/android/exoplayer/d/a/g;->j()I

    move-result v4

    move v5, v4

    :goto_5
    and-int/lit8 v4, v10, 0x20

    if-eqz v4, :cond_f

    invoke-virtual {v9}, Lcom/google/android/exoplayer/d/a/g;->j()I

    move-result v4

    :goto_6
    new-instance v19, Lcom/google/android/exoplayer/d/a/e;

    move-object/from16 v0, v19

    invoke-direct {v0, v7, v6, v5, v4}, Lcom/google/android/exoplayer/d/a/e;-><init>(IIII)V

    move-object/from16 v0, v19

    iget v4, v0, Lcom/google/android/exoplayer/d/a/e;->a:I

    move-object/from16 v0, v16

    iput v4, v0, Lcom/google/android/exoplayer/d/a/k;->a:I

    const v4, 0x7472756e

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer/d/a/b;->a(I)Lcom/google/android/exoplayer/d/a/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/exoplayer/d/a/c;->a()Lcom/google/android/exoplayer/d/a/g;

    move-result-object v20

    const/16 v4, 0x8

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/exoplayer/d/a/f;->b(I)I

    move-result v21

    const v5, 0xffffff

    and-int v7, v5, v4

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/exoplayer/d/a/g;->j()I

    move-result v22

    and-int/lit8 v4, v7, 0x1

    if-eqz v4, :cond_5

    const/4 v4, 0x4

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    :cond_5
    and-int/lit8 v4, v7, 0x4

    if-eqz v4, :cond_10

    const/4 v4, 0x1

    :goto_7
    move-object/from16 v0, v19

    iget v6, v0, Lcom/google/android/exoplayer/d/a/e;->d:I

    if-eqz v4, :cond_6

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/exoplayer/d/a/g;->j()I

    move-result v6

    :cond_6
    and-int/lit16 v5, v7, 0x100

    if-eqz v5, :cond_11

    const/4 v5, 0x1

    move v14, v5

    :goto_8
    and-int/lit16 v5, v7, 0x200

    if-eqz v5, :cond_12

    const/4 v5, 0x1

    move v13, v5

    :goto_9
    and-int/lit16 v5, v7, 0x400

    if-eqz v5, :cond_13

    const/4 v5, 0x1

    move v12, v5

    :goto_a
    and-int/lit16 v5, v7, 0x800

    if-eqz v5, :cond_14

    const/4 v5, 0x1

    :goto_b
    move/from16 v0, v22

    new-array v0, v0, [I

    move-object/from16 v23, v0

    move/from16 v0, v22

    new-array v0, v0, [I

    move-object/from16 v24, v0

    move/from16 v0, v22

    new-array v0, v0, [I

    move-object/from16 v25, v0

    move/from16 v0, v22

    new-array v0, v0, [Z

    move-object/from16 v26, v0

    iget-wide v0, v15, Lcom/google/android/exoplayer/d/a/i;->c:J

    move-wide/from16 v27, v0

    const/4 v7, 0x0

    move-wide v9, v2

    move v11, v7

    :goto_c
    move/from16 v0, v22

    if-ge v11, v0, :cond_1b

    if-eqz v14, :cond_15

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/exoplayer/d/a/g;->j()I

    move-result v2

    move v8, v2

    :goto_d
    if-eqz v13, :cond_16

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/exoplayer/d/a/g;->j()I

    move-result v2

    move v7, v2

    :goto_e
    if-nez v11, :cond_17

    if-eqz v4, :cond_17

    move v3, v6

    :goto_f
    if-eqz v5, :cond_7

    if-nez v21, :cond_19

    if-nez v17, :cond_19

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/exoplayer/d/a/g;->j()I

    move-result v2

    :goto_10
    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v0, v2

    move-wide/from16 v29, v0

    div-long v29, v29, v27

    move-wide/from16 v0, v29

    long-to-int v2, v0

    aput v2, v25, v11

    :cond_7
    const-wide/16 v29, 0x3e8

    mul-long v29, v29, v9

    div-long v29, v29, v27

    move-wide/from16 v0, v29

    long-to-int v2, v0

    aput v2, v24, v11

    aput v7, v23, v11

    shr-int/lit8 v2, v3, 0x10

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_1a

    const/4 v2, 0x1

    :goto_11
    iget v3, v15, Lcom/google/android/exoplayer/d/a/i;->b:I

    const v7, 0x76696465

    if-ne v3, v7, :cond_8

    if-eqz v17, :cond_8

    if-eqz v11, :cond_8

    const/4 v2, 0x0

    :cond_8
    if-eqz v2, :cond_9

    const/4 v2, 0x1

    aput-boolean v2, v26, v11

    :cond_9
    int-to-long v2, v8

    add-long/2addr v2, v9

    add-int/lit8 v7, v11, 0x1

    move-wide v9, v2

    move v11, v7

    goto :goto_c

    :cond_a
    const v2, 0x74666474

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/d/a/b;->a(I)Lcom/google/android/exoplayer/d/a/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/exoplayer/d/a/c;->a()Lcom/google/android/exoplayer/d/a/g;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    invoke-virtual {v2}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/exoplayer/d/a/f;->b(I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_b

    invoke-virtual {v2}, Lcom/google/android/exoplayer/d/a/g;->k()J

    move-result-wide v2

    goto/16 :goto_2

    :cond_b
    invoke-virtual {v2}, Lcom/google/android/exoplayer/d/a/g;->f()J

    move-result-wide v2

    goto/16 :goto_2

    :cond_c
    iget v4, v8, Lcom/google/android/exoplayer/d/a/e;->a:I

    move v7, v4

    goto/16 :goto_3

    :cond_d
    iget v4, v8, Lcom/google/android/exoplayer/d/a/e;->b:I

    move v6, v4

    goto/16 :goto_4

    :cond_e
    iget v4, v8, Lcom/google/android/exoplayer/d/a/e;->c:I

    move v5, v4

    goto/16 :goto_5

    :cond_f
    iget v4, v8, Lcom/google/android/exoplayer/d/a/e;->d:I

    goto/16 :goto_6

    :cond_10
    const/4 v4, 0x0

    goto/16 :goto_7

    :cond_11
    const/4 v5, 0x0

    move v14, v5

    goto/16 :goto_8

    :cond_12
    const/4 v5, 0x0

    move v13, v5

    goto/16 :goto_9

    :cond_13
    const/4 v5, 0x0

    move v12, v5

    goto/16 :goto_a

    :cond_14
    const/4 v5, 0x0

    goto/16 :goto_b

    :cond_15
    move-object/from16 v0, v19

    iget v2, v0, Lcom/google/android/exoplayer/d/a/e;->b:I

    move v8, v2

    goto/16 :goto_d

    :cond_16
    move-object/from16 v0, v19

    iget v2, v0, Lcom/google/android/exoplayer/d/a/e;->c:I

    move v7, v2

    goto/16 :goto_e

    :cond_17
    if-eqz v12, :cond_18

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v2

    move v3, v2

    goto/16 :goto_f

    :cond_18
    move-object/from16 v0, v19

    iget v2, v0, Lcom/google/android/exoplayer/d/a/e;->d:I

    move v3, v2

    goto/16 :goto_f

    :cond_19
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v2

    goto/16 :goto_10

    :cond_1a
    const/4 v2, 0x0

    goto/16 :goto_11

    :cond_1b
    move-object/from16 v0, v23

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/android/exoplayer/d/a/k;->c:[I

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/android/exoplayer/d/a/k;->d:[I

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/android/exoplayer/d/a/k;->e:[I

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/android/exoplayer/d/a/k;->f:[Z

    move-object/from16 v0, v23

    array-length v2, v0

    move-object/from16 v0, v16

    iput v2, v0, Lcom/google/android/exoplayer/d/a/k;->b:I

    const v2, 0x75756964

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/d/a/b;->a(I)Lcom/google/android/exoplayer/d/a/c;

    move-result-object v2

    if-eqz v2, :cond_1f

    invoke-virtual {v2}, Lcom/google/android/exoplayer/d/a/c;->a()Lcom/google/android/exoplayer/d/a/g;

    move-result-object v3

    const/16 v2, 0x8

    invoke-virtual {v3, v2}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    const/16 v2, 0x10

    new-array v2, v2, [B

    const/4 v4, 0x0

    const/16 v5, 0x10

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/exoplayer/d/a/g;->a([BII)V

    sget-object v4, Lcom/google/android/exoplayer/d/a/f;->b:[B

    invoke-static {v2, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_1f

    invoke-virtual {v3}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v2

    const v4, 0xffffff

    and-int/2addr v2, v4

    and-int/lit8 v4, v2, 0x1

    if-eqz v4, :cond_1c

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Overriding TrackEncryptionBox parameters is unsupported"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1c
    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_1d

    const/4 v2, 0x1

    :goto_12
    invoke-virtual {v3}, Lcom/google/android/exoplayer/d/a/g;->j()I

    move-result v4

    move-object/from16 v0, v16

    iget v5, v0, Lcom/google/android/exoplayer/d/a/k;->b:I

    if-eq v4, v5, :cond_1e

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Length mismatch: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    iget v4, v0, Lcom/google/android/exoplayer/d/a/k;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1d
    const/4 v2, 0x0

    goto :goto_12

    :cond_1e
    invoke-virtual {v3}, Lcom/google/android/exoplayer/d/a/g;->b()I

    move-result v4

    invoke-virtual {v3}, Lcom/google/android/exoplayer/d/a/g;->c()I

    move-result v5

    sub-int/2addr v4, v5

    new-instance v5, Lcom/google/android/exoplayer/d/a/g;

    invoke-direct {v5, v4}, Lcom/google/android/exoplayer/d/a/g;-><init>(I)V

    invoke-virtual {v5}, Lcom/google/android/exoplayer/d/a/g;->a()[B

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v5}, Lcom/google/android/exoplayer/d/a/g;->b()I

    move-result v7

    invoke-virtual {v3, v4, v6, v7}, Lcom/google/android/exoplayer/d/a/g;->a([BII)V

    move-object/from16 v0, v16

    iput-object v5, v0, Lcom/google/android/exoplayer/d/a/k;->j:Lcom/google/android/exoplayer/d/a/g;

    move-object/from16 v0, v16

    iput-boolean v2, v0, Lcom/google/android/exoplayer/d/a/k;->i:Z

    :cond_1f
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/exoplayer/d/a/f;->s:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/exoplayer/d/a/f;->u:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/exoplayer/d/a/f;->t:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->r:I

    if-eqz v2, :cond_22

    const/4 v2, 0x0

    :goto_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget v3, v3, Lcom/google/android/exoplayer/d/a/k;->b:I

    if-ge v2, v3, :cond_21

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget-object v3, v3, Lcom/google/android/exoplayer/d/a/k;->f:[Z

    aget-boolean v3, v3, v2

    if-eqz v3, :cond_20

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    invoke-virtual {v3, v2}, Lcom/google/android/exoplayer/d/a/k;->a(I)I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/exoplayer/d/a/f;->r:I

    if-gt v3, v4, :cond_20

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/exoplayer/d/a/f;->t:I

    :cond_20
    add-int/lit8 v2, v2, 0x1

    goto :goto_13

    :cond_21
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/exoplayer/d/a/f;->r:I

    :cond_22
    return-void
.end method

.method private static b(Lcom/google/android/exoplayer/d/a/g;)[B
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->e()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->c()I

    move-result v1

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/d/a/g;->a()[B

    move-result-object v2

    invoke-static {v2, v1, v0}, Lcom/google/android/exoplayer/d/a/d;->a([BII)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/exoplayer/upstream/p;Lcom/google/android/exoplayer/ah;)I
    .locals 22

    const/4 v1, 0x0

    move v10, v1

    :goto_0
    and-int/lit8 v1, v10, 0x7

    if-nez v1, :cond_1d

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/d/a/f;->i:I

    packed-switch v1, :pswitch_data_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/d/a/f;->s:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget v2, v2, Lcom/google/android/exoplayer/d/a/k;->b:I

    if-lt v1, v2, :cond_14

    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/d/a/f;->a(I)V

    const/4 v1, 0x0

    :goto_1
    or-int/2addr v1, v10

    move v10, v1

    goto :goto_0

    :pswitch_0
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/d/a/f;->j:I

    rsub-int/lit8 v1, v1, 0x8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/d/a/f;->f:Lcom/google/android/exoplayer/d/a/g;

    invoke-virtual {v2}, Lcom/google/android/exoplayer/d/a/g;->a()[B

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/exoplayer/d/a/f;->j:I

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/exoplayer/upstream/p;->a([BII)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x2

    :goto_2
    or-int/2addr v1, v10

    move v10, v1

    goto :goto_0

    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->k:I

    add-int/2addr v2, v1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/exoplayer/d/a/f;->k:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->j:I

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/exoplayer/d/a/f;->j:I

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/d/a/f;->j:I

    const/16 v2, 0x8

    if-eq v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_2

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->f:Lcom/google/android/exoplayer/d/a/g;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->f:Lcom/google/android/exoplayer/d/a/g;

    invoke-virtual {v1}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/exoplayer/d/a/f;->m:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->f:Lcom/google/android/exoplayer/d/a/g;

    invoke-virtual {v1}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/exoplayer/d/a/f;->l:I

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/d/a/f;->l:I

    const v2, 0x6d646174

    if-ne v1, v2, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget v1, v1, Lcom/google/android/exoplayer/d/a/k;->g:I

    if-lez v1, :cond_2

    new-instance v2, Lcom/google/android/exoplayer/d/a/g;

    invoke-direct {v2, v1}, Lcom/google/android/exoplayer/d/a/g;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/exoplayer/d/a/f;->o:Lcom/google/android/exoplayer/d/a/g;

    const/4 v1, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/d/a/f;->a(I)V

    :goto_3
    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->o:Lcom/google/android/exoplayer/d/a/g;

    const/4 v1, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/d/a/f;->a(I)V

    goto :goto_3

    :cond_3
    sget-object v1, Lcom/google/android/exoplayer/d/a/f;->c:Ljava/util/Set;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Lcom/google/android/exoplayer/d/a/f;->d:Ljava/util/Set;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/d/a/f;->a(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->g:Ljava/util/Stack;

    new-instance v2, Lcom/google/android/exoplayer/d/a/b;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/exoplayer/d/a/f;->l:I

    invoke-direct {v2, v3}, Lcom/google/android/exoplayer/d/a/b;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->h:Ljava/util/Stack;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->k:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/exoplayer/d/a/f;->m:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    :goto_4
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_4
    new-instance v1, Lcom/google/android/exoplayer/d/a/g;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->m:I

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer/d/a/g;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->n:Lcom/google/android/exoplayer/d/a/g;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->f:Lcom/google/android/exoplayer/d/a/g;

    invoke-virtual {v1}, Lcom/google/android/exoplayer/d/a/g;->a()[B

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/d/a/f;->n:Lcom/google/android/exoplayer/d/a/g;

    invoke-virtual {v3}, Lcom/google/android/exoplayer/d/a/g;->a()[B

    move-result-object v3

    const/4 v4, 0x0

    const/16 v5, 0x8

    invoke-static {v1, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/d/a/f;->a(I)V

    goto :goto_4

    :cond_5
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->n:Lcom/google/android/exoplayer/d/a/g;

    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/d/a/f;->a(I)V

    goto :goto_4

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->n:Lcom/google/android/exoplayer/d/a/g;

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->n:Lcom/google/android/exoplayer/d/a/g;

    invoke-virtual {v1}, Lcom/google/android/exoplayer/d/a/g;->a()[B

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->j:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/exoplayer/d/a/f;->m:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/exoplayer/d/a/f;->j:I

    sub-int/2addr v3, v4

    move-object/from16 v0, p1

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/exoplayer/upstream/p;->a([BII)I

    move-result v1

    :goto_5
    const/4 v2, -0x1

    if-ne v1, v2, :cond_7

    const/4 v3, 0x2

    :goto_6
    or-int v1, v10, v3

    move v10, v1

    goto/16 :goto_0

    :cond_6
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/d/a/f;->m:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->j:I

    sub-int/2addr v1, v2

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/upstream/p;->a(I)I

    move-result v1

    goto :goto_5

    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->k:I

    add-int/2addr v2, v1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/exoplayer/d/a/f;->k:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->j:I

    add-int/2addr v1, v2

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/exoplayer/d/a/f;->j:I

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/d/a/f;->j:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->m:I

    if-eq v1, v2, :cond_8

    const/4 v3, 0x1

    goto :goto_6

    :cond_8
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/d/a/f;->n:Lcom/google/android/exoplayer/d/a/g;

    if-eqz v2, :cond_1e

    new-instance v2, Lcom/google/android/exoplayer/d/a/c;

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/d/a/f;->l:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/d/a/f;->n:Lcom/google/android/exoplayer/d/a/g;

    invoke-direct {v2, v1, v3}, Lcom/google/android/exoplayer/d/a/c;-><init>(ILcom/google/android/exoplayer/d/a/g;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->g:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->g:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer/d/a/b;

    invoke-virtual {v1, v2}, Lcom/google/android/exoplayer/d/a/b;->a(Lcom/google/android/exoplayer/d/a/a;)V

    :cond_9
    const/4 v1, 0x0

    :goto_7
    or-int/lit8 v1, v1, 0x0

    move v3, v1

    :goto_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->h:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->h:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->k:I

    if-ne v1, v2, :cond_11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->h:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->g:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer/d/a/b;

    iget v2, v1, Lcom/google/android/exoplayer/d/a/b;->a:I

    const v4, 0x6d6f6f76

    if-ne v2, v4, :cond_e

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/d/a/f;->a(Lcom/google/android/exoplayer/d/a/b;)V

    const/16 v1, 0x10

    :goto_9
    or-int/2addr v1, v3

    move v3, v1

    goto :goto_8

    :cond_a
    iget v1, v2, Lcom/google/android/exoplayer/d/a/c;->a:I

    const v3, 0x73696478

    if-ne v1, v3, :cond_9

    invoke-virtual {v2}, Lcom/google/android/exoplayer/d/a/c;->a()Lcom/google/android/exoplayer/d/a/g;

    move-result-object v11

    const/16 v1, 0x8

    invoke-virtual {v11, v1}, Lcom/google/android/exoplayer/d/a/g;->a(I)V

    invoke-virtual {v11}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/exoplayer/d/a/f;->b(I)I

    move-result v1

    const/4 v2, 0x4

    invoke-virtual {v11, v2}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    invoke-virtual {v11}, Lcom/google/android/exoplayer/d/a/g;->f()J

    move-result-wide v12

    if-nez v1, :cond_b

    invoke-virtual {v11}, Lcom/google/android/exoplayer/d/a/g;->f()J

    move-result-wide v3

    invoke-virtual {v11}, Lcom/google/android/exoplayer/d/a/g;->f()J

    move-result-wide v1

    move-wide v7, v3

    :goto_a
    const/4 v3, 0x2

    invoke-virtual {v11, v3}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    invoke-virtual {v11}, Lcom/google/android/exoplayer/d/a/g;->e()I

    move-result v14

    new-array v3, v14, [I

    new-array v4, v14, [J

    new-array v5, v14, [J

    new-array v6, v14, [J

    const/4 v9, 0x0

    :goto_b
    if-ge v9, v14, :cond_d

    invoke-virtual {v11}, Lcom/google/android/exoplayer/d/a/g;->g()I

    move-result v15

    const/high16 v16, -0x80000000

    and-int v16, v16, v15

    if-eqz v16, :cond_c

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unhandled indirect reference"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_b
    invoke-virtual {v11}, Lcom/google/android/exoplayer/d/a/g;->k()J

    move-result-wide v3

    invoke-virtual {v11}, Lcom/google/android/exoplayer/d/a/g;->k()J

    move-result-wide v1

    move-wide v7, v3

    goto :goto_a

    :cond_c
    invoke-virtual {v11}, Lcom/google/android/exoplayer/d/a/g;->f()J

    move-result-wide v16

    const v18, 0x7fffffff

    and-int v15, v15, v18

    aput v15, v3, v9

    aput-wide v1, v4, v9

    const-wide/32 v18, 0xf4240

    mul-long v18, v18, v7

    div-long v18, v18, v12

    aput-wide v18, v6, v9

    add-long v18, v7, v16

    const-wide/32 v20, 0xf4240

    mul-long v18, v18, v20

    div-long v18, v18, v12

    aget-wide v20, v6, v9

    sub-long v18, v18, v20

    aput-wide v18, v5, v9

    add-long v7, v7, v16

    const/4 v15, 0x4

    invoke-virtual {v11, v15}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    aget v15, v3, v9

    int-to-long v15, v15

    add-long/2addr v1, v15

    add-int/lit8 v9, v9, 0x1

    goto :goto_b

    :cond_d
    new-instance v1, Lcom/google/android/exoplayer/d/a/h;

    invoke-virtual {v11}, Lcom/google/android/exoplayer/d/a/g;->b()I

    move-result v2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/exoplayer/d/a/h;-><init>(I[I[J[J[J)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->w:Lcom/google/android/exoplayer/d/a/h;

    const/16 v1, 0x20

    goto/16 :goto_7

    :cond_e
    iget v2, v1, Lcom/google/android/exoplayer/d/a/b;->a:I

    const v4, 0x6d6f6f66

    if-ne v2, v4, :cond_10

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/d/a/f;->b(Lcom/google/android/exoplayer/d/a/b;)V

    :cond_f
    :goto_c
    const/4 v1, 0x0

    goto/16 :goto_9

    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/d/a/f;->g:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/d/a/f;->g:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer/d/a/b;

    invoke-virtual {v2, v1}, Lcom/google/android/exoplayer/d/a/b;->a(Lcom/google/android/exoplayer/d/a/a;)V

    goto :goto_c

    :cond_11
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/d/a/f;->a(I)V

    goto/16 :goto_6

    :pswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->o:Lcom/google/android/exoplayer/d/a/g;

    invoke-virtual {v1}, Lcom/google/android/exoplayer/d/a/g;->b()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/d/a/f;->o:Lcom/google/android/exoplayer/d/a/g;

    invoke-virtual {v2}, Lcom/google/android/exoplayer/d/a/g;->a()[B

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/exoplayer/d/a/f;->p:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/exoplayer/d/a/f;->p:I

    sub-int v4, v1, v4

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4}, Lcom/google/android/exoplayer/upstream/p;->a([BII)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_12

    const/4 v1, 0x2

    :goto_d
    or-int/2addr v1, v10

    move v10, v1

    goto/16 :goto_0

    :cond_12
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/exoplayer/d/a/f;->p:I

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/exoplayer/d/a/f;->p:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->p:I

    if-ge v2, v1, :cond_13

    const/4 v1, 0x1

    goto :goto_d

    :cond_13
    const/4 v1, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/d/a/f;->a(I)V

    const/4 v1, 0x0

    goto :goto_d

    :cond_14
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/d/a/f;->s:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->t:I

    if-ge v1, v2, :cond_1c

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/d/a/f;->i:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_15

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->o:Lcom/google/android/exoplayer/d/a/g;

    if-eqz v1, :cond_16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->o:Lcom/google/android/exoplayer/d/a/g;

    move-object v3, v1

    :goto_e
    if-eqz v3, :cond_15

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->x:Lcom/google/android/exoplayer/d/a/i;

    iget-object v1, v1, Lcom/google/android/exoplayer/d/a/i;->e:[Lcom/google/android/exoplayer/d/a/j;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget v2, v2, Lcom/google/android/exoplayer/d/a/k;->a:I

    aget-object v1, v1, v2

    iget v2, v1, Lcom/google/android/exoplayer/d/a/j;->b:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->o:Lcom/google/android/exoplayer/d/a/g;

    if-eqz v1, :cond_18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget-object v1, v1, Lcom/google/android/exoplayer/d/a/k;->h:[I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/exoplayer/d/a/f;->s:I

    aget v1, v1, v4

    if-le v1, v2, :cond_17

    const/4 v1, 0x1

    :goto_f
    invoke-virtual {v3, v2}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    if-eqz v1, :cond_19

    invoke-virtual {v3}, Lcom/google/android/exoplayer/d/a/g;->e()I

    move-result v2

    :goto_10
    if-eqz v1, :cond_15

    mul-int/lit8 v1, v2, 0x6

    invoke-virtual {v3, v1}, Lcom/google/android/exoplayer/d/a/g;->b(I)V

    :cond_15
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget-object v1, v1, Lcom/google/android/exoplayer/d/a/k;->c:[I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->s:I

    aget v1, v1, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->q:I

    sub-int v2, v1, v2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lcom/google/android/exoplayer/upstream/p;->a(I)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1a

    const/4 v1, 0x2

    goto/16 :goto_1

    :cond_16
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget-object v1, v1, Lcom/google/android/exoplayer/d/a/k;->j:Lcom/google/android/exoplayer/d/a/g;

    move-object v3, v1

    goto :goto_e

    :cond_17
    const/4 v1, 0x0

    goto :goto_f

    :cond_18
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget-boolean v1, v1, Lcom/google/android/exoplayer/d/a/k;->i:Z

    goto :goto_f

    :cond_19
    const/4 v2, 0x1

    goto :goto_10

    :cond_1a
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/exoplayer/d/a/f;->q:I

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/exoplayer/d/a/f;->q:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/exoplayer/d/a/f;->q:I

    if-eq v1, v2, :cond_1b

    const/4 v1, 0x4

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/d/a/f;->a(I)V

    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_1b
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/exoplayer/d/a/f;->s:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/exoplayer/d/a/f;->s:I

    const/4 v1, 0x3

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/d/a/f;->a(I)V

    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_1c
    invoke-direct/range {p0 .. p2}, Lcom/google/android/exoplayer/d/a/f;->b(Lcom/google/android/exoplayer/upstream/p;Lcom/google/android/exoplayer/ah;)I

    move-result v1

    goto/16 :goto_1

    :cond_1d
    return v10

    :cond_1e
    move v3, v1

    goto/16 :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a()Lcom/google/android/exoplayer/d/a/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->w:Lcom/google/android/exoplayer/d/a/h;

    return-object v0
.end method

.method public final a(JZ)Z
    .locals 6

    const/4 v1, 0x0

    const-wide/16 v2, 0x3e8

    div-long v2, p1, v2

    long-to-int v0, v2

    iput v0, p0, Lcom/google/android/exoplayer/d/a/f;->r:I

    if-eqz p3, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/exoplayer/d/a/f;->r:I

    iget-object v2, p0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    invoke-virtual {v2, v1}, Lcom/google/android/exoplayer/d/a/k;->a(I)I

    move-result v2

    if-lt v0, v2, :cond_3

    iget v0, p0, Lcom/google/android/exoplayer/d/a/f;->r:I

    iget-object v2, p0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget-object v3, p0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget v3, v3, Lcom/google/android/exoplayer/d/a/k;->b:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer/d/a/k;->a(I)I

    move-result v2

    if-gt v0, v2, :cond_3

    move v0, v1

    move v2, v1

    move v3, v1

    :goto_0
    iget-object v4, p0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget v4, v4, Lcom/google/android/exoplayer/d/a/k;->b:I

    if-ge v0, v4, :cond_2

    iget-object v4, p0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    invoke-virtual {v4, v0}, Lcom/google/android/exoplayer/d/a/k;->a(I)I

    move-result v4

    iget v5, p0, Lcom/google/android/exoplayer/d/a/f;->r:I

    if-gt v4, v5, :cond_1

    iget-object v3, p0, Lcom/google/android/exoplayer/d/a/f;->z:Lcom/google/android/exoplayer/d/a/k;

    iget-object v3, v3, Lcom/google/android/exoplayer/d/a/k;->f:[Z

    aget-boolean v3, v3, v0

    if-eqz v3, :cond_0

    move v2, v0

    :cond_0
    move v3, v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/exoplayer/d/a/f;->u:I

    if-ne v2, v0, :cond_3

    iget v0, p0, Lcom/google/android/exoplayer/d/a/f;->s:I

    if-lt v3, v0, :cond_3

    iput v1, p0, Lcom/google/android/exoplayer/d/a/f;->r:I

    :goto_1
    return v1

    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->g:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    invoke-direct {p0, v1}, Lcom/google/android/exoplayer/d/a/f;->a(I)V

    const/4 v1, 0x1

    goto :goto_1
.end method

.method public final b()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->v:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->v:Ljava/util/HashMap;

    goto :goto_0
.end method

.method public final c()Lcom/google/android/exoplayer/ag;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->x:Lcom/google/android/exoplayer/d/a/i;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->x:Lcom/google/android/exoplayer/d/a/i;

    iget-object v0, v0, Lcom/google/android/exoplayer/d/a/i;->d:Lcom/google/android/exoplayer/ag;

    goto :goto_0
.end method

.method public final d()Lcom/google/android/exoplayer/d/a/i;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/d/a/f;->x:Lcom/google/android/exoplayer/d/a/i;

    return-object v0
.end method
