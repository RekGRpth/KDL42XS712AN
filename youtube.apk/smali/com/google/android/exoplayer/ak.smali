.class public abstract Lcom/google/android/exoplayer/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/e;


# instance fields
.field private a:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a()I
.end method

.method public a(ILjava/lang/Object;)V
    .locals 0

    return-void
.end method

.method protected abstract a(J)V
.end method

.method protected a(JZ)V
    .locals 0

    return-void
.end method

.method protected b()V
    .locals 0

    return-void
.end method

.method protected abstract b(J)V
.end method

.method final b(JZ)V
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/exoplayer/ak;->a:I

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/exoplayer/ak;->a:I

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/exoplayer/ak;->a(JZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c()V
    .locals 0

    return-void
.end method

.method protected abstract d()J
.end method

.method protected abstract e()J
.end method

.method protected abstract f()J
.end method

.method protected g()V
    .locals 0

    return-void
.end method

.method protected h()V
    .locals 0

    return-void
.end method

.method protected abstract i()Z
.end method

.method protected abstract j()Z
.end method

.method protected k()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final p()I
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer/ak;->a:I

    return v0
.end method

.method final q()I
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget v0, p0, Lcom/google/android/exoplayer/ak;->a:I

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/ak;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer/ak;->a:I

    iget v0, p0, Lcom/google/android/exoplayer/ak;->a:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer/ak;->a:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer/ak;->a:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    invoke-static {v2}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iget v0, p0, Lcom/google/android/exoplayer/ak;->a:I

    return v0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method final r()V
    .locals 2

    iget v0, p0, Lcom/google/android/exoplayer/ak;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/exoplayer/ak;->a:I

    invoke-virtual {p0}, Lcom/google/android/exoplayer/ak;->g()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final s()V
    .locals 2

    iget v0, p0, Lcom/google/android/exoplayer/ak;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/exoplayer/ak;->a:I

    invoke-virtual {p0}, Lcom/google/android/exoplayer/ak;->h()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final t()V
    .locals 3

    const/4 v1, 0x1

    iget v0, p0, Lcom/google/android/exoplayer/ak;->a:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iput v1, p0, Lcom/google/android/exoplayer/ak;->a:I

    invoke-virtual {p0}, Lcom/google/android/exoplayer/ak;->b()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final u()V
    .locals 3

    const/4 v2, -0x2

    iget v0, p0, Lcom/google/android/exoplayer/ak;->a:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer/ak;->a:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer/ak;->a:I

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iput v2, p0, Lcom/google/android/exoplayer/ak;->a:I

    invoke-virtual {p0}, Lcom/google/android/exoplayer/ak;->c()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
