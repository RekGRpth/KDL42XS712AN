.class final Lcom/google/android/exoplayer/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/d;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lcom/google/android/exoplayer/j;

.field private final c:Ljava/util/concurrent/CopyOnWriteArraySet;

.field private final d:[Z

.field private e:Z

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(III)V
    .locals 6

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/google/android/exoplayer/h;->f:I

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/h;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    new-array v0, p1, [Z

    iput-object v0, p0, Lcom/google/android/exoplayer/h;->d:[Z

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/exoplayer/h;->d:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer/h;->d:[Z

    aput-boolean v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/exoplayer/i;

    invoke-direct {v0, p0}, Lcom/google/android/exoplayer/i;-><init>(Lcom/google/android/exoplayer/h;)V

    iput-object v0, p0, Lcom/google/android/exoplayer/h;->a:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/exoplayer/j;

    iget-object v1, p0, Lcom/google/android/exoplayer/h;->a:Landroid/os/Handler;

    iget-boolean v2, p0, Lcom/google/android/exoplayer/h;->e:Z

    iget-object v3, p0, Lcom/google/android/exoplayer/h;->d:[Z

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/exoplayer/j;-><init>(Landroid/os/Handler;Z[ZII)V

    iput-object v0, p0, Lcom/google/android/exoplayer/h;->b:Lcom/google/android/exoplayer/j;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/exoplayer/h;->f:I

    return v0
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/h;->b:Lcom/google/android/exoplayer/j;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer/j;->a(I)V

    return-void
.end method

.method public final a(IZ)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/exoplayer/h;->d:[Z

    aget-boolean v0, v0, v1

    if-eq v0, p2, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/h;->d:[Z

    aput-boolean p2, v0, v1

    iget-object v0, p0, Lcom/google/android/exoplayer/h;->b:Lcom/google/android/exoplayer/j;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/exoplayer/j;->a(IZ)V

    :cond_0
    return-void
.end method

.method final a(Landroid/os/Message;)V
    .locals 4

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    return-void

    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, Lcom/google/android/exoplayer/h;->f:I

    iget-object v0, p0, Lcom/google/android/exoplayer/h;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/g;

    iget-boolean v2, p0, Lcom/google/android/exoplayer/h;->e:Z

    iget v3, p0, Lcom/google/android/exoplayer/h;->f:I

    invoke-interface {v0, v2, v3}, Lcom/google/android/exoplayer/g;->a(ZI)V

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/google/android/exoplayer/h;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer/h;->g:I

    iget v0, p0, Lcom/google/android/exoplayer/h;->g:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/h;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_1

    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/exoplayer/ExoPlaybackException;

    iget-object v1, p0, Lcom/google/android/exoplayer/h;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer/g;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer/g;->a(Lcom/google/android/exoplayer/ExoPlaybackException;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/exoplayer/e;ILjava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/h;->b:Lcom/google/android/exoplayer/j;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer/j;->a(Lcom/google/android/exoplayer/e;ILjava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/android/exoplayer/g;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/h;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Z)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/exoplayer/h;->e:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/exoplayer/h;->e:Z

    iget v0, p0, Lcom/google/android/exoplayer/h;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer/h;->g:I

    iget-object v0, p0, Lcom/google/android/exoplayer/h;->b:Lcom/google/android/exoplayer/j;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer/j;->a(Z)V

    iget-object v0, p0, Lcom/google/android/exoplayer/h;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/g;

    iget v2, p0, Lcom/google/android/exoplayer/h;->f:I

    invoke-interface {v0, p1, v2}, Lcom/google/android/exoplayer/g;->a(ZI)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final varargs a([Lcom/google/android/exoplayer/ak;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/h;->b:Lcom/google/android/exoplayer/j;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer/j;->a([Lcom/google/android/exoplayer/ak;)V

    return-void
.end method

.method public final b(Lcom/google/android/exoplayer/e;ILjava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/exoplayer/h;->b:Lcom/google/android/exoplayer/j;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/exoplayer/j;->b(Lcom/google/android/exoplayer/e;ILjava/lang/Object;)V

    return-void
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer/h;->e:Z

    return v0
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/h;->b:Lcom/google/android/exoplayer/j;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/j;->d()V

    iget-object v0, p0, Lcom/google/android/exoplayer/h;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method

.method public final d()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/h;->b:Lcom/google/android/exoplayer/j;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/j;->c()I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/h;->b:Lcom/google/android/exoplayer/j;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/j;->a()I

    move-result v0

    return v0
.end method

.method public final f()I
    .locals 3

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/google/android/exoplayer/h;->b:Lcom/google/android/exoplayer/j;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/j;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/exoplayer/h;->b:Lcom/google/android/exoplayer/j;

    invoke-virtual {v1}, Lcom/google/android/exoplayer/j;->c()I

    move-result v1

    if-eq v0, v2, :cond_0

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    if-nez v1, :cond_2

    const/16 v0, 0x64

    goto :goto_0

    :cond_2
    mul-int/lit8 v0, v0, 0x64

    div-int/2addr v0, v1

    goto :goto_0
.end method
