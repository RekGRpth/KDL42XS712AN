.class public final Lcom/google/android/exoplayer/e/c;
.super Ljava/io/InputStream;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/exoplayer/upstream/i;

.field private final b:Lcom/google/android/exoplayer/upstream/j;

.field private final c:[B

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/j;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/exoplayer/e/c;->d:Z

    iput-boolean v0, p0, Lcom/google/android/exoplayer/e/c;->e:Z

    iput-object p1, p0, Lcom/google/android/exoplayer/e/c;->a:Lcom/google/android/exoplayer/upstream/i;

    iput-object p2, p0, Lcom/google/android/exoplayer/e/c;->b:Lcom/google/android/exoplayer/upstream/j;

    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/exoplayer/e/c;->c:[B

    return-void
.end method

.method private a()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/exoplayer/e/c;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/e/c;->a:Lcom/google/android/exoplayer/upstream/i;

    iget-object v1, p0, Lcom/google/android/exoplayer/e/c;->b:Lcom/google/android/exoplayer/upstream/j;

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/upstream/i;->a(Lcom/google/android/exoplayer/upstream/j;)J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer/e/c;->d:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer/e/c;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/e/c;->a:Lcom/google/android/exoplayer/upstream/i;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/i;->a()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer/e/c;->e:Z

    :cond_0
    return-void
.end method

.method public final read()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/e/c;->c:[B

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/e/c;->read([B)I

    iget-object v0, p0, Lcom/google/android/exoplayer/e/c;->c:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    return v0
.end method

.method public final read([B)I
    .locals 2

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/exoplayer/e/c;->read([BII)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer/e/c;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    invoke-direct {p0}, Lcom/google/android/exoplayer/e/c;->a()V

    iget-object v0, p0, Lcom/google/android/exoplayer/e/c;->a:Lcom/google/android/exoplayer/upstream/i;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/exoplayer/upstream/i;->a([BII)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final skip(J)J
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/exoplayer/e/c;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    invoke-direct {p0}, Lcom/google/android/exoplayer/e/c;->a()V

    invoke-super {p0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    return-wide v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
