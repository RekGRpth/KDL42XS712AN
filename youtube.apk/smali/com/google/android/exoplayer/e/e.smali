.class public final Lcom/google/android/exoplayer/e/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Comparator;

.field private static final b:Ljava/util/Comparator;


# instance fields
.field private final c:I

.field private final d:Ljava/util/ArrayList;

.field private final e:[Lcom/google/android/exoplayer/e/h;

.field private f:I

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/exoplayer/e/f;

    invoke-direct {v0}, Lcom/google/android/exoplayer/e/f;-><init>()V

    sput-object v0, Lcom/google/android/exoplayer/e/e;->a:Ljava/util/Comparator;

    new-instance v0, Lcom/google/android/exoplayer/e/g;

    invoke-direct {v0}, Lcom/google/android/exoplayer/e/g;-><init>()V

    sput-object v0, Lcom/google/android/exoplayer/e/e;->b:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/exoplayer/e/e;->c:I

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/exoplayer/e/h;

    iput-object v0, p0, Lcom/google/android/exoplayer/e/e;->e:[Lcom/google/android/exoplayer/e/h;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/e/e;->d:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer/e/e;->f:I

    return-void
.end method


# virtual methods
.method public final a(F)F
    .locals 5

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/exoplayer/e/e;->f:I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer/e/e;->d:Ljava/util/ArrayList;

    sget-object v2, Lcom/google/android/exoplayer/e/e;->b:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iput v0, p0, Lcom/google/android/exoplayer/e/e;->f:I

    :cond_0
    const/high16 v1, 0x3f000000    # 0.5f

    iget v2, p0, Lcom/google/android/exoplayer/e/e;->h:I

    int-to-float v2, v2

    mul-float v3, v1, v2

    move v1, v0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer/e/e;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer/e/e;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/e/h;

    iget v4, v0, Lcom/google/android/exoplayer/e/h;->b:I

    add-int/2addr v2, v4

    int-to-float v4, v2

    cmpl-float v4, v4, v3

    if-ltz v4, :cond_1

    iget v0, v0, Lcom/google/android/exoplayer/e/h;->c:F

    :goto_1
    return v0

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer/e/e;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    const/high16 v0, 0x7fc00000    # NaNf

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer/e/e;->d:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/exoplayer/e/e;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/e/h;

    iget v0, v0, Lcom/google/android/exoplayer/e/h;->c:F

    goto :goto_1
.end method

.method public final a(IF)V
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    iget v0, p0, Lcom/google/android/exoplayer/e/e;->f:I

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/e/e;->d:Ljava/util/ArrayList;

    sget-object v1, Lcom/google/android/exoplayer/e/e;->a:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iput v2, p0, Lcom/google/android/exoplayer/e/e;->f:I

    :cond_0
    iget v0, p0, Lcom/google/android/exoplayer/e/e;->i:I

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer/e/e;->e:[Lcom/google/android/exoplayer/e/h;

    iget v1, p0, Lcom/google/android/exoplayer/e/e;->i:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/exoplayer/e/e;->i:I

    aget-object v0, v0, v1

    :goto_0
    iget v1, p0, Lcom/google/android/exoplayer/e/e;->g:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/exoplayer/e/e;->g:I

    iput v1, v0, Lcom/google/android/exoplayer/e/h;->a:I

    iput p1, v0, Lcom/google/android/exoplayer/e/h;->b:I

    iput p2, v0, Lcom/google/android/exoplayer/e/h;->c:F

    iget-object v1, p0, Lcom/google/android/exoplayer/e/e;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v0, p0, Lcom/google/android/exoplayer/e/e;->h:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/exoplayer/e/e;->h:I

    :cond_1
    :goto_1
    iget v0, p0, Lcom/google/android/exoplayer/e/e;->h:I

    iget v1, p0, Lcom/google/android/exoplayer/e/e;->c:I

    if-le v0, v1, :cond_4

    iget v0, p0, Lcom/google/android/exoplayer/e/e;->h:I

    iget v1, p0, Lcom/google/android/exoplayer/e/e;->c:I

    sub-int v1, v0, v1

    iget-object v0, p0, Lcom/google/android/exoplayer/e/e;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/e/h;

    iget v2, v0, Lcom/google/android/exoplayer/e/h;->b:I

    if-gt v2, v1, :cond_3

    iget v1, p0, Lcom/google/android/exoplayer/e/e;->h:I

    iget v2, v0, Lcom/google/android/exoplayer/e/h;->b:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/exoplayer/e/e;->h:I

    iget-object v1, p0, Lcom/google/android/exoplayer/e/e;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget v1, p0, Lcom/google/android/exoplayer/e/e;->i:I

    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/exoplayer/e/e;->e:[Lcom/google/android/exoplayer/e/h;

    iget v2, p0, Lcom/google/android/exoplayer/e/e;->i:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/exoplayer/e/e;->i:I

    aput-object v0, v1, v2

    goto :goto_1

    :cond_2
    new-instance v0, Lcom/google/android/exoplayer/e/h;

    invoke-direct {v0, v4}, Lcom/google/android/exoplayer/e/h;-><init>(B)V

    goto :goto_0

    :cond_3
    iget v2, v0, Lcom/google/android/exoplayer/e/h;->b:I

    sub-int/2addr v2, v1

    iput v2, v0, Lcom/google/android/exoplayer/e/h;->b:I

    iget v0, p0, Lcom/google/android/exoplayer/e/e;->h:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/exoplayer/e/e;->h:I

    goto :goto_1

    :cond_4
    return-void
.end method
