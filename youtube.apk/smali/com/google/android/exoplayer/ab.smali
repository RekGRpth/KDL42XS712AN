.class public final Lcom/google/android/exoplayer/ab;
.super Lcom/google/android/exoplayer/w;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/exoplayer/af;

.field private final d:J

.field private final e:I

.field private final f:I

.field private g:Landroid/view/Surface;

.field private h:Z

.field private i:Z

.field private j:J

.field private k:J

.field private l:I


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/ai;Lcom/google/android/exoplayer/c/a;IJLandroid/os/Handler;Lcom/google/android/exoplayer/af;I)V
    .locals 3

    const/4 v2, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p6, p7}, Lcom/google/android/exoplayer/w;-><init>(Lcom/google/android/exoplayer/ai;Lcom/google/android/exoplayer/c/a;Landroid/os/Handler;Lcom/google/android/exoplayer/z;)V

    iput v2, p0, Lcom/google/android/exoplayer/ab;->e:I

    const-wide/32 v0, 0x4c4b40

    iput-wide v0, p0, Lcom/google/android/exoplayer/ab;->d:J

    iput-object p7, p0, Lcom/google/android/exoplayer/ab;->c:Lcom/google/android/exoplayer/af;

    iput v2, p0, Lcom/google/android/exoplayer/ab;->f:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/exoplayer/ab;->j:J

    return-void
.end method

.method static synthetic a(Lcom/google/android/exoplayer/ab;)Lcom/google/android/exoplayer/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/ab;->c:Lcom/google/android/exoplayer/af;

    return-object v0
.end method

.method private a(Landroid/media/MediaCodec;I)V
    .locals 6

    const/4 v5, 0x1

    const-string v0, "renderVideoBuffer"

    invoke-static {v0}, Lcom/google/android/exoplayer/e/j;->a(Ljava/lang/String;)V

    invoke-virtual {p1, p2, v5}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    invoke-static {}, Lcom/google/android/exoplayer/e/j;->a()V

    iget-object v0, p0, Lcom/google/android/exoplayer/ab;->a:Lcom/google/android/exoplayer/a;

    iget-wide v1, v0, Lcom/google/android/exoplayer/a;->i:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Lcom/google/android/exoplayer/a;->i:J

    iget-boolean v0, p0, Lcom/google/android/exoplayer/ab;->h:Z

    if-nez v0, :cond_0

    iput-boolean v5, p0, Lcom/google/android/exoplayer/ab;->h:Z

    iget-object v0, p0, Lcom/google/android/exoplayer/ab;->g:Landroid/view/Surface;

    iget-object v1, p0, Lcom/google/android/exoplayer/ab;->b:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer/ab;->c:Lcom/google/android/exoplayer/af;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer/ab;->b:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/exoplayer/ad;

    invoke-direct {v2, p0, v0}, Lcom/google/android/exoplayer/ad;-><init>(Lcom/google/android/exoplayer/ab;Landroid/view/Surface;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private v()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/exoplayer/ab;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/ab;->c:Lcom/google/android/exoplayer/af;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/exoplayer/ab;->l:I

    if-lez v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget v2, p0, Lcom/google/android/exoplayer/ab;->l:I

    iget-wide v3, p0, Lcom/google/android/exoplayer/ab;->k:J

    sub-long v3, v0, v3

    const/4 v5, 0x0

    iput v5, p0, Lcom/google/android/exoplayer/ab;->l:I

    iput-wide v0, p0, Lcom/google/android/exoplayer/ab;->k:J

    iget-object v0, p0, Lcom/google/android/exoplayer/ab;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/ae;

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/exoplayer/ae;-><init>(Lcom/google/android/exoplayer/ab;IJ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)V
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    check-cast p2, Landroid/view/Surface;

    iget-object v0, p0, Lcom/google/android/exoplayer/ab;->g:Landroid/view/Surface;

    if-eq v0, p2, :cond_1

    iput-object p2, p0, Lcom/google/android/exoplayer/ab;->g:Landroid/view/Surface;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer/ab;->h:Z

    invoke-virtual {p0}, Lcom/google/android/exoplayer/ab;->p()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer/ab;->o()V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/ab;->l()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-super {p0, p1, p2}, Lcom/google/android/exoplayer/w;->a(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method protected final a(J)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/android/exoplayer/w;->a(J)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer/ab;->i:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/exoplayer/ab;->j:J

    return-void
.end method

.method protected final a(JZ)V
    .locals 4

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/exoplayer/w;->a(JZ)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer/ab;->i:Z

    if-eqz p3, :cond_0

    iget-wide v0, p0, Lcom/google/android/exoplayer/ab;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/exoplayer/ab;->d:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer/ab;->j:J

    :cond_0
    return-void
.end method

.method protected final a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;Landroid/media/MediaCrypto;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/ab;->g:Landroid/view/Surface;

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    iget v0, p0, Lcom/google/android/exoplayer/ab;->e:I

    invoke-virtual {p1, v0}, Landroid/media/MediaCodec;->setVideoScalingMode(I)V

    return-void
.end method

.method protected final a(Landroid/media/MediaFormat;)V
    .locals 4

    const-string v0, "width"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    const-string v1, "height"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/exoplayer/ab;->b:Landroid/os/Handler;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/exoplayer/ab;->c:Lcom/google/android/exoplayer/af;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/exoplayer/ab;->b:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/exoplayer/ac;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/exoplayer/ac;-><init>(Lcom/google/android/exoplayer/ab;II)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method protected final a(JLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;I)Z
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-wide v2, p5, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    sub-long/2addr v2, p1

    const-wide/16 v4, -0x7530

    cmp-long v4, v2, v4

    if-gez v4, :cond_1

    const-string v2, "dropVideoBuffer"

    invoke-static {v2}, Lcom/google/android/exoplayer/e/j;->a(Ljava/lang/String;)V

    invoke-virtual {p3, p6, v1}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    invoke-static {}, Lcom/google/android/exoplayer/e/j;->a()V

    iget-object v1, p0, Lcom/google/android/exoplayer/ab;->a:Lcom/google/android/exoplayer/a;

    iget-wide v2, v1, Lcom/google/android/exoplayer/a;->j:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Lcom/google/android/exoplayer/a;->j:J

    iget v1, p0, Lcom/google/android/exoplayer/ab;->l:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/exoplayer/ab;->l:I

    iget v1, p0, Lcom/google/android/exoplayer/ab;->l:I

    iget v2, p0, Lcom/google/android/exoplayer/ab;->f:I

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/exoplayer/ab;->v()V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v4, p0, Lcom/google/android/exoplayer/ab;->i:Z

    if-nez v4, :cond_2

    invoke-direct {p0, p3, p6}, Lcom/google/android/exoplayer/ab;->a(Landroid/media/MediaCodec;I)V

    iput-boolean v0, p0, Lcom/google/android/exoplayer/ab;->i:Z

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/exoplayer/ab;->p()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_4

    const-wide/16 v4, 0x7530

    cmp-long v4, v2, v4

    if-gez v4, :cond_4

    const-wide/16 v4, 0x2af8

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    const-wide/16 v4, 0x2710

    sub-long v1, v2, v4

    const-wide/16 v3, 0x3e8

    :try_start_0
    div-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    invoke-direct {p0, p3, p6}, Lcom/google/android/exoplayer/ab;->a(Landroid/media/MediaCodec;I)V

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method protected final a(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p1}, Lcom/google/android/exoplayer/e/k;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/exoplayer/w;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(ZLcom/google/android/exoplayer/ag;Lcom/google/android/exoplayer/ag;)Z
    .locals 2

    iget-object v0, p3, Lcom/google/android/exoplayer/ag;->a:Ljava/lang/String;

    const-string v1, "video/avc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/google/android/exoplayer/ag;->a:Ljava/lang/String;

    const-string v1, "video/avc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    iget v0, p2, Lcom/google/android/exoplayer/ag;->c:I

    iget v1, p3, Lcom/google/android/exoplayer/ag;->c:I

    if-ne v0, v1, :cond_2

    iget v0, p2, Lcom/google/android/exoplayer/ag;->d:I

    iget v1, p3, Lcom/google/android/exoplayer/ag;->d:I

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final g()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/exoplayer/w;->g()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer/ab;->l:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/exoplayer/ab;->k:J

    return-void
.end method

.method protected final h()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/exoplayer/w;->h()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/exoplayer/ab;->j:J

    invoke-direct {p0}, Lcom/google/android/exoplayer/ab;->v()V

    return-void
.end method

.method protected final j()Z
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v6, -0x1

    invoke-super {p0}, Lcom/google/android/exoplayer/w;->j()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/exoplayer/ab;->i:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer/ab;->n()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    iput-wide v6, p0, Lcom/google/android/exoplayer/ab;->j:J

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-wide v2, p0, Lcom/google/android/exoplayer/ab;->j:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iget-wide v4, p0, Lcom/google/android/exoplayer/ab;->j:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    iput-wide v6, p0, Lcom/google/android/exoplayer/ab;->j:J

    move v0, v1

    goto :goto_0
.end method

.method protected final m()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/exoplayer/w;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/ab;->g:Landroid/view/Surface;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
