.class public abstract Lcom/google/android/exoplayer/w;
.super Lcom/google/android/exoplayer/ak;
.source "SourceFile"


# instance fields
.field private A:J

.field public final a:Lcom/google/android/exoplayer/a;

.field protected final b:Landroid/os/Handler;

.field private final c:Lcom/google/android/exoplayer/c/a;

.field private final d:Lcom/google/android/exoplayer/ai;

.field private final e:Lcom/google/android/exoplayer/ah;

.field private final f:Lcom/google/android/exoplayer/l;

.field private final g:Ljava/util/HashSet;

.field private final h:Landroid/media/MediaCodec$BufferInfo;

.field private final i:Lcom/google/android/exoplayer/z;

.field private j:Lcom/google/android/exoplayer/ag;

.field private k:Ljava/util/Map;

.field private l:Landroid/media/MediaCodec;

.field private m:Z

.field private n:[Ljava/nio/ByteBuffer;

.field private o:[Ljava/nio/ByteBuffer;

.field private p:J

.field private q:I

.field private r:I

.field private s:Z

.field private t:Z

.field private u:I

.field private v:I

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/ai;Lcom/google/android/exoplayer/c/a;Landroid/os/Handler;Lcom/google/android/exoplayer/z;)V
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/exoplayer/ak;-><init>()V

    sget v0, Lcom/google/android/exoplayer/e/k;->a:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/e/a;->b(Z)V

    iput-object p1, p0, Lcom/google/android/exoplayer/w;->d:Lcom/google/android/exoplayer/ai;

    iput-object p2, p0, Lcom/google/android/exoplayer/w;->c:Lcom/google/android/exoplayer/c/a;

    iput-object p3, p0, Lcom/google/android/exoplayer/w;->b:Landroid/os/Handler;

    iput-object p4, p0, Lcom/google/android/exoplayer/w;->i:Lcom/google/android/exoplayer/z;

    new-instance v0, Lcom/google/android/exoplayer/a;

    invoke-direct {v0}, Lcom/google/android/exoplayer/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/w;->a:Lcom/google/android/exoplayer/a;

    new-instance v0, Lcom/google/android/exoplayer/ah;

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/ah;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    new-instance v0, Lcom/google/android/exoplayer/l;

    invoke-direct {v0}, Lcom/google/android/exoplayer/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/w;->f:Lcom/google/android/exoplayer/l;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/w;->g:Ljava/util/HashSet;

    new-instance v0, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v0}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/w;->h:Landroid/media/MediaCodec$BufferInfo;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/exoplayer/w;)Lcom/google/android/exoplayer/z;
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->i:Lcom/google/android/exoplayer/z;

    return-object v0
.end method

.method private a(Landroid/media/MediaCodec$CryptoException;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->i:Lcom/google/android/exoplayer/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/y;

    invoke-direct {v1, p0, p1}, Lcom/google/android/exoplayer/y;-><init>(Lcom/google/android/exoplayer/w;Landroid/media/MediaCodec$CryptoException;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/exoplayer/l;)V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->j:Lcom/google/android/exoplayer/ag;

    iget-object v1, p1, Lcom/google/android/exoplayer/l;->a:Lcom/google/android/exoplayer/ag;

    iput-object v1, p0, Lcom/google/android/exoplayer/w;->j:Lcom/google/android/exoplayer/ag;

    iget-object v1, p1, Lcom/google/android/exoplayer/l;->b:Ljava/util/Map;

    iput-object v1, p0, Lcom/google/android/exoplayer/w;->k:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    iget-boolean v1, p0, Lcom/google/android/exoplayer/w;->m:Z

    iget-object v2, p0, Lcom/google/android/exoplayer/w;->j:Lcom/google/android/exoplayer/ag;

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/exoplayer/w;->a(ZLcom/google/android/exoplayer/ag;Lcom/google/android/exoplayer/ag;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/google/android/exoplayer/w;->t:Z

    iput v3, p0, Lcom/google/android/exoplayer/w;->u:I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer/w;->o()V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/w;->l()V

    goto :goto_0
.end method

.method private v()Z
    .locals 12

    const/4 v11, -0x1

    const-wide/16 v9, 0x1

    const/4 v6, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-boolean v0, p0, Lcom/google/android/exoplayer/w;->w:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v7

    :cond_1
    iget v0, p0, Lcom/google/android/exoplayer/w;->q:I

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer/w;->q:I

    iget v0, p0, Lcom/google/android/exoplayer/w;->q:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    iget-object v1, p0, Lcom/google/android/exoplayer/w;->n:[Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/google/android/exoplayer/w;->q:I

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/google/android/exoplayer/ah;->c:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    iget-object v0, v0, Lcom/google/android/exoplayer/ah;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/exoplayer/w;->y:Z

    if-eqz v0, :cond_3

    const/4 v0, -0x3

    :goto_1
    const/4 v1, -0x2

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->a:Lcom/google/android/exoplayer/a;

    iget-wide v1, v0, Lcom/google/android/exoplayer/a;->f:J

    add-long/2addr v1, v9

    iput-wide v1, v0, Lcom/google/android/exoplayer/a;->f:J

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/google/android/exoplayer/w;->u:I

    if-ne v0, v8, :cond_5

    move v1, v7

    :goto_2
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->j:Lcom/google/android/exoplayer/ag;

    iget-object v0, v0, Lcom/google/android/exoplayer/ag;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->j:Lcom/google/android/exoplayer/ag;

    iget-object v0, v0, Lcom/google/android/exoplayer/ag;->g:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iget-object v2, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    iget-object v2, v2, Lcom/google/android/exoplayer/ah;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_4
    iput v6, p0, Lcom/google/android/exoplayer/w;->u:I

    :cond_5
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->d:Lcom/google/android/exoplayer/ai;

    iget v1, p0, Lcom/google/android/exoplayer/w;->v:I

    iget-wide v2, p0, Lcom/google/android/exoplayer/w;->A:J

    iget-object v4, p0, Lcom/google/android/exoplayer/w;->f:Lcom/google/android/exoplayer/l;

    iget-object v5, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/exoplayer/ai;->a(IJLcom/google/android/exoplayer/l;Lcom/google/android/exoplayer/ah;)I

    move-result v0

    goto :goto_1

    :cond_6
    const/4 v1, -0x4

    if-ne v0, v1, :cond_8

    iget v0, p0, Lcom/google/android/exoplayer/w;->u:I

    if-ne v0, v6, :cond_7

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    iget-object v0, v0, Lcom/google/android/exoplayer/ah;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iput v8, p0, Lcom/google/android/exoplayer/w;->u:I

    :cond_7
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->f:Lcom/google/android/exoplayer/l;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/w;->a(Lcom/google/android/exoplayer/l;)V

    move v7, v8

    goto :goto_0

    :cond_8
    if-ne v0, v11, :cond_a

    iget v0, p0, Lcom/google/android/exoplayer/w;->u:I

    if-ne v0, v6, :cond_9

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    iget-object v0, v0, Lcom/google/android/exoplayer/ah;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iput v8, p0, Lcom/google/android/exoplayer/w;->u:I

    :cond_9
    iput-boolean v8, p0, Lcom/google/android/exoplayer/w;->w:Z

    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    iget v1, p0, Lcom/google/android/exoplayer/w;->q:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x4

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer/w;->q:I

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->a:Lcom/google/android/exoplayer/a;

    iget-wide v1, v0, Lcom/google/android/exoplayer/a;->h:J

    add-long/2addr v1, v9

    iput-wide v1, v0, Lcom/google/android/exoplayer/a;->h:J
    :try_end_0
    .catch Landroid/media/MediaCodec$CryptoException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/w;->a(Landroid/media/MediaCodec$CryptoException;)V

    new-instance v1, Lcom/google/android/exoplayer/ExoPlaybackException;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer/ExoPlaybackException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_a
    iget-boolean v0, p0, Lcom/google/android/exoplayer/w;->z:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    iget v0, v0, Lcom/google/android/exoplayer/ah;->e:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    iget-object v0, v0, Lcom/google/android/exoplayer/ah;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget v0, p0, Lcom/google/android/exoplayer/w;->u:I

    if-ne v0, v6, :cond_b

    iput v8, p0, Lcom/google/android/exoplayer/w;->u:I

    :cond_b
    move v7, v8

    goto/16 :goto_0

    :cond_c
    iput-boolean v7, p0, Lcom/google/android/exoplayer/w;->z:Z

    :cond_d
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    iget v0, v0, Lcom/google/android/exoplayer/ah;->e:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_e

    move v0, v8

    :goto_3
    iget-boolean v1, p0, Lcom/google/android/exoplayer/w;->s:Z

    if-eqz v1, :cond_13

    iget-object v1, p0, Lcom/google/android/exoplayer/w;->c:Lcom/google/android/exoplayer/c/a;

    invoke-interface {v1}, Lcom/google/android/exoplayer/c/a;->b()I

    move-result v1

    if-nez v1, :cond_f

    new-instance v0, Lcom/google/android/exoplayer/ExoPlaybackException;

    iget-object v1, p0, Lcom/google/android/exoplayer/w;->c:Lcom/google/android/exoplayer/c/a;

    invoke-interface {v1}, Lcom/google/android/exoplayer/c/a;->e()Ljava/lang/Exception;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/ExoPlaybackException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_e
    move v0, v7

    goto :goto_3

    :cond_f
    const/4 v2, 0x4

    if-eq v1, v2, :cond_13

    if-nez v0, :cond_10

    iget-object v1, p0, Lcom/google/android/exoplayer/w;->c:Lcom/google/android/exoplayer/c/a;

    invoke-interface {v1}, Lcom/google/android/exoplayer/c/a;->a()Z

    move-result v1

    if-nez v1, :cond_13

    :cond_10
    move v1, v8

    :goto_4
    iput-boolean v1, p0, Lcom/google/android/exoplayer/w;->y:Z

    iget-boolean v1, p0, Lcom/google/android/exoplayer/w;->y:Z

    if-nez v1, :cond_0

    :try_start_1
    iget-object v1, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    iget-object v1, v1, Lcom/google/android/exoplayer/ah;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    iget-object v1, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    iget v1, v1, Lcom/google/android/exoplayer/ah;->d:I

    sub-int v1, v3, v1

    iget-object v2, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    iget-wide v4, v2, Lcom/google/android/exoplayer/ah;->f:J

    iget-object v2, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    iget-boolean v2, v2, Lcom/google/android/exoplayer/ah;->g:Z

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/exoplayer/w;->g:Ljava/util/HashSet;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_11
    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    iget-object v0, v0, Lcom/google/android/exoplayer/ah;->b:Lcom/google/android/exoplayer/b;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/b;->a()Landroid/media/MediaCodec$CryptoInfo;

    move-result-object v3

    if-nez v1, :cond_14

    :goto_5
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    iget v1, p0, Lcom/google/android/exoplayer/w;->q:I

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueSecureInputBuffer(IILandroid/media/MediaCodec$CryptoInfo;JI)V

    :goto_6
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->a:Lcom/google/android/exoplayer/a;

    iget-wide v1, v0, Lcom/google/android/exoplayer/a;->e:J

    add-long/2addr v1, v9

    iput-wide v1, v0, Lcom/google/android/exoplayer/a;->e:J

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    iget v0, v0, Lcom/google/android/exoplayer/ah;->e:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->a:Lcom/google/android/exoplayer/a;

    iget-wide v1, v0, Lcom/google/android/exoplayer/a;->g:J

    add-long/2addr v1, v9

    iput-wide v1, v0, Lcom/google/android/exoplayer/a;->g:J

    :cond_12
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer/w;->q:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer/w;->u:I

    move v7, v8

    goto/16 :goto_0

    :cond_13
    move v1, v7

    goto :goto_4

    :cond_14
    iget-object v0, v3, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfClearData:[I

    if-nez v0, :cond_15

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, v3, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfClearData:[I

    :cond_15
    iget-object v0, v3, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfClearData:[I

    const/4 v2, 0x0

    aget v6, v0, v2

    add-int/2addr v1, v6

    aput v1, v0, v2
    :try_end_1
    .catch Landroid/media/MediaCodec$CryptoException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_5

    :catch_1
    move-exception v0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/w;->a(Landroid/media/MediaCodec$CryptoException;)V

    new-instance v1, Lcom/google/android/exoplayer/ExoPlaybackException;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer/ExoPlaybackException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_16
    :try_start_2
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    iget v1, p0, Lcom/google/android/exoplayer/w;->q:I

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V
    :try_end_2
    .catch Landroid/media/MediaCodec$CryptoException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_6
.end method


# virtual methods
.method protected final a()I
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->d:Lcom/google/android/exoplayer/ai;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ai;->a()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/exoplayer/w;->d:Lcom/google/android/exoplayer/ai;

    invoke-interface {v2}, Lcom/google/android/exoplayer/ai;->b()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/exoplayer/w;->d:Lcom/google/android/exoplayer/ai;

    invoke-interface {v2, v1}, Lcom/google/android/exoplayer/ai;->a(I)Lcom/google/android/exoplayer/aj;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/exoplayer/aj;->a:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer/w;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput v1, p0, Lcom/google/android/exoplayer/w;->v:I

    const/4 v0, 0x1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/exoplayer/ExoPlaybackException;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer/ExoPlaybackException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method protected a(J)V
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x0

    iput-wide p1, p0, Lcom/google/android/exoplayer/w;->A:J

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->d:Lcom/google/android/exoplayer/ai;

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer/ai;->b(J)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    if-eqz v0, :cond_1

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/exoplayer/w;->p:J

    iput v3, p0, Lcom/google/android/exoplayer/w;->q:I

    iput v3, p0, Lcom/google/android/exoplayer/w;->r:I

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->g:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    sget v0, Lcom/google/android/exoplayer/e/k;->a:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->flush()V

    :goto_1
    iget-boolean v0, p0, Lcom/google/android/exoplayer/w;->t:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->j:Lcom/google/android/exoplayer/ag;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/exoplayer/w;->u:I

    :cond_1
    iput-boolean v2, p0, Lcom/google/android/exoplayer/w;->w:Z

    iput-boolean v2, p0, Lcom/google/android/exoplayer/w;->x:Z

    iput-boolean v2, p0, Lcom/google/android/exoplayer/w;->y:Z

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/exoplayer/w;->o()V

    invoke-virtual {p0}, Lcom/google/android/exoplayer/w;->l()V

    goto :goto_1
.end method

.method protected a(JZ)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->d:Lcom/google/android/exoplayer/ai;

    iget v1, p0, Lcom/google/android/exoplayer/w;->v:I

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/exoplayer/ai;->a(IJ)V

    iput-boolean v2, p0, Lcom/google/android/exoplayer/w;->w:Z

    iput-boolean v2, p0, Lcom/google/android/exoplayer/w;->x:Z

    iput-boolean v2, p0, Lcom/google/android/exoplayer/w;->y:Z

    iput-wide p1, p0, Lcom/google/android/exoplayer/w;->A:J

    return-void
.end method

.method protected a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;Landroid/media/MediaCrypto;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    return-void
.end method

.method protected a(Landroid/media/MediaFormat;)V
    .locals 0

    return-void
.end method

.method protected abstract a(JLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;I)Z
.end method

.method protected a(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected a(ZLcom/google/android/exoplayer/ag;Lcom/google/android/exoplayer/ag;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected b()V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/exoplayer/w;->o()V

    iput-object v0, p0, Lcom/google/android/exoplayer/w;->j:Lcom/google/android/exoplayer/ag;

    iput-object v0, p0, Lcom/google/android/exoplayer/w;->k:Ljava/util/Map;

    iget-boolean v0, p0, Lcom/google/android/exoplayer/w;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->c:Lcom/google/android/exoplayer/c/a;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/exoplayer/w;->s:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->d:Lcom/google/android/exoplayer/ai;

    iget v1, p0, Lcom/google/android/exoplayer/w;->v:I

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/ai;->b(I)V

    return-void
.end method

.method protected b(J)V
    .locals 11

    const/4 v6, -0x4

    const-wide/16 v9, 0x1

    const/4 v8, -0x3

    const/4 v7, 0x1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->d:Lcom/google/android/exoplayer/ai;

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer/ai;->a(J)V

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->j:Lcom/google/android/exoplayer/ag;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->d:Lcom/google/android/exoplayer/ai;

    iget v1, p0, Lcom/google/android/exoplayer/w;->v:I

    iget-wide v2, p0, Lcom/google/android/exoplayer/w;->A:J

    iget-object v4, p0, Lcom/google/android/exoplayer/w;->f:Lcom/google/android/exoplayer/l;

    iget-object v5, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/exoplayer/ai;->a(IJLcom/google/android/exoplayer/l;Lcom/google/android/exoplayer/ah;)I

    move-result v0

    if-ne v0, v6, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->f:Lcom/google/android/exoplayer/l;

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/w;->a(Lcom/google/android/exoplayer/l;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/exoplayer/w;->m()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/exoplayer/w;->p()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/exoplayer/ah;->c:Ljava/nio/ByteBuffer;

    move v0, v8

    :cond_2
    :goto_1
    if-ne v0, v8, :cond_0

    iget-wide v0, p0, Lcom/google/android/exoplayer/w;->A:J

    cmp-long v0, v0, p1

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->d:Lcom/google/android/exoplayer/ai;

    iget v1, p0, Lcom/google/android/exoplayer/w;->v:I

    iget-wide v2, p0, Lcom/google/android/exoplayer/w;->A:J

    iget-object v4, p0, Lcom/google/android/exoplayer/w;->f:Lcom/google/android/exoplayer/l;

    iget-object v5, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/exoplayer/ai;->a(IJLcom/google/android/exoplayer/l;Lcom/google/android/exoplayer/ah;)I

    move-result v0

    if-ne v0, v8, :cond_3

    iget-object v1, p0, Lcom/google/android/exoplayer/w;->e:Lcom/google/android/exoplayer/ah;

    iget-wide v1, v1, Lcom/google/android/exoplayer/ah;->f:J

    iput-wide v1, p0, Lcom/google/android/exoplayer/w;->A:J

    iget-object v1, p0, Lcom/google/android/exoplayer/w;->a:Lcom/google/android/exoplayer/a;

    iget-wide v2, v1, Lcom/google/android/exoplayer/a;->k:J

    add-long/2addr v2, v9

    iput-wide v2, v1, Lcom/google/android/exoplayer/a;->k:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/exoplayer/ExoPlaybackException;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer/ExoPlaybackException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_3
    if-ne v0, v6, :cond_2

    :try_start_1
    iget-object v1, p0, Lcom/google/android/exoplayer/w;->f:Lcom/google/android/exoplayer/l;

    invoke-direct {p0, v1}, Lcom/google/android/exoplayer/w;->a(Lcom/google/android/exoplayer/l;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/exoplayer/w;->m()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/exoplayer/w;->l()V

    :cond_5
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/exoplayer/w;->x:Z

    if-nez v0, :cond_b

    iget v0, p0, Lcom/google/android/exoplayer/w;->r:I

    if-gez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    iget-object v1, p0, Lcom/google/android/exoplayer/w;->h:Landroid/media/MediaCodec$BufferInfo;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v0

    iput v0, p0, Lcom/google/android/exoplayer/w;->r:I

    :cond_7
    iget v0, p0, Lcom/google/android/exoplayer/w;->r:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_9

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/w;->a(Landroid/media/MediaFormat;)V

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->a:Lcom/google/android/exoplayer/a;

    iget-wide v1, v0, Lcom/google/android/exoplayer/a;->c:J

    add-long/2addr v1, v9

    iput-wide v1, v0, Lcom/google/android/exoplayer/a;->c:J

    move v0, v7

    :goto_2
    if-nez v0, :cond_6

    :cond_8
    invoke-direct {p0}, Lcom/google/android/exoplayer/w;->v()Z

    move-result v0

    if-nez v0, :cond_8

    goto/16 :goto_0

    :cond_9
    iget v0, p0, Lcom/google/android/exoplayer/w;->r:I

    if-ne v0, v8, :cond_a

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/w;->o:[Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->a:Lcom/google/android/exoplayer/a;

    iget-wide v1, v0, Lcom/google/android/exoplayer/a;->d:J

    add-long/2addr v1, v9

    iput-wide v1, v0, Lcom/google/android/exoplayer/a;->d:J

    move v0, v7

    goto :goto_2

    :cond_a
    iget v0, p0, Lcom/google/android/exoplayer/w;->r:I

    if-ltz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->h:Landroid/media/MediaCodec$BufferInfo;

    iget v0, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/exoplayer/w;->x:Z

    :cond_b
    const/4 v0, 0x0

    goto :goto_2

    :cond_c
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->g:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/exoplayer/w;->h:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v1, v1, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    iget v1, p0, Lcom/google/android/exoplayer/w;->r:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer/w;->r:I

    move v0, v7

    goto :goto_2

    :cond_d
    iget-object v3, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->o:[Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/google/android/exoplayer/w;->r:I

    aget-object v4, v0, v1

    iget-object v5, p0, Lcom/google/android/exoplayer/w;->h:Landroid/media/MediaCodec$BufferInfo;

    iget v6, p0, Lcom/google/android/exoplayer/w;->r:I

    move-object v0, p0

    move-wide v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/exoplayer/w;->a(JLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;I)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->h:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v0, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iput-wide v0, p0, Lcom/google/android/exoplayer/w;->A:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/exoplayer/w;->r:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move v0, v7

    goto :goto_2
.end method

.method protected final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->d:Lcom/google/android/exoplayer/ai;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ai;->d()V

    return-void
.end method

.method protected d()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/exoplayer/w;->A:J

    return-wide v0
.end method

.method protected final e()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->d:Lcom/google/android/exoplayer/ai;

    iget v1, p0, Lcom/google/android/exoplayer/w;->v:I

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/ai;->a(I)Lcom/google/android/exoplayer/aj;

    move-result-object v0

    iget-wide v0, v0, Lcom/google/android/exoplayer/aj;->b:J

    return-wide v0
.end method

.method protected final f()J
    .locals 4

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->d:Lcom/google/android/exoplayer/ai;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ai;->c()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    const-wide/16 v2, -0x3

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/exoplayer/w;->d()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method protected g()V
    .locals 0

    return-void
.end method

.method protected h()V
    .locals 0

    return-void
.end method

.method protected i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/exoplayer/w;->x:Z

    return v0
.end method

.method protected j()Z
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/exoplayer/w;->j:Lcom/google/android/exoplayer/ag;

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/exoplayer/w;->y:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer/w;->m()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    iget v2, p0, Lcom/google/android/exoplayer/w;->r:I

    if-gez v2, :cond_1

    iget v2, p0, Lcom/google/android/exoplayer/w;->q:I

    if-ltz v2, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/exoplayer/w;->p:J

    const-wide/16 v6, 0x3e8

    add-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    :cond_1
    :goto_1
    return v0

    :cond_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method protected final l()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, -0x1

    invoke-virtual {p0}, Lcom/google/android/exoplayer/w;->m()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->j:Lcom/google/android/exoplayer/ag;

    iget-object v2, v0, Lcom/google/android/exoplayer/ag;->a:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/exoplayer/w;->k:Ljava/util/Map;

    if-eqz v3, :cond_6

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->c:Lcom/google/android/exoplayer/c/a;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/exoplayer/ExoPlaybackException;

    const-string v1, "Media requires a DrmSessionManager"

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/ExoPlaybackException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/exoplayer/w;->s:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->c:Lcom/google/android/exoplayer/c/a;

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->k:Ljava/util/Map;

    iput-boolean v5, p0, Lcom/google/android/exoplayer/w;->s:Z

    :cond_3
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->c:Lcom/google/android/exoplayer/c/a;

    invoke-interface {v0}, Lcom/google/android/exoplayer/c/a;->b()I

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/exoplayer/ExoPlaybackException;

    iget-object v1, p0, Lcom/google/android/exoplayer/w;->c:Lcom/google/android/exoplayer/c/a;

    invoke-interface {v1}, Lcom/google/android/exoplayer/c/a;->e()Ljava/lang/Exception;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/ExoPlaybackException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_4
    if-eq v0, v6, :cond_5

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/exoplayer/w;->c:Lcom/google/android/exoplayer/c/a;

    invoke-interface {v0}, Lcom/google/android/exoplayer/c/a;->c()Landroid/media/MediaCrypto;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->c:Lcom/google/android/exoplayer/c/a;

    invoke-interface {v0}, Lcom/google/android/exoplayer/c/a;->d()Z

    move-result v0

    :cond_6
    invoke-static {v2}, Lcom/google/android/exoplayer/aa;->a(Ljava/lang/String;)Lcom/google/android/exoplayer/c;

    move-result-object v3

    iget-object v2, v3, Lcom/google/android/exoplayer/c;->a:Ljava/lang/String;

    if-eqz v0, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".secure"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-boolean v2, v3, Lcom/google/android/exoplayer/c;->b:Z

    iput-boolean v2, p0, Lcom/google/android/exoplayer/w;->m:Z

    :try_start_0
    invoke-static {v0}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    iget-object v2, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    iget-object v3, p0, Lcom/google/android/exoplayer/w;->j:Lcom/google/android/exoplayer/ag;

    invoke-virtual {v3}, Lcom/google/android/exoplayer/ag;->a()Landroid/media/MediaFormat;

    move-result-object v3

    invoke-virtual {p0, v2, v3, v1}, Lcom/google/android/exoplayer/w;->a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;Landroid/media/MediaCrypto;)V

    iget-object v1, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->start()V

    iget-object v1, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer/w;->n:[Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/exoplayer/w;->o:[Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer/w;->p()I

    move-result v0

    if-ne v0, v6, :cond_8

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    :goto_2
    iput-wide v0, p0, Lcom/google/android/exoplayer/w;->p:J

    iput v4, p0, Lcom/google/android/exoplayer/w;->q:I

    iput v4, p0, Lcom/google/android/exoplayer/w;->r:I

    iput-boolean v5, p0, Lcom/google/android/exoplayer/w;->z:Z

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->a:Lcom/google/android/exoplayer/a;

    iget-wide v1, v0, Lcom/google/android/exoplayer/a;->a:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Lcom/google/android/exoplayer/a;->a:J

    goto/16 :goto_0

    :catch_0
    move-exception v1

    new-instance v2, Lcom/google/android/exoplayer/DecoderInitializationException;

    invoke-direct {v2, v0, v1}, Lcom/google/android/exoplayer/DecoderInitializationException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->b:Landroid/os/Handler;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->i:Lcom/google/android/exoplayer/z;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/exoplayer/x;

    invoke-direct {v1, p0, v2}, Lcom/google/android/exoplayer/x;-><init>(Lcom/google/android/exoplayer/w;Lcom/google/android/exoplayer/DecoderInitializationException;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_7
    new-instance v0, Lcom/google/android/exoplayer/ExoPlaybackException;

    invoke-direct {v0, v2}, Lcom/google/android/exoplayer/ExoPlaybackException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_8
    const-wide/16 v0, -0x1

    goto :goto_2

    :cond_9
    move-object v0, v2

    goto :goto_1
.end method

.method protected m()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->j:Lcom/google/android/exoplayer/ag;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final n()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final o()V
    .locals 5

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/exoplayer/w;->p:J

    iput v4, p0, Lcom/google/android/exoplayer/w;->q:I

    iput v4, p0, Lcom/google/android/exoplayer/w;->r:I

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->g:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iput-object v3, p0, Lcom/google/android/exoplayer/w;->n:[Ljava/nio/ByteBuffer;

    iput-object v3, p0, Lcom/google/android/exoplayer/w;->o:[Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V

    iput-object v3, p0, Lcom/google/android/exoplayer/w;->l:Landroid/media/MediaCodec;

    iput-boolean v2, p0, Lcom/google/android/exoplayer/w;->t:Z

    iput-boolean v2, p0, Lcom/google/android/exoplayer/w;->m:Z

    iput v2, p0, Lcom/google/android/exoplayer/w;->u:I

    iget-object v0, p0, Lcom/google/android/exoplayer/w;->a:Lcom/google/android/exoplayer/a;

    iget-wide v1, v0, Lcom/google/android/exoplayer/a;->b:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Lcom/google/android/exoplayer/a;->b:J

    :cond_0
    return-void
.end method
