.class public final Lcom/google/android/apps/common/csi/lib/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:Ljava/util/Map;

.field private k:Lcom/google/android/apps/common/csi/lib/Sender;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "Android %s"

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/csi/lib/a;->a:Ljava/lang/String;

    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/common/csi/lib/a;->b:Ljava/lang/String;

    iput v5, p0, Lcom/google/android/apps/common/csi/lib/a;->c:I

    const-string v0, "_s"

    iput-object v0, p0, Lcom/google/android/apps/common/csi/lib/a;->d:Ljava/lang/String;

    const-string v0, "http://csi.gstatic.com/csi"

    iput-object v0, p0, Lcom/google/android/apps/common/csi/lib/a;->e:Ljava/lang/String;

    const/16 v0, 0x10

    iput v0, p0, Lcom/google/android/apps/common/csi/lib/a;->f:I

    iput v5, p0, Lcom/google/android/apps/common/csi/lib/a;->g:I

    iput v4, p0, Lcom/google/android/apps/common/csi/lib/a;->h:I

    iput v4, p0, Lcom/google/android/apps/common/csi/lib/a;->i:I

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/common/csi/lib/a;->j:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/common/csi/lib/a;->c:I

    return v0
.end method

.method public final a(I)Lcom/google/android/apps/common/csi/lib/a;
    .locals 2

    const/4 v0, 0x1

    const-string v1, "batchSize"

    iput v0, p0, Lcom/google/android/apps/common/csi/lib/a;->i:I

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/common/csi/lib/Sender;)Lcom/google/android/apps/common/csi/lib/a;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/common/csi/lib/a;->k:Lcom/google/android/apps/common/csi/lib/Sender;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/common/csi/lib/a;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/common/csi/lib/a;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/common/csi/lib/a;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/common/csi/lib/a;->l:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/common/csi/lib/a;->m:Ljava/lang/String;

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/common/csi/lib/a;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/common/csi/lib/a;->e:Ljava/lang/String;

    return-object p0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/csi/lib/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/csi/lib/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/common/csi/lib/a;->f:I

    return v0
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/common/csi/lib/a;->g:I

    return v0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/common/csi/lib/a;->h:I

    return v0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/common/csi/lib/a;->i:I

    return v0
.end method

.method public final h()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/csi/lib/a;->j:Ljava/util/Map;

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/common/csi/lib/Sender;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/common/csi/lib/a;->k:Lcom/google/android/apps/common/csi/lib/Sender;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/common/csi/lib/g;

    iget-object v1, p0, Lcom/google/android/apps/common/csi/lib/a;->l:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/common/csi/lib/a;->m:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/common/csi/lib/g;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/common/csi/lib/a;->k:Lcom/google/android/apps/common/csi/lib/Sender;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/common/csi/lib/a;->k:Lcom/google/android/apps/common/csi/lib/Sender;

    return-object v0
.end method
