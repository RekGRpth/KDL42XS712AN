.class public final Lcom/google/android/apps/common/csi/lib/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/apps/common/csi/lib/d;


# direct methods
.method public static declared-synchronized a()Lcom/google/android/apps/common/csi/lib/c;
    .locals 2

    const-class v1, Lcom/google/android/apps/common/csi/lib/f;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/common/csi/lib/f;->a:Lcom/google/android/apps/common/csi/lib/d;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/common/csi/lib/a;

    invoke-direct {v0}, Lcom/google/android/apps/common/csi/lib/a;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/common/csi/lib/f;->b(Lcom/google/android/apps/common/csi/lib/a;)V

    :cond_0
    sget-object v0, Lcom/google/android/apps/common/csi/lib/f;->a:Lcom/google/android/apps/common/csi/lib/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Lcom/google/android/apps/common/csi/lib/a;)V
    .locals 2

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "configuration can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/common/csi/lib/f;->b(Lcom/google/android/apps/common/csi/lib/a;)V

    return-void
.end method

.method private static declared-synchronized b(Lcom/google/android/apps/common/csi/lib/a;)V
    .locals 9

    const-class v8, Lcom/google/android/apps/common/csi/lib/f;

    monitor-enter v8

    :try_start_0
    new-instance v0, Lcom/google/android/apps/common/csi/lib/d;

    invoke-virtual {p0}, Lcom/google/android/apps/common/csi/lib/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/common/csi/lib/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/common/csi/lib/a;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/common/csi/lib/a;->i()Lcom/google/android/apps/common/csi/lib/Sender;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/common/csi/lib/a;->d()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/common/csi/lib/a;->e()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/common/csi/lib/a;->f()I

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/common/csi/lib/d;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/common/csi/lib/Sender;III)V

    sput-object v0, Lcom/google/android/apps/common/csi/lib/f;->a:Lcom/google/android/apps/common/csi/lib/d;

    invoke-virtual {p0}, Lcom/google/android/apps/common/csi/lib/a;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/csi/lib/d;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/common/csi/lib/a;->h()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    sget-object v3, Lcom/google/android/apps/common/csi/lib/f;->a:Lcom/google/android/apps/common/csi/lib/d;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, Lcom/google/android/apps/common/csi/lib/d;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0

    :cond_0
    monitor-exit v8

    return-void
.end method
