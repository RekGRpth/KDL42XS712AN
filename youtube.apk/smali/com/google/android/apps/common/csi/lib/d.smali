.class public final Lcom/google/android/apps/common/csi/lib/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/common/csi/lib/c;


# instance fields
.field private a:Lcom/google/android/apps/common/csi/lib/Sender;

.field private b:Ljava/util/concurrent/ExecutorService;

.field private c:Ljava/util/LinkedHashMap;

.field private d:Ljava/util/concurrent/BlockingQueue;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/common/csi/lib/Sender;III)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/common/csi/lib/d;->c:Ljava/util/LinkedHashMap;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/common/csi/lib/d;->i:I

    iput-object p1, p0, Lcom/google/android/apps/common/csi/lib/d;->e:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/common/csi/lib/d;->f:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/common/csi/lib/d;->g:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/common/csi/lib/d;->a:Lcom/google/android/apps/common/csi/lib/Sender;

    iput p5, p0, Lcom/google/android/apps/common/csi/lib/d;->h:I

    iput p6, p0, Lcom/google/android/apps/common/csi/lib/d;->j:I

    iput p7, p0, Lcom/google/android/apps/common/csi/lib/d;->k:I

    const-string v0, "v"

    iget-object v1, p0, Lcom/google/android/apps/common/csi/lib/d;->g:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/common/csi/lib/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "s"

    iget-object v1, p0, Lcom/google/android/apps/common/csi/lib/d;->f:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/common/csi/lib/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    iget v1, p0, Lcom/google/android/apps/common/csi/lib/d;->h:I

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/common/csi/lib/d;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/common/csi/lib/d;->b:Ljava/util/concurrent/ExecutorService;

    iget-object v0, p0, Lcom/google/android/apps/common/csi/lib/d;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/apps/common/csi/lib/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/common/csi/lib/e;-><init>(Lcom/google/android/apps/common/csi/lib/d;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Ljava/util/List;)Ljava/util/Map;
    .locals 7

    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-direct {v3}, Ljava/util/LinkedHashMap;-><init>()V

    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/common/csi/lib/i;

    invoke-virtual {v0}, Lcom/google/android/apps/common/csi/lib/i;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v2, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    new-instance v5, Ljava/util/LinkedHashMap;

    iget-object v2, p0, Lcom/google/android/apps/common/csi/lib/d;->c:Ljava/util/LinkedHashMap;

    invoke-direct {v5, v2}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    const/4 v2, 0x0

    :try_start_0
    new-array v2, v2, [Lcom/google/android/apps/common/csi/lib/i;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/android/apps/common/csi/lib/i;

    invoke-static {v2}, Lcom/google/android/apps/common/csi/lib/i;->a([Lcom/google/android/apps/common/csi/lib/i;)Ljava/util/Map;
    :try_end_0
    .catch Lcom/google/android/apps/common/csi/lib/AndroidCsiException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v2, "ReporterDefault"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "failed to merge tickers:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_2
    return-object v3
.end method

.method private a(Ljava/util/Map;)Z
    .locals 6

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/apps/common/csi/lib/d;->j:I

    move v3, v0

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    if-lez v3, :cond_0

    :try_start_0
    iget v2, p0, Lcom/google/android/apps/common/csi/lib/d;->k:I

    int-to-long v4, v2

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    iget-object v2, p0, Lcom/google/android/apps/common/csi/lib/d;->a:Lcom/google/android/apps/common/csi/lib/Sender;

    iget-object v4, p0, Lcom/google/android/apps/common/csi/lib/d;->e:Ljava/lang/String;

    invoke-interface {v2, v4, p1}, Lcom/google/android/apps/common/csi/lib/Sender;->a(Ljava/lang/String;Ljava/util/Map;)V
    :try_end_0
    .catch Lcom/google/android/apps/common/csi/lib/Sender$SenderException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v0, 0x1

    :goto_1
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "ReporterDefault"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "#"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " failed to send report"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    goto :goto_1

    :catch_1
    move-exception v2

    const-string v4, "ReporterDefault"

    const-string v5, "interrupted in sendReport()"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    :cond_0
    return v0
.end method


# virtual methods
.method final a()V
    .locals 5

    :cond_0
    const/4 v1, 0x0

    :try_start_0
    iget v3, p0, Lcom/google/android/apps/common/csi/lib/d;->i:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/common/csi/lib/d;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v4}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "ReporterDefault"

    const-string v3, "reporter interrupted"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    move-object v0, v1

    :cond_1
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/common/csi/lib/d;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-direct {p0, v0}, Lcom/google/android/apps/common/csi/lib/d;->a(Ljava/util/Map;)Z

    goto :goto_1
.end method

.method public final a(I)V
    .locals 3

    if-gtz p1, :cond_0

    const-string v0, "ReporterDefault"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "too small batch size :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", changed to 1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x1

    :cond_0
    iget v0, p0, Lcom/google/android/apps/common/csi/lib/d;->h:I

    if-le p1, v0, :cond_1

    const-string v0, "ReporterDefault"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "batch size :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bigger than buffer size, change to buffer limit"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iput p1, p0, Lcom/google/android/apps/common/csi/lib/d;->i:I

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/csi/lib/d;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final a(Lcom/google/android/apps/common/csi/lib/i;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/common/csi/lib/d;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
