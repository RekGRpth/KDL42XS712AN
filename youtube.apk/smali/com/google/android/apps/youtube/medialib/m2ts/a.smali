.class final Lcom/google/android/apps/youtube/medialib/m2ts/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/util/SparseIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0xd

    new-array v1, v0, [I

    fill-array-data v1, :array_0

    new-instance v0, Landroid/util/SparseIntArray;

    array-length v2, v1

    invoke-direct {v0, v2}, Landroid/util/SparseIntArray;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/youtube/medialib/m2ts/a;->a:Landroid/util/SparseIntArray;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/youtube/medialib/m2ts/a;->a:Landroid/util/SparseIntArray;

    aget v3, v1, v0

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseIntArray;->append(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void

    :array_0
    .array-data 4
        0x17700
        0x15888
        0xfa00
        0xbb80
        0xac44
        0x7d00
        0x5dc0
        0x5622
        0x3e80
        0x2ee0
        0x2b11
        0x1f40
        0x1cb6
    .end array-data
.end method

.method public static a(Lcom/google/android/exoplayer/ah;Lcom/google/android/exoplayer/ag;)[B
    .locals 6

    const/4 v1, 0x1

    const/4 v5, -0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/medialib/m2ts/a;->a:Landroid/util/SparseIntArray;

    iget v3, p1, Lcom/google/android/exoplayer/ag;->f:I

    invoke-virtual {v0, v3, v5}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    if-ltz v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iget v0, p0, Lcom/google/android/exoplayer/ah;->d:I

    add-int/lit8 v0, v0, 0x7

    new-array v4, v0, [B

    aput-byte v5, v4, v2

    const/16 v5, -0xf

    aput-byte v5, v4, v1

    const/4 v1, 0x2

    shl-int/lit8 v3, v3, 0x2

    or-int/lit8 v3, v3, 0x40

    iget v5, p1, Lcom/google/android/exoplayer/ag;->e:I

    shr-int/lit8 v5, v5, 0x2

    or-int/2addr v3, v5

    int-to-byte v3, v3

    aput-byte v3, v4, v1

    const/4 v1, 0x3

    iget v3, p1, Lcom/google/android/exoplayer/ag;->e:I

    and-int/lit8 v3, v3, 0x3

    shl-int/lit8 v3, v3, 0x6

    shr-int/lit8 v5, v0, 0xb

    or-int/2addr v3, v5

    int-to-byte v3, v3

    aput-byte v3, v4, v1

    const/4 v1, 0x4

    shr-int/lit8 v3, v0, 0x3

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v4, v1

    const/4 v1, 0x5

    and-int/lit8 v0, v0, 0x7

    shl-int/lit8 v0, v0, 0x5

    or-int/lit8 v0, v0, 0x1f

    int-to-byte v0, v0

    aput-byte v0, v4, v1

    const/4 v0, 0x6

    const/4 v1, -0x4

    aput-byte v1, v4, v0

    iget-object v0, p0, Lcom/google/android/exoplayer/ah;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    const/4 v1, 0x7

    iget v3, p0, Lcom/google/android/exoplayer/ah;->d:I

    invoke-static {v0, v2, v4, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v4

    :cond_0
    move v0, v2

    goto :goto_0
.end method
