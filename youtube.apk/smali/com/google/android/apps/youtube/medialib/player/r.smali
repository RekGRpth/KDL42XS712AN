.class final Lcom/google/android/apps/youtube/medialib/player/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/medialib/player/o;
.implements Lcom/google/android/apps/youtube/medialib/player/z;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/medialib/player/p;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/medialib/player/p;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/medialib/player/p;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/medialib/player/r;-><init>(Lcom/google/android/apps/youtube/medialib/player/p;)V

    return-void
.end method

.method private c(Lcom/google/android/apps/youtube/medialib/player/n;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->i(Lcom/google/android/apps/youtube/medialib/player/p;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->e(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/medialib/player/y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v1}, Lcom/google/android/apps/youtube/medialib/player/p;->j(Lcom/google/android/apps/youtube/medialib/player/p;)Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/y;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/medialib/player/n;->b(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->j(Lcom/google/android/apps/youtube/medialib/player/p;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->k(Lcom/google/android/apps/youtube/medialib/player/p;)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->k(Lcom/google/android/apps/youtube/medialib/player/p;)I

    move-result v0

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/medialib/player/n;->a(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->l(Lcom/google/android/apps/youtube/medialib/player/p;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->m(Lcom/google/android/apps/youtube/medialib/player/p;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->n(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/medialib/player/p;->d(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->j(Lcom/google/android/apps/youtube/medialib/player/p;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->h(Lcom/google/android/apps/youtube/medialib/player/p;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/medialib/player/p;->e(Lcom/google/android/apps/youtube/medialib/player/p;Z)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->o(Lcom/google/android/apps/youtube/medialib/player/p;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->b()V

    :cond_5
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/common/L;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/p;->a(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->d(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v1}, Lcom/google/android/apps/youtube/medialib/player/p;->d(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/p;->a(Lcom/google/android/apps/youtube/medialib/player/p;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/p;->b(Lcom/google/android/apps/youtube/medialib/player/p;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/medialib/player/n;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->n(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v1}, Lcom/google/android/apps/youtube/medialib/player/p;->k(Lcom/google/android/apps/youtube/medialib/player/p;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->c(I)V

    return-void
.end method

.method public final a(II)Z
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->q(Lcom/google/android/apps/youtube/medialib/player/p;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/p;->d(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/medialib/player/p;->g(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "MediaPlayer error during prepare [what="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", extra="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :goto_0
    if-ne p1, v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/medialib/player/p;->m()Ljava/util/Set;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    if-ne p1, v1, :cond_5

    const/16 v0, -0x3ec

    if-eq p2, v0, :cond_1

    const/high16 v0, -0x80000000

    if-ne p2, v0, :cond_5

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->t(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    :cond_2
    move v0, v1

    :goto_2
    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->r(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/medialib/player/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/v;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->r(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/medialib/player/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/v;->a()V

    const/16 v0, 0x64

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->e(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/medialib/player/y;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->f()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->n(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->b(II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->j(Lcom/google/android/apps/youtube/medialib/player/p;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v2}, Lcom/google/android/apps/youtube/medialib/player/p;->s(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/medialib/player/p;->c(Lcom/google/android/apps/youtube/medialib/player/p;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)V

    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->r(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/medialib/player/v;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v2}, Lcom/google/android/apps/youtube/medialib/player/p;->k(Lcom/google/android/apps/youtube/medialib/player/p;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/medialib/player/v;->a(I)V

    :goto_4
    return v1

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/medialib/player/p;->d(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/p;->g(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "MediaPlayer error during playback [what="

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", extra="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "]"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto/16 :goto_1

    :cond_6
    move v0, v2

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v2}, Lcom/google/android/apps/youtube/medialib/player/p;->s(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v3}, Lcom/google/android/apps/youtube/medialib/player/p;->k(Lcom/google/android/apps/youtube/medialib/player/p;)I

    move-result v3

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/youtube/medialib/player/p;->a(Lcom/google/android/apps/youtube/medialib/player/p;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;I)V

    goto :goto_3

    :cond_8
    const-string v0, "Reporting MediaPlayer error"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->r(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/medialib/player/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/v;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/medialib/player/p;->d(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/medialib/player/p;->g(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->d()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->n(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(II)V

    goto :goto_4
.end method

.method public final a(Lcom/google/android/apps/youtube/medialib/player/n;II)Z
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "media player info "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    sparse-switch p2, :sswitch_data_0

    :goto_0
    return v2

    :sswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Buffering data from "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v1}, Lcom/google/android/apps/youtube/medialib/player/p;->s(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/p;->e(Lcom/google/android/apps/youtube/medialib/player/p;Z)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/medialib/player/p;->e(Lcom/google/android/apps/youtube/medialib/player/p;Z)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->n(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x2bd -> :sswitch_0
        0x2be -> :sswitch_1
        0x385 -> :sswitch_2
    .end sparse-switch
.end method

.method public final b()V
    .locals 0

    invoke-static {}, Lcom/google/android/apps/youtube/common/L;->a()V

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/medialib/player/n;)V
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/common/L;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/p;->b(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-interface {p1}, Lcom/google/android/apps/youtube/medialib/player/n;->f()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/p;->b(Lcom/google/android/apps/youtube/medialib/player/p;I)I

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/medialib/player/r;->c(Lcom/google/android/apps/youtube/medialib/player/n;)V

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/medialib/player/n;II)V
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/common/L;->a()V

    if-lez p2, :cond_0

    if-gtz p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->f(Lcom/google/android/apps/youtube/medialib/player/p;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->e(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/medialib/player/y;

    move-result-object v0

    invoke-interface {v0, p2, p3}, Lcom/google/android/apps/youtube/medialib/player/y;->setVideoSize(II)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->h(Lcom/google/android/apps/youtube/medialib/player/p;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/p;->c(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/medialib/player/r;->c(Lcom/google/android/apps/youtube/medialib/player/n;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->g(Lcom/google/android/apps/youtube/medialib/player/p;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/medialib/player/s;

    invoke-direct {v1, p0, p2, p3}, Lcom/google/android/apps/youtube/medialib/player/s;-><init>(Lcom/google/android/apps/youtube/medialib/player/r;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method public final c()V
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/common/L;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->e(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/medialib/player/y;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/p;->a(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z

    return-void
.end method

.method public final d(I)V
    .locals 2

    const/16 v0, 0x64

    const/16 v1, 0x5a

    if-le p1, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v1}, Lcom/google/android/apps/youtube/medialib/player/p;->p(Lcom/google/android/apps/youtube/medialib/player/p;)I

    move-result v1

    if-eq v1, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v1}, Lcom/google/android/apps/youtube/medialib/player/p;->p(Lcom/google/android/apps/youtube/medialib/player/p;)I

    move-result v1

    if-ne v1, v0, :cond_1

    :cond_0
    move p1, v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/p;->c(Lcom/google/android/apps/youtube/medialib/player/p;I)I

    return-void
.end method

.method public final g()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/android/apps/youtube/common/L;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/p;->d(Lcom/google/android/apps/youtube/medialib/player/p;I)I

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/p;->f(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/r;->a:Lcom/google/android/apps/youtube/medialib/player/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/p;->n(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    return-void
.end method
