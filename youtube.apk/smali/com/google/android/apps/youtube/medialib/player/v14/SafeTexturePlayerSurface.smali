.class public final Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/medialib/player/y;


# instance fields
.field private a:Lcom/google/android/apps/youtube/medialib/player/y;

.field private b:Landroid/view/View;

.field private c:Z

.field private d:Z

.field private e:Lcom/google/android/apps/youtube/medialib/player/z;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method private a()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->i()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PlayerSurface method called before surface created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private i()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/medialib/player/n;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/y;->a(Lcom/google/android/apps/youtube/medialib/player/n;)V

    return-void
.end method

.method public final a(Lcom/google/android/exoplayer/d;Lcom/google/android/exoplayer/ak;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/medialib/player/y;->a(Lcom/google/android/exoplayer/d;Lcom/google/android/exoplayer/ak;)V

    return-void
.end method

.method public final a(Z)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/y;->a(Z)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->b()I

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/apps/youtube/medialib/player/n;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/y;->b(Lcom/google/android/apps/youtube/medialib/player/n;)V

    return-void
.end method

.method public final b(Lcom/google/android/exoplayer/d;Lcom/google/android/exoplayer/ak;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/medialib/player/y;->b(Lcom/google/android/exoplayer/d;Lcom/google/android/exoplayer/ak;)V

    return-void
.end method

.method public final c()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->c()I

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->d:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->d()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->d:Z

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->e()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->d:Z

    return-void
.end method

.method public final f()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->f()V

    :cond_0
    return-void
.end method

.method public final g()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->g()V

    :cond_0
    return-void
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->h()Z

    move-result v0

    goto :goto_0
.end method

.method protected final onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->b:Landroid/view/View;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->b:Landroid/view/View;

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->addView(Landroid/view/View;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->c:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->e:Lcom/google/android/apps/youtube/medialib/player/z;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/y;->setListener(Lcom/google/android/apps/youtube/medialib/player/z;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->d:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->d()V

    :cond_1
    return-void

    :cond_2
    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->b:Landroid/view/View;

    goto :goto_0
.end method

.method protected final onLayout(ZIIII)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0, v3}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    :cond_0
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v1, v1}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public final setListener(Lcom/google/android/apps/youtube/medialib/player/z;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->e:Lcom/google/android/apps/youtube/medialib/player/z;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->c:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/y;->setListener(Lcom/google/android/apps/youtube/medialib/player/z;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->c:Z

    goto :goto_0
.end method

.method public final setOnLetterboxChangedListener(Lcom/google/android/apps/youtube/medialib/player/aa;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/y;->setOnLetterboxChangedListener(Lcom/google/android/apps/youtube/medialib/player/aa;)V

    return-void
.end method

.method public final setVideoSize(II)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/medialib/player/y;->setVideoSize(II)V

    return-void
.end method

.method public final setZoom(I)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/y;->setZoom(I)V

    return-void
.end method
