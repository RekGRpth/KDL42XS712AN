.class public final Lcom/google/android/apps/youtube/medialib/player/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/medialib/player/x;


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:I

.field public static final d:I

.field public static final e:I

.field public static final f:I

.field public static final g:I

.field public static final h:I

.field private static final i:Ljava/util/Set;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private final D:Lcom/google/android/apps/youtube/medialib/player/r;

.field private E:I

.field private F:I

.field private G:I

.field private final H:Lcom/google/android/apps/youtube/medialib/player/v;

.field private final I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

.field private final J:Lcom/google/android/apps/youtube/common/network/h;

.field private final K:Ljava/lang/String;

.field private final L:Lcom/google/android/apps/youtube/medialib/player/ae;

.field private final j:Landroid/content/Context;

.field private final k:Ljava/util/concurrent/atomic/AtomicReference;

.field private final l:Lcom/google/android/apps/youtube/medialib/player/u;

.field private final m:Landroid/os/Handler;

.field private final n:Lcom/google/android/apps/youtube/medialib/player/t;

.field private volatile o:Z

.field private volatile p:Z

.field private volatile q:Z

.field private volatile r:Z

.field private volatile s:Z

.field private volatile t:Z

.field private volatile u:Z

.field private v:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

.field private w:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

.field private x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

.field private y:Ljava/lang/String;

.field private z:Lcom/google/android/apps/youtube/medialib/player/y;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const v3, 0x7fffffff

    const/16 v2, -0xbb8

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-le v0, v1, :cond_1

    const/16 v0, -0xbb6

    sput v0, Lcom/google/android/apps/youtube/medialib/player/p;->a:I

    const/16 v0, -0xbb5

    sput v0, Lcom/google/android/apps/youtube/medialib/player/p;->b:I

    const/16 v0, -0xbb4

    sput v0, Lcom/google/android/apps/youtube/medialib/player/p;->c:I

    const/16 v0, -0xbb3

    sput v0, Lcom/google/android/apps/youtube/medialib/player/p;->d:I

    const/16 v0, -0xbb2

    sput v0, Lcom/google/android/apps/youtube/medialib/player/p;->e:I

    const/16 v0, -0xbb1

    sput v0, Lcom/google/android/apps/youtube/medialib/player/p;->f:I

    const/16 v0, -0xbb0

    sput v0, Lcom/google/android/apps/youtube/medialib/player/p;->g:I

    sput v2, Lcom/google/android/apps/youtube/medialib/player/p;->h:I

    :goto_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const/16 v1, -0x3e81

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, -0x3f2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, -0x7d2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, -0x7d1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/apps/youtube/medialib/player/p;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/apps/youtube/medialib/player/p;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/apps/youtube/medialib/player/p;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/apps/youtube/medialib/player/p;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/apps/youtube/medialib/player/p;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/apps/youtube/medialib/player/p;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/apps/youtube/medialib/player/p;->g:I

    if-eq v1, v3, :cond_0

    sget v1, Lcom/google/android/apps/youtube/medialib/player/p;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    sget v1, Lcom/google/android/apps/youtube/medialib/player/p;->h:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x1f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x21

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/medialib/player/p;->i:Ljava/util/Set;

    return-void

    :cond_1
    sput v2, Lcom/google/android/apps/youtube/medialib/player/p;->a:I

    const/16 v0, -0xbb9

    sput v0, Lcom/google/android/apps/youtube/medialib/player/p;->b:I

    const/16 v0, -0xbba

    sput v0, Lcom/google/android/apps/youtube/medialib/player/p;->c:I

    const/16 v0, -0xbbb

    sput v0, Lcom/google/android/apps/youtube/medialib/player/p;->d:I

    const/16 v0, -0xbbc

    sput v0, Lcom/google/android/apps/youtube/medialib/player/p;->e:I

    const/16 v0, -0xbbd

    sput v0, Lcom/google/android/apps/youtube/medialib/player/p;->f:I

    sput v3, Lcom/google/android/apps/youtube/medialib/player/p;->g:I

    const/16 v0, -0xbbe

    sput v0, Lcom/google/android/apps/youtube/medialib/player/p;->h:I

    goto/16 :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/medialib/player/ae;Ljava/lang/String;)V
    .locals 6

    new-instance v5, Lcom/google/android/apps/youtube/medialib/player/q;

    invoke-direct {v5}, Lcom/google/android/apps/youtube/medialib/player/q;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/medialib/player/p;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/medialib/player/ae;Ljava/lang/String;Lcom/google/android/apps/youtube/medialib/player/t;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/medialib/player/ae;Ljava/lang/String;Lcom/google/android/apps/youtube/medialib/player/t;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->j:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->J:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/ae;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->L:Lcom/google/android/apps/youtube/medialib/player/ae;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->K:Ljava/lang/String;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/t;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->n:Lcom/google/android/apps/youtube/medialib/player/t;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->k:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/r;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/medialib/player/r;-><init>(Lcom/google/android/apps/youtube/medialib/player/p;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->D:Lcom/google/android/apps/youtube/medialib/player/r;

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/u;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/medialib/player/u;-><init>(Lcom/google/android/apps/youtube/medialib/player/p;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->l:Lcom/google/android/apps/youtube/medialib/player/u;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->l:Lcom/google/android/apps/youtube/medialib/player/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/u;->start()V

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->m:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/medialib/player/v;-><init>(Lcom/google/android/apps/youtube/medialib/player/p;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->H:Lcom/google/android/apps/youtube/medialib/player/v;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->H:Lcom/google/android/apps/youtube/medialib/player/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/v;->start()V

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->C:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/medialib/player/p;->p:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->o:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/medialib/player/p;->B:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    iput v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->E:I

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/medialib/player/p;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->C:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->o:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->p:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    iput p2, p0, Lcom/google/android/apps/youtube/medialib/player/p;->E:I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->isHls()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->B:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/medialib/player/p;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/medialib/player/p;)V
    .locals 3

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/android/apps/youtube/common/L;->a()V

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->q:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->r:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/p;->b(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->k:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/n;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->t:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->u:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    :cond_0
    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/n;->d()V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/medialib/player/p;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/n;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/p;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->b(I)V

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/n;->a(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Error calling mediaPlayer"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->b(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/youtube/medialib/player/p;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->c(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/medialib/player/p;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/medialib/player/p;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/medialib/player/p;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/medialib/player/p;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/medialib/player/p;Lcom/google/android/apps/youtube/medialib/player/n;Landroid/net/Uri;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->t:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->u:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/y;->a(Lcom/google/android/apps/youtube/medialib/player/n;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "x-disconnect-at-highwatermark"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "User-Agent"

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/p;->K:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->j:Landroid/content/Context;

    invoke-interface {p1, v1, p2, v0}, Lcom/google/android/apps/youtube/medialib/player/n;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    invoke-interface {p1}, Lcom/google/android/apps/youtube/medialib/player/n;->a()V

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/medialib/player/n;->a(Z)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/p;->b(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Media Player error preparing video"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Ljava/lang/Exception;)V

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "Media Player error preparing video"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Ljava/lang/Exception;)V

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v1, "Error calling mediaPlayer"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Media Player null pointer preparing video "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->A:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/medialib/player/p;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->F:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/medialib/player/p;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->w:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    return-object v0
.end method

.method private b(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)V
    .locals 3

    invoke-static {}, Lcom/google/android/apps/youtube/common/L;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->l:Lcom/google/android/apps/youtube/medialib/player/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/u;->c()V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getVideoDurationMillis()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->F:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->e()V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->A:Z

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->w:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->f()V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->n:Lcom/google/android/apps/youtube/medialib/player/t;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->p:Z

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/medialib/player/t;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Z)Lcom/google/android/apps/youtube/medialib/player/n;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/n;->c(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->D:Lcom/google/android/apps/youtube/medialib/player/r;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/n;->a(Lcom/google/android/apps/youtube/medialib/player/o;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->l:Lcom/google/android/apps/youtube/medialib/player/u;

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/p;->y:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getUriWithCpn(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/medialib/player/u;->a(Lcom/google/android/apps/youtube/medialib/player/n;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Factory failed to create a MediaPlayer for the stream"

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/medialib/player/p;)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->C:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/n;

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/android/apps/youtube/common/L;->a()V

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->p:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->r:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->q:Z

    if-eqz v1, :cond_1

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/n;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->d()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->r:Z

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->u:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->q:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->o:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    :cond_2
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->u:Z

    :cond_3
    :goto_1
    return-void

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/p;->o()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/n;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->d()V

    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->r:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->u:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Error calling mediaPlayer"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private b(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->s:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->s:Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->s:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x6

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->q:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/medialib/player/p;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->G:I

    return p1
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/medialib/player/p;)V
    .locals 3

    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->H:Lcom/google/android/apps/youtube/medialib/player/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/v;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/n;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/p;->o()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/android/apps/youtube/common/L;->a()V

    :try_start_0
    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/n;->c()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->r:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/p;->b(Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Error calling mediaPlayer"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->C:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->C:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/medialib/player/p;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/medialib/player/p;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->o:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/medialib/player/p;I)I
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->E:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->w:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->t:Z

    return p1
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/medialib/player/y;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/medialib/player/p;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/medialib/player/p;->b(Z)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/medialib/player/p;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->j:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->r:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/medialib/player/p;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->m:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/medialib/player/p;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->u:Z

    return p1
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/medialib/player/p;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->o:Z

    return v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/medialib/player/p;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/p;->o()Z

    move-result v0

    return v0
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/medialib/player/p;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->p:Z

    return v0
.end method

.method static synthetic k(Lcom/google/android/apps/youtube/medialib/player/p;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->E:I

    return v0
.end method

.method static synthetic l(Lcom/google/android/apps/youtube/medialib/player/p;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->t:Z

    return v0
.end method

.method static synthetic m()Ljava/util/Set;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/medialib/player/p;->i:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/apps/youtube/medialib/player/p;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->u:Z

    return v0
.end method

.method static synthetic n(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    return-object v0
.end method

.method private n()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/n;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/medialib/player/m;->a:Ljava/util/Set;

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/n;->h()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method private o()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->q:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->B:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->o:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic o(Lcom/google/android/apps/youtube/medialib/player/p;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->C:Z

    return v0
.end method

.method static synthetic p(Lcom/google/android/apps/youtube/medialib/player/p;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->G:I

    return v0
.end method

.method static synthetic q(Lcom/google/android/apps/youtube/medialib/player/p;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->q:Z

    return v0
.end method

.method static synthetic r(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/medialib/player/v;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->H:Lcom/google/android/apps/youtube/medialib/player/v;

    return-object v0
.end method

.method static synthetic s(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    return-object v0
.end method

.method static synthetic t(Lcom/google/android/apps/youtube/medialib/player/p;)Lcom/google/android/apps/youtube/common/network/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->J:Lcom/google/android/apps/youtube/common/network/h;

    return-object v0
.end method

.method static synthetic u(Lcom/google/android/apps/youtube/medialib/player/p;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->k:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    return-object v0
.end method

.method public final a(F)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/n;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p1}, Lcom/google/android/apps/youtube/medialib/player/n;->a(FF)V

    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->p:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->E:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->E:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->l:Lcom/google/android/apps/youtube/medialib/player/u;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/youtube/medialib/player/p;->F:I

    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/u;->a(I)V

    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->L:Lcom/google/android/apps/youtube/medialib/player/ae;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/p;->n()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v1, v2, p1}, Lcom/google/android/apps/youtube/medialib/player/ae;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;Ljava/util/Set;I)Lcom/google/android/apps/youtube/medialib/player/ad;
    :try_end_0
    .catch Lcom/google/android/apps/youtube/medialib/player/MissingStreamException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/ad;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/p;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    if-ne v1, v2, :cond_1

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Lcom/google/android/apps/youtube/medialib/player/ad;I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isLive()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/ad;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/p;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/ad;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/p;->f()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/medialib/player/p;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;I)V

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final a(ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;ILjava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)V
    .locals 3

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->y:Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->L:Lcom/google/android/apps/youtube/medialib/player/ae;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/p;->n()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, p2, v1, p1}, Lcom/google/android/apps/youtube/medialib/player/ae;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;Ljava/util/Set;I)Lcom/google/android/apps/youtube/medialib/player/ad;
    :try_end_0
    .catch Lcom/google/android/apps/youtube/medialib/player/MissingStreamException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Lcom/google/android/apps/youtube/medialib/player/ad;I)V

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isLive()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/ad;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/p;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Ljava/lang/Exception;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/ad;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/google/android/apps/youtube/medialib/player/p;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;I)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Handler;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Landroid/os/Handler;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/medialib/player/y;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->D:Lcom/google/android/apps/youtube/medialib/player/r;

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/medialib/player/y;->setListener(Lcom/google/android/apps/youtube/medialib/player/z;)V

    invoke-interface {p1}, Lcom/google/android/apps/youtube/medialib/player/y;->h()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->A:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/n;

    if-eqz v0, :cond_0

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/medialib/player/y;->a(Lcom/google/android/apps/youtube/medialib/player/n;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->r:Z

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/google/android/apps/youtube/medialib/player/y;->d()V

    :cond_1
    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->H:Lcom/google/android/apps/youtube/medialib/player/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/v;->quit()Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->l:Lcom/google/android/apps/youtube/medialib/player/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/u;->quit()Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->g()V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->l:Lcom/google/android/apps/youtube/medialib/player/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/u;->a()V

    return-void
.end method

.method public final b(Landroid/os/Handler;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->I:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->b(Landroid/os/Handler;)Z

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->l:Lcom/google/android/apps/youtube/medialib/player/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/u;->b()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->H:Lcom/google/android/apps/youtube/medialib/player/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/v;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->l:Lcom/google/android/apps/youtube/medialib/player/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/u;->c()V

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->H:Lcom/google/android/apps/youtube/medialib/player/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/v;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->l:Lcom/google/android/apps/youtube/medialib/player/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/u;->d()V

    return-void
.end method

.method public final f()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/n;

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->q:Z

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/n;->e()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->E:I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->E:I

    return v0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->F:I

    return v0
.end method

.method public final h()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->G:I

    return v0
.end method

.method public final i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->s:Z

    return v0
.end method

.method public final j()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->r:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->e()V

    :cond_0
    return-void
.end method

.method public final l()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/n;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/medialib/player/y;->b(Lcom/google/android/apps/youtube/medialib/player/n;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->A:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/medialib/player/y;->setListener(Lcom/google/android/apps/youtube/medialib/player/z;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/p;->e()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/p;->z:Lcom/google/android/apps/youtube/medialib/player/y;

    :cond_1
    return-void
.end method
