.class public final Lcom/google/android/apps/youtube/medialib/player/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/medialib/player/x;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/medialib/player/x;

.field private final b:Lcom/google/android/apps/youtube/medialib/player/x;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/apps/youtube/medialib/player/h;

.field private e:Lcom/google/android/apps/youtube/medialib/player/x;

.field private f:Z

.field private g:Lcom/google/android/apps/youtube/medialib/player/y;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/medialib/player/x;Lcom/google/android/apps/youtube/medialib/player/x;Landroid/content/Context;Lcom/google/android/apps/youtube/medialib/player/h;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/g;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    iput-object p2, p0, Lcom/google/android/apps/youtube/medialib/player/g;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    iput-object p3, p0, Lcom/google/android/apps/youtube/medialib/player/g;->c:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/apps/youtube/medialib/player/g;->d:Lcom/google/android/apps/youtube/medialib/player/h;

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)Z
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    :try_start_0
    invoke-static {}, Lcom/google/android/apps/common/a/a/a;->a()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v0, v3, :cond_1

    invoke-static {}, Lcom/google/android/apps/youtube/medialib/player/g;->m()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isLive()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getProgressiveFormatStreams()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->isExoPlayerEnabled()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isAudioOnly()Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_1
    if-nez v0, :cond_9

    move v0, v1

    goto :goto_0

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getAdaptiveFormatStreams()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/i;->b(I)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    goto :goto_1

    :cond_8
    move v0, v2

    goto :goto_1

    :cond_9
    move v0, v2

    goto :goto_0
.end method

.method private static m()Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x2

    const/16 v2, 0x40

    :try_start_0
    invoke-static {v1, v2}, Lcom/google/android/exoplayer/aa;->a(II)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v0

    return-object v0
.end method

.method public final a(F)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(F)V

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(I)V

    return-void
.end method

.method public final a(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/medialib/player/x;->a(II)V

    return-void
.end method

.method public final a(ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;ILjava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)V
    .locals 6

    invoke-direct {p0, p2, p5}, Lcom/google/android/apps/youtube/medialib/player/g;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    if-eq v0, v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/medialib/player/g;->f:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/medialib/player/x;->l()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/g;->g:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Lcom/google/android/apps/youtube/medialib/player/y;)V

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->d:Lcom/google/android/apps/youtube/medialib/player/h;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->d:Lcom/google/android/apps/youtube/medialib/player/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/h;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v1, :cond_4

    const-string v0, "Using fallback player"

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/g;->c:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/youtube/medialib/player/x;->a(ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;ILjava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)V

    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    goto :goto_0

    :cond_4
    const-string v0, "Using ExoPlayer"

    goto :goto_1
.end method

.method public final a(Landroid/os/Handler;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Landroid/os/Handler;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/medialib/player/y;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->f:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/g;->g:Lcom/google/android/apps/youtube/medialib/player/y;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Lcom/google/android/apps/youtube/medialib/player/y;)V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Z)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->b()V

    return-void
.end method

.method public final b(Landroid/os/Handler;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->b(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->b(Landroid/os/Handler;)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->c()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->d()V

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->e()V

    return-void
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->f()I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->g()I

    move-result v0

    return v0
.end method

.method public final h()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->h()I

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->i()Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->j()Z

    move-result v0

    return v0
.end method

.method public final k()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->k()V

    return-void
.end method

.method public final l()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->f:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->g:Lcom/google/android/apps/youtube/medialib/player/y;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/g;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->l()V

    return-void
.end method
