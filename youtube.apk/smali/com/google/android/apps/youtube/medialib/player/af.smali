.class public final Lcom/google/android/apps/youtube/medialib/player/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/medialib/player/x;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/medialib/player/x;

.field private final b:Lcom/google/android/apps/youtube/medialib/player/x;

.field private final c:Landroid/os/Handler;

.field private final d:Landroid/os/Handler;

.field private final e:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private f:Lcom/google/android/apps/youtube/medialib/player/x;

.field private g:I

.field private h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

.field private i:Ljava/lang/String;

.field private j:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

.field private k:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/medialib/player/x;Lcom/google/android/apps/youtube/medialib/player/x;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    iput-object p2, p0, Lcom/google/android/apps/youtube/medialib/player/af;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/medialib/player/ai;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/medialib/player/ai;-><init>(Lcom/google/android/apps/youtube/medialib/player/af;B)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->c:Landroid/os/Handler;

    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/medialib/player/ag;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/medialib/player/ag;-><init>(Lcom/google/android/apps/youtube/medialib/player/af;B)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->d:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->c:Landroid/os/Handler;

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->d:Landroid/os/Handler;

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Landroid/os/Handler;)V

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    iput-object p2, p0, Lcom/google/android/apps/youtube/medialib/player/af;->f:Lcom/google/android/apps/youtube/medialib/player/x;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/medialib/player/af;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method private b(Z)V
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    const/16 v4, 0x10

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-static {v0, v4, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/medialib/player/af;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/af;->m()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/medialib/player/af;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->k:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/medialib/player/af;)Lcom/google/android/apps/youtube/medialib/player/x;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/medialib/player/af;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/af;->n()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/medialib/player/af;)Lcom/google/android/apps/youtube/medialib/player/x;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    return-object v0
.end method

.method private m()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->f:Lcom/google/android/apps/youtube/medialib/player/x;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/af;->k:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->f:Lcom/google/android/apps/youtube/medialib/player/x;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->c()V

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/medialib/player/af;->b(Z)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->f:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v0

    return-object v0
.end method

.method public final a(F)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(F)V

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->k:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/af;->n()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->f:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(I)V

    return-void
.end method

.method public final a(II)V
    .locals 1

    iput p1, p0, Lcom/google/android/apps/youtube/medialib/player/af;->g:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->f:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/medialib/player/x;->a(II)V

    return-void
.end method

.method public final a(ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;ILjava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)V
    .locals 6

    iput p1, p0, Lcom/google/android/apps/youtube/medialib/player/af;->g:I

    iput-object p2, p0, Lcom/google/android/apps/youtube/medialib/player/af;->h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    iput-object p4, p0, Lcom/google/android/apps/youtube/medialib/player/af;->i:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/youtube/medialib/player/af;->j:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/af;->m()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isLive()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/youtube/medialib/player/x;->a(ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;ILjava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/youtube/medialib/player/x;->a(ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;ILjava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Handler;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/medialib/player/y;)V
    .locals 7

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v6}, Lcom/google/android/apps/youtube/medialib/player/af;->b(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Lcom/google/android/apps/youtube/medialib/player/y;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/af;->m()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->j()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->k:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isOffline()Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x7d0

    move v3, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    iget v1, p0, Lcom/google/android/apps/youtube/medialib/player/af;->g:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/af;->h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    iget-object v4, p0, Lcom/google/android/apps/youtube/medialib/player/af;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v4}, Lcom/google/android/apps/youtube/medialib/player/x;->f()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/youtube/medialib/player/af;->i:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/youtube/medialib/player/af;->j:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/youtube/medialib/player/x;->a(ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;ILjava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)V

    iput-boolean v6, p0, Lcom/google/android/apps/youtube/medialib/player/af;->k:Z

    :cond_2
    :goto_1
    return-void

    :cond_3
    const/16 v0, 0xfa0

    move v3, v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->f:Lcom/google/android/apps/youtube/medialib/player/x;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->g()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    iget v1, p0, Lcom/google/android/apps/youtube/medialib/player/af;->g:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/af;->h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    iget-object v3, p0, Lcom/google/android/apps/youtube/medialib/player/af;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/medialib/player/x;->f()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/medialib/player/af;->i:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/youtube/medialib/player/af;->j:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/youtube/medialib/player/x;->a(ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;ILjava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->c()V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/af;->c:Landroid/os/Handler;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/x;->b(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/af;->d:Landroid/os/Handler;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/x;->b(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Z)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->k:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/af;->n()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->f:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->b()V

    return-void
.end method

.method public final b(Landroid/os/Handler;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->f:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->c()V

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->d()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->d()V

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->e()V

    return-void
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->f:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->f()I

    move-result v0

    return v0
.end method

.method public final g()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->f:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->g()I

    move-result v0

    return v0
.end method

.method public final h()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->h()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->f:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->h()I

    move-result v0

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->f:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->i()Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->f:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->j()Z

    move-result v0

    return v0
.end method

.method public final k()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->k()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/af;->b(Z)V

    return-void
.end method

.method public final l()V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isLive()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/medialib/player/af;->k:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/af;->m()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    iget v1, p0, Lcom/google/android/apps/youtube/medialib/player/af;->g:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/af;->h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    iget-object v3, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/medialib/player/x;->f()I

    move-result v3

    add-int/lit16 v3, v3, 0xc8

    iget-object v4, p0, Lcom/google/android/apps/youtube/medialib/player/af;->i:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/youtube/medialib/player/af;->j:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/youtube/medialib/player/x;->a(ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;ILjava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->b:Lcom/google/android/apps/youtube/medialib/player/x;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->f:Lcom/google/android/apps/youtube/medialib/player/x;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->d()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/af;->a:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->l()V

    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method
