.class public final Lcom/google/android/apps/youtube/medialib/player/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/medialib/player/x;
.implements Lcom/google/android/exoplayer/g;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/common/fromguava/e;

.field private final b:Lcom/google/android/exoplayer/upstream/c;

.field private final c:Lcom/google/android/apps/youtube/medialib/player/h;

.field private final d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

.field private final e:Lcom/google/android/apps/youtube/medialib/player/k;

.field private final f:Lcom/google/android/exoplayer/af;

.field private final g:Lcom/google/android/apps/youtube/medialib/player/ae;

.field private h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

.field private i:Lcom/google/android/apps/youtube/medialib/player/y;

.field private j:Lcom/google/android/exoplayer/d;

.field private k:Lcom/google/android/apps/youtube/medialib/player/ak;

.field private l:Lcom/google/android/exoplayer/ak;

.field private m:Lcom/google/android/exoplayer/ak;

.field private n:Z

.field private o:Z

.field private p:Ljava/util/List;

.field private q:[Lcom/google/android/exoplayer/b/a/a;

.field private r:[Lcom/google/android/exoplayer/b/a/a;

.field private s:I

.field private t:I

.field private u:F

.field private v:Z

.field private w:Ljava/lang/String;

.field private x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/common/fromguava/e;Lcom/google/android/exoplayer/upstream/c;Lcom/google/android/apps/youtube/medialib/player/ae;Lcom/google/android/apps/youtube/medialib/player/h;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->a:Lcom/google/android/apps/youtube/common/fromguava/e;

    iput-object p2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->b:Lcom/google/android/exoplayer/upstream/c;

    iput-object p3, p0, Lcom/google/android/apps/youtube/medialib/player/i;->g:Lcom/google/android/apps/youtube/medialib/player/ae;

    iput-object p4, p0, Lcom/google/android/apps/youtube/medialib/player/i;->c:Lcom/google/android/apps/youtube/medialib/player/h;

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/k;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/medialib/player/k;-><init>(Lcom/google/android/apps/youtube/medialib/player/i;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->e:Lcom/google/android/apps/youtube/medialib/player/k;

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/j;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/medialib/player/j;-><init>(Lcom/google/android/apps/youtube/medialib/player/i;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->f:Lcom/google/android/exoplayer/af;

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->u:F

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/medialib/player/i;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->s:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/medialib/player/i;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->p()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    new-instance v1, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents$CantPlayStreamException;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents$CantPlayStreamException;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Ljava/lang/Exception;)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->q:[Lcom/google/android/exoplayer/b/a/a;

    iput-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->r:[Lcom/google/android/exoplayer/b/a/a;

    iput-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    return-void
.end method

.method private a([Lcom/google/android/exoplayer/b/a/a;[Lcom/google/android/exoplayer/b/a/a;I)V
    .locals 12

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->n()V

    :cond_0
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->minBufferMs()I

    move-result v1

    const/16 v2, 0x1388

    invoke-static {v0, v1, v2}, Lcom/google/android/exoplayer/f;->a(III)Lcom/google/android/exoplayer/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v0}, Lcom/google/android/exoplayer/d;->a(IZ)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    invoke-interface {v0, p0}, Lcom/google/android/exoplayer/d;->a(Lcom/google/android/exoplayer/g;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    invoke-interface {v0, p3}, Lcom/google/android/exoplayer/d;->a(I)V

    new-instance v9, Landroid/os/Handler;

    invoke-direct {v9}, Landroid/os/Handler;-><init>()V

    new-instance v0, Lcom/google/android/exoplayer/m;

    new-instance v1, Lcom/google/android/exoplayer/upstream/f;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->m()I

    move-result v2

    invoke-direct {v1, v2}, Lcom/google/android/exoplayer/upstream/f;-><init>(I)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/youtube/medialib/player/i;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->lowWatermarkMs()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/medialib/player/i;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->highWatermarkMs()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/medialib/player/i;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->lowPoolLoad()F

    move-result v6

    iget-object v7, p0, Lcom/google/android/apps/youtube/medialib/player/i;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->highPoolLoad()F

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/exoplayer/m;-><init>(Lcom/google/android/exoplayer/upstream/b;Landroid/os/Handler;Lcom/google/android/exoplayer/o;IIFF)V

    if-eqz p1, :cond_5

    const/4 v1, 0x0

    aget-object v1, p1, v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    move v2, v1

    :goto_1
    if-eqz v2, :cond_6

    const/4 v1, 0x2

    :goto_2
    new-array v10, v1, [Lcom/google/android/exoplayer/ak;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->a:Lcom/google/android/apps/youtube/common/fromguava/e;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/fromguava/e;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer/upstream/i;

    new-instance v4, Lcom/google/android/exoplayer/b/a;

    new-instance v5, Lcom/google/android/exoplayer/a/s;

    invoke-direct {v5}, Lcom/google/android/exoplayer/a/s;-><init>()V

    const/4 v6, 0x1

    new-array v6, v6, [Lcom/google/android/exoplayer/b/a/a;

    const/4 v7, 0x0

    const/4 v8, 0x0

    aget-object v8, p2, v8

    aput-object v8, v6, v7

    invoke-direct {v4, v1, v5, v6}, Lcom/google/android/exoplayer/b/a;-><init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/a/o;[Lcom/google/android/exoplayer/b/a/a;)V

    new-instance v5, Lcom/google/android/exoplayer/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->c:Lcom/google/android/apps/youtube/medialib/player/h;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/medialib/player/h;->d()I

    move-result v1

    const/4 v6, -0x1

    if-ne v1, v6, :cond_1

    const/16 v1, 0x64

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->m()I

    move-result v6

    mul-int/2addr v1, v6

    const/4 v6, 0x1

    invoke-direct {v5, v4, v0, v1, v6}, Lcom/google/android/exoplayer/a/b;-><init>(Lcom/google/android/exoplayer/a/l;Lcom/google/android/exoplayer/m;IZ)V

    new-instance v1, Lcom/google/android/exoplayer/r;

    const/4 v4, 0x0

    invoke-direct {v1, v5, v4}, Lcom/google/android/exoplayer/r;-><init>(Lcom/google/android/exoplayer/ai;Lcom/google/android/exoplayer/c/a;)V

    aput-object v1, v10, v3

    if-eqz v2, :cond_3

    const/4 v11, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->a:Lcom/google/android/apps/youtube/common/fromguava/e;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/fromguava/e;->b()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/google/android/exoplayer/upstream/i;

    new-instance v1, Lcom/google/android/apps/youtube/medialib/player/ak;

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->b:Lcom/google/android/exoplayer/upstream/c;

    iget-object v3, p0, Lcom/google/android/apps/youtube/medialib/player/i;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->maxInitialByteRate()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/medialib/player/i;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->minDurationForQualityIncreaseMs()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/medialib/player/i;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->maxDurationForQualityDecreaseMs()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/medialib/player/i;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->minDurationToRetainAfterDiscardMs()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/apps/youtube/medialib/player/i;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->bandwidthFraction()F

    move-result v7

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/youtube/medialib/player/ak;-><init>(Lcom/google/android/exoplayer/upstream/c;IIIIF)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->k:Lcom/google/android/apps/youtube/medialib/player/ak;

    new-instance v3, Lcom/google/android/exoplayer/b/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->k:Lcom/google/android/apps/youtube/medialib/player/ak;

    invoke-direct {v3, v8, v1, p1}, Lcom/google/android/exoplayer/b/a;-><init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/a/o;[Lcom/google/android/exoplayer/b/a/a;)V

    new-instance v1, Lcom/google/android/exoplayer/a/b;

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->c:Lcom/google/android/apps/youtube/medialib/player/h;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/medialib/player/h;->c()I

    move-result v2

    const/4 v4, -0x1

    if-ne v2, v4, :cond_2

    const/16 v2, 0x104

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->m()I

    move-result v4

    mul-int/2addr v2, v4

    const/4 v4, 0x1

    invoke-direct {v1, v3, v0, v2, v4}, Lcom/google/android/exoplayer/a/b;-><init>(Lcom/google/android/exoplayer/a/l;Lcom/google/android/exoplayer/m;IZ)V

    new-instance v0, Lcom/google/android/exoplayer/ab;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-wide/16 v4, 0x1388

    iget-object v7, p0, Lcom/google/android/apps/youtube/medialib/player/i;->f:Lcom/google/android/exoplayer/af;

    const/4 v8, 0x1

    move-object v6, v9

    invoke-direct/range {v0 .. v8}, Lcom/google/android/exoplayer/ab;-><init>(Lcom/google/android/exoplayer/ai;Lcom/google/android/exoplayer/c/a;IJLandroid/os/Handler;Lcom/google/android/exoplayer/af;I)V

    aput-object v0, v10, v11

    :cond_3
    const/4 v0, 0x0

    aget-object v0, v10, v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->m:Lcom/google/android/exoplayer/ak;

    array-length v0, v10

    const/4 v1, 0x1

    if-le v0, v1, :cond_7

    const/4 v0, 0x1

    aget-object v0, v10, v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->l:Lcom/google/android/exoplayer/ak;

    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    invoke-interface {v0, v10}, Lcom/google/android/exoplayer/d;->a([Lcom/google/android/exoplayer/ak;)V

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->u:F

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/i;->a(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    return-void

    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_5
    const/4 v1, 0x0

    move v2, v1

    goto/16 :goto_1

    :cond_6
    const/4 v1, 0x1

    goto/16 :goto_2

    :cond_7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->l:Lcom/google/android/exoplayer/ak;

    goto :goto_3
.end method

.method private static a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Ljava/lang/String;)[Lcom/google/android/exoplayer/b/a/a;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/exoplayer/b/a/a;

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getRepresentation(Ljava/lang/String;)Lcom/google/android/exoplayer/b/a/a;

    move-result-object v2

    aput-object v2, v0, v1

    return-object v0
.end method

.method private static a(Ljava/util/List;Ljava/lang/String;)[Lcom/google/android/exoplayer/b/a/a;
    .locals 3

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/medialib/player/i;->c(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/exoplayer/b/a/a;

    const/4 v2, 0x0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getRepresentation(Ljava/lang/String;)Lcom/google/android/exoplayer/b/a/a;

    move-result-object v0

    aput-object v0, v1, v2

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/medialib/player/i;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->s:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/medialib/player/i;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->t:I

    return p1
.end method

.method private static b(Ljava/util/List;Ljava/lang/String;)Landroid/util/Pair;
    .locals 6

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/youtube/medialib/player/i;->b(I)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getRepresentation(Ljava/lang/String;)Lcom/google/android/exoplayer/b/a/a;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-static {v4}, Lcom/google/android/apps/youtube/medialib/player/i;->c(I)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getRepresentation(Ljava/lang/String;)Lcom/google/android/exoplayer/b/a/a;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_4
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Lcom/google/android/exoplayer/b/a/a;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [Lcom/google/android/exoplayer/b/a/a;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static b(I)Z
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/c;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/medialib/player/i;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->t:I

    return v0
.end method

.method private static c(I)Z
    .locals 1

    const/16 v0, 0x8c

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/apps/youtube/medialib/player/y;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/apps/youtube/medialib/player/ak;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->k:Lcom/google/android/apps/youtube/medialib/player/ak;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/exoplayer/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/medialib/player/i;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->q()V

    return-void
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    return-object v0
.end method

.method private m()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->c:Lcom/google/android/apps/youtube/medialib/player/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/h;->b()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const v0, 0xc800

    :cond_0
    return v0
.end method

.method private n()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->o()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    invoke-interface {v0}, Lcom/google/android/exoplayer/d;->c()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    iput-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->l:Lcom/google/android/exoplayer/ak;

    iput-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    :cond_0
    return-void
.end method

.method private o()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->l:Lcom/google/android/exoplayer/ak;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->l:Lcom/google/android/exoplayer/ak;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/medialib/player/y;->b(Lcom/google/android/exoplayer/d;Lcom/google/android/exoplayer/ak;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->o:Z

    :cond_0
    return-void
.end method

.method private p()V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->l:Lcom/google/android/exoplayer/ak;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->k:Lcom/google/android/apps/youtube/medialib/player/ak;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/medialib/player/y;->b()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/medialib/player/y;->c()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/medialib/player/ak;->a(Lcom/google/android/exoplayer/d;II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->l:Lcom/google/android/exoplayer/ak;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/medialib/player/y;->a(Lcom/google/android/exoplayer/d;Lcom/google/android/exoplayer/ak;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    invoke-interface {v0, v4, v4}, Lcom/google/android/exoplayer/d;->a(IZ)V

    iput-boolean v4, p0, Lcom/google/android/apps/youtube/medialib/player/i;->o:Z

    :cond_0
    return-void
.end method

.method private q()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/y;->setListener(Lcom/google/android/apps/youtube/medialib/player/z;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->o()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    return-object v0
.end method

.method public final a(F)V
    .locals 4

    iput p1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->u:F

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->m:Lcom/google/android/exoplayer/ak;

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/exoplayer/d;->a(Lcom/google/android/exoplayer/e;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->b(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->v:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer/d;->a(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->c(I)V

    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->isAdaptiveBitrateEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->g:Lcom/google/android/apps/youtube/medialib/player/ae;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->p:Ljava/util/List;

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/e;->b()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v1, v2, p1}, Lcom/google/android/apps/youtube/medialib/player/ae;->a(Ljava/util/Collection;Ljava/util/Set;I)Lcom/google/android/apps/youtube/medialib/player/ad;
    :try_end_0
    .catch Lcom/google/android/apps/youtube/medialib/player/MissingStreamException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/ad;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/ad;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->p:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->w:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/medialib/player/i;->a(Ljava/util/List;Ljava/lang/String;)[Lcom/google/android/exoplayer/b/a/a;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v2, v0, p2}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Lcom/google/android/apps/youtube/medialib/player/ad;I)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->r:[Lcom/google/android/exoplayer/b/a/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->w:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/medialib/player/i;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Ljava/lang/String;)[Lcom/google/android/exoplayer/b/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->q:[Lcom/google/android/exoplayer/b/a/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->q:[Lcom/google/android/exoplayer/b/a/a;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->f()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/youtube/medialib/player/i;->a([Lcom/google/android/exoplayer/b/a/a;[Lcom/google/android/exoplayer/b/a/a;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->b()V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;ILjava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)V
    .locals 4

    const/4 v3, 0x0

    iput v3, p0, Lcom/google/android/apps/youtube/medialib/player/i;->s:I

    iput v3, p0, Lcom/google/android/apps/youtube/medialib/player/i;->t:I

    iput-boolean v3, p0, Lcom/google/android/apps/youtube/medialib/player/i;->v:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->e()V

    :cond_0
    if-nez p2, :cond_1

    const-string v0, "No streaming data available for ExoPlayer playback."

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/i;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isLive()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Live video not supported by adaptive DASH player."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    :cond_2
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getAdaptiveFormatStreams()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->p:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "No adaptive streams available for ExoPlayer playback."

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/i;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isAudioOnly()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getAudioOnlyStream()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/ad;

    invoke-direct {v0, v1, v1, p1, v3}, Lcom/google/android/apps/youtube/medialib/player/ad;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;IZ)V

    :goto_1
    iput-object p4, p0, Lcom/google/android/apps/youtube/medialib/player/i;->w:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/youtube/medialib/player/i;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/ad;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p0, p3}, Lcom/google/android/apps/youtube/medialib/player/i;->a(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->d()V

    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->b()V

    goto :goto_0

    :cond_5
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->g:Lcom/google/android/apps/youtube/medialib/player/ae;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->p:Ljava/util/List;

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/e;->b()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v1, v2, p1}, Lcom/google/android/apps/youtube/medialib/player/ae;->a(Ljava/util/Collection;Ljava/util/Set;I)Lcom/google/android/apps/youtube/medialib/player/ad;
    :try_end_0
    .catch Lcom/google/android/apps/youtube/medialib/player/MissingStreamException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "No video stream selected for ExoPlayer playback."

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/i;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    iput-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->h:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    invoke-virtual {p5}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->isAdaptiveBitrateEnabled()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->p:Ljava/util/List;

    invoke-static {v1, p4}, Lcom/google/android/apps/youtube/medialib/player/i;->b(Ljava/util/List;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    :goto_3
    if-nez v1, :cond_9

    const-string v0, "No compatible adaptive streams available for ExoPlayer playback."

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/i;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->p:Ljava/util/List;

    invoke-static {v2, p4}, Lcom/google/android/apps/youtube/medialib/player/i;->a(Ljava/util/List;Ljava/lang/String;)[Lcom/google/android/exoplayer/b/a/a;

    move-result-object v2

    invoke-static {v1, p4}, Lcom/google/android/apps/youtube/medialib/player/i;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Ljava/lang/String;)[Lcom/google/android/exoplayer/b/a/a;

    move-result-object v1

    if-nez v2, :cond_8

    if-nez v1, :cond_8

    const-string v0, "No compatible audio or video streams available for ExoPlayer playback"

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/i;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    invoke-static {v1, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    goto :goto_3

    :cond_9
    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Lcom/google/android/apps/youtube/medialib/player/ad;I)V

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, [Lcom/google/android/exoplayer/b/a/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->q:[Lcom/google/android/exoplayer/b/a/a;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, [Lcom/google/android/exoplayer/b/a/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->r:[Lcom/google/android/exoplayer/b/a/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->q:[Lcom/google/android/exoplayer/b/a/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->r:[Lcom/google/android/exoplayer/b/a/a;

    invoke-direct {p0, v0, v1, p3}, Lcom/google/android/apps/youtube/medialib/player/i;->a([Lcom/google/android/exoplayer/b/a/a;[Lcom/google/android/exoplayer/b/a/a;I)V

    goto :goto_2
.end method

.method public final a(Landroid/os/Handler;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Landroid/os/Handler;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/medialib/player/y;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->q()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->e:Lcom/google/android/apps/youtube/medialib/player/k;

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/medialib/player/y;->setListener(Lcom/google/android/apps/youtube/medialib/player/z;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->p()V

    return-void
.end method

.method public final a(Lcom/google/android/exoplayer/ExoPlaybackException;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->n()V

    return-void
.end method

.method public final a(ZI)V
    .locals 3

    const/4 v2, 0x0

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->n:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->n:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->n:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    :cond_1
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->v:Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/medialib/player/i;->v:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->q:[Lcom/google/android/exoplayer/b/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->r:[Lcom/google/android/exoplayer/b/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->q:[Lcom/google/android/exoplayer/b/a/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/i;->r:[Lcom/google/android/exoplayer/b/a/a;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/youtube/medialib/player/i;->a([Lcom/google/android/exoplayer/b/a/a;[Lcom/google/android/exoplayer/b/a/a;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->p()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    invoke-interface {v0}, Lcom/google/android/exoplayer/d;->a()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->v:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    invoke-interface {v0, v2}, Lcom/google/android/exoplayer/d;->a(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/d;->a(Z)V

    goto :goto_0
.end method

.method public final b(Landroid/os/Handler;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->b(Landroid/os/Handler;)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/d;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->e()V

    return-void
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->n()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->d:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    :cond_0
    return-void
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    invoke-interface {v0}, Lcom/google/android/exoplayer/d;->e()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    invoke-interface {v0}, Lcom/google/android/exoplayer/d;->d()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    invoke-interface {v0}, Lcom/google/android/exoplayer/d;->f()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    invoke-interface {v0}, Lcom/google/android/exoplayer/d;->a()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->i()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    invoke-interface {v0}, Lcom/google/android/exoplayer/d;->a()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    invoke-interface {v0}, Lcom/google/android/exoplayer/d;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->i:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->e()V

    :cond_0
    return-void
.end method

.method public final l()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/i;->j:Lcom/google/android/exoplayer/d;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/exoplayer/d;->a(IZ)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/i;->q()V

    return-void
.end method
