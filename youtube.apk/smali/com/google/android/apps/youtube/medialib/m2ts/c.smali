.class final Lcom/google/android/apps/youtube/medialib/m2ts/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/medialib/m2ts/d;

.field private final b:Lcom/google/android/exoplayer/ag;

.field private c:B


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/medialib/m2ts/d;Lcom/google/android/exoplayer/ag;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/c;->a:Lcom/google/android/apps/youtube/medialib/m2ts/d;

    iput-object p2, p0, Lcom/google/android/apps/youtube/medialib/m2ts/c;->b:Lcom/google/android/exoplayer/ag;

    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/c;->c:B

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/c;->a:Lcom/google/android/apps/youtube/medialib/m2ts/d;

    const-string v1, "474000100000b00d0001c100000001f0002ab104b2"

    invoke-static {v1}, Lcom/google/android/apps/youtube/medialib/m2ts/c;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/medialib/m2ts/d;->a([B)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/c;->a:Lcom/google/android/apps/youtube/medialib/m2ts/d;

    const-string v1, "475000100002b0180001c10000e100f0000fe100f0060a04756e6400152c6928"

    invoke-static {v1}, Lcom/google/android/apps/youtube/medialib/m2ts/c;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/medialib/m2ts/d;->a([B)V

    return-void
.end method

.method private static a(Ljava/lang/String;)[B
    .locals 10

    const/16 v9, 0xbc

    const/16 v8, 0x10

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    mul-int/lit8 v0, v3, 0x2

    new-array v4, v0, [B

    move v0, v2

    :goto_1
    if-ge v0, v3, :cond_1

    div-int/lit8 v5, v0, 0x2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6, v8}, Ljava/lang/Character;->digit(CI)I

    move-result v6

    shl-int/lit8 v6, v6, 0x4

    add-int/lit8 v7, v0, 0x1

    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v7, v8}, Ljava/lang/Character;->digit(CI)I

    move-result v7

    add-int/2addr v6, v7

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    array-length v0, v4

    if-gt v0, v9, :cond_2

    :goto_2
    invoke-static {v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    const/4 v0, -0x1

    invoke-static {v4, v9, v0}, Lcom/google/android/apps/youtube/common/e/a;->a([BIB)[B

    move-result-object v0

    return-object v0

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method private a([BJZ)[B
    .locals 9

    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    if-eqz p4, :cond_0

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    or-int/lit8 v2, v2, 0x40

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    :cond_0
    iget-byte v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/c;->c:B

    or-int/lit8 v1, v1, 0x30

    int-to-byte v1, v1

    iget-byte v2, p0, Lcom/google/android/apps/youtube/medialib/m2ts/c;->c:B

    add-int/lit8 v2, v2, 0x1

    and-int/lit8 v2, v2, 0xf

    int-to-byte v2, v2

    iput-byte v2, p0, Lcom/google/android/apps/youtube/medialib/m2ts/c;->c:B

    const/4 v2, 0x4

    new-array v2, v2, [B

    const/4 v3, 0x0

    const/16 v4, 0x47

    aput-byte v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    aget-byte v4, v0, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x2

    const/4 v4, 0x1

    aget-byte v0, v0, v4

    aput-byte v0, v2, v3

    const/4 v0, 0x3

    aput-byte v1, v2, v0

    array-length v1, p1

    const-wide/16 v3, -0x1

    cmp-long v0, p2, v3

    if-eqz v0, :cond_1

    const/4 v0, 0x7

    new-array v0, v0, [B

    const/4 v3, 0x0

    const/16 v4, 0x50

    aput-byte v4, v0, v3

    const/4 v3, 0x1

    const/16 v4, 0x19

    shr-long v4, p2, v4

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    const/4 v3, 0x2

    const/16 v4, 0x11

    shr-long v4, p2, v4

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    const/4 v3, 0x3

    const/16 v4, 0x9

    shr-long v4, p2, v4

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    const/4 v3, 0x4

    const/4 v4, 0x1

    shr-long v4, p2, v4

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    const/4 v3, 0x5

    const/4 v4, 0x7

    shl-long v4, p2, v4

    const-wide/16 v6, 0x80

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    const/4 v3, 0x6

    const/4 v4, 0x0

    aput-byte v4, v0, v3

    :goto_0
    array-length v3, v0

    add-int/lit8 v3, v3, 0x1

    rsub-int v4, v3, 0xb8

    sub-int/2addr v4, v1

    if-ltz v4, :cond_2

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    new-array v1, v4, [B

    const/4 v5, -0x1

    invoke-static {v1, v5}, Ljava/util/Arrays;->fill([BB)V

    const/4 v5, 0x3

    new-array v5, v5, [[B

    const/4 v6, 0x0

    const/4 v7, 0x1

    new-array v7, v7, [B

    const/4 v8, 0x0

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    int-to-byte v3, v3

    aput-byte v3, v7, v8

    aput-object v7, v5, v6

    const/4 v3, 0x1

    aput-object v0, v5, v3

    const/4 v0, 0x2

    aput-object v1, v5, v0

    invoke-static {v5}, Lcom/google/android/apps/youtube/common/e/a;->a([[B)[B

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [[B

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object v0, v1, v2

    const/4 v0, 0x2

    aput-object p1, v1, v0

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/e/a;->a([[B)[B

    move-result-object v1

    array-length v0, v1

    const/16 v2, 0xbc

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    return-object v1

    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput-byte v4, v0, v3

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :array_0
    .array-data 1
        0x1t
        0x0t
    .end array-data
.end method


# virtual methods
.method public final a([Lcom/google/android/exoplayer/ah;I)V
    .locals 11

    new-array v1, p2, [[B

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    aget-object v2, p1, v0

    iget-object v3, p0, Lcom/google/android/apps/youtube/medialib/m2ts/c;->b:Lcom/google/android/exoplayer/ag;

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/medialib/m2ts/a;->a(Lcom/google/android/exoplayer/ah;Lcom/google/android/exoplayer/ag;)[B

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v1}, Lcom/google/android/apps/youtube/common/e/a;->a([[B)[B

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    iget-wide v1, v1, Lcom/google/android/exoplayer/ah;->f:J

    const-wide/16 v3, 0x9

    mul-long/2addr v1, v3

    const-wide/16 v3, 0x64

    div-long/2addr v1, v3

    array-length v3, v0

    add-int/lit8 v3, v3, 0x8

    const/16 v4, 0xe

    new-array v4, v4, [B

    const/4 v5, 0x0

    const/4 v6, 0x0

    aput-byte v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput-byte v6, v4, v5

    const/4 v5, 0x2

    const/4 v6, 0x1

    aput-byte v6, v4, v5

    const/4 v5, 0x3

    const/16 v6, -0x40

    aput-byte v6, v4, v5

    const/4 v5, 0x4

    shr-int/lit8 v6, v3, 0x8

    int-to-byte v6, v6

    aput-byte v6, v4, v5

    const/4 v5, 0x5

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v4, v5

    const/4 v3, 0x6

    const/16 v5, -0x80

    aput-byte v5, v4, v3

    const/4 v3, 0x7

    const/16 v5, -0x80

    aput-byte v5, v4, v3

    const/16 v3, 0x8

    const/4 v5, 0x5

    aput-byte v5, v4, v3

    const/16 v3, 0x9

    const-wide/16 v5, 0x21

    const/16 v7, 0x1e

    shr-long v7, v1, v7

    or-long/2addr v5, v7

    long-to-int v5, v5

    int-to-byte v5, v5

    aput-byte v5, v4, v3

    const/16 v3, 0xa

    const/16 v5, 0x16

    shr-long v5, v1, v5

    const-wide/16 v7, 0xff

    and-long/2addr v5, v7

    long-to-int v5, v5

    int-to-byte v5, v5

    aput-byte v5, v4, v3

    const/16 v3, 0xb

    const-wide/16 v5, 0x1

    const/16 v7, 0xe

    shr-long v7, v1, v7

    const-wide/16 v9, 0xfe

    and-long/2addr v7, v9

    or-long/2addr v5, v7

    long-to-int v5, v5

    int-to-byte v5, v5

    aput-byte v5, v4, v3

    const/16 v3, 0xc

    const/4 v5, 0x7

    shr-long v5, v1, v5

    const-wide/16 v7, 0xff

    and-long/2addr v5, v7

    long-to-int v5, v5

    int-to-byte v5, v5

    aput-byte v5, v4, v3

    const/16 v3, 0xd

    const/4 v5, 0x1

    shl-long v5, v1, v5

    const-wide/16 v7, 0xfe

    and-long/2addr v5, v7

    const-wide/16 v7, 0x1

    or-long/2addr v5, v7

    long-to-int v5, v5

    int-to-byte v5, v5

    aput-byte v5, v4, v3

    const/4 v3, 0x2

    new-array v3, v3, [[B

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v3}, Lcom/google/android/apps/youtube/common/e/a;->a([[B)[B

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/c;->a:Lcom/google/android/apps/youtube/medialib/m2ts/d;

    const/4 v4, 0x0

    const/16 v5, 0xb0

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/youtube/common/e/a;->a([BII)[B

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {p0, v4, v1, v2, v5}, Lcom/google/android/apps/youtube/medialib/m2ts/c;->a([BJZ)[B

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/medialib/m2ts/d;->a([B)V

    const/16 v0, 0xb0

    :goto_1
    array-length v1, v3

    if-ge v0, v1, :cond_1

    const/16 v1, 0xb6

    array-length v2, v3

    sub-int/2addr v2, v0

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/m2ts/c;->a:Lcom/google/android/apps/youtube/medialib/m2ts/d;

    invoke-static {v3, v0, v1}, Lcom/google/android/apps/youtube/common/e/a;->a([BII)[B

    move-result-object v1

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    invoke-direct {p0, v1, v4, v5, v6}, Lcom/google/android/apps/youtube/medialib/m2ts/c;->a([BJZ)[B

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/google/android/apps/youtube/medialib/m2ts/d;->a([B)V

    add-int/lit16 v0, v0, 0xb6

    goto :goto_1

    :cond_1
    return-void
.end method
