.class public Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Lcom/google/android/apps/youtube/medialib/player/y;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/medialib/player/d;

.field private final b:Landroid/view/View;

.field private final c:Ljava/lang/Runnable;

.field private final d:Ljava/lang/Runnable;

.field private e:Lcom/google/android/apps/youtube/medialib/player/aa;

.field private f:Lcom/google/android/apps/youtube/medialib/player/z;

.field private g:I

.field private h:I

.field private final i:Z

.field private j:I

.field private k:I

.field private l:I

.field private m:Z

.field private volatile n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/d;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/youtube/medialib/player/d;-><init>(Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/d;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->b:Landroid/view/View;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/medialib/player/a;-><init>(Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->c:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/medialib/player/b;-><init>(Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->d:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/d;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->addView(Landroid/view/View;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->i:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->k:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->b:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->l:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;)Lcom/google/android/apps/youtube/medialib/player/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/d;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;)Lcom/google/android/apps/youtube/medialib/player/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->e:Lcom/google/android/apps/youtube/medialib/player/aa;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->g:I

    return v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->h:I

    return v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->i:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->j:I

    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/medialib/player/n;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/d;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/medialib/player/n;->a(Landroid/view/SurfaceHolder;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->n:Z

    return-void
.end method

.method public final a(Lcom/google/android/exoplayer/d;Lcom/google/android/exoplayer/ak;)V
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/d;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/medialib/player/d;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    invoke-interface {p1, p2, v0, v1}, Lcom/google/android/exoplayer/d;->a(Lcom/google/android/exoplayer/e;ILjava/lang/Object;)V

    return-void
.end method

.method public final a(Z)Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->i:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->k:I

    return v0
.end method

.method public final b(Lcom/google/android/apps/youtube/medialib/player/n;)V
    .locals 1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/medialib/player/n;->a(Landroid/view/SurfaceHolder;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->n:Z

    return-void
.end method

.method public final b(Lcom/google/android/exoplayer/d;Lcom/google/android/exoplayer/ak;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-interface {p1, p2, v0, v1}, Lcom/google/android/exoplayer/d;->b(Lcom/google/android/exoplayer/e;ILjava/lang/Object;)V

    return-void
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->l:I

    return v0
.end method

.method public final d()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->n:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->c:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/medialib/player/c;-><init>(Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final g()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/d;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    :cond_0
    return-void
.end method

.method public final h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->m:Z

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/d;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/medialib/player/d;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/d;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/medialib/player/d;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/google/android/apps/youtube/medialib/player/d;->layout(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    const/high16 v4, 0x40000000    # 2.0f

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/d;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/medialib/player/d;->measure(II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/d;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/d;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/medialib/player/d;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->b:Landroid/view/View;

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->setMeasuredDimension(II)V

    return-void
.end method

.method public setListener(Lcom/google/android/apps/youtube/medialib/player/z;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->f:Lcom/google/android/apps/youtube/medialib/player/z;

    return-void
.end method

.method public setOnLetterboxChangedListener(Lcom/google/android/apps/youtube/medialib/player/aa;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->e:Lcom/google/android/apps/youtube/medialib/player/aa;

    return-void
.end method

.method public setVideoSize(II)V
    .locals 1

    iput p1, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->g:I

    iput p2, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->h:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/d;->requestLayout()V

    return-void
.end method

.method public setZoom(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->j:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->j:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->requestLayout()V

    :cond_0
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->f:Lcom/google/android/apps/youtube/medialib/player/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->f:Lcom/google/android/apps/youtube/medialib/player/z;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/z;->b()V

    :cond_0
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->m:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->f:Lcom/google/android/apps/youtube/medialib/player/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->f:Lcom/google/android/apps/youtube/medialib/player/z;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/z;->a()V

    :cond_0
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->m:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->f:Lcom/google/android/apps/youtube/medialib/player/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->f:Lcom/google/android/apps/youtube/medialib/player/z;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/z;->c()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;->e()V

    return-void
.end method
