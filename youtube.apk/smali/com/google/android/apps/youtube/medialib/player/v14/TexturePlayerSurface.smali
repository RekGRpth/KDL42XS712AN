.class public Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements Lcom/google/android/apps/youtube/medialib/player/y;


# instance fields
.field private a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

.field private final b:Ljava/lang/Runnable;

.field private final c:Ljava/lang/Runnable;

.field private d:Lcom/google/android/apps/youtube/medialib/player/z;

.field private e:Landroid/view/Surface;

.field private f:I

.field private g:I

.field private volatile h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a()V

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/v14/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/medialib/player/v14/a;-><init>(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->b:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/v14/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/medialib/player/v14/b;-><init>(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->c:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->addView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;)Lcom/google/android/apps/youtube/medialib/player/v14/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    return-object v0
.end method

.method private a()V
    .locals 4

    const/4 v3, -0x2

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/v14/d;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/medialib/player/v14/d;-><init>(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->setPivotX(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->setPivotY(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    const v1, 0x3f800054    # 1.00001f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->setScaleX(F)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->f:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->g:I

    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/medialib/player/n;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->e:Landroid/view/Surface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MediaPlayer should only be attached after Surface has been created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->e:Landroid/view/Surface;

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/medialib/player/n;->a(Landroid/view/Surface;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->h:Z

    return-void
.end method

.method public final a(Lcom/google/android/exoplayer/d;Lcom/google/android/exoplayer/ak;)V
    .locals 2

    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    const/4 v1, 0x1

    invoke-interface {p1, p2, v1, v0}, Lcom/google/android/exoplayer/d;->a(Lcom/google/android/exoplayer/e;ILjava/lang/Object;)V

    return-void
.end method

.method public final a(Z)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->getMeasuredWidth()I

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/apps/youtube/medialib/player/n;)V
    .locals 1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/medialib/player/n;->a(Landroid/view/Surface;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->h:Z

    return-void
.end method

.method public final b(Lcom/google/android/exoplayer/d;Lcom/google/android/exoplayer/ak;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-interface {p1, p2, v0, v1}, Lcom/google/android/exoplayer/d;->b(Lcom/google/android/exoplayer/e;ILjava/lang/Object;)V

    return-void
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->c:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->b:Ljava/lang/Runnable;

    const-wide/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->b:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->b:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->removeCallbacks(Ljava/lang/Runnable;)Z

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->c:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/v14/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/medialib/player/v14/c;-><init>(Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->e:Landroid/view/Surface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->e:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    :cond_0
    return-void
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->e:Landroid/view/Surface;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->measure(II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->setMeasuredDimension(II)V

    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    new-instance v0, Landroid/view/Surface;

    invoke-direct {v0, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->e:Landroid/view/Surface;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->d:Lcom/google/android/apps/youtube/medialib/player/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->d:Lcom/google/android/apps/youtube/medialib/player/z;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/z;->a()V

    :cond_0
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->e:Landroid/view/Surface;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->d:Lcom/google/android/apps/youtube/medialib/player/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->d:Lcom/google/android/apps/youtube/medialib/player/z;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/z;->c()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->b:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->removeCallbacks(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->d:Lcom/google/android/apps/youtube/medialib/player/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->d:Lcom/google/android/apps/youtube/medialib/player/z;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/z;->b()V

    :cond_0
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->d:Lcom/google/android/apps/youtube/medialib/player/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->d:Lcom/google/android/apps/youtube/medialib/player/z;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/z;->b()V

    :cond_0
    return-void
.end method

.method public setListener(Lcom/google/android/apps/youtube/medialib/player/z;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->d:Lcom/google/android/apps/youtube/medialib/player/z;

    return-void
.end method

.method public setOnLetterboxChangedListener(Lcom/google/android/apps/youtube/medialib/player/aa;)V
    .locals 0

    return-void
.end method

.method public setVideoSize(II)V
    .locals 1

    iput p1, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->f:I

    iput p2, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->g:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/v14/TexturePlayerSurface;->a:Lcom/google/android/apps/youtube/medialib/player/v14/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/v14/d;->requestLayout()V

    return-void
.end method

.method public setZoom(I)V
    .locals 0

    return-void
.end method
