.class public Lcom/google/android/apps/youtube/medialib/player/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

.field private final b:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

.field private final c:Z

.field private final d:Z

.field private final e:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;ZZI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/l;->a:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    iput-object p2, p0, Lcom/google/android/apps/youtube/medialib/player/l;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    iput-boolean p3, p0, Lcom/google/android/apps/youtube/medialib/player/l;->c:Z

    iput-boolean p4, p0, Lcom/google/android/apps/youtube/medialib/player/l;->d:Z

    iput p5, p0, Lcom/google/android/apps/youtube/medialib/player/l;->e:I

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/l;->a:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/l;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/l;->c:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/player/l;->d:Z

    return v0
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/player/l;->e:I

    return v0
.end method
