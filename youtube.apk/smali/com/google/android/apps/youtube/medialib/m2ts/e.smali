.class public final Lcom/google/android/apps/youtube/medialib/m2ts/e;
.super Lcom/google/android/exoplayer/ak;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/exoplayer/ai;

.field private final b:Lcom/google/android/exoplayer/l;

.field private final c:[Lcom/google/android/exoplayer/ah;

.field private d:I

.field private e:I

.field private f:Z

.field private g:J

.field private h:J

.field private i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

.field private j:Lcom/google/android/apps/youtube/medialib/m2ts/c;

.field private k:F


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/ai;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/exoplayer/ak;-><init>()V

    sget v0, Lcom/google/android/exoplayer/e/k;->a:I

    const/16 v3, 0x9

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->a:Lcom/google/android/exoplayer/ai;

    new-instance v0, Lcom/google/android/exoplayer/l;

    invoke-direct {v0}, Lcom/google/android/exoplayer/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->b:Lcom/google/android/exoplayer/l;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/exoplayer/ah;

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->c:[Lcom/google/android/exoplayer/ah;

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->c:[Lcom/google/android/exoplayer/ah;

    array-length v0, v0

    if-ge v2, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->c:[Lcom/google/android/exoplayer/ah;

    new-instance v3, Lcom/google/android/exoplayer/ah;

    invoke-direct {v3, v1}, Lcom/google/android/exoplayer/ah;-><init>(Z)V

    aput-object v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->k:F

    return-void
.end method

.method private l()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;->e()V

    :cond_0
    :try_start_0
    new-instance v0, Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;
    :try_end_0
    .catch Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer$InitException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v0, Lcom/google/android/apps/youtube/medialib/m2ts/c;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->b:Lcom/google/android/exoplayer/l;

    iget-object v2, v2, Lcom/google/android/exoplayer/l;->a:Lcom/google/android/exoplayer/ag;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/medialib/m2ts/c;-><init>(Lcom/google/android/apps/youtube/medialib/m2ts/d;Lcom/google/android/exoplayer/ag;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->j:Lcom/google/android/apps/youtube/medialib/m2ts/c;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->k:F

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;->a(F)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/m2ts/e;->p()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;->c()V

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "Couldn\'t create the native player."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    goto :goto_0
.end method

.method private m()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->d:I

    :goto_0
    const/4 v1, 0x7

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->c:[Lcom/google/android/exoplayer/ah;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/google/android/exoplayer/ah;->c:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->c:[Lcom/google/android/exoplayer/ah;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/google/android/exoplayer/ah;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->a:Lcom/google/android/exoplayer/ai;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ai;->a()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->a:Lcom/google/android/exoplayer/ai;

    invoke-interface {v2}, Lcom/google/android/exoplayer/ai;->b()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->a:Lcom/google/android/exoplayer/ai;

    invoke-interface {v2, v1}, Lcom/google/android/exoplayer/ai;->a(I)Lcom/google/android/exoplayer/aj;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/exoplayer/aj;->a:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/exoplayer/e/k;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->e:I

    const/4 v0, 0x1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/exoplayer/ExoPlaybackException;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer/ExoPlaybackException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 2

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->k:F

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;->a(F)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/google/android/exoplayer/ak;->a(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method protected final a(J)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->a:Lcom/google/android/exoplayer/ai;

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer/ai;->b(J)Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/m2ts/e;->m()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->f:Z

    iput-wide p1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->g:J

    iput-wide p1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->h:J

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/m2ts/e;->l()V

    return-void
.end method

.method protected final a(JZ)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->a:Lcom/google/android/exoplayer/ai;

    iget v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->e:I

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/exoplayer/ai;->a(IJ)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/m2ts/e;->m()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->f:Z

    iput-wide p1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->g:J

    iput-wide p1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->h:J

    return-void
.end method

.method protected final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->a:Lcom/google/android/exoplayer/ai;

    iget v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->e:I

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/ai;->b(I)V

    return-void
.end method

.method protected final b(J)V
    .locals 9

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->a:Lcom/google/android/exoplayer/ai;

    invoke-interface {v0, p1, p2}, Lcom/google/android/exoplayer/ai;->a(J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_1
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->f:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->h:J

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/m2ts/e;->d()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x4c4b40

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    move v0, v7

    :goto_0
    if-eqz v0, :cond_4

    :cond_2
    move v0, v6

    :goto_1
    if-nez v0, :cond_1

    return-void

    :cond_3
    move v0, v6

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->a:Lcom/google/android/exoplayer/ai;

    iget v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->e:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/m2ts/e;->d()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->b:Lcom/google/android/exoplayer/l;

    iget-object v5, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->c:[Lcom/google/android/exoplayer/ah;

    iget v8, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->d:I

    aget-object v5, v5, v8

    invoke-interface/range {v0 .. v5}, Lcom/google/android/exoplayer/ai;->a(IJLcom/google/android/exoplayer/l;Lcom/google/android/exoplayer/ah;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_2
    move v0, v7

    goto :goto_1

    :pswitch_0
    move v0, v6

    goto :goto_1

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/m2ts/e;->l()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/exoplayer/ExoPlaybackException;

    invoke-direct {v1, v0}, Lcom/google/android/exoplayer/ExoPlaybackException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :pswitch_2
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->f:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;->b()V

    move v0, v6

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->c:[Lcom/google/android/exoplayer/ah;

    iget v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->d:I

    aget-object v0, v0, v1

    iget-wide v0, v0, Lcom/google/android/exoplayer/ah;->f:J

    iput-wide v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->h:J

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->d:I

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->d:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->j:Lcom/google/android/apps/youtube/medialib/m2ts/c;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->c:[Lcom/google/android/exoplayer/ah;

    iget v2, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/medialib/m2ts/c;->a([Lcom/google/android/exoplayer/ah;I)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/m2ts/e;->m()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_5
    move v0, v7

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x4
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;->e()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->a:Lcom/google/android/exoplayer/ai;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ai;->d()V

    return-void
.end method

.method protected final d()J
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    invoke-static {}, Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;->f()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->g:J

    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->g:J

    return-wide v0
.end method

.method protected final e()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->a:Lcom/google/android/exoplayer/ai;

    iget v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->e:I

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/ai;->a(I)Lcom/google/android/exoplayer/aj;

    move-result-object v0

    iget-wide v0, v0, Lcom/google/android/exoplayer/aj;->b:J

    return-wide v0
.end method

.method protected final f()J
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->a:Lcom/google/android/exoplayer/ai;

    invoke-interface {v0}, Lcom/google/android/exoplayer/ai;->c()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    const-wide/16 v2, -0x3

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/m2ts/e;->d()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method protected final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;->c()V

    :cond_0
    return-void
.end method

.method protected final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;->d()V

    :cond_0
    return-void
.end method

.method protected final i()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;->g()Z

    move-result v0

    goto :goto_0
.end method

.method protected final j()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/e;->i:Lcom/google/android/apps/youtube/medialib/m2ts/NativeM2TSPlayer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final k()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
