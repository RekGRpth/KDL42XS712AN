.class final Lcom/google/android/apps/youtube/medialib/player/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/af;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/medialib/player/i;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/medialib/player/i;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/medialib/player/i;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/medialib/player/j;-><init>(Lcom/google/android/apps/youtube/medialib/player/i;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/i;->d(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/apps/youtube/medialib/player/y;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/i;->d(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/apps/youtube/medialib/player/y;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->d()V

    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/i;->h(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->d(I)V

    return-void
.end method

.method public final a(II)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/i;->a(Lcom/google/android/apps/youtube/medialib/player/i;I)I

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/medialib/player/i;->b(Lcom/google/android/apps/youtube/medialib/player/i;I)I

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/i;->d(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/apps/youtube/medialib/player/y;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/i;->d(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/apps/youtube/medialib/player/y;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/y;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/i;->d(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/apps/youtube/medialib/player/y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v1}, Lcom/google/android/apps/youtube/medialib/player/i;->b(Lcom/google/android/apps/youtube/medialib/player/i;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v2}, Lcom/google/android/apps/youtube/medialib/player/i;->c(Lcom/google/android/apps/youtube/medialib/player/i;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/medialib/player/y;->setVideoSize(II)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/media/MediaCodec$CryptoException;)V
    .locals 1

    const-string v0, "CryptoError with ExoPlayer."

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/i;->h(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Lcom/google/android/exoplayer/DecoderInitializationException;)V
    .locals 1

    const-string v0, "Error with ExoPlayer decoder initialization."

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/medialib/player/i;->h(Lcom/google/android/apps/youtube/medialib/player/i;)Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Ljava/lang/Exception;)V

    return-void
.end method
