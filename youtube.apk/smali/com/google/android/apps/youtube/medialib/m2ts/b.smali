.class public final Lcom/google/android/apps/youtube/medialib/m2ts/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/medialib/player/x;
.implements Lcom/google/android/exoplayer/g;


# instance fields
.field a:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

.field private final b:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

.field private c:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

.field private d:Lcom/google/android/exoplayer/b/a/a;

.field private e:Lcom/google/android/exoplayer/d;

.field private f:Lcom/google/android/exoplayer/ak;

.field private g:Z

.field private final h:Lcom/google/android/apps/youtube/common/fromguava/e;

.field private i:F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/common/fromguava/e;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->h:Lcom/google/android/apps/youtube/common/fromguava/e;

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->b:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->i:F

    return-void
.end method

.method private a(Lcom/google/android/exoplayer/b/a/a;I)V
    .locals 11

    const/4 v2, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    invoke-interface {v0}, Lcom/google/android/exoplayer/d;->c()V

    :cond_0
    const/16 v0, 0x7d0

    invoke-static {v9, v0}, Lcom/google/android/exoplayer/f;->a(II)Lcom/google/android/exoplayer/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    invoke-interface {v0, p0}, Lcom/google/android/exoplayer/d;->a(Lcom/google/android/exoplayer/g;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    invoke-interface {v0, p2}, Lcom/google/android/exoplayer/d;->a(I)V

    iget-object v8, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->h:Lcom/google/android/apps/youtube/common/fromguava/e;

    iget-object v3, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->a:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    new-instance v0, Lcom/google/android/exoplayer/m;

    new-instance v1, Lcom/google/android/exoplayer/upstream/f;

    const v4, 0xa000

    invoke-direct {v1, v4}, Lcom/google/android/exoplayer/upstream/f;-><init>(I)V

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->lowWatermarkMs()I

    move-result v4

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->highWatermarkMs()I

    move-result v5

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->lowPoolLoad()F

    move-result v6

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->highPoolLoad()F

    move-result v7

    move-object v3, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/exoplayer/m;-><init>(Lcom/google/android/exoplayer/upstream/b;Landroid/os/Handler;Lcom/google/android/exoplayer/o;IIFF)V

    invoke-interface {v8}, Lcom/google/android/apps/youtube/common/fromguava/e;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer/upstream/i;

    new-instance v2, Lcom/google/android/exoplayer/b/a;

    new-instance v3, Lcom/google/android/exoplayer/a/s;

    invoke-direct {v3}, Lcom/google/android/exoplayer/a/s;-><init>()V

    new-array v4, v9, [Lcom/google/android/exoplayer/b/a/a;

    aput-object p1, v4, v10

    invoke-direct {v2, v1, v3, v4}, Lcom/google/android/exoplayer/b/a;-><init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/a/o;[Lcom/google/android/exoplayer/b/a/a;)V

    new-instance v1, Lcom/google/android/exoplayer/a/b;

    const/high16 v3, 0x500000

    invoke-direct {v1, v2, v0, v3, v10}, Lcom/google/android/exoplayer/a/b;-><init>(Lcom/google/android/exoplayer/a/l;Lcom/google/android/exoplayer/m;IZ)V

    new-instance v0, Lcom/google/android/apps/youtube/medialib/m2ts/e;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/medialib/m2ts/e;-><init>(Lcom/google/android/exoplayer/ai;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->f:Lcom/google/android/exoplayer/ak;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    new-array v1, v9, [Lcom/google/android/exoplayer/ak;

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->f:Lcom/google/android/exoplayer/ak;

    aput-object v2, v1, v10

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/d;->a([Lcom/google/android/exoplayer/ak;)V

    iget v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->i:F

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/medialib/m2ts/b;->a(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->b:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/m2ts/b;->b()V

    return-void
.end method

.method private m()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    invoke-interface {v0}, Lcom/google/android/exoplayer/d;->c()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    iput-object v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->f:Lcom/google/android/exoplayer/ak;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    return-object v0
.end method

.method public final a(F)V
    .locals 4

    iput p1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->i:F

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->f:Lcom/google/android/exoplayer/ak;

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/exoplayer/d;->a(Lcom/google/android/exoplayer/e;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->b:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->b(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    invoke-interface {v0, p1}, Lcom/google/android/exoplayer/d;->a(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->b:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->c(I)V

    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 0

    return-void
.end method

.method public final a(ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;ILjava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)V
    .locals 4

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->hasAudioOnlyStream()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isLive()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "No streaming data available for background playback."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    iput-object v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->d:Lcom/google/android/exoplayer/b/a/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->b:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    new-instance v1, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents$CantPlayStreamException;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents$CantPlayStreamException;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_1
    iput-object p5, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->a:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getAudioOnlyStream()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getRepresentation(Ljava/lang/String;)Lcom/google/android/exoplayer/b/a/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->d:Lcom/google/android/exoplayer/b/a/a;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->d:Lcom/google/android/exoplayer/b/a/a;

    iget-object v2, v2, Lcom/google/android/exoplayer/b/a/a;->k:Landroid/net/Uri;

    iget-object v3, v1, Lcom/google/android/exoplayer/b/a/a;->k:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    iput-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    iput-object v1, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->d:Lcom/google/android/exoplayer/b/a/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->b:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->d:Lcom/google/android/exoplayer/b/a/a;

    invoke-direct {p0, v0, p3}, Lcom/google/android/apps/youtube/medialib/m2ts/b;->a(Lcom/google/android/exoplayer/b/a/a;I)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/m2ts/b;->b()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p3}, Lcom/google/android/apps/youtube/medialib/m2ts/b;->a(I)V

    goto :goto_1
.end method

.method public final a(Landroid/os/Handler;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->b:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Landroid/os/Handler;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/medialib/player/y;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/exoplayer/ExoPlaybackException;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->b:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/m2ts/b;->m()V

    return-void
.end method

.method public final a(ZI)V
    .locals 2

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->g:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->b:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->g:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->b:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->b:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->d:Lcom/google/android/exoplayer/b/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->d:Lcom/google/android/exoplayer/b/a/a;

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/youtube/medialib/m2ts/b;->a(Lcom/google/android/exoplayer/b/a/a;I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->b:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    invoke-interface {v0}, Lcom/google/android/exoplayer/d;->a()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    invoke-interface {v0, v2}, Lcom/google/android/exoplayer/d;->a(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/d;->a(Z)V

    goto :goto_0
.end method

.method public final b(Landroid/os/Handler;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->b:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->b(Landroid/os/Handler;)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/d;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->b:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e()V

    return-void
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/medialib/m2ts/b;->m()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->b:Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents;->a(I)V

    :cond_0
    return-void
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    invoke-interface {v0}, Lcom/google/android/exoplayer/d;->e()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    invoke-interface {v0}, Lcom/google/android/exoplayer/d;->d()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    invoke-interface {v0}, Lcom/google/android/exoplayer/d;->f()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    invoke-interface {v0}, Lcom/google/android/exoplayer/d;->a()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    invoke-interface {v0}, Lcom/google/android/exoplayer/d;->a()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/medialib/m2ts/b;->e:Lcom/google/android/exoplayer/d;

    invoke-interface {v0}, Lcom/google/android/exoplayer/d;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()V
    .locals 0

    return-void
.end method

.method public final l()V
    .locals 0

    return-void
.end method
