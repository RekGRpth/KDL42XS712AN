.class public final Lcom/google/android/apps/youtube/core/player/overlay/bs;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/overlay/br;

.field private final b:Lcom/google/android/apps/youtube/core/client/bj;

.field private final c:Lcom/google/android/apps/youtube/common/a/b;

.field private final d:Z

.field private e:Lcom/google/android/apps/youtube/common/a/d;

.field private f:Landroid/net/Uri;

.field private g:Landroid/graphics/Bitmap;

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/br;Lcom/google/android/apps/youtube/core/client/bj;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/br;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->a:Lcom/google/android/apps/youtube/core/player/overlay/br;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->b:Lcom/google/android/apps/youtube/core/client/bj;

    iput-boolean p3, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->d:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/apps/youtube/core/player/overlay/bt;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/player/overlay/bt;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/bs;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->c:Lcom/google/android/apps/youtube/common/a/b;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/bs;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->g:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/bs;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->g:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private a()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->h:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->d:Z

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->g:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->f:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->e:Lcom/google/android/apps/youtube/common/a/d;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->c:Lcom/google/android/apps/youtube/common/a/b;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->e:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->b:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->f:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->e:Lcom/google/android/apps/youtube/common/a/d;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->a:Lcom/google/android/apps/youtube/core/player/overlay/br;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/br;->d()V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->a:Lcom/google/android/apps/youtube/core/player/overlay/br;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/br;->e()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/overlay/bs;)Lcom/google/android/apps/youtube/core/player/overlay/br;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->a:Lcom/google/android/apps/youtube/core/player/overlay/br;

    return-object v0
.end method


# virtual methods
.method public final handleAudioOnlyEvent(Lcom/google/android/apps/youtube/core/player/event/a;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/a;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->i:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bs;->a()V

    return-void
.end method

.method public final handleVideoStageEvent(Lcom/google/android/apps/youtube/core/player/event/ac;)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->NEW:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_2

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->f:Landroid/net/Uri;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->g:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->e:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->e:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->e:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->a:Lcom/google/android/apps/youtube/core/player/overlay/br;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/br;->f()V

    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->READY:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isIn([Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->h:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bs;->a()V

    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->a:Lcom/google/android/apps/youtube/core/player/overlay/br;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/br;->b()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v0

    move v1, v0

    :goto_1
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v0

    :goto_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getThumbnailDetails()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v2, v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;II)Lcom/google/android/apps/youtube/datalib/innertube/model/ao;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->a()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bs;->f:Landroid/net/Uri;

    goto :goto_0

    :cond_3
    const/16 v0, 0x1e0

    move v1, v0

    goto :goto_1

    :cond_4
    const/16 v0, 0x140

    goto :goto_2
.end method
