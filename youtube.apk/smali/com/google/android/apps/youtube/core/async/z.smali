.class final Lcom/google/android/apps/youtube/core/async/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/async/y;

.field private final b:Lcom/google/android/apps/youtube/common/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/async/y;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/async/z;->a:Lcom/google/android/apps/youtube/core/async/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/a/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/z;->b:Lcom/google/android/apps/youtube/common/a/b;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/z;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/z;->a:Lcom/google/android/apps/youtube/core/async/y;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/y;->b(Lcom/google/android/apps/youtube/core/async/y;)Ljava/util/concurrent/atomic/AtomicLong;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/z;->a:Lcom/google/android/apps/youtube/core/async/y;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/async/y;->a(Lcom/google/android/apps/youtube/core/async/y;)Lcom/google/android/apps/youtube/common/e/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/z;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
