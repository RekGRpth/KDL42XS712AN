.class final Lcom/google/android/apps/youtube/core/async/ao;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/async/an;

.field private final b:Lcom/google/android/apps/youtube/common/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/async/an;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/async/ao;->a:Lcom/google/android/apps/youtube/core/async/an;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/async/ao;->b:Lcom/google/android/apps/youtube/common/a/b;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/ao;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/ao;->a:Lcom/google/android/apps/youtube/core/async/an;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/an;->b(Lcom/google/android/apps/youtube/core/async/an;)Lcom/google/android/apps/youtube/common/cache/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/ao;->a:Lcom/google/android/apps/youtube/core/async/an;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/async/an;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/core/async/Timestamped;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/async/ao;->a:Lcom/google/android/apps/youtube/core/async/an;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/async/an;->a(Lcom/google/android/apps/youtube/core/async/an;)Lcom/google/android/apps/youtube/common/e/b;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v3

    invoke-direct {v2, p2, v3, v4}, Lcom/google/android/apps/youtube/core/async/Timestamped;-><init>(Ljava/lang/Object;J)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/common/cache/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/ao;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
