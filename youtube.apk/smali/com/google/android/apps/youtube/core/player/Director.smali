.class public final Lcom/google/android/apps/youtube/core/player/Director;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/client/bi;
.implements Lcom/google/android/apps/youtube/core/player/az;
.implements Lcom/google/android/apps/youtube/core/player/sequencer/r;
.implements Lcom/google/android/apps/youtube/core/player/y;


# instance fields
.field private A:Lcom/google/android/apps/youtube/common/a/d;

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

.field private F:Lcom/google/android/apps/youtube/core/player/r;

.field private G:Z

.field private H:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

.field private I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

.field private final J:Lcom/google/android/apps/youtube/core/player/n;

.field private K:Z

.field private L:Z

.field private final M:Lcom/google/android/apps/youtube/core/player/l;

.field private N:I

.field private O:Z

.field private P:I

.field private Q:I

.field private R:I

.field private S:Z

.field private T:Lcom/google/android/apps/youtube/medialib/player/y;

.field private U:Lcom/google/android/apps/youtube/common/a/b;

.field private final a:Landroid/os/Handler;

.field private final b:Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;

.field private final c:Lcom/google/android/apps/youtube/common/c/a;

.field private final d:Landroid/content/SharedPreferences;

.field private final e:Lcom/google/android/apps/youtube/medialib/player/x;

.field private final f:Landroid/content/Context;

.field private final g:Lcom/google/android/apps/youtube/core/Analytics;

.field private final h:Lcom/google/android/apps/youtube/common/e/b;

.field private final i:Lcom/google/android/apps/youtube/common/network/h;

.field private final j:Lcom/google/android/apps/youtube/core/player/w;

.field private final k:Lcom/google/android/apps/youtube/core/client/e;

.field private final l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

.field private final m:Lcom/google/android/apps/youtube/core/aw;

.field private n:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field private o:I

.field private p:I

.field private q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

.field private r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

.field private s:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

.field private t:I

.field private u:Z

.field private v:Z

.field private w:Lcom/google/android/apps/youtube/core/player/al;

.field private x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;

.field private y:Z

.field private final z:Lcom/google/android/apps/youtube/core/player/ad;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/medialib/player/x;Landroid/content/Context;Lcom/google/android/apps/youtube/common/c/a;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/core/client/e;Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->y:Z

    const/4 v1, 0x4

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->N:I

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/medialib/player/x;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->f:Landroid/content/Context;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->d:Landroid/content/SharedPreferences;

    iput-object p5, p0, Lcom/google/android/apps/youtube/core/player/Director;->k:Lcom/google/android/apps/youtube/core/client/e;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/player/ad;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->z:Lcom/google/android/apps/youtube/core/player/ad;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/player/w;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->j:Lcom/google/android/apps/youtube/core/player/w;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/Analytics;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->g:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/aw;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->m:Lcom/google/android/apps/youtube/core/aw;

    invoke-static/range {p11 .. p11}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->i:Lcom/google/android/apps/youtube/common/network/h;

    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$PlayerState;->NOT_PREPARED:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->H:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->NEW:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    sget-object v1, Lcom/google/android/apps/youtube/core/client/WatchFeature;->NO_FEATURE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->n:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {p2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/youtube/core/player/m;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/apps/youtube/core/player/m;-><init>(Lcom/google/android/apps/youtube/core/player/Director;B)V

    invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/common/e/n;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/common/e/n;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->h:Lcom/google/android/apps/youtube/common/e/b;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;

    new-instance v2, Lcom/google/android/apps/youtube/common/b/b;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/common/b/b;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/Director;->h:Lcom/google/android/apps/youtube/common/e/b;

    invoke-direct {v1, v2, p0, p9, v3}, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/player/az;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/common/e/b;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->b:Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;

    invoke-virtual {p6, p0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(Lcom/google/android/apps/youtube/core/client/bi;)V

    const-string v1, "default_hq"

    const/4 v2, 0x0

    invoke-interface {p4, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface/range {p11 .. p11}, Lcom/google/android/apps/youtube/common/network/h;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "youtube:device_lowend"

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-static {p2}, Lcom/google/android/apps/youtube/core/utils/Util;->c(Landroid/content/Context;)Z

    move-result v1

    :goto_0
    if-nez v1, :cond_1

    invoke-interface/range {p11 .. p11}, Lcom/google/android/apps/youtube/common/network/h;->b()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    iput v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->t:I

    new-instance v1, Lcom/google/android/apps/youtube/core/player/k;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/player/k;-><init>(Lcom/google/android/apps/youtube/core/player/Director;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->U:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->a:Landroid/os/Handler;

    invoke-interface {p1, v1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Landroid/os/Handler;)V

    new-instance v1, Lcom/google/android/apps/youtube/core/player/n;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/player/n;-><init>(Lcom/google/android/apps/youtube/core/player/Director;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->J:Lcom/google/android/apps/youtube/core/player/n;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->L:Z

    new-instance v1, Lcom/google/android/apps/youtube/core/player/l;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/core/player/l;-><init>(Lcom/google/android/apps/youtube/core/player/Director;B)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->M:Lcom/google/android/apps/youtube/core/player/l;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->M:Lcom/google/android/apps/youtube/core/player/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/l;->a()V

    return-void

    :pswitch_0
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private A()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/af;

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->N:I

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/core/player/event/af;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    return-void
.end method

.method private B()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/ab;

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->v:Z

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/core/player/Director;->y:Z

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/youtube/core/player/event/ab;-><init>(ZZ)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    return-void
.end method

.method private C()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isPlayingAd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/ae;

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->p:I

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getDuration()I

    move-result v3

    mul-int/lit16 v3, v3, 0x3e8

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/player/event/ae;-><init>(III)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/ae;

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->o:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->x()I

    move-result v3

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/player/event/ae;-><init>(III)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private D()V
    .locals 5

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_AD_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_AD:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_VIDEO_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_VIDEO:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->a([Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/ae;

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->P:I

    iget v3, p0, Lcom/google/android/apps/youtube/core/player/Director;->Q:I

    iget v4, p0, Lcom/google/android/apps/youtube/core/player/Director;->R:I

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/player/event/ae;-><init>(III)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Media progress reported outside media playback: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private E()V
    .locals 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->LOADING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->E:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    if-eqz v1, :cond_1

    :goto_1
    return-void

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->H:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/Director$PlayerState;->PREPARING:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    if-eq v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/medialib/player/x;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/medialib/player/x;->i()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->LOADING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/event/z;

    invoke-direct {v2, v0}, Lcom/google/android/apps/youtube/core/player/event/z;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->H()Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->ENDED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    goto :goto_2

    :cond_4
    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_AD:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_VIDEO:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/medialib/player/x;->j()Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    goto :goto_2

    :cond_6
    new-array v1, v5, [Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->READY:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v4, v1, v3

    sget-object v4, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->AD_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v4, v1, v2

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->a([Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v1

    if-nez v1, :cond_8

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->H()Z

    move-result v1

    if-nez v1, :cond_8

    :cond_7
    new-array v1, v5, [Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_AD:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v4, v1, v3

    sget-object v3, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_VIDEO:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v3, v1, v2

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->a([Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/medialib/player/x;->j()Z

    move-result v1

    if-nez v1, :cond_9

    :cond_8
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PAUSED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    goto :goto_2

    :cond_9
    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->NEW:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v1

    if-eqz v1, :cond_a

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->NEW:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    goto :goto_2

    :cond_a
    const-string v1, "Unhandled state in setControlsState."

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private F()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->w:Lcom/google/android/apps/youtube/core/player/al;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->w:Lcom/google/android/apps/youtube/core/player/al;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private G()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/a;

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->S:Z

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/core/player/event/a;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    return-void
.end method

.method private H()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isLive()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoStreamingData()Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private I()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/f;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/player/event/f;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->AD_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->J()V

    return-void
.end method

.method private J()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->M()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->D:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->C:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->C()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->E()V

    :goto_1
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->READY:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->g()V

    goto :goto_1
.end method

.method private K()V
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->C:Z

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->o:I

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_VIDEO_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    :cond_0
    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->s:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->n:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/Director;->z:Lcom/google/android/apps/youtube/core/player/ad;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/core/player/ad;->e()Z

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/Director;->z:Lcom/google/android/apps/youtube/core/player/ad;

    invoke-interface {v4}, Lcom/google/android/apps/youtube/core/player/ad;->f()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/Director;->w:Lcom/google/android/apps/youtube/core/player/al;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/Director;->z:Lcom/google/android/apps/youtube/core/player/ad;

    invoke-interface {v6}, Lcom/google/android/apps/youtube/core/player/ad;->h()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Lcom/google/android/apps/youtube/core/client/WatchFeature;ZZLcom/google/android/apps/youtube/core/player/al;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_VIDEO_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_VIDEO_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->C()V

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoStreamingData()Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isAudioOnly()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->p()V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->L()Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayerConfig(Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/event/n;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->isExoPlayerEnabled()Z

    move-result v3

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->isAdaptiveBitrateEnabled()Z

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/youtube/core/player/event/n;-><init>(ZZ)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    iget v6, p0, Lcom/google/android/apps/youtube/core/player/Director;->t:I

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoStreamingData()Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/youtube/core/player/Director;->o:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d()Ljava/lang/String;

    move-result-object v4

    move v1, v6

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/youtube/medialib/player/x;->a(ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;ILjava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)V

    return-void
.end method

.method private L()Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->a(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->getDefaultValue(Z)Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->d:Landroid/content/SharedPreferences;

    const-string v2, "ExoPlayer"

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private M()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->h:Lcom/google/android/apps/youtube/common/e/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldPlayAd(Lcom/google/android/apps/youtube/common/e/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private N()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->A:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->A:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->A:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    return-void
.end method

.method private O()Lcom/google/android/apps/youtube/core/player/al;
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->S:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->K:Z

    if-eqz v0, :cond_2

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/core/player/al;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/player/al;-><init>(I)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->w:Lcom/google/android/apps/youtube/core/player/al;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/al;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->w:Lcom/google/android/apps/youtube/core/player/al;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->F()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->w:Lcom/google/android/apps/youtube/core/player/al;

    return-object v0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->v:Z

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/apps/youtube/core/player/al;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/player/al;-><init>(I)V

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->u:Z

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/android/apps/youtube/core/player/al;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/player/al;-><init>(I)V

    goto :goto_0

    :cond_4
    new-instance v0, Lcom/google/android/apps/youtube/core/player/al;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/player/al;-><init>(I)V

    goto :goto_0
.end method

.method private P()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isPlayingAd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->f()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->M()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->p:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Q()Lcom/google/android/apps/youtube/core/player/r;
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->y()Z

    move-result v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    new-instance v4, Lcom/google/android/apps/youtube/core/player/r;

    if-nez v3, :cond_1

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->w()I

    move-result v2

    invoke-direct {v4, v1, v0, v2}, Lcom/google/android/apps/youtube/core/player/r;-><init>(ZZI)V

    return-object v4

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/Director;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/core/player/Director;->N:I

    return p1
.end method

.method private a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/b;
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->A:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->a:Landroid/os/Handler;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/Director;Lcom/google/android/apps/youtube/core/player/Director$PlayerState;)Lcom/google/android/apps/youtube/core/player/Director$PlayerState;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/Director;->H:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/Director;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->I()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/Director;III)V
    .locals 3

    iput p1, p0, Lcom/google/android/apps/youtube/core/player/Director;->P:I

    iput p2, p0, Lcom/google/android/apps/youtube/core/player/Director;->Q:I

    iput p3, p0, Lcom/google/android/apps/youtube/core/player/Director;->R:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(J)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->D()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/Director;Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/Director;Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/Director;Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;)V
    .locals 4

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;->getAdBreak()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->I()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;->getAdBreak()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->s:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;->getAd()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isForecastingAd()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->s:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->w:Lcom/google/android/apps/youtube/core/player/al;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/core/player/al;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->s:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/Director;->w:Lcom/google/android/apps/youtube/core/player/al;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Lcom/google/android/apps/youtube/core/player/al;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->I()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/Director;Ljava/lang/Object;II)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isPlayingAd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->g:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "AdPlayError"

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->P()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PlaybackMediaError - "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->w()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->a(Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    const-string v0, "PlaybackMediaError - MediaPlayerWrapper: what %d extra %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->w()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/Director;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/core/player/Director;->a(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/Director;Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->h(Z)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/core/player/r;Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;I)V
    .locals 1

    iget-boolean v0, p1, Lcom/google/android/apps/youtube/core/player/r;->a:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->C:Z

    iget-boolean v0, p1, Lcom/google/android/apps/youtube/core/player/r;->b:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->D:Z

    iget v0, p1, Lcom/google/android/apps/youtube/core/player/r;->c:I

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->o:I

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/Director;->s:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iput p4, p0, Lcom/google/android/apps/youtube/core/player/Director;->p:I

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSurvey()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isForecastingAd()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->k:Lcom/google/android/apps/youtube/core/client/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/client/e;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->s:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->I()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoStreamingData()Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->j()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->s:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->I()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;I)V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/medialib/player/x;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getHeight()I

    move-result v0

    :cond_0
    const/4 v1, -0x1

    if-eq p2, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->g:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-interface {v1, p1, v0, p2}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;II)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->g:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-interface {v1, p1, v0}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private varargs a([Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isIn([Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/Director;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/core/player/Director;->o:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/Director$VideoStage;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/Director;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/Director;->g(Z)V

    return-void
.end method

.method private b(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/Director;->E:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->AD_LOADING:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->E()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->E:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->E:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/Director$PlayerState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->H:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    return-object v0
.end method

.method private c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "VideoStage: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->z()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/common/c/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/core/player/Director;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->p:I

    return v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/core/player/Director;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->o:I

    return v0
.end method

.method private g(Z)V
    .locals 2

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->k:Lcom/google/android/apps/youtube/core/client/e;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/client/e;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->i()V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->READY:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->F:Lcom/google/android/apps/youtube/core/player/r;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->b:Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->b()V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->K()V

    goto :goto_0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->J:Lcom/google/android/apps/youtube/core/player/n;

    return-object v0
.end method

.method private h(Z)V
    .locals 4

    const/4 v1, 0x1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->t:I

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->t:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->g:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v2, "streamSelectionHint"

    iget v3, p0, Lcom/google/android/apps/youtube/core/player/Director;->t:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->READY:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->t:I

    if-eqz p1, :cond_3

    const/4 v1, 0x2

    :cond_3
    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(II)V

    goto :goto_1
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/core/player/Director;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->E()V

    return-void
.end method

.method private i(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/Director;->K:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->O()Lcom/google/android/apps/youtube/core/player/al;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->w:Lcom/google/android/apps/youtube/core/player/al;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->b(Lcom/google/android/apps/youtube/core/player/al;)V

    return-void
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/core/player/Director;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->t:I

    return v0
.end method

.method static synthetic k(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/medialib/player/x;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/apps/youtube/core/player/Director;)V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->E()V

    return-void
.end method

.method static synthetic m(Lcom/google/android/apps/youtube/core/player/Director;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->K:Z

    return v0
.end method

.method static synthetic n(Lcom/google/android/apps/youtube/core/player/Director;)I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->P()I

    move-result v0

    return v0
.end method

.method static synthetic o(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->g:Lcom/google/android/apps/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic p(Lcom/google/android/apps/youtube/core/player/Director;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->A()V

    return-void
.end method

.method static synthetic q(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/common/network/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->i:Lcom/google/android/apps/youtube/common/network/h;

    return-object v0
.end method

.method static synthetic r(Lcom/google/android/apps/youtube/core/player/Director;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->f:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic s(Lcom/google/android/apps/youtube/core/player/Director;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->C:Z

    return v0
.end method

.method static synthetic t(Lcom/google/android/apps/youtube/core/player/Director;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->L:Z

    return v0
.end method

.method private z()V
    .locals 7

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->onAd()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->c()Ljava/lang/String;

    move-result-object v3

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->onAd()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->H()Z

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/ac;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/event/ac;-><init>(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Z)V

    invoke-virtual {v6, v0}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    return-void

    :cond_0
    move-object v2, v0

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_2
    move-object v4, v0

    goto :goto_2

    :cond_3
    move-object v3, v0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v3, Lcom/google/android/apps/youtube/core/player/event/i;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/core/player/event/i;-><init>()V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->B:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->k:Lcom/google/android/apps/youtube/core/client/e;

    if-eqz v2, :cond_0

    move v3, v0

    :goto_0
    if-eqz v3, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVmapXml()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getVideo()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/Director;->f:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isMonetized(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v0

    :goto_1
    if-eqz v3, :cond_2

    if-nez v2, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getVideo()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/Director;->f:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isMonetized(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    :goto_2
    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->k:Lcom/google/android/apps/youtube/core/client/e;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->t:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/client/e;->a(I)V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->AD_LOADING:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->k:Lcom/google/android/apps/youtube/core/client/e;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/Director;->U:Lcom/google/android/apps/youtube/common/a/b;

    invoke-direct {p0, v3}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/b;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/e;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_3
    return-void

    :cond_0
    move v3, v1

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->k:Lcom/google/android/apps/youtube/core/client/e;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->t:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/client/e;->a(I)V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->AD_LOADING:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->k:Lcom/google/android/apps/youtube/core/client/e;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/Director;->U:Lcom/google/android/apps/youtube/common/a/b;

    invoke-direct {p0, v3}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/b;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/e;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V

    goto :goto_3

    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->J()V

    goto :goto_3
.end method

.method public final a(I)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_VIDEO_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    :cond_0
    iput p1, p0, Lcom/google/android/apps/youtube/core/player/Director;->o:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isPlayingVideo()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->H:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$PlayerState;->NOT_PREPARED:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(I)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->C()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/client/WatchFeature;ZI)V
    .locals 3

    const/4 v1, 0x0

    if-ltz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "startPosition has to be >= 0"

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->B:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/Director;->n:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v0, "PlaybackState reset by init"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/youtube/core/player/Director;->a(ZI)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/DirectorSavedState;)V
    .locals 5

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/apps/youtube/core/player/Director;->B:Z

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->feature:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->n:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->adBreak:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->ad:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->ad:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->h:Lcom/google/android/apps/youtube/common/e/b;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->hasExpired(Lcom/google/android/apps/youtube/common/e/b;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->ad:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    :goto_0
    iget-object v2, p1, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->playbackClientManagerState:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iget-object v3, p1, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->playbackClientManagerState:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    invoke-virtual {v2, v3, v1, v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V

    :cond_0
    const-string v2, "PlaybackState set by initFromState"

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->contentVideoState:Lcom/google/android/apps/youtube/core/player/r;

    iget v3, p1, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->adStartPositionMillis:I

    invoke-direct {p0, v2, v1, v0, v3}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/r;Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-gt v0, v1, :cond_1

    iput-boolean v4, p0, Lcom/google/android/apps/youtube/core/player/Director;->C:Z

    :cond_1
    iget v0, p1, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->streamSelectionHint:I

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->t:I

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->playbackPair:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    if-nez v0, :cond_4

    iget-boolean v0, p1, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->userInitiatedPlayback:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->NAVIGATION:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->e()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->f()V

    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    iget-boolean v0, p1, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;->userInitiatedPlayback:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->PLAYER_CONTROL:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->x:Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->L()Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayerConfig(Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->C()V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->j:Lcom/google/android/apps/youtube/core/player/w;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->K:Z

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/android/apps/youtube/core/player/w;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/w;Lcom/google/android/apps/youtube/core/player/y;Z)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/medialib/player/y;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/Director;->T:Lcom/google/android/apps/youtube/medialib/player/y;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->i(Z)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->S:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Lcom/google/android/apps/youtube/medialib/player/y;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 5

    const/4 v4, 0x0

    instance-of v0, p1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->k()V

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    :cond_0
    return-void

    :cond_1
    instance-of v0, p1, Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->isLocal()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->LICENSE_SERVER_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->f:Landroid/content/Context;

    sget v3, Lcom/google/android/youtube/p;->br:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2, p1}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;-><init>(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;ZLjava/lang/String;Ljava/lang/Throwable;)V

    move-object p1, v0

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->UNKNOWN:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    invoke-direct {v0, v1, v4, p1}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;-><init>(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;ZLjava/lang/Throwable;)V

    const-string v1, "Unexpected exception received"

    invoke-static {v1, p1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object p1, v0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->k()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->N()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->i()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->a:Landroid/os/Handler;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/x;->b(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->J:Lcom/google/android/apps/youtube/core/player/n;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/n;->d()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->M:Lcom/google/android/apps/youtube/core/player/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/l;->b()V

    return-void
.end method

.method public final a(ZI)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/Director;->C:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->D:Z

    iput p2, p0, Lcom/google/android/apps/youtube/core/player/Director;->o:I

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->s:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->p:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->e()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->n()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->f()V

    return-void
.end method

.method final a(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/c;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/player/event/c;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    return-void
.end method

.method public final b(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->w()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->a(I)V

    return-void
.end method

.method public final b(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->u:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/Director;->u:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->O()Lcom/google/android/apps/youtube/core/player/al;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->w:Lcom/google/android/apps/youtube/core/player/al;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->b(Lcom/google/android/apps/youtube/core/player/al;)V

    :cond_0
    return-void
.end method

.method final b(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c()Lcom/google/android/apps/youtube/core/player/au;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->b:Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;

    return-object v0
.end method

.method public final c(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->v:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/Director;->v:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->O()Lcom/google/android/apps/youtube/core/player/al;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->w:Lcom/google/android/apps/youtube/core/player/al;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->b(Lcom/google/android/apps/youtube/core/player/al;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->B()V

    :cond_0
    return-void
.end method

.method public final d()Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    return-object v0
.end method

.method public final d(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->y:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/Director;->y:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->B()V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->z()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->A()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->B()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/aa;

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->O:Z

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/core/player/event/aa;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->C:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->C()V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->E()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->F()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->G()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->D()V

    goto :goto_0
.end method

.method public final e(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/Director;->L:Z

    return-void
.end method

.method public final f(Z)Lcom/google/android/apps/youtube/core/player/DirectorSavedState;
    .locals 13

    const/4 v0, 0x0

    const/4 v11, 0x0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isPlayingAd()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v11

    :cond_0
    if-eqz p1, :cond_1

    move v9, v0

    move v8, v0

    move-object v10, v11

    move-object v2, v11

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->z:Lcom/google/android/apps/youtube/core/player/ad;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->z:Lcom/google/android/apps/youtube/core/player/ad;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/player/ad;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->z:Lcom/google/android/apps/youtube/core/player/ad;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/ad;->f()Z

    move-result v0

    move v7, v0

    :goto_2
    new-instance v0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->Q()Lcom/google/android/apps/youtube/core/player/r;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/Director;->n:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget v5, p0, Lcom/google/android/apps/youtube/core/player/Director;->t:I

    iget-boolean v6, p0, Lcom/google/android/apps/youtube/core/player/Director;->y:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->P()I

    move-result v12

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;-><init>(Lcom/google/android/apps/youtube/core/player/r;Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Lcom/google/android/apps/youtube/core/client/WatchFeature;IZZZZLcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;I)V

    move-object v11, v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a()Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;

    move-result-object v2

    iget-object v10, p0, Lcom/google/android/apps/youtube/core/player/Director;->s:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    iget-object v11, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-boolean v8, p0, Lcom/google/android/apps/youtube/core/player/Director;->K:Z

    iget-boolean v9, p0, Lcom/google/android/apps/youtube/core/player/Director;->S:Z

    goto :goto_1

    :cond_2
    move v7, v0

    goto :goto_2
.end method

.method final f()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->N()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->b:Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->b:Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->a()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->F:Lcom/google/android/apps/youtube/core/player/r;

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$PlayerState;->NOT_PREPARED:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->H:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->NEW:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->NEW:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    :cond_0
    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->E:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->O:Z

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->P:I

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->Q:I

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->R:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->k()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->d()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->O()Lcom/google/android/apps/youtube/core/player/al;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->w:Lcom/google/android/apps/youtube/core/player/al;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(Lcom/google/android/apps/youtube/core/player/al;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->C()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->A()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->E()V

    return-void
.end method

.method public final g()V
    .locals 6

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->AD_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "play() called when the player wasn\'t loaded."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->C:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->G:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->h:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoStreamingData()Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isExpired(J)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->f()V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v4, Lcom/google/android/apps/youtube/core/player/event/w;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getExpiredForSeconds(J)I

    move-result v0

    invoke-direct {v4, v0}, Lcom/google/android/apps/youtube/core/player/event/w;-><init>(I)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->H:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$PlayerState;->PREPARED:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    if-ne v0, v1, :cond_4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_VIDEO:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->b()V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->M()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSurvey()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->n:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/Director;->w:Lcom/google/android/apps/youtube/core/player/al;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/player/al;)V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_AD_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->C()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPlayerConfig()Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/n;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->isExoPlayerEnabled()Z

    move-result v2

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->isAdaptiveBitrateEnabled()Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/youtube/core/player/event/n;-><init>(ZZ)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->t:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->r:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoStreamingData()Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/youtube/core/player/Director;->p:I

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/youtube/medialib/player/x;->a(ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;ILjava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->b:Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->F:Lcom/google/android/apps/youtube/core/player/r;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->K()V

    goto/16 :goto_0
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->g:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "Replay"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->C:Z

    if-nez v0, :cond_0

    const-string v0, "PlayStarted"

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->a(Ljava/lang/String;I)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->g()V

    return-void
.end method

.method public final i()Z
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_AD_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_AD:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->a([Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    return v0
.end method

.method public final j()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->c()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->P()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->p:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->w()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->o:I

    :cond_0
    return-void
.end method

.method public final k()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->P()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->p:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->w()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->o:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->C:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_AD_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->AD_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_VIDEO_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->READY:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    goto :goto_0
.end method

.method public final l()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->Q()Lcom/google/android/apps/youtube/core/player/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->F:Lcom/google/android/apps/youtube/core/player/r;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->G:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->j()V

    return-void
.end method

.method public final m()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->F:Lcom/google/android/apps/youtube/core/player/r;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v2, v2, v1}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/r;Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;I)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->F:Lcom/google/android/apps/youtube/core/player/r;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->D:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->C:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->G:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$PlayerState;->NOT_PREPARED:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->H:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->K()V

    :cond_1
    return-void

    :cond_2
    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->READY:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    goto :goto_0
.end method

.method public final n()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->NEW:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->i()V

    return-void
.end method

.method public final o()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->h(Z)V

    return-void
.end method

.method public final p()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->S:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->l()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->S:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->O()Lcom/google/android/apps/youtube/core/player/al;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->G()V

    :cond_0
    return-void
.end method

.method public final q()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->S:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->T:Lcom/google/android/apps/youtube/medialib/player/y;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Director;->T:Lcom/google/android/apps/youtube/medialib/player/y;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Lcom/google/android/apps/youtube/medialib/player/y;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->S:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->O()Lcom/google/android/apps/youtube/core/player/al;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/Director;->G()V

    goto :goto_0

    :cond_1
    const-string v0, "Error: no UI elements available to display video"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->g:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "NoUiToDisplayVideo"

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->w()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public final r()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->k()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->g(Z)V

    return-void
.end method

.method final s()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->l:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->h()V

    return-void
.end method

.method public final t()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->T:Lcom/google/android/apps/youtube/medialib/player/y;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->i(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->l()V

    return-void
.end method

.method public final u()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->K:Z

    return v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final w()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isPlayingVideo()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->C:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->f()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director;->x()I

    move-result v0

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->o:I

    goto :goto_0
.end method

.method public final x()I
    .locals 2

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_VIDEO:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->H:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$PlayerState;->PREPARED:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->g()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Director;->q:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getLengthSeconds()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->I:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/medialib/player/x;->j()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->H:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/Director$PlayerState;->PREPARING:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    if-ne v2, v3, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/Director;->C:Z

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0
.end method
