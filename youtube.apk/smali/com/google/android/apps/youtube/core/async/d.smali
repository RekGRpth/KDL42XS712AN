.class public final Lcom/google/android/apps/youtube/core/async/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/async/af;

.field private final b:Lcom/google/android/apps/youtube/core/identity/b;

.field private final c:Lcom/google/android/apps/youtube/core/async/f;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/identity/b;Lcom/google/android/apps/youtube/core/async/f;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/async/d;->a:Lcom/google/android/apps/youtube/core/async/af;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/async/d;->b:Lcom/google/android/apps/youtube/core/identity/b;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/async/d;->c:Lcom/google/android/apps/youtube/core/async/f;

    return-void
.end method

.method public static a(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/identity/b;Lcom/google/android/apps/youtube/core/async/f;)Lcom/google/android/apps/youtube/core/async/d;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/core/async/d;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/youtube/core/async/d;-><init>(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/identity/b;Lcom/google/android/apps/youtube/core/async/f;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/async/d;)Lcom/google/android/apps/youtube/core/async/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/d;->c:Lcom/google/android/apps/youtube/core/async/f;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/async/d;)Lcom/google/android/apps/youtube/core/identity/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/d;->b:Lcom/google/android/apps/youtube/core/identity/b;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/async/d;)Lcom/google/android/apps/youtube/core/async/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/d;->a:Lcom/google/android/apps/youtube/core/async/af;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/d;->a:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v1, Lcom/google/android/apps/youtube/core/async/e;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/youtube/core/async/e;-><init>(Lcom/google/android/apps/youtube/core/async/d;Lcom/google/android/apps/youtube/common/a/b;Ljava/lang/Object;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
