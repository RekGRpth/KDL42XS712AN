.class final Lcom/google/android/apps/youtube/core/player/sequencer/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/sequencer/h;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/h;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/h;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/sequencer/i;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/h;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->a(Lcom/google/android/apps/youtube/core/player/sequencer/h;Lcom/google/android/apps/youtube/common/a/d;)Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->REQUEST_FAILED:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    iget-object v4, v4, Lcom/google/android/apps/youtube/core/player/sequencer/h;->e:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v4, p2}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, p2}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;-><init>(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;ZLjava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->a(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    iget v1, v1, Lcom/google/android/apps/youtube/core/player/sequencer/h;->q:I

    iput v1, v0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->p:I

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    const/4 v6, 0x0

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    invoke-static {v0, v6}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->a(Lcom/google/android/apps/youtube/core/player/sequencer/h;Lcom/google/android/apps/youtube/common/a/d;)Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->l:[Ljava/lang/String;

    array-length v1, v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->l:[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    iget-object v3, v3, Lcom/google/android/apps/youtube/core/player/sequencer/h;->l:[Ljava/lang/String;

    array-length v3, v3

    iget-object v4, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v0, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/apps/youtube/core/player/sequencer/h;->l:[Ljava/lang/String;

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v3, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    iget-object v3, v3, Lcom/google/android/apps/youtube/core/player/sequencer/h;->d:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v4, "PlaybackError - GData: NoVideoIdPage"

    iget-object v5, p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    iget-object v3, v3, Lcom/google/android/apps/youtube/core/player/sequencer/h;->l:[Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    aput-object v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->nextUri:Landroid/net/Uri;

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    invoke-static {v0, v6}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->a(Lcom/google/android/apps/youtube/core/player/sequencer/h;Lcom/google/android/apps/youtube/core/async/GDataRequest;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->q:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/player/sequencer/h;->l:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->a(Lcom/google/android/apps/youtube/core/player/sequencer/h;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->b(Lcom/google/android/apps/youtube/core/player/sequencer/h;)V

    :goto_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->nextUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->a(Lcom/google/android/apps/youtube/core/player/sequencer/h;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/core/async/GDataRequest;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->a(Lcom/google/android/apps/youtube/core/player/sequencer/h;Lcom/google/android/apps/youtube/core/async/GDataRequest;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/i;->a:Lcom/google/android/apps/youtube/core/player/sequencer/h;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->c(Lcom/google/android/apps/youtube/core/player/sequencer/h;)V

    goto :goto_2
.end method
