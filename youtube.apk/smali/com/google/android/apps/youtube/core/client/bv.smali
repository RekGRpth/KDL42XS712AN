.class public final Lcom/google/android/apps/youtube/core/client/bv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/identity/l;

.field private final b:Lcom/google/android/apps/youtube/core/offline/store/q;

.field private final c:Lcom/google/android/apps/youtube/common/e/b;

.field private final d:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bv;->a:Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bv;->b:Lcom/google/android/apps/youtube/core/offline/store/q;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bv;->c:Lcom/google/android/apps/youtube/common/e/b;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/bv;->d:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bv;->a:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/q;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bv;->b:Lcom/google/android/apps/youtube/core/offline/store/q;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bv;->c:Lcom/google/android/apps/youtube/common/e/b;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/bv;->d:Z

    return-void
.end method

.method public static a()Lcom/google/android/apps/youtube/core/client/bv;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/core/client/bv;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/client/bv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/bu;
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/bv;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/core/client/bu;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/bv;->a:Lcom/google/android/apps/youtube/core/identity/l;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/bv;->b:Lcom/google/android/apps/youtube/core/offline/store/q;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/bv;->c:Lcom/google/android/apps/youtube/common/e/b;

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/google/android/apps/youtube/core/client/bu;-><init>(Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/common/e/b;Ljava/lang/String;)V

    goto :goto_0
.end method
