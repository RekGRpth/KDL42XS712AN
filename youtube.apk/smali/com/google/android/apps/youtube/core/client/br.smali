.class public final Lcom/google/android/apps/youtube/core/client/br;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/medialib/a/b;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/apps/youtube/common/e/b;

.field private final d:Lcom/google/android/apps/youtube/medialib/a/a;

.field private final e:Lcom/google/android/apps/youtube/core/utils/o;

.field private f:J

.field private g:J

.field private h:I

.field private i:J

.field private j:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/medialib/a/a;Lcom/google/android/apps/youtube/core/utils/o;Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/br;->i:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/br;->j:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/br;->b:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/client/br;->c:Lcom/google/android/apps/youtube/common/e/b;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/client/br;->d:Lcom/google/android/apps/youtube/medialib/a/a;

    iput-object p4, p0, Lcom/google/android/apps/youtube/core/client/br;->e:Lcom/google/android/apps/youtube/core/utils/o;

    iput-object p5, p0, Lcom/google/android/apps/youtube/core/client/br;->a:Ljava/lang/String;

    invoke-interface {p2}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x7530

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/br;->f:J

    return-void
.end method

.method private b(Z)V
    .locals 11

    const-wide/16 v9, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/br;->c:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v7

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/client/br;->g:J

    cmp-long v0, v0, v9

    if-lez v0, :cond_2

    if-nez p1, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/client/br;->f:J

    cmp-long v0, v7, v0

    if-lez v0, :cond_2

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/core/client/br;->h:I

    const/16 v1, 0x7d0

    if-le v0, v1, :cond_1

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const-string v0, "cpn"

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/br;->a:Ljava/lang/String;

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "bytes_transferred"

    iget-wide v3, p0, Lcom/google/android/apps/youtube/core/client/br;->g:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "time_window_millis"

    iget v1, p0, Lcom/google/android/apps/youtube/core/client/br;->h:I

    int-to-long v3, v1

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/br;->h:I

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "bandwidthElapsed is zero.  bandwidthBytes is: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/google/android/apps/youtube/core/client/br;->g:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :cond_1
    :goto_0
    const-wide/16 v0, 0x7530

    add-long/2addr v0, v7

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/br;->f:J

    iput-wide v9, p0, Lcom/google/android/apps/youtube/core/client/br;->g:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/core/client/br;->h:I

    :cond_2
    return-void

    :cond_3
    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/client/br;->g:J

    const-wide/16 v3, 0x3e8

    mul-long/2addr v0, v3

    iget v3, p0, Lcom/google/android/apps/youtube/core/client/br;->h:I

    int-to-long v3, v3

    div-long v3, v0, v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/br;->e:Lcom/google/android/apps/youtube/core/utils/o;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/br;->b:Landroid/content/Context;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/utils/o;->a(Landroid/content/Context;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/br;->d:Lcom/google/android/apps/youtube/medialib/a/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/medialib/a/a;->a(Lcom/google/android/apps/youtube/medialib/a/b;)V

    return-void
.end method

.method public final a(IJ)V
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/client/br;->g:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/br;->g:J

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/br;->h:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/youtube/core/client/br;->h:I

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/br;->b(Z)V

    return-void
.end method

.method public final a(Z)V
    .locals 7

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/br;->c:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/br;->i:J

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/br;->j:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/client/br;->i:J

    const-wide/16 v3, -0x1

    cmp-long v0, v0, v3

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/br;->c:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    iget-wide v3, p0, Lcom/google/android/apps/youtube/core/client/br;->i:J

    sub-long/2addr v0, v3

    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-gez v3, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "buffering ended before it began, buffer time: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :cond_2
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/br;->j:Z

    goto :goto_0

    :cond_3
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const-string v3, "cpn"

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/client/br;->a:Ljava/lang/String;

    invoke-virtual {v6, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "buffering_delay_millis"

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/br;->e:Lcom/google/android/apps/youtube/core/utils/o;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/br;->b:Landroid/content/Context;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/utils/o;->a(Landroid/content/Context;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/br;->b(Z)V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/br;->i:J

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/br;->d:Lcom/google/android/apps/youtube/medialib/a/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/medialib/a/a;->b(Lcom/google/android/apps/youtube/medialib/a/b;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/br;->b(Z)V

    return-void
.end method
