.class public final Lcom/google/android/apps/youtube/core/player/ae;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:Z

.field private B:Z

.field private final C:Ljava/lang/Runnable;

.field private final D:Ljava/lang/Runnable;

.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/youtube/common/c/a;

.field private final c:Lcom/google/android/apps/youtube/core/player/Director;

.field private final d:Lcom/google/android/apps/youtube/medialib/player/x;

.field private final e:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

.field private final f:Lcom/google/android/apps/youtube/core/player/fetcher/e;

.field private final g:Lcom/google/android/apps/youtube/core/client/bc;

.field private final h:Lcom/google/android/apps/youtube/core/identity/l;

.field private final i:Lcom/google/android/apps/youtube/datalib/offline/n;

.field private final j:Lcom/google/android/apps/youtube/core/offline/store/q;

.field private final k:Lcom/google/android/apps/youtube/core/Analytics;

.field private final l:Lcom/google/android/apps/youtube/core/aw;

.field private final m:Lcom/google/android/apps/youtube/core/au;

.field private final n:Ljava/util/concurrent/Executor;

.field private final o:Landroid/util/SparseArray;

.field private final p:Lcom/google/android/apps/youtube/core/player/ai;

.field private final q:Landroid/media/AudioManager;

.field private final r:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final s:Landroid/os/Handler;

.field private final t:Lcom/google/android/apps/youtube/core/player/ad;

.field private u:I

.field private v:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

.field private w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

.field private x:I

.field private y:I

.field private z:Z


# direct methods
.method protected constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/youtube/core/player/af;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/player/af;-><init>(Lcom/google/android/apps/youtube/core/player/ae;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->C:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/ag;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/player/ag;-><init>(Lcom/google/android/apps/youtube/core/player/ae;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->D:Ljava/lang/Runnable;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->a:Landroid/content/Context;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->b:Lcom/google/android/apps/youtube/common/c/a;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->d:Lcom/google/android/apps/youtube/medialib/player/x;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->e:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->g:Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->l:Lcom/google/android/apps/youtube/core/aw;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->k:Lcom/google/android/apps/youtube/core/Analytics;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->n:Ljava/util/concurrent/Executor;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->m:Lcom/google/android/apps/youtube/core/au;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->q:Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->r:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->B:Z

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->t:Lcom/google/android/apps/youtube/core/player/ad;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->h:Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->j:Lcom/google/android/apps/youtube/core/offline/store/q;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->s:Landroid/os/Handler;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->i:Lcom/google/android/apps/youtube/datalib/offline/n;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->p:Lcom/google/android/apps/youtube/core/player/ai;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->o:Landroid/util/SparseArray;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->f:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/medialib/player/x;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/e;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/au;Lcom/google/android/apps/youtube/core/Analytics;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/datalib/offline/n;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/player/w;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;)V
    .locals 14

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/google/android/apps/youtube/core/player/af;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/player/af;-><init>(Lcom/google/android/apps/youtube/core/player/ae;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->C:Ljava/lang/Runnable;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/ag;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/player/ag;-><init>(Lcom/google/android/apps/youtube/core/player/ae;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->D:Ljava/lang/Runnable;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->a:Landroid/content/Context;

    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->b:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/medialib/player/x;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->d:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-static/range {p4 .. p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->e:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    invoke-static/range {p6 .. p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->g:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p8 .. p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/aw;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->l:Lcom/google/android/apps/youtube/core/aw;

    invoke-static/range {p10 .. p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/Analytics;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->k:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static/range {p11 .. p11}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->n:Ljava/util/concurrent/Executor;

    invoke-static/range {p9 .. p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/au;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->m:Lcom/google/android/apps/youtube/core/au;

    invoke-static/range {p13 .. p13}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p16 .. p16}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->a:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->q:Landroid/media/AudioManager;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/ah;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/core/player/ah;-><init>(Lcom/google/android/apps/youtube/core/player/ae;B)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->r:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->B:Z

    invoke-static/range {p18 .. p18}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p19 .. p19}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/player/ad;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->t:Lcom/google/android/apps/youtube/core/player/ad;

    invoke-static/range {p12 .. p12}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->h:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static/range {p15 .. p15}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/offline/store/q;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->j:Lcom/google/android/apps/youtube/core/offline/store/q;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->i:Lcom/google/android/apps/youtube/datalib/offline/n;

    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->f:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/Director;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/ae;->a:Landroid/content/Context;

    move-object/from16 v2, p3

    move-object/from16 v4, p2

    move-object/from16 v5, p17

    move-object/from16 v6, p7

    move-object/from16 v7, p18

    move-object/from16 v8, p19

    move-object/from16 v9, p16

    move-object/from16 v10, p10

    move-object/from16 v11, p8

    move-object/from16 v12, p13

    move-object/from16 v13, p20

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/youtube/core/player/Director;-><init>(Lcom/google/android/apps/youtube/medialib/player/x;Landroid/content/Context;Lcom/google/android/apps/youtube/common/c/a;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/core/client/e;Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->o:Landroid/util/SparseArray;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/ai;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/core/player/ai;-><init>(Lcom/google/android/apps/youtube/core/player/ae;B)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->p:Lcom/google/android/apps/youtube/core/player/ai;

    new-instance v1, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->s:Landroid/os/Handler;

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->u:I

    return-void
.end method

.method private H()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ae;->k()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->d:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->k()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->v:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->k()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    :cond_0
    iput v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->x:I

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->y:I

    return-void
.end method

.method private I()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->v:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getWatchFeature()Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->v:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->shouldStartPaused()Z

    move-result v2

    iget v3, p0, Lcom/google/android/apps/youtube/core/player/ae;->x:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/client/WatchFeature;ZI)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->l()V

    return-void
.end method

.method private J()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->u()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->z:Z

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "background_mode"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/Director;->u()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->z:Z

    :cond_1
    return-void
.end method

.method private K()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->B:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->q:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->r:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/high16 v2, -0x80000000

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    :cond_0
    return-void
.end method

.method private L()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->h:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->j:Lcom/google/android/apps/youtube/core/offline/store/q;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/q;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->j:Lcom/google/android/apps/youtube/core/offline/store/q;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->h:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/q;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/ae;)Lcom/google/android/apps/youtube/core/player/Director;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/ae;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/ae;->A:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/ae;)Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/player/ae;)Lcom/google/android/apps/youtube/common/c/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->b:Lcom/google/android/apps/youtube/common/c/a;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/player/ae;)Lcom/google/android/apps/youtube/medialib/player/x;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->d:Lcom/google/android/apps/youtube/medialib/player/x;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/core/player/ae;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->a:Landroid/content/Context;

    return-object v0
.end method

.method private handleVideoStageEvent(Lcom/google/android/apps/youtube/core/player/event/ac;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_VIDEO:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->y:I

    :cond_0
    return-void
.end method

.method private handleVideoStageEvent(Lcom/google/android/apps/youtube/core/player/event/af;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/af;->a()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->A:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ae;->K()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final A()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->u()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ae;->k()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->s()V

    :cond_0
    return-void
.end method

.method public final B()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->u()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->o:Landroid/util/SparseArray;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->u:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    :cond_0
    return-void
.end method

.method public final C()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->k()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->o:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    return-void
.end method

.method public final D()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->o()V

    return-void
.end method

.method public final E()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ae;->H()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->n()V

    return-void
.end method

.method public final F()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->d:Lcom/google/android/apps/youtube/medialib/player/x;

    instance-of v0, v0, Lcom/google/android/apps/youtube/medialib/player/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->d:Lcom/google/android/apps/youtube/medialib/player/x;

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/ab;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/ab;->m()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "The player is not a RemoteControlAwarePlayer. Will not switch to local route mode"

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final G()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->d:Lcom/google/android/apps/youtube/medialib/player/x;

    instance-of v0, v0, Lcom/google/android/apps/youtube/medialib/player/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->d:Lcom/google/android/apps/youtube/medialib/player/x;

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/ab;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/ab;->n()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "The player is not a RemoteControlAwarePlayer. Will not switch to remote route mode"

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1}, Ljava/lang/RuntimeException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a()Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->d()Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-result-object v0

    return-object v0
.end method

.method public final a(F)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->d:Lcom/google/android/apps/youtube/medialib/player/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(F)V

    return-void
.end method

.method public final a(I)V
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->u:I

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ae;->k()V

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->u:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/ae;->g(Z)Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/Director;->u()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->o:Landroid/util/SparseArray;

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->u:I

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->n()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->f()V

    iput p1, p0, Lcom/google/android/apps/youtube/core/player/ae;->u:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->o:Landroid/util/SparseArray;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->u:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->o:Landroid/util/SparseArray;

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->u:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->remove(I)V

    if-eqz v0, :cond_3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/ae;->a(Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->o:Landroid/util/SparseArray;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->u:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_1

    :cond_3
    iput-object v3, p0, Lcom/google/android/apps/youtube/core/player/ae;->v:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iput-object v3, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;)V
    .locals 12

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->b:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/j;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/player/event/j;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ae;->H()V

    iget-boolean v0, p1, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->manageAudioFocus:Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/ae;->f(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ae;->K()V

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->playbackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->v:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iget-object v11, p1, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->sequencerState:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerState;

    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;

    if-ne v0, v1, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/sequencer/u;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->b:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/ae;->t:Lcom/google/android/apps/youtube/core/player/ad;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/ae;->k:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/ae;->n:Ljava/util/concurrent/Executor;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/ae;->e:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/ae;->f:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    iget-object v8, p0, Lcom/google/android/apps/youtube/core/player/ae;->l:Lcom/google/android/apps/youtube/core/aw;

    move-object v9, v11

    check-cast v9, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/youtube/core/player/sequencer/u;-><init>(Lcom/google/android/apps/youtube/core/player/Director;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->directorState:Lcom/google/android/apps/youtube/core/player/DirectorSavedState;

    if-nez v0, :cond_6

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ae;->I()V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->p:Lcom/google/android/apps/youtube/core/player/ai;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ai;->a()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ae;->v()V

    return-void

    :cond_1
    const-class v1, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;

    if-ne v0, v1, :cond_2

    new-instance v0, Lcom/google/android/apps/youtube/core/player/sequencer/j;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/ae;->b:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/ae;->t:Lcom/google/android/apps/youtube/core/player/ad;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/ae;->k:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/ae;->l:Lcom/google/android/apps/youtube/core/aw;

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/ae;->n:Ljava/util/concurrent/Executor;

    iget-object v8, p0, Lcom/google/android/apps/youtube/core/player/ae;->f:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    iget-object v9, p0, Lcom/google/android/apps/youtube/core/player/ae;->i:Lcom/google/android/apps/youtube/datalib/offline/n;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ae;->L()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v10

    check-cast v11, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/youtube/core/player/sequencer/j;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/datalib/offline/n;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;)V

    goto :goto_0

    :cond_2
    const-class v1, Lcom/google/android/apps/youtube/core/player/state/GDataPlaylistSequencerState;

    if-ne v0, v1, :cond_3

    new-instance v0, Lcom/google/android/apps/youtube/core/player/sequencer/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->b:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/ae;->t:Lcom/google/android/apps/youtube/core/player/ad;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/ae;->k:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/ae;->e:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/ae;->l:Lcom/google/android/apps/youtube/core/aw;

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/ae;->g:Lcom/google/android/apps/youtube/core/client/bc;

    move-object v8, v11

    check-cast v8, Lcom/google/android/apps/youtube/core/player/state/GDataPlaylistSequencerState;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/core/player/sequencer/h;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/player/state/GDataPlaylistSequencerState;)V

    goto :goto_0

    :cond_3
    const-class v1, Lcom/google/android/apps/youtube/core/player/state/WatchNextVideoIdsSequencerState;

    if-ne v0, v1, :cond_4

    new-instance v0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->b:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/ae;->t:Lcom/google/android/apps/youtube/core/player/ad;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/ae;->k:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/ae;->n:Ljava/util/concurrent/Executor;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/ae;->e:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/ae;->f:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    iget-object v8, p0, Lcom/google/android/apps/youtube/core/player/ae;->l:Lcom/google/android/apps/youtube/core/aw;

    move-object v9, v11

    check-cast v9, Lcom/google/android/apps/youtube/core/player/state/WatchNextVideoIdsSequencerState;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/youtube/core/player/sequencer/ae;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/player/state/WatchNextVideoIdsSequencerState;)V

    goto :goto_0

    :cond_4
    const-class v1, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;

    if-ne v0, v1, :cond_5

    new-instance v0, Lcom/google/android/apps/youtube/core/player/sequencer/s;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->b:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/ae;->t:Lcom/google/android/apps/youtube/core/player/ad;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/ae;->k:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/ae;->e:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/ae;->l:Lcom/google/android/apps/youtube/core/aw;

    move-object v7, v11

    check-cast v7, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/player/sequencer/s;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;)V

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/DirectorSavedState;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->l()V

    goto/16 :goto_1
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V
    .locals 13

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->b:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/j;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/player/event/j;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ae;->K()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    instance-of v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/player/ae;->c(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/ae;->v:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    check-cast v0, Lcom/google/android/apps/youtube/core/player/sequencer/u;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ae;->H()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/ae;->v:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->a()V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoStartTime()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->x:I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->isOffline()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->f:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/sequencer/j;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/ae;->b:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/ae;->t:Lcom/google/android/apps/youtube/core/player/ad;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/ae;->k:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/ae;->l:Lcom/google/android/apps/youtube/core/aw;

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/ae;->n:Ljava/util/concurrent/Executor;

    iget-object v8, p0, Lcom/google/android/apps/youtube/core/player/ae;->f:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    iget-object v9, p0, Lcom/google/android/apps/youtube/core/player/ae;->i:Lcom/google/android/apps/youtube/datalib/offline/n;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ae;->L()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v10

    move-object v11, p1

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/youtube/core/player/sequencer/j;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/datalib/offline/n;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->isScriptedPlay()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->b:Lcom/google/android/apps/youtube/common/c/a;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->NAVIGATION:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ae;->I()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->p:Lcom/google/android/apps/youtube/core/player/ai;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ai;->a()V

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoIds()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoIds()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->k:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "WatchIntentWithMultipleVideoIds"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoStartTime()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->x:I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->isWatchNextDisabled()Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Lcom/google/android/apps/youtube/core/player/sequencer/s;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->b:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/ae;->t:Lcom/google/android/apps/youtube/core/player/ad;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/ae;->k:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/ae;->e:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/ae;->l:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoIds()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistIndex()I

    move-result v8

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getClickTrackingParams()[B

    move-result-object v9

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlayerParams()Ljava/lang/String;

    move-result-object v10

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/youtube/core/player/sequencer/s;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/aw;Ljava/util/List;I[BLjava/lang/String;)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->f:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->b:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/ae;->t:Lcom/google/android/apps/youtube/core/player/ad;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/ae;->k:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/ae;->n:Ljava/util/concurrent/Executor;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/ae;->e:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/ae;->f:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    iget-object v8, p0, Lcom/google/android/apps/youtube/core/player/ae;->l:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoIds()Ljava/util/List;

    move-result-object v9

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistIndex()I

    move-result v10

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getClickTrackingParams()[B

    move-result-object v11

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlayerParams()Ljava/lang/String;

    move-result-object v12

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/youtube/core/player/sequencer/ae;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/core/aw;Ljava/util/List;I[BLjava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->isWatchNextDisabled()Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v0, Lcom/google/android/apps/youtube/core/player/sequencer/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->b:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/ae;->t:Lcom/google/android/apps/youtube/core/player/ad;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/ae;->k:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/ae;->e:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/ae;->l:Lcom/google/android/apps/youtube/core/aw;

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/ae;->g:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistIndex()I

    move-result v9

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/youtube/core/player/sequencer/h;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/bc;Ljava/lang/String;I)V

    goto/16 :goto_1

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->f:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/sequencer/u;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->b:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/ae;->t:Lcom/google/android/apps/youtube/core/player/ad;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/ae;->k:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/ae;->n:Ljava/util/concurrent/Executor;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/ae;->e:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/ae;->f:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    iget-object v8, p0, Lcom/google/android/apps/youtube/core/player/ae;->l:Lcom/google/android/apps/youtube/core/aw;

    move-object v9, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/youtube/core/player/sequencer/u;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    goto/16 :goto_1
.end method

.method public final a(Lcom/google/android/apps/youtube/medialib/player/y;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/medialib/player/y;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->q()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ae;->J()V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->b(Z)V

    return-void
.end method

.method public final b(I)Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->u:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/ae;->g(Z)Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->o:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    goto :goto_0
.end method

.method public final b()Lcom/google/android/apps/youtube/core/player/au;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->c()Lcom/google/android/apps/youtube/core/player/au;

    move-result-object v0

    return-object v0
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->a(Z)V

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->v:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ae;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ae;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->f()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistIndex()I

    move-result v2

    if-ne v1, v2, :cond_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoIds()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->v:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->isInnerTube()Z

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->isInnerTube()Z

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->v:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getWatchFeature()Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getWatchFeature()Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-result-object v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->v:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->isOffline()Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->v:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->isOffline()Z

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->v:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->shouldContinuePlayback()Z

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->shouldContinuePlayback()Z

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->s()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/Director;->a(I)V

    return-void
.end method

.method public final c(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/Director;->d(Z)V

    return-void
.end method

.method public final c(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)Z
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->shouldContinuePlayback()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    instance-of v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/u;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/Director;->v()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->v:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->v()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/Director;->b(I)V

    return-void
.end method

.method public final d(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/Director;->c(Z)V

    return-void
.end method

.method public final e()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->w()I

    move-result v0

    return v0
.end method

.method public final e(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/Director;->b(Z)V

    return-void
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->x()I

    move-result v0

    return v0
.end method

.method public final f(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/ae;->B:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/Director;->e(Z)V

    return-void
.end method

.method public final g(Z)Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->v:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->r()Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerState;

    move-result-object v0

    :cond_1
    new-instance v1, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->v:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/youtube/core/player/Director;->f(Z)Lcom/google/android/apps/youtube/core/player/DirectorSavedState;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/apps/youtube/core/player/ae;->B:Z

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;-><init>(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerState;Lcom/google/android/apps/youtube/core/player/DirectorSavedState;Z)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->y()Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->i()Z

    move-result v0

    return v0
.end method

.method public final handlePlaybackServiceException(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->k:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->recordExceptionInAnalytics(Lcom/google/android/apps/youtube/core/Analytics;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ae;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->shouldSkipVideo()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->y:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->m:Lcom/google/android/apps/youtube/core/au;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/au;->S()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->s:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->D:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->y:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->y:I

    :cond_0
    return-void
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->g()V

    return-void
.end method

.method public final j()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->h()V

    return-void
.end method

.method public final k()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->k()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->p:Lcom/google/android/apps/youtube/core/player/ai;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ai;->b()V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->z:Z

    return-void
.end method

.method public final l()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->j()V

    return-void
.end method

.method public final m()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->AD_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->g()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ae;->n()Z

    move-result v0

    goto :goto_0
.end method

.method public final n()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/Director;->w()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/core/player/Director;->a(ZI)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->p()V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final o()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->p_()Z

    move-result v0

    goto :goto_0
.end method

.method public final p()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->r_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->q_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->n()V

    :cond_0
    return-void
.end method

.method public final s()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->m()V

    :cond_0
    return-void
.end method

.method public final t()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->r()V

    return-void
.end method

.method public final u()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->o()V

    :cond_0
    return-void
.end method

.method public final v()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->s:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ae;->C:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final w()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->u()Z

    move-result v0

    return v0
.end method

.method public final x()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ae;->K()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->t()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->w:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;->q()V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/ae;->J()V

    return-void
.end method

.method public final y()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->q()V

    return-void
.end method

.method public final z()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ae;->c:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director;->p()V

    return-void
.end method
