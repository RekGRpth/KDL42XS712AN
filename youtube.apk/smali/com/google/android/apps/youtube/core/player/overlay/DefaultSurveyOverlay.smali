.class public Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/av;
.implements Lcom/google/android/apps/youtube/core/player/overlay/bm;


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/view/ViewGroup;

.field private final d:[Landroid/view/ViewGroup;

.field private final e:[Landroid/widget/TextView;

.field private final f:Landroid/view/View;

.field private final g:Landroid/view/View;

.field private final h:Landroid/content/Context;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:Landroid/graphics/drawable/Drawable;

.field private m:Lcom/google/android/apps/youtube/core/player/overlay/bn;

.field private n:Z

.field private o:I

.field private p:Z

.field private q:Z

.field private r:I

.field private s:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->h:Landroid/content/Context;

    sget v2, Lcom/google/android/youtube/l;->bm:I

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->a:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->a:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/youtube/j;->cG:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->a:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/youtube/j;->cL:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->c:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->c:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/youtube/j;->fs:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->i:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/youtube/h;->az:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/youtube/h;->aA:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->l:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->c:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/youtube/j;->fr:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->j:Landroid/widget/TextView;

    new-array v2, v6, [Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->c:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/youtube/j;->fo:I

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    aput-object v0, v2, v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->c:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/youtube/j;->fp:I

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    aput-object v0, v2, v4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->c:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/youtube/j;->fq:I

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    aput-object v0, v2, v5

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->d:[Landroid/view/ViewGroup;

    const/4 v0, 0x5

    new-array v2, v0, [Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->c:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/youtube/j;->fj:I

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v2, v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->c:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/youtube/j;->fk:I

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v2, v4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->c:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/youtube/j;->fl:I

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v2, v5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->c:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/youtube/j;->fm:I

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v2, v6

    const/4 v3, 0x4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->c:Landroid/view/ViewGroup;

    sget v4, Lcom/google/android/youtube/j;->fn:I

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v2, v3

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->e:[Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->c:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/youtube/j;->eO:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->f:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/ag;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/ag;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;B)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->f:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/ah;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/ah;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;B)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->c:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/youtube/j;->eY:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->g:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->g:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/ai;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/ai;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;B)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move v0, v1

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->e:[Landroid/widget/TextView;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->e:[Landroid/widget/TextView;

    aget-object v1, v1, v0

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/aj;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/aj;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;I)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->a()V

    return-void
.end method

.method private a(IZ)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->e:[Landroid/widget/TextView;

    array-length v0, v0

    if-lt p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->e:[Landroid/widget/TextView;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setSelected(Z)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->n:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->e:[Landroid/widget/TextView;

    aget-object v1, v0, p1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->k:Landroid/graphics/drawable/Drawable;

    :goto_1
    invoke-virtual {v1, v2, v2, v0, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->l:Landroid/graphics/drawable/Drawable;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->e:[Landroid/widget/TextView;

    aget-object v0, v0, p1

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->f()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;ILandroid/view/View;)V
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->a(IZ)V

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->n:Z

    if-eqz v2, :cond_3

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->n:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->o:I

    if-ge p1, v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->o:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->a(IZ)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->e()V

    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_2
    iget v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->o:I

    if-ge v0, v2, :cond_0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->a(IZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->f()V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;Landroid/view/MotionEvent;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->r:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->s:I

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/bn;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->m:Lcom/google/android/apps/youtube/core/player/overlay/bn;

    return-object v0
.end method

.method private b(I)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->e:[Landroid/widget/TextView;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->e:[Landroid/widget/TextView;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->r:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->s:I

    return v0
.end method

.method private e()V
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->p:Z

    move v0, v1

    :goto_0
    iget v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->o:I

    if-ge v0, v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->p:Z

    if-nez v2, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->b(I)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->p:Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->p:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->n:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->o:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->b(I)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    :goto_2
    iput-boolean v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->p:Z

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->g:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->p:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->n:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->f:Landroid/view/View;

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->q:Z

    if-eqz v2, :cond_6

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->p:Z

    if-nez v2, :cond_6

    :goto_4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_4
    move v3, v1

    goto :goto_2

    :cond_5
    move v0, v4

    goto :goto_3

    :cond_6
    move v1, v4

    goto :goto_4
.end method

.method private f()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->m:Lcom/google/android/apps/youtube/core/player/overlay/bn;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->o:I

    new-array v3, v1, [I

    move v1, v0

    :goto_1
    iget v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->o:I

    if-ge v0, v2, :cond_2

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->b(I)Z

    move-result v2

    if-eqz v2, :cond_1

    add-int/lit8 v2, v1, 0x1

    aput v0, v3, v1

    move v1, v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->m:Lcom/google/android/apps/youtube/core/player/overlay/bn;

    invoke-static {v3, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/bn;->a([I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->p:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->q:Z

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->r:I

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->s:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->setVisibility(I)V

    return-void
.end method

.method public final a(I)V
    .locals 6

    int-to-float v0, p1

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/e/m;->a(II)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->j:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->a:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/youtube/p;->fT:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->a()V

    iput-boolean p3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->n:Z

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->o:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->e:[Landroid/widget/TextView;

    array-length v0, v0

    if-ge v1, v0, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->e:[Landroid/widget/TextView;

    aget-object v4, v0, v1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->e:[Landroid/widget/TextView;

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->a(IZ)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    if-ne v1, v3, :cond_1

    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->e:[Landroid/widget/TextView;

    aget-object v0, v0, v1

    sget v4, Lcom/google/android/youtube/p;->fU:I

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->e:[Landroid/widget/TextView;

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->e:[Landroid/widget/TextView;

    aget-object v0, v0, v1

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final c()Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->q:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->e()V

    return-void
.end method

.method public setListener(Lcom/google/android/apps/youtube/core/player/overlay/bn;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->m:Lcom/google/android/apps/youtube/core/player/overlay/bn;

    return-void
.end method

.method public setMinimized(Z)V
    .locals 4

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->b:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->c:Landroid/view/ViewGroup;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1
.end method

.method public setVisible(Z)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
