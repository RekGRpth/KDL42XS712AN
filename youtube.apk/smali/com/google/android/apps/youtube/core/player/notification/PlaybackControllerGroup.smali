.class public final Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/client/bc;

.field private final b:Lcom/google/android/apps/youtube/core/client/bj;

.field private final c:Lcom/google/android/apps/youtube/common/network/h;

.field private final d:Lcom/google/android/apps/youtube/core/identity/l;

.field private final e:Lcom/google/android/apps/youtube/core/offline/store/q;

.field private final f:Lcom/google/android/apps/youtube/core/player/notification/j;

.field private final g:Ljava/util/List;

.field private final h:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;

.field private final i:Landroid/os/Handler;

.field private j:Ljava/lang/String;

.field private k:Lcom/google/android/apps/youtube/common/a/d;

.field private l:Lcom/google/android/apps/youtube/common/a/d;

.field private m:Z

.field private n:Lcom/google/android/apps/youtube/core/player/notification/i;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/common/network/h;Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/apps/youtube/core/player/notification/j;Ljava/lang/String;ILcom/google/android/apps/youtube/core/player/notification/c;)V
    .locals 14

    const/4 v10, 0x0

    const/4 v13, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v11, p11

    move-object/from16 v12, p12

    invoke-direct/range {v0 .. v13}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/common/network/h;Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/apps/youtube/core/player/notification/j;Ljava/lang/String;ILcom/google/android/apps/youtube/core/player/notification/c;B)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/common/network/h;Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/apps/youtube/core/player/notification/j;Ljava/lang/String;ILcom/google/android/apps/youtube/core/player/notification/c;B)V
    .locals 10

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->b:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->d:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/core/offline/store/q;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->e:Lcom/google/android/apps/youtube/core/offline/store/q;

    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->c:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static/range {p9 .. p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/core/player/notification/j;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->f:Lcom/google/android/apps/youtube/core/player/notification/j;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->h:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->i:Landroid/os/Handler;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->g:Ljava/util/List;

    iget-object v9, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->g:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/notification/d;

    move-object v3, p1

    move-object/from16 v4, p7

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    move/from16 v8, p11

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/youtube/core/player/notification/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/apps/youtube/core/player/notification/j;Ljava/lang/String;I)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;

    move-object/from16 v0, p9

    move-object/from16 v1, p12

    invoke-direct {v2, p1, v0, v1}, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/notification/j;Lcom/google/android/apps/youtube/core/player/notification/c;)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->n:Lcom/google/android/apps/youtube/core/player/notification/i;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->g:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->n:Lcom/google/android/apps/youtube/core/player/notification/i;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f100000    # 0.5625f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-static {p1, v2, v1, v3, v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->h:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->title:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->h:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->h:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;

    iget v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->duration:I

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    iput-wide v1, v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->g:J

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->f:Lcom/google/android/apps/youtube/core/player/notification/j;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/notification/j;->a()V

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->sdThumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->sdThumbnailUri:Landroid/net/Uri;

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->b:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->l:Lcom/google/android/apps/youtube/common/a/d;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->hqThumbnailUri:Landroid/net/Uri;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->h:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;

    iput-object p1, v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->f:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->c()V

    return-void
.end method

.method private c()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->m:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->h:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/notification/i;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->h:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/core/player/notification/i;->a(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;)V

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->d:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->e:Lcom/google/android/apps/youtube/core/offline/store/q;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->d:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/q;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->v()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->i:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/notification/f;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/youtube/core/player/notification/f;-><init>(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->b(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->m:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->c()V

    return-void
.end method

.method public final a(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->n:Lcom/google/android/apps/youtube/core/player/notification/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->n:Lcom/google/android/apps/youtube/core/player/notification/i;

    instance-of v0, v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->n:Lcom/google/android/apps/youtube/core/player/notification/i;

    check-cast v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->a(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->h:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;

    iput-object p1, v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->c:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->c()V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/notification/i;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/player/notification/i;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->n:Lcom/google/android/apps/youtube/core/player/notification/i;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->g:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->n:Lcom/google/android/apps/youtube/core/player/notification/i;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->g:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->n:Lcom/google/android/apps/youtube/core/player/notification/i;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->g:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->n:Lcom/google/android/apps/youtube/core/player/notification/i;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(ZZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->h:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;

    iput-boolean p1, v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->d:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->h:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;

    iput-boolean p2, v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->e:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->c()V

    return-void
.end method

.method public final b()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->m:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/notification/i;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/notification/i;->a()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->j:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->h:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;

    iput-object v2, v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->a:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->b:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->BUFFERING:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    iput-object v1, v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->c:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    iput-boolean v3, v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->d:Z

    iput-boolean v3, v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->e:Z

    iput-object v2, v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->f:Landroid/graphics/Bitmap;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->g:J

    iput-boolean v3, v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->h:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->l:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->l:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->k:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->k:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/core/player/notification/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/player/notification/g;-><init>(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->k:Lcom/google/android/apps/youtube/common/a/d;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/notification/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/player/notification/h;-><init>(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->l:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->c:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->k:Lcom/google/android/apps/youtube/common/a/d;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->h:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;

    iput-boolean p1, v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->h:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->c()V

    return-void
.end method
