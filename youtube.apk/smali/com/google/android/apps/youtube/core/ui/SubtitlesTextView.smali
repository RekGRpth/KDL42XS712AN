.class public Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field private a:Landroid/text/TextPaint;

.field private b:Landroid/text/TextPaint;

.field private c:Landroid/text/TextPaint;

.field private d:Landroid/graphics/Path;

.field private e:Landroid/text/StaticLayout;

.field private f:Landroid/text/StaticLayout;

.field private g:Landroid/text/Editable;

.field private h:Landroid/text/Layout$Alignment;

.field private i:I

.field private j:I

.field private k:I

.field private l:Z

.field private m:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a()V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;I)I
    .locals 5

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->getPaddingRight()I

    move-result v3

    add-int/2addr v0, v3

    iget v3, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->j:I

    packed-switch v3, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Edge type %d is not supported."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->j:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a:Landroid/text/TextPaint;

    invoke-static {p1, v3}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v0, v3

    :goto_0
    if-nez v2, :cond_0

    :goto_1
    return v0

    :pswitch_1
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->b:Landroid/text/TextPaint;

    invoke-static {p1, v3}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v0, v3

    goto :goto_0

    :cond_0
    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(I)Landroid/text/StaticLayout;
    .locals 8

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->e:Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->g:Landroid/text/Editable;

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a(Ljava/lang/CharSequence;I)I

    move-result v3

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getWidth()I

    move-result v1

    if-eq v3, v1, :cond_1

    :cond_0
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->g:Landroid/text/Editable;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->h:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    :cond_1
    return-object v0
.end method

.method private a()V
    .locals 3

    const/4 v2, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->setLayerType(ILandroid/graphics/Paint;)V

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a:Landroid/text/TextPaint;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->b:Landroid/text/TextPaint;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->b:Landroid/text/TextPaint;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->b:Landroid/text/TextPaint;

    iget v1, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->i:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->b:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->c:Landroid/text/TextPaint;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->c:Landroid/text/TextPaint;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->d:Landroid/graphics/Path;

    invoke-static {}, Landroid/text/Editable$Factory;->getInstance()Landroid/text/Editable$Factory;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/text/Editable$Factory;->newEditable(Ljava/lang/CharSequence;)Landroid/text/Editable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->g:Landroid/text/Editable;

    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->h:Landroid/text/Layout$Alignment;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getTextSize()F

    move-result v0

    const/high16 v1, 0x3d800000    # 0.0625f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->m:F

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->fP:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private b()I
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->getPaddingRight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->getPaddingLeft()I

    move-result v2

    add-int v3, v0, v2

    iget v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->j:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Edge type %d is not supported."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->j:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->e:Landroid/text/StaticLayout;

    :goto_0
    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->getLineMax(I)F

    move-result v5

    float-to-int v5, v5

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->f:Landroid/text/StaticLayout;

    goto :goto_0

    :cond_0
    add-int v0, v2, v3

    return v0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private b(I)Landroid/text/StaticLayout;
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->f:Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->g:Landroid/text/Editable;

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a(Ljava/lang/CharSequence;I)I

    move-result v3

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getWidth()I

    move-result v1

    if-eq v3, v1, :cond_2

    :cond_0
    new-instance v1, Landroid/text/SpannableString;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->g:Landroid/text/Editable;

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v0

    const-class v4, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {v1, v2, v0, v4}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/ForegroundColorSpan;

    array-length v4, v0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v0, v2

    invoke-virtual {v1, v5}, Landroid/text/SpannableString;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->b:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->h:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    :cond_2
    return-object v0
.end method

.method private c()I
    .locals 5

    iget v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->j:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Edge type %d is not supported."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->j:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->e:Landroid/text/StaticLayout;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->e:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->f:Landroid/text/StaticLayout;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->f:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->g:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->f:Landroid/text/StaticLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->e:Landroid/text/StaticLayout;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->k:I

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->d:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->c:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    :cond_2
    iget v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->j:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->f:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->e:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    sub-int v0, p4, p2

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a(I)Landroid/text/StaticLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->e:Landroid/text/StaticLayout;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->b(I)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->f:Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->e:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v2

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->d:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {v1, v0}, Landroid/text/StaticLayout;->getLineBottom(I)I

    move-result v4

    invoke-virtual {v1, v0}, Landroid/text/StaticLayout;->getLineTop(I)I

    move-result v5

    invoke-virtual {v1, v0}, Landroid/text/StaticLayout;->getLineLeft(I)F

    move-result v6

    invoke-virtual {v1, v0}, Landroid/text/StaticLayout;->getLineRight(I)F

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->getPaddingLeft()I

    move-result v8

    int-to-float v8, v8

    sub-float/2addr v6, v8

    int-to-float v5, v5

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->getPaddingRight()I

    move-result v8

    int-to-float v8, v8

    add-float/2addr v7, v8

    int-to-float v4, v4

    invoke-virtual {v3, v6, v5, v7, v4}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->d:Landroid/graphics/Path;

    sget-object v5, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v4, v3, v5}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v3, -0x80000000

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a(I)Landroid/text/StaticLayout;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->e:Landroid/text/StaticLayout;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->b(I)Landroid/text/StaticLayout;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->f:Landroid/text/StaticLayout;

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    if-ne v2, v3, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->b()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_0
    :goto_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    if-ne v2, v3, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->c()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    :cond_1
    :goto_1
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->setMeasuredDimension(II)V

    return-void

    :cond_2
    if-eq v2, v4, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->b()I

    move-result v0

    goto :goto_0

    :cond_3
    if-eq v2, v4, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->c()I

    move-result v1

    goto :goto_1
.end method

.method public setBackgroundColor(I)V
    .locals 1

    iput p1, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->k:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->c:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->invalidate()V

    return-void
.end method

.method public setEdgeColor(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->b:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->invalidate()V

    return-void
.end method

.method public setEdgeType(I)V
    .locals 7

    const/4 v2, 0x1

    const/4 v6, 0x0

    const/high16 v5, 0x41100000    # 9.0f

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->j:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Edge type %d is not supported."

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->j:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a:Landroid/text/TextPaint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    :goto_0
    :pswitch_1
    packed-switch p1, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Edge type %d is not supported."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->clearShadowLayer()V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a:Landroid/text/TextPaint;

    new-instance v1, Landroid/graphics/EmbossMaskFilter;

    const/4 v2, 0x3

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-direct {v1, v2, v3, v5, v4}, Landroid/graphics/EmbossMaskFilter;-><init>([FFFF)V

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    :goto_1
    :pswitch_4
    iput p1, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->j:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->requestLayout()V

    return-void

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a:Landroid/text/TextPaint;

    new-instance v1, Landroid/graphics/EmbossMaskFilter;

    const/4 v2, 0x3

    new-array v2, v2, [F

    fill-array-data v2, :array_1

    invoke-direct {v1, v2, v3, v5, v4}, Landroid/graphics/EmbossMaskFilter;-><init>([FFFF)V

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    goto :goto_1

    :pswitch_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a:Landroid/text/TextPaint;

    iget v1, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->m:F

    iget v2, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->m:F

    iget v3, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->m:F

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->b:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->getColor()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_6
        :pswitch_3
        :pswitch_5
    .end packed-switch

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->g:Landroid/text/Editable;

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->g:Landroid/text/Editable;

    invoke-interface {v0, p1}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->e:Landroid/text/StaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->f:Landroid/text/StaticLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->invalidate()V

    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->invalidate()V

    return-void
.end method

.method public setTextSize(F)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    const/high16 v1, 0x3d800000    # 0.0625f

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->m:F

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->b:Landroid/text/TextPaint;

    invoke-virtual {v1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->b:Landroid/text/TextPaint;

    iget v1, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->m:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    iget v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->j:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->setEdgeType(I)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->f:Landroid/text/StaticLayout;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->e:Landroid/text/StaticLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->invalidate()V

    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->a:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->b:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->c:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->invalidate()V

    return-void
.end method

.method public setWrapText(Z)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->l:Z

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->f:Landroid/text/StaticLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->e:Landroid/text/StaticLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ui/SubtitlesTextView;->requestLayout()V

    return-void
.end method
