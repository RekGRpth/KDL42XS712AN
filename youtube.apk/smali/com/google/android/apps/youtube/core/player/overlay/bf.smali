.class public final Lcom/google/android/apps/youtube/core/player/overlay/bf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;
.implements Lcom/google/android/apps/youtube/core/player/overlay/bi;
.implements Lcom/google/android/apps/youtube/core/utils/ap;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/overlay/be;

.field private final b:Lcom/google/android/apps/youtube/core/client/ce;

.field private final c:Lcom/google/android/apps/youtube/core/player/overlay/q;

.field private final d:Lcom/google/android/apps/youtube/core/Analytics;

.field private final e:Landroid/content/Context;

.field private final f:Lcom/google/android/apps/youtube/core/utils/an;

.field private final g:Landroid/os/Handler;

.field private final h:Lcom/google/android/apps/youtube/core/player/ao;

.field private final i:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;

.field private final j:Lcom/google/android/apps/youtube/common/c/a;

.field private k:Lcom/google/android/apps/youtube/datalib/legacy/model/Subtitles;

.field private l:Ljava/util/List;

.field private m:I

.field private n:Lcom/google/android/apps/youtube/common/a/d;

.field private o:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

.field private p:Z

.field private q:Z

.field private r:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/be;Lcom/google/android/apps/youtube/core/client/ce;Lcom/google/android/apps/youtube/core/player/overlay/q;Lcom/google/android/apps/youtube/core/Analytics;Landroid/content/SharedPreferences;Landroid/content/Context;Lcom/google/android/apps/youtube/common/c/a;)V
    .locals 7

    const/4 v6, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/be;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->a:Lcom/google/android/apps/youtube/core/player/overlay/be;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/ce;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->b:Lcom/google/android/apps/youtube/core/client/ce;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/q;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->c:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->j:Lcom/google/android/apps/youtube/common/c/a;

    iput-object p4, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->d:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->e:Landroid/content/Context;

    new-instance v0, Lcom/google/android/apps/youtube/core/utils/an;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/utils/an;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->f:Lcom/google/android/apps/youtube/core/utils/an;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p6}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->g:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;

    invoke-direct {v0, p6, p5}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->i:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->i:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a(Lcom/google/android/apps/youtube/core/player/overlay/bi;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->i:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/core/player/overlay/be;->setSubtitlesStyle(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->i:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a()F

    move-result v0

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/core/player/overlay/be;->setFontScale(F)V

    new-instance v0, Lcom/google/android/apps/youtube/core/player/ao;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->g:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/apps/youtube/core/player/overlay/bg;

    invoke-direct {v3, p0, v6}, Lcom/google/android/apps/youtube/core/player/overlay/bg;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/bf;B)V

    sget v2, Lcom/google/android/youtube/p;->gk:I

    invoke-virtual {p6, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v2, p5

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/ao;-><init>(Landroid/os/Handler;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/core/player/ap;Lcom/google/android/apps/youtube/core/client/ce;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->h:Lcom/google/android/apps/youtube/core/player/ao;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/bh;

    invoke-direct {v0, p0, v6}, Lcom/google/android/apps/youtube/core/player/overlay/bh;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/bf;B)V

    invoke-virtual {p3, v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->a(Lcom/google/android/apps/youtube/core/player/overlay/t;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/bf;)Lcom/google/android/apps/youtube/core/player/overlay/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->c:Lcom/google/android/apps/youtube/core/player/overlay/q;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/overlay/bf;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->e:Landroid/content/Context;

    return-object v0
.end method

.method private b(I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/Subtitles;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->p:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->q:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->a:Lcom/google/android/apps/youtube/core/player/overlay/be;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/Subtitles;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Subtitles;->getSubtitleWindowSnapshotsAt(I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/be;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->l:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_1

    add-int/lit8 v0, v0, 0x1

    :goto_0
    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->m:I

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->m:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->f:Lcom/google/android/apps/youtube/core/utils/an;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->l:Ljava/util/List;

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->m:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, p0, p1, v0}, Lcom/google/android/apps/youtube/core/utils/an;->a(Lcom/google/android/apps/youtube/core/utils/ap;II)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    xor-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bf;->d()V

    goto :goto_1
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/player/overlay/bf;)Lcom/google/android/apps/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->d:Lcom/google/android/apps/youtube/core/Analytics;

    return-object v0
.end method

.method private c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->f:Lcom/google/android/apps/youtube/core/utils/an;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/utils/an;->a(Lcom/google/android/apps/youtube/core/utils/ap;)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/player/overlay/bf;)Lcom/google/android/apps/youtube/core/player/ao;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->h:Lcom/google/android/apps/youtube/core/player/ao;

    return-object v0
.end method

.method private d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->f:Lcom/google/android/apps/youtube/core/utils/an;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/utils/an;->a(Lcom/google/android/apps/youtube/core/utils/ap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->a:Lcom/google/android/apps/youtube/core/player/overlay/be;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/be;->d()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->a:Lcom/google/android/apps/youtube/core/player/overlay/be;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/be;->e()V

    return-void
.end method

.method private e()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bf;->d()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/Subtitles;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->c:Lcom/google/android/apps/youtube/core/player/overlay/q;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/q;->b(Z)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/Subtitles;

    :cond_0
    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->o:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 3

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/Subtitles;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->a:Lcom/google/android/apps/youtube/core/player/overlay/be;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/Subtitles;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Subtitles;->getSubtitleWindowSnapshotsAt(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/youtube/core/player/overlay/be;->a(Ljava/util/List;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/Subtitles;

    if-nez v1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->m:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->m:I

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->m:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->l:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->m:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->i:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a(Lcom/google/android/apps/youtube/core/player/overlay/bi;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->c:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/q;->a(Lcom/google/android/apps/youtube/core/player/overlay/t;)V

    return-void
.end method

.method public final a(F)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->a:Lcom/google/android/apps/youtube/core/player/overlay/be;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/be;->setFontScale(F)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->o:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->j:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/x;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->vssId:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/core/player/event/x;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->n:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->n:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->isDisableOption()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bf;->e()V

    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->n:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->b:Lcom/google/android/apps/youtube/core/client/ce;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->g:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->n:Lcom/google/android/apps/youtube/common/a/d;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/core/client/ce;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->a:Lcom/google/android/apps/youtube/core/player/overlay/be;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/be;->setSubtitlesStyle(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "error retrieving subtitle"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bf;->e()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Subtitles;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/Subtitles;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/legacy/model/Subtitles;->getEventTimes()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->l:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->c:Lcom/google/android/apps/youtube/core/player/overlay/q;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/q;->b(Z)V

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->r:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/bf;->b(I)V

    return-void
.end method

.method public final b()Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->o:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    return-object v0
.end method

.method public final handleVideoStageEvent(Lcom/google/android/apps/youtube/core/player/event/ac;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isPlayingVideo()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->q:Z

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->NEW:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->h:Lcom/google/android/apps/youtube/core/player/ao;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ao;->b()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bf;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->n:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->n:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->n:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->h:Lcom/google/android/apps/youtube/core/player/ao;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ao;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->a:Lcom/google/android/apps/youtube/core/player/overlay/be;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->i:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/be;->setSubtitlesStyle(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->a:Lcom/google/android/apps/youtube/core/player/overlay/be;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->i:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a()F

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/be;->setFontScale(F)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_VIDEO:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->j:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/event/x;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->o:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->o:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->vssId:Ljava/lang/String;

    :goto_1
    invoke-direct {v2, v0}, Lcom/google/android/apps/youtube/core/player/event/x;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    const-string v0, "-"

    goto :goto_1
.end method

.method public final handleVideoTimeEvent(Lcom/google/android/apps/youtube/core/player/event/ae;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ae;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->r:I

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->r:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/bf;->b(I)V

    return-void
.end method

.method public final handleYouTubePlayerStateEvent(Lcom/google/android/apps/youtube/core/player/event/af;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/af;->a()I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/af;->a()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/af;->a()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bf;->p:Z

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/af;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bf;->d()V

    goto :goto_1

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bf;->c()V

    goto :goto_1

    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bf;->c()V

    goto :goto_1

    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bf;->d()V

    goto :goto_1

    :pswitch_5
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bf;->d()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
