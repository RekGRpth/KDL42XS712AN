.class public final Lcom/google/android/apps/youtube/core/player/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/ui/c;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lcom/google/android/apps/youtube/datalib/d/a;

.field private final c:Lcom/google/android/apps/youtube/core/client/bj;

.field private final d:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

.field private final e:Lcom/google/android/apps/youtube/datalib/innertube/t;

.field private final f:Lcom/google/android/apps/youtube/core/player/u;

.field private final g:Landroid/support/v4/app/l;

.field private h:Lcom/google/android/apps/youtube/common/a/d;

.field private i:Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;

.field private j:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;Lcom/google/android/apps/youtube/datalib/innertube/t;Lcom/google/android/apps/youtube/core/player/u;Landroid/support/v4/app/l;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->a:Landroid/os/Handler;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/d/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->c:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->d:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/t;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->e:Lcom/google/android/apps/youtube/datalib/innertube/t;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/u;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->f:Lcom/google/android/apps/youtube/core/player/u;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->g:Landroid/support/v4/app/l;

    return-void
.end method

.method private a(ILandroid/graphics/Bitmap;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->i:Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->j:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->i:Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->a(ILandroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->j:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/s;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;I)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/s;ILandroid/graphics/Bitmap;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/core/player/s;->a(ILandroid/graphics/Bitmap;)V

    return-void
.end method

.method private a([B)V
    .locals 3

    if-eqz p1, :cond_0

    array-length v0, p1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->e:Lcom/google/android/apps/youtube/datalib/innertube/t;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/t;->a()Lcom/google/android/apps/youtube/datalib/innertube/u;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/u;->b([B)Lcom/google/android/apps/youtube/datalib/innertube/u;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/s;->e:Lcom/google/android/apps/youtube/datalib/innertube/t;

    const-class v2, Lcom/google/a/a/a/a/il;

    invoke-static {v2}, Lcom/google/android/apps/youtube/datalib/a/m;->a(Ljava/lang/Class;)Lcom/google/android/apps/youtube/datalib/a/l;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/t;->a(Lcom/google/android/apps/youtube/datalib/innertube/u;Lcom/google/android/apps/youtube/datalib/a/l;)V

    :cond_0
    return-void
.end method

.method private b(Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;I)V
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->getInfoCards()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->getInfoCards()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->b()Lcom/google/a/a/a/a/rm;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->b()Lcom/google/a/a/a/a/rm;

    move-result-object v2

    iget-object v2, v2, Lcom/google/a/a/a/a/rm;->b:Lcom/google/a/a/a/a/sx;

    if-nez v2, :cond_2

    :cond_1
    const-string v0, "Image is missing in info card\'s content"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    invoke-direct {p0, p2, v1}, Lcom/google/android/apps/youtube/core/player/s;->a(ILandroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->b()Lcom/google/a/a/a/a/rm;

    move-result-object v0

    iget-object v0, v0, Lcom/google/a/a/a/a/rm;->b:Lcom/google/a/a/a/a/sx;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/t;

    invoke-direct {v2, p0, p2}, Lcom/google/android/apps/youtube/core/player/t;-><init>(Lcom/google/android/apps/youtube/core/player/s;I)V

    if-eqz v0, :cond_3

    iget-object v3, v0, Lcom/google/a/a/a/a/sx;->b:[Lcom/google/a/a/a/a/sy;

    array-length v3, v3

    if-nez v3, :cond_4

    :cond_3
    move-object v0, v1

    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    :goto_2
    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/s;->h:Lcom/google/android/apps/youtube/common/a/d;

    goto :goto_0

    :cond_4
    iget-object v0, v0, Lcom/google/a/a/a/a/sx;->b:[Lcom/google/a/a/a/a/sy;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/google/a/a/a/a/sy;->b:Ljava/lang/String;

    goto :goto_1

    :cond_5
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/s;->c:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/s;->a:Landroid/os/Handler;

    invoke-static {v3, v1}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->h:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->h:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/s;->h:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->i:Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->i:Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->a(Lcom/google/android/apps/youtube/core/player/ui/c;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/s;->i:Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;

    :cond_1
    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/s;->j:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->f:Lcom/google/android/apps/youtube/core/player/u;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/u;->a()V

    return-void
.end method

.method public final a(I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->j:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->getInfoCards()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    const-string v0, "info card index (%d) out of range (size of list: %d)."

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/s;->j:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->getInfoCards()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->j:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->getInfoCards()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->d()Lcom/google/a/a/a/a/rl;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/s;->d:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->d(I)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->d()Lcom/google/a/a/a/a/rl;

    move-result-object v0

    iget-object v0, v0, Lcom/google/a/a/a/a/rl;->c:[B

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/s;->a([B)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;I)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/s;->j:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-static {p1, p2}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;I)Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->i:Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->i:Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->a(Lcom/google/android/apps/youtube/core/player/ui/c;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->g:Landroid/support/v4/app/l;

    invoke-virtual {v0}, Landroid/support/v4/app/l;->a()Landroid/support/v4/app/v;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/s;->g:Landroid/support/v4/app/l;

    const-class v2, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/l;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/v;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/s;->i:Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;

    const-class v2, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/core/player/ui/InfoCardGalleryDialogFragment;->a(Landroid/support/v4/app/v;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/player/s;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;I)V

    return-void
.end method

.method public final b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->j:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->getInfoCards()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/s;->j:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->getInfoCards()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->c()Lcom/google/a/a/a/a/rk;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->c()Lcom/google/a/a/a/a/rk;

    move-result-object v1

    iget-object v1, v1, Lcom/google/a/a/a/a/rk;->c:Lcom/google/a/a/a/a/kz;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/s;->d:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->e(I)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->c()Lcom/google/a/a/a/a/rk;

    move-result-object v1

    iget-object v1, v1, Lcom/google/a/a/a/a/rk;->e:[B

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/player/s;->a([B)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/s;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->c()Lcom/google/a/a/a/a/rk;

    move-result-object v0

    iget-object v0, v0, Lcom/google/a/a/a/a/rk;->c:Lcom/google/a/a/a/a/kz;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/datalib/d/a;->a(Lcom/google/a/a/a/a/kz;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
