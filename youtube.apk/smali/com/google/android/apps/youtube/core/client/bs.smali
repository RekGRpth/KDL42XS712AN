.class public final Lcom/google/android/apps/youtube/core/client/bs;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/youtube/common/e/b;

.field private final c:Lcom/google/android/apps/youtube/medialib/a/a;

.field private final d:Lcom/google/android/apps/youtube/core/utils/o;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/medialib/a/a;Lcom/google/android/apps/youtube/core/utils/o;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bs;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bs;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/medialib/a/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bs;->c:Lcom/google/android/apps/youtube/medialib/a/a;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/utils/o;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bs;->d:Lcom/google/android/apps/youtube/core/utils/o;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/br;
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/core/client/br;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/bs;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/bs;->b:Lcom/google/android/apps/youtube/common/e/b;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/bs;->c:Lcom/google/android/apps/youtube/medialib/a/a;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/client/bs;->d:Lcom/google/android/apps/youtube/core/utils/o;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/client/br;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/medialib/a/a;Lcom/google/android/apps/youtube/core/utils/o;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/br;->a()V

    return-object v0
.end method
