.class public final Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/a;
.implements Lcom/google/android/apps/youtube/core/player/overlay/av;


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/TextView;

.field private f:Lcom/google/android/apps/youtube/core/player/overlay/b;

.field private final g:Lcom/google/android/apps/youtube/core/Analytics;

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:I

.field private l:I

.field private m:I

.field private n:Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;

.field private final o:Landroid/widget/ImageView;

.field private final p:Landroid/util/DisplayMetrics;

.field private final q:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/Analytics;I)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->g:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->p:Landroid/util/DisplayMetrics;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->M:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->l:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->a:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->m:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->q:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->eL:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->eN:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->eM:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->o:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->q:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->d:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->q:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v1, p3

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/u;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/player/overlay/u;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/overlay/v;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/player/overlay/v;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/w;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/youtube/core/player/overlay/w;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;Lcom/google/android/apps/youtube/core/Analytics;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->d()V

    return-void
.end method

.method private a()V
    .locals 7

    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->h:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/youtube/p;->g:I

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, ""

    aput-object v4, v3, v5

    const-string v4, ""

    aput-object v4, v3, v6

    invoke-virtual {v2, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_0
    sget v0, Lcom/google/android/youtube/p;->h:I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, " \u00b7 "

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->j:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v2, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->n:Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;->SKIPPABLE:Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->g:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "SkipAd"

    const-string v2, "Overlay"

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->f:Lcom/google/android/apps/youtube/core/player/overlay/b;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->l:I

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->m:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/overlay/b;->a(II)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;Landroid/view/MotionEvent;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->l:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->m:I

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->f:Lcom/google/android/apps/youtube/core/player/overlay/b;

    return-object v0
.end method

.method private e()V
    .locals 7

    const/16 v1, 0x8

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->q:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->h:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->getWidth()I

    move-result v3

    const/4 v4, 0x1

    const/high16 v5, 0x43fa0000    # 500.0f

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->p:Landroid/util/DisplayMetrics;

    invoke-static {v4, v5, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v4, v4

    if-ge v3, v4, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private f()V
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->n:Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;->NOT_SKIPPABLE:Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->n:Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;->WAITING_TO_SKIP:Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->k:I

    if-gtz v0, :cond_4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;->SKIPPABLE:Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->n:Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/p;->fD:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->h:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->f:Lcom/google/android/apps/youtube/core/player/overlay/b;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->f:Lcom/google/android/apps/youtube/core/player/overlay/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/b;->c()V

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->b:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/p;->fE:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->k:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    sub-int v0, p2, p1

    div-int/lit16 v0, v0, 0x3e8

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/e/m;->a(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->j:Ljava/lang/String;

    div-int/lit16 v0, p1, 0x3e8

    rsub-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->k:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->f()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->a()V

    return-void
.end method

.method public final a(Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 2

    const/4 v1, -0x1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->i:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->j:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->l:I

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->m:I

    if-eqz p2, :cond_0

    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->k:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->o:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;->WAITING_TO_SKIP:Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->n:Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->a()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->f()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->setVisibility(I)V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;->NOT_SKIPPABLE:Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->n:Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay$State;

    goto :goto_0
.end method

.method public final b()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final c()Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public final d()V
    .locals 3

    const/4 v2, 0x0

    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method protected final onSizeChanged(IIII)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->e()V

    return-void
.end method

.method public final setAdTitle(Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->i:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setChannel(Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public final setFullscreen(Z)V
    .locals 0

    return-void
.end method

.method public final setListener(Lcom/google/android/apps/youtube/core/player/overlay/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->f:Lcom/google/android/apps/youtube/core/player/overlay/b;

    return-void
.end method

.method public final setMinimized(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->h:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->f()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->a()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;->e()V

    return-void
.end method
