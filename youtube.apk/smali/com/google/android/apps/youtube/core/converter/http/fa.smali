.class final Lcom/google/android/apps/youtube/core/converter/http/fa;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/converter/http/dj;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/converter/http/dj;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/converter/http/fa;->a:Lcom/google/android/apps/youtube/core/converter/http/dj;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/common/e/l;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/converter/http/fk;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    iget v0, v0, Lcom/google/android/apps/youtube/core/converter/http/fk;->a:I

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;-><init>(I)V

    const-string v0, "id"

    invoke-interface {p2, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->f(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 5

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/common/e/l;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/converter/http/fk;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/converter/http/fa;->a:Lcom/google/android/apps/youtube/core/converter/http/dj;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/core/converter/http/dj;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    :try_end_0
    .catch Lcom/google/android/apps/youtube/core/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/converter/http/fk;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/core/converter/http/fk;

    :cond_1
    return-void

    :catch_0
    move-exception v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Survey convertion error: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->EMPTY:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_0
.end method
