.class final Lcom/google/android/apps/youtube/core/ui/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/ui/n;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/ui/n;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/ui/o;->a:Lcom/google/android/apps/youtube/core/ui/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/o;->a:Lcom/google/android/apps/youtube/core/ui/n;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/n;->dismiss()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/o;->a:Lcom/google/android/apps/youtube/core/ui/n;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/ui/n;->f(Lcom/google/android/apps/youtube/core/ui/n;)Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "ChannelCreationError"

    invoke-virtual {p2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/o;->a:Lcom/google/android/apps/youtube/core/ui/n;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/ui/n;->g(Lcom/google/android/apps/youtube/core/ui/n;)Lcom/google/android/apps/youtube/core/identity/y;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/youtube/core/identity/y;->a(Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Lcom/google/android/apps/youtube/core/model/plus/Person;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/o;->a:Lcom/google/android/apps/youtube/core/ui/n;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/ui/n;->a(Lcom/google/android/apps/youtube/core/ui/n;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/o;->a:Lcom/google/android/apps/youtube/core/ui/n;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/ui/n;->a(Lcom/google/android/apps/youtube/core/ui/n;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/o;->a:Lcom/google/android/apps/youtube/core/ui/n;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/ui/n;->b(Lcom/google/android/apps/youtube/core/ui/n;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/youtube/core/model/plus/Person;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/o;->a:Lcom/google/android/apps/youtube/core/ui/n;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/ui/n;->e(Lcom/google/android/apps/youtube/core/ui/n;)Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/youtube/core/model/plus/Person;->imageUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/ui/o;->a:Lcom/google/android/apps/youtube/core/ui/n;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/ui/n;->c(Lcom/google/android/apps/youtube/core/ui/n;)Landroid/app/Activity;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/youtube/core/ui/p;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/core/ui/p;-><init>(Lcom/google/android/apps/youtube/core/ui/o;)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
