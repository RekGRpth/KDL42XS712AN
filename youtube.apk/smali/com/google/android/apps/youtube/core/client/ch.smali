.class public final Lcom/google/android/apps/youtube/core/client/ch;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/common/e/b;

.field private final b:Lcom/google/android/apps/youtube/core/Analytics;

.field private final c:Lcom/google/android/apps/youtube/common/network/h;

.field private final d:Lcom/google/android/apps/youtube/common/c/a;

.field private final e:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

.field private final f:Lcom/google/android/apps/youtube/core/client/cf;

.field private final g:Lcom/google/android/apps/youtube/datalib/e/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/DeviceClassification;)V
    .locals 8

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/client/ch;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Lcom/google/android/apps/youtube/core/client/cf;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Lcom/google/android/apps/youtube/core/client/cf;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/ch;->g:Lcom/google/android/apps/youtube/datalib/e/b;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ch;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ch;->b:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ch;->c:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ch;->d:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ch;->e:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    iput-object p7, p0, Lcom/google/android/apps/youtube/core/client/ch;->f:Lcom/google/android/apps/youtube/core/client/cf;

    return-void
.end method

.method private a(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZZLjava/lang/String;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/player/al;Ljava/util/List;)Lcom/google/android/apps/youtube/core/client/VideoStats2Client;
    .locals 27

    new-instance v1, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/core/client/ch;->g:Lcom/google/android/apps/youtube/datalib/e/b;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/core/client/ch;->a:Lcom/google/android/apps/youtube/common/e/b;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/core/client/ch;->b:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static/range {p7 .. p7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/core/client/ch;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v5}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v16

    const-string v19, "-"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/client/ch;->c:Lcom/google/android/apps/youtube/common/network/h;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/client/ch;->d:Lcom/google/android/apps/youtube/common/c/a;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/client/ch;->e:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/client/ch;->f:Lcom/google/android/apps/youtube/core/client/cf;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p5

    move-object/from16 v15, p6

    move-object/from16 v18, p12

    move-object/from16 v20, p13

    move-object/from16 v24, p14

    invoke-direct/range {v1 .. v26}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/Analytics;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/google/android/apps/youtube/core/client/WatchFeature;Ljava/lang/String;Lcom/google/android/apps/youtube/core/player/al;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Ljava/util/List;Lcom/google/android/apps/youtube/core/client/cf;B)V

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a(Lcom/google/android/apps/youtube/core/client/VideoStats2Client;)V

    return-object v1
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/core/player/al;Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;)Lcom/google/android/apps/youtube/core/client/VideoStats2Client;
    .locals 12

    new-instance v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/ch;->g:Lcom/google/android/apps/youtube/datalib/e/b;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/ch;->a:Lcom/google/android/apps/youtube/common/e/b;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/ch;->b:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/youtube/core/player/al;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/client/ch;->c:Lcom/google/android/apps/youtube/common/network/h;

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/client/ch;->d:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v8, p0, Lcom/google/android/apps/youtube/core/client/ch;->e:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    iget-object v9, p2, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->videoId:Ljava/lang/String;

    new-instance v9, Ljava/util/LinkedList;

    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V

    iget-object v10, p0, Lcom/google/android/apps/youtube/core/client/ch;->f:Lcom/google/android/apps/youtube/core/client/cf;

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;Lcom/google/android/apps/youtube/core/player/al;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Ljava/util/List;Lcom/google/android/apps/youtube/core/client/cf;B)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a(Lcom/google/android/apps/youtube/core/client/VideoStats2Client;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/player/al;)Lcom/google/android/apps/youtube/core/client/VideoStats2Client;
    .locals 16

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->getBaseUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->getBaseUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->getBaseUri()Landroid/net/Uri;

    move-result-object v4

    invoke-static/range {p4 .. p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {p5 .. p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    if-eqz p7, :cond_0

    const/16 v1, 0x1e

    :goto_0
    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->getElapsedMediaTimeSec(I)I

    move-result v9

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-static/range {p8 .. p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-static/range {p9 .. p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-static/range {p10 .. p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/apps/youtube/core/player/al;

    new-instance v15, Ljava/util/LinkedList;

    invoke-direct {v15}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v1, p0

    move/from16 v8, p6

    invoke-direct/range {v1 .. v15}, Lcom/google/android/apps/youtube/core/client/ch;->a(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZZLjava/lang/String;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/player/al;Ljava/util/List;)Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    move-result-object v1

    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZLcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/player/al;)Lcom/google/android/apps/youtube/core/client/VideoStats2Client;
    .locals 16

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->getBaseUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->getBaseUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->getBaseUri()Landroid/net/Uri;

    move-result-object v4

    invoke-static/range {p4 .. p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static/range {p5 .. p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz p8, :cond_0

    const/4 v1, 0x4

    :goto_0
    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->getElapsedMediaTimeSec(I)I

    move-result v9

    const/4 v12, 0x0

    invoke-static/range {p10 .. p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-static/range {p11 .. p11}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/apps/youtube/core/player/al;

    new-instance v15, Ljava/util/LinkedList;

    invoke-direct {v15}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v1, p0

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v10, p8

    move/from16 v11, p9

    invoke-direct/range {v1 .. v15}, Lcom/google/android/apps/youtube/core/client/ch;->a(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZZLjava/lang/String;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/player/al;Ljava/util/List;)Lcom/google/android/apps/youtube/core/client/VideoStats2Client;

    move-result-object v1

    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
