.class public final Lcom/google/android/apps/youtube/core/client/a/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/client/a/c;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/identity/l;

.field private final b:Lcom/google/android/apps/common/csi/lib/c;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/android/apps/youtube/common/network/h;

.field private final e:Landroid/content/SharedPreferences;

.field private final f:Lcom/google/android/apps/youtube/common/c/a;

.field private g:Ljava/util/List;

.field private h:Ljava/util/List;

.field private i:Ljava/util/List;

.field private j:Ljava/util/List;

.field private k:Ljava/util/List;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/common/csi/lib/c;Ljava/lang/String;Lcom/google/android/apps/youtube/common/network/h;Landroid/content/SharedPreferences;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/h;->g:Ljava/util/List;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/h;->h:Ljava/util/List;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/h;->i:Ljava/util/List;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/h;->j:Ljava/util/List;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/h;->k:Ljava/util/List;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/h;->f:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/h;->a:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/common/csi/lib/c;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/h;->b:Lcom/google/android/apps/common/csi/lib/c;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/h;->c:Ljava/lang/String;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/h;->d:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/h;->e:Landroid/content/SharedPreferences;

    return-void
.end method

.method public static a(Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/identity/l;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/common/network/h;Landroid/content/SharedPreferences;Lcom/google/android/apps/common/csi/lib/Sender;)Lcom/google/android/apps/youtube/core/client/a/h;
    .locals 7

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "packageName cannot be null or empty."

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v0, "version cannot be null or empty."

    invoke-static {p3, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p4, :cond_0

    const-string v4, ""

    :goto_0
    new-instance v0, Lcom/google/android/apps/common/csi/lib/a;

    invoke-direct {v0}, Lcom/google/android/apps/common/csi/lib/a;-><init>()V

    const-string v1, "youtube_android"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/csi/lib/a;->a(Ljava/lang/String;)Lcom/google/android/apps/common/csi/lib/a;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/common/csi/lib/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/common/csi/lib/a;

    move-result-object v0

    const-string v1, "https://csi.gstatic.com/csi"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/csi/lib/a;->b(Ljava/lang/String;)Lcom/google/android/apps/common/csi/lib/a;

    move-result-object v0

    invoke-virtual {v0, p7}, Lcom/google/android/apps/common/csi/lib/a;->a(Lcom/google/android/apps/common/csi/lib/Sender;)Lcom/google/android/apps/common/csi/lib/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/common/csi/lib/a;->a(I)Lcom/google/android/apps/common/csi/lib/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/common/csi/lib/f;->a(Lcom/google/android/apps/common/csi/lib/a;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/client/a/h;

    invoke-static {}, Lcom/google/android/apps/common/csi/lib/f;->a()Lcom/google/android/apps/common/csi/lib/c;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/client/a/h;-><init>(Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/common/csi/lib/c;Ljava/lang/String;Lcom/google/android/apps/youtube/common/network/h;Landroid/content/SharedPreferences;)V

    return-object v0

    :cond_0
    move-object v4, p4

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/client/a/h;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/h;->k:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/client/a/h;)Lcom/google/android/apps/common/csi/lib/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/h;->b:Lcom/google/android/apps/common/csi/lib/c;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;Lcom/google/android/apps/youtube/core/client/a/b;)Lcom/google/android/apps/youtube/core/client/a/e;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/core/client/a/j;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p2, v1}, Lcom/google/android/apps/youtube/core/client/a/j;-><init>(Lcom/google/android/apps/youtube/core/client/a/h;Lcom/google/android/apps/youtube/core/client/a/b;B)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/a/h;->f:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v1, p0, p1, v0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/apps/youtube/common/c/d;)Lcom/google/android/apps/youtube/common/c/e;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/a/h;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public final a()Lcom/google/android/apps/youtube/core/identity/l;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/h;->a:Lcom/google/android/apps/youtube/core/identity/l;

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Lcom/google/android/apps/youtube/core/client/a/d;)V
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/core/client/a/i;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/youtube/core/client/a/i;-><init>(Lcom/google/android/apps/youtube/core/client/a/h;Lcom/google/android/apps/youtube/core/client/a/d;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/a/h;->f:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v1, p0, p1, v0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/apps/youtube/common/c/d;)Lcom/google/android/apps/youtube/common/c/e;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/a/h;->j:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Ljava/lang/Class;Lcom/google/android/apps/youtube/core/client/a/f;)V
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/core/client/a/l;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p2, v1}, Lcom/google/android/apps/youtube/core/client/a/l;-><init>(Lcom/google/android/apps/youtube/core/client/a/h;Lcom/google/android/apps/youtube/core/client/a/f;B)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/a/h;->f:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v1, p0, p1, v0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/apps/youtube/common/c/d;)Lcom/google/android/apps/youtube/common/c/e;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/a/h;->i:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/core/client/a/k;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p2, v1}, Lcom/google/android/apps/youtube/core/client/a/k;-><init>(Lcom/google/android/apps/youtube/core/client/a/h;Ljava/lang/String;B)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/a/h;->f:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v1, p0, p1, v0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/apps/youtube/common/c/d;)Lcom/google/android/apps/youtube/common/c/e;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/a/h;->h:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/h;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/google/a/a/a/a/dt;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/h;->e:Landroid/content/SharedPreferences;

    const-string v1, "csi_params_from_innertube"

    new-instance v2, Lcom/google/a/a/a/a/dt;

    invoke-direct {v2}, Lcom/google/a/a/a/a/dt;-><init>()V

    invoke-static {v2}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v2

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :try_start_0
    new-instance v0, Lcom/google/a/a/a/a/dt;

    invoke-direct {v0}, Lcom/google/a/a/a/a/dt;-><init>()V

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "Error parsing CsiParams from preferences"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    new-instance v0, Lcom/google/a/a/a/a/dt;

    invoke-direct {v0}, Lcom/google/a/a/a/a/dt;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/a/a/a/a/ix;

    new-instance v2, Lcom/google/a/a/a/a/ix;

    invoke-direct {v2}, Lcom/google/a/a/a/a/ix;-><init>()V

    aput-object v2, v1, v3

    iput-object v1, v0, Lcom/google/a/a/a/a/dt;->b:[Lcom/google/a/a/a/a/ix;

    iget-object v1, v0, Lcom/google/a/a/a/a/dt;->b:[Lcom/google/a/a/a/a/ix;

    aget-object v1, v1, v3

    const-string v2, "csiParamsParseException"

    iput-object v2, v1, Lcom/google/a/a/a/a/ix;->b:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/a/a/a/a/dt;->b:[Lcom/google/a/a/a/a/ix;

    aget-object v1, v1, v3

    const-string v2, "true"

    iput-object v2, v1, Lcom/google/a/a/a/a/ix;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final d()Lcom/google/android/apps/youtube/common/network/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/h;->d:Lcom/google/android/apps/youtube/common/network/h;

    return-object v0
.end method
