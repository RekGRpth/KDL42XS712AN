.class public final enum Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

.field public static final enum AUTOPLAY:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

.field public static final enum JUMP:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

.field public static final enum NEXT:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

.field public static final enum PREVIOUS:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

.field public static final enum RETRY:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

.field public static final enum START:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    const-string v1, "START"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->START:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    const-string v1, "NEXT"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->NEXT:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    const-string v1, "PREVIOUS"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->PREVIOUS:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    const-string v1, "AUTOPLAY"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->AUTOPLAY:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    const-string v1, "RETRY"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->RETRY:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    const-string v1, "JUMP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->JUMP:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->START:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->NEXT:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->PREVIOUS:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->AUTOPLAY:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->RETRY:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->JUMP:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->$VALUES:[Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->$VALUES:[Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    return-object v0
.end method


# virtual methods
.method public final varargs isIn([Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;)Z
    .locals 4

    const/4 v0, 0x0

    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    if-ne p0, v3, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
