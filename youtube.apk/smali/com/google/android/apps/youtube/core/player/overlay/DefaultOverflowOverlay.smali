.class public final Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field private a:Landroid/widget/LinearLayout;

.field private b:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

.field private c:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

.field private d:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

.field private e:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

.field private f:Landroid/view/animation/Animation;

.field private g:Landroid/view/animation/Animation;

.field private h:Landroid/view/animation/Animation;

.field private i:Landroid/view/animation/Animation;

.field private j:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Lcom/google/android/apps/youtube/core/player/overlay/af;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget v0, Lcom/google/android/youtube/b;->c:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->f:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/youtube/b;->d:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->g:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->g:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/k;->j:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->f:Landroid/view/animation/Animation;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->g:Landroid/view/animation/Animation;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    sget v0, Lcom/google/android/youtube/b;->k:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->h:Landroid/view/animation/Animation;

    sget v0, Lcom/google/android/youtube/b;->l:I

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->i:Landroid/view/animation/Animation;

    return-void
.end method

.method private e()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->c:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->j:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->supportsPlayHQCC:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->b:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->l:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->j:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-boolean v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->supportsPlayHQCC:Z

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->e:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->m:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setEnabled(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->clearAnimation()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->a:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->h:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->f:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public final b()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->clearAnimation()V

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->g:Landroid/view/animation/Animation;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setStartTime(J)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->c:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->n:Lcom/google/android/apps/youtube/core/player/overlay/af;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/af;->b()V

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->k:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->l:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->m:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->e()V

    return-void
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->g:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->g:Landroid/view/animation/Animation;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setStartTime(J)V

    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->b:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->n:Lcom/google/android/apps/youtube/core/player/overlay/af;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/af;->a()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->g:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->a:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->i:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->g:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->n:Lcom/google/android/apps/youtube/core/player/overlay/af;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/af;->e()V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->c:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->n:Lcom/google/android/apps/youtube/core/player/overlay/af;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/af;->b()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->d:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->n:Lcom/google/android/apps/youtube/core/player/overlay/af;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/af;->c()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->e:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->n:Lcom/google/android/apps/youtube/core/player/overlay/af;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/af;->d()V

    goto :goto_0
.end method

.method protected final onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    sget v0, Lcom/google/android/youtube/j;->cY:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->a:Landroid/widget/LinearLayout;

    sget v0, Lcom/google/android/youtube/j;->bA:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->b:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->b:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->af:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->c:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->c:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->bp:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->d:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->d:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->q:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->e:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->e:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setShowAudioOnly(Z)V

    invoke-virtual {p0, p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setAudioOnlyEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->e:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setSelected(Z)V

    return-void
.end method

.method public final setCC(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->c:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setSelected(Z)V

    return-void
.end method

.method public final setHQ(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->b:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setSelected(Z)V

    return-void
.end method

.method public final setHQisHD(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->b:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/youtube/h;->aq:I

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setImageResource(I)V

    return-void

    :cond_0
    sget v0, Lcom/google/android/youtube/h;->ar:I

    goto :goto_0
.end method

.method public final setHasAudioOnly(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->m:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->e()V

    return-void
.end method

.method public final setHasCC(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->k:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->e()V

    return-void
.end method

.method public final setHasQuality(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->l:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->e()V

    return-void
.end method

.method public final setListener(Lcom/google/android/apps/youtube/core/player/overlay/af;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->n:Lcom/google/android/apps/youtube/core/player/overlay/af;

    return-void
.end method

.method public final setShowAudioOnly(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->e:Lcom/google/android/apps/youtube/core/ui/TouchImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/ui/TouchImageView;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->j:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    return-void
.end method
