.class public final Lcom/google/android/apps/youtube/core/converter/http/f;
.super Lcom/google/android/apps/youtube/core/converter/http/ap;
.source "SourceFile"


# static fields
.field private static final c:Ljava/util/Set;


# instance fields
.field private final d:Lcom/google/android/apps/youtube/core/converter/e;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x16

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "channel.global.title.string"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "channel.global.description.string"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "channel.global.keywords.string"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "mobile_watchpage.banner.image.url"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "mobile_watchpage.banner.image_target.url"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "watchpage.global.featured_playlist.id"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "watchpage.large_branded_banner.image.url"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "device_watchpage.watermark.image.url"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "device_watchpage.watermark.image_target.url"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "device_watchpage.interstitial.image.url"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "device_watchpage.interstitial.image_target.url"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "generictv_watchpage.banner.image.url"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "channel.banner.mobile.low.image.url"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "channel.banner.mobile.medium.image.url"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "channel.banner.mobile.medium_hd.image.url"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "channel.banner.mobile.hd.image.url"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "channel.banner.mobile.extra_hd.image.url"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "channel.banner.tablet.low.image.url"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "channel.banner.tablet.medium.image.url"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "channel.banner.tablet.hd.image.url"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "channel.banner.tablet.extra_hd.image.url"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "channel.banner.tv.image.url"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/core/converter/http/f;->c:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/converter/n;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/converter/http/ap;-><init>(Lcom/google/android/apps/youtube/core/converter/n;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/f;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/converter/f;-><init>()V

    const-string v1, "/entry"

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/h;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/core/converter/http/h;-><init>(Lcom/google/android/apps/youtube/core/converter/http/f;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    const-string v1, "/entry/yt:option"

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/g;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/core/converter/http/g;-><init>(Lcom/google/android/apps/youtube/core/converter/http/f;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/converter/f;->a()Lcom/google/android/apps/youtube/core/converter/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/f;->d:Lcom/google/android/apps/youtube/core/converter/e;

    return-void
.end method

.method static synthetic b()Ljava/util/Set;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/converter/http/f;->c:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method protected final a()Lcom/google/android/apps/youtube/core/converter/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/f;->d:Lcom/google/android/apps/youtube/core/converter/e;

    return-object v0
.end method
