.class public final Lcom/google/android/apps/youtube/core/client/ar;
.super Lcom/google/android/apps/youtube/core/client/m;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/client/ce;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/common/cache/a;

.field private final h:Lcom/google/android/apps/youtube/core/async/af;

.field private final i:Lcom/google/android/apps/youtube/core/async/af;

.field private final j:Lcom/google/android/apps/youtube/core/async/af;

.field private final k:Lcom/google/android/apps/youtube/core/async/af;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/core/client/m;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;)V

    const/16 v0, 0x14

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/client/ar;->a(I)Lcom/google/android/apps/youtube/common/cache/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ar;->a:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/ar;->a()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ar;->h:Lcom/google/android/apps/youtube/core/async/af;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/ar;->b()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ar;->i:Lcom/google/android/apps/youtube/core/async/af;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/ar;->c()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ar;->j:Lcom/google/android/apps/youtube/core/async/af;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/ar;->f()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ar;->k:Lcom/google/android/apps/youtube/core/async/af;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/n;Ljava/lang/String;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 1

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/youtube/core/client/m;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/n;Ljava/lang/String;Lcom/google/android/apps/youtube/common/e/b;)V

    const/16 v0, 0x14

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/client/ar;->a(I)Lcom/google/android/apps/youtube/common/cache/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ar;->a:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/ar;->a()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ar;->h:Lcom/google/android/apps/youtube/core/async/af;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/ar;->b()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ar;->i:Lcom/google/android/apps/youtube/core/async/af;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/ar;->c()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ar;->j:Lcom/google/android/apps/youtube/core/async/af;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/ar;->f()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ar;->k:Lcom/google/android/apps/youtube/core/async/af;

    return-void
.end method

.method private a()Lcom/google/android/apps/youtube/core/async/af;
    .locals 5

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/dg;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/converter/http/dg;-><init>()V

    const/16 v1, 0x14

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/client/ar;->a(I)Lcom/google/android/apps/youtube/common/cache/b;

    move-result-object v1

    invoke-virtual {p0, v0, v0}, Lcom/google/android/apps/youtube/core/client/ar;->a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/ar;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/client/ar;->e()Lcom/google/android/apps/youtube/common/cache/c;

    move-result-object v2

    const-wide/32 v3, 0x240c8400

    invoke-virtual {p0, v2, v0, v3, v4}, Lcom/google/android/apps/youtube/core/client/ar;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/client/ar;->a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v0

    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/apps/youtube/core/client/ar;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v0

    return-object v0
.end method

.method private b()Lcom/google/android/apps/youtube/core/async/af;
    .locals 4

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/di;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/ar;->g:Lcom/google/android/apps/youtube/core/converter/n;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/converter/http/di;-><init>(Lcom/google/android/apps/youtube/core/converter/n;)V

    invoke-virtual {p0, v0, v0}, Lcom/google/android/apps/youtube/core/client/ar;->a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/ar;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/client/ar;->e()Lcom/google/android/apps/youtube/common/cache/c;

    move-result-object v1

    const-wide/32 v2, 0x240c8400

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/apps/youtube/core/client/ar;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/client/ar;->a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/ar;->a:Lcom/google/android/apps/youtube/common/cache/a;

    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/apps/youtube/core/client/ar;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v0

    return-object v0
.end method

.method private c()Lcom/google/android/apps/youtube/core/async/af;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/di;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/ar;->g:Lcom/google/android/apps/youtube/core/converter/n;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/converter/http/di;-><init>(Lcom/google/android/apps/youtube/core/converter/n;)V

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/http/ba;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/converter/http/ba;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/client/ar;->a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/client/ar;->a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v0

    return-object v0
.end method

.method private f()Lcom/google/android/apps/youtube/core/async/af;
    .locals 4

    new-instance v0, Lcom/google/android/apps/youtube/core/async/aa;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/ar;->g:Lcom/google/android/apps/youtube/core/converter/n;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/async/aa;-><init>(Lcom/google/android/apps/youtube/core/converter/n;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/client/ar;->a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/ar;->a:Lcom/google/android/apps/youtube/common/cache/a;

    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/apps/youtube/core/client/ar;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->offlineSubtitlesPath:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ar;->k:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ar;->i:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/dh;

    invoke-direct {v0, p2}, Lcom/google/android/apps/youtube/core/converter/http/dh;-><init>(Lcom/google/android/apps/youtube/common/a/b;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/ar;->h:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v1, p1, v0}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ar;->j:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
