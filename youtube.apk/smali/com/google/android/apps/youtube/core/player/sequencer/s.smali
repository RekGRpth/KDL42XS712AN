.class public Lcom/google/android/apps/youtube/core/player/sequencer/s;
.super Lcom/google/android/apps/youtube/core/player/sequencer/a;
.source "SourceFile"


# instance fields
.field protected l:[Ljava/lang/String;

.field protected final m:[B

.field protected n:Ljava/lang/String;

.field protected o:Lcom/google/android/apps/youtube/common/a/d;

.field protected p:I

.field protected q:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;)V
    .locals 7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/player/sequencer/a;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;)V

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p7, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->videoIds:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->l:[Ljava/lang/String;

    iget-object v0, p7, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->clickTrackingParams:[B

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->m:[B

    iget-object v0, p7, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->playerParams:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->n:Ljava/lang/String;

    iget v0, p7, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->index:I

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->p:I

    iget v0, p7, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->pendingIndex:I

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->q:I

    iget-object v0, p7, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->currentPlayerResponse:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-boolean v0, p7, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->loop:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->j:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->i()V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->NEW:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/aw;Ljava/util/List;I[BLjava/lang/String;)V
    .locals 8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p6

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/youtube/core/player/sequencer/a;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;)V

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p9 .. p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->m:[B

    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->n:Ljava/lang/String;

    invoke-interface {p7}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {p7, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->l:[Ljava/lang/String;

    move/from16 v0, p8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->b(I)I

    move-result v1

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->p:I

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->q:I

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->NEW:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->i()V

    return-void
.end method

.method private u()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->j:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->c(I)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->p:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->c(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->o:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->o:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->o:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->p:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->p:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->l:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_2

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->NEW:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    :goto_0
    return-void

    :cond_2
    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->h()V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    return-void
.end method

.method protected b(I)I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->l:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->l:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->j:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->h()V

    return-void
.end method

.method protected b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final c()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->p:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->p:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->l:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->l:[Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->p:I

    aget-object v0, v0, v1

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->q:I

    if-ltz v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->q:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->l:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->l:[Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->q:I

    aget-object v0, v0, v1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected c(I)V
    .locals 8

    const/4 v5, -0x1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->b(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->q:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->l:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_1

    if-ltz p1, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/sequencer/t;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/player/sequencer/t;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/s;)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->o:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->a:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->l:[Ljava/lang/String;

    aget-object v1, v1, p1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->m:[B

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->n:Ljava/lang/String;

    const-string v4, ""

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->o:Lcom/google/android/apps/youtube/common/a/d;

    move v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/common/a/b;)V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_LOADING:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->l:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->q:I

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->p:I

    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->SEQUENCE_EMPTY:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    goto :goto_0
.end method

.method protected d()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method protected e()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->m:[B

    return-object v0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->p:I

    return v0
.end method

.method public final l()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->a()V

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->l()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->b:Lcom/google/android/apps/youtube/core/player/sequencer/r;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/r;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;)V

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->q:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->c(I)V

    goto :goto_1
.end method

.method public final m()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->a()V

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->m()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->u()V

    return-void
.end method

.method public final n()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->a()V

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->n()V

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->p:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->c(I)V

    return-void
.end method

.method public final o()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->a()V

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->o()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->u()V

    return-void
.end method

.method protected final o_()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final p()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->a()V

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->p()V

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->q:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->c(I)V

    return-void
.end method

.method public p_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final q_()Z
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->j:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->t()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerState;
    .locals 8

    new-instance v0, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->l:[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->m:[B

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->n:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->p:I

    iget v5, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->q:I

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-boolean v7, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->j:Z

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;-><init>([Ljava/lang/String;[BLjava/lang/String;IILcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Z)V

    return-object v0
.end method

.method public final r_()Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->p:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    const-string v0, ""

    return-object v0
.end method

.method public final s_()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->q_()Z

    move-result v0

    return v0
.end method

.method protected t()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->p:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->l:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
