.class public final Lcom/google/android/apps/youtube/core/identity/ar;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/identity/l;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/identity/l;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/ar;->a:Lcom/google/android/apps/youtube/core/identity/l;

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3

    const-string v0, "on-behalf-of"

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/ar;->a:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->g()Lcom/google/android/apps/youtube/core/identity/f;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/core/identity/f;->a:Lcom/google/android/apps/youtube/core/identity/f;

    if-eq v1, v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/f;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    :cond_0
    return-object p1
.end method
