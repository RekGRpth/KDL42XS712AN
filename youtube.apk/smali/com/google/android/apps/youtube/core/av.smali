.class public final Lcom/google/android/apps/youtube/core/av;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/Analytics;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/client/l;

.field private final b:Lcom/google/android/apps/youtube/common/network/h;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/client/l;Lcom/google/android/apps/youtube/common/network/h;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "analyticsClient cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/av;->a:Lcom/google/android/apps/youtube/core/client/l;

    const-string v0, "networkStatus cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/av;->b:Lcom/google/android/apps/youtube/common/network/h;

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/av;->a:Lcom/google/android/apps/youtube/core/client/l;

    const/4 v1, 0x2

    invoke-interface {v0, p1, p2, p3, v1}, Lcom/google/android/apps/youtube/core/client/l;->a(ILjava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;I)V
    .locals 2

    const-string v0, "PlaySelected"

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p2}, Lcom/google/android/apps/youtube/core/av;->a(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/av;->a:Lcom/google/android/apps/youtube/core/client/l;

    const-string v2, "PageView"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " < "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/av;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "Entry"

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, -0x1

    invoke-interface {v1, v2, v0, v3}, Lcom/google/android/apps/youtube/core/client/l;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/av;->c:Ljava/lang/String;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/av;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/youtube/core/av;->a(Ljava/lang/String;II)V

    return-void
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 2

    if-gtz p2, :cond_0

    const-string v0, "?"

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/av;->b:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/network/h;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3}, Lcom/google/android/apps/youtube/core/av;->a(Ljava/lang/String;Ljava/lang/String;I)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "STREAM_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "P"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/youtube/core/av;->a(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/av;->a:Lcom/google/android/apps/youtube/core/client/l;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/client/l;->a(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/apps/youtube/core/av;->a(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method
