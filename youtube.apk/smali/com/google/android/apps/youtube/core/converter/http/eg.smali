.class final Lcom/google/android/apps/youtube/core/converter/http/eg;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/common/e/l;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {p3, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x6d

    if-ne v1, v2, :cond_1

    sget-object v1, Lcom/google/android/apps/youtube/core/identity/UserProfile$Gender;->MALE:Lcom/google/android/apps/youtube/core/identity/UserProfile$Gender;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->gender(Lcom/google/android/apps/youtube/core/identity/UserProfile$Gender;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p3, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x66

    if-ne v1, v2, :cond_0

    sget-object v1, Lcom/google/android/apps/youtube/core/identity/UserProfile$Gender;->FEMALE:Lcom/google/android/apps/youtube/core/identity/UserProfile$Gender;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->gender(Lcom/google/android/apps/youtube/core/identity/UserProfile$Gender;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    goto :goto_0
.end method
