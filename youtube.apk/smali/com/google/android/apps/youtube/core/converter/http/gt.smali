.class public final Lcom/google/android/apps/youtube/core/converter/http/gt;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static synthetic a(Ljava/lang/String;)Landroid/util/Pair;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/converter/http/gt;->d(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/youtube/core/converter/f;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/converter/http/dj;)V
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "/VMAP"

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/converter/http/gt;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/http/gu;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/converter/http/gu;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    const-string v0, "/VMAP/AdBreak"

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/converter/http/gt;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/http/gv;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/converter/http/gv;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    const-string v0, "/VMAP/AdBreak/TrackingEvents/Tracking"

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/converter/http/gt;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/http/gw;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/converter/http/gw;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    const-string v0, "/VMAP/Extensions/Extension"

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/converter/http/gt;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/TrackingDecoration"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/http/gx;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/converter/http/gx;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    const-string v0, "/VMAP/AdBreak/AdSource/VASTData"

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/converter/http/gt;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/http/gy;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/converter/http/gy;-><init>()V

    invoke-static {v0, p1, p0, v1, p2}, Lcom/google/android/apps/youtube/core/converter/http/em;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/converter/f;Lcom/google/android/apps/youtube/core/converter/http/fl;Lcom/google/android/apps/youtube/core/converter/http/dj;)V

    return-void
.end method

.method static synthetic a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/bj;)V
    .locals 4

    if-nez p0, :cond_1

    const-string v0, "in Vmap AdBreak: timeOffset is null"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v0, v1

    const/4 v2, 0x1

    if-le v0, v2, :cond_2

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-static {v3, p1}, Lcom/google/android/apps/youtube/core/converter/http/gt;->b(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/bj;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-static {p0, p1}, Lcom/google/android/apps/youtube/core/converter/http/gt;->b(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/bj;)V

    goto :goto_0
.end method

.method static synthetic b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$TrackingEventType;
    .locals 1

    const-string v0, "breakEnd"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$TrackingEventType;->END:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$TrackingEventType;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "error"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$TrackingEventType;->ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$TrackingEventType;

    goto :goto_0

    :cond_1
    const-string v0, "breakStart"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$TrackingEventType;->START:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$TrackingEventType;

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/bj;)V
    .locals 2

    const/4 v1, 0x1

    const-string v0, "linear"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "nonlinear"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->b(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    goto :goto_0

    :cond_2
    const-string v0, "display"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->c(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "\\/"

    const-string v1, "/vmap:"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static d(Ljava/lang/String;)Landroid/util/Pair;
    .locals 6

    const/16 v1, 0x64

    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v0, "in Vmap AdBreak: timeOffset is null or empty"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;->UNKNOWN:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-object v0

    :cond_0
    const-string v3, "start"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;->PRE_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    const-string v3, "end"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;->POST_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    const/16 v3, 0x23

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v3, v4, :cond_3

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-gtz v1, :cond_8

    :try_start_1
    const-string v2, "in Vmap AdBreak(positional): timeOffset must be >= 1"

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_1
    new-instance v1, Landroid/util/Pair;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;->POSITIONAL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    :goto_2
    const-string v2, "in Vmap AdBreak(positional): integer parse error"

    invoke-static {v2, v1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v3, 0x25

    add-int/lit8 v4, v0, -0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v3, v4, :cond_6

    const/4 v3, 0x0

    add-int/lit8 v0, v0, -0x1

    :try_start_2
    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v0

    if-gez v0, :cond_5

    :try_start_3
    const-string v1, "in Vmap AdBreak(percentage): value must not be <0"

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2

    move v0, v2

    :cond_4
    :goto_3
    new-instance v1, Landroid/util/Pair;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;->PERCENTAGE:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto/16 :goto_0

    :cond_5
    if-le v0, v1, :cond_4

    :try_start_4
    const-string v2, "in Vmap AdBreak(percentage): value must not be >100"

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_2

    move v0, v1

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v1, v0

    move v0, v2

    :goto_4
    const-string v2, "in Vmap AdBreak(percentage): integer parse error"

    invoke-static {v2, v1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_6
    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/ag;->a(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_7

    const-string v0, "in Vmap AdBreak(time): value must not be <00:00:00.000"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    :goto_5
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;->TIME:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v0

    move-object v5, v0

    move v0, v1

    move-object v1, v5

    goto :goto_2

    :cond_7
    move v2, v0

    goto :goto_5

    :cond_8
    move v0, v1

    goto :goto_1
.end method
