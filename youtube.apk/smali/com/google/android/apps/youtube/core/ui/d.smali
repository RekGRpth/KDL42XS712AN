.class final Lcom/google/android/apps/youtube/core/ui/d;
.super Landroid/database/DataSetObserver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/ui/BasePagedView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/ui/BasePagedView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/ui/d;->a:Lcom/google/android/apps/youtube/core/ui/BasePagedView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/ui/BasePagedView;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/ui/d;-><init>(Lcom/google/android/apps/youtube/core/ui/BasePagedView;)V

    return-void
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/d;->a:Lcom/google/android/apps/youtube/core/ui/BasePagedView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/ui/BasePagedView;->a(Lcom/google/android/apps/youtube/core/ui/BasePagedView;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/ui/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/ui/e;-><init>(Lcom/google/android/apps/youtube/core/ui/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public final onChanged()V
    .locals 0

    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/d;->a()V

    return-void
.end method

.method public final onInvalidated()V
    .locals 0

    invoke-super {p0}, Landroid/database/DataSetObserver;->onInvalidated()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/ui/d;->a()V

    return-void
.end method
