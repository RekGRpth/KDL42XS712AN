.class public final Lcom/google/android/apps/youtube/core/player/overlay/DummyAnnotationOverlay;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/aw;
.implements Lcom/google/android/apps/youtube/core/player/overlay/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(Z)V
    .locals 0

    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final c()Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public final d()V
    .locals 0

    return-void
.end method

.method public final e()V
    .locals 0

    return-void
.end method

.method public final f()V
    .locals 0

    return-void
.end method

.method public final g()V
    .locals 0

    return-void
.end method

.method public final h()V
    .locals 0

    return-void
.end method

.method public final i()V
    .locals 0

    return-void
.end method

.method public final j()V
    .locals 0

    return-void
.end method

.method public final setAdStyle(Z)V
    .locals 0

    return-void
.end method

.method public final setCallToActionImage(Landroid/graphics/Bitmap;)V
    .locals 0

    return-void
.end method

.method public final setCallToActionText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final setFeaturedChannelImage(Landroid/graphics/Bitmap;)V
    .locals 0

    return-void
.end method

.method public final setFeaturedVideoImage(Landroid/graphics/Bitmap;)V
    .locals 0

    return-void
.end method

.method public final setFeaturedVideoTitle(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final setInfoCardTeaserImage(Landroid/graphics/Bitmap;)V
    .locals 0

    return-void
.end method

.method public final setInfoCardTeaserMessage(Ljava/lang/CharSequence;)V
    .locals 0

    return-void
.end method

.method public final setListener(Lcom/google/android/apps/youtube/core/player/overlay/h;)V
    .locals 0

    return-void
.end method

.method public final setVisible(Z)V
    .locals 0

    return-void
.end method
