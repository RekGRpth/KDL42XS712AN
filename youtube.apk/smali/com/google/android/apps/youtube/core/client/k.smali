.class public final Lcom/google/android/apps/youtube/core/client/k;
.super Lcom/google/android/apps/youtube/core/client/m;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/core/client/m;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/core/converter/http/a;Lcom/google/android/apps/youtube/core/converter/http/el;)Lcom/google/android/apps/youtube/core/async/u;
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/converter/c;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/converter/http/ay;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/client/k;->a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/converter/http/b;Lcom/google/android/apps/youtube/core/converter/http/gs;)Lcom/google/android/apps/youtube/core/async/u;
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/converter/c;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/converter/http/ay;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/client/k;->a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v0

    return-object v0
.end method
