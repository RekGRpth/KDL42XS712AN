.class final Lcom/google/android/apps/youtube/core/identity/ap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Landroid/widget/CheckedTextView;

.field final synthetic b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

.field final synthetic c:Lcom/google/android/apps/youtube/core/identity/ao;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/identity/ao;Landroid/widget/CheckedTextView;Lcom/google/android/apps/youtube/core/identity/UserProfile;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/identity/ap;->c:Lcom/google/android/apps/youtube/core/identity/ao;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/identity/ap;->a:Landroid/widget/CheckedTextView;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/identity/ap;->b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    const/4 v3, 0x0

    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/ap;->a:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/ap;->b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/ap;->c:Lcom/google/android/apps/youtube/core/identity/ao;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/identity/ao;->a(Lcom/google/android/apps/youtube/core/identity/ao;)Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/g;->r:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-static {p2, v0, v0, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/ap;->c:Lcom/google/android/apps/youtube/core/identity/ao;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/identity/ao;->a(Lcom/google/android/apps/youtube/core/identity/ao;)Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/ap;->a:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/CheckedTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/ap;->c:Lcom/google/android/apps/youtube/core/identity/ao;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/ao;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method
