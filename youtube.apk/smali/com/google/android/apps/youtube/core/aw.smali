.class public final Lcom/google/android/apps/youtube/core/aw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/youtube/common/network/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/network/h;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->b:Lcom/google/android/apps/youtube/common/network/h;

    return-void
.end method

.method public static a(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    instance-of v0, p0, Lcom/google/android/apps/youtube/core/async/GDataResponseException;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/google/android/apps/youtube/core/async/GDataResponseException;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p4}, Lcom/google/android/apps/youtube/core/async/GDataResponseException;->containsError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/aw;->b(Ljava/lang/Throwable;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    return-void
.end method

.method public final b(Ljava/lang/Throwable;)Landroid/util/Pair;
    .locals 7

    const/16 v6, 0x193

    const/4 v2, 0x1

    const/4 v3, 0x0

    move-object v1, p1

    :goto_0
    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->bn:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "genericError"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const-class v4, Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis$DeviceRegistrationException;

    move-object v0, v1

    :goto_2
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v0, v2

    :goto_3
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->bj:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "deviceRegistration"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_2

    :cond_2
    move v0, v3

    goto :goto_3

    :cond_3
    instance-of v0, v1, Lcom/google/android/apps/youtube/medialib/player/MissingStreamException;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->go:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "missingStream"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    :cond_4
    instance-of v0, v1, Landroid/accounts/AuthenticatorException;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->bf:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "authenticator"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    :cond_5
    instance-of v0, v1, Ljava/net/SocketException;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->b:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->bi:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "connection"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->cO:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "noNetwork"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    :cond_7
    instance-of v0, v1, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_d

    instance-of v0, v1, Lcom/google/android/apps/youtube/core/async/GDataResponseException;

    if-eqz v0, :cond_b

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataResponseException;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/async/GDataResponseException;->containsDisabledInMaintenanceModeError()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->bB:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "youtubeServerDown"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    :cond_8
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/async/GDataResponseException;->containsYouTubeSignupRequiredError()Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->bt:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "noLinkedYoutubeAccount"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    :cond_9
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/async/GDataResponseException;->containsInvalidUriError()Z

    move-result v4

    if-eqz v4, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->bq:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "invalidRequest"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    :cond_a
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/async/GDataResponseException;->containsVideoIsPrivateError()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->gM:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "videoIsPrivate"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    :cond_b
    check-cast v1, Lorg/apache/http/client/HttpResponseException;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "httpError "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v4

    if-ne v4, v6, :cond_c

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/youtube/p;->bl:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    :cond_c
    iget-object v4, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v5, Lcom/google/android/youtube/p;->bo:I

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-virtual {v4, v5, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    :cond_d
    instance-of v0, v1, Lcom/android/volley/VolleyError;

    if-eqz v0, :cond_f

    move-object v0, v1

    check-cast v0, Lcom/android/volley/VolleyError;

    iget-object v4, v0, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/j;

    if-eqz v4, :cond_f

    iget v5, v4, Lcom/android/volley/j;->a:I

    if-lez v5, :cond_f

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "httpError "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v4, Lcom/android/volley/j;->a:I

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/j;

    iget v0, v0, Lcom/android/volley/j;->a:I

    if-ne v0, v6, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/youtube/p;->bl:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v5, Lcom/google/android/youtube/p;->bo:I

    new-array v2, v2, [Ljava/lang/Object;

    iget v4, v4, Lcom/android/volley/j;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v5, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    :cond_f
    instance-of v0, v1, Lcom/google/android/apps/youtube/core/converter/InvalidFormatException;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->bx:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "invalidResponse"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    :cond_10
    instance-of v0, v1, Lcom/google/android/apps/youtube/core/converter/ConverterException;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->bw:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "genericResponseError"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    :cond_11
    instance-of v0, v1, Ljava/io/IOException;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->b:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->bs:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "genericNetworkError"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->cO:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "noNetwork"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    :cond_13
    instance-of v0, v1, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePolicyException;

    if-eqz v0, :cond_15

    instance-of v0, v1, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePolicyException$OfflineVideoExpiredPolicyException;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->gm:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "offlinePolicyExpired"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->gm:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "offlinePolicyError"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    :cond_15
    instance-of v0, v1, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePlaybackException;

    if-eqz v0, :cond_18

    instance-of v0, v1, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePlaybackException$OfflineMediaUnplayableException;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->gm:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "offlineMediaUnplayable"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    :cond_16
    instance-of v0, v1, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePlaybackException$OfflineMediaIncompleteException;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->gO:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "offlineMediaIncomplete"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    :cond_17
    instance-of v0, v1, Lcom/google/android/apps/youtube/core/offline/exception/OfflinePlaybackException$OfflineNoMediaException;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/aw;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/youtube/p;->gO:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "offlineNoMedia"

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_1

    :cond_18
    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public final c(Ljava/lang/Throwable;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/String;)V

    return-void
.end method
