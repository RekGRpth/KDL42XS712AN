.class final Lcom/google/android/apps/youtube/core/player/notification/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/notification/f;->b:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/notification/f;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to get the video for use in playback controls: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/f;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/f;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/f;->b:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    :cond_0
    return-void
.end method
