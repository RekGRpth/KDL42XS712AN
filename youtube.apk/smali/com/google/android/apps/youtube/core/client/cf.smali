.class public final Lcom/google/android/apps/youtube/core/client/cf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:J

.field private final b:Lcom/google/android/apps/youtube/common/e/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/cf;->b:Lcom/google/android/apps/youtube/common/e/b;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/cf;->a:J

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/cf;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/cf;->a:J

    return-void
.end method

.method public final b()J
    .locals 4

    const-wide/16 v0, -0x1

    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/client/cf;->a:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/cf;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/client/cf;->a:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method
