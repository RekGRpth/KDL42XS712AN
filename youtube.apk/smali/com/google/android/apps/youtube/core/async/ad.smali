.class public final Lcom/google/android/apps/youtube/core/async/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/async/af;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/async/af;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/ad;->a:Lcom/google/android/apps/youtube/core/async/af;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/async/ad;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/core/async/ad;->a(Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v1, v0, 0x32

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-ge v0, v1, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/ad;->a:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v0, v2, p3}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p4, p1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    check-cast p1, Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/google/android/apps/youtube/core/async/ae;

    invoke-direct {v1, p0, p1, v0, p2}, Lcom/google/android/apps/youtube/core/async/ae;-><init>(Lcom/google/android/apps/youtube/core/async/ad;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/google/android/apps/youtube/core/async/ad;->a(Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
