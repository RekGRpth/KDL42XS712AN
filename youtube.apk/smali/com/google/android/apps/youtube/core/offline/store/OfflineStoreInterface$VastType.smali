.class public final enum Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

.field public static final enum EMPTY:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

.field public static final enum FORECASTING:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

.field public static final enum FULL:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

.field static final lookup:Landroid/util/SparseArray;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    const-string v2, "EMPTY"

    invoke-direct {v1, v2, v0, v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->EMPTY:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    new-instance v1, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    const-string v2, "FORECASTING"

    invoke-direct {v1, v2, v3, v3}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->FORECASTING:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    new-instance v1, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    const-string v2, "FULL"

    invoke-direct {v1, v2, v4, v4}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->FULL:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    sget-object v2, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->EMPTY:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    aput-object v2, v1, v0

    sget-object v2, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->FORECASTING:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    aput-object v2, v1, v3

    sget-object v2, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->FULL:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    aput-object v2, v1, v4

    sput-object v1, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->$VALUES:[Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    sput-object v1, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->lookup:Landroid/util/SparseArray;

    invoke-static {}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->values()[Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    sget-object v4, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->lookup:Landroid/util/SparseArray;

    iget v5, v3, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->value:I

    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->lookup:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->$VALUES:[Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;

    return-object v0
.end method


# virtual methods
.method public final value()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$VastType;->value:I

    return v0
.end method
