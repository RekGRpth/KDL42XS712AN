.class public final Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/transfer/m;


# instance fields
.field private final A:I

.field private final B:Z

.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/youtube/core/transfer/v;

.field private final c:Lcom/google/android/apps/youtube/core/transfer/r;

.field private final d:Ljava/util/Random;

.field private final e:Landroid/os/HandlerThread;

.field private final f:Landroid/os/Handler;

.field private final g:Ljava/lang/Object;

.field private h:I

.field private i:I

.field private j:Z

.field private final k:Lcom/google/android/apps/youtube/core/transfer/n;

.field private final l:Ljava/util/Map;

.field private final m:Ljava/util/Map;

.field private final n:Ljava/util/Map;

.field private final o:Ljava/util/HashSet;

.field private final p:Landroid/os/PowerManager$WakeLock;

.field private final q:Landroid/net/wifi/WifiManager$WifiLock;

.field private final r:Lcom/google/android/apps/youtube/core/transfer/u;

.field private final s:Lcom/google/android/apps/youtube/core/transfer/s;

.field private final t:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor$ChargingReceiver;

.field private u:Z

.field private v:Z

.field private w:I

.field private x:Z

.field private y:Z

.field private volatile z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/transfer/r;Lcom/google/android/apps/youtube/core/transfer/v;Ljava/lang/String;IZ)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->c:Lcom/google/android/apps/youtube/core/transfer/r;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->b:Lcom/google/android/apps/youtube/core/transfer/v;

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->j:Z

    iput p5, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->A:I

    iput-boolean p6, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->B:Z

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-direct {v0, p1, p4}, Lcom/google/android/apps/youtube/core/transfer/n;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->d:Ljava/util/Random;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->g:Ljava/lang/Object;

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->m:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->n:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->o:Ljava/util/HashSet;

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/s;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/transfer/s;-><init>(Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->s:Lcom/google/android/apps/youtube/core/transfer/s;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->s:Lcom/google/android/apps/youtube/core/transfer/s;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/s;->b()V

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/u;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/transfer/u;-><init>(Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->r:Lcom/google/android/apps/youtube/core/transfer/u;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->r:Lcom/google/android/apps/youtube/core/transfer/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/u;->c()V

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor$ChargingReceiver;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor$ChargingReceiver;-><init>(Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->t:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor$ChargingReceiver;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->t:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor$ChargingReceiver;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor$ChargingReceiver;->a()V

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->p:Landroid/os/PowerManager$WakeLock;

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->createWifiLock(Ljava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->q:Landroid/net/wifi/WifiManager$WifiLock;

    new-instance v0, Landroid/os/HandlerThread;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->e:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/p;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->e:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/core/transfer/p;-><init>(Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f:Landroid/os/Handler;

    return-void
.end method

.method private a(IIILjava/lang/Object;)I
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->j:Z

    iget v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->i:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(ILjava/lang/Object;)I
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f:Landroid/os/Handler;

    invoke-virtual {v0, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->j:Z

    iget v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->i:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(ILjava/lang/Object;I)I
    .locals 5

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f:Landroid/os/Handler;

    const/16 v3, 0xc

    invoke-virtual {v2, v3, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    int-to-long v3, p3

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->j:Z

    iget v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->i:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;)Landroid/os/PowerManager$WakeLock;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->p:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method private a(Lcom/google/android/apps/youtube/core/transfer/t;I)V
    .locals 4

    const/4 v2, 0x1

    const/4 v0, 0x0

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/transfer/t;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->PENDING:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-eq v1, v3, :cond_3

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->PENDING:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    iput-object v0, p1, Lcom/google/android/apps/youtube/core/transfer/t;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    move v1, v2

    :goto_0
    iget-object v3, p1, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->m:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/l;

    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Lcom/google/android/apps/youtube/core/transfer/l;->a(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->n:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->o:Ljava/util/HashSet;

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget v0, p1, Lcom/google/android/apps/youtube/core/transfer/t;->d:I

    if-eq v0, p2, :cond_2

    iput p2, p1, Lcom/google/android/apps/youtube/core/transfer/t;->d:I

    :goto_1
    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/transfer/n;->b(Lcom/google/android/apps/youtube/core/transfer/t;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->c:Lcom/google/android/apps/youtube/core/transfer/r;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/transfer/t;->a()Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/r;->f(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    :cond_1
    return-void

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v1, v0

    goto :goto_0
.end method

.method private b(I)I
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->j:Z

    iget v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->i:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->j:Z

    if-nez v0, :cond_0

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->b(I)I

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private final f()V
    .locals 11

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->x:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->u:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->r:Lcom/google/android/apps/youtube/core/transfer/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/u;->b()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    iget-boolean v3, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->v:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->t:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor$ChargingReceiver;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor$ChargingReceiver;->c()Z

    move-result v3

    if-nez v3, :cond_5

    move v3, v1

    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->s:Lcom/google/android/apps/youtube/core/transfer/s;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/transfer/s;->a()Z

    move-result v4

    if-nez v4, :cond_6

    move v4, v1

    :goto_3
    iget-object v5, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->r:Lcom/google/android/apps/youtube/core/transfer/u;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/core/transfer/u;->a()Z

    move-result v5

    if-nez v5, :cond_7

    const/4 v5, 0x2

    :goto_4
    or-int/lit8 v6, v5, 0x0

    if-eqz v4, :cond_8

    const/4 v5, 0x4

    :goto_5
    or-int/2addr v5, v6

    if-eqz v0, :cond_9

    const/16 v0, 0x8

    :goto_6
    or-int/2addr v5, v0

    if-eqz v3, :cond_a

    const/16 v0, 0x10

    :goto_7
    or-int v7, v5, v0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v5, v2

    move v6, v2

    :goto_8
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/t;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/t;->b()Z

    move-result v9

    if-eqz v9, :cond_11

    iget v6, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->w:I

    if-lt v5, v6, :cond_b

    const/16 v6, 0x20

    :goto_9
    or-int/2addr v6, v7

    if-nez v6, :cond_e

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->m:Ljava/util/Map;

    iget-object v9, v0, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    invoke-interface {v6, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->o:Ljava/util/HashSet;

    iget-object v9, v0, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    :cond_2
    move v6, v1

    :goto_a
    if-nez v6, :cond_3

    iget-object v9, v0, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->m:Ljava/util/Map;

    invoke-interface {v6, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_d

    move v6, v1

    :goto_b
    invoke-static {v6}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->b:Lcom/google/android/apps/youtube/core/transfer/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/t;->a()Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    move-result-object v10

    invoke-interface {v6, v10, p0}, Lcom/google/android/apps/youtube/core/transfer/v;->a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;Lcom/google/android/apps/youtube/core/transfer/m;)Lcom/google/android/apps/youtube/core/transfer/l;

    move-result-object v6

    iget-object v10, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->m:Ljava/util/Map;

    invoke-interface {v10, v9, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v9, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->RUNNING:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    iput-object v9, v0, Lcom/google/android/apps/youtube/core/transfer/t;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    iput v2, v0, Lcom/google/android/apps/youtube/core/transfer/t;->d:I

    iget-object v9, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v9, v0}, Lcom/google/android/apps/youtube/core/transfer/n;->b(Lcom/google/android/apps/youtube/core/transfer/t;)V

    new-instance v9, Lcom/google/android/apps/youtube/core/transfer/q;

    invoke-direct {v9, p0, v6}, Lcom/google/android/apps/youtube/core/transfer/q;-><init>(Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;Ljava/lang/Runnable;)V

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/core/transfer/q;->start()V

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->c:Lcom/google/android/apps/youtube/core/transfer/r;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/t;->a()Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    move-result-object v0

    invoke-interface {v6, v0}, Lcom/google/android/apps/youtube/core/transfer/r;->f(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    :cond_3
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v6, v1

    goto :goto_8

    :cond_4
    move v0, v2

    goto/16 :goto_1

    :cond_5
    move v3, v2

    goto/16 :goto_2

    :cond_6
    move v4, v2

    goto/16 :goto_3

    :cond_7
    move v5, v2

    goto/16 :goto_4

    :cond_8
    move v5, v2

    goto/16 :goto_5

    :cond_9
    move v0, v2

    goto/16 :goto_6

    :cond_a
    move v0, v2

    goto/16 :goto_7

    :cond_b
    move v6, v2

    goto :goto_9

    :cond_c
    move v6, v2

    goto :goto_a

    :cond_d
    move v6, v2

    goto :goto_b

    :cond_e
    invoke-direct {p0, v0, v6}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(Lcom/google/android/apps/youtube/core/transfer/t;I)V

    move v0, v1

    :goto_c
    move v6, v0

    goto/16 :goto_8

    :cond_f
    iput-boolean v6, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->y:Z

    if-eqz v6, :cond_10

    if-nez v3, :cond_10

    if-nez v4, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->q:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->q:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    goto/16 :goto_0

    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->q:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->q:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto/16 :goto_0

    :cond_11
    move v0, v6

    goto :goto_c
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->b(I)I

    move-result v0

    return v0
.end method

.method public final a(I)I
    .locals 3

    const/4 v0, 0x6

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(IIILjava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)I
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(ILjava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;I)I
    .locals 2

    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, v0, p2, v1, p1}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(IIILjava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)I
    .locals 3

    const/4 v0, 0x2

    new-instance v1, Lcom/google/android/apps/youtube/core/transfer/t;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1, p2, p3}, Lcom/google/android/apps/youtube/core/transfer/t;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(ILjava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)I
    .locals 2

    const/4 v0, 0x2

    new-instance v1, Lcom/google/android/apps/youtube/core/transfer/t;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/core/transfer/t;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(ILjava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final a(Z)I
    .locals 4

    const/4 v1, 0x0

    const/4 v2, 0x5

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 v3, 0x0

    invoke-direct {p0, v2, v0, v1, v3}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(IIILjava/lang/Object;)I

    move-result v0

    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final a(Landroid/os/Message;)V
    .locals 7

    const/4 v6, 0x0

    const/16 v5, 0x1f

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->h:I

    iget v2, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->i:I

    if-ne v0, v2, :cond_19

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->y:Z

    if-nez v0, :cond_19

    :goto_1
    iput-boolean v3, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->j:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->c:Lcom/google/android/apps/youtube/core/transfer/r;

    iget v2, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->h:I

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/core/transfer/r;->a(I)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/n;->a()V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v1, v6}, Lcom/google/android/apps/youtube/core/transfer/n;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/transfer/t;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Restoring task: (filePath="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v1, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; accountName="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Lcom/google/android/apps/youtube/core/transfer/t;->i:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; status="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v1, Lcom/google/android/apps/youtube/core/transfer/t;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    iget-object v6, v1, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    invoke-interface {v5, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/transfer/t;->b()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, v1, Lcom/google/android/apps/youtube/core/transfer/t;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v6, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->PENDING:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-eq v5, v6, :cond_3

    sget-object v5, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->PENDING:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    iput-object v5, v1, Lcom/google/android/apps/youtube/core/transfer/t;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    iput v3, v1, Lcom/google/android/apps/youtube/core/transfer/t;->d:I

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v5, v1}, Lcom/google/android/apps/youtube/core/transfer/n;->b(Lcom/google/android/apps/youtube/core/transfer/t;)V

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/n;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    goto :goto_2

    :cond_5
    new-instance v2, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v2, v1}, Ljava/util/HashMap;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/transfer/t;

    iget-object v6, v1, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/transfer/t;->a()Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    move-result-object v1

    invoke-interface {v2, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_6
    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->z:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->c:Lcom/google/android/apps/youtube/core/transfer/r;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/core/transfer/r;->a(Ljava/util/Map;)V

    iput-boolean v3, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->x:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f()V

    goto/16 :goto_0

    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_7

    move v0, v3

    :goto_5
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->v:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->v:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f()V

    goto/16 :goto_0

    :cond_7
    move v0, v4

    goto :goto_5

    :pswitch_2
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_8

    move v0, v3

    :goto_6
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->u:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->u:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f()V

    goto/16 :goto_0

    :cond_8
    move v0, v4

    goto :goto_6

    :pswitch_3
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->w:I

    if-eq v1, v0, :cond_0

    iput v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->w:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f()V

    goto/16 :goto_0

    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f()V

    goto/16 :goto_0

    :pswitch_5
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->x:Z

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/t;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    iget-object v2, v0, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    iget-object v2, v0, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/transfer/t;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/transfer/t;->b()Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/core/transfer/n;->c(Lcom/google/android/apps/youtube/core/transfer/t;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/n;->a(Lcom/google/android/apps/youtube/core/transfer/t;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    iget-object v2, v0, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->c:Lcom/google/android/apps/youtube/core/transfer/r;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/t;->a()Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/r;->b(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f()V

    goto/16 :goto_0

    :cond_9
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->o:Ljava/util/HashSet;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f()V

    goto/16 :goto_0

    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/n;->a(Lcom/google/android/apps/youtube/core/transfer/t;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->z:Ljava/lang/String;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->z:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/youtube/core/transfer/t;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_b
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    iget-object v2, v0, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->c:Lcom/google/android/apps/youtube/core/transfer/r;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/t;->a()Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/r;->b(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f()V

    goto/16 :goto_0

    :pswitch_6
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->x:Z

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/n;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->m:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/transfer/l;

    if-eqz v1, :cond_d

    invoke-interface {v1, v2}, Lcom/google/android/apps/youtube/core/transfer/l;->a(I)V

    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->m:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->n:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->o:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/t;

    iget v1, v0, Lcom/google/android/apps/youtube/core/transfer/t;->d:I

    or-int/2addr v1, v2

    iput v1, v0, Lcom/google/android/apps/youtube/core/transfer/t;->d:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/n;->c(Lcom/google/android/apps/youtube/core/transfer/t;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/t;->a()Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->b:Lcom/google/android/apps/youtube/core/transfer/v;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/v;->a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Ljava/lang/Runnable;

    move-result-object v1

    if-eqz v1, :cond_e

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    :cond_e
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->c:Lcom/google/android/apps/youtube/core/transfer/r;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/r;->c(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f()V

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/t;

    if-eqz v0, :cond_2

    iget v1, p1, Landroid/os/Message;->arg1:I

    int-to-long v1, v1

    shl-long/2addr v1, v5

    iget v5, p1, Landroid/os/Message;->arg2:I

    int-to-long v5, v5

    add-long/2addr v1, v5

    iput-wide v1, v0, Lcom/google/android/apps/youtube/core/transfer/t;->f:J

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/n;->b(Lcom/google/android/apps/youtube/core/transfer/t;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->c:Lcom/google/android/apps/youtube/core/transfer/r;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/t;->a()Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/r;->d(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/t;

    if-eqz v0, :cond_2

    iget v1, p1, Landroid/os/Message;->arg1:I

    int-to-long v1, v1

    shl-long/2addr v1, v5

    iget v5, p1, Landroid/os/Message;->arg2:I

    int-to-long v5, v5

    add-long/2addr v1, v5

    iget-wide v5, v0, Lcom/google/android/apps/youtube/core/transfer/t;->e:J

    cmp-long v5, v5, v1

    if-eqz v5, :cond_f

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->n:Ljava/util/Map;

    iget-object v6, v0, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_f
    iput-wide v1, v0, Lcom/google/android/apps/youtube/core/transfer/t;->e:J

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/n;->b(Lcom/google/android/apps/youtube/core/transfer/t;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->c:Lcom/google/android/apps/youtube/core/transfer/r;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/t;->a()Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/r;->e(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    goto/16 :goto_0

    :pswitch_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/core/transfer/t;

    if-eqz v2, :cond_2

    iget-object v5, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v5, :cond_10

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    :goto_7
    iput-object v0, v2, Lcom/google/android/apps/youtube/core/transfer/t;->h:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    iput-object v0, v2, Lcom/google/android/apps/youtube/core/transfer/t;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->m:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->n:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->o:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->B:Z

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/transfer/n;->b(Lcom/google/android/apps/youtube/core/transfer/t;)V

    :goto_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->c:Lcom/google/android/apps/youtube/core/transfer/r;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/transfer/t;->a()Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/r;->f(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f()V

    goto/16 :goto_0

    :cond_10
    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;-><init>()V

    goto :goto_7

    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/transfer/n;->c(Lcom/google/android/apps/youtube/core/transfer/t;)V

    goto :goto_8

    :pswitch_a
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/t;

    if-eqz v0, :cond_2

    iget v1, p1, Landroid/os/Message;->arg1:I

    if-ne v1, v3, :cond_13

    move v2, v3

    :goto_9
    iget-object v5, v0, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->n:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v1, :cond_14

    move v1, v4

    :goto_a
    add-int/lit8 v1, v1, 0x1

    if-nez v2, :cond_12

    iget v2, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->A:I

    if-le v1, v2, :cond_16

    :cond_12
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "transfer fatal fail "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->FAILED:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    iput-object v1, v0, Lcom/google/android/apps/youtube/core/transfer/t;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->B:Z

    if-eqz v1, :cond_15

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/n;->b(Lcom/google/android/apps/youtube/core/transfer/t;)V

    :goto_b
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->m:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->n:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->o:Ljava/util/HashSet;

    invoke-virtual {v1, v5}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->c:Lcom/google/android/apps/youtube/core/transfer/r;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/t;->a()Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/r;->f(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    :goto_c
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f()V

    goto/16 :goto_0

    :cond_13
    move v2, v4

    goto :goto_9

    :cond_14
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_a

    :cond_15
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/n;->c(Lcom/google/android/apps/youtube/core/transfer/t;)V

    goto :goto_b

    :cond_16
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "transfer fail "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->n:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->m:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->o:Ljava/util/HashSet;

    invoke-virtual {v0, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    shl-int v0, v3, v1

    mul-int/lit16 v0, v0, 0x3e8

    const v1, 0x927c0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->d:Ljava/util/Random;

    const/16 v2, 0x1388

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    add-int/2addr v0, v1

    const/16 v1, 0xc

    invoke-direct {p0, v1, v5, v0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(ILjava/lang/Object;I)I

    goto :goto_c

    :pswitch_b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->o:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->f()V

    goto/16 :goto_0

    :pswitch_c
    const-string v0, "Pausing all tasks"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/t;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Pausing task: (filePath="

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "; accountName="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, v0, Lcom/google/android/apps/youtube/core/transfer/t;->i:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "; status="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, v0, Lcom/google/android/apps/youtube/core/transfer/t;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/google/android/apps/youtube/core/transfer/t;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v5, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->RUNNING:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    const/16 v2, 0x80

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(Lcom/google/android/apps/youtube/core/transfer/t;I)V

    :cond_17
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/transfer/n;->b(Lcom/google/android/apps/youtube/core/transfer/t;)V

    goto :goto_d

    :cond_18
    iput-object v6, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->z:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->o:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    goto/16 :goto_0

    :pswitch_d
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/n;->a()V

    const-string v0, "Removing all tasks"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/n;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->o:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    goto/16 :goto_0

    :cond_19
    move v3, v4

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 4

    const/16 v0, 0x8

    const/16 v1, 0x1f

    shr-long v1, p2, v1

    long-to-int v1, v1

    const-wide/32 v2, 0x7fffffff

    and-long/2addr v2, p2

    long-to-int v2, v2

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(IIILjava/lang/Object;)I

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/transfer/TransferException;)V
    .locals 3

    const/4 v1, 0x0

    const/16 v2, 0xb

    iget-boolean v0, p2, Lcom/google/android/apps/youtube/core/transfer/TransferException;->fatal:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v2, v0, v1, p1}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(IIILjava/lang/Object;)I

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)V
    .locals 2

    const/16 v0, 0xa

    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(ILjava/lang/Object;)I

    return-void
.end method

.method public final b(Z)I
    .locals 4

    const/4 v1, 0x0

    const/4 v2, 0x4

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 v3, 0x0

    invoke-direct {p0, v2, v0, v1, v3}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(IIILjava/lang/Object;)I

    move-result v0

    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;J)V
    .locals 4

    const/16 v0, 0x9

    const/16 v1, 0x1f

    shr-long v1, p2, v1

    long-to-int v1, v1

    const-wide/32 v2, 0x7fffffff

    and-long/2addr v2, p2

    long-to-int v2, v2

    invoke-direct {p0, v0, v1, v2, p1}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(IIILjava/lang/Object;)I

    return-void
.end method

.method public final c()I
    .locals 1

    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->b(I)I

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 5

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->q:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "wifiLock held in quit"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->q:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->s:Lcom/google/android/apps/youtube/core/transfer/s;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/s;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->r:Lcom/google/android/apps/youtube/core/transfer/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/u;->d()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->t:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor$ChargingReceiver;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor$ChargingReceiver;->b()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->i:I

    iget v2, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->h:I

    sub-int v2, v0, v2

    if-nez v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "pendingMessages = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(ZLjava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->k:Lcom/google/android/apps/youtube/core/transfer/n;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/n;->c()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->z:Ljava/lang/String;

    return-object v0
.end method
