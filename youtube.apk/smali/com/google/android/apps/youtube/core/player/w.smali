.class public Lcom/google/android/apps/youtube/core/player/w;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Z

.field protected b:Z

.field protected c:Lcom/google/android/apps/youtube/core/player/z;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(Lcom/google/android/apps/youtube/datalib/innertube/model/w;)Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->a()I

    move-result v1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->UNKNOWN:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->VIDEO_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    :cond_0
    :goto_0
    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;-><init>(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;ZLjava/lang/String;)V

    return-object v1

    :cond_1
    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->UNPLAYABLE:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->USER_CHECK_FAILED:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/w;->c:Lcom/google/android/apps/youtube/core/player/z;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/z;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/w;->c:Lcom/google/android/apps/youtube/core/player/z;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/ak;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/w;->a:Z

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->a(Z)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/w;->b:Z

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->b(Z)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    return-void
.end method

.method protected a(Lcom/google/android/apps/youtube/datalib/innertube/model/w;)V
    .locals 0

    return-void
.end method

.method protected a(Lcom/google/android/apps/youtube/datalib/innertube/model/w;Lcom/google/android/apps/youtube/core/player/y;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/player/w;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/w;)Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/core/player/y;->a(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/w;Lcom/google/android/apps/youtube/core/player/y;Z)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    if-nez p1, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->UNKNOWN:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;-><init>(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;ZLjava/lang/String;)V

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/core/player/y;->a(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p3, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->h()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->UNPLAYABLE_IN_BACKGROUND:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;-><init>(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;ZLjava/lang/String;)V

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/core/player/y;->a(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    goto :goto_0

    :cond_1
    invoke-interface {p2}, Lcom/google/android/apps/youtube/core/player/y;->a()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/w;->c:Lcom/google/android/apps/youtube/core/player/z;

    if-nez v0, :cond_3

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/player/w;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/w;)Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/core/player/y;->a(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/w;->c:Lcom/google/android/apps/youtube/core/player/z;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/core/player/x;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/apps/youtube/core/player/x;-><init>(Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/datalib/innertube/model/w;Lcom/google/android/apps/youtube/core/player/y;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/z;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/player/ab;)V

    goto :goto_0

    :cond_4
    invoke-static {p1}, Lcom/google/android/apps/youtube/core/player/w;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/w;)Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/core/player/y;->a(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    goto :goto_0
.end method
