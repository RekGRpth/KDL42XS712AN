.class public final Lcom/google/android/apps/youtube/core/ui/n;
.super Landroid/app/AlertDialog;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/core/Analytics;

.field private final c:Lcom/google/android/apps/youtube/core/client/bc;

.field private final d:Lcom/google/android/apps/youtube/core/client/bj;

.field private final e:Lcom/google/android/apps/youtube/core/client/bz;

.field private final f:Lcom/google/android/apps/youtube/core/identity/y;

.field private final g:Landroid/view/View;

.field private final h:Landroid/widget/TextView;

.field private final i:Landroid/widget/ImageView;

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/client/bz;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/identity/y;)V
    .locals 6

    const/4 v2, 0x0

    new-instance v0, Landroid/view/ContextThemeWrapper;

    sget v1, Lcom/google/android/youtube/q;->a:I

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {p0, v0}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->c:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->d:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bz;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->e:Lcom/google/android/apps/youtube/core/client/bz;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->b:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/y;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->f:Lcom/google/android/apps/youtube/core/identity/y;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/youtube/l;->D:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    sget v0, Lcom/google/android/youtube/p;->gE:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/ui/n;->setTitle(I)V

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/ui/n;->setView(Landroid/view/View;IIII)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/core/ui/n;->setIcon(I)V

    sget v0, Lcom/google/android/youtube/j;->cn:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->g:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->cJ:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->h:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->i:Landroid/widget/ImageView;

    const/4 v0, -0x1

    const v1, 0x104000a    # android.R.string.ok

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p0}, Lcom/google/android/apps/youtube/core/ui/n;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    const/4 v0, -0x2

    const/high16 v1, 0x1040000    # android.R.string.cancel

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p0}, Lcom/google/android/apps/youtube/core/ui/n;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {p0, p0}, Lcom/google/android/apps/youtube/core/ui/n;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {p0, p0}, Lcom/google/android/apps/youtube/core/ui/n;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/ui/n;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->g:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/ui/n;Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/ui/n;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/ui/n;->j:Z

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/ui/n;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/ui/n;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->h:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/ui/n;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/ui/n;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->i:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/core/ui/n;)Lcom/google/android/apps/youtube/core/client/bj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->d:Lcom/google/android/apps/youtube/core/client/bj;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/core/ui/n;)Lcom/google/android/apps/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->b:Lcom/google/android/apps/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/core/ui/n;)Lcom/google/android/apps/youtube/core/identity/y;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->f:Lcom/google/android/apps/youtube/core/identity/y;

    return-object v0
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->f:Lcom/google/android/apps/youtube/core/identity/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/identity/y;->b()V

    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/ui/n;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->c:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/n;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/apps/youtube/core/ui/q;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/core/ui/q;-><init>(Lcom/google/android/apps/youtube/core/ui/n;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/client/bc;->c(Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->f:Lcom/google/android/apps/youtube/core/identity/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/identity/y;->b()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->a:Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->removeDialog(I)V

    return-void
.end method

.method protected final onStart()V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/app/AlertDialog;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/ui/n;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ui/n;->e:Lcom/google/android/apps/youtube/core/client/bz;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ui/n;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/apps/youtube/core/ui/o;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/core/ui/o;-><init>(Lcom/google/android/apps/youtube/core/ui/n;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/client/bz;->a(Lcom/google/android/apps/youtube/common/a/b;)V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/ui/n;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/ui/n;->j:Z

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method
