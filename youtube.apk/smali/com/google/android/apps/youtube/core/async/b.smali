.class public final Lcom/google/android/apps/youtube/core/async/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private final b:Lcom/google/android/apps/youtube/core/async/af;


# direct methods
.method private constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/async/af;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/async/b;->a:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/async/b;->b:Lcom/google/android/apps/youtube/core/async/af;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/async/b;)Lcom/google/android/apps/youtube/core/async/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/b;->b:Lcom/google/android/apps/youtube/core/async/af;

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/async/b;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/youtube/core/async/b;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/async/af;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/b;->a:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/async/c;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/core/async/c;-><init>(Lcom/google/android/apps/youtube/core/async/b;Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-interface {p2, p1, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
