.class public final Lcom/google/android/apps/youtube/core/identity/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/a/e;
.implements Lcom/google/android/apps/youtube/datalib/a/f;
.implements Lcom/google/android/apps/youtube/datalib/a/p;


# static fields
.field private static final b:Landroid/util/Pair;


# instance fields
.field private final c:Lcom/google/android/apps/youtube/core/identity/b;

.field private final d:Lcom/google/android/apps/youtube/core/identity/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, ""

    const-string v1, ""

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/core/identity/k;->b:Landroid/util/Pair;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/identity/b;Lcom/google/android/apps/youtube/core/identity/l;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/k;->c:Lcom/google/android/apps/youtube/core/identity/b;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/k;->d:Lcom/google/android/apps/youtube/core/identity/l;

    return-void
.end method

.method private a()Landroid/util/Pair;
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/k;->d:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->g()Lcom/google/android/apps/youtube/core/identity/f;

    move-result-object v0

    :try_start_0
    sget-object v1, Lcom/google/android/apps/youtube/core/identity/f;->a:Lcom/google/android/apps/youtube/core/identity/f;

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/k;->c:Lcom/google/android/apps/youtube/core/identity/b;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/identity/b;->a(Lcom/google/android/apps/youtube/core/identity/f;)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->d()Landroid/util/Pair;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_1
    sget-object v0, Lcom/google/android/apps/youtube/core/identity/k;->b:Landroid/util/Pair;

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/a/a/a/a/ii;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/k;->d:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->g()Lcom/google/android/apps/youtube/core/identity/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/f;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/a/a/a/a/tx;

    invoke-direct {v1}, Lcom/google/a/a/a/a/tx;-><init>()V

    iput-object v1, p1, Lcom/google/a/a/a/a/ii;->c:Lcom/google/a/a/a/a/tx;

    iget-object v1, p1, Lcom/google/a/a/a/a/ii;->c:Lcom/google/a/a/a/a/tx;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/f;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/a/a/a/a/tx;->b:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/identity/k;->a()Landroid/util/Pair;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/identity/k;->b:Landroid/util/Pair;

    if-eq v0, v1, :cond_0

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public final a(Lorg/apache/http/client/methods/HttpUriRequest;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/identity/k;->a()Landroid/util/Pair;

    move-result-object v1

    sget-object v0, Lcom/google/android/apps/youtube/core/identity/k;->b:Landroid/util/Pair;

    if-eq v1, v0, :cond_0

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
