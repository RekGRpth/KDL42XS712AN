.class final Lcom/google/android/apps/youtube/core/client/a/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/c/d;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/client/a/h;

.field private final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/client/a/h;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/a/k;->a:Lcom/google/android/apps/youtube/core/client/a/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/k;->b:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/client/a/h;Ljava/lang/String;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/core/client/a/k;-><init>(Lcom/google/android/apps/youtube/core/client/a/h;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 3

    check-cast p1, Lcom/google/android/apps/youtube/core/client/a/g;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/k;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/client/a/g;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/k;->a:Lcom/google/android/apps/youtube/core/client/a/h;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/client/a/h;->a(Lcom/google/android/apps/youtube/core/client/a/h;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/client/a/a;->a(Lcom/google/android/apps/youtube/core/client/a/g;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/a/k;->a:Lcom/google/android/apps/youtube/core/client/a/h;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/client/a/h;->b(Lcom/google/android/apps/youtube/core/client/a/h;)Lcom/google/android/apps/common/csi/lib/c;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/a/a;->a()Lcom/google/android/apps/common/csi/lib/i;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/google/android/apps/common/csi/lib/c;->a(Lcom/google/android/apps/common/csi/lib/i;)Z

    goto :goto_0

    :cond_1
    return-void
.end method
