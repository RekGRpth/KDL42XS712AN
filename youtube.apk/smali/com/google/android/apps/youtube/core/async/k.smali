.class public final Lcom/google/android/apps/youtube/core/async/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# instance fields
.field private final a:J

.field private final b:Lcom/google/android/apps/youtube/core/async/af;

.field private final c:Ljava/util/concurrent/atomic/AtomicLong;

.field private final d:Lcom/google/android/apps/youtube/common/e/b;

.field private final e:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/common/e/b;Ljava/util/concurrent/atomic/AtomicLong;JLjava/util/concurrent/ScheduledExecutorService;)V
    .locals 4

    const-wide/16 v2, 0x1388

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/k;->b:Lcom/google/android/apps/youtube/core/async/af;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/k;->c:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "delay cannot be negative or zero"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    iput-wide v2, p0, Lcom/google/android/apps/youtube/core/async/k;->a:J

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/k;->d:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/k;->e:Ljava/util/concurrent/ScheduledExecutorService;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/async/k;)Lcom/google/android/apps/youtube/core/async/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/k;->b:Lcom/google/android/apps/youtube/core/async/af;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 6

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/k;->d:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/async/k;->c:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    sub-long/2addr v0, v4

    iget-wide v4, p0, Lcom/google/android/apps/youtube/core/async/k;->a:J

    cmp-long v4, v0, v4

    if-ltz v4, :cond_0

    move-wide v0, v2

    :goto_0
    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/async/k;->e:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v3, Lcom/google/android/apps/youtube/core/async/l;

    invoke-direct {v3, p0, p1, p2}, Lcom/google/android/apps/youtube/core/async/l;-><init>(Lcom/google/android/apps/youtube/core/async/k;Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v0, v1, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    :goto_1
    return-void

    :cond_0
    iget-wide v4, p0, Lcom/google/android/apps/youtube/core/async/k;->a:J

    sub-long v0, v4, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/k;->b:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_1
.end method
