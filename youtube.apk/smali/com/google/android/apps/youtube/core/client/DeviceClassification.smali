.class public final Lcom/google/android/apps/youtube/core/client/DeviceClassification;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/apps/youtube/core/client/DeviceClassification$Platform;

.field private final d:Lcom/google/android/apps/youtube/core/client/DeviceClassification$SoftwareInterface;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/client/DeviceClassification$Platform;Lcom/google/android/apps/youtube/core/client/DeviceClassification$SoftwareInterface;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "packageName cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/DeviceClassification;->a:Ljava/lang/String;

    const-string v0, "appVersion cannot be empty"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/DeviceClassification;->b:Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/DeviceClassification$Platform;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/DeviceClassification;->c:Lcom/google/android/apps/youtube/core/client/DeviceClassification$Platform;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/DeviceClassification$SoftwareInterface;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/DeviceClassification;->d:Lcom/google/android/apps/youtube/core/client/DeviceClassification$SoftwareInterface;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/o;)Lcom/google/android/apps/youtube/common/e/o;
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "cplatform"

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/DeviceClassification;->c:Lcom/google/android/apps/youtube/core/client/DeviceClassification$Platform;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/client/DeviceClassification$Platform;->param:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "c"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/DeviceClassification;->d:Lcom/google/android/apps/youtube/core/client/DeviceClassification$SoftwareInterface;

    iget-object v2, v2, Lcom/google/android/apps/youtube/core/client/DeviceClassification$SoftwareInterface;->param:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "cver"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/DeviceClassification;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "cos"

    const-string v2, "Android"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "cosver"

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "cbr"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/DeviceClassification;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "cbrver"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/DeviceClassification;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "cbrand"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "cmodel"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    return-object p1
.end method
