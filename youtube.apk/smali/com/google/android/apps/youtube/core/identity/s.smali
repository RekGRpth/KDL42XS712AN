.class final Lcom/google/android/apps/youtube/core/identity/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/identity/v;

.field final synthetic b:Landroid/app/Activity;

.field final synthetic c:Lcom/google/android/apps/youtube/core/identity/o;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/identity/o;Lcom/google/android/apps/youtube/core/identity/v;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/identity/s;->c:Lcom/google/android/apps/youtube/core/identity/o;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/identity/s;->a:Lcom/google/android/apps/youtube/core/identity/v;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/identity/s;->b:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/s;->a:Lcom/google/android/apps/youtube/core/identity/v;

    invoke-interface {v0, p2}, Lcom/google/android/apps/youtube/core/identity/v;->a(Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-boolean v0, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->isEligibleForChannel:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/s;->c:Lcom/google/android/apps/youtube/core/identity/o;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/s;->a:Lcom/google/android/apps/youtube/core/identity/v;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/s;->b:Landroid/app/Activity;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/o;->a(Lcom/google/android/apps/youtube/core/identity/o;Lcom/google/android/apps/youtube/core/identity/v;Landroid/app/Activity;)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->isLightweight:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/s;->c:Lcom/google/android/apps/youtube/core/identity/o;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/s;->a:Lcom/google/android/apps/youtube/core/identity/v;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/s;->b:Landroid/app/Activity;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/o;->b(Lcom/google/android/apps/youtube/core/identity/o;Lcom/google/android/apps/youtube/core/identity/v;Landroid/app/Activity;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/s;->a:Lcom/google/android/apps/youtube/core/identity/v;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/identity/v;->a()V

    goto :goto_0
.end method
