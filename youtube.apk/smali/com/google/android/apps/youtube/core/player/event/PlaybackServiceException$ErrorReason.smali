.class public final enum Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

.field public static final enum LICENSE_SERVER_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

.field public static final enum NO_STREAMS:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

.field public static final enum PLAYER_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

.field public static final enum REQUEST_FAILED:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

.field public static final enum UNKNOWN:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

.field public static final enum UNPLAYABLE:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

.field public static final enum UNPLAYABLE_IN_BACKGROUND:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

.field public static final enum USER_CHECK_FAILED:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

.field public static final enum VIDEO_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

.field public static final enum WATCH_NEXT_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->UNKNOWN:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const-string v1, "VIDEO_ERROR"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->VIDEO_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const-string v1, "UNPLAYABLE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->UNPLAYABLE:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const-string v1, "REQUEST_FAILED"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->REQUEST_FAILED:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const-string v1, "USER_CHECK_FAILED"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->USER_CHECK_FAILED:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const-string v1, "LICENSE_SERVER_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->LICENSE_SERVER_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const-string v1, "PLAYER_ERROR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->PLAYER_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const-string v1, "NO_STREAMS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->NO_STREAMS:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const-string v1, "WATCH_NEXT_ERROR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->WATCH_NEXT_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const-string v1, "UNPLAYABLE_IN_BACKGROUND"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->UNPLAYABLE_IN_BACKGROUND:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->UNKNOWN:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->VIDEO_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->UNPLAYABLE:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->REQUEST_FAILED:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->USER_CHECK_FAILED:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->LICENSE_SERVER_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->PLAYER_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->NO_STREAMS:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->WATCH_NEXT_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->UNPLAYABLE_IN_BACKGROUND:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->$VALUES:[Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->$VALUES:[Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    return-object v0
.end method


# virtual methods
.method public final isCritical()Z
    .locals 3

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->UNKNOWN:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->VIDEO_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->UNPLAYABLE:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->REQUEST_FAILED:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->USER_CHECK_FAILED:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->LICENSE_SERVER_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->PLAYER_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->NO_STREAMS:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->UNPLAYABLE_IN_BACKGROUND:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->isIn([Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;)Z

    move-result v0

    return v0
.end method

.method public final varargs isIn([Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;)Z
    .locals 4

    const/4 v0, 0x0

    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    if-ne p0, v3, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
