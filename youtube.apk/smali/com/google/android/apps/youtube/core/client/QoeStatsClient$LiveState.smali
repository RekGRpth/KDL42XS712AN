.class public final enum Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

.field public static final enum DVR:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

.field public static final enum LIVE:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

.field public static final enum VOD:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    const-string v1, "VOD"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->VOD:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    const-string v1, "LIVE"

    const-string v2, "live"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->LIVE:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    const-string v1, "DVR"

    const-string v2, "dvr"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->DVR:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    sget-object v1, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->VOD:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->LIVE:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->DVR:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->$VALUES:[Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->value:Ljava/lang/String;

    return-void
.end method

.method public static fromOrdinal(I)Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;
    .locals 1

    if-ltz p0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->values()[Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    move-result-object v0

    array-length v0, v0

    if-lt p0, v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->VOD:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->values()[Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    move-result-object v0

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->$VALUES:[Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    return-object v0
.end method


# virtual methods
.method public final isLive()Z
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->VOD:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->value:Ljava/lang/String;

    return-object v0
.end method
