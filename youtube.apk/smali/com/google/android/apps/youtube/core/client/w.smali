.class final Lcom/google/android/apps/youtube/core/client/w;
.super Lcom/google/android/apps/youtube/core/async/x;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/client/v;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/client/v;Lcom/google/android/apps/youtube/core/identity/l;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/w;->a:Lcom/google/android/apps/youtube/core/client/v;

    invoke-direct {p0, p2}, Lcom/google/android/apps/youtube/core/async/x;-><init>(Lcom/google/android/apps/youtube/core/identity/l;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/lang/Exception;)Z
    .locals 1

    instance-of v0, p2, Lcom/google/android/apps/youtube/core/async/GDataResponseException;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataResponseException;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/async/GDataResponseException;->containsYouTubeSignupRequiredError()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "operation needs a full YouTube account"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/core/async/x;->a(Lcom/google/android/apps/youtube/core/async/w;Ljava/lang/Exception;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/google/android/apps/youtube/core/async/w;Ljava/lang/Exception;)Z
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/core/client/w;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)Z
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/core/client/w;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method
