.class final Lcom/google/android/apps/youtube/core/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/fromguava/e;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/q;

.field private b:Ljava/io/File;

.field private c:Lcom/google/android/exoplayer/upstream/cache/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/q;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/r;->a:Lcom/google/android/apps/youtube/core/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic b()Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/r;->a:Lcom/google/android/apps/youtube/core/q;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/q;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->aP()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "enable_exo_cache"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/r;->a:Lcom/google/android/apps/youtube/core/q;

    iget-object v2, v2, Lcom/google/android/apps/youtube/core/q;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/a;->ak()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/r;->a:Lcom/google/android/apps/youtube/core/q;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/q;->a:Lcom/google/android/apps/youtube/core/a;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/r;->b:Ljava/io/File;

    invoke-virtual {v1, v0}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/r;->b:Ljava/io/File;

    new-instance v0, Lcom/google/android/exoplayer/upstream/cache/g;

    new-instance v2, Ljava/io/File;

    const-string v3, "exo"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/exoplayer/upstream/cache/e;

    const-wide/32 v3, 0x4000000

    invoke-direct {v1, v3, v4}, Lcom/google/android/exoplayer/upstream/cache/e;-><init>(J)V

    invoke-direct {v0, v2, v1}, Lcom/google/android/exoplayer/upstream/cache/g;-><init>(Ljava/io/File;Lcom/google/android/exoplayer/upstream/cache/c;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/r;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/r;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    goto :goto_0
.end method
