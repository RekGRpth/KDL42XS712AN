.class final Lcom/google/android/apps/youtube/core/player/sequencer/o;
.super Lcom/google/android/apps/youtube/core/player/sequencer/k;
.source "SourceFile"


# instance fields
.field final synthetic c:Lcom/google/android/apps/youtube/core/player/sequencer/j;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/j;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/core/player/sequencer/k;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/j;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/sequencer/o;Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    iput-object p1, v0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/o;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/o;->a()V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/player/sequencer/o;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->b(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v1, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/event/r;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/player/event/r;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->f(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Lcom/google/android/apps/youtube/datalib/offline/n;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/offline/n;->a()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/a/k;->a()Lcom/google/android/apps/youtube/datalib/a/k;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->j(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Lcom/google/android/apps/youtube/core/player/fetcher/e;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->g(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->h(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/lang/String;

    move-result-object v2

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->g(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, p1

    :goto_1
    const-string v4, ""

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v5}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->i(Lcom/google/android/apps/youtube/core/player/sequencer/j;)[B

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/player/fetcher/e;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[BLcom/google/android/apps/youtube/datalib/a/l;)V

    const-wide/16 v0, 0x3

    :try_start_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/a/k;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->k(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->b(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/util/List;

    move-result-object v3

    invoke-static {v0, v1, v2, v3, p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->createWatchNextResponseForOfflinePlaylist(Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;Landroid/content/Context;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;I)Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->d(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/core/player/sequencer/p;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/p;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/o;Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_2
    return-void

    :cond_1
    const-string v2, ""

    goto :goto_0

    :cond_2
    const/4 v3, -0x1

    goto :goto_1

    :catch_0
    move-exception v0

    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->g(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->k(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->b(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/util/List;

    move-result-object v2

    invoke-static {v0, v1, v2, p1}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;I)Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    move-result-object v0

    :goto_4
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->d(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/core/player/sequencer/q;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/q;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/o;Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/o;->c:Lcom/google/android/apps/youtube/core/player/sequencer/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->b(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    move-result-object v0

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_3
.end method
