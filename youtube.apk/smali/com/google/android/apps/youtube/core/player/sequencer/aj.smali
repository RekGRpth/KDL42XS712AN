.class final Lcom/google/android/apps/youtube/core/player/sequencer/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;

.field private final b:Ljava/lang/Exception;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/ae;Ljava/lang/Exception;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/aj;->a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/aj;->b:Ljava/lang/Exception;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/aj;->a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/aj;->a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->WATCH_NEXT_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/aj;->a:Lcom/google/android/apps/youtube/core/player/sequencer/ae;

    iget-object v4, v4, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->e:Lcom/google/android/apps/youtube/core/aw;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/sequencer/aj;->b:Ljava/lang/Exception;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/sequencer/aj;->b:Ljava/lang/Exception;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;-><init>(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;ZLjava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    return-void
.end method
