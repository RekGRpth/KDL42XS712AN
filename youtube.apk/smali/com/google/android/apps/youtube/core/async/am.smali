.class final Lcom/google/android/apps/youtube/core/async/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/async/ak;

.field private final b:Lcom/google/android/apps/youtube/common/a/b;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/async/ak;Lcom/google/android/apps/youtube/common/a/b;Z)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/async/am;->a:Lcom/google/android/apps/youtube/core/async/ak;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/a/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/am;->b:Lcom/google/android/apps/youtube/common/a/b;

    iput-boolean p3, p0, Lcom/google/android/apps/youtube/core/async/am;->c:Z

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    check-cast p1, Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/am;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p1, Landroid/net/Uri;

    check-cast p2, Ljava/lang/Long;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/async/am;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/async/am;->c:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/am;->a:Lcom/google/android/apps/youtube/core/async/ak;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/async/ak;->a(Lcom/google/android/apps/youtube/core/async/ak;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/am;->a:Lcom/google/android/apps/youtube/core/async/ak;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/ak;->a(Lcom/google/android/apps/youtube/core/async/ak;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    invoke-interface {v0, p1, p0}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/am;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method
