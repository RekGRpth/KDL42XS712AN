.class public abstract Lcom/google/android/apps/youtube/core/async/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# instance fields
.field protected final a:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

.field private final b:Lcom/google/android/apps/youtube/core/async/af;

.field private final c:Lcom/google/android/apps/youtube/core/async/af;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/async/af;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/a;->a:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/a;->b:Lcom/google/android/apps/youtube/core/async/af;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/a;->c:Lcom/google/android/apps/youtube/core/async/af;

    return-void
.end method

.method private c(Ljava/lang/Object;Landroid/net/Uri;)Ljava/util/List;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    invoke-static {}, Lcom/google/android/apps/youtube/common/a/c;->a()Lcom/google/android/apps/youtube/common/a/c;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/async/a;->c:Lcom/google/android/apps/youtube/core/async/af;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/core/async/a;->b(Ljava/lang/Object;Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object p2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->nextUri:Landroid/net/Uri;

    iget v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->totalResults:I

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-le v0, v2, :cond_1

    if-nez p2, :cond_0

    :cond_1
    return-object v1
.end method


# virtual methods
.method protected abstract a(Ljava/lang/Object;Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
.end method

.method protected abstract a(Ljava/lang/Object;)Ljava/lang/String;
.end method

.method public final a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 4

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://gdata.youtube.com/feeds/api/playlists/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/async/a;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/youtube/common/a/c;->a()Lcom/google/android/apps/youtube/common/a/c;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/async/a;->b:Lcom/google/android/apps/youtube/core/async/af;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/youtube/core/async/a;->a(Ljava/lang/Object;Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/async/a;->c(Ljava/lang/Object;Landroid/net/Uri;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/common/a/c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, v0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p2, p1, v1}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-interface {p2, p1, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected abstract b(Ljava/lang/Object;Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
.end method
