.class final Lcom/google/android/apps/youtube/core/client/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/cache/m;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/client/v;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/client/v;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/ae;->a:Lcom/google/android/apps/youtube/core/client/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/client/v;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/client/ae;-><init>(Lcom/google/android/apps/youtube/core/client/v;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/apps/youtube/common/cache/a;)V
    .locals 6

    check-cast p2, Lcom/google/android/apps/youtube/core/async/Timestamped;

    iget-object v0, p2, Lcom/google/android/apps/youtube/core/async/Timestamped;->element:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->type:Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;->CHANNEL:Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription$Type;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/ae;->a:Lcom/google/android/apps/youtube/core/client/v;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/client/v;->a(Lcom/google/android/apps/youtube/core/client/v;)Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->h(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/youtube/core/async/Timestamped;

    iget-wide v4, p2, Lcom/google/android/apps/youtube/core/async/Timestamped;->timestamp:J

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/apps/youtube/core/async/Timestamped;-><init>(Ljava/lang/Object;J)V

    invoke-interface {p3, v2, v3}, Lcom/google/android/apps/youtube/common/cache/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method
