.class final Lcom/google/android/apps/youtube/core/async/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/common/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/a/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/aj;->a:Lcom/google/android/apps/youtube/common/a/b;

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/aj;->a:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    check-cast p2, Ljava/util/List;

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    const-string v1, "start-index"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    const-string v2, "max-results"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    new-instance v2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;-><init>()V

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->startIndex(I)Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->elementsPerPage(I)Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->totalResults(I)Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;

    if-lez v0, :cond_0

    iget-object v3, p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    const/4 v4, 0x0

    sub-int v5, v0, v1

    add-int/lit8 v5, v5, 0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v4, v1}, Lcom/google/android/apps/youtube/core/async/ag;->a(Landroid/net/Uri;II)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->previousUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;

    :cond_0
    add-int v3, v0, v1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    iget-object v3, p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    add-int v4, v0, v1

    add-int/lit8 v4, v4, 0x1

    invoke-static {v3, v4, v1}, Lcom/google/android/apps/youtube/core/async/ag;->a(Landroid/net/Uri;II)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->nextUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;

    :cond_1
    invoke-static {p2}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    add-int/2addr v1, v0

    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-interface {p2, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->addEntries(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/aj;->a:Lcom/google/android/apps/youtube/common/a/b;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->build()Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/aj;->a:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v1, p1, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
