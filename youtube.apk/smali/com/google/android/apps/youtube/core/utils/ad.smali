.class final Lcom/google/android/apps/youtube/core/utils/ad;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 8

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/an;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/an;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "w"

    aput-object v2, v1, v7

    const-string v2, "win"

    aput-object v2, v1, v6

    invoke-static {p2, v1}, Lcom/google/android/apps/youtube/core/utils/aa;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v7}, Lcom/google/android/apps/youtube/common/e/m;->b(Ljava/lang/String;I)I

    move-result v1

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "t"

    aput-object v3, v2, v7

    const-string v3, "start"

    aput-object v3, v2, v6

    invoke-static {p2, v2}, Lcom/google/android/apps/youtube/core/utils/aa;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    new-array v3, v4, [Ljava/lang/String;

    const-string v4, "d"

    aput-object v4, v3, v7

    const-string v4, "dur"

    aput-object v4, v3, v6

    invoke-static {p2, v3}, Lcom/google/android/apps/youtube/core/utils/aa;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v7}, Lcom/google/android/apps/youtube/common/e/m;->b(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "\n"

    const-string v5, "<br/>"

    invoke-virtual {p3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/String;

    const-string v6, "append"

    aput-object v6, v5, v7

    invoke-static {p2, v5}, Lcom/google/android/apps/youtube/core/utils/aa;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_0

    add-int/2addr v3, v2

    invoke-virtual {v0, v1, v4, v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/an;->a(ILjava/lang/String;II)Lcom/google/android/apps/youtube/datalib/legacy/model/an;

    :goto_0
    return-void

    :cond_0
    add-int/2addr v3, v2

    invoke-virtual {v0, v1, v4, v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/an;->b(ILjava/lang/String;II)Lcom/google/android/apps/youtube/datalib/legacy/model/an;

    goto :goto_0
.end method
