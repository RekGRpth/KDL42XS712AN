.class public final Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;
.super Lcom/google/android/apps/youtube/core/player/j;
.source "SourceFile"


# static fields
.field public static final b:Ljava/util/Set;


# instance fields
.field private final c:Landroid/os/Handler;

.field private final d:Ljava/lang/Runnable;

.field private e:Lcom/google/android/apps/youtube/medialib/player/o;

.field private f:I

.field private g:I

.field private h:Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    const-string v1, "video/wvm"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->b:Ljava/util/Set;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;I)I
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->g:I

    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    iput p1, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->g:I

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->h:Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;->PLAYING:Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;

    if-eq v0, v1, :cond_1

    :cond_0
    iput p1, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->f:I

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->f:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->i:Z

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/core/player/j;->a(I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/medialib/player/n;)V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->i:Z

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->f:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->f:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->a(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->h:Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;->PAUSED:Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;

    if-ne v0, v1, :cond_2

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/j;->b()V

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/j;->c()V

    :cond_0
    :goto_1
    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/core/player/j;->a(Lcom/google/android/apps/youtube/medialib/player/n;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/aq;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/player/aq;-><init>(Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->h:Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;->PLAYING:Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;

    if-ne v0, v1, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/j;->b()V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/youtube/medialib/player/o;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/core/player/j;->a(Lcom/google/android/apps/youtube/medialib/player/o;)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->e:Lcom/google/android/apps/youtube/medialib/player/o;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/medialib/player/n;II)Z
    .locals 6

    const/4 v5, 0x0

    const/4 v4, -0x1

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/player/j;->a(Lcom/google/android/apps/youtube/medialib/player/n;II)Z

    return v5

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->f()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->e()I

    move-result v1

    sub-int/2addr v0, v1

    int-to-long v0, v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->h:Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;->PLAYING:Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;

    if-ne v2, v3, :cond_1

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/j;->c()V

    const/16 v0, 0x2bd

    invoke-super {p0, p1, v0, v4}, Lcom/google/android/apps/youtube/core/player/j;->a(Lcom/google/android/apps/youtube/medialib/player/n;II)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->i:Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-boolean v5, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->i:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->h:Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;->PLAYING:Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;

    if-ne v0, v1, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/j;->b()V

    const/16 v0, 0x2be

    invoke-super {p0, p1, v0, v4}, Lcom/google/android/apps/youtube/core/player/j;->a(Lcom/google/android/apps/youtube/medialib/player/n;II)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2f1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->i:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/j;->b()V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;->PLAYING:Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->h:Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->f:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->f:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->a(I)V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->i:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/j;->c()V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;->PAUSED:Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->h:Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/j;->d()V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;->IDLE:Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->h:Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer$State;

    return-void
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->g:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->g:I

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/j;->e()I

    move-result v0

    goto :goto_0
.end method

.method public final h()Ljava/util/Set;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/TvWidevineMediaPlayer;->b:Ljava/util/Set;

    return-object v0
.end method
