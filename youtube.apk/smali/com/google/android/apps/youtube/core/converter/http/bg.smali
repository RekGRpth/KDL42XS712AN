.class final Lcom/google/android/apps/youtube/core/converter/http/bg;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;)V
    .locals 4

    const-string v0, "type"

    invoke-interface {p2, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/youtube/core/converter/http/bb;->b()Ljava/util/Map;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/http/bb;->a(Ljava/lang/String;Ljava/util/Map;I)I

    move-result v1

    const-string v0, "link_url"

    invoke-interface {p2, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    new-instance v2, Lcom/google/android/apps/youtube/datalib/legacy/model/i;

    const-string v3, "title"

    invoke-interface {p2, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v0, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/i;-><init>(ILandroid/net/Uri;Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Lcom/google/android/apps/youtube/common/e/l;->offer(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "Badly formed action uri - ignoring action"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 2

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/i;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/i;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;

    move-result-object v1

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/f;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/f;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;)Lcom/google/android/apps/youtube/datalib/legacy/model/f;

    return-void
.end method
