.class final Lcom/google/android/apps/youtube/core/converter/http/ew;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/common/e/b;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/converter/http/ew;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 4

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    new-instance v1, Lcom/google/a/a/a/a/fj;

    invoke-direct {v1}, Lcom/google/a/a/a/a/fj;-><init>()V

    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/a/a/a/a/fj;->c:Ljava/lang/String;

    const-string v2, "type"

    invoke-interface {p2, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/a/a/a/a/fj;->f:Ljava/lang/String;

    const-string v2, "height"

    invoke-interface {p2, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/core/converter/http/em;->a(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, Lcom/google/a/a/a/a/fj;->i:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/converter/http/ew;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/a/a/a/a/fj;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    return-void
.end method
