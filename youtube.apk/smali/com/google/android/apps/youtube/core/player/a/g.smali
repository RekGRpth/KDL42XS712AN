.class public final Lcom/google/android/apps/youtube/core/player/a/g;
.super Lcom/google/android/apps/youtube/core/player/j;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/apps/youtube/core/player/a/l;

.field private final c:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/medialib/player/n;Lcom/google/android/apps/youtube/core/player/a/l;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/j;-><init>(Lcom/google/android/apps/youtube/medialib/player/n;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/a/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/g;->b:Lcom/google/android/apps/youtube/core/player/a/l;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/a/g;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 8

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/e/p;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/pudl"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/g;->b:Lcom/google/android/apps/youtube/core/player/a/l;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/g;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getVideoId()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/g;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v3

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/g;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getContentLength()J

    move-result-wide v4

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/g;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getLastModified()J

    move-result-wide v6

    move-object v1, p2

    invoke-interface/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/player/a/l;->a(Landroid/net/Uri;Ljava/lang/String;IJJ)Landroid/net/Uri;

    move-result-object p2

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/player/j;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
