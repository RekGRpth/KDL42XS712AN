.class public final Lcom/google/android/apps/youtube/core/identity/as;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/accounts/AccountManager;

.field protected final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/as;->a:Landroid/accounts/AccountManager;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "http://gdata.youtube.com"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/Util;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/as;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/as;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/as;->a:Landroid/accounts/AccountManager;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/as;->c:Ljava/lang/String;

    const-string v0, "com.google"

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/as;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/core/identity/l;)Landroid/accounts/Account;
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/identity/l;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/identity/as;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Landroid/accounts/Account;
    .locals 6

    const/4 v0, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/identity/as;->b()[Landroid/accounts/Account;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v1, v3, v2

    iget-object v5, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v5, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/as;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()[Landroid/accounts/Account;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/as;->a:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/as;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method
