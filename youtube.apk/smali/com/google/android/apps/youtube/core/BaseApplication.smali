.class public abstract Lcom/google/android/apps/youtube/core/BaseApplication;
.super Landroid/app/Application;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/client/bd;


# instance fields
.field private a:Lcom/google/android/apps/youtube/common/network/ConnectivityReceiver;

.field private b:Lcom/google/android/apps/youtube/core/a;

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method


# virtual methods
.method public final A()Lcom/google/android/apps/youtube/core/aw;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->b:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    return-object v0
.end method

.method public final B()Landroid/content/SharedPreferences;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->b:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->aP()Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public final C()Lcom/google/android/apps/youtube/common/network/h;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->b:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v0

    return-object v0
.end method

.method public final D()Lcom/google/android/apps/youtube/core/Analytics;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->b:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    return-object v0
.end method

.method public final E()Lcom/google/android/apps/youtube/common/c/a;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->b:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    return-object v0
.end method

.method protected a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected abstract b()Lcom/google/android/apps/youtube/core/a;
.end method

.method protected c()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/BaseApplication;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->b:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->aP()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "version"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->c:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->b:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->aP()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "version"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "device_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "device_key"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/common/network/ConnectivityReceiver;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/BaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->b:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/a;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/common/network/ConnectivityReceiver;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/c/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->a:Lcom/google/android/apps/youtube/common/network/ConnectivityReceiver;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->b:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->bh()Lcom/google/android/apps/youtube/common/d/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/d/j;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->b:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->b:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/a;->bi()Lcom/google/android/apps/youtube/common/d/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    const-string v0, "?"

    return-object v0
.end method

.method public final onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/BaseApplication;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->d:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/BaseApplication;->b()Lcom/google/android/apps/youtube/core/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->b:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/BaseApplication;->c()V

    :cond_0
    return-void
.end method

.method public x()Lcom/google/android/apps/youtube/datalib/innertube/ag;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public y()Lcom/google/android/apps/youtube/core/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->b:Lcom/google/android/apps/youtube/core/a;

    return-object v0
.end method

.method public final z()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/BaseApplication;->c:Z

    return v0
.end method
