.class public Lcom/google/android/apps/youtube/core/client/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/google/android/apps/common/csi/lib/i;

.field private c:Lcom/google/android/apps/common/csi/lib/h;

.field private d:Ljava/util/Set;

.field private e:Z

.field private final f:Lcom/google/android/apps/common/csi/lib/j;

.field private final g:Ljava/lang/String;

.field private final h:Lcom/google/a/a/a/a/dt;

.field private final i:I

.field private j:Ljava/util/Set;


# direct methods
.method protected constructor <init>(Ljava/lang/String;IZLjava/lang/String;Lcom/google/a/a/a/a/dt;)V
    .locals 7

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v6, Lcom/google/android/apps/common/csi/lib/b;

    invoke-direct {v6}, Lcom/google/android/apps/common/csi/lib/b;-><init>()V

    move-object v0, p0

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/client/a/a;-><init>(Ljava/lang/String;IZLjava/lang/String;Lcom/google/a/a/a/a/dt;Lcom/google/android/apps/common/csi/lib/j;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZLjava/lang/String;Lcom/google/a/a/a/a/dt;Lcom/google/android/apps/common/csi/lib/j;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/a;->a:Ljava/lang/String;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/common/csi/lib/j;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/a;->f:Lcom/google/android/apps/common/csi/lib/j;

    iput-boolean p3, p0, Lcom/google/android/apps/youtube/core/client/a/a;->e:Z

    iput-object p4, p0, Lcom/google/android/apps/youtube/core/client/a/a;->g:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/youtube/core/client/a/a;->h:Lcom/google/a/a/a/a/dt;

    iput p2, p0, Lcom/google/android/apps/youtube/core/client/a/a;->i:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/a;->d:Ljava/util/Set;

    return-void
.end method

.method private b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/a;->b:Lcom/google/android/apps/common/csi/lib/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/a;->c:Lcom/google/android/apps/common/csi/lib/h;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/common/csi/lib/i;
    .locals 5

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CsiAction.start() should be called before report. Ignored."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "mod_li"

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/a/a;->e:Z

    if-eqz v0, :cond_2

    const-string v0, "1"

    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/core/client/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, ""

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/a/a;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "egs"

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/a/a;->g:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/client/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/a;->h:Lcom/google/a/a/a/a/dt;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/a;->h:Lcom/google/a/a/a/a/dt;

    iget-object v0, v0, Lcom/google/a/a/a/a/dt;->b:[Lcom/google/a/a/a/a/ix;

    array-length v0, v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/a;->h:Lcom/google/a/a/a/a/dt;

    iget-object v1, v0, Lcom/google/a/a/a/a/dt;->b:[Lcom/google/a/a/a/a/ix;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    iget-object v4, v3, Lcom/google/a/a/a/a/ix;->b:Ljava/lang/String;

    iget-object v3, v3, Lcom/google/a/a/a/a/ix;->c:Ljava/lang/String;

    invoke-virtual {p0, v4, v3}, Lcom/google/android/apps/youtube/core/client/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    const-string v0, "0"

    goto :goto_1

    :cond_3
    const-string v0, "conn"

    iget v1, p0, Lcom/google/android/apps/youtube/core/client/a/a;->i:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/client/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/a;->b:Lcom/google/android/apps/common/csi/lib/i;

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/apps/youtube/core/client/a/g;Ljava/util/Set;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/a/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CsiAction ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] already started. Ignored."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/a;->j:Ljava/util/Set;

    new-instance v0, Lcom/google/android/apps/common/csi/lib/i;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/a/a;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/a/a;->f:Lcom/google/android/apps/common/csi/lib/j;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/common/csi/lib/i;-><init>(Ljava/lang/String;Lcom/google/android/apps/common/csi/lib/j;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/a;->b:Lcom/google/android/apps/common/csi/lib/i;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/a;->b:Lcom/google/android/apps/common/csi/lib/i;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/client/a/g;->a()J

    move-result-wide v0

    new-instance v2, Lcom/google/android/apps/common/csi/lib/h;

    invoke-direct {v2, v0, v1, v3, v3}, Lcom/google/android/apps/common/csi/lib/h;-><init>(JLjava/lang/String;Lcom/google/android/apps/common/csi/lib/h;)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/client/a/a;->c:Lcom/google/android/apps/common/csi/lib/h;

    goto :goto_0
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/a;->b:Lcom/google/android/apps/common/csi/lib/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/common/csi/lib/i;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/a/a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CsiAction not yet started."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/a;->b:Lcom/google/android/apps/common/csi/lib/i;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/common/csi/lib/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Lcom/google/android/apps/youtube/core/client/a/g;)Z
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/a/a;->b()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "CsiAction ["

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "] not yet started."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/a/a;->d:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/client/a/g;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "CsiAction [%s] already ticked %s. Ignored."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/client/a/g;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/a/a;->j:Ljava/util/Set;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/a/a;->d:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    if-le v2, v0, :cond_3

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/client/a/g;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/a/a;->b:Lcom/google/android/apps/common/csi/lib/i;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/a/a;->c:Lcom/google/android/apps/common/csi/lib/h;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/client/a/g;->a()J

    move-result-wide v4

    new-array v6, v0, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/client/a/g;->b()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/apps/common/csi/lib/i;->a(Lcom/google/android/apps/common/csi/lib/h;J[Ljava/lang/String;)Z

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/a/a;->d:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/client/a/g;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const-string v2, "CsiAction [%s] triggered with no registered label"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method
