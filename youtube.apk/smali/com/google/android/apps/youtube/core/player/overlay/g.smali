.class public interface abstract Lcom/google/android/apps/youtube/core/player/overlay/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/ax;


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(Z)V
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public abstract f()V
.end method

.method public abstract g()V
.end method

.method public abstract h()V
.end method

.method public abstract i()V
.end method

.method public abstract j()V
.end method

.method public abstract setAdStyle(Z)V
.end method

.method public abstract setCallToActionImage(Landroid/graphics/Bitmap;)V
.end method

.method public abstract setCallToActionText(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract setFeaturedChannelImage(Landroid/graphics/Bitmap;)V
.end method

.method public abstract setFeaturedVideoImage(Landroid/graphics/Bitmap;)V
.end method

.method public abstract setFeaturedVideoTitle(Ljava/lang/String;)V
.end method

.method public abstract setInfoCardTeaserImage(Landroid/graphics/Bitmap;)V
.end method

.method public abstract setInfoCardTeaserMessage(Ljava/lang/CharSequence;)V
.end method

.method public abstract setListener(Lcom/google/android/apps/youtube/core/player/overlay/h;)V
.end method

.method public abstract setVisible(Z)V
.end method
