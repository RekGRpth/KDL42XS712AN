.class public final enum Lcom/google/android/apps/youtube/core/player/Director$VideoStage;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

.field public static final enum AD_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

.field public static final enum AD_LOADING:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

.field public static final enum ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

.field public static final enum MEDIA_AD_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

.field public static final enum MEDIA_PLAYING_AD:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

.field public static final enum MEDIA_PLAYING_VIDEO:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

.field public static final enum MEDIA_VIDEO_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

.field public static final enum NEW:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

.field public static final enum PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

.field public static final enum READY:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const-string v1, "NEW"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->NEW:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const-string v1, "PLAYBACK_LOADED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const-string v1, "AD_LOADING"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->AD_LOADING:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const-string v1, "AD_LOADED"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->AD_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const-string v1, "MEDIA_AD_PLAY_REQUESTED"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_AD_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const-string v1, "MEDIA_PLAYING_AD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_AD:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const-string v1, "READY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->READY:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const-string v1, "MEDIA_VIDEO_PLAY_REQUESTED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_VIDEO_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const-string v1, "MEDIA_PLAYING_VIDEO"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_VIDEO:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const-string v1, "ENDED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->NEW:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->AD_LOADING:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->AD_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_AD_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_AD:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->READY:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_VIDEO_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_VIDEO:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->$VALUES:[Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/Director$VideoStage;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/player/Director$VideoStage;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->$VALUES:[Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    return-object v0
.end method


# virtual methods
.method public final varargs isIn([Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z
    .locals 4

    const/4 v0, 0x0

    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    if-ne p0, v3, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final isOrPast(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ordinal()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isPlaying()Z
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_AD_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_AD:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_VIDEO_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_VIDEO:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isIn([Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    return v0
.end method

.method public final isPlayingAd()Z
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_AD_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_AD:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isIn([Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    return v0
.end method

.method public final isPlayingVideo()Z
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_VIDEO_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_VIDEO:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isIn([Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    return v0
.end method

.method public final onAd()Z
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->AD_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_AD_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_AD:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isIn([Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    return v0
.end method
