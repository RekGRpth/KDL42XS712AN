.class final Lcom/google/android/apps/youtube/core/identity/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/identity/f;

.field final synthetic b:Landroid/app/Activity;

.field final synthetic c:Lcom/google/android/apps/youtube/core/identity/al;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/identity/al;Lcom/google/android/apps/youtube/core/identity/f;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/identity/am;->c:Lcom/google/android/apps/youtube/core/identity/al;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/identity/am;->a:Lcom/google/android/apps/youtube/core/identity/f;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/identity/am;->b:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/am;->c:Lcom/google/android/apps/youtube/core/identity/al;

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/core/identity/al;->a(Lcom/google/android/apps/youtube/core/identity/al;Ljava/lang/Exception;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    iget v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->totalResults:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/am;->c:Lcom/google/android/apps/youtube/core/identity/al;

    const-string v1, "No +Page Delegate"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/identity/al;->a(Lcom/google/android/apps/youtube/core/identity/al;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/am;->c:Lcom/google/android/apps/youtube/core/identity/al;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/am;->a:Lcom/google/android/apps/youtube/core/identity/f;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/identity/al;->a(Lcom/google/android/apps/youtube/core/identity/al;Lcom/google/android/apps/youtube/core/identity/f;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/am;->c:Lcom/google/android/apps/youtube/core/identity/al;

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/core/identity/al;->a(Lcom/google/android/apps/youtube/core/identity/al;Lcom/google/android/apps/youtube/datalib/legacy/model/Page;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/am;->c:Lcom/google/android/apps/youtube/core/identity/al;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/am;->b:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/am;->a:Lcom/google/android/apps/youtube/core/identity/f;

    iget-object v3, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/identity/al;->a(Lcom/google/android/apps/youtube/core/identity/al;Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/f;Ljava/util/List;)V

    goto :goto_0
.end method
