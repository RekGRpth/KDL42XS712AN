.class public final Lcom/google/android/apps/youtube/core/player/notification/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/notification/i;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/Class;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private f:Landroid/app/Service;

.field private g:Landroid/app/NotificationManager;

.field private h:Ljava/lang/String;

.field private final i:Landroid/content/BroadcastReceiver;

.field private final j:Landroid/content/IntentFilter;

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/apps/youtube/core/player/notification/j;Ljava/lang/String;I)V
    .locals 8

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/player/notification/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/apps/youtube/core/player/notification/j;Ljava/lang/String;ILandroid/app/Service;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/apps/youtube/core/player/notification/j;Ljava/lang/String;ILandroid/app/Service;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->a:Landroid/content/Context;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->c:Ljava/lang/Class;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->b:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->d:Ljava/lang/String;

    iput p6, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->e:I

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->g:Landroid/app/NotificationManager;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->j:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->j:Landroid/content/IntentFilter;

    const-string v1, "com.google.android.youtube.action.controller_notification_prev"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->j:Landroid/content/IntentFilter;

    const-string v1, "com.google.android.youtube.action.controller_notification_play_pause"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->j:Landroid/content/IntentFilter;

    const-string v1, "com.google.android.youtube.action.controller_notification_next"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->j:Landroid/content/IntentFilter;

    const-string v1, "com.google.android.youtube.action.controller_notification_close"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->j:Landroid/content/IntentFilter;

    const-string v1, "com.google.android.youtube.action.controller_notification_replay"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/player/notification/e;

    invoke-direct {v0, p0, p4}, Lcom/google/android/apps/youtube/core/player/notification/e;-><init>(Lcom/google/android/apps/youtube/core/player/notification/d;Lcom/google/android/apps/youtube/core/player/notification/j;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->i:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;Z)Landroid/widget/RemoteViews;
    .locals 8

    const/4 v1, 0x1

    const/16 v4, 0x8

    const/4 v2, 0x0

    if-eqz p2, :cond_3

    sget v0, Lcom/google/android/youtube/l;->aC:I

    :goto_0
    new-instance v6, Landroid/widget/RemoteViews;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v3, v0}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    sget v0, Lcom/google/android/youtube/j;->fF:I

    iget-object v3, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->b:Ljava/lang/String;

    invoke-virtual {v6, v0, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    sget v0, Lcom/google/android/youtube/j;->ff:I

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->b:Ljava/lang/String;

    invoke-virtual {v6, v0, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->c:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->PLAYING:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    if-ne v0, v3, :cond_4

    sget v0, Lcom/google/android/youtube/h;->Q:I

    :goto_1
    sget v3, Lcom/google/android/youtube/j;->dn:I

    invoke-virtual {v6, v3, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->c:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->BUFFERING:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    if-ne v0, v3, :cond_5

    move v0, v1

    :goto_2
    sget v5, Lcom/google/android/youtube/j;->I:I

    if-eqz v0, :cond_6

    move v3, v2

    :goto_3
    invoke-virtual {v6, v5, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget-object v3, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->c:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    sget-object v5, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->ENDED:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    if-ne v3, v5, :cond_7

    move v3, v1

    :goto_4
    sget v7, Lcom/google/android/youtube/j;->eg:I

    if-eqz v3, :cond_8

    move v5, v2

    :goto_5
    invoke-virtual {v6, v7, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget-object v5, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->c:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    sget-object v7, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->ERROR:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    if-ne v5, v7, :cond_9

    :goto_6
    sget v7, Lcom/google/android/youtube/j;->aV:I

    if-eqz v1, :cond_a

    move v5, v2

    :goto_7
    invoke-virtual {v6, v7, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v5, Lcom/google/android/youtube/j;->dn:I

    if-nez v0, :cond_b

    if-nez v3, :cond_b

    if-nez v1, :cond_b

    move v0, v2

    :goto_8
    invoke-virtual {v6, v5, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/youtube/j;->fy:I

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v0, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    :cond_0
    if-eqz p2, :cond_e

    sget v0, Lcom/google/android/youtube/j;->dN:I

    const-string v1, "setEnabled"

    iget-boolean v2, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->d:Z

    invoke-virtual {v6, v0, v1, v2}, Landroid/widget/RemoteViews;->setBoolean(ILjava/lang/String;Z)V

    sget v0, Lcom/google/android/youtube/j;->cK:I

    const-string v1, "setEnabled"

    iget-boolean v2, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->e:Z

    invoke-virtual {v6, v0, v1, v2}, Landroid/widget/RemoteViews;->setBoolean(ILjava/lang/String;Z)V

    iget-boolean v0, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->d:Z

    if-eqz v0, :cond_c

    sget v0, Lcom/google/android/youtube/h;->S:I

    :goto_9
    sget v1, Lcom/google/android/youtube/j;->dN:I

    invoke-virtual {v6, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    iget-boolean v0, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->e:Z

    if-eqz v0, :cond_d

    sget v0, Lcom/google/android/youtube/h;->O:I

    :goto_a
    sget v1, Lcom/google/android/youtube/j;->cK:I

    invoke-virtual {v6, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    :cond_1
    :goto_b
    if-eqz p2, :cond_2

    sget v0, Lcom/google/android/youtube/j;->dN:I

    const-string v1, "com.google.android.youtube.action.controller_notification_prev"

    invoke-direct {p0, v6, v0, v1}, Lcom/google/android/apps/youtube/core/player/notification/d;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    :cond_2
    sget v0, Lcom/google/android/youtube/j;->dn:I

    const-string v1, "com.google.android.youtube.action.controller_notification_play_pause"

    invoke-direct {p0, v6, v0, v1}, Lcom/google/android/apps/youtube/core/player/notification/d;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    sget v0, Lcom/google/android/youtube/j;->cK:I

    const-string v1, "com.google.android.youtube.action.controller_notification_next"

    invoke-direct {p0, v6, v0, v1}, Lcom/google/android/apps/youtube/core/player/notification/d;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    sget v0, Lcom/google/android/youtube/j;->eg:I

    const-string v1, "com.google.android.youtube.action.controller_notification_replay"

    invoke-direct {p0, v6, v0, v1}, Lcom/google/android/apps/youtube/core/player/notification/d;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    sget v0, Lcom/google/android/youtube/j;->aw:I

    const-string v1, "com.google.android.youtube.action.controller_notification_close"

    invoke-direct {p0, v6, v0, v1}, Lcom/google/android/apps/youtube/core/player/notification/d;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    return-object v6

    :cond_3
    sget v0, Lcom/google/android/youtube/l;->aD:I

    goto/16 :goto_0

    :cond_4
    sget v0, Lcom/google/android/youtube/h;->R:I

    goto/16 :goto_1

    :cond_5
    move v0, v2

    goto/16 :goto_2

    :cond_6
    move v3, v4

    goto/16 :goto_3

    :cond_7
    move v3, v2

    goto/16 :goto_4

    :cond_8
    move v5, v4

    goto/16 :goto_5

    :cond_9
    move v1, v2

    goto/16 :goto_6

    :cond_a
    move v5, v4

    goto/16 :goto_7

    :cond_b
    move v0, v4

    goto :goto_8

    :cond_c
    sget v0, Lcom/google/android/youtube/h;->T:I

    goto :goto_9

    :cond_d
    sget v0, Lcom/google/android/youtube/h;->P:I

    goto :goto_a

    :cond_e
    sget v0, Lcom/google/android/youtube/j;->cK:I

    iget-boolean v1, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->e:Z

    if-eqz v1, :cond_f

    :goto_c
    invoke-virtual {v6, v0, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/youtube/j;->ff:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_b

    :cond_f
    move v2, v4

    goto :goto_c
.end method

.method private a(Landroid/widget/RemoteViews;ILjava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->a:Landroid/content/Context;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->f:Landroid/app/Service;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->f:Landroid/app/Service;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Service;->stopForeground(Z)V

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->k:Z

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->g:Landroid/app/NotificationManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;)V
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-direct {p0, p1, v7}, Lcom/google/android/apps/youtube/core/player/notification/d;->a(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-direct {p0, p1, v6}, Lcom/google/android/apps/youtube/core/player/notification/d;->a(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;Z)Landroid/widget/RemoteViews;

    move-result-object v1

    new-instance v2, Landroid/support/v4/app/al;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/support/v4/app/al;-><init>(Landroid/content/Context;)V

    iget v3, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->e:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/al;->a(I)Landroid/support/v4/app/al;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/support/v4/app/al;->a(Z)Landroid/support/v4/app/al;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/al;->a(Landroid/widget/RemoteViews;)Landroid/support/v4/app/al;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/support/v4/app/al;->d(Ljava/lang/CharSequence;)Landroid/support/v4/app/al;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->a:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->c:Ljava/lang/Class;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v4, 0x4000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->a:Landroid/content/Context;

    const/high16 v5, 0x8000000

    invoke-static {v4, v7, v3, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/al;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/al;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/al;->a()Landroid/app/Notification;

    move-result-object v2

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v3, v4, :cond_0

    iput-object v1, v2, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xa

    if-gt v1, v3, :cond_1

    iput-object v0, v2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->f:Landroid/app/Service;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->f:Landroid/app/Service;

    invoke-virtual {v0, v8, v2}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->k:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->i:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->j:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iput-boolean v6, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->k:Z

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->g:Landroid/app/NotificationManager;

    invoke-virtual {v0, v8, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/notification/d;->h:Ljava/lang/String;

    return-void
.end method
