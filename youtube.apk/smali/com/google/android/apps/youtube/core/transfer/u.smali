.class final Lcom/google/android/apps/youtube/core/transfer/u;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

.field private final b:Landroid/net/ConnectivityManager;

.field private volatile c:Z

.field private volatile d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/u;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->b(Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/u;->b:Landroid/net/ConnectivityManager;

    return-void
.end method

.method private e()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/u;->b:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    move v3, v0

    :goto_0
    if-eqz v3, :cond_2

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iget-boolean v4, p0, Lcom/google/android/apps/youtube/core/transfer/u;->c:Z

    if-ne v4, v3, :cond_3

    iget-boolean v4, p0, Lcom/google/android/apps/youtube/core/transfer/u;->d:Z

    if-ne v4, v0, :cond_3

    :goto_2
    return v2

    :cond_0
    move v3, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iput-boolean v3, p0, Lcom/google/android/apps/youtube/core/transfer/u;->c:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/u;->d:Z

    move v2, v1

    goto :goto_2
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/u;->c:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/u;->d:Z

    return v0
.end method

.method public final c()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/u;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->b(Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/u;->e()Z

    return-void
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/u;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->b(Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/u;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/u;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->c(Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;)V

    :cond_0
    return-void
.end method
