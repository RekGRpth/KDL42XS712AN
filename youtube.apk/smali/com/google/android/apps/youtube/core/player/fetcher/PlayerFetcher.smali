.class public final Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[B


# instance fields
.field private final b:Lcom/google/android/apps/youtube/common/c/a;

.field private final c:Lcom/google/android/apps/youtube/datalib/innertube/ah;

.field private final d:Lcom/google/android/apps/youtube/core/client/bc;

.field private final e:Lcom/google/android/apps/youtube/core/player/w;

.field private final f:Lcom/google/android/apps/youtube/core/identity/l;

.field private final g:Lcom/google/android/apps/youtube/core/offline/store/q;

.field private final h:Ljava/util/concurrent/Executor;

.field private final i:Ljava/util/concurrent/Executor;

.field private final j:Lcom/google/android/apps/youtube/datalib/innertube/ag;

.field private final k:Lcom/google/android/apps/youtube/common/e/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->a:[B

    sput-object v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a:[B

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->b:Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->c:Lcom/google/android/apps/youtube/datalib/innertube/ah;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->e:Lcom/google/android/apps/youtube/core/player/w;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->f:Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->g:Lcom/google/android/apps/youtube/core/offline/store/q;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->d:Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->h:Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->i:Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->j:Lcom/google/android/apps/youtube/datalib/innertube/ag;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->k:Lcom/google/android/apps/youtube/common/e/b;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/datalib/innertube/ah;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/datalib/innertube/ag;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->b:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/ah;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->c:Lcom/google/android/apps/youtube/datalib/innertube/ah;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/w;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->e:Lcom/google/android/apps/youtube/core/player/w;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->d:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->f:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/q;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->g:Lcom/google/android/apps/youtube/core/offline/store/q;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->h:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/apps/youtube/common/b/b;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/common/b/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->i:Ljava/util/concurrent/Executor;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/ag;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->j:Lcom/google/android/apps/youtube/datalib/innertube/ag;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->k:Lcom/google/android/apps/youtube/common/e/b;

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;
    .locals 3

    if-nez p2, :cond_1

    const/4 v0, 0x0

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->f:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->g:Lcom/google/android/apps/youtube/core/offline/store/q;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->f:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/core/offline/store/q;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    :goto_1
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getExpirationInElapsedTimeMillis()J

    move-result-wide v1

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->b(Ljava/lang/String;J)Lcom/google/android/apps/youtube/datalib/legacy/model/u;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->e()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->c()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->d()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->cloneAndMergeOfflineStreams(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    :cond_0
    :goto_2
    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-direct {v0, p2, p3}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    return-object v0

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoStreamingData()Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->g:Lcom/google/android/apps/youtube/core/offline/store/q;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/q;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;
    :try_end_1
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->i:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/fetcher/b;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/core/player/fetcher/b;-><init>(Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/common/a/b;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->i:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/fetcher/c;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/core/player/fetcher/c;-><init>(Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/common/a/b;Ljava/lang/Exception;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;II)Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;
    .locals 8

    sget-object v7, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->BOTH:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;)Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;)Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;
    .locals 7

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->b:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/m;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/player/event/m;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    invoke-static {}, Lcom/google/android/apps/youtube/common/a/c;->a()Lcom/google/android/apps/youtube/common/a/c;

    move-result-object v5

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/a/k;->a()Lcom/google/android/apps/youtube/datalib/a/k;

    move-result-object v0

    invoke-virtual {p7}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->shouldRequestGData()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->d:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v1, p1, v5}, Lcom/google/android/apps/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_0
    invoke-virtual {p7}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->shouldRequestPlayerService()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->c:Lcom/google/android/apps/youtube/datalib/innertube/ah;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/ah;->a()Lcom/google/android/apps/youtube/datalib/innertube/ak;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->a([B)V

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    invoke-virtual {v1, p4}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    invoke-virtual {v1, p5}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->a(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    invoke-virtual {v1, p6}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->i(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    invoke-virtual {v1, p3}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ak;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->e:Lcom/google/android/apps/youtube/core/player/w;

    invoke-virtual {v6, v1}, Lcom/google/android/apps/youtube/core/player/w;->a(Lcom/google/android/apps/youtube/datalib/innertube/ak;)V

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->j:Lcom/google/android/apps/youtube/datalib/innertube/ag;

    invoke-interface {v6, v1}, Lcom/google/android/apps/youtube/datalib/innertube/ag;->a(Lcom/google/android/apps/youtube/datalib/innertube/ak;)V

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->c:Lcom/google/android/apps/youtube/datalib/innertube/ah;

    invoke-virtual {v6, v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ah;->a(Lcom/google/android/apps/youtube/datalib/innertube/ak;Lcom/google/android/apps/youtube/datalib/a/l;)V

    :cond_1
    invoke-virtual {p7}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->shouldRequestPlayerService()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/a/k;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-object v1, v0

    :goto_0
    invoke-virtual {p7}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->shouldRequestGData()Z

    move-result v0

    if-eqz v0, :cond_9

    :try_start_0
    invoke-virtual {v5}, Lcom/google/android/apps/youtube/common/a/c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->b:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/event/l;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/player/event/l;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    return-object v0

    :cond_2
    move-object v1, v2

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v2, v0

    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/youtube/core/async/GDataResponseException;

    if-eqz v0, :cond_4

    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataResponseException;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/async/GDataResponseException;->getStatusCode()I

    move-result v0

    const/16 v5, 0x193

    if-ne v0, v5, :cond_4

    move v0, v3

    :goto_2
    if-eqz v0, :cond_6

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->id(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getCaptionTracksUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->captionTracksUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->monetize(Z)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getLengthSeconds()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->duration(I)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->PRIVATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->privacy(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->isLive()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoStreamingData()Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoStreamingData()Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getHlsStream()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoStreamingData()Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getHlsStream()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->liveEventUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PROCESSING:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->state(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    :cond_3
    :goto_3
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->a()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_4
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->build()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v0

    goto/16 :goto_1

    :cond_4
    move v0, v4

    goto :goto_2

    :cond_5
    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->INVALID_FORMAT:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->state(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    goto :goto_3

    :pswitch_0
    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->state(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    goto :goto_4

    :pswitch_1
    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->CANT_PROCESS:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->state(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    goto :goto_4

    :pswitch_2
    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->NOT_AVAILABLE_ON_MOBILE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->state(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    goto :goto_4

    :pswitch_3
    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->INAPPROPRIATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->state(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    goto :goto_4

    :pswitch_4
    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PRIVATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->state(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    goto :goto_4

    :pswitch_5
    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->INAPPROPRIATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->state(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->adultContent(Z)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    goto :goto_4

    :cond_6
    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_7

    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v0

    const/16 v5, 0x194

    if-ne v0, v5, :cond_7

    move v0, v3

    :goto_5
    if-eqz v0, :cond_8

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->id(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getCaptionTracksUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->captionTracksUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->monetize(Z)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getLengthSeconds()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->duration(I)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->DELETED:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->state(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->build()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v0

    goto/16 :goto_1

    :cond_7
    move v0, v4

    goto :goto_5

    :cond_8
    throw v2

    :cond_9
    move-object v0, v2

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/common/a/b;)V
    .locals 10

    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->h:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/fetcher/a;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/core/player/fetcher/a;-><init>(Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v9, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
