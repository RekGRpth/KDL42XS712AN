.class final Lcom/google/android/apps/youtube/core/identity/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/identity/f;

.field final synthetic b:Lcom/google/android/apps/youtube/core/identity/aa;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/identity/f;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/identity/ac;->b:Lcom/google/android/apps/youtube/core/identity/aa;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/identity/ac;->a:Lcom/google/android/apps/youtube/core/identity/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/ac;->b:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v0, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->username:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->channelId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/ac;->b:Lcom/google/android/apps/youtube/core/identity/aa;

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Username or channel id is empty."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/ac;->b:Lcom/google/android/apps/youtube/core/identity/aa;

    iget-object v1, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->username:Ljava/lang/String;

    iget-object v2, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->channelId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Lcom/google/android/apps/youtube/core/identity/aa;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/ac;->b:Lcom/google/android/apps/youtube/core/identity/aa;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/ac;->a:Lcom/google/android/apps/youtube/core/identity/f;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/ac;->a:Lcom/google/android/apps/youtube/core/identity/f;

    invoke-static {v2, p2}, Lcom/google/android/apps/youtube/core/identity/z;->a(Lcom/google/android/apps/youtube/core/identity/f;Lcom/google/android/apps/youtube/core/identity/UserProfile;)Lcom/google/android/apps/youtube/core/identity/z;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Lcom/google/android/apps/youtube/core/identity/f;Lcom/google/android/apps/youtube/core/identity/z;)V

    goto :goto_0
.end method
