.class public final Lcom/google/android/apps/youtube/core/client/bf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/common/e/b;

.field private final b:Ljava/util/concurrent/Executor;

.field private final c:Landroid/os/Handler;

.field private final d:Lcom/google/android/apps/youtube/datalib/innertube/m;

.field private e:Lcom/google/android/apps/youtube/core/client/bi;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/common/e/b;Ljava/util/concurrent/Executor;Landroid/os/Handler;Lcom/google/android/apps/youtube/datalib/innertube/m;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bf;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bf;->b:Ljava/util/concurrent/Executor;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bf;->c:Landroid/os/Handler;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/m;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bf;->d:Lcom/google/android/apps/youtube/datalib/innertube/m;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/a/a/a/a/gr;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/HeartbeatClient;
    .locals 12

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bf;->e:Lcom/google/android/apps/youtube/core/client/bi;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/a/a/a/a/gr;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v0, p1, Lcom/google/a/a/a/a/gr;->c:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p1, Lcom/google/a/a/a/a/gr;->d:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/bf;->a:Lcom/google/android/apps/youtube/common/e/b;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/bf;->b:Ljava/util/concurrent/Executor;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/bf;->c:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/client/bf;->d:Lcom/google/android/apps/youtube/datalib/innertube/m;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/client/bf;->e:Lcom/google/android/apps/youtube/core/client/bi;

    iget-object v6, p1, Lcom/google/a/a/a/a/gr;->b:Ljava/lang/String;

    iget-wide v7, p1, Lcom/google/a/a/a/a/gr;->c:J

    iget-wide v9, p1, Lcom/google/a/a/a/a/gr;->d:J

    move-object v11, p2

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;-><init>(Lcom/google/android/apps/youtube/common/e/b;Ljava/util/concurrent/Executor;Landroid/os/Handler;Lcom/google/android/apps/youtube/datalib/innertube/m;Lcom/google/android/apps/youtube/core/client/bi;Ljava/lang/String;JJLjava/lang/String;)V

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;)Lcom/google/android/apps/youtube/core/client/HeartbeatClient;
    .locals 12

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bf;->e:Lcom/google/android/apps/youtube/core/client/bi;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/bf;->a:Lcom/google/android/apps/youtube/common/e/b;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/bf;->b:Ljava/util/concurrent/Executor;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/bf;->c:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/client/bf;->d:Lcom/google/android/apps/youtube/datalib/innertube/m;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/client/bf;->e:Lcom/google/android/apps/youtube/core/client/bi;

    # getter for: Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->heartbeatToken:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->access$200(Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;)Ljava/lang/String;

    move-result-object v6

    # getter for: Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->intervalMsec:J
    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->access$300(Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;)J

    move-result-wide v7

    # getter for: Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->maxRetries:J
    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->access$400(Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;)J

    move-result-wide v9

    # getter for: Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->videoId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->access$500(Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;)Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;-><init>(Lcom/google/android/apps/youtube/common/e/b;Ljava/util/concurrent/Executor;Landroid/os/Handler;Lcom/google/android/apps/youtube/datalib/innertube/m;Lcom/google/android/apps/youtube/core/client/bi;Ljava/lang/String;JJLjava/lang/String;)V

    # getter for: Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->nextHeartbeatTimestamp:J
    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->access$700(Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;)J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->a(Lcom/google/android/apps/youtube/core/client/HeartbeatClient;J)J

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/client/bi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/bf;->e:Lcom/google/android/apps/youtube/core/client/bi;

    return-void
.end method
