.class final Lcom/google/android/apps/youtube/core/player/notification/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/notification/h;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    check-cast p1, Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to get bitmap for playback controllers with URI: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/h;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/h;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-static {v1, p2}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->b(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;Landroid/graphics/Bitmap;)V

    return-void
.end method
