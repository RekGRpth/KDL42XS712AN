.class public final Lcom/google/android/apps/youtube/core/client/cc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/common/e/b;

.field private final b:Lcom/google/android/apps/youtube/datalib/e/b;

.field private final c:Lcom/google/android/apps/youtube/common/network/h;

.field private final d:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

.field private final e:Lcom/google/android/apps/youtube/medialib/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Lcom/google/android/apps/youtube/medialib/a/a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/cc;->b:Lcom/google/android/apps/youtube/datalib/e/b;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/client/cc;->a:Lcom/google/android/apps/youtube/common/e/b;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/client/cc;->c:Lcom/google/android/apps/youtube/common/network/h;

    iput-object p4, p0, Lcom/google/android/apps/youtube/core/client/cc;->d:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    iput-object p5, p0, Lcom/google/android/apps/youtube/core/client/cc;->e:Lcom/google/android/apps/youtube/medialib/a/a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;)Lcom/google/android/apps/youtube/core/client/QoeStatsClient;
    .locals 13

    new-instance v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/cc;->a:Lcom/google/android/apps/youtube/common/e/b;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/cc;->b:Lcom/google/android/apps/youtube/datalib/e/b;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/cc;->c:Lcom/google/android/apps/youtube/common/network/h;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/client/cc;->d:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/client/cc;->e:Lcom/google/android/apps/youtube/medialib/a/a;

    iget-object v6, p1, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;->baseQoeUri:Landroid/net/Uri;

    iget-object v7, p1, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;->cpn:Ljava/lang/String;

    iget-object v8, p1, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;->videoId:Ljava/lang/String;

    iget-object v9, p1, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;->liveState:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    iget-wide v10, p1, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;->startPlaybackTime:J

    iget-boolean v12, p1, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;->wasEnded:Z

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;-><init>(Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Lcom/google/android/apps/youtube/medialib/a/a;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;JZ)V

    iget v1, p1, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;->videoItag:I

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->a(Lcom/google/android/apps/youtube/core/client/QoeStatsClient;I)I

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->g()V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/youtube/core/client/QoeStatsClient;
    .locals 13

    new-instance v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/cc;->a:Lcom/google/android/apps/youtube/common/e/b;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/cc;->b:Lcom/google/android/apps/youtube/datalib/e/b;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/cc;->c:Lcom/google/android/apps/youtube/common/network/h;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/client/cc;->d:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/client/cc;->e:Lcom/google/android/apps/youtube/medialib/a/a;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->getBaseUri()Landroid/net/Uri;

    move-result-object v6

    if-eqz p4, :cond_0

    sget-object v9, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->LIVE:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    :goto_0
    const-wide/16 v10, -0x1

    const/4 v12, 0x0

    move-object v7, p2

    move-object/from16 v8, p3

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;-><init>(Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Lcom/google/android/apps/youtube/medialib/a/a;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;JZ)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/QoeStatsClient;->g()V

    return-object v0

    :cond_0
    sget-object v9, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;->VOD:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$LiveState;

    goto :goto_0
.end method
