.class public final Lcom/google/android/apps/youtube/core/client/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/datalib/e/b;

.field private final b:Lcom/google/android/apps/youtube/datalib/e/b;

.field private final c:Lcom/google/android/apps/youtube/common/e/b;

.field private final d:Lcom/google/android/apps/youtube/common/c/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/common/c/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/d;->a:Lcom/google/android/apps/youtube/datalib/e/b;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/d;->b:Lcom/google/android/apps/youtube/datalib/e/b;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/d;->c:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/d;->d:Lcom/google/android/apps/youtube/common/c/a;

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)Lcom/google/android/apps/youtube/core/client/j;
    .locals 5

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getTrackingDecoration()Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getTrackingDecoration()Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->getUrlMatchPattern()Ljava/util/regex/Pattern;

    move-result-object v0

    :goto_0
    new-instance v1, Lcom/google/android/apps/youtube/core/client/j;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/d;->a:Lcom/google/android/apps/youtube/datalib/e/b;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/d;->b:Lcom/google/android/apps/youtube/datalib/e/b;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/client/d;->c:Lcom/google/android/apps/youtube/common/e/b;

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/google/android/apps/youtube/core/client/j;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/datalib/e/b;Ljava/util/regex/Pattern;Lcom/google/android/apps/youtube/common/e/b;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/common/e/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/d;->c:Lcom/google/android/apps/youtube/common/e/b;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/AdStatsClient;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/core/client/bk;

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/d;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)Lcom/google/android/apps/youtube/core/client/j;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/apps/youtube/core/client/bk;-><init>(Lcom/google/android/apps/youtube/core/client/j;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/d;->d:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/AdStatsClient;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/core/client/bm;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/client/d;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)Lcom/google/android/apps/youtube/core/client/j;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/google/android/apps/youtube/core/client/bm;-><init>(Lcom/google/android/apps/youtube/core/client/j;Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/d;->d:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;)Lcom/google/android/apps/youtube/core/client/AdStatsClient;
    .locals 11

    const/4 v1, 0x0

    if-nez p4, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p4, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->kind:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    sget-object v2, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->INSTREAM:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    if-ne v0, v2, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/core/client/bm;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/client/d;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)Lcom/google/android/apps/youtube/core/client/j;

    move-result-object v1

    iget v5, p4, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->nextQuartile:I

    iget-boolean v6, p4, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->engagedViewPinged:Z

    iget-boolean v7, p4, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->impressionPinged:Z

    iget-boolean v8, p4, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->skipAdShownPinged:Z

    iget-object v9, p4, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->pingedCustomConversionTypes:Ljava/util/List;

    iget v10, p4, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->lastProgressEventMillis:I

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/youtube/core/client/bm;-><init>(Lcom/google/android/apps/youtube/core/client/j;Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;IZZZLjava/util/List;I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/d;->d:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v0, p4, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->kind:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    sget-object v2, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->TRUEVIEW_INDISPLAY:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    if-ne v0, v2, :cond_2

    new-instance v0, Lcom/google/android/apps/youtube/core/client/bk;

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/d;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)Lcom/google/android/apps/youtube/core/client/j;

    move-result-object v1

    iget v4, p4, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->nextQuartile:I

    iget-boolean v5, p4, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->impressionPinged:Z

    iget v6, p4, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;->lastProgressEventMillis:I

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/client/bk;-><init>(Lcom/google/android/apps/youtube/core/client/j;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;IZI)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/d;->d:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/AdStatsClient;
    .locals 3

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->EMPTY_AD:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    const-wide v1, 0x7fffffffffffffffL

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/apps/youtube/core/client/d;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/AdStatsClient;

    move-result-object v0

    return-object v0
.end method
