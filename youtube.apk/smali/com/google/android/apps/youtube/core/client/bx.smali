.class public final Lcom/google/android/apps/youtube/core/client/bx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/datalib/e/b;

.field private final b:Lcom/google/android/apps/youtube/common/network/h;

.field private final c:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

.field private final d:Lcom/google/android/apps/youtube/core/utils/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Lcom/google/android/apps/youtube/core/utils/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bx;->a:Lcom/google/android/apps/youtube/datalib/e/b;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bx;->b:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bx;->c:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/utils/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bx;->d:Lcom/google/android/apps/youtube/core/utils/a;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;)Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;
    .locals 2

    # getter for: Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;->trackingUrls:[Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;
    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;->access$000(Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;)[Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    # getter for: Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;->cpn:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;->access$100(Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/client/bx;->a(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;
    .locals 7

    new-instance v0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/bx;->a:Lcom/google/android/apps/youtube/datalib/e/b;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/bx;->b:Lcom/google/android/apps/youtube/common/network/h;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/bx;->c:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/client/bx;->d:Lcom/google/android/apps/youtube/core/utils/a;

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Lcom/google/android/apps/youtube/core/utils/a;Ljava/util/List;Ljava/lang/String;)V

    return-object v0
.end method
