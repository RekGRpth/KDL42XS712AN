.class public final Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final adCpn:Ljava/lang/String;

.field private final adState:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;

.field private final heartbeatState:Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;

.field private final playbackOffsetState:Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;

.field private final qoeState:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;

.field private final trueViewInDisplayAd:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

.field private final videoCpn:Ljava/lang/String;

.field private final videoId:Ljava/lang/String;

.field private final vss2State:Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/ac;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/player/ac;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->adCpn:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->videoCpn:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->adState:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->heartbeatState:Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->playbackOffsetState:Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->qoeState:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->vss2State:Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->trueViewInDisplayAd:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->adCpn:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->videoCpn:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->videoId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->adState:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;

    iput-object p5, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->heartbeatState:Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;

    iput-object p6, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->playbackOffsetState:Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;

    iput-object p7, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->qoeState:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    iput-object p8, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->vss2State:Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    iput-object p9, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->trueViewInDisplayAd:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->adCpn:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->videoCpn:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->videoId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->trueViewInDisplayAd:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->adState:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->heartbeatState:Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->playbackOffsetState:Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->qoeState:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;)Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->vss2State:Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "PlaybackClientManagerState { adCpn=%s videoCpn=%s videoId=%s }"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->adCpn:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->videoCpn:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->videoId:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->adCpn:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->videoCpn:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->adState:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->heartbeatState:Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->playbackOffsetState:Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->qoeState:Lcom/google/android/apps/youtube/core/client/QoeStatsClient$QoeStatsClientState;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->vss2State:Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager$PlaybackClientManagerState;->trueViewInDisplayAd:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
