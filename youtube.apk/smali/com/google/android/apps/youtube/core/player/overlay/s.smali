.class final Lcom/google/android/apps/youtube/core/player/overlay/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/p;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/overlay/q;

.field private b:Z


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/q;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/q;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/s;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/q;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->e(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->PLAYER_CONTROL:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/s;->a(Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->a(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->i()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->a(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/ae;->c(I)V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->PLAYER_CONTROL:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/s;->a(Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->a(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->i()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    return-void
.end method

.method public final a_(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->c(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/overlay/t;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->c(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/overlay/t;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/t;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->a(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    return-void
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->a(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/ae;->d(Z)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->a(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->l()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    return-void
.end method

.method public final d()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->NAVIGATION:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/s;->a(Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->a(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->s()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    return-void
.end method

.method public final e()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->NAVIGATION:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/s;->a(Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->a(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->r()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    return-void
.end method

.method public final f()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->PLAYER_CONTROL:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/s;->a(Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->a(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->D()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->c(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/overlay/t;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->c(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/overlay/t;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/t;->a()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->d(Lcom/google/android/apps/youtube/core/player/overlay/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->a(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->y()V

    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->a(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->z()V

    goto :goto_0
.end method

.method public final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->e(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/b;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/player/event/b;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    return-void
.end method

.method public final j()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->e(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/aa;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/core/player/event/aa;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    return-void
.end method

.method public final k()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->e(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/aa;

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/core/player/event/aa;-><init>(Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    return-void
.end method

.method public final l()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->PLAYER_CONTROL:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/s;->a(Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->a(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->m()Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    return-void
.end method

.method public final m()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->b(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->d()V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->PLAYER_CONTROL:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/s;->a(Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->a:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;->a(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->j()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    return-void
.end method

.method public final n()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/s;->b:Z

    return-void
.end method
