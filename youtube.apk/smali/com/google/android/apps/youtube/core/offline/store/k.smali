.class final Lcom/google/android/apps/youtube/core/offline/store/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/offline/store/i;

.field private final b:Landroid/database/Cursor;

.field private final c:Lcom/google/android/apps/youtube/core/offline/store/ad;

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/offline/store/i;Landroid/database/Cursor;Lcom/google/android/apps/youtube/core/offline/store/l;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->a:Lcom/google/android/apps/youtube/core/offline/store/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->b:Landroid/database/Cursor;

    new-instance v0, Lcom/google/android/apps/youtube/core/offline/store/ad;

    invoke-direct {v0, p2, p3}, Lcom/google/android/apps/youtube/core/offline/store/ad;-><init>(Landroid/database/Cursor;Lcom/google/android/apps/youtube/core/offline/store/l;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->c:Lcom/google/android/apps/youtube/core/offline/store/ad;

    const-string v0, "player_response_proto"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->d:I

    const-string v0, "saved_timestamp"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->e:I

    const-string v0, "last_refresh_timestamp"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->f:I

    const-string v0, "last_playback_timestamp"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->g:I

    const-string v0, "media_status"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->h:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/offline/store/i;Landroid/database/Cursor;Lcom/google/android/apps/youtube/core/offline/store/l;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/offline/store/k;-><init>(Lcom/google/android/apps/youtube/core/offline/store/i;Landroid/database/Cursor;Lcom/google/android/apps/youtube/core/offline/store/l;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/offline/store/k;Lcom/google/android/apps/youtube/core/offline/store/x;)V
    .locals 7

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->c:Lcom/google/android/apps/youtube/core/offline/store/ad;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/ad;->a()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v6

    invoke-virtual {p1, v6}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->b:Landroid/database/Cursor;

    iget v1, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->d:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->b:Landroid/database/Cursor;

    iget v1, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->d:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->fromBlob([BJ)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, v6, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/z;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->b:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->e:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->b:Landroid/database/Cursor;

    iget v5, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->f:I

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/offline/store/z;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;JJ)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->b:Landroid/database/Cursor;

    iget v2, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->g:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->b:Landroid/database/Cursor;

    iget v2, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->g:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/offline/store/z;->a(J)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->b:Landroid/database/Cursor;

    iget v2, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->h:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->b:Landroid/database/Cursor;

    iget v2, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->h:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->fromValue(I)Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/z;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/k;->a:Lcom/google/android/apps/youtube/core/offline/store/i;

    iget-object v2, v6, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/t;)Lcom/google/android/apps/youtube/datalib/legacy/model/u;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->f()J

    move-result-wide v2

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->g()J

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/apps/youtube/core/offline/store/z;->a(JJ)V

    goto/16 :goto_0

    :cond_2
    return-void
.end method
