.class public final Lcom/google/android/apps/youtube/core/client/VideoStats2Client;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:Lcom/google/android/apps/youtube/core/player/al;

.field private B:J

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Z

.field private volatile H:Z

.field private I:I

.field private J:Ljava/lang/String;

.field private final a:Lcom/google/android/apps/youtube/common/e/b;

.field private final b:Lcom/google/android/apps/youtube/core/Analytics;

.field private final c:Landroid/net/Uri;

.field private final d:Landroid/net/Uri;

.field private final e:Landroid/net/Uri;

.field private final f:J

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:I

.field private final m:Ljava/util/LinkedList;

.field private final n:Ljava/util/LinkedList;

.field private final o:Ljava/util/LinkedList;

.field private final p:Ljava/util/LinkedList;

.field private final q:Ljava/util/LinkedList;

.field private final r:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field private final s:Ljava/util/List;

.field private final t:Z

.field private final u:Z

.field private final v:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

.field private final w:Lcom/google/android/apps/youtube/core/client/cf;

.field private final x:Lcom/google/android/apps/youtube/common/network/h;

.field private final y:Lcom/google/android/apps/youtube/common/c/a;

.field private final z:Lcom/google/android/apps/youtube/datalib/e/b;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/Analytics;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/google/android/apps/youtube/core/client/WatchFeature;Ljava/lang/String;Lcom/google/android/apps/youtube/core/player/al;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Ljava/util/List;Lcom/google/android/apps/youtube/core/client/cf;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/datalib/e/b;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->z:Lcom/google/android/apps/youtube/datalib/e/b;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a:Lcom/google/android/apps/youtube/common/e/b;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->b:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->c:Landroid/net/Uri;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->d:Landroid/net/Uri;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->e:Landroid/net/Uri;

    iput-object p7, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->i:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->h:Ljava/lang/String;

    iput p9, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->l:I

    iput-boolean p10, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->t:Z

    iput-boolean p11, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->u:Z

    iput-object p12, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->g:Ljava/lang/String;

    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->j:Ljava/lang/String;

    move-wide/from16 v0, p15

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->f:J

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->r:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->J:Ljava/lang/String;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->A:Lcom/google/android/apps/youtube/core/player/al;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->x:Lcom/google/android/apps/youtube/common/network/h;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->y:Lcom/google/android/apps/youtube/common/c/a;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->v:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->s:Ljava/util/List;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->k:Ljava/lang/String;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->m:Ljava/util/LinkedList;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->n:Ljava/util/LinkedList;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->o:Ljava/util/LinkedList;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->p:Ljava/util/LinkedList;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->q:Ljava/util/LinkedList;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->w:Lcom/google/android/apps/youtube/core/client/cf;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/Analytics;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/google/android/apps/youtube/core/client/WatchFeature;Ljava/lang/String;Lcom/google/android/apps/youtube/core/player/al;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Ljava/util/List;Lcom/google/android/apps/youtube/core/client/cf;B)V
    .locals 0

    invoke-direct/range {p0 .. p24}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/Analytics;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/google/android/apps/youtube/core/client/WatchFeature;Ljava/lang/String;Lcom/google/android/apps/youtube/core/player/al;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Ljava/util/List;Lcom/google/android/apps/youtube/core/client/cf;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;Lcom/google/android/apps/youtube/core/player/al;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Ljava/util/List;Lcom/google/android/apps/youtube/core/client/cf;)V
    .locals 27

    move-object/from16 v0, p4

    iget-object v6, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->basePlaybackUri:Landroid/net/Uri;

    move-object/from16 v0, p4

    iget-object v7, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->baseDelayplayUri:Landroid/net/Uri;

    move-object/from16 v0, p4

    iget-object v8, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->baseWatchtimeUri:Landroid/net/Uri;

    move-object/from16 v0, p4

    iget-object v9, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->videoId:Ljava/lang/String;

    move-object/from16 v0, p4

    iget-object v10, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->length:Ljava/lang/String;

    move-object/from16 v0, p4

    iget v11, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->delaySeconds:I

    move-object/from16 v0, p4

    iget-boolean v12, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->autoplay:Z

    move-object/from16 v0, p4

    iget-boolean v13, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->scriptedPlayback:Z

    move-object/from16 v0, p4

    iget-object v14, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->adformat:Ljava/lang/String;

    move-object/from16 v0, p4

    iget-object v15, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->cpn:Ljava/lang/String;

    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->playlistId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p4

    iget-wide v0, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->sessionStartTimestamp:J

    move-wide/from16 v17, v0

    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->feature:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-object/from16 v19, v0

    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->subtitleTrackId:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v21, p5

    move-object/from16 v22, p6

    move-object/from16 v23, p7

    move-object/from16 v24, p8

    move-object/from16 v25, p9

    move-object/from16 v26, p10

    invoke-direct/range {v2 .. v26}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/Analytics;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/google/android/apps/youtube/core/client/WatchFeature;Ljava/lang/String;Lcom/google/android/apps/youtube/core/player/al;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Ljava/util/List;Lcom/google/android/apps/youtube/core/client/cf;)V

    move-object/from16 v0, p4

    iget-wide v2, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->currentPlaybackPosition:J

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->B:J

    move-object/from16 v0, p4

    iget-boolean v2, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->delayedPingSent:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->C:Z

    move-object/from16 v0, p4

    iget-boolean v2, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->finalPingSent:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->D:Z

    move-object/from16 v0, p4

    iget-boolean v2, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->initialPingSent:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->E:Z

    move-object/from16 v0, p4

    iget-boolean v2, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->throttled:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->H:Z

    move-object/from16 v0, p4

    iget v2, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;->videoItag:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->I:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;Lcom/google/android/apps/youtube/core/player/al;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Ljava/util/List;Lcom/google/android/apps/youtube/core/client/cf;B)V
    .locals 0

    invoke-direct/range {p0 .. p10}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;Lcom/google/android/apps/youtube/core/player/al;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Ljava/util/List;Lcom/google/android/apps/youtube/core/client/cf;)V

    return-void
.end method

.method private static a(Ljava/util/LinkedList;)Ljava/lang/String;
    .locals 1

    const-string v0, ","

    invoke-static {v0, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/util/LinkedList;->clear()V

    return-object v0
.end method

.method private a(Landroid/net/Uri;)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->D:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Final ping for playback "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " has already been sent - Ignoring request"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->H:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Pinging "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->z:Lcom/google/android/apps/youtube/datalib/e/b;

    const-string v0, "vss"

    const v1, 0x323467f

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/e/b;->a(Ljava/lang/String;I)Lcom/google/android/apps/youtube/datalib/e/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/e/f;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/e/f;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->z:Lcom/google/android/apps/youtube/datalib/e/b;

    new-instance v2, Lcom/google/android/apps/youtube/core/client/cg;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/core/client/cg;-><init>(Lcom/google/android/apps/youtube/core/client/VideoStats2Client;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/e/b;->a(Lcom/google/android/apps/youtube/datalib/e/f;Lcom/android/volley/n;)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Playback "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is throttled - Ignoring request"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/youtube/common/e/o;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->b(Lcom/google/android/apps/youtube/common/e/o;)V

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->B:J

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->d(J)Ljava/lang/String;

    move-result-object v0

    const-string v1, "cmt"

    invoke-virtual {p1, v1, v0}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "conn"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->x:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/common/network/h;->i()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;I)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "vis"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->A:Lcom/google/android/apps/youtube/core/player/al;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/al;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->l:I

    if-lez v0, :cond_0

    const-string v0, "delay"

    iget v1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;I)Lcom/google/android/apps/youtube/common/e/o;

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->t:Z

    if-eqz v0, :cond_1

    const-string v0, "autoplay"

    const-string v1, "1"

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->u:Z

    if-eqz v0, :cond_2

    const-string v0, "splay"

    const-string v1, "1"

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->r:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    sget-object v1, Lcom/google/android/apps/youtube/core/client/WatchFeature;->NO_FEATURE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    if-eq v0, v1, :cond_3

    const-string v0, "feature"

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->r:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/client/WatchFeature;->getFeatureString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->J:Ljava/lang/String;

    const-string v1, "-"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "cc"

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->J:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/util/Pair;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/common/e/o;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/client/VideoStats2Client;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->y:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    return-void
.end method

.method private a(Z)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->e:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/o;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->b(Lcom/google/android/apps/youtube/common/e/o;)V

    const-string v2, "state"

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->F:Z

    if-eqz v0, :cond_2

    const-string v0, "playing"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->n:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "st"

    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->B:J

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->d(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v2, "et"

    iget-wide v3, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->B:J

    invoke-static {v3, v4}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->d(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v2, "conn"

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->x:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/common/network/h;->i()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;I)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v2, "vis"

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->A:Lcom/google/android/apps/youtube/core/player/al;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/player/al;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    const-string v0, "-"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->J:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "cc"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->J:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    :cond_0
    :goto_1
    if-eqz p1, :cond_1

    const-string v0, "final"

    const-string v2, "1"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/common/e/o;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a(Landroid/net/Uri;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->D:Z

    or-int/2addr v0, p1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->D:Z

    return-void

    :cond_2
    const-string v0, "paused"

    goto :goto_0

    :cond_3
    const-string v0, "st"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->m:Ljava/util/LinkedList;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a(Ljava/util/LinkedList;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ",:"

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v2, "et"

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->n:Ljava/util/LinkedList;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a(Ljava/util/LinkedList;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ",:"

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v2, "conn"

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->o:Ljava/util/LinkedList;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a(Ljava/util/LinkedList;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ",:"

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v2, "vis"

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->p:Ljava/util/LinkedList;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a(Ljava/util/LinkedList;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ",:"

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->q:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "-"

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_6

    const-string v0, "cc"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->q:Ljava/util/LinkedList;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a(Ljava/util/LinkedList;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ",:"

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->q:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/client/VideoStats2Client;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->H:Z

    return v0
.end method

.method private b(Lcom/google/android/apps/youtube/common/e/o;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->f:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->d(J)Ljava/lang/String;

    move-result-object v0

    const-string v1, "cpn"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->j:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v1

    const-string v2, "rt"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "ns"

    const-string v2, "yt"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "docid"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "ver"

    const-string v2, "2"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "len"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->v:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/client/DeviceClassification;->a(Lcom/google/android/apps/youtube/common/e/o;)Lcom/google/android/apps/youtube/common/e/o;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string v0, "el"

    const-string v1, "adunit"

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    const-string v0, "adformat"

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->w:Lcom/google/android/apps/youtube/core/client/cf;

    if-eqz v0, :cond_0

    const-string v0, "lact"

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->w:Lcom/google/android/apps/youtube/core/client/cf;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/client/cf;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->I:I

    if-lez v0, :cond_1

    const-string v0, "fmt"

    iget v1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->I:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;I)Lcom/google/android/apps/youtube/common/e/o;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "list"

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    :cond_2
    return-void

    :cond_3
    const-string v0, "el"

    const-string v1, "detailpage"

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    goto :goto_0
.end method

.method private static d(J)Ljava/lang/String;
    .locals 8

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%.1f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    long-to-double v4, p0

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->F:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->G:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->G:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->m:Ljava/util/LinkedList;

    iget-wide v1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->B:J

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->d(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->o:Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->x:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/network/h;->i()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->p:Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->A:Lcom/google/android/apps/youtube/core/player/al;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/al;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->q:Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->J:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private h()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->G:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->G:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->n:Ljava/util/LinkedList;

    iget-wide v1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->B:J

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->d(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private i()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->C:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->d:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/o;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a(Lcom/google/android/apps/youtube/common/e/o;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->F:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->C:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->l:I

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->i()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->h()V

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a(Z)V

    return-void
.end method

.method public final a(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->I:I

    return-void
.end method

.method public final a(J)V
    .locals 2

    iput-wide p1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->B:J

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->E:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->E:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->c:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/o;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a(Lcom/google/android/apps/youtube/common/e/o;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->C:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->l:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->l:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->i()V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->g()V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/al;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->h()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->A:Lcom/google/android/apps/youtube/core/player/al;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->g()V

    return-void
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->F:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->h()V

    return-void
.end method

.method public final b(J)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->F:Z

    iput-wide p1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->B:J

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->g()V

    return-void
.end method

.method public final c()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->F:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->h()V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->E:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a(Z)V

    :cond_0
    return-void
.end method

.method public final c(J)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->h()V

    iput-wide p1, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->B:J

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->g()V

    return-void
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->F:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->h()V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->E:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->a(Z)V

    :cond_0
    return-void
.end method

.method public final e()Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;
    .locals 24

    new-instance v1, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->c:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->d:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->e:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->f:J

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->B:J

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->l:I

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->t:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->u:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->C:Z

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->D:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->E:Z

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->H:Z

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->I:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->r:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->J:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-direct/range {v1 .. v23}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client$VideoStats2ClientState;-><init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZZZZZILcom/google/android/apps/youtube/core/client/WatchFeature;Ljava/lang/String;)V

    return-object v1
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->y:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->G:Z

    if-eqz v0, :cond_0

    const-string v0, "VSS2 client released unexpectedly"

    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->b:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "Vss2UnexpectedRelease"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->c()V

    :cond_0
    return-void
.end method

.method public final handleConnectivityChangedEvent(Lcom/google/android/apps/youtube/common/network/a;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->h()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->g()V

    return-void
.end method

.method public final handleSubtitleTrackChangedEvent(Lcom/google/android/apps/youtube/core/player/event/x;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->J:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/x;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/x;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->J:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->h()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/VideoStats2Client;->g()V

    :cond_0
    return-void
.end method
