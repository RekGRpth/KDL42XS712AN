.class public final Lcom/google/android/apps/youtube/core/identity/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Landroid/content/Intent;

.field private final c:Ljava/lang/Exception;

.field private final d:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/a;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/a;->b:Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/a;->c:Ljava/lang/Exception;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/identity/a;->d:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/content/Intent;Ljava/lang/Exception;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/identity/a;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/identity/a;->b:Landroid/content/Intent;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/identity/a;->c:Ljava/lang/Exception;

    iput-boolean p4, p0, Lcom/google/android/apps/youtube/core/identity/a;->d:Z

    return-void
.end method

.method static a(Landroid/content/Intent;)Lcom/google/android/apps/youtube/core/identity/a;
    .locals 4

    const/4 v3, 0x0

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/a;

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-direct {v1, v3, v0, v3, v2}, Lcom/google/android/apps/youtube/core/identity/a;-><init>(Ljava/lang/String;Landroid/content/Intent;Ljava/lang/Exception;Z)V

    return-object v1
.end method

.method static a(Ljava/lang/Exception;)Lcom/google/android/apps/youtube/core/identity/a;
    .locals 4

    const/4 v3, 0x0

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/a;

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    const/4 v2, 0x1

    invoke-direct {v1, v3, v3, v0, v2}, Lcom/google/android/apps/youtube/core/identity/a;-><init>(Ljava/lang/String;Landroid/content/Intent;Ljava/lang/Exception;Z)V

    return-object v1
.end method

.method protected static a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/identity/a;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/identity/a;

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/google/android/apps/youtube/core/identity/a;-><init>(Ljava/lang/String;Landroid/content/Intent;Ljava/lang/Exception;Z)V

    return-object v0
.end method

.method protected static b(Ljava/lang/Exception;)Lcom/google/android/apps/youtube/core/identity/a;
    .locals 4

    const/4 v3, 0x0

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/a;

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    const/4 v2, 0x0

    invoke-direct {v1, v3, v3, v0, v2}, Lcom/google/android/apps/youtube/core/identity/a;-><init>(Ljava/lang/String;Landroid/content/Intent;Ljava/lang/Exception;Z)V

    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/a;->b:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/identity/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call getAuthenticationToken on an unsuccessful fetch."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Landroid/util/Pair;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/identity/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call getAuthenticationHeaderInfo on an unsuccessful fetch."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v0, "Authorization"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bearer "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/content/Intent;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/identity/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call getRecoveryIntent() on a successful fetch."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/identity/a;->b()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call getRecoveryIntent() on an unrecoverable fetch."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/a;->b:Landroid/content/Intent;

    return-object v0
.end method

.method public final f()Ljava/lang/Exception;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/a;->c:Ljava/lang/Exception;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call getException() on a successful or recoverable fetch."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/a;->c:Ljava/lang/Exception;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/identity/a;->d:Z

    return v0
.end method
