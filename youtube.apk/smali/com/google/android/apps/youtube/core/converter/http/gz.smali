.class public abstract Lcom/google/android/apps/youtube/core/converter/http/gz;
.super Lcom/google/android/apps/youtube/core/converter/http/ay;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/apps/youtube/core/converter/n;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/converter/n;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/http/ay;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/converter/n;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/gz;->a:Lcom/google/android/apps/youtube/core/converter/n;

    return-void
.end method


# virtual methods
.method protected abstract a()Lcom/google/android/apps/youtube/core/converter/e;
.end method

.method protected final a(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/gz;->a:Lcom/google/android/apps/youtube/core/converter/n;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/converter/http/gz;->a()Lcom/google/android/apps/youtube/core/converter/e;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/core/converter/n;->a(Ljava/io/InputStream;Lcom/google/android/apps/youtube/core/converter/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/r;

    :try_start_0
    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/r;->build()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/ConverterException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
