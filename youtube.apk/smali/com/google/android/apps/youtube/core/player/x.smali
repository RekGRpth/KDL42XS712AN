.class public final Lcom/google/android/apps/youtube/core/player/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/ab;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/w;

.field private final b:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

.field private final c:Lcom/google/android/apps/youtube/core/player/y;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/datalib/innertube/model/w;Lcom/google/android/apps/youtube/core/player/y;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/x;->a:Lcom/google/android/apps/youtube/core/player/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/x;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/x;->c:Lcom/google/android/apps/youtube/core/player/y;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/x;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/x;->a:Lcom/google/android/apps/youtube/core/player/w;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/x;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/x;->c:Lcom/google/android/apps/youtube/core/player/y;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/w;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/w;Lcom/google/android/apps/youtube/core/player/y;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/x;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/x;->a:Lcom/google/android/apps/youtube/core/player/w;

    iput-boolean v1, v0, Lcom/google/android/apps/youtube/core/player/w;->a:Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/x;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/x;->a:Lcom/google/android/apps/youtube/core/player/w;

    iput-boolean v1, v0, Lcom/google/android/apps/youtube/core/player/w;->b:Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/x;->a:Lcom/google/android/apps/youtube/core/player/w;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/x;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/w;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/w;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/x;->c:Lcom/google/android/apps/youtube/core/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/y;->b()V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/x;->c:Lcom/google/android/apps/youtube/core/player/y;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/x;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/w;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/w;)Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/y;->a(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    return-void
.end method
