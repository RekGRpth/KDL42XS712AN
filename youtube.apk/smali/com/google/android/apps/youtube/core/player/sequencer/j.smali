.class public final Lcom/google/android/apps/youtube/core/player/sequencer/j;
.super Lcom/google/android/apps/youtube/core/player/sequencer/c;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field private final l:Lcom/google/android/apps/youtube/core/player/fetcher/e;

.field private final m:Lcom/google/android/apps/youtube/datalib/offline/n;

.field private final n:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

.field private final o:Ljava/util/concurrent/Executor;

.field private final p:[B

.field private final q:Ljava/util/concurrent/Executor;

.field private final r:Ljava/lang/String;

.field private final s:Ljava/lang/String;

.field private volatile t:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

.field private volatile u:Ljava/util/List;

.field private volatile v:[I

.field private volatile w:I

.field private x:I

.field private y:Lcom/google/android/apps/youtube/core/player/sequencer/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/datalib/offline/n;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V
    .locals 13

    new-instance v8, Lcom/google/android/apps/youtube/common/b/b;

    invoke-direct {v8}, Lcom/google/android/apps/youtube/common/b/b;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/youtube/core/player/sequencer/j;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/datalib/offline/n;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->NEW:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/datalib/offline/n;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;)V
    .locals 7

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/youtube/core/player/sequencer/c;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;)V

    invoke-static/range {p11 .. p11}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a:Landroid/content/Context;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/player/fetcher/e;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->l:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    invoke-static/range {p9 .. p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/datalib/offline/n;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->m:Lcom/google/android/apps/youtube/datalib/offline/n;

    invoke-static/range {p10 .. p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->n:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->o:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p11

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->clickTrackingParams:[B

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->p:[B

    new-instance v1, Lcom/google/android/apps/youtube/common/b/b;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/common/b/b;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->q:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p11

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->videoId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->r:Ljava/lang/String;

    move-object/from16 v0, p11

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->playlistId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->s:Ljava/lang/String;

    move-object/from16 v0, p11

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->currentPlaybackPair:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-object/from16 v0, p11

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->currentWatchNextResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    move-object/from16 v0, p11

    iget v1, v0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->index:I

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->w:I

    move-object/from16 v0, p11

    iget v1, v0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->pendingIndex:I

    iput v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->x:I

    move-object/from16 v0, p11

    iget-boolean v1, v0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;->loop:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->j:Z

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->NEW:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->i()V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/datalib/offline/n;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V
    .locals 6

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/sequencer/c;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a:Landroid/content/Context;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/fetcher/e;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->l:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    invoke-static/range {p10 .. p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/offline/n;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->m:Lcom/google/android/apps/youtube/datalib/offline/n;

    invoke-static/range {p11 .. p11}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->n:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->o:Ljava/util/concurrent/Executor;

    invoke-virtual/range {p12 .. p12}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getClickTrackingParams()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->p:[B

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->q:Ljava/util/concurrent/Executor;

    invoke-virtual/range {p12 .. p12}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->r:Ljava/lang/String;

    invoke-virtual/range {p12 .. p12}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->s:Ljava/lang/String;

    :goto_0
    invoke-virtual/range {p12 .. p12}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistIndex()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->x:I

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->x:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->w:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->i()V

    return-void

    :cond_0
    invoke-virtual/range {p12 .. p12}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->r:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->s:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/sequencer/j;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->w:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/sequencer/j;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->t:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/sequencer/j;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->u:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/sequencer/j;)[I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->v:[I

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/sequencer/j;[I)[I
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->v:[I

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->u:Ljava/util/List;

    return-object v0
.end method

.method private declared-synchronized b(I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_LOADING:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->u:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    const/4 v1, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->x:I

    new-instance v0, Lcom/google/android/apps/youtube/core/player/sequencer/o;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/youtube/core/player/sequencer/o;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/j;I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->y:Lcom/google/android/apps/youtube/core/player/sequencer/k;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->o:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->y:Lcom/google/android/apps/youtube/core/player/sequencer/k;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/player/sequencer/j;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->v()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->q:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->n:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Lcom/google/android/apps/youtube/datalib/offline/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->m:Lcom/google/android/apps/youtube/datalib/offline/n;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->x()Z

    move-result v0

    return v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->s:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/core/player/sequencer/j;)[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->p:[B

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Lcom/google/android/apps/youtube/core/player/fetcher/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->l:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/apps/youtube/core/player/sequencer/j;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->t:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    return-object v0
.end method

.method private u()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->j:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->b(I)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->w:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->b(I)V

    goto :goto_0
.end method

.method private v()V
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->v:[I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->w:I

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->k:Z

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->v:[I

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->w:I

    aget v0, v0, v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->v:[I

    array-length v3, v1

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->v:[I

    aput v1, v4, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->v:[I

    aput v0, v1, v2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->v:[I

    aput v2, v1, v0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->v:[I

    const/4 v1, 0x1

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/youtube/core/utils/Util;->a([III)V

    :cond_2
    return-void
.end method

.method private w()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->u:Ljava/util/List;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->w:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private x()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->y:Lcom/google/android/apps/youtube/core/player/sequencer/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->y:Lcom/google/android/apps/youtube/core/player/sequencer/k;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/k;->a(Lcom/google/android/apps/youtube/core/player/sequencer/k;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->y:Lcom/google/android/apps/youtube/core/player/sequencer/k;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->NEW:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method final a(I)V
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/core/player/sequencer/k;

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->x:I

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/k;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/j;I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->y:Lcom/google/android/apps/youtube/core/player/sequencer/k;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->o:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->y:Lcom/google/android/apps/youtube/core/player/sequencer/k;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method protected final a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->h()V

    return-void
.end method

.method public final a(Z)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->v()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->w:I

    :goto_0
    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->k:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->h()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->v:[I

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->w:I

    aget v0, v0, v1

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->w:I

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->j:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->h()V

    return-void
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->w:I

    return v0
.end method

.method public final l()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a()V

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->l()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->b:Lcom/google/android/apps/youtube/core/player/sequencer/r;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/r;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->x:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->b(I)V

    goto :goto_0
.end method

.method public final m()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a()V

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->m()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->u()V

    return-void
.end method

.method public final n()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a()V

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->n()V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->j:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->w:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->u:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->b(I)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->w:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->b(I)V

    goto :goto_0
.end method

.method public final o()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a()V

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->o()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->u()V

    return-void
.end method

.method public final p()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->a()V

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->p()V

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->x:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->b(I)V

    return-void
.end method

.method public final p_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final q_()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->u:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->j:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->w()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final r()Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerState;
    .locals 9

    new-instance v0, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->s:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->p:[B

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    iget v6, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->w:I

    iget v7, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->x:I

    iget-boolean v8, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->j:Z

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/core/player/state/OfflineSequencerState;-><init>(Ljava/lang/String;Ljava/lang/String;[BLcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;IIZ)V

    return-object v0
.end method

.method public final r_()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->u:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->j:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->w:I

    if-lez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->s:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final s_()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->q_()Z

    move-result v0

    return v0
.end method

.method final t()Landroid/util/Pair;
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/j;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->n:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->s:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->i(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->n:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/j;->r:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->j(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_1
    new-instance v1, Landroid/util/Pair;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_1
.end method
