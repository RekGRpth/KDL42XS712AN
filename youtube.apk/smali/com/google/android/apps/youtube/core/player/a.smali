.class final Lcom/google/android/apps/youtube/core/player/a;
.super Landroid/util/SparseArray;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/util/SparseArray;-><init>()V

    const/16 v0, -0x3ea

    sget-object v1, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->VIDEO_PLAYBACK_ERROR_UNKNOWN_HOST:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/a;->put(ILjava/lang/Object;)V

    const/16 v0, -0x3eb

    sget-object v1, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->VIDEO_PLAYBACK_ERROR_CANNOT_CONNECT:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/a;->put(ILjava/lang/Object;)V

    const/16 v0, -0x3ed

    sget-object v1, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->VIDEO_PLAYBACK_ERROR_TIMEOUT:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/a;->put(ILjava/lang/Object;)V

    return-void
.end method
