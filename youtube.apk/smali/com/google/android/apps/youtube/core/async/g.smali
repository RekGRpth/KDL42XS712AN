.class public Lcom/google/android/apps/youtube/core/async/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/async/af;

.field private final b:Lcom/google/android/apps/youtube/core/converter/c;

.field private final c:Lcom/google/android/apps/youtube/core/converter/d;

.field private final d:Ljava/util/concurrent/Executor;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/d;Ljava/util/concurrent/Executor;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/g;->a:Lcom/google/android/apps/youtube/core/async/af;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/g;->b:Lcom/google/android/apps/youtube/core/converter/c;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/async/g;->c:Lcom/google/android/apps/youtube/core/converter/d;

    iput-object p4, p0, Lcom/google/android/apps/youtube/core/async/g;->d:Ljava/util/concurrent/Executor;

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/d;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/g;->a:Lcom/google/android/apps/youtube/core/async/af;

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/async/g;->b:Lcom/google/android/apps/youtube/core/converter/c;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/async/g;->c:Lcom/google/android/apps/youtube/core/converter/d;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/g;->d:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public static a(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/converter/d;)Lcom/google/android/apps/youtube/core/async/af;
    .locals 2

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/async/g;

    invoke-direct {v0, p0, v1, p1, v1}, Lcom/google/android/apps/youtube/core/async/g;-><init>(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/d;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/converter/d;Ljava/util/concurrent/Executor;)Lcom/google/android/apps/youtube/core/async/af;
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/async/g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/google/android/apps/youtube/core/async/g;-><init>(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/d;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/async/g;)Lcom/google/android/apps/youtube/core/converter/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/g;->c:Lcom/google/android/apps/youtube/core/converter/d;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/async/g;)Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/g;->d:Ljava/util/concurrent/Executor;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/g;->b:Lcom/google/android/apps/youtube/core/converter/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/g;->b:Lcom/google/android/apps/youtube/core/converter/c;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/converter/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    new-instance v1, Lcom/google/android/apps/youtube/core/async/h;

    invoke-direct {v1, p0, p1, v0, p2}, Lcom/google/android/apps/youtube/core/async/h;-><init>(Lcom/google/android/apps/youtube/core/async/g;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/async/g;->b(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    :try_end_0
    .catch Lcom/google/android/apps/youtube/core/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    move-object v0, p1

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, p2, v0}, Lcom/google/android/apps/youtube/core/async/g;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method protected a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;Ljava/lang/Exception;)V
    .locals 0

    invoke-interface {p3, p1, p4}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method protected b(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/g;->a:Lcom/google/android/apps/youtube/core/async/af;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/g;->a:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
