.class public final Lcom/google/android/apps/youtube/core/utils/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:[I


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/apps/youtube/common/e/f;

.field private final d:Lcom/google/android/apps/youtube/medialib/player/ae;

.field private final e:Ljava/lang/String;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/youtube/core/utils/a;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x90
        0xf0
        0x168
        0x2d0
        0x438
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/medialib/player/ae;Ljava/lang/String;)V
    .locals 6

    const-string v4, "googleads.g.doubleclick.net"

    const-string v5, "/pagead/ads"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/utils/a;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/medialib/player/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/medialib/player/ae;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/utils/a;->b:Landroid/content/Context;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/ae;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/utils/a;->d:Lcom/google/android/apps/youtube/medialib/player/ae;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "a."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/utils/a;->e:Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/youtube/core/utils/b;

    invoke-direct {v0, p0, p1, p4, p5}, Lcom/google/android/apps/youtube/core/utils/b;-><init>(Lcom/google/android/apps/youtube/core/utils/a;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/utils/a;->c:Lcom/google/android/apps/youtube/common/e/f;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/utils/a;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/utils/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public static c()Ljava/lang/String;
    .locals 1

    const-string v0, "ms"

    return-object v0
.end method

.method private e()Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/google/android/apps/youtube/core/utils/c;->a:Landroid/util/SparseIntArray;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/utils/a;->d:Lcom/google/android/apps/youtube/medialib/player/ae;

    sget-object v2, Lcom/google/android/apps/youtube/core/utils/a;->a:[I

    iget v3, p0, Lcom/google/android/apps/youtube/core/utils/a;->f:I

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/youtube/medialib/player/ae;->a([II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Could not select a stream, defaulting to itag 36"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    const-string v0, "36"

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/utils/a;->c:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ads/t;

    invoke-virtual {v0, p1}, Lcom/google/android/ads/t;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/utils/a;->b(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public final a()Ljava/util/Map;
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "ms"

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/utils/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public final a(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/core/utils/a;->f:I

    return-void
.end method

.method public final b(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/e/o;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "sdkv"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/utils/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "video_format"

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/utils/a;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "output"

    const-string v2, "xml_vast2"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/o;->a()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/utils/a;->c:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ads/t;

    invoke-virtual {v0}, Lcom/google/android/ads/t;->a()Lcom/google/android/ads/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/utils/a;->b:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/google/android/ads/q;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/net/Uri;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/utils/a;->c:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ads/t;

    invoke-virtual {v0, p1}, Lcom/google/android/ads/t;->a(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sdkv="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/utils/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&video_format="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/utils/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&output=xml_vast2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
