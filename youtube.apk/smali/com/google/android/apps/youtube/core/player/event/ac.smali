.class public Lcom/google/android/apps/youtube/core/player/event/ac;
.super Lcom/google/android/apps/youtube/core/client/a/g;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

.field private final c:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

.field private final f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Z)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/a/g;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/event/ac;->b:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/event/ac;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/event/ac;->d:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/youtube/core/player/event/ac;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iput-boolean p5, p0, Lcom/google/android/apps/youtube/core/player/event/ac;->f:Z

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/core/client/a/g;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/event/ac;->b:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/event/ac;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/event/ac;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-object v0
.end method

.method public final f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/event/ac;->f:Z

    return v0
.end method
