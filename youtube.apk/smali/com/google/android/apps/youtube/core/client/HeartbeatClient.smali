.class public final Lcom/google/android/apps/youtube/core/client/HeartbeatClient;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/common/e/b;

.field private final b:Ljava/util/concurrent/Executor;

.field private final c:Landroid/os/Handler;

.field private final d:Lcom/google/android/apps/youtube/datalib/innertube/m;

.field private final e:Lcom/google/android/apps/youtube/core/client/bi;

.field private final f:Ljava/lang/String;

.field private final g:J

.field private final h:J

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/Runnable;

.field private volatile k:J

.field private volatile l:I

.field private volatile m:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/common/e/b;Ljava/util/concurrent/Executor;Landroid/os/Handler;Lcom/google/android/apps/youtube/datalib/innertube/m;Lcom/google/android/apps/youtube/core/client/bi;Ljava/lang/String;JJLjava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->b:Ljava/util/concurrent/Executor;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->c:Landroid/os/Handler;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/m;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->d:Lcom/google/android/apps/youtube/datalib/innertube/m;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bi;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->e:Lcom/google/android/apps/youtube/core/client/bi;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->f:Ljava/lang/String;

    iput-wide p7, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->g:J

    iput-wide p9, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->h:J

    invoke-static {p11}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->i:Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/bh;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/youtube/core/client/bh;-><init>(Lcom/google/android/apps/youtube/core/client/HeartbeatClient;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->j:Ljava/lang/Runnable;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->k:J

    iput v2, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->l:I

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->m:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/client/HeartbeatClient;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->k:J

    return-wide p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/client/HeartbeatClient;)Lcom/google/android/apps/youtube/core/client/bi;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->e:Lcom/google/android/apps/youtube/core/client/bi;

    return-object v0
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/core/client/be;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/core/client/be;-><init>(Lcom/google/android/apps/youtube/core/client/HeartbeatClient;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->k:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->m:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->k:J

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->m:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->b:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->j:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 4

    const-wide/16 v2, 0x0

    if-eqz p1, :cond_1

    iput-wide v2, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->k:J

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->k:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->g:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->k:J

    goto :goto_0
.end method

.method public final b()Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;
    .locals 9

    new-instance v0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->f:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->g:J

    iget-wide v4, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->h:J

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->i:Ljava/lang/String;

    iget-wide v7, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->k:J

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;-><init>(Ljava/lang/String;JJLjava/lang/String;J)V

    return-object v0
.end method

.method final c()V
    .locals 6

    const/4 v5, 0x0

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->d:Lcom/google/android/apps/youtube/datalib/innertube/m;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/m;->a()Lcom/google/android/apps/youtube/datalib/innertube/o;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/o;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/o;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/o;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/o;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->d:Lcom/google/android/apps/youtube/datalib/innertube/m;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/m;->a(Lcom/google/android/apps/youtube/datalib/innertube/o;)Lcom/google/a/a/a/a/gt;
    :try_end_0
    .catch Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget-object v1, v0, Lcom/google/a/a/a/a/gt;->c:Lcom/google/a/a/a/a/mw;

    if-eqz v1, :cond_2

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    iget-object v0, v0, Lcom/google/a/a/a/a/gt;->c:Lcom/google/a/a/a/a/mw;

    invoke-direct {v1, v0, v5}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;-><init>(Lcom/google/a/a/a/a/mw;Z)V

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iput v5, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->l:I

    iput-boolean v5, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->m:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->g:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->k:J

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget v1, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->l:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->l:I

    iget v1, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->l:I

    int-to-long v1, v1

    iget-wide v3, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->h:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->a(Ljava/lang/Exception;)V

    goto :goto_0

    :cond_0
    iput-boolean v5, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->m:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x7d0

    iget v4, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->l:I

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->k:J

    goto :goto_0

    :cond_1
    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/w;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/w;)Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->a(Ljava/lang/Exception;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient;->e:Lcom/google/android/apps/youtube/core/client/bi;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->UNKNOWN:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v5, v3}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;-><init>(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;ZLjava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/client/bi;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method
