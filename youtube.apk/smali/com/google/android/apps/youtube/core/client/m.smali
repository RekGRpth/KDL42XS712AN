.class public abstract Lcom/google/android/apps/youtube/core/client/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final b:Ljava/util/concurrent/Executor;

.field protected final c:Lorg/apache/http/client/HttpClient;

.field protected final d:Lcom/google/android/apps/youtube/common/e/b;

.field protected final e:Ljava/lang/String;

.field protected final f:Lcom/google/android/apps/youtube/core/converter/http/dp;

.field protected final g:Lcom/google/android/apps/youtube/core/converter/n;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->b:Ljava/util/concurrent/Executor;

    const-string v0, "httpClient can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->c:Lorg/apache/http/client/HttpClient;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/dp;

    sget-object v1, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/converter/http/dp;-><init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->f:Lcom/google/android/apps/youtube/core/converter/http/dp;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/client/m;->g:Lcom/google/android/apps/youtube/core/converter/n;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/client/m;->e:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/client/m;->d:Lcom/google/android/apps/youtube/common/e/b;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->b:Ljava/util/concurrent/Executor;

    const-string v0, "httpClient can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->c:Lorg/apache/http/client/HttpClient;

    const-string v0, "clock can\'t be null"

    invoke-static {p3, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->d:Lcom/google/android/apps/youtube/common/e/b;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/dp;

    sget-object v1, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/converter/http/dp;-><init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->f:Lcom/google/android/apps/youtube/core/converter/http/dp;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/client/m;->g:Lcom/google/android/apps/youtube/core/converter/n;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/client/m;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->b:Ljava/util/concurrent/Executor;

    const-string v0, "httpClient can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->c:Lorg/apache/http/client/HttpClient;

    const-string v0, "xmlParser cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/converter/n;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->g:Lcom/google/android/apps/youtube/core/converter/n;

    const-string v0, "clock cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->d:Lcom/google/android/apps/youtube/common/e/b;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/dp;

    sget-object v1, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/converter/http/dp;-><init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->f:Lcom/google/android/apps/youtube/core/converter/http/dp;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/n;Ljava/lang/String;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->b:Ljava/util/concurrent/Executor;

    const-string v0, "httpClient can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->c:Lorg/apache/http/client/HttpClient;

    const-string v0, "xmlParser can\'t be null"

    invoke-static {p3, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/converter/n;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->g:Lcom/google/android/apps/youtube/core/converter/n;

    const-string v0, "cachePath can\'t be null"

    invoke-static {p4, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->e:Ljava/lang/String;

    const-string v0, "clock can\'t be null"

    invoke-static {p5, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->d:Lcom/google/android/apps/youtube/common/e/b;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/dp;

    sget-object v1, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/converter/http/dp;-><init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->f:Lcom/google/android/apps/youtube/core/converter/http/dp;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "executor can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->b:Ljava/util/concurrent/Executor;

    const-string v0, "httpClient can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->c:Lorg/apache/http/client/HttpClient;

    const-string v0, "clock can\'t be null"

    invoke-static {p4, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->d:Lcom/google/android/apps/youtube/common/e/b;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/client/m;->e:Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/dp;

    sget-object v1, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/converter/http/dp;-><init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->f:Lcom/google/android/apps/youtube/core/converter/http/dp;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->g:Lcom/google/android/apps/youtube/core/converter/n;

    return-void
.end method

.method protected static a(I)Lcom/google/android/apps/youtube/common/cache/b;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/common/cache/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/common/cache/b;-><init>(I)V

    return-object v0
.end method

.method protected static a(Lcom/google/android/apps/youtube/common/fromguava/d;Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/m;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/core/async/m;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/youtube/core/async/m;-><init>(Lcom/google/android/apps/youtube/common/fromguava/d;Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;)V

    return-object v0
.end method

.method protected static b(I)Lcom/google/android/apps/youtube/common/cache/j;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/common/cache/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/common/cache/j;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->d:Lcom/google/android/apps/youtube/common/e/b;

    const-string v1, "this instance does not contain a clock"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->d:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p1, p2, v0, p3, p4}, Lcom/google/android/apps/youtube/core/async/an;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/common/e/b;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/core/async/b;->a(Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/core/async/u;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/m;->c:Lorg/apache/http/client/HttpClient;

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/apps/youtube/core/async/u;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)V

    return-object v0
.end method

.method protected final d()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->e:Ljava/lang/String;

    const-string v1, "this instance does not support persistent caching"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->b:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/m;->e:Ljava/lang/String;

    const-wide/32 v2, 0x1400000

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/common/cache/c;->a(Ljava/util/concurrent/Executor;Ljava/lang/String;J)V

    return-void
.end method

.method protected final e()Lcom/google/android/apps/youtube/common/cache/c;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/m;->e:Ljava/lang/String;

    const-string v1, "this instance does not support persistent caching"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/common/cache/i;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/m;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/common/cache/i;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/m;->b:Ljava/util/concurrent/Executor;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/cache/i;->a(Ljava/util/concurrent/Executor;)Lcom/google/android/apps/youtube/common/cache/c;

    move-result-object v0

    return-object v0
.end method
