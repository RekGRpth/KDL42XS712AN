.class final Lcom/google/android/apps/youtube/core/offline/store/ad;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/database/Cursor;

.field private final b:Lcom/google/android/apps/youtube/core/offline/store/l;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:I

.field private final r:I


# direct methods
.method constructor <init>(Landroid/database/Cursor;Lcom/google/android/apps/youtube/core/offline/store/l;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->b:Lcom/google/android/apps/youtube/core/offline/store/l;

    const-string v0, "id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->c:I

    const-string v0, "watch_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->d:I

    const-string v0, "title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->e:I

    const-string v0, "duration"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->f:I

    const-string v0, "view_count"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->g:I

    const-string v0, "likes_count"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->h:I

    const-string v0, "dislikes_count"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->i:I

    const-string v0, "owner"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->j:I

    const-string v0, "owner_display_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->k:I

    const-string v0, "owner_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->l:I

    const-string v0, "upload_date"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->m:I

    const-string v0, "published_date"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->n:I

    const-string v0, "tags"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->o:I

    const-string v0, "description"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->p:I

    const-string v0, "subtitle_tracks_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->q:I

    const-string v0, "state"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->r:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/offline/store/ad;)Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/offline/store/ad;->a()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/offline/store/ad;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/offline/store/ad;->a()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;
    .locals 8

    const/4 v2, 0x0

    const/4 v7, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    iget v1, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->c:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    iget v1, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->j:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->b:Lcom/google/android/apps/youtube/core/offline/store/l;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/offline/store/l;->e(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->b:Lcom/google/android/apps/youtube/core/offline/store/l;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/offline/store/l;->d(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    if-eqz v4, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->b:Lcom/google/android/apps/youtube/core/offline/store/l;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/core/offline/store/l;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    :goto_0
    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;-><init>()V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->id(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->streams(Ljava/util/Set;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->defaultThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->mqThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->hqThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->sdThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->d:I

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/core/utils/i;->b(Landroid/database/Cursor;I)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->watchUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->e:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->title(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->f:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->duration(I)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->g:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->viewCount(J)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->h:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->likesCount(J)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->i:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->dislikesCount(J)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->owner(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->l:I

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/core/utils/i;->b(Landroid/database/Cursor;I)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->ownerUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->m:I

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/core/utils/i;->a(Landroid/database/Cursor;I)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->uploadedDate(Ljava/util/Date;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->n:I

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/core/utils/i;->a(Landroid/database/Cursor;I)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->publishedDate(Ljava/util/Date;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->o:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->tags(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->p:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->description(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->k:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->ownerDisplayName(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->q:I

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/core/utils/i;->b(Landroid/database/Cursor;I)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->captionTracksUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v2

    const-class v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->a:Landroid/database/Cursor;

    iget v4, p0, Lcom/google/android/apps/youtube/core/offline/store/ad;->r:I

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->state(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->isInOfflineStore(Z)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->offlineChannelAvatarThumbnailUrl(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->monetize(Z)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->build()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v1, v2

    goto/16 :goto_0
.end method
