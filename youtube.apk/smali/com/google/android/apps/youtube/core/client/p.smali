.class public final Lcom/google/android/apps/youtube/core/client/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/p;->a:Landroid/content/SharedPreferences;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/p;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    if-eqz p1, :cond_0

    const-string v1, "debugAdType"

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->ordinal()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :cond_0
    const-string v1, "debugAdType"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/p;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "debugAdEnable"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public final a()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/p;->a:Landroid/content/SharedPreferences;

    const-string v1, "debugAdEnable"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final b()Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/p;->a:Landroid/content/SharedPreferences;

    const-string v1, "debugAdType"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->values()[Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->values()[Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    move-result-object v1

    aget-object v0, v1, v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
