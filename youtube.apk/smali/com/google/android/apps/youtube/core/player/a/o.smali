.class public final Lcom/google/android/apps/youtube/core/player/a/o;
.super Lcom/google/android/apps/youtube/core/player/j;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/apps/youtube/core/player/a/l;

.field private final c:Lcom/google/android/apps/youtube/core/player/a/p;

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/medialib/player/n;Lcom/google/android/apps/youtube/core/player/a/l;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/j;-><init>(Lcom/google/android/apps/youtube/medialib/player/n;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/a/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/o;->b:Lcom/google/android/apps/youtube/core/player/a/l;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/a/p;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/youtube/core/player/a/p;-><init>(Lcom/google/android/apps/youtube/core/player/a/o;Landroid/os/Looper;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/o;->c:Lcom/google/android/apps/youtube/core/player/a/p;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/a/o;I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/player/a/o;->e(I)V

    return-void
.end method

.method private i()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/a/o;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/o;->c:Lcom/google/android/apps/youtube/core/player/a/p;

    const/4 v1, 0x1

    const/16 v2, 0x64

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/player/a/p;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/o;->b:Lcom/google/android/apps/youtube/core/player/a/l;

    invoke-interface {v0, p2}, Lcom/google/android/apps/youtube/core/player/a/l;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-super {p0, p1, v0, p3}, Lcom/google/android/apps/youtube/core/player/j;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/a/o;->d:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/a/o;->i()V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/medialib/player/o;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/core/player/j;->a(Lcom/google/android/apps/youtube/medialib/player/o;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/a/o;->i()V

    return-void
.end method

.method public final d(I)V
    .locals 0

    return-void
.end method
