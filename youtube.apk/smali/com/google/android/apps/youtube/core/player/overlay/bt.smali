.class final Lcom/google/android/apps/youtube/core/player/overlay/bt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/overlay/bs;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/bs;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bt;->a:Lcom/google/android/apps/youtube/core/player/overlay/bs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "Failed to load playerview thumbnail"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bt;->a:Lcom/google/android/apps/youtube/core/player/overlay/bs;

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/core/player/overlay/bs;->a(Lcom/google/android/apps/youtube/core/player/overlay/bs;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bt;->a:Lcom/google/android/apps/youtube/core/player/overlay/bs;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/bs;->b(Lcom/google/android/apps/youtube/core/player/overlay/bs;)Lcom/google/android/apps/youtube/core/player/overlay/br;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bt;->a:Lcom/google/android/apps/youtube/core/player/overlay/bs;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/overlay/bs;->a(Lcom/google/android/apps/youtube/core/player/overlay/bs;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/br;->setThumbnail(Landroid/graphics/Bitmap;)V

    return-void
.end method
