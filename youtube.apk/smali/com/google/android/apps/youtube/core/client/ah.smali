.class public final Lcom/google/android/apps/youtube/core/client/ah;
.super Lcom/google/android/apps/youtube/core/client/m;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/client/bj;


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private final h:I

.field private final i:I

.field private final j:Lcom/google/android/apps/youtube/core/async/af;

.field private final k:Lcom/google/android/apps/youtube/core/async/af;

.field private final l:Lcom/google/android/apps/youtube/core/async/af;

.field private final m:Lcom/google/android/apps/youtube/core/async/af;

.field private final n:Lcom/google/android/apps/youtube/core/async/af;

.field private final o:Lcom/google/android/apps/youtube/core/async/af;

.field private final p:Lcom/google/android/apps/youtube/core/async/af;

.field private final q:Lcom/google/android/apps/youtube/core/async/af;

.field private final r:Lcom/google/android/apps/youtube/core/async/af;

.field private final s:Lcom/google/android/apps/youtube/core/async/af;


# direct methods
.method private constructor <init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/client/ai;II)V
    .locals 5

    invoke-direct {p0, p1, p3, p4, p5}, Lcom/google/android/apps/youtube/core/client/m;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/apps/youtube/common/e/b;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ah;->a:Ljava/util/concurrent/Executor;

    iput p7, p0, Lcom/google/android/apps/youtube/core/client/ah;->h:I

    iput p8, p0, Lcom/google/android/apps/youtube/core/client/ah;->i:I

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/ba;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/converter/http/ba;-><init>()V

    iget v1, p0, Lcom/google/android/apps/youtube/core/client/ah;->h:I

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/client/ah;->a(I)Lcom/google/android/apps/youtube/common/cache/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/ah;->f:Lcom/google/android/apps/youtube/core/converter/http/dp;

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/youtube/core/client/ah;->a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/ah;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/client/ah;->e()Lcom/google/android/apps/youtube/common/cache/c;

    move-result-object v2

    const-wide/32 v3, 0x240c8400

    invoke-virtual {p0, v2, v0, v3, v4}, Lcom/google/android/apps/youtube/core/client/ah;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v0

    :cond_0
    new-instance v2, Lcom/google/android/apps/youtube/core/async/s;

    invoke-direct {v2, v0}, Lcom/google/android/apps/youtube/core/async/s;-><init>(Lcom/google/android/apps/youtube/core/async/af;)V

    const-wide/32 v3, 0x6ddd00

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/client/ah;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ah;->j:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ah;->j:Lcom/google/android/apps/youtube/core/async/af;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/client/ah;->a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ah;->k:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/i;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/converter/http/i;-><init>(Z)V

    iget v1, p0, Lcom/google/android/apps/youtube/core/client/ah;->i:I

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/client/ah;->b(I)Lcom/google/android/apps/youtube/common/cache/j;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/ah;->j:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/ah;->a:Ljava/util/concurrent/Executor;

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/youtube/core/async/g;->a(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/converter/d;Ljava/util/concurrent/Executor;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/client/ah;->a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v0

    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/apps/youtube/core/client/ah;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ah;->l:Lcom/google/android/apps/youtube/core/async/af;

    iget v0, p6, Lcom/google/android/apps/youtube/core/client/ai;->a:I

    iget-boolean v1, p6, Lcom/google/android/apps/youtube/core/client/ai;->d:Z

    const/4 v2, 0x1

    iget-object v3, p6, Lcom/google/android/apps/youtube/core/client/ai;->f:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/ah;->a(IZZLandroid/graphics/Bitmap$Config;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ah;->m:Lcom/google/android/apps/youtube/core/async/af;

    iget v0, p6, Lcom/google/android/apps/youtube/core/client/ai;->b:I

    iget-boolean v1, p6, Lcom/google/android/apps/youtube/core/client/ai;->e:Z

    const/4 v2, 0x1

    iget-object v3, p6, Lcom/google/android/apps/youtube/core/client/ai;->g:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/ah;->a(IZZLandroid/graphics/Bitmap$Config;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ah;->o:Lcom/google/android/apps/youtube/core/async/af;

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/client/ah;->b(I)Lcom/google/android/apps/youtube/common/cache/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/a;

    const/high16 v2, 0x3fd00000    # 1.625f

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/core/converter/a;-><init>(F)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/ah;->l:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/ah;->a:Ljava/util/concurrent/Executor;

    invoke-static {v2, v1, v3}, Lcom/google/android/apps/youtube/core/async/g;->a(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/converter/d;Ljava/util/concurrent/Executor;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/client/ah;->a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v1

    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/ah;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ah;->n:Lcom/google/android/apps/youtube/core/async/af;

    iget v0, p6, Lcom/google/android/apps/youtube/core/client/ai;->a:I

    iget-boolean v1, p6, Lcom/google/android/apps/youtube/core/client/ai;->d:Z

    const/4 v2, 0x0

    iget-object v3, p6, Lcom/google/android/apps/youtube/core/client/ai;->f:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/ah;->a(IZZLandroid/graphics/Bitmap$Config;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ah;->p:Lcom/google/android/apps/youtube/core/async/af;

    iget v0, p6, Lcom/google/android/apps/youtube/core/client/ai;->b:I

    iget-boolean v1, p6, Lcom/google/android/apps/youtube/core/client/ai;->e:Z

    const/4 v2, 0x0

    iget-object v3, p6, Lcom/google/android/apps/youtube/core/client/ai;->g:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/ah;->a(IZZLandroid/graphics/Bitmap$Config;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ah;->q:Lcom/google/android/apps/youtube/core/async/af;

    const/16 v0, 0x1e0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/ah;->a(IZZLandroid/graphics/Bitmap$Config;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ah;->r:Lcom/google/android/apps/youtube/core/async/af;

    iget v0, p6, Lcom/google/android/apps/youtube/core/client/ai;->c:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/ah;->a(IZZLandroid/graphics/Bitmap$Config;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ah;->s:Lcom/google/android/apps/youtube/core/async/af;

    return-void
.end method

.method private a(IZZLandroid/graphics/Bitmap$Config;)Lcom/google/android/apps/youtube/core/async/af;
    .locals 4

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/i;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/core/converter/http/i;-><init>(IZZLandroid/graphics/Bitmap$Config;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/ah;->j:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/ah;->a:Ljava/util/concurrent/Executor;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/youtube/core/async/g;->a(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/converter/d;Ljava/util/concurrent/Executor;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/client/ah;->a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v0

    if-eqz p3, :cond_0

    iget v1, p0, Lcom/google/android/apps/youtube/core/client/ah;->i:I

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/client/ah;->a(I)Lcom/google/android/apps/youtube/common/cache/b;

    move-result-object v1

    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/apps/youtube/core/client/ah;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/client/ai;II)Lcom/google/android/apps/youtube/core/client/ah;
    .locals 9

    new-instance v0, Lcom/google/android/apps/youtube/core/client/ah;

    const/4 v4, 0x0

    const/16 v7, 0x46

    const/16 v8, 0x1e

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/core/client/ah;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/client/ai;II)V

    return-object v0
.end method

.method public static a(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/client/ai;Z)Lcom/google/android/apps/youtube/core/client/ah;
    .locals 9

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/ah;

    if-eqz p6, :cond_0

    const/16 v7, 0x12c

    :goto_0
    if-eqz p6, :cond_1

    const/16 v8, 0x64

    :goto_1
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/core/client/ah;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/client/ai;II)V

    return-object v0

    :cond_0
    const/16 v7, 0x32

    goto :goto_0

    :cond_1
    const/16 v8, 0xf

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ah;->l:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final b(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ah;->o:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final c(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ah;->s:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final d(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ah;->k:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
