.class public Lcom/google/android/apps/youtube/core/player/PlayerView;
.super Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/medialib/player/y;

.field private b:Landroid/view/View;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/player/PlayerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/BaseApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/BaseApplication;->y()Lcom/google/android/apps/youtube/core/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->au()Lcom/google/android/apps/youtube/core/au;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/l;->a(Lcom/google/android/apps/youtube/core/au;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/PlayerView;->c:Z

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/v14/SafeTexturePlayerSurface;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlayerView;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlayerView;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlayerView;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/PlayerView;->setVideoView(Landroid/view/View;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/PlayerView;->c:Z

    new-instance v0, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/DefaultPlayerSurface;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlayerView;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlayerView;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlayerView;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/PlayerView;->setVideoView(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public final b()Lcom/google/android/apps/youtube/medialib/player/y;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlayerView;->a:Lcom/google/android/apps/youtube/medialib/player/y;

    return-object v0
.end method

.method public setSliding(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/PlayerView;->c:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlayerView;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;->setSliding(Z)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlayerView;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
