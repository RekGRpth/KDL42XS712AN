.class public abstract Lcom/google/android/apps/youtube/core/a/m;
.super Lcom/google/android/apps/youtube/core/a/e;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/apps/youtube/core/a/g;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/a/g;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/a/e;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/a/g;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a/m;->b:Lcom/google/android/apps/youtube/core/a/g;

    return-void
.end method

.method public static final a(Landroid/view/View;Z)Lcom/google/android/apps/youtube/core/a/m;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/core/a/n;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/core/a/n;-><init>(Landroid/view/View;Z)V

    return-object v0
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final a(Ljava/util/Set;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a/m;->b:Lcom/google/android/apps/youtube/core/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final b()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final e(I)Lcom/google/android/apps/youtube/core/a/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a/m;->b:Lcom/google/android/apps/youtube/core/a/g;

    return-object v0
.end method
