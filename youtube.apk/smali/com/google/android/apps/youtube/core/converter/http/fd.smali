.class final Lcom/google/android/apps/youtube/core/converter/http/fd;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;
    .locals 3

    const/4 v1, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Badly formed progress tracking event (missing offset attribute) - ignoring"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_0
    const-string v0, "%"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/e/m;->b(Ljava/lang/String;I)I

    move-result v1

    if-ltz v1, :cond_1

    const/16 v0, 0x64

    if-gt v1, v0, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;-><init>(IZLandroid/net/Uri;)V

    goto :goto_1

    :cond_1
    const-string v0, "Badly formed progress tracking event (invalid offset percentage) - ignoring"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_0
    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/ag;->a(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;-><init>(IZLandroid/net/Uri;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "Badly formed progress tracking event (invalid offset format) - ignoring"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x3

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    const-string v1, "event"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v0, "Badly formed tracking event - ignoring"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/e/p;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "start"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "Badly formed tracking uri - ignoring"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_1
    const-string v3, "creativeView"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_0

    :cond_3
    const-string v3, "firstQuartile"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->c(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_0

    :cond_4
    const-string v3, "midpoint"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_0

    :cond_5
    const-string v3, "thirdQuartile"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->e(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_0

    :cond_6
    const-string v3, "complete"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->i(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_0

    :cond_7
    const-string v3, "pause"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->k(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_0

    :cond_8
    const-string v3, "resume"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->l(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_0

    :cond_9
    const-string v3, "mute"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->m(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_0

    :cond_a
    const-string v3, "fullscreen"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->n(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto/16 :goto_0

    :cond_b
    const-string v3, "close"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->j(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto/16 :goto_0

    :cond_c
    const-string v3, "skip"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    iget v3, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a:I

    if-lt v3, v4, :cond_d

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->f(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto/16 :goto_0

    :cond_d
    const-string v3, "progress"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a:I

    if-lt v1, v4, :cond_0

    const-string v1, "offset"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/converter/http/fd;->a(Ljava/lang/String;Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
