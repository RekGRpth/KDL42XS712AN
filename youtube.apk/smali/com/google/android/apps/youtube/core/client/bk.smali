.class public final Lcom/google/android/apps/youtube/core/client/bk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;
.implements Lcom/google/android/apps/youtube/core/client/AdStatsClient;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/client/j;

.field private final b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

.field private final c:Lcom/google/android/apps/youtube/core/player/b;

.field private d:Z

.field private e:I

.field private f:I

.field private g:Ljava/util/PriorityQueue;

.field private h:Lcom/google/android/apps/youtube/core/player/al;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/client/j;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/client/bk;->h:Lcom/google/android/apps/youtube/core/player/al;

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/bk;->a:Lcom/google/android/apps/youtube/core/client/j;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->f:I

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->f:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bk;->f(I)Ljava/util/PriorityQueue;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->g:Ljava/util/PriorityQueue;

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/c;->a()Lcom/google/android/apps/youtube/core/player/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->c:Lcom/google/android/apps/youtube/core/player/b;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->c:Lcom/google/android/apps/youtube/core/player/b;

    invoke-virtual {v0, v1, p3}, Lcom/google/android/apps/youtube/core/player/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->c:Lcom/google/android/apps/youtube/core/player/b;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/player/b;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->c:Lcom/google/android/apps/youtube/core/player/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/bk;->h:Lcom/google/android/apps/youtube/core/player/al;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/b;->a(Lcom/google/android/apps/youtube/core/player/al;)V

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/youtube/core/client/j;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;IZI)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/client/bk;-><init>(Lcom/google/android/apps/youtube/core/client/j;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;)V

    iput p4, p0, Lcom/google/android/apps/youtube/core/client/bk;->e:I

    iput-boolean p5, p0, Lcom/google/android/apps/youtube/core/client/bk;->d:Z

    iput p6, p0, Lcom/google/android/apps/youtube/core/client/bk;->f:I

    invoke-direct {p0, p6}, Lcom/google/android/apps/youtube/core/client/bk;->f(I)Ljava/util/PriorityQueue;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->g:Ljava/util/PriorityQueue;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/client/bk;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-object v0
.end method

.method private a(Landroid/net/Uri;)V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/AdError;->a:Lcom/google/android/apps/youtube/core/player/AdError;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/client/bk;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/core/player/AdError;)V

    return-void
.end method

.method private a(Landroid/net/Uri;Lcom/google/android/apps/youtube/core/player/AdError;)V
    .locals 4

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->c:Lcom/google/android/apps/youtube/core/player/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/player/b;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/core/player/AdError;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/google/android/apps/youtube/core/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Pinging "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->a:Lcom/google/android/apps/youtube/core/client/j;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/bk;->a:Lcom/google/android/apps/youtube/core/client/j;

    const-string v2, "vastad"

    invoke-virtual {v1, p1, v2}, Lcom/google/android/apps/youtube/core/client/j;->a(Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/e/f;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getShouldAllowQueuedOfflinePings()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/e/f;->a(Z)Lcom/google/android/apps/youtube/datalib/e/f;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExpirationTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/e/f;->a(J)Lcom/google/android/apps/youtube/datalib/e/f;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/datalib/a/b;->a:Lcom/android/volley/n;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/j;->a(Lcom/google/android/apps/youtube/datalib/e/f;Lcom/android/volley/n;)V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to substitute URI macros "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/util/List;)Z
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/AdError;->a:Lcom/google/android/apps/youtube/core/player/AdError;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/client/bk;->a(Ljava/util/List;Lcom/google/android/apps/youtube/core/player/AdError;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/util/List;Lcom/google/android/apps/youtube/core/player/AdError;)Z
    .locals 2

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/youtube/core/client/bk;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/core/player/AdError;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private f(I)Ljava/util/PriorityQueue;
    .locals 4

    new-instance v1, Ljava/util/PriorityQueue;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getProgressPings()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-instance v2, Lcom/google/android/apps/youtube/core/client/bl;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/core/client/bl;-><init>(Lcom/google/android/apps/youtube/core/client/bk;)V

    invoke-direct {v1, v0, v2}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getProgressPings()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getDuration()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getTimeOffsetMilliseconds(I)I

    move-result v3

    if-le v3, p1, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(I)V
    .locals 3

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->g:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->g:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getDuration()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getTimeOffsetMilliseconds(I)I

    move-result v0

    if-lt p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->g:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getPingUri()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bk;->a(Landroid/net/Uri;)V

    goto :goto_0

    :cond_0
    iput p1, p0, Lcom/google/android/apps/youtube/core/client/bk;->f:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getDuration()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    if-lez v0, :cond_1

    mul-int/lit8 v1, p1, 0x4

    div-int v0, v1, v0

    :goto_1
    iget v1, p0, Lcom/google/android/apps/youtube/core/client/bk;->e:I

    if-lt v0, v1, :cond_3

    move v2, v0

    :goto_2
    iget v1, p0, Lcom/google/android/apps/youtube/core/client/bk;->e:I

    if-lt v2, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    packed-switch v2, :pswitch_data_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    :goto_3
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/bk;->a(Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_2

    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_0
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getFirstQuartilePingUris()Ljava/util/List;

    move-result-object v1

    goto :goto_3

    :pswitch_1
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getMidpointPingUris()Ljava/util/List;

    move-result-object v1

    goto :goto_3

    :pswitch_2
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getThirdQuartilePingUris()Ljava/util/List;

    move-result-object v1

    goto :goto_3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->e:I

    :cond_3
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(II)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/AdError;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/al;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/bk;->h:Lcom/google/android/apps/youtube/core/player/al;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->c:Lcom/google/android/apps/youtube/core/player/b;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/b;->a(Lcom/google/android/apps/youtube/core/player/al;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;I)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/aw;)V
    .locals 0

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    check-cast p1, Lcom/google/android/apps/youtube/core/async/w;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Ping failed "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final b(I)V
    .locals 0

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/core/player/AdError;)V
    .locals 2

    const/4 v1, 0x5

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->e:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClosePingUris()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/youtube/core/client/bk;->a(Ljava/util/List;Lcom/google/android/apps/youtube/core/player/AdError;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getErrorPingUris()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/youtube/core/client/bk;->a(Ljava/util/List;Lcom/google/android/apps/youtube/core/player/AdError;)Z

    iput v1, p0, Lcom/google/android/apps/youtube/core/client/bk;->e:I

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isForecastingAd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getImpressionUris()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bk;->a(Ljava/util/List;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->d:Z

    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 0

    return-void
.end method

.method public final d()V
    .locals 0

    return-void
.end method

.method public final d(I)V
    .locals 0

    return-void
.end method

.method public final e()V
    .locals 0

    return-void
.end method

.method public final e(I)V
    .locals 0

    return-void
.end method

.method public final f()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getImpressionUris()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bk;->a(Ljava/util/List;)Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/client/bk;->d:Z

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->e:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getStartPingUris()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bk;->a(Ljava/util/List;)Z

    iput v1, p0, Lcom/google/android/apps/youtube/core/client/bk;->e:I

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getResumePingUris()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bk;->a(Ljava/util/List;)Z

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClosePingUris()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bk;->a(Ljava/util/List;)Z

    return-void
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPausePingUris()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bk;->a(Ljava/util/List;)Z

    return-void
.end method

.method public final handleAdCompanionClickEvent(Lcom/google/android/apps/youtube/core/client/b;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    return-void
.end method

.method public final i()V
    .locals 1

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->g:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->g:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getPingUri()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bk;->a(Landroid/net/Uri;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getCompletePingUris()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bk;->a(Ljava/util/List;)Z

    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/apps/youtube/core/client/bk;->e:I

    return-void
.end method

.method public final j()Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;
    .locals 8

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;

    iget v1, p0, Lcom/google/android/apps/youtube/core/client/bk;->e:I

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/core/client/bk;->d:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    iget v6, p0, Lcom/google/android/apps/youtube/core/client/bk;->f:I

    sget-object v7, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->TRUEVIEW_INDISPLAY:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    move v4, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;-><init>(IZZZLjava/util/List;ILcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;)V

    return-object v0
.end method

.method public final k()V
    .locals 0

    return-void
.end method
