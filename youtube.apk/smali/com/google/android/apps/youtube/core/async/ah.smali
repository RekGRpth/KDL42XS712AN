.class final Lcom/google/android/apps/youtube/core/async/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/async/af;

.field private final b:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/async/af;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/ah;->a:Lcom/google/android/apps/youtube/core/async/af;

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "pageSize must be greater than zero"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    iput p2, p0, Lcom/google/android/apps/youtube/core/async/ah;->b:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/async/ah;)Lcom/google/android/apps/youtube/core/async/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/ah;->a:Lcom/google/android/apps/youtube/core/async/af;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 3

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/youtube/core/async/ah;->b:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/async/ag;->a(Landroid/net/Uri;II)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/ah;->a:Lcom/google/android/apps/youtube/core/async/af;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->b(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/youtube/core/async/ai;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/apps/youtube/core/async/ai;-><init>(Lcom/google/android/apps/youtube/core/async/ah;Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
