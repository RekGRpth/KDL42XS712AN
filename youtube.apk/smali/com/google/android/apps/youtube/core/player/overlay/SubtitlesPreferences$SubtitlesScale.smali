.class public final enum Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

.field public static final enum LARGE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

.field public static final enum NORMAL:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

.field public static final enum SMALL:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

.field public static final enum VERY_LARGE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

.field public static final enum VERY_SMALL:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

.field private static scaleEntryStrings:[Ljava/lang/String;

.field private static scaleValueStrings:[Ljava/lang/String;


# instance fields
.field private scaleStringId:I

.field private scaleValue:F


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    const-string v1, "VERY_SMALL"

    sget v2, Lcom/google/android/youtube/p;->ex:I

    const/high16 v3, 0x3e800000    # 0.25f

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->VERY_SMALL:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    const-string v1, "SMALL"

    sget v2, Lcom/google/android/youtube/p;->ev:I

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->SMALL:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    const-string v1, "NORMAL"

    sget v2, Lcom/google/android/youtube/p;->eu:I

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->NORMAL:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    const-string v1, "LARGE"

    sget v2, Lcom/google/android/youtube/p;->et:I

    const/high16 v3, 0x3fc00000    # 1.5f

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->LARGE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    const-string v1, "VERY_LARGE"

    sget v2, Lcom/google/android/youtube/p;->ew:I

    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;-><init>(Ljava/lang/String;IIF)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->VERY_LARGE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->VERY_SMALL:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->SMALL:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->NORMAL:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->LARGE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->VERY_LARGE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    aput-object v1, v0, v8

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->$VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIF)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->scaleStringId:I

    iput p4, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->scaleValue:F

    return-void
.end method

.method public static getDefaultScaleValue()F
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    move-result-object v0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->scaleValue:F

    return v0
.end method

.method public static getScaleEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->scaleEntryStrings:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    move-result-object v1

    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->scaleEntryStrings:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->scaleEntryStrings:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->scaleStringId:I

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->scaleEntryStrings:[Ljava/lang/String;

    return-object v0
.end method

.method public static getScaleValueStrings()[Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->scaleValueStrings:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    move-result-object v1

    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->scaleValueStrings:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->scaleValueStrings:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->scaleValue:F

    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->scaleValueStrings:[Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->$VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;

    return-object v0
.end method
