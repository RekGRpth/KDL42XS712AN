.class final Lcom/google/android/apps/youtube/core/ak;
.super Lcom/google/android/apps/youtube/common/e/f;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/ak;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/common/e/f;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 8

    const/4 v5, 0x1

    new-instance v0, Lcom/google/android/apps/youtube/core/client/ai;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ak;->a:Lcom/google/android/apps/youtube/core/a;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/a;->b:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/g;->ad:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/ak;->a:Lcom/google/android/apps/youtube/core/a;

    iget-object v2, v2, Lcom/google/android/apps/youtube/core/a;->b:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/youtube/g;->M:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/ak;->a:Lcom/google/android/apps/youtube/core/a;

    iget-object v3, v3, Lcom/google/android/apps/youtube/core/a;->b:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/youtube/g;->e:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xa

    if-le v4, v6, :cond_0

    move v4, v5

    :goto_0
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/client/ai;-><init>(IIIZZ)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/ak;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/a;->aJ()Ljava/util/concurrent/Executor;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/ak;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/a;->aK()Ljava/util/concurrent/Executor;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/ak;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/a;->aL()Lorg/apache/http/client/HttpClient;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/ak;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/a;->av()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/ak;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/core/a;->aG()Lcom/google/android/apps/youtube/common/e/n;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/ak;->a:Lcom/google/android/apps/youtube/core/a;

    iget-object v6, v6, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/apps/youtube/core/utils/Util;->d(Landroid/content/Context;)Z

    move-result v7

    move-object v6, v0

    invoke-static/range {v1 .. v7}, Lcom/google/android/apps/youtube/core/client/ah;->a(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/client/ai;Z)Lcom/google/android/apps/youtube/core/client/ah;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method
