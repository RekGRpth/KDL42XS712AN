.class final Lcom/google/android/apps/youtube/core/converter/http/bq;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 3

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p3, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    :try_start_0
    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;

    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Status;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Status;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->status(Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Status;)Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected status "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_0
.end method
