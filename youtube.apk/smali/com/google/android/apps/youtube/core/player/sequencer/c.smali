.class public abstract Lcom/google/android/apps/youtube/core/player/sequencer/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer;


# instance fields
.field protected final b:Lcom/google/android/apps/youtube/core/player/sequencer/r;

.field protected final c:Lcom/google/android/apps/youtube/common/c/a;

.field protected final d:Lcom/google/android/apps/youtube/core/Analytics;

.field protected final e:Lcom/google/android/apps/youtube/core/aw;

.field protected final f:Lcom/google/android/apps/youtube/core/player/ad;

.field protected volatile g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

.field protected volatile h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

.field protected volatile i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

.field protected volatile j:Z

.field protected volatile k:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/sequencer/r;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->b:Lcom/google/android/apps/youtube/core/player/sequencer/r;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->d:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->e:Lcom/google/android/apps/youtube/core/aw;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->f:Lcom/google/android/apps/youtube/core/player/ad;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->c:Lcom/google/android/apps/youtube/common/c/a;

    const-class v1, Lcom/google/android/apps/youtube/core/player/event/ac;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/sequencer/g;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/youtube/core/player/sequencer/g;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/c;B)V

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/apps/youtube/common/c/d;)Lcom/google/android/apps/youtube/common/c/e;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->c:Lcom/google/android/apps/youtube/common/c/a;

    const-class v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/sequencer/e;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/youtube/core/player/sequencer/e;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/c;B)V

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/apps/youtube/common/c/d;)Lcom/google/android/apps/youtube/common/c/e;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->c:Lcom/google/android/apps/youtube/common/c/a;

    const-class v1, Lcom/google/android/apps/youtube/core/player/event/w;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/sequencer/f;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/youtube/core/player/sequencer/f;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/c;B)V

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/apps/youtube/common/c/d;)Lcom/google/android/apps/youtube/common/c/e;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->c:Lcom/google/android/apps/youtube/common/c/a;

    const-class v1, Lcom/google/android/apps/youtube/core/player/event/c;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/sequencer/d;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/youtube/core/player/sequencer/d;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/c;B)V

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/apps/youtube/common/c/d;)Lcom/google/android/apps/youtube/common/c/e;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->f:Lcom/google/android/apps/youtube/core/player/ad;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/ad;->a()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SequencerStage: New "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method abstract a(I)V
.end method

.method protected final a(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_ERROR:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    return-void
.end method

.method protected final a(Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;)V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->NEXT:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->PREVIOUS:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->AUTOPLAY:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->isIn([Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/o;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/player/event/o;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->d:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SequencerStage: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->g()V

    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method protected g()V
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    aput-object v4, v1, v3

    const/4 v3, 0x1

    sget-object v4, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    aput-object v4, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isIn([Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v3, Lcom/google/android/apps/youtube/core/player/event/v;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-direct {v3, v4, v1, v0}, Lcom/google/android/apps/youtube/core/player/event/v;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object v1, v2

    goto :goto_0

    :cond_1
    move-object v0, v2

    goto :goto_1
.end method

.method protected final h()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/u;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->r_()Z

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->q_()Z

    move-result v3

    iget-boolean v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->j:Z

    iget-boolean v5, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->k:Z

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/youtube/core/player/event/u;-><init>(ZZZZ)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    return-void
.end method

.method protected final i()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/s;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->s()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/core/player/event/s;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    return-void
.end method

.method public final j()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->g()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->h()V

    return-void
.end method

.method public final k()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->f:Lcom/google/android/apps/youtube/core/player/ad;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/ad;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public l()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->START:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->a(Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;)V

    return-void
.end method

.method public m()V
    .locals 2

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->NEXT:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->a(Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->b:Lcom/google/android/apps/youtube/core/player/sequencer/r;

    invoke-interface {v0, v1, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/r;->a(ZI)V

    return-void
.end method

.method public n()V
    .locals 2

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->PREVIOUS:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->a(Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->b:Lcom/google/android/apps/youtube/core/player/sequencer/r;

    invoke-interface {v0, v1, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/r;->a(ZI)V

    return-void
.end method

.method public o()V
    .locals 2

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->AUTOPLAY:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->a(Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/c;->b:Lcom/google/android/apps/youtube/core/player/sequencer/r;

    invoke-interface {v0, v1, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/r;->a(ZI)V

    return-void
.end method

.method public p()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->RETRY:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/c;->a(Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;)V

    return-void
.end method

.method public q()V
    .locals 0

    return-void
.end method
