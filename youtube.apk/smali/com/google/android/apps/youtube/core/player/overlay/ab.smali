.class final Lcom/google/android/apps/youtube/core/player/overlay/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/au;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/ab;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->setScrubbing(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->g(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/aa;->a()V

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->setScrubberTime(I)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->h(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->b()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->h(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PAUSED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->a()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->h(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->ENDED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->m()V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->setScrubbing(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->g(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/aa;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/aa;->a(I)V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->h(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PAUSED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->h(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->ENDED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->m()V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->h(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->b()V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/ab;->a:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->i(Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;->c()V

    return-void
.end method
