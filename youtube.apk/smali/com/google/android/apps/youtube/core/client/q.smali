.class public final Lcom/google/android/apps/youtube/core/client/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/client/AdsClient;


# static fields
.field public static final a:J


# instance fields
.field private final b:Lcom/google/android/apps/youtube/common/e/b;

.field private final c:Lcom/google/android/apps/youtube/core/client/k;

.field private d:Lcom/google/android/apps/youtube/core/client/cj;

.field private e:Lcom/google/android/apps/youtube/core/client/VastFetcher;

.field private final f:Lcom/google/android/apps/youtube/core/client/h;

.field private final g:Lcom/google/android/apps/youtube/core/player/fetcher/d;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0xf

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/youtube/core/client/q;->a:J

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/youtube/core/client/r;)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/r;->a(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/common/e/b;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/q;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/r;->b(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/core/client/h;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/q;->f:Lcom/google/android/apps/youtube/core/client/h;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/r;->c(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/core/player/fetcher/d;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/fetcher/d;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/q;->g:Lcom/google/android/apps/youtube/core/player/fetcher/d;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/k;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/r;->d(Lcom/google/android/apps/youtube/core/client/r;)Ljava/util/concurrent/Executor;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/r;->e(Lcom/google/android/apps/youtube/core/client/r;)Lorg/apache/http/client/HttpClient;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/r;->f(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/core/converter/n;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/r;->a(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/common/e/b;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/client/k;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/q;->c:Lcom/google/android/apps/youtube/core/client/k;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/r;->g(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/core/client/cj;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/q;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/r;->f(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/core/converter/n;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/r;->g(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/cj;-><init>(Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/core/async/af;)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/q;->d:Lcom/google/android/apps/youtube/core/client/cj;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/r;->i(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/core/client/VastFetcher;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/q;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/r;->j(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/q;->f:Lcom/google/android/apps/youtube/core/client/h;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/client/q;->g:Lcom/google/android/apps/youtube/core/player/fetcher/d;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/r;->i(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/client/VastFetcher;-><init>(Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/h;Lcom/google/android/apps/youtube/core/player/fetcher/d;Lcom/google/android/apps/youtube/core/async/af;)V

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/q;->e:Lcom/google/android/apps/youtube/core/client/VastFetcher;

    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/core/client/cj;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/q;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/r;->f(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/core/converter/n;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/q;->c:Lcom/google/android/apps/youtube/core/client/k;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/r;->h(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/core/converter/http/b;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/client/cj;-><init>(Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/core/client/k;Lcom/google/android/apps/youtube/core/converter/http/b;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/core/client/VastFetcher;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/q;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/r;->j(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/q;->f:Lcom/google/android/apps/youtube/core/client/h;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/client/q;->g:Lcom/google/android/apps/youtube/core/player/fetcher/d;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/client/q;->c:Lcom/google/android/apps/youtube/core/client/k;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/client/r;->f(Lcom/google/android/apps/youtube/core/client/r;)Lcom/google/android/apps/youtube/core/converter/n;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/client/VastFetcher;-><init>(Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/h;Lcom/google/android/apps/youtube/core/player/fetcher/d;Lcom/google/android/apps/youtube/core/client/k;Lcom/google/android/apps/youtube/core/converter/n;)V

    goto :goto_1
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/client/r;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/client/q;-><init>(Lcom/google/android/apps/youtube/core/client/r;)V

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/q;->f:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/h;->g()J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Ljava/lang/String;JLjava/util/Map;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 1

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/q;->e:Lcom/google/android/apps/youtube/core/client/VastFetcher;

    invoke-virtual {v0, p1, p3, p4, p5}, Lcom/google/android/apps/youtube/core/client/VastFetcher;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;JLjava/util/Map;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 6

    sget-wide v3, Lcom/google/android/apps/youtube/core/client/q;->a:J

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/client/q;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Ljava/lang/String;JLjava/util/Map;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Ljava/lang/String;)Ljava/util/List;
    .locals 8

    const-wide/16 v0, 0x0

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/q;->f:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/client/h;->g()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v7

    cmp-long v4, v2, v0

    if-nez v4, :cond_0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/q;->f:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/client/h;->l()Lcom/google/android/apps/youtube/core/player/ad;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/core/player/ad;->g()Lcom/google/android/apps/youtube/core/player/al;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/q;->f:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/client/h;->i()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/common/network/h;->i()I

    move-result v4

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/q;->f:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/client/h;->l()Lcom/google/android/apps/youtube/core/player/ad;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/core/player/ad;->d()I

    move-result v5

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/q;->f:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/client/h;->l()Lcom/google/android/apps/youtube/core/player/ad;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/core/player/ad;->f()Z

    move-result v6

    move-object v2, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/converter/http/b;->a(JLjava/lang/String;Lcom/google/android/apps/youtube/core/player/al;IIZ)Ljava/util/Map;

    move-result-object v0

    sget-wide v1, Lcom/google/android/apps/youtube/core/client/q;->a:J

    invoke-virtual {p0, v7, v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/q;->a(Ljava/lang/String;Ljava/util/Map;J)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/q;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;J)Ljava/util/List;
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/q;->d:Lcom/google/android/apps/youtube/core/client/cj;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/core/client/cj;->a(Ljava/lang/String;Ljava/util/Map;J)Lcom/google/android/apps/youtube/datalib/legacy/model/bf;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bf;->a()Ljava/util/List;
    :try_end_0
    .catch Lcom/google/android/apps/youtube/core/client/AdsClient$VmapException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Error retrieving VMAP response"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/q;->f:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/h;->a()Lcom/google/android/apps/youtube/core/utils/a;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/utils/a;->a(I)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V
    .locals 3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isForecastingAd()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/q;->f:Lcom/google/android/apps/youtube/core/client/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/q;->b:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/h;->a(J)V

    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Ljava/lang/String;)Ljava/util/List;
    .locals 4

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVmapXml()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/q;->d:Lcom/google/android/apps/youtube/core/client/cj;

    sget-wide v2, Lcom/google/android/apps/youtube/core/client/q;->a:J

    invoke-virtual {v1, p1, v2, v3}, Lcom/google/android/apps/youtube/core/client/cj;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;J)Lcom/google/android/apps/youtube/datalib/legacy/model/bf;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bf;->a()Ljava/util/List;
    :try_end_0
    .catch Lcom/google/android/apps/youtube/core/client/AdsClient$VmapException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Error retrieving VMAP from PlayerResponse"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method
