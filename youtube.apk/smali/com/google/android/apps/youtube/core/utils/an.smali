.class public final Lcom/google/android/apps/youtube/core/utils/an;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/common/e/b;

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/youtube/common/e/n;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/common/e/n;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/utils/an;->a:Lcom/google/android/apps/youtube/common/e/b;

    new-instance v0, Lcom/google/android/apps/youtube/core/utils/ao;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/core/utils/ao;-><init>(Lcom/google/android/apps/youtube/core/utils/an;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/utils/an;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/utils/an;)Lcom/google/android/apps/youtube/common/e/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/utils/an;->a:Lcom/google/android/apps/youtube/common/e/b;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/utils/an;Lcom/google/android/apps/youtube/core/utils/ap;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/utils/an;->b(Lcom/google/android/apps/youtube/core/utils/ap;II)V

    return-void
.end method

.method private b(Lcom/google/android/apps/youtube/core/utils/ap;II)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/utils/an;->b:Landroid/os/Handler;

    invoke-virtual {v0, v4, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    const/4 v0, -0x1

    if-eq p3, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/utils/an;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v0

    int-to-long v2, p2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    sub-int v0, p3, v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/utils/an;->b:Landroid/os/Handler;

    invoke-virtual {v1, v4, p2, v4, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    if-lez v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/utils/an;->b:Landroid/os/Handler;

    int-to-long v3, v0

    invoke-virtual {v2, v1, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/utils/an;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/core/utils/ap;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/utils/an;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/utils/ap;II)V
    .locals 4

    const-string v0, "client cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/utils/an;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v0

    int-to-long v2, p2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/apps/youtube/core/utils/an;->b(Lcom/google/android/apps/youtube/core/utils/ap;II)V

    return-void
.end method
