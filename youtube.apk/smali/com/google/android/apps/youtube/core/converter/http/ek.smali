.class public final Lcom/google/android/apps/youtube/core/converter/http/ek;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/a/a/a/a/tz;Lcom/google/android/apps/youtube/common/e/b;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 5

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/youtube/core/converter/http/ek;->a(Lcom/google/a/a/a/a/tz;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v1

    sget-wide v3, Lcom/google/android/apps/youtube/core/client/q;->a:J

    add-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/google/a/a/a/a/tz;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 13

    const/4 v3, 0x0

    const/4 v12, 0x3

    const/4 v11, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/a/a/a/a/tz;->b:Ljava/lang/String;

    invoke-static {v0, v12}, Lcom/google/android/apps/youtube/common/e/m;->b(Ljava/lang/String;I)I

    move-result v2

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;-><init>(I)V

    iget-object v2, p0, Lcom/google/a/a/a/a/tz;->c:[Lcom/google/a/a/a/a/j;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/a/a/a/a/tz;->c:[Lcom/google/a/a/a/a/j;

    array-length v2, v2

    if-nez v2, :cond_2

    :cond_0
    const-string v1, "Invalid Vast Ad proto with no Ads."

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    iget-object v5, p0, Lcom/google/a/a/a/a/tz;->c:[Lcom/google/a/a/a/a/j;

    array-length v6, v5

    move v4, v1

    :goto_1
    if-ge v4, v6, :cond_13

    aget-object v2, v5, v4

    iget v7, v2, Lcom/google/a/a/a/a/j;->b:I

    if-ne v7, v11, :cond_3

    :goto_2
    if-nez v2, :cond_4

    const-string v1, "Invalid Vast Ad proto with no inLine Ad."

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    :cond_4
    iget-object v4, v2, Lcom/google/a/a/a/a/j;->e:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->f(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    iget-object v5, v2, Lcom/google/a/a/a/a/j;->c:Lcom/google/a/a/a/a/he;

    iget-object v2, v5, Lcom/google/a/a/a/a/he;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    iget-object v2, v5, Lcom/google/a/a/a/a/he;->b:Lcom/google/a/a/a/a/p;

    if-eqz v2, :cond_5

    iget-object v2, v5, Lcom/google/a/a/a/a/he;->b:Lcom/google/a/a/a/a/p;

    iget-object v2, v2, Lcom/google/a/a/a/a/p;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->g(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    :cond_5
    iget-object v2, v5, Lcom/google/a/a/a/a/he;->i:[Lcom/google/a/a/a/a/tt;

    if-eqz v2, :cond_6

    iget-object v4, v5, Lcom/google/a/a/a/a/he;->i:[Lcom/google/a/a/a/a/tt;

    array-length v6, v4

    move v2, v1

    :goto_3
    if-ge v2, v6, :cond_6

    aget-object v7, v4, v2

    :try_start_0
    iget-object v7, v7, Lcom/google/a/a/a/a/tt;->b:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/apps/youtube/common/e/p;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :catch_0
    move-exception v7

    const-string v7, "Badly formed impression uri - ignoring"

    invoke-static {v7}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_4

    :cond_6
    iget-object v2, v5, Lcom/google/a/a/a/a/he;->h:[Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v4, v5, Lcom/google/a/a/a/a/he;->h:[Ljava/lang/String;

    array-length v6, v4

    move v2, v1

    :goto_5
    if-ge v2, v6, :cond_7

    aget-object v7, v4, v2

    :try_start_1
    invoke-static {v7}, Lcom/google/android/apps/youtube/common/e/p;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->q(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :catch_1
    move-exception v7

    const-string v7, "Badly formed error uri - ignoring"

    invoke-static {v7}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_6

    :cond_7
    iget-object v2, v5, Lcom/google/a/a/a/a/he;->j:[Lcom/google/a/a/a/a/hh;

    if-eqz v2, :cond_e

    iget-object v4, v5, Lcom/google/a/a/a/a/he;->j:[Lcom/google/a/a/a/a/hh;

    array-length v6, v4

    move v2, v1

    :goto_7
    if-ge v2, v6, :cond_e

    aget-object v7, v4, v2

    iget v8, v7, Lcom/google/a/a/a/a/hh;->b:I

    if-ne v8, v11, :cond_d

    iget-object v2, v7, Lcom/google/a/a/a/a/hh;->c:Lcom/google/a/a/a/a/hi;

    iget-object v6, v2, Lcom/google/a/a/a/a/hi;->e:[Lcom/google/a/a/a/a/ta;

    array-length v7, v6

    move v2, v1

    :goto_8
    if-ge v2, v7, :cond_e

    aget-object v4, v6, v2

    if-nez v4, :cond_9

    const-string v4, "Badly formed tracking event - ignoring"

    invoke-static {v4}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :cond_8
    :goto_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    :cond_9
    :try_start_2
    iget-object v8, v4, Lcom/google/a/a/a/a/ta;->c:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/apps/youtube/common/e/p;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    iget v9, v4, Lcom/google/a/a/a/a/ta;->b:I

    packed-switch v9, :pswitch_data_0

    :pswitch_0
    const-string v4, "Badly formed tracking uri - ignoring"

    invoke-static {v4}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_9

    :catch_2
    move-exception v4

    const-string v4, "Badly formed tracking uri - ignoring"

    invoke-static {v4}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_9

    :pswitch_1
    :try_start_3
    invoke-virtual {v0, v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_9

    :pswitch_2
    invoke-virtual {v0, v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->c(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_9

    :pswitch_3
    invoke-virtual {v0, v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_9

    :pswitch_4
    invoke-virtual {v0, v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->e(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_9

    :pswitch_5
    invoke-virtual {v0, v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->i(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_9

    :pswitch_6
    invoke-virtual {v0, v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->k(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_9

    :pswitch_7
    invoke-virtual {v0, v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->l(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_9

    :pswitch_8
    invoke-virtual {v0, v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->m(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_9

    :pswitch_9
    invoke-virtual {v0, v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->n(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_9

    :pswitch_a
    invoke-virtual {v0, v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->j(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_9

    :pswitch_b
    iget v4, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a:I

    if-lt v4, v12, :cond_8

    invoke-virtual {v0, v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->f(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_9

    :pswitch_c
    iget v9, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a:I

    if-lt v9, v12, :cond_8

    iget-object v9, v4, Lcom/google/a/a/a/a/ta;->d:Lcom/google/a/a/a/a/mp;

    iget v4, v9, Lcom/google/a/a/a/a/mp;->b:I

    if-ne v4, v11, :cond_a

    iget v4, v9, Lcom/google/a/a/a/a/mp;->c:F

    const/4 v10, 0x0

    cmpl-float v4, v4, v10

    if-ltz v4, :cond_c

    iget v4, v9, Lcom/google/a/a/a/a/mp;->c:F

    const/high16 v10, 0x42c80000    # 100.0f

    cmpg-float v4, v4, v10

    if-gtz v4, :cond_c

    new-instance v4, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    iget v9, v9, Lcom/google/a/a/a/a/mp;->c:F

    float-to-int v9, v9

    const/4 v10, 0x1

    invoke-direct {v4, v9, v10, v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;-><init>(IZLandroid/net/Uri;)V

    :goto_a
    if-eqz v4, :cond_8

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    goto :goto_9

    :cond_a
    iget v4, v9, Lcom/google/a/a/a/a/mp;->b:I

    const/4 v10, 0x2

    if-ne v4, v10, :cond_b

    new-instance v4, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    iget v9, v9, Lcom/google/a/a/a/a/mp;->d:I

    const/4 v10, 0x0

    invoke-direct {v4, v9, v10, v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;-><init>(IZLandroid/net/Uri;)V

    goto :goto_a

    :cond_b
    const-string v4, "Badly formed progress tracking uri - ignoring"

    invoke-static {v4}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_c
    move-object v4, v3

    goto :goto_a

    :cond_d
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_7

    :cond_e
    iget-object v6, v5, Lcom/google/a/a/a/a/he;->k:[Lcom/google/a/a/a/a/ex;

    if-eqz v6, :cond_1

    array-length v7, v6

    move v4, v1

    :goto_b
    if-ge v4, v7, :cond_11

    aget-object v2, v6, v4

    iget-object v8, v2, Lcom/google/a/a/a/a/ex;->b:[Lcom/google/a/a/a/a/ac;

    array-length v9, v8

    move v5, v1

    :goto_c
    if-ge v5, v9, :cond_10

    aget-object v10, v8, v5

    iget-object v11, v10, Lcom/google/a/a/a/a/ac;->b:Lcom/google/a/a/a/a/ky;

    if-eqz v11, :cond_f

    const-string v11, "type"

    iget-object v12, v10, Lcom/google/a/a/a/a/ac;->b:Lcom/google/a/a/a/a/ky;

    iget-object v12, v12, Lcom/google/a/a/a/a/ky;->b:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_f

    const-string v11, "adsense"

    iget-object v10, v10, Lcom/google/a/a/a/a/ac;->c:Ljava/lang/String;

    invoke-virtual {v11, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_f

    :goto_d
    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/google/a/a/a/a/ex;->d:[Lcom/google/a/a/a/a/ll;

    array-length v3, v2

    :goto_e
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    iget-object v5, v4, Lcom/google/a/a/a/a/ll;->b:Lcom/google/a/a/a/a/ky;

    if-eqz v5, :cond_12

    const-string v5, "ConversionUrl"

    iget-object v6, v4, Lcom/google/a/a/a/a/ll;->b:Lcom/google/a/a/a/a/ky;

    iget-object v6, v6, Lcom/google/a/a/a/a/ky;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_12

    :try_start_4
    iget-object v1, v4, Lcom/google/a/a/a/a/ll;->d:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/e/p;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->v(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    :try_end_4
    .catch Ljava/net/MalformedURLException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v1

    const-string v1, "Badly formed ConversionUrl uri - ignoring"

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_f
    add-int/lit8 v5, v5, 0x1

    goto :goto_c

    :cond_10
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_b

    :cond_11
    move-object v2, v3

    goto :goto_d

    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    :cond_13
    move-object v2, v3

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method
