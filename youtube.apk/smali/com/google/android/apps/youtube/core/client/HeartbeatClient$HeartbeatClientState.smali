.class public Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final heartbeatToken:Ljava/lang/String;

.field private final intervalMsec:J

.field private final maxRetries:J

.field private final nextHeartbeatTimestamp:J

.field private final videoId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/core/client/bg;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/client/bg;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->heartbeatToken:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->intervalMsec:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->maxRetries:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->videoId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->nextHeartbeatTimestamp:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JJLjava/lang/String;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->heartbeatToken:Ljava/lang/String;

    iput-wide p2, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->intervalMsec:J

    iput-wide p4, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->maxRetries:J

    iput-object p6, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->videoId:Ljava/lang/String;

    iput-wide p7, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->nextHeartbeatTimestamp:J

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->heartbeatToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->intervalMsec:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->maxRetries:J

    return-wide v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->videoId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->nextHeartbeatTimestamp:J

    return-wide v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->heartbeatToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->intervalMsec:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->maxRetries:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/client/HeartbeatClient$HeartbeatClientState;->nextHeartbeatTimestamp:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
