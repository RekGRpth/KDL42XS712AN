.class public final enum Lcom/google/android/apps/youtube/core/player/CueRange$Style;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/player/CueRange$Style;

.field public static final enum AD_MARKER:Lcom/google/android/apps/youtube/core/player/CueRange$Style;

.field public static final enum NOT_DRAWABLE:Lcom/google/android/apps/youtube/core/player/CueRange$Style;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/CueRange$Style;

    const-string v1, "NOT_DRAWABLE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/CueRange$Style;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/CueRange$Style;->NOT_DRAWABLE:Lcom/google/android/apps/youtube/core/player/CueRange$Style;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/CueRange$Style;

    const-string v1, "AD_MARKER"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/core/player/CueRange$Style;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/CueRange$Style;->AD_MARKER:Lcom/google/android/apps/youtube/core/player/CueRange$Style;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/CueRange$Style;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/CueRange$Style;->NOT_DRAWABLE:Lcom/google/android/apps/youtube/core/player/CueRange$Style;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/core/player/CueRange$Style;->AD_MARKER:Lcom/google/android/apps/youtube/core/player/CueRange$Style;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/youtube/core/player/CueRange$Style;->$VALUES:[Lcom/google/android/apps/youtube/core/player/CueRange$Style;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/CueRange$Style;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/player/CueRange$Style;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/CueRange$Style;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/player/CueRange$Style;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/CueRange$Style;->$VALUES:[Lcom/google/android/apps/youtube/core/player/CueRange$Style;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/player/CueRange$Style;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/player/CueRange$Style;

    return-object v0
.end method
