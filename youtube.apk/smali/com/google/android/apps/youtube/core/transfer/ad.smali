.class final Lcom/google/android/apps/youtube/core/transfer/ad;
.super Ljava/io/FilterOutputStream;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/transfer/aa;

.field private final b:Ljava/io/OutputStream;

.field private c:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/transfer/aa;Ljava/io/OutputStream;J)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->a:Lcom/google/android/apps/youtube/core/transfer/aa;

    invoke-direct {p0, p2}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->b:Ljava/io/OutputStream;

    iput-wide p3, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->c:J

    return-void
.end method

.method private a(JJ)V
    .locals 4

    const-wide/32 v2, 0x19000

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->a:Lcom/google/android/apps/youtube/core/transfer/aa;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/transfer/aa;->b(Lcom/google/android/apps/youtube/core/transfer/aa;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->a:Lcom/google/android/apps/youtube/core/transfer/aa;

    invoke-static {v0, p3, p4}, Lcom/google/android/apps/youtube/core/transfer/aa;->a(Lcom/google/android/apps/youtube/core/transfer/aa;J)J

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    div-long v0, p3, v2

    div-long v2, p1, v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->a:Lcom/google/android/apps/youtube/core/transfer/aa;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/transfer/aa;->c(Lcom/google/android/apps/youtube/core/transfer/aa;)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    cmp-long v0, p3, v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->a:Lcom/google/android/apps/youtube/core/transfer/aa;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/transfer/aa;->e(Lcom/google/android/apps/youtube/core/transfer/aa;)Lcom/google/android/apps/youtube/core/transfer/m;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->a:Lcom/google/android/apps/youtube/core/transfer/aa;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/transfer/aa;->d(Lcom/google/android/apps/youtube/core/transfer/aa;)Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p3, p4}, Lcom/google/android/apps/youtube/core/transfer/m;->b(Ljava/lang/String;J)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final write(I)V
    .locals 4

    const-wide/16 v2, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->b:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->c:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->c:J

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->c:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->c:J

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/transfer/ad;->a(JJ)V

    return-void
.end method

.method public final write([BII)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->b:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->c:J

    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->c:J

    int-to-long v4, p3

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->c:J

    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/transfer/ad;->c:J

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/transfer/ad;->a(JJ)V

    return-void
.end method
