.class public abstract Lcom/google/android/apps/youtube/core/player/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/medialib/player/n;
.implements Lcom/google/android/apps/youtube/medialib/player/o;


# instance fields
.field protected final a:Lcom/google/android/apps/youtube/medialib/player/n;

.field private b:Lcom/google/android/apps/youtube/medialib/player/o;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/medialib/player/n;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/n;

    invoke-interface {p1, p0}, Lcom/google/android/apps/youtube/medialib/player/n;->a(Lcom/google/android/apps/youtube/medialib/player/o;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/n;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/n;->a()V

    return-void
.end method

.method public final a(FF)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/n;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/medialib/player/n;->a(FF)V

    return-void
.end method

.method public a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/n;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/n;->a(I)V

    return-void
.end method

.method public a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/n;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/medialib/player/n;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    return-void
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/n;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/n;->a(Landroid/view/Surface;)V

    return-void
.end method

.method public final a(Landroid/view/SurfaceHolder;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/n;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/n;->a(Landroid/view/SurfaceHolder;)V

    return-void
.end method

.method public a(Lcom/google/android/apps/youtube/medialib/player/n;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->b:Lcom/google/android/apps/youtube/medialib/player/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->b:Lcom/google/android/apps/youtube/medialib/player/o;

    invoke-interface {v0, p0}, Lcom/google/android/apps/youtube/medialib/player/o;->a(Lcom/google/android/apps/youtube/medialib/player/n;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/youtube/medialib/player/o;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/j;->b:Lcom/google/android/apps/youtube/medialib/player/o;

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/n;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/n;->a(Z)V

    return-void
.end method

.method public final a(II)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->b:Lcom/google/android/apps/youtube/medialib/player/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->b:Lcom/google/android/apps/youtube/medialib/player/o;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/medialib/player/o;->a(II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/youtube/medialib/player/n;II)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->b:Lcom/google/android/apps/youtube/medialib/player/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->b:Lcom/google/android/apps/youtube/medialib/player/o;

    invoke-interface {v0, p0, p2, p3}, Lcom/google/android/apps/youtube/medialib/player/o;->a(Lcom/google/android/apps/youtube/medialib/player/n;II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/n;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/n;->b()V

    return-void
.end method

.method public final b(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/n;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/n;->b(I)V

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/medialib/player/n;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->b:Lcom/google/android/apps/youtube/medialib/player/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->b:Lcom/google/android/apps/youtube/medialib/player/o;

    invoke-interface {v0, p0}, Lcom/google/android/apps/youtube/medialib/player/o;->b(Lcom/google/android/apps/youtube/medialib/player/n;)V

    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/medialib/player/n;II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->b:Lcom/google/android/apps/youtube/medialib/player/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->b:Lcom/google/android/apps/youtube/medialib/player/o;

    invoke-interface {v0, p0, p2, p3}, Lcom/google/android/apps/youtube/medialib/player/o;->b(Lcom/google/android/apps/youtube/medialib/player/n;II)V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/n;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/n;->c()V

    return-void
.end method

.method public final c(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/n;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/n;->c(I)V

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/n;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/n;->d()V

    return-void
.end method

.method public d(I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/player/j;->e(I)V

    return-void
.end method

.method public e()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/n;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/n;->e()I

    move-result v0

    return v0
.end method

.method protected final e(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->b:Lcom/google/android/apps/youtube/medialib/player/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->b:Lcom/google/android/apps/youtube/medialib/player/o;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/medialib/player/o;->d(I)V

    :cond_0
    return-void
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/n;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/n;->f()I

    move-result v0

    return v0
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->b:Lcom/google/android/apps/youtube/medialib/player/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->b:Lcom/google/android/apps/youtube/medialib/player/o;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/o;->g()V

    :cond_0
    return-void
.end method

.method public h()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/j;->a:Lcom/google/android/apps/youtube/medialib/player/n;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/n;->h()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
