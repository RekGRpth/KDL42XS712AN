.class public final Lcom/google/android/apps/youtube/core/identity/UserProfile;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final LEGAL_AGE:I = 0x12


# instance fields
.field public final activityUri:Landroid/net/Uri;

.field public final age:I

.field public final alternateUri:Landroid/net/Uri;

.field public final channelId:Ljava/lang/String;

.field public final channelViewsCount:J

.field public final displayUsername:Ljava/lang/String;

.field public final email:Ljava/lang/String;

.field public final favoritesCount:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final favoritesUri:Landroid/net/Uri;

.field public final gender:Lcom/google/android/apps/youtube/core/identity/UserProfile$Gender;

.field public final isEligibleForChannel:Z

.field public final isLightweight:Z

.field public final playlistsUri:Landroid/net/Uri;

.field public final plusUserId:Ljava/lang/String;

.field public final selfUri:Landroid/net/Uri;

.field public final subscribersCount:I

.field public final subscriptionsCount:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final subscriptionsUri:Landroid/net/Uri;

.field public final summary:Ljava/lang/String;

.field public final thumbnailUri:Landroid/net/Uri;

.field public final uploadViewsCount:J

.field public final uploadedCount:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final uploadsUri:Landroid/net/Uri;

.field public final uri:Landroid/net/Uri;

.field public final username:Ljava/lang/String;

.field public final watchHistoryUri:Landroid/net/Uri;

.field public final watchLaterUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/apps/youtube/core/identity/UserProfile$Gender;Ljava/lang/String;Landroid/net/Uri;ZZLandroid/net/Uri;ILandroid/net/Uri;ILandroid/net/Uri;ILandroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;JJI)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->selfUri:Landroid/net/Uri;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->alternateUri:Landroid/net/Uri;

    if-eqz p4, :cond_0

    move-object v2, p4

    :goto_0
    iput-object v2, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->username:Ljava/lang/String;

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    iput-object p4, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->displayUsername:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->channelId:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->plusUserId:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->email:Ljava/lang/String;

    iput p9, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->age:I

    iput-object p10, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->gender:Lcom/google/android/apps/youtube/core/identity/UserProfile$Gender;

    iput-object p11, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->summary:Ljava/lang/String;

    iput-object p12, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->thumbnailUri:Landroid/net/Uri;

    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->isLightweight:Z

    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->isEligibleForChannel:Z

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uploadsUri:Landroid/net/Uri;

    move/from16 v0, p16

    iput v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uploadedCount:I

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->favoritesUri:Landroid/net/Uri;

    move/from16 v0, p18

    iput v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->favoritesCount:I

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->subscriptionsUri:Landroid/net/Uri;

    move/from16 v0, p20

    iput v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->subscriptionsCount:I

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->watchHistoryUri:Landroid/net/Uri;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->watchLaterUri:Landroid/net/Uri;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->playlistsUri:Landroid/net/Uri;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->activityUri:Landroid/net/Uri;

    move-wide/from16 v0, p25

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->channelViewsCount:J

    move-wide/from16 v0, p27

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uploadViewsCount:J

    move/from16 v0, p29

    iput v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->subscribersCount:I

    return-void

    :cond_0
    move-object v2, p8

    goto :goto_0

    :cond_1
    move-object p4, p5

    goto :goto_1
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "builder required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/identity/UserProfile;->buildUpon()Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final buildUpon()Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->uri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->selfUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->selfUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->alternateUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->alternateUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->username:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->username(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->displayUsername:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->displayUsername(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->channelId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->channelId(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->plusUserId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->plusUserId(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->email(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->age:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->age(I)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->gender:Lcom/google/android/apps/youtube/core/identity/UserProfile$Gender;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->gender(Lcom/google/android/apps/youtube/core/identity/UserProfile$Gender;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->summary:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->summary(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->thumbnailUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->thumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->isLightweight:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->isLightweight(Z)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->isEligibleForChannel:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->isEligibleForChannel(Z)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uploadsUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->uploadsUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uploadedCount:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->uploadedCount(I)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->favoritesUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->favoritesUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->favoritesCount:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->favoritesCount(I)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->subscriptionsUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->subscriptionsUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->subscriptionsCount:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->subscriptionsCount(I)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->watchHistoryUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->watchHistoryUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->watchLaterUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->watchLaterUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->playlistsUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->playlistsUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->activityUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->activityUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->channelViewsCount:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->channelViewsCount(J)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->uploadViewsCount:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->uploadViewsCount(J)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->subscribersCount:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;->subscribersCount(I)Lcom/google/android/apps/youtube/core/identity/UserProfile$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final hasAge()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->age:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasLegalAge()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->age:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->username:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->username:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->email:Ljava/lang/String;

    goto :goto_0
.end method
