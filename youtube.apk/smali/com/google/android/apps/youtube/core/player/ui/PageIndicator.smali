.class public Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field a:Landroid/graphics/Rect;

.field b:Landroid/graphics/Rect;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:Landroid/graphics/drawable/Drawable;

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->a:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->b:Landroid/graphics/Rect;

    sget-object v0, Lcom/google/android/youtube/r;->w:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/youtube/h;->ao:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/google/android/youtube/h;->ap:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_1
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v1, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v0, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->c:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->e:I

    iput v6, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->f:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->g:I

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->f:I

    if-gtz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->f:I

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->e:I

    add-int/2addr v3, v4

    mul-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->e:I

    sub-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->a:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getPaddingTop()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getHeight()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getPaddingBottom()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    const/16 v3, 0x31

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->a:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->b:Landroid/graphics/Rect;

    invoke-static {v3, v2, v1, v4, v5}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    :goto_1
    iget v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->f:I

    if-ge v0, v1, :cond_2

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->g:I

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->d:Landroid/graphics/drawable/Drawable;

    :goto_2
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->e:I

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    add-int/2addr v1, v2

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->c:Landroid/graphics/drawable/Drawable;

    goto :goto_2

    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 4

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->f:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->f:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->e:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getPaddingLeft()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->resolveSize(II)I

    move-result v0

    invoke-static {v1, p2}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->setMeasuredDimension(II)V

    return-void
.end method

.method public setCurrentPage(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->g:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->g:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->invalidate()V

    :cond_0
    return-void
.end method

.method public setPageCount(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->f:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->f:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/ui/PageIndicator;->invalidate()V

    :cond_0
    return-void
.end method
