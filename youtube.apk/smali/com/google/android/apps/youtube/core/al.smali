.class final Lcom/google/android/apps/youtube/core/al;
.super Lcom/google/android/apps/youtube/common/e/f;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/al;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/common/e/f;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 9

    new-instance v0, Lcom/google/android/apps/youtube/core/client/v;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/al;->a:Lcom/google/android/apps/youtube/core/a;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/al;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/a;->aJ()Ljava/util/concurrent/Executor;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/al;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/a;->aL()Lorg/apache/http/client/HttpClient;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/al;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/a;->aG()Lcom/google/android/apps/youtube/common/e/n;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/al;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/core/a;->aN()Lcom/google/android/apps/youtube/core/converter/n;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/al;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/core/a;->aB()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/al;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/core/a;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/youtube/core/al;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/core/a;->av()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/core/client/v;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;Lcom/google/android/apps/youtube/core/identity/l;Ljava/lang/String;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/al;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/a;->aE()Lcom/google/android/apps/youtube/datalib/f/a;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/al;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/a;->aS()Lcom/google/android/apps/youtube/core/identity/k;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/al;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/a;->aA()Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/al;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/a;->aR()Lcom/google/android/apps/youtube/core/identity/b;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/al;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/a;->aU()Lcom/google/android/apps/youtube/core/identity/ar;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/al;->a:Lcom/google/android/apps/youtube/core/a;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/core/a;->au()Lcom/google/android/apps/youtube/core/au;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/youtube/core/au;->C()Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/client/v;->a(Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;Lcom/google/android/apps/youtube/core/identity/b;Lcom/google/android/apps/youtube/core/identity/ar;Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;)V

    return-object v0
.end method
