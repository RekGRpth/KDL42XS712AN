.class public final Lcom/google/android/apps/youtube/core/converter/http/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/converter/c;


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private final b:Ljava/util/List;

.field private final c:Lcom/google/android/apps/youtube/core/client/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "https://www.youtube.com/get_ad_tags"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/core/converter/http/b;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/client/h;Ljava/util/List;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/b;->c:Lcom/google/android/apps/youtube/core/client/h;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/b;->b:Ljava/util/List;

    return-void
.end method

.method public static a(JLjava/lang/String;Lcom/google/android/apps/youtube/core/player/al;IIZ)Ljava/util/Map;
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "last_ad"

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "cpn"

    const-string v2, "cpn cannot be null or empty"

    invoke-static {p2, v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p3, :cond_0

    const-string v1, "vis"

    invoke-virtual {p3}, Lcom/google/android/apps/youtube/core/player/al;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const-string v1, "conn"

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "apsad"

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p6, :cond_1

    const-string v1, "splay"

    const-string v2, "1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public static a(Z)Ljava/util/Map;
    .locals 3

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "pin"

    if-eqz p0, :cond_0

    const-string v0, "2"

    :goto_0
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v1

    :cond_0
    const-string v0, "1"

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/youtube/core/converter/http/c;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 6

    sget-object v0, Lcom/google/android/apps/youtube/core/converter/http/b;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "action_vmap"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "version"

    const-string v2, "2"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "v"

    iget-object v2, p1, Lcom/google/android/apps/youtube/core/converter/http/c;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "platform"

    const-string v2, "android"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "afv_instream"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/b;->c:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/h;->k()Lcom/google/android/apps/youtube/core/client/cf;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "lact"

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/cf;->b()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/b;->c:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/h;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "clientid"

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/converter/http/b;->c:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/client/h;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/b;->c:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/h;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "iso_country"

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/converter/http/b;->c:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/client/h;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/youtube/core/converter/http/c;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/b;->c:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/h;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "more_ads"

    const-string v1, "1"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/b;->c:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/h;->a()Lcom/google/android/apps/youtube/core/utils/a;

    move-result-object v0

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/utils/a;->b(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/a;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v5, v1, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/b;->c:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/h;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "channel_id"

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/converter/http/b;->c:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/client/h;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/b;->c:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/h;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "Doritos"

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/converter/http/b;->c:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/client/h;->d()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    :try_start_0
    new-instance v0, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v0, v3}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V

    invoke-virtual {v2, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/b;->c:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/h;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    const-string v0, "embedded_app_package"

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/converter/http/b;->c:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/client/h;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/a/p;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/datalib/a/p;->a(Lorg/apache/http/client/methods/HttpUriRequest;)V

    goto :goto_3

    :catch_0
    move-exception v0

    const-string v0, "UnsupportedEncodingException encountered when generating adTagRequest"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    goto :goto_2

    :cond_9
    return-object v2
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/core/converter/http/c;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/converter/http/b;->a(Lcom/google/android/apps/youtube/core/converter/http/c;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method
