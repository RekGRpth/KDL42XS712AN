.class final Lcom/google/android/apps/youtube/core/player/ba;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/at;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;

.field private final b:J

.field private c:Lcom/google/android/apps/youtube/core/player/aw;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/ba;->a:Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->b(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;)Lcom/google/android/apps/youtube/common/e/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/player/ba;->b:J

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/ba;)Lcom/google/android/apps/youtube/core/player/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ba;->c:Lcom/google/android/apps/youtube/core/player/aw;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 8

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ba;->a:Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->e(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;)Lcom/google/android/apps/youtube/core/player/ba;

    move-result-object v0

    if-eq v0, p0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager$VideoInterruptStaleException;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager$VideoInterruptStaleException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ba;->a:Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;

    invoke-static {v0, p0}, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->b(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;Lcom/google/android/apps/youtube/core/player/ba;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ba;->a:Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->c(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;Lcom/google/android/apps/youtube/core/player/ba;)Lcom/google/android/apps/youtube/core/player/ba;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ba;->a:Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->c(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;)Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "VideoInterruption"

    const-string v2, "InterruptionTime"

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/ba;->a:Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;

    invoke-static {v4}, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->b(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;)Lcom/google/android/apps/youtube/common/e/b;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/apps/youtube/core/player/ba;->b:J

    sub-long/2addr v4, v6

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v6}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v3

    long-to-int v3, v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ba;->a:Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->d(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;)V

    return-void
.end method
