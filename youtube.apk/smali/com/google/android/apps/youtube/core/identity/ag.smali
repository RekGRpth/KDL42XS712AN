.class final Lcom/google/android/apps/youtube/core/identity/ag;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/identity/aa;

.field private final b:Landroid/app/Activity;

.field private final c:Lcom/google/android/apps/youtube/core/identity/f;

.field private final d:Lcom/google/android/apps/youtube/common/e/d;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/identity/aa;Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/f;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/identity/ag;->a:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/identity/ag;->b:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/identity/ag;->c:Lcom/google/android/apps/youtube/core/identity/f;

    new-instance v0, Lcom/google/android/apps/youtube/common/e/d;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/common/e/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/ag;->d:Lcom/google/android/apps/youtube/common/e/d;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/ag;->a:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/identity/aa;->b(Lcom/google/android/apps/youtube/core/identity/aa;)Lcom/google/android/apps/youtube/core/identity/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/ag;->c:Lcom/google/android/apps/youtube/core/identity/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/b;->a(Lcom/google/android/apps/youtube/core/identity/f;)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/a;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->a()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/ag;->d:Lcom/google/android/apps/youtube/common/e/d;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/common/e/d;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/identity/ag;->run()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/ag;->a:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Ljava/lang/Exception;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/ag;->a:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Ljava/lang/Exception;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/ag;->a:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->f()Ljava/lang/Exception;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Ljava/lang/Exception;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->a()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->e()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/ag;->a:Lcom/google/android/apps/youtube/core/identity/aa;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/ag;->b:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/identity/ag;->c:Lcom/google/android/apps/youtube/core/identity/f;

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Lcom/google/android/apps/youtube/core/identity/aa;Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/f;Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->a()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/ag;->a:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->f()Ljava/lang/Exception;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Ljava/lang/Exception;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/ag;->a:Lcom/google/android/apps/youtube/core/identity/aa;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/ag;->b:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/ag;->c:Lcom/google/android/apps/youtube/core/identity/f;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/f;)V

    goto :goto_0
.end method
