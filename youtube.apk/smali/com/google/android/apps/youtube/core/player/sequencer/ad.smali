.class final Lcom/google/android/apps/youtube/core/player/sequencer/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/Exception;

.field final synthetic b:Lcom/google/android/apps/youtube/core/player/sequencer/y;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/y;Ljava/lang/Exception;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ad;->b:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ad;->a:Ljava/lang/Exception;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ad;->b:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a(Lcom/google/android/apps/youtube/core/player/sequencer/y;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ad;->b:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->WATCH_NEXT_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ad;->b:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    iget-object v4, v4, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    iget-object v4, v4, Lcom/google/android/apps/youtube/core/player/sequencer/u;->e:Lcom/google/android/apps/youtube/core/aw;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ad;->a:Ljava/lang/Exception;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ad;->a:Ljava/lang/Exception;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;-><init>(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;ZLjava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method
