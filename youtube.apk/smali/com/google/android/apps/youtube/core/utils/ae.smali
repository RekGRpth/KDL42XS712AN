.class final Lcom/google/android/apps/youtube/core/utils/ae;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 11

    const/16 v10, 0x64

    const/4 v2, 0x2

    const/4 v5, 0x1

    const/4 v7, 0x0

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/an;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/apps/youtube/datalib/legacy/model/an;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "w"

    aput-object v1, v0, v7

    const-string v1, "win"

    aput-object v1, v0, v5

    const-string v1, "id"

    aput-object v1, v0, v2

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/core/utils/aa;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v7}, Lcom/google/android/apps/youtube/common/e/m;->b(Ljava/lang/String;I)I

    move-result v8

    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "t"

    aput-object v1, v0, v7

    const-string v1, "start"

    aput-object v1, v0, v5

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/core/utils/aa;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v9

    const-string v0, "op"

    invoke-interface {p2, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "define"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v1, 0x22

    const/16 v2, 0x32

    const/16 v3, 0x5f

    new-array v0, v5, [Ljava/lang/String;

    const-string v4, "ap"

    aput-object v4, v0, v7

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/core/utils/aa;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/aa;->a(I)I

    move-result v1

    :cond_0
    new-array v0, v5, [Ljava/lang/String;

    const-string v4, "ah"

    aput-object v4, v0, v7

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/core/utils/aa;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0, v10}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    :cond_1
    new-array v0, v5, [Ljava/lang/String;

    const-string v4, "av"

    aput-object v4, v0, v7

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/core/utils/aa;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0, v10}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    :cond_2
    new-array v0, v5, [Ljava/lang/String;

    const-string v4, "vs"

    aput-object v4, v0, v7

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/core/utils/aa;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    :goto_0
    new-array v0, v5, [Ljava/lang/String;

    const-string v10, "sd"

    aput-object v10, v0, v7

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/core/utils/aa;->a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    :goto_1
    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;-><init>(IIIZZ)V

    invoke-virtual {v6, v8, v9, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/an;->a(IILcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;)Lcom/google/android/apps/youtube/datalib/legacy/model/an;

    :cond_3
    return-void

    :cond_4
    move v5, v7

    goto :goto_1

    :cond_5
    move v4, v5

    goto :goto_0
.end method
