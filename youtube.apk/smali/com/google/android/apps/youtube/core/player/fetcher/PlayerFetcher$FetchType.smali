.class public final enum Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

.field public static final enum BOTH:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

.field public static final enum GDATA_ONLY:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

.field public static final enum PLAYER_SERVICE_ONLY:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    const-string v1, "PLAYER_SERVICE_ONLY"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->PLAYER_SERVICE_ONLY:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    const-string v1, "GDATA_ONLY"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->GDATA_ONLY:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    const-string v1, "BOTH"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->BOTH:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->PLAYER_SERVICE_ONLY:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->GDATA_ONLY:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->BOTH:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->$VALUES:[Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->$VALUES:[Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    return-object v0
.end method


# virtual methods
.method final shouldRequestGData()Z
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->GDATA_ONLY:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->BOTH:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final shouldRequestPlayerService()Z
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->PLAYER_SERVICE_ONLY:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;->BOTH:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher$FetchType;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
