.class public final Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/notification/i;


# static fields
.field private static a:Lcom/google/android/apps/youtube/core/player/notification/j;


# instance fields
.field private final b:Lcom/google/android/apps/youtube/core/player/notification/j;

.field private final c:Lcom/google/android/apps/youtube/core/player/notification/c;

.field private final d:Landroid/media/AudioManager;

.field private final e:Landroid/media/RemoteControlClient;

.field private final f:Landroid/content/ComponentName;

.field private g:Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;

.field private h:Z

.field private i:Landroid/graphics/Bitmap;

.field private j:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/notification/j;Lcom/google/android/apps/youtube/core/player/notification/c;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->b:Lcom/google/android/apps/youtube/core/player/notification/j;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Lcom/google/android/apps/youtube/core/player/notification/c;

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->d:Landroid/media/AudioManager;

    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14$RemoteControlIntentReceiver;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->f:Landroid/content/ComponentName;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_BUTTON"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->f:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/media/RemoteControlClient;

    invoke-direct {v1, v0}, Landroid/media/RemoteControlClient;-><init>(Landroid/app/PendingIntent;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Landroid/media/RemoteControlClient;

    sget v0, Lcom/google/android/exoplayer/e/k;->a:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Landroid/media/RemoteControlClient;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/notification/a;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/player/notification/a;-><init>(Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;)V

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->setPlaybackPositionUpdateListener(Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;)Lcom/google/android/apps/youtube/core/player/notification/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->b:Lcom/google/android/apps/youtube/core/player/notification/j;

    return-object v0
.end method

.method static synthetic b()Lcom/google/android/apps/youtube/core/player/notification/j;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->a:Lcom/google/android/apps/youtube/core/player/notification/j;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v2}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Landroid/media/RemoteControlClient;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/RemoteControlClient;->editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/RemoteControlClient$MetadataEditor;->apply()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->d:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->f:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->d:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->unregisterRemoteControlClient(Landroid/media/RemoteControlClient;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Lcom/google/android/apps/youtube/core/player/notification/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Lcom/google/android/apps/youtube/core/player/notification/c;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Landroid/media/RemoteControlClient;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/notification/c;->b(Landroid/media/RemoteControlClient;)V

    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->h:Z

    :cond_1
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->a:Lcom/google/android/apps/youtube/core/player/notification/j;

    return-void
.end method

.method public final a(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->g:Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, p1}, Landroid/media/RemoteControlClient;->setOnGetPlaybackPositionListener(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;)V
    .locals 10

    const/16 v9, 0x12

    const/16 v1, 0x8

    const/16 v3, 0x9

    const/4 v4, 0x0

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->h:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->d:Landroid/media/AudioManager;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->f:Landroid/content/ComponentName;

    invoke-virtual {v0, v5}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->d:Landroid/media/AudioManager;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v5}, Landroid/media/AudioManager;->registerRemoteControlClient(Landroid/media/RemoteControlClient;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Lcom/google/android/apps/youtube/core/player/notification/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Lcom/google/android/apps/youtube/core/player/notification/c;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Landroid/media/RemoteControlClient;

    invoke-interface {v0, v5}, Lcom/google/android/apps/youtube/core/player/notification/c;->a(Landroid/media/RemoteControlClient;)V

    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->h:Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->b:Lcom/google/android/apps/youtube/core/player/notification/j;

    sput-object v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->a:Lcom/google/android/apps/youtube/core/player/notification/j;

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->c:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    sget-object v5, Lcom/google/android/apps/youtube/core/player/notification/b;->a:[I

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    move v0, v2

    :goto_0
    sget v5, Lcom/google/android/exoplayer/e/k;->a:I

    if-lt v5, v9, :cond_7

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->g:Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Landroid/media/RemoteControlClient;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->g:Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;

    invoke-interface {v6}, Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;->onGetPlaybackPosition()J

    move-result-wide v6

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v5, v0, v6, v7, v8}, Landroid/media/RemoteControlClient;->setPlaybackState(IJF)V

    :goto_1
    iget-boolean v0, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->d:Z

    iget-boolean v5, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->e:Z

    iget-boolean v6, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->h:Z

    if-eqz v0, :cond_a

    move v0, v3

    :goto_2
    if-eqz v5, :cond_2

    or-int/lit16 v0, v0, 0x80

    :cond_2
    sget v1, Lcom/google/android/exoplayer/e/k;->a:I

    if-lt v1, v9, :cond_3

    if-eqz v6, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->g:Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;

    if-eqz v1, :cond_3

    or-int/lit16 v0, v0, 0x100

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Landroid/media/RemoteControlClient;

    invoke-virtual {v1, v0}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->b:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->f:Landroid/graphics/Bitmap;

    iget-wide v6, p1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo;->g:J

    if-nez v5, :cond_8

    move v0, v2

    :goto_3
    iget-object v8, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Landroid/media/RemoteControlClient;

    invoke-virtual {v8, v0}, Landroid/media/RemoteControlClient;->editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v0

    const/4 v8, 0x7

    invoke-virtual {v0, v8, v1}, Landroid/media/RemoteControlClient$MetadataEditor;->putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->j:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_4
    :goto_4
    if-eqz v5, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    if-eqz v2, :cond_6

    :cond_5
    iput-object v5, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    invoke-virtual {v5, v1, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->j:Landroid/graphics/Bitmap;

    const/16 v1, 0x64

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->j:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Landroid/media/RemoteControlClient$MetadataEditor;->putBitmap(ILandroid/graphics/Bitmap;)Landroid/media/RemoteControlClient$MetadataEditor;

    :cond_6
    invoke-virtual {v0, v3, v6, v7}, Landroid/media/RemoteControlClient$MetadataEditor;->putLong(IJ)Landroid/media/RemoteControlClient$MetadataEditor;

    invoke-virtual {v0}, Landroid/media/RemoteControlClient$MetadataEditor;->apply()V

    return-void

    :pswitch_0
    move v0, v1

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_3
    move v0, v2

    goto :goto_0

    :pswitch_4
    move v0, v3

    goto/16 :goto_0

    :cond_7
    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->e:Landroid/media/RemoteControlClient;

    invoke-virtual {v5, v0}, Landroid/media/RemoteControlClient;->setPlaybackState(I)V

    goto :goto_1

    :cond_8
    move v0, v4

    goto :goto_3

    :cond_9
    move v2, v4

    goto :goto_4

    :cond_a
    move v0, v1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    return-void
.end method
