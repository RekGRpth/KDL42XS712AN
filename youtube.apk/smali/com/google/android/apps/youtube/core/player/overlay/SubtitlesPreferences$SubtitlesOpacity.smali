.class public final enum Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

.field public static final enum PERCENT_100:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

.field public static final enum PERCENT_25:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

.field public static final enum PERCENT_50:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

.field public static final enum PERCENT_75:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

.field private static opacityEntryStrings:[Ljava/lang/String;

.field private static opacityValueStrings:[Ljava/lang/String;


# instance fields
.field private opacityValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    const-string v1, "PERCENT_25"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->PERCENT_25:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    const-string v1, "PERCENT_50"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->PERCENT_50:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    const-string v1, "PERCENT_75"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->PERCENT_75:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    const-string v1, "PERCENT_100"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->PERCENT_100:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->PERCENT_25:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->PERCENT_50:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->PERCENT_75:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->PERCENT_100:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->$VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->opacityValue:I

    return-void
.end method

.method public static getDefaultBackgroundOpacityValue()I
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    move-result-object v0

    const/4 v1, 0x3

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->opacityValue:I

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->getHexAlphaValue(I)I

    move-result v0

    return v0
.end method

.method public static getDefaultTextOpacityValue()I
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    move-result-object v0

    const/4 v1, 0x3

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->opacityValue:I

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->getHexAlphaValue(I)I

    move-result v0

    return v0
.end method

.method public static getDefaultWindowOpacityValue()I
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    move-result-object v0

    const/4 v1, 0x3

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->opacityValue:I

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->getHexAlphaValue(I)I

    move-result v0

    return v0
.end method

.method private static getHexAlphaValue(I)I
    .locals 1

    mul-int/lit16 v0, p0, 0xff

    div-int/lit8 v0, v0, 0x64

    return v0
.end method

.method public static getOpacityEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;
    .locals 7

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->opacityEntryStrings:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    move-result-object v2

    array-length v0, v2

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->opacityEntryStrings:[Ljava/lang/String;

    move v0, v1

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    sget-object v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->opacityEntryStrings:[Ljava/lang/String;

    sget v4, Lcom/google/android/youtube/p;->ep:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aget-object v6, v2, v0

    iget v6, v6, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->opacityValue:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {p0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->opacityEntryStrings:[Ljava/lang/String;

    return-object v0
.end method

.method public static getOpacityValueStrings()[Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->opacityValueStrings:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    move-result-object v1

    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->opacityValueStrings:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget-object v2, v1, v0

    iget v2, v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->opacityValue:I

    mul-int/lit16 v2, v2, 0xff

    div-int/lit8 v2, v2, 0x64

    sget-object v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->opacityValueStrings:[Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->opacityValueStrings:[Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->$VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;

    return-object v0
.end method
