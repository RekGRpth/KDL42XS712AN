.class final Lcom/google/android/apps/youtube/core/player/fetcher/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:[B

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:I

.field final synthetic f:I

.field final synthetic g:Lcom/google/android/apps/youtube/common/a/b;

.field final synthetic h:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->h:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->b:[B

    iput-object p4, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->d:Ljava/lang/String;

    iput p6, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->e:I

    iput p7, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->f:I

    iput-object p8, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->g:Lcom/google/android/apps/youtube/common/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->h:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->b:[B

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->d:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->e:I

    iget v6, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->f:I

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;II)Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->h:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->g:Lcom/google/android/apps/youtube/common/a/b;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a(Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->h:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->g:Lcom/google/android/apps/youtube/common/a/b;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a(Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/common/a/b;Ljava/lang/Exception;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->h:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/fetcher/a;->g:Lcom/google/android/apps/youtube/common/a/b;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a(Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/common/a/b;Ljava/lang/Exception;)V

    goto :goto_0
.end method
