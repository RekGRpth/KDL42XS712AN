.class public final Lcom/google/android/apps/youtube/core/async/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/async/af;

.field private final b:Lcom/google/android/apps/youtube/core/async/af;

.field private volatile c:Z


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/async/af;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/ak;->a:Lcom/google/android/apps/youtube/core/async/af;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/ak;->b:Lcom/google/android/apps/youtube/core/async/af;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/async/ak;)Lcom/google/android/apps/youtube/core/async/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/ak;->b:Lcom/google/android/apps/youtube/core/async/af;

    return-object v0
.end method

.method public static a(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/common/e/b;)Lcom/google/android/apps/youtube/core/async/af;
    .locals 4

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/ac;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/converter/http/ac;-><init>()V

    new-instance v1, Lcom/google/android/apps/youtube/core/async/al;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/core/async/al;-><init>(B)V

    new-instance v2, Lcom/google/android/apps/youtube/core/async/ak;

    new-instance v3, Lcom/google/android/apps/youtube/core/async/u;

    invoke-direct {v3, p0, v0, v0}, Lcom/google/android/apps/youtube/core/async/u;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/async/u;

    invoke-direct {v0, p0, v1, v1}, Lcom/google/android/apps/youtube/core/async/u;-><init>(Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)V

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/youtube/core/async/ak;-><init>(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/async/af;)V

    invoke-static {p1, v2}, Lcom/google/android/apps/youtube/core/async/b;->a(Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/common/cache/b;

    const/16 v2, 0x64

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/common/cache/b;-><init>(I)V

    const-wide/32 v2, 0x1b7740

    invoke-static {v1, v0, p2, v2, v3}, Lcom/google/android/apps/youtube/core/async/an;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/common/e/b;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/async/ak;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/async/ak;->c:Z

    return v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 3

    check-cast p1, Landroid/net/Uri;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/async/ak;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/ak;->a:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v1, Lcom/google/android/apps/youtube/core/async/am;

    const/4 v2, 0x1

    invoke-direct {v1, p0, p2, v2}, Lcom/google/android/apps/youtube/core/async/am;-><init>(Lcom/google/android/apps/youtube/core/async/ak;Lcom/google/android/apps/youtube/common/a/b;Z)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/ak;->b:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v1, Lcom/google/android/apps/youtube/core/async/am;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p2, v2}, Lcom/google/android/apps/youtube/core/async/am;-><init>(Lcom/google/android/apps/youtube/core/async/ak;Lcom/google/android/apps/youtube/common/a/b;Z)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method
