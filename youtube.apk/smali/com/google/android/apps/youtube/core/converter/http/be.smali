.class final Lcom/google/android/apps/youtube/core/converter/http/be;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;)V
    .locals 4

    const-string v0, "type"

    invoke-interface {p2, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/youtube/core/converter/http/bb;->a()Ljava/util/Map;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/http/bb;->a(Ljava/lang/String;Ljava/util/Map;I)I

    move-result v1

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/f;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/f;

    const-string v2, "base_url"

    invoke-interface {p2, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/e/p;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardTrackingEvent;

    invoke-direct {v3, v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardTrackingEvent;-><init>(ILandroid/net/Uri;)V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/f;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardTrackingEvent;)Lcom/google/android/apps/youtube/datalib/legacy/model/f;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "Badly formed event\'s uri - ignoring event"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_0
.end method
