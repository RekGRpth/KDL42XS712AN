.class public final Lcom/google/android/apps/youtube/core/player/sequencer/ae;
.super Lcom/google/android/apps/youtube/core/player/sequencer/s;
.source "SourceFile"


# instance fields
.field private final r:Lcom/google/android/apps/youtube/core/player/fetcher/e;

.field private final s:Ljava/util/concurrent/Executor;

.field private t:Lcom/google/android/apps/youtube/datalib/a/k;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/player/state/WatchNextVideoIdsSequencerState;)V
    .locals 9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p6

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/youtube/core/player/sequencer/s;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;)V

    move-object/from16 v0, p9

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/player/state/WatchNextVideoIdsSequencerState;->currentWatchNextResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/player/fetcher/e;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->r:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->s:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->i()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/core/aw;Ljava/util/List;I[BLjava/lang/String;)V
    .locals 11

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p6

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move/from16 v8, p10

    move-object/from16 v9, p11

    move-object/from16 v10, p12

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/youtube/core/player/sequencer/s;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/aw;Ljava/util/List;I[BLjava/lang/String;)V

    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/fetcher/e;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->r:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    invoke-static/range {p5 .. p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->s:Ljava/util/concurrent/Executor;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->i()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/sequencer/ae;)Lcom/google/android/apps/youtube/datalib/a/k;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->t:Lcom/google/android/apps/youtube/datalib/a/k;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/sequencer/ae;Landroid/os/Handler;Lcom/google/android/apps/youtube/datalib/a/k;)V
    .locals 2

    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/a/k;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/sequencer/ag;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/ag;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/ae;Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;)V

    invoke-virtual {p1, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/core/player/sequencer/aj;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/aj;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/ae;Ljava/lang/Exception;)V

    invoke-virtual {p1, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/core/player/sequencer/aj;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/aj;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/ae;Ljava/lang/Exception;)V

    invoke-virtual {p1, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/sequencer/ae;)Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->s:Ljava/util/concurrent/Executor;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->o:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->o:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->o:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->t:Lcom/google/android/apps/youtube/datalib/a/k;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->t:Lcom/google/android/apps/youtube/datalib/a/k;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/a/k;->cancel(Z)Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->NEW:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    if-eq v0, v1, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->NEW:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    goto :goto_0
.end method

.method protected final b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final c(I)V
    .locals 9

    const/4 v2, 0x0

    const/4 v5, -0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->l:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->q:I

    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->q:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->l:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_1

    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/r;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/player/event/r;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/player/sequencer/ah;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/youtube/core/player/sequencer/ah;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/ae;B)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->o:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->a:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->l:[Ljava/lang/String;

    aget-object v1, v1, p1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->m:[B

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->n:Ljava/lang/String;

    const-string v4, ""

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->o:Lcom/google/android/apps/youtube/common/a/d;

    move v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/common/a/b;)V

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/a/k;->a()Lcom/google/android/apps/youtube/datalib/a/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->t:Lcom/google/android/apps/youtube/datalib/a/k;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->r:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->l:[Ljava/lang/String;

    aget-object v3, v0, p1

    const-string v4, ""

    const-string v6, ""

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->m:[B

    iget-object v8, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->t:Lcom/google/android/apps/youtube/datalib/a/k;

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/apps/youtube/core/player/fetcher/e;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[BLcom/google/android/apps/youtube/datalib/a/l;)V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_LOADING:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->l:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->q:I

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->p:I

    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->SEQUENCE_EMPTY:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    goto :goto_0
.end method

.method protected final g()V
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isIn([Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    if-ne v2, v3, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v3, Lcom/google/android/apps/youtube/core/player/event/v;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-direct {v3, v4, v0, v1}, Lcom/google/android/apps/youtube/core/player/event/v;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final p_()Z
    .locals 7

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/event/r;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/player/event/r;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->t:Lcom/google/android/apps/youtube/datalib/a/k;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->t:Lcom/google/android/apps/youtube/datalib/a/k;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/datalib/a/k;->cancel(Z)Z

    :cond_0
    invoke-static {}, Lcom/google/android/apps/youtube/datalib/a/k;->a()Lcom/google/android/apps/youtube/datalib/a/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->t:Lcom/google/android/apps/youtube/datalib/a/k;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->r:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->l:[Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->p:I

    aget-object v1, v1, v2

    const-string v2, ""

    const/4 v3, -0x1

    const-string v4, ""

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->m:[B

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->t:Lcom/google/android/apps/youtube/datalib/a/k;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/player/fetcher/e;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[BLcom/google/android/apps/youtube/datalib/a/l;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->s:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/sequencer/af;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/af;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/ae;Landroid/os/Handler;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final r()Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerState;
    .locals 9

    new-instance v0, Lcom/google/android/apps/youtube/core/player/state/WatchNextVideoIdsSequencerState;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->l:[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->m:[B

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->n:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->p:I

    iget v5, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->q:I

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    iget-boolean v8, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ae;->j:Z

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/core/player/state/WatchNextVideoIdsSequencerState;-><init>([Ljava/lang/String;[BLjava/lang/String;IILcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;Z)V

    return-object v0
.end method
