.class public final enum Lcom/google/android/apps/youtube/core/utils/Typefaces;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/utils/Typefaces;

.field public static final enum ROBOTO_LIGHT:Lcom/google/android/apps/youtube/core/utils/Typefaces;

.field public static final enum ROBOTO_REGULAR:Lcom/google/android/apps/youtube/core/utils/Typefaces;

.field private static final typefaceMap:Ljava/util/Map;


# instance fields
.field private final fontName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/utils/Typefaces;

    const-string v1, "ROBOTO_LIGHT"

    const-string v2, "Roboto-Light.ttf"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/youtube/core/utils/Typefaces;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/utils/Typefaces;->ROBOTO_LIGHT:Lcom/google/android/apps/youtube/core/utils/Typefaces;

    new-instance v0, Lcom/google/android/apps/youtube/core/utils/Typefaces;

    const-string v1, "ROBOTO_REGULAR"

    const-string v2, "Roboto-Regular.ttf"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/youtube/core/utils/Typefaces;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/utils/Typefaces;->ROBOTO_REGULAR:Lcom/google/android/apps/youtube/core/utils/Typefaces;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/utils/Typefaces;

    sget-object v1, Lcom/google/android/apps/youtube/core/utils/Typefaces;->ROBOTO_LIGHT:Lcom/google/android/apps/youtube/core/utils/Typefaces;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/core/utils/Typefaces;->ROBOTO_REGULAR:Lcom/google/android/apps/youtube/core/utils/Typefaces;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/youtube/core/utils/Typefaces;->$VALUES:[Lcom/google/android/apps/youtube/core/utils/Typefaces;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/utils/Typefaces;->typefaceMap:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/utils/Typefaces;->fontName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/utils/Typefaces;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/utils/Typefaces;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/utils/Typefaces;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/utils/Typefaces;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/utils/Typefaces;->$VALUES:[Lcom/google/android/apps/youtube/core/utils/Typefaces;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/utils/Typefaces;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/utils/Typefaces;

    return-object v0
.end method


# virtual methods
.method public final toTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;
    .locals 3

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->a()V

    sget-object v0, Lcom/google/android/apps/youtube/core/utils/Typefaces;->typefaceMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/utils/Typefaces;->fontName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/utils/Typefaces;->fontName:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/utils/Typefaces;->typefaceMap:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/utils/Typefaces;->fontName:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-object v0, Lcom/google/android/apps/youtube/core/utils/Typefaces;->typefaceMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/utils/Typefaces;->fontName:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    return-object v0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/youtube/core/utils/Typefaces;->typefaceMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/utils/Typefaces;->fontName:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
