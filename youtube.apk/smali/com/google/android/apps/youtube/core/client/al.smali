.class public final Lcom/google/android/apps/youtube/core/client/al;
.super Lcom/google/android/apps/youtube/core/client/m;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/client/bz;


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private final h:Lcom/google/android/apps/youtube/core/async/af;

.field private final i:Ljava/lang/String;

.field private final j:Lcom/google/android/apps/youtube/core/identity/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "https://www.googleapis.com/plus/v1whitelisted/people"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/core/client/al;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/lang/String;Lcom/google/android/apps/youtube/common/e/b;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/apps/youtube/core/identity/k;)V
    .locals 5

    invoke-direct {p0, p1, p4, p2, p3}, Lcom/google/android/apps/youtube/core/client/m;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lcom/google/android/apps/youtube/common/e/b;)V

    iput-object p5, p0, Lcom/google/android/apps/youtube/core/client/al;->i:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/youtube/core/client/al;->j:Lcom/google/android/apps/youtube/core/identity/k;

    const/16 v0, 0x1f4

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/client/al;->a(I)Lcom/google/android/apps/youtube/common/cache/b;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/http/by;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/converter/http/by;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/al;->j:Lcom/google/android/apps/youtube/core/identity/k;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/google/android/apps/youtube/core/converter/http/bw;

    sget-object v4, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    invoke-direct {v3, v4, v2}, Lcom/google/android/apps/youtube/core/converter/http/bw;-><init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;Ljava/util/List;)V

    invoke-virtual {p0, v3, v1}, Lcom/google/android/apps/youtube/core/client/al;->a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/client/al;->a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/core/async/i;

    invoke-direct {v2, v1}, Lcom/google/android/apps/youtube/core/async/i;-><init>(Lcom/google/android/apps/youtube/core/async/af;)V

    const-wide/32 v3, 0x6ddd00

    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/google/android/apps/youtube/core/client/al;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/al;->h:Lcom/google/android/apps/youtube/core/async/af;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/core/client/al;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "me"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "fields"

    const-string v2, "displayName,id,image,url"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/async/w;

    invoke-direct {v1, v0, v3, v3}, Lcom/google/android/apps/youtube/core/async/w;-><init>(Landroid/net/Uri;Ljava/util/Map;[B)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/al;->h:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
