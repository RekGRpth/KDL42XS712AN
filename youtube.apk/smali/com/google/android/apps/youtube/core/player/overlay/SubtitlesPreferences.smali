.class public final Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/SharedPreferences;

.field private c:Lcom/google/android/apps/youtube/core/player/overlay/bi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->b:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a:Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    return-void
.end method

.method public static a(Landroid/content/Context;FII)F
    .locals 4

    const/high16 v0, 0x41500000    # 13.0f

    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    const/high16 v3, 0x3d800000    # 0.0625f

    int-to-float v1, v1

    iget v2, v2, Landroid/util/DisplayMetrics;->scaledDensity:F

    div-float/2addr v1, v2

    mul-float/2addr v1, v3

    cmpg-float v2, v1, v0

    if-gez v2, :cond_0

    :goto_0
    mul-float/2addr v0, p1

    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static a(II)I
    .locals 2

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->NONE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    # getter for: Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->access$300(Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;)I

    move-result v0

    if-eq p0, v0, :cond_0

    const v0, 0xffffff

    and-int/2addr v0, p0

    shl-int/lit8 v1, p1, 0x18

    or-int p0, v0, v1

    :cond_0
    return p0
.end method

.method private static a(Landroid/content/SharedPreferences;Ljava/lang/String;I)I
    .locals 1

    const/4 v0, 0x0

    invoke-interface {p0, p1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return p2

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    goto :goto_0
.end method

.method public static a(Landroid/content/SharedPreferences;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;
    .locals 7

    const-string v0, "subtitles_style"

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->getDefaultStyleValue()I

    move-result v0

    :goto_0
    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->CUSTOM:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    # getter for: Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleValue:I
    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->access$200(Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;)I

    move-result v1

    if-ne v0, v1, :cond_1

    const-string v0, "subtitles_background_color"

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getDefaultBackgroundColorValue()I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a(Landroid/content/SharedPreferences;Ljava/lang/String;I)I

    move-result v0

    const-string v1, "subtitles_background_opacity"

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->getDefaultBackgroundOpacityValue()I

    move-result v2

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a(Landroid/content/SharedPreferences;Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a(II)I

    move-result v1

    const-string v0, "subtitles_window_color"

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getDefaultWindowColorValue()I

    move-result v2

    invoke-static {p0, v0, v2}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a(Landroid/content/SharedPreferences;Ljava/lang/String;I)I

    move-result v0

    const-string v2, "subtitles_window_opacity"

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->getDefaultWindowOpacityValue()I

    move-result v3

    invoke-static {p0, v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a(Landroid/content/SharedPreferences;Ljava/lang/String;I)I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a(II)I

    move-result v2

    const-string v0, "subtitles_text_color"

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getDefaultTextColorValue()I

    move-result v3

    invoke-static {p0, v0, v3}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a(Landroid/content/SharedPreferences;Ljava/lang/String;I)I

    move-result v0

    const-string v3, "subtitles_text_opacity"

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->getDefaultTextOpacityValue()I

    move-result v4

    invoke-static {p0, v3, v4}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a(Landroid/content/SharedPreferences;Ljava/lang/String;I)I

    move-result v3

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a(II)I

    move-result v5

    const-string v0, "subtitles_edge_type"

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->getDefaultEdgeTypeValue()I

    move-result v3

    invoke-static {p0, v0, v3}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a(Landroid/content/SharedPreferences;Ljava/lang/String;I)I

    move-result v4

    const-string v0, "subtitles_edge_color"

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getDefaultEdgeColorValue()I

    move-result v3

    invoke-static {p0, v0, v3}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a(Landroid/content/SharedPreferences;Ljava/lang/String;I)I

    move-result v3

    const-string v0, "subtitles_font"

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->getDefaultFontValue()I

    move-result v6

    invoke-static {p0, v0, v6}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a(Landroid/content/SharedPreferences;Ljava/lang/String;I)I

    move-result v6

    :goto_1
    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;-><init>(IIIIII)V

    return-object v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->WHITE_ON_BLACK:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    # getter for: Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleValue:I
    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->access$200(Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;)I

    move-result v1

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->BLACK:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    # getter for: Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->access$300(Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;)I

    move-result v1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->WHITE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    # getter for: Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->access$300(Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;)I

    move-result v0

    :goto_2
    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getDefaultWindowColorValue()I

    move-result v2

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->getDefaultEdgeTypeValue()I

    move-result v4

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getDefaultEdgeColorValue()I

    move-result v3

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->getDefaultFontValue()I

    move-result v6

    move v5, v0

    goto :goto_1

    :cond_2
    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->BLACK_ON_WHITE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    # getter for: Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleValue:I
    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->access$200(Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;)I

    move-result v1

    if-ne v0, v1, :cond_3

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->WHITE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    # getter for: Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->access$300(Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;)I

    move-result v1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->BLACK:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    # getter for: Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->access$300(Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;)I

    move-result v0

    goto :goto_2

    :cond_3
    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->YELLOW_ON_BLACK:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    # getter for: Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleValue:I
    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->access$200(Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;)I

    move-result v1

    if-ne v0, v1, :cond_4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->BLACK:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    # getter for: Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->access$300(Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;)I

    move-result v1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->YELLOW:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    # getter for: Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->access$300(Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;)I

    move-result v0

    goto :goto_2

    :cond_4
    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->YELLOW_ON_BLUE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    # getter for: Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleValue:I
    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->access$200(Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;)I

    move-result v1

    if-ne v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->BLUE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    # getter for: Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->access$300(Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;)I

    move-result v1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->YELLOW:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    # getter for: Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->access$300(Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;)I

    move-result v0

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static b(Landroid/content/SharedPreferences;)F
    .locals 2

    const-string v0, "subtitles_scale"

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->getDefaultScaleValue()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    goto :goto_0
.end method

.method private c()V
    .locals 10

    const/4 v9, 0x1

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/util/HashSet;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v3, "fonts"

    invoke-virtual {v0, v3}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v3, "Project is missing required font %s. Please copy font assets found at //depot/google3/java/com/google/android/apps/youtube/app/assets/fonts/ into ./assets/fonts in the root directory of your Android project."

    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, "MonoSerif-Regular.ttf"

    aput-object v0, v4, v1

    const-string v0, "ComingSoon-Regular.ttf"

    aput-object v0, v4, v9

    const/4 v0, 0x2

    const-string v5, "DancingScript-Regular.ttf"

    aput-object v5, v4, v0

    const/4 v0, 0x3

    const-string v5, "CarroisGothicSC-Regular.ttf"

    aput-object v5, v4, v0

    array-length v5, v4

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v6, v4, v0

    invoke-interface {v2, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    new-array v8, v9, [Ljava/lang/Object;

    aput-object v6, v8, v1

    invoke-static {v3, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(ZLjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Project is missing required fonts. Please copy font assets found at //depot/google3/java/com/google/android/apps/youtube/app/assets/fonts/ into ./assets/fonts in the root directory of your Android project."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()F
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->b:Landroid/content/SharedPreferences;

    const-string v1, "subtitles_scale"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->getDefaultScaleValue()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/overlay/bi;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->c:Lcom/google/android/apps/youtube/core/player/overlay/bi;

    return-void
.end method

.method public final b()Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->b:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a(Landroid/content/SharedPreferences;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;

    move-result-object v0

    return-object v0
.end method

.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->c:Lcom/google/android/apps/youtube/core/player/overlay/bi;

    if-eqz v0, :cond_0

    const-string v0, "subtitles_scale"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->c:Lcom/google/android/apps/youtube/core/player/overlay/bi;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->b(Landroid/content/SharedPreferences;)F

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/bi;->a(F)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "subtitles_style"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "subtitles_font"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "subtitles_text_color"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "subtitles_text_opacity"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "subtitles_edge_type"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "subtitles_edge_color"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "subtitles_background_color"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "subtitles_background_opacity"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "subtitles_window_color"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "subtitles_window_opacity"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->c:Lcom/google/android/apps/youtube/core/player/overlay/bi;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a(Landroid/content/SharedPreferences;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/bi;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;)V

    goto :goto_0
.end method
