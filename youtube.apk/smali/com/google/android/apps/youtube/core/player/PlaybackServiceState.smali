.class public Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final directorState:Lcom/google/android/apps/youtube/core/player/DirectorSavedState;

.field public final manageAudioFocus:Z

.field public final playbackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

.field public final sequencerState:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/ak;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/player/ak;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->playbackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/DirectorSavedState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->directorState:Lcom/google/android/apps/youtube/core/player/DirectorSavedState;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->sequencerState:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerState;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->manageAudioFocus:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerState;Lcom/google/android/apps/youtube/core/player/DirectorSavedState;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->playbackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->directorState:Lcom/google/android/apps/youtube/core/player/DirectorSavedState;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->sequencerState:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerState;

    iput-boolean p4, p0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->manageAudioFocus:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->playbackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->directorState:Lcom/google/android/apps/youtube/core/player/DirectorSavedState;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->sequencerState:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerState;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->manageAudioFocus:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
