.class public final Lcom/google/android/apps/youtube/core/offline/store/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->CANNOT_ADD:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->CANNOT_ADD:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(J)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/util/Pair;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {p2, v2, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/v;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->CANNOT_ADD:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/s;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Ljava/lang/String;J)Lcom/google/android/apps/youtube/datalib/legacy/model/u;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/t;Lcom/google/android/apps/youtube/datalib/legacy/model/t;)V

    return-object v0
.end method

.method public final b(Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    const/4 v0, 0x0

    invoke-interface {p2, v0, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/v;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final d()Lcom/google/android/apps/youtube/core/offline/store/i;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)Ljava/util/List;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Ljava/util/Collection;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final f()V
    .locals 0

    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final g()Ljava/util/Map;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final i()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final i(Ljava/lang/String;)Landroid/util/Pair;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final j(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final k(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final l(Ljava/lang/String;)Ljava/util/List;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final m(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final n(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final o(Ljava/lang/String;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final p(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->DELETED:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    return-object v0
.end method
