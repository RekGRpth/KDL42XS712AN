.class public final enum Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

.field public static final enum NAVIGATION:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

.field public static final enum PLAYER_CONTROL:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    const-string v1, "NAVIGATION"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->NAVIGATION:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    const-string v1, "PLAYER_CONTROL"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->PLAYER_CONTROL:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->NAVIGATION:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->PLAYER_CONTROL:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->$VALUES:[Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->$VALUES:[Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    return-object v0
.end method
