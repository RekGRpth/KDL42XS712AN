.class public final Lcom/google/android/apps/youtube/core/client/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/client/AdsClient;

.field private final b:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/client/AdsClient;Ljava/util/concurrent/Executor;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/AdsClient;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/e;->a:Lcom/google/android/apps/youtube/core/client/AdsClient;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/e;->b:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/client/e;)Lcom/google/android/apps/youtube/core/client/AdsClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/e;->a:Lcom/google/android/apps/youtube/core/client/AdsClient;

    return-object v0
.end method


# virtual methods
.method protected final a(Ljava/util/List;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->firstPrerollAdBreak(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/e;->a:Lcom/google/android/apps/youtube/core/client/AdsClient;

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/apps/youtube/core/client/AdsClient;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/e;->a:Lcom/google/android/apps/youtube/core/client/AdsClient;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/client/AdsClient;->a(I)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/e;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/client/f;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/client/f;-><init>(Lcom/google/android/apps/youtube/core/client/e;Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/e;->a:Lcom/google/android/apps/youtube/core/client/AdsClient;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/client/AdsClient;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/e;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/client/g;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/client/g;-><init>(Lcom/google/android/apps/youtube/core/client/e;Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
