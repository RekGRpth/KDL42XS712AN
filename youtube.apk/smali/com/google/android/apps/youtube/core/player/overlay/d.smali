.class final Lcom/google/android/apps/youtube/core/player/overlay/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/overlay/c;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/c;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/d;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/c;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/d;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/c;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    check-cast p1, Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Couldn\'t retrieve thumbnail from [uri="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/d;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/c;->b(Lcom/google/android/apps/youtube/core/player/overlay/c;)Lcom/google/android/apps/youtube/core/player/overlay/a;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/youtube/core/player/overlay/a;->setChannel(Landroid/graphics/Bitmap;)V

    return-void
.end method
