.class public Lcom/google/android/apps/youtube/core/transfer/UploadService;
.super Lcom/google/android/apps/youtube/core/transfer/TransferService;
.source "SourceFile"


# instance fields
.field private b:Ljava/util/concurrent/Executor;

.field private c:Lorg/apache/http/client/HttpClient;

.field private d:Lcom/google/android/apps/youtube/core/client/bc;

.field private e:Lcom/google/android/apps/youtube/core/identity/ak;

.field private f:Lcom/google/android/apps/youtube/core/identity/l;

.field private g:Lcom/google/android/apps/youtube/core/converter/http/fo;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/transfer/UploadService;

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/transfer/h;)Lcom/google/android/apps/youtube/core/utils/w;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/transfer/UploadService;

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/apps/youtube/core/transfer/h;)Lcom/google/android/apps/youtube/core/utils/w;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;Lcom/google/android/apps/youtube/core/transfer/m;)Lcom/google/android/apps/youtube/core/transfer/l;
    .locals 11

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/aa;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->b:Ljava/util/concurrent/Executor;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->c:Lorg/apache/http/client/HttpClient;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->d:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->e:Lcom/google/android/apps/youtube/core/identity/ak;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->f:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/UploadService;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/BaseApplication;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/BaseApplication;->D()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->g:Lcom/google/android/apps/youtube/core/converter/http/fo;

    move-object v1, p0

    move-object v9, p1

    move-object v10, p2

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/youtube/core/transfer/aa;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/converter/http/fo;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;Lcom/google/android/apps/youtube/core/transfer/m;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Ljava/lang/Runnable;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final d()Ljava/lang/String;
    .locals 1

    const-string v0, "uploads.db"

    return-object v0
.end method

.method protected final e()Ljava/lang/String;
    .locals 1

    const-string v0, "upload_policy"

    return-object v0
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final g()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final i()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/transfer/UploadService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/BaseApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/BaseApplication;->y()Lcom/google/android/apps/youtube/core/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/a;->aJ()Ljava/util/concurrent/Executor;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->b:Ljava/util/concurrent/Executor;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/a;->aL()Lorg/apache/http/client/HttpClient;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->c:Lorg/apache/http/client/HttpClient;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/a;->aV()Lcom/google/android/apps/youtube/core/identity/ak;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->e:Lcom/google/android/apps/youtube/core/identity/ak;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/a;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->f:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/a;->aN()Lcom/google/android/apps/youtube/core/converter/n;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/fo;

    invoke-direct {v2, v1}, Lcom/google/android/apps/youtube/core/converter/http/fo;-><init>(Lcom/google/android/apps/youtube/core/converter/n;)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->g:Lcom/google/android/apps/youtube/core/converter/http/fo;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/BaseApplication;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/UploadService;->d:Lcom/google/android/apps/youtube/core/client/bc;

    return-void
.end method
