.class public final Lcom/google/android/apps/youtube/core/player/overlay/bo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/av;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/overlay/bm;

.field private final b:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

.field private final c:Lcom/google/android/apps/youtube/common/e/b;

.field private final d:Lcom/google/android/apps/youtube/core/player/au;

.field private final e:Lcom/google/android/apps/youtube/core/player/am;

.field private f:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

.field private g:Landroid/os/CountDownTimer;

.field private h:Lcom/google/android/apps/youtube/datalib/legacy/model/aw;

.field private i:Lcom/google/android/apps/youtube/core/player/at;

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;Lcom/google/android/apps/youtube/core/player/overlay/bm;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/player/au;Lcom/google/android/apps/youtube/core/player/am;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/bm;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->a:Lcom/google/android/apps/youtube/core/player/overlay/bm;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->c:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/au;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->d:Lcom/google/android/apps/youtube/core/player/au;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->b:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/am;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->e:Lcom/google/android/apps/youtube/core/player/am;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/bp;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/player/overlay/bp;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/bo;)V

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/core/player/overlay/bm;->setListener(Lcom/google/android/apps/youtube/core/player/overlay/bn;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bo;->c()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/bo;II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/aw;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->b:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/aw;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/aw;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->b:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(II)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bo;->e()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/bo;J)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->getQuestions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->getQuestions()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getDurationSeconds()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    sub-long/2addr v0, p1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->b:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(J)V

    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->a:Lcom/google/android/apps/youtube/core/player/overlay/bm;

    long-to-int v3, p1

    invoke-interface {v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/bm;->a(I)V

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->k:Z

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x1388

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->j:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->j:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->a:Lcom/google/android/apps/youtube/core/player/overlay/bm;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/bm;->d()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->b:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->k()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bo;->d()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/bo;[I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/aw;

    if-eqz v0, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    aget v2, p1, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/aw;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->a(Ljava/util/List;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->b:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->l()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->b:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->g()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->b:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/aw;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/aw;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bo;->e()V

    return-void
.end method

.method private static a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSurvey()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bo;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->a:Lcom/google/android/apps/youtube/core/player/overlay/bm;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/bm;->a()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->j:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    return-void
.end method

.method private d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/aw;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->b:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/aw;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/aw;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->b:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->g()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bo;->e()V

    return-void
.end method

.method private e()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->e:Lcom/google/android/apps/youtube/core/player/am;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/am;->a(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bo;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->a:Lcom/google/android/apps/youtube/core/player/overlay/bm;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/bm;->setVisible(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->i:Lcom/google/android/apps/youtube/core/player/at;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->i:Lcom/google/android/apps/youtube/core/player/at;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/at;->a()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->i:Lcom/google/android/apps/youtube/core/player/at;

    :cond_0
    iput-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    return-void
.end method

.method private f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->g:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->g:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->g:Landroid/os/CountDownTimer;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->i:Lcom/google/android/apps/youtube/core/player/at;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bo;->c()V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/at;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->i:Lcom/google/android/apps/youtube/core/player/at;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->getQuestion(I)Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;

    move-result-object v0

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->j:Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->a:Lcom/google/android/apps/youtube/core/player/overlay/bm;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getQuestion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getAnswers()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->isMultiSelectQuestion()Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/player/overlay/bm;->a(Ljava/lang/String;Ljava/util/List;Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->a:Lcom/google/android/apps/youtube/core/player/overlay/bm;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getDurationSeconds()I

    move-result v3

    int-to-long v3, v3

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/youtube/core/player/overlay/bm;->a(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->b:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->b(J)V

    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->c:Lcom/google/android/apps/youtube/common/e/b;

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;-><init>(Lcom/google/android/apps/youtube/common/e/b;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/aw;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getDurationSeconds()I

    move-result v0

    new-instance v1, Lcom/google/android/apps/youtube/core/player/overlay/bq;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v3, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3, v4, v0}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    long-to-int v0, v2

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/core/player/overlay/bq;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/bo;I)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->g:Landroid/os/CountDownTimer;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->g:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/aw;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->a:Lcom/google/android/apps/youtube/core/player/overlay/bm;

    invoke-interface {v0, v6}, Lcom/google/android/apps/youtube/core/player/overlay/bm;->setVisible(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->e:Lcom/google/android/apps/youtube/core/player/am;

    invoke-interface {v0, v6}, Lcom/google/android/apps/youtube/core/player/am;->a(Z)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bo;->d()V

    :cond_0
    return-void
.end method

.method public final handleVideoStageEvent(Lcom/google/android/apps/youtube/core/player/event/ac;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->NEW:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bo;->c()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->AD_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->e()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/bo;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSurvey()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v2

    if-eq v1, v2, :cond_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/overlay/bo;->c()V

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/bo;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isSkippable()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->k:Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSurvey()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/bo;->d:Lcom/google/android/apps/youtube/core/player/au;

    invoke-interface {v0, p0}, Lcom/google/android/apps/youtube/core/player/au;->a(Lcom/google/android/apps/youtube/core/player/av;)V

    goto :goto_0
.end method
