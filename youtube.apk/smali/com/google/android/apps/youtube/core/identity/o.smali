.class public final Lcom/google/android/apps/youtube/core/identity/o;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final b:Landroid/net/Uri;


# instance fields
.field protected final a:Lcom/google/android/apps/youtube/core/identity/y;

.field private final c:Lcom/google/android/apps/youtube/core/client/bc;

.field private final d:Lcom/google/android/apps/youtube/core/client/bz;

.field private final e:Lcom/google/android/apps/youtube/core/client/bj;

.field private final f:Lcom/google/android/apps/youtube/core/identity/b;

.field private final g:Lcom/google/android/apps/youtube/core/Analytics;

.field private final h:Landroid/content/SharedPreferences;

.field private final i:Ljava/util/concurrent/Executor;

.field private final j:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "http://m.youtube.com/create_channel"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/core/identity/o;->b:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bz;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/identity/b;Lcom/google/android/apps/youtube/core/Analytics;Landroid/content/SharedPreferences;Ljava/util/concurrent/Executor;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->c:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bz;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->d:Lcom/google/android/apps/youtube/core/client/bz;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->e:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->f:Lcom/google/android/apps/youtube/core/identity/b;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->g:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->h:Landroid/content/SharedPreferences;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->i:Ljava/util/concurrent/Executor;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->j:Ljava/util/List;

    new-instance v0, Lcom/google/android/apps/youtube/core/identity/w;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/core/identity/w;-><init>(Lcom/google/android/apps/youtube/core/identity/o;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->a:Lcom/google/android/apps/youtube/core/identity/y;

    return-void
.end method

.method static synthetic a()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/identity/o;->b:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/identity/o;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->j:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/identity/o;Lcom/google/android/apps/youtube/core/identity/v;Landroid/app/Activity;)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {p2, v1}, Landroid/app/Activity;->showDialog(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->g:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "PlusChannelUpgradeDialogShown"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/identity/o;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->i:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/r;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/core/identity/r;-><init>(Lcom/google/android/apps/youtube/core/identity/o;Ljava/lang/Exception;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/identity/o;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/core/identity/o;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->h:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "username"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_channel_id"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/identity/o;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->i:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/q;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/identity/q;-><init>(Lcom/google/android/apps/youtube/core/identity/o;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/identity/o;Lcom/google/android/apps/youtube/core/identity/v;Landroid/app/Activity;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p2, v0}, Landroid/app/Activity;->showDialog(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->g:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "WebChannelUpgradeDialogShown"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/identity/o;)Lcom/google/android/apps/youtube/core/identity/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->f:Lcom/google/android/apps/youtube/core/identity/b;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/identity/o;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->i:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/p;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/identity/p;-><init>(Lcom/google/android/apps/youtube/core/identity/o;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/core/identity/o;)Lcom/google/android/apps/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->c:Lcom/google/android/apps/youtube/core/client/bc;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)Landroid/app/Dialog;
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/ui/aa;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/core/ui/aa;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/youtube/p;->ae:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/aa;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->cd:I

    new-instance v2, Lcom/google/android/apps/youtube/core/identity/u;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/youtube/core/identity/u;-><init>(Lcom/google/android/apps/youtube/core/identity/o;Landroid/app/Activity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/t;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/identity/t;-><init>(Lcom/google/android/apps/youtube/core/identity/o;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/v;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/o;->c:Lcom/google/android/apps/youtube/core/client/bc;

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/s;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/youtube/core/identity/s;-><init>(Lcom/google/android/apps/youtube/core/identity/o;Lcom/google/android/apps/youtube/core/identity/v;Landroid/app/Activity;)V

    invoke-static {p1, v1}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/client/bc;->a(Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final b(Landroid/app/Activity;)Landroid/app/Dialog;
    .locals 7

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/ui/n;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/o;->c:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/identity/o;->e:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/identity/o;->d:Lcom/google/android/apps/youtube/core/client/bz;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/identity/o;->g:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/identity/o;->a:Lcom/google/android/apps/youtube/core/identity/y;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/ui/n;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/client/bz;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/identity/y;)V

    return-object v0
.end method
