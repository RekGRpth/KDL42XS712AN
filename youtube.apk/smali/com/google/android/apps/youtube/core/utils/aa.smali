.class public final Lcom/google/android/apps/youtube/core/utils/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static synthetic a(F)I
    .locals 1

    const/high16 v0, 0x447a0000    # 1000.0f

    mul-float/2addr v0, p0

    float-to-int v0, v0

    return v0
.end method

.method static synthetic a(I)I
    .locals 1

    const/16 v0, 0x22

    packed-switch p0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/16 v0, 0x9

    goto :goto_0

    :pswitch_2
    const/16 v0, 0xa

    goto :goto_0

    :pswitch_3
    const/16 v0, 0xc

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x11

    goto :goto_0

    :pswitch_5
    const/16 v0, 0x12

    goto :goto_0

    :pswitch_6
    const/16 v0, 0x14

    goto :goto_0

    :pswitch_7
    const/16 v0, 0x21

    goto :goto_0

    :pswitch_8
    const/16 v0, 0x24

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method public static a()Lcom/google/android/apps/youtube/core/converter/e;
    .locals 4

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/f;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/converter/f;-><init>()V

    const-string v1, "/transcript"

    new-instance v2, Lcom/google/android/apps/youtube/core/utils/ac;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/utils/ac;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v1

    const-string v2, "/transcript/text"

    new-instance v3, Lcom/google/android/apps/youtube/core/utils/ab;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/core/utils/ab;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    const-string v1, "/timedtext"

    new-instance v2, Lcom/google/android/apps/youtube/core/utils/af;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/utils/af;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v1

    const-string v2, "/timedtext/window"

    new-instance v3, Lcom/google/android/apps/youtube/core/utils/ae;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/core/utils/ae;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v1

    const-string v2, "/timedtext/text"

    new-instance v3, Lcom/google/android/apps/youtube/core/utils/ad;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/core/utils/ad;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/converter/f;->a()Lcom/google/android/apps/youtube/core/converter/e;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    aget-object v1, p1, v0

    invoke-interface {p0, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
