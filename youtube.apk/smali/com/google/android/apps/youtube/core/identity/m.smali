.class final Lcom/google/android/apps/youtube/core/identity/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/identity/f;

.field final synthetic b:Lcom/google/android/apps/youtube/common/c/a;

.field final synthetic c:Lcom/google/android/apps/youtube/core/identity/l;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/f;Lcom/google/android/apps/youtube/common/c/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/identity/m;->c:Lcom/google/android/apps/youtube/core/identity/l;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/identity/m;->a:Lcom/google/android/apps/youtube/core/identity/f;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/identity/m;->b:Lcom/google/android/apps/youtube/common/c/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "Could not fetch user profile"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/m;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/identity/l;->a(Lcom/google/android/apps/youtube/core/identity/l;)Lcom/google/android/apps/youtube/core/identity/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/m;->a:Lcom/google/android/apps/youtube/core/identity/f;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/m;->c:Lcom/google/android/apps/youtube/core/identity/l;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/m;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/identity/l;->a(Lcom/google/android/apps/youtube/core/identity/l;)Lcom/google/android/apps/youtube/core/identity/f;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/google/android/apps/youtube/core/identity/z;->a(Lcom/google/android/apps/youtube/core/identity/f;Lcom/google/android/apps/youtube/core/identity/UserProfile;)Lcom/google/android/apps/youtube/core/identity/z;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/identity/l;->a(Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/z;)Lcom/google/android/apps/youtube/core/identity/z;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/m;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/identity/l;->c(Lcom/google/android/apps/youtube/core/identity/l;)Lcom/google/android/apps/youtube/core/identity/z;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/m;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/identity/l;->b(Lcom/google/android/apps/youtube/core/identity/l;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/z;->a(Landroid/content/SharedPreferences;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/m;->b:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/n;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/identity/n;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
