.class final Lcom/google/android/apps/youtube/core/converter/q;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Object;

.field private final b:Lcom/google/android/apps/youtube/core/converter/e;

.field private final c:Lcom/google/android/apps/youtube/common/e/l;

.field private final d:Lcom/google/android/apps/youtube/common/e/l;

.field private final e:Lcom/google/android/apps/youtube/common/e/l;

.field private final f:Lcom/google/android/apps/youtube/common/e/l;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/converter/e;)V
    .locals 2

    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/converter/q;->b:Lcom/google/android/apps/youtube/core/converter/e;

    new-instance v0, Lcom/google/android/apps/youtube/common/e/l;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/common/e/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/q;->d:Lcom/google/android/apps/youtube/common/e/l;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/q;->d:Lcom/google/android/apps/youtube/common/e/l;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/e/l;->offer(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/apps/youtube/common/e/l;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/common/e/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/q;->e:Lcom/google/android/apps/youtube/common/e/l;

    new-instance v0, Lcom/google/android/apps/youtube/common/e/l;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/common/e/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/q;->f:Lcom/google/android/apps/youtube/common/e/l;

    new-instance v0, Lcom/google/android/apps/youtube/common/e/l;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/common/e/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/q;->c:Lcom/google/android/apps/youtube/common/e/l;

    return-void
.end method


# virtual methods
.method public final characters([CII)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/q;->d:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/l;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/converter/q;->b:Lcom/google/android/apps/youtube/core/converter/e;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/converter/e;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/converter/r;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/q;->f:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/l;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method public final endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/q;->d:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/l;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/converter/q;->b:Lcom/google/android/apps/youtube/core/converter/e;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/converter/e;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/converter/r;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/q;->e:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/l;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/xml/sax/Attributes;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/converter/q;->f:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/common/e/l;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/converter/q;->c:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/common/e/l;->peek()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    :goto_0
    iput-object v1, p0, Lcom/google/android/apps/youtube/core/converter/q;->a:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/converter/q;->c:Lcom/google/android/apps/youtube/common/e/l;

    invoke-interface {v2, v1, v0, v3}, Lcom/google/android/apps/youtube/core/converter/r;->a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/converter/q;->a:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/q;->d:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/l;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/converter/q;->d:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/common/e/l;->offer(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/converter/q;->b:Lcom/google/android/apps/youtube/core/converter/e;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/converter/e;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/converter/r;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/converter/q;->e:Lcom/google/android/apps/youtube/common/e/l;

    if-eqz p4, :cond_1

    new-instance v0, Lorg/xml/sax/helpers/AttributesImpl;

    invoke-direct {v0, p4}, Lorg/xml/sax/helpers/AttributesImpl;-><init>(Lorg/xml/sax/Attributes;)V

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/common/e/l;->offer(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/q;->f:Lcom/google/android/apps/youtube/common/e/l;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/common/e/l;->offer(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/q;->c:Lcom/google/android/apps/youtube/common/e/l;

    invoke-interface {v1, v0, p4}, Lcom/google/android/apps/youtube/core/converter/r;->a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;)V

    :cond_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/android/apps/youtube/core/converter/n;->b()Lorg/xml/sax/Attributes;

    move-result-object v0

    goto :goto_0
.end method
