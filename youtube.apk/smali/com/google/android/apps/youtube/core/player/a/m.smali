.class public final Lcom/google/android/apps/youtube/core/player/a/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/exoplayer/upstream/i;


# instance fields
.field private final a:Lcom/google/android/exoplayer/upstream/cache/a;

.field private final b:Lcom/google/android/exoplayer/upstream/cache/a;

.field private final c:Lcom/google/android/exoplayer/upstream/i;

.field private d:Lcom/google/android/exoplayer/upstream/i;

.field private e:Ljava/lang/String;

.field private f:J

.field private g:J

.field private h:Lcom/google/android/exoplayer/upstream/cache/d;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/i;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/upstream/cache/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/m;->a:Lcom/google/android/exoplayer/upstream/cache/a;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/a/m;->b:Lcom/google/android/exoplayer/upstream/cache/a;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/upstream/i;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/m;->c:Lcom/google/android/exoplayer/upstream/i;

    return-void
.end method

.method private b()V
    .locals 9

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/m;->a:Lcom/google/android/exoplayer/upstream/cache/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/m;->e:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/player/a/m;->f:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Ljava/lang/String;J)Lcom/google/android/exoplayer/upstream/cache/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/m;->b:Lcom/google/android/exoplayer/upstream/cache/a;

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lcom/google/android/exoplayer/upstream/cache/d;->d:Z

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/m;->b:Lcom/google/android/exoplayer/upstream/cache/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/m;->e:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/player/a/m;->f:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Ljava/lang/String;J)Lcom/google/android/exoplayer/upstream/cache/d;

    move-result-object v0

    :cond_0
    iget-boolean v1, v0, Lcom/google/android/exoplayer/upstream/cache/d;->d:Z

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/exoplayer/upstream/cache/d;->e:Ljava/io/File;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/player/a/m;->f:J

    iget-wide v4, v0, Lcom/google/android/exoplayer/upstream/cache/d;->b:J

    sub-long v7, v2, v4

    iget-wide v2, v0, Lcom/google/android/exoplayer/upstream/cache/d;->c:J

    sub-long/2addr v2, v7

    iget-wide v4, p0, Lcom/google/android/apps/youtube/core/player/a/m;->g:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    new-instance v0, Lcom/google/android/exoplayer/upstream/j;

    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/player/a/m;->f:J

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/a/m;->e:Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/exoplayer/upstream/j;-><init>(Landroid/net/Uri;JJLjava/lang/String;J)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/m;->c:Lcom/google/android/exoplayer/upstream/i;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/m;->d:Lcom/google/android/exoplayer/upstream/i;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/m;->d:Lcom/google/android/exoplayer/upstream/i;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer/upstream/i;->a(Lcom/google/android/exoplayer/upstream/j;)J

    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/m;->a:Lcom/google/android/exoplayer/upstream/cache/a;

    invoke-interface {v1, v0}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Lcom/google/android/exoplayer/upstream/cache/d;)V

    new-instance v1, Ljava/io/FileNotFoundException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Chunk not found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v3, v0, Lcom/google/android/exoplayer/upstream/cache/d;->b:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, v0, Lcom/google/android/exoplayer/upstream/cache/d;->b:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, v0, Lcom/google/android/exoplayer/upstream/cache/d;->c:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private c()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/m;->d:Lcom/google/android/exoplayer/upstream/i;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/m;->d:Lcom/google/android/exoplayer/upstream/i;

    invoke-interface {v0}, Lcom/google/android/exoplayer/upstream/i;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/m;->d:Lcom/google/android/exoplayer/upstream/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/m;->h:Lcom/google/android/exoplayer/upstream/cache/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/m;->a:Lcom/google/android/exoplayer/upstream/cache/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/m;->h:Lcom/google/android/exoplayer/upstream/cache/d;

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Lcom/google/android/exoplayer/upstream/cache/d;)V

    iput-object v3, p0, Lcom/google/android/apps/youtube/core/player/a/m;->h:Lcom/google/android/exoplayer/upstream/cache/d;

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/m;->h:Lcom/google/android/exoplayer/upstream/cache/d;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/m;->a:Lcom/google/android/exoplayer/upstream/cache/a;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/a/m;->h:Lcom/google/android/exoplayer/upstream/cache/d;

    invoke-interface {v1, v2}, Lcom/google/android/exoplayer/upstream/cache/a;->a(Lcom/google/android/exoplayer/upstream/cache/d;)V

    iput-object v3, p0, Lcom/google/android/apps/youtube/core/player/a/m;->h:Lcom/google/android/exoplayer/upstream/cache/d;

    :cond_2
    throw v0
.end method


# virtual methods
.method public final a([BII)I
    .locals 5

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/m;->d:Lcom/google/android/exoplayer/upstream/i;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/exoplayer/upstream/i;->a([BII)I

    move-result v0

    if-ltz v0, :cond_1

    iget-wide v1, p0, Lcom/google/android/apps/youtube/core/player/a/m;->f:J

    int-to-long v3, v0

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/android/apps/youtube/core/player/a/m;->f:J

    iget-wide v1, p0, Lcom/google/android/apps/youtube/core/player/a/m;->g:J

    int-to-long v3, v0

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/google/android/apps/youtube/core/player/a/m;->g:J

    :cond_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/a/m;->c()V

    iget-wide v1, p0, Lcom/google/android/apps/youtube/core/player/a/m;->g:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/a/m;->b()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/exoplayer/upstream/j;)J
    .locals 4

    iget-boolean v0, p1, Lcom/google/android/exoplayer/upstream/j;->b:Z

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iget-wide v0, p1, Lcom/google/android/exoplayer/upstream/j;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iget-object v0, p1, Lcom/google/android/exoplayer/upstream/j;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/m;->e:Ljava/lang/String;

    iget-wide v0, p1, Lcom/google/android/exoplayer/upstream/j;->d:J

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/player/a/m;->f:J

    iget-wide v0, p1, Lcom/google/android/exoplayer/upstream/j;->e:J

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/player/a/m;->g:J

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/a/m;->b()V

    iget-wide v0, p1, Lcom/google/android/exoplayer/upstream/j;->e:J

    return-wide v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/a/m;->c()V

    return-void
.end method
