.class final Lcom/google/android/apps/youtube/core/player/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/Director;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/player/Director;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/player/Director;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/m;-><init>(Lcom/google/android/apps/youtube/core/player/Director;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 9

    const/4 v8, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "PlayerEvent: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_1

    :goto_2
    :pswitch_1
    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director;I)I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->p(Lcom/google/android/apps/youtube/core/player/Director;)V

    :cond_1
    return v1

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/Director$PlayerState;->PREPARING:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director;Lcom/google/android/apps/youtube/core/player/Director$PlayerState;)Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->c(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/youtube/core/player/Director$PlayerState;->PREPARED:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    if-eq v0, v3, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    const-string v3, "PlayStarted"

    invoke-static {v0, v3, v8}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director;Ljava/lang/String;I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/Director$PlayerState;->PREPARED:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director;Lcom/google/android/apps/youtube/core/player/Director$PlayerState;)Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/Director$PlayerState;->NOT_PREPARED:Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director;Lcom/google/android/apps/youtube/core/player/Director$PlayerState;)Lcom/google/android/apps/youtube/core/player/Director$PlayerState;

    goto :goto_0

    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/l;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/player/Director;->d(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/l;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v4

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/l;->b()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v5

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/l;->e()I

    move-result v6

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/medialib/player/l;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->isWidevine()Z

    move-result v7

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(IIIZ)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/player/Director;->e(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->e(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/youtube/core/player/event/ad;

    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-direct {v4, v0}, Lcom/google/android/apps/youtube/core/player/event/ad;-><init>(Z)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->d(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->onAd()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/player/Director;->f(Lcom/google/android/apps/youtube/core/player/Director;)I

    move-result v3

    int-to-long v3, v3

    :goto_4
    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->b(J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->h(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/n;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_AD_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_AD:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director;Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->e(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/youtube/core/player/event/k;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/core/player/event/k;-><init>()V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_VIDEO_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_VIDEO:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director;Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->e(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/youtube/core/player/event/k;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/core/player/event/k;-><init>()V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->i(Lcom/google/android/apps/youtube/core/player/Director;)V

    goto/16 :goto_1

    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/player/Director;->g(Lcom/google/android/apps/youtube/core/player/Director;)I

    move-result v3

    int-to-long v3, v3

    goto :goto_4

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isPlayingVideo()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    const-string v3, "PlayStopped"

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/player/Director;->w()I

    move-result v4

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director;Ljava/lang/String;I)V

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->d(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->i(Lcom/google/android/apps/youtube/core/player/Director;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->h(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/n;->b()V

    goto/16 :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->d(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->i(Lcom/google/android/apps/youtube/core/player/Director;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->h(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/n;->b()V

    goto/16 :goto_1

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->d(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->i(Lcom/google/android/apps/youtube/core/player/Director;)V

    goto/16 :goto_1

    :pswitch_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->d(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->i(Lcom/google/android/apps/youtube/core/player/Director;)V

    goto/16 :goto_1

    :pswitch_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->j(Lcom/google/android/apps/youtube/core/player/Director;)I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->k(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->isHD()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Notified about buffer underrun, switching to lq stream"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director;Z)V

    goto/16 :goto_1

    :pswitch_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    const-string v3, "PlayEnded"

    invoke-static {v0, v3, v8}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->d(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->g()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isPlayingAd()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director;Z)V

    :goto_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->h(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/n;->c()V

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->l(Lcom/google/android/apps/youtube/core/player/Director;)V

    goto :goto_5

    :pswitch_9
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->m(Lcom/google/android/apps/youtube/core/player/Director;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isPlayingAd()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Lcom/google/android/apps/youtube/medialib/player/YouTubePlayerEvents$CantPlayStreamException;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->o(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    const-string v3, "AdAbandonedInBackground"

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v5}, Lcom/google/android/apps/youtube/core/player/Director;->n(Lcom/google/android/apps/youtube/core/player/Director;)I

    move-result v5

    invoke-interface {v0, v3, v4, v5}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :goto_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isPlayingAd()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director;Z)V

    :cond_9
    :goto_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->h(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/n;->b()V

    goto/16 :goto_1

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->d(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-result-object v0

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v5}, Lcom/google/android/apps/youtube/core/player/Director;->k(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/youtube/medialib/player/x;->f()I

    move-result v5

    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(IIILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v5, p1, Landroid/os/Message;->arg2:I

    invoke-static {v0, v3, v4, v5}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director;Ljava/lang/Object;II)V

    goto :goto_6

    :cond_b
    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    sget v0, Lcom/google/android/youtube/p;->eH:I

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v5}, Lcom/google/android/apps/youtube/core/player/Director;->q(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v5

    if-nez v5, :cond_c

    sget v0, Lcom/google/android/youtube/p;->cO:I

    move v3, v0

    move v0, v1

    :goto_8
    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    new-instance v5, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    sget-object v6, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->PLAYER_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v7}, Lcom/google/android/apps/youtube/core/player/Director;->r(Lcom/google/android/apps/youtube/core/player/Director;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v6, v0, v3}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;-><init>(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;ZLjava/lang/String;)V

    invoke-static {v4, v5}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director;Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isPlayingVideo()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/player/Director;->k(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/youtube/medialib/player/x;->f()I

    move-result v3

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/core/player/Director;->b(Lcom/google/android/apps/youtube/core/player/Director;I)I

    goto :goto_7

    :cond_c
    if-ne v3, v1, :cond_1a

    const/16 v3, -0x3ea

    if-ne v4, v3, :cond_d

    sget v0, Lcom/google/android/youtube/p;->gl:I

    move v3, v0

    move v0, v1

    goto :goto_8

    :cond_d
    const/16 v3, -0x3eb

    if-ne v4, v3, :cond_e

    sget v0, Lcom/google/android/youtube/p;->gl:I

    move v3, v0

    move v0, v1

    goto :goto_8

    :cond_e
    const/16 v3, -0x3ec

    if-ne v4, v3, :cond_f

    sget v0, Lcom/google/android/youtube/p;->aA:I

    move v3, v0

    move v0, v1

    goto :goto_8

    :cond_f
    const/16 v3, -0x3ed

    if-ne v4, v3, :cond_10

    sget v0, Lcom/google/android/youtube/p;->aA:I

    move v3, v0

    move v0, v1

    goto :goto_8

    :cond_10
    const/16 v3, -0x3f2

    if-ne v4, v3, :cond_11

    sget v0, Lcom/google/android/youtube/p;->go:I

    move v3, v0

    move v0, v2

    goto :goto_8

    :cond_11
    const/16 v3, 0x21

    if-ne v4, v3, :cond_12

    sget v0, Lcom/google/android/youtube/p;->H:I

    move v3, v0

    move v0, v1

    goto :goto_8

    :cond_12
    sget v3, Lcom/google/android/apps/youtube/medialib/player/p;->a:I

    if-ne v4, v3, :cond_13

    sget v0, Lcom/google/android/youtube/p;->aS:I

    move v3, v0

    move v0, v1

    goto :goto_8

    :cond_13
    sget v3, Lcom/google/android/apps/youtube/medialib/player/p;->b:I

    if-ne v4, v3, :cond_14

    sget v0, Lcom/google/android/youtube/p;->aW:I

    move v3, v0

    move v0, v2

    goto :goto_8

    :cond_14
    sget v3, Lcom/google/android/apps/youtube/medialib/player/p;->c:I

    if-ne v4, v3, :cond_15

    sget v0, Lcom/google/android/youtube/p;->aU:I

    move v3, v0

    move v0, v1

    goto/16 :goto_8

    :cond_15
    sget v3, Lcom/google/android/apps/youtube/medialib/player/p;->d:I

    if-ne v4, v3, :cond_16

    sget v0, Lcom/google/android/youtube/p;->aY:I

    move v3, v0

    move v0, v2

    goto/16 :goto_8

    :cond_16
    sget v3, Lcom/google/android/apps/youtube/medialib/player/p;->e:I

    if-ne v4, v3, :cond_17

    sget v0, Lcom/google/android/youtube/p;->aX:I

    move v3, v0

    move v0, v1

    goto/16 :goto_8

    :cond_17
    sget v3, Lcom/google/android/apps/youtube/medialib/player/p;->f:I

    if-ne v4, v3, :cond_18

    sget v0, Lcom/google/android/youtube/p;->aT:I

    move v3, v0

    move v0, v2

    goto/16 :goto_8

    :cond_18
    sget v3, Lcom/google/android/apps/youtube/medialib/player/p;->g:I

    if-ne v4, v3, :cond_19

    sget v0, Lcom/google/android/youtube/p;->aV:I

    move v3, v0

    move v0, v1

    goto/16 :goto_8

    :cond_19
    sget v3, Lcom/google/android/apps/youtube/medialib/player/p;->h:I

    if-ne v4, v3, :cond_1a

    sget v0, Lcom/google/android/youtube/p;->aA:I

    move v3, v0

    move v0, v1

    goto/16 :goto_8

    :pswitch_a
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->d(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-result-object v0

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(ZI)V

    goto/16 :goto_1

    :pswitch_b
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/m;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->d(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-result-object v0

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(I)V

    goto/16 :goto_1

    :pswitch_c
    move v2, v1

    goto/16 :goto_2

    :cond_1a
    move v3, v0

    move v0, v1

    goto/16 :goto_8

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_2
        0xd -> :sswitch_3
        0x10 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_b
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_1
        :pswitch_c
    .end packed-switch
.end method
