.class final Lcom/google/android/apps/youtube/core/player/n;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/Director;

.field private final b:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/Director;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/n;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Lcom/google/android/apps/youtube/core/player/o;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/youtube/core/player/o;-><init>(Lcom/google/android/apps/youtube/core/player/n;Lcom/google/android/apps/youtube/core/player/Director;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/n;->b:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/n;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/n;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->s(Lcom/google/android/apps/youtube/core/player/Director;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/n;->a:Lcom/google/android/apps/youtube/core/player/Director;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/n;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/Director;->k(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/youtube/medialib/player/x;->f()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/n;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/Director;->k(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/medialib/player/x;->g()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/n;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/player/Director;->k(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/youtube/medialib/player/x;->h()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director;III)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/n;->b:Ljava/lang/Runnable;

    const-wide/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/n;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/n;->b:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/n;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/n;->b:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/n;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/n;->b:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/n;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final c()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/n;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/n;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->k(Lcom/google/android/apps/youtube/core/player/Director;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/x;->g()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/n;->a:Lcom/google/android/apps/youtube/core/player/Director;

    const/16 v2, 0x64

    invoke-static {v1, v0, v0, v2}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director;III)V

    return-void
.end method

.method public final d()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/n;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method
