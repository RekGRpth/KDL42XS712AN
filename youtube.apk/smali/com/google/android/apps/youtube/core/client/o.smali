.class final Lcom/google/android/apps/youtube/core/client/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/converter/http/c;

.field final synthetic b:Lcom/google/android/apps/youtube/common/a/b;

.field final synthetic c:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;Lcom/google/android/apps/youtube/core/converter/http/c;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/o;->c:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/client/o;->a:Lcom/google/android/apps/youtube/core/converter/http/c;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/client/o;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/o;->c:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;->a(Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Forced VMAP request should only be used in debug build"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/o;->c:Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/o;->a:Lcom/google/android/apps/youtube/core/converter/http/c;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/o;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;->a(Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester;Lcom/google/android/apps/youtube/core/converter/http/c;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
