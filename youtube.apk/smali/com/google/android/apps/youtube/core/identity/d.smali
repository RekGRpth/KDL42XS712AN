.class public final Lcom/google/android/apps/youtube/core/identity/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/q;


# instance fields
.field private a:Lcom/google/android/apps/youtube/core/identity/b;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/identity/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/core/identity/d;->b:I

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/d;->a:Lcom/google/android/apps/youtube/core/identity/b;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/android/volley/VolleyError;)V
    .locals 2

    instance-of v0, p1, Lcom/android/volley/AuthFailureError;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/core/identity/d;->b:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/apps/youtube/core/identity/d;->b:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/d;->a:Lcom/google/android/apps/youtube/core/identity/b;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/b;->a()V

    return-void

    :cond_0
    throw p1
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/identity/d;->b:I

    return v0
.end method
