.class public final enum Lcom/google/android/apps/youtube/core/ui/PagedView$State;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/ui/PagedView$State;

.field public static final enum EMPTY:Lcom/google/android/apps/youtube/core/ui/PagedView$State;

.field public static final enum ERROR:Lcom/google/android/apps/youtube/core/ui/PagedView$State;

.field public static final enum ITEMS:Lcom/google/android/apps/youtube/core/ui/PagedView$State;

.field public static final enum LOADING:Lcom/google/android/apps/youtube/core/ui/PagedView$State;

.field public static final enum NEW:Lcom/google/android/apps/youtube/core/ui/PagedView$State;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    const-string v1, "NEW"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/ui/PagedView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/ui/PagedView$State;->NEW:Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    new-instance v0, Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/core/ui/PagedView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/ui/PagedView$State;->EMPTY:Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    new-instance v0, Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/youtube/core/ui/PagedView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/ui/PagedView$State;->LOADING:Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    new-instance v0, Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    const-string v1, "ITEMS"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/youtube/core/ui/PagedView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/ui/PagedView$State;->ITEMS:Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    new-instance v0, Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/youtube/core/ui/PagedView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/ui/PagedView$State;->ERROR:Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    sget-object v1, Lcom/google/android/apps/youtube/core/ui/PagedView$State;->NEW:Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/core/ui/PagedView$State;->EMPTY:Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/core/ui/PagedView$State;->LOADING:Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/core/ui/PagedView$State;->ITEMS:Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/core/ui/PagedView$State;->ERROR:Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/youtube/core/ui/PagedView$State;->$VALUES:[Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/ui/PagedView$State;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/ui/PagedView$State;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/ui/PagedView$State;->$VALUES:[Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/ui/PagedView$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/ui/PagedView$State;

    return-object v0
.end method
