.class public final Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/datalib/e/b;

.field private final b:Lcom/google/android/apps/youtube/common/network/h;

.field private final c:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

.field private final d:Lcom/google/android/apps/youtube/core/utils/a;

.field private final e:Ljava/util/PriorityQueue;

.field private final f:Ljava/lang/String;

.field private g:J


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Lcom/google/android/apps/youtube/core/utils/a;Ljava/util/List;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->a:Lcom/google/android/apps/youtube/datalib/e/b;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->b:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->c:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/utils/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->d:Lcom/google/android/apps/youtube/core/utils/a;

    new-instance v1, Ljava/util/PriorityQueue;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-direct {v1, v0}, Ljava/util/PriorityQueue;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->e:Ljava/util/PriorityQueue;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;
    .locals 3

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->e:Ljava/util/PriorityQueue;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->f:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient$PlaybackTrackingUrlPingClientState;-><init>(Ljava/util/PriorityQueue;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(J)V
    .locals 9

    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->g:J

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->e:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->size()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->e:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->shouldPingNow(J)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->getBaseUri()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->getClientParams()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    sget-object v2, Lcom/google/android/apps/youtube/core/client/bw;->a:[I

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_1

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->c:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/youtube/core/client/DeviceClassification;->a(Lcom/google/android/apps/youtube/common/e/o;)Lcom/google/android/apps/youtube/common/e/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :pswitch_1
    :try_start_1
    const-string v1, "cpn"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->f:Ljava/lang/String;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    goto :goto_1

    :pswitch_2
    const-string v1, "conn"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->b:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/common/network/h;->i()I

    move-result v2

    invoke-virtual {v3, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;I)Lcom/google/android/apps/youtube/common/e/o;

    goto :goto_1

    :pswitch_3
    const-string v1, "cmt"

    iget-wide v5, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->g:J

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    goto :goto_1

    :pswitch_4
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->d:Lcom/google/android/apps/youtube/core/utils/a;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/utils/a;->a()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/Map$Entry;

    move-object v2, v0

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    goto :goto_2

    :cond_1
    invoke-virtual {v3}, Lcom/google/android/apps/youtube/common/e/o;->a()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pinging "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->a:Lcom/google/android/apps/youtube/datalib/e/b;

    const-string v2, "remarketing"

    const v3, 0x323467f

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/datalib/e/b;->a(Ljava/lang/String;I)Lcom/google/android/apps/youtube/datalib/e/f;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/datalib/e/f;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/e/f;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->a:Lcom/google/android/apps/youtube/datalib/e/b;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/a/b;->a:Lcom/android/volley/n;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/e/b;->a(Lcom/google/android/apps/youtube/datalib/e/f;Lcom/android/volley/n;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/PlaybackTrackingUrlPingClient;->e:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->remove()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :cond_2
    monitor-exit p0

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
