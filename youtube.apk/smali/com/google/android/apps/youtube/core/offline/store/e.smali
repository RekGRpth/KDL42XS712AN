.class public final Lcom/google/android/apps/youtube/core/offline/store/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;


# direct methods
.method private constructor <init>(Ljava/lang/String;ILcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/offline/store/e;->a:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/apps/youtube/core/offline/store/e;->b:I

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/offline/store/e;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    return-void
.end method

.method static a(Ljava/lang/String;Landroid/database/Cursor;)Lcom/google/android/apps/youtube/core/offline/store/e;
    .locals 5

    const/4 v0, 0x0

    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "ad_video_id"

    invoke-static {p0, v1}, Lcom/google/android/apps/youtube/core/utils/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    const-string v2, "playback_count"

    invoke-static {p0, v2}, Lcom/google/android/apps/youtube/core/utils/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    const-string v3, "status"

    invoke-static {p0, v3}, Lcom/google/android/apps/youtube/core/utils/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/core/offline/store/e;

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->fromValue(I)Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/offline/store/e;-><init>(Ljava/lang/String;ILcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V

    goto :goto_0
.end method
