.class public abstract Lcom/google/android/apps/youtube/core/player/sequencer/a;
.super Lcom/google/android/apps/youtube/core/player/sequencer/c;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

.field private l:Lcom/google/android/apps/youtube/common/a/d;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;)V
    .locals 1

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/youtube/core/player/sequencer/c;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;)V

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/a;->a:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/a;->l:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/a;->l:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/a;->l:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    return-void
.end method

.method final a(I)V
    .locals 8

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/a;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->p()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/a;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/sequencer/b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/b;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/a;B)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/a;->l:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/a;->a:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->e()[B

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->o_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->s()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->d()I

    move-result v5

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/sequencer/a;->l:Lcom/google/android/apps/youtube/common/a/d;

    move v6, p1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method protected abstract b()Z
.end method

.method protected abstract c()Ljava/lang/String;
.end method

.method protected abstract d()I
.end method

.method protected abstract e()[B
.end method

.method protected abstract o_()Ljava/lang/String;
.end method
