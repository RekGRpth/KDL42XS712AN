.class public final enum Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

.field public static final enum CASUAL:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

.field public static final enum CURSIVE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

.field public static final enum MONOSPACED_SANS_SERIF:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

.field public static final enum MONOSPACED_SERIF:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

.field public static final enum PROPORTIONAL_SANS_SERIF:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

.field public static final enum PROPORTIONAL_SERIF:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

.field public static final enum SMALL_CAPITALS:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

.field private static fontEntryStrings:[Ljava/lang/String;

.field private static fontValueStrings:[Ljava/lang/String;


# instance fields
.field private final fontStringId:I

.field private final fontValue:I

.field private typeface:Landroid/graphics/Typeface;

.field private final typefaceFactory:Lcom/google/android/apps/youtube/core/player/overlay/bl;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    const-string v1, "MONOSPACED_SERIF"

    sget v3, Lcom/google/android/youtube/p;->ei:I

    const-string v4, "fonts/MonoSerif-Regular.ttf"

    invoke-static {v4}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->createTypefaceFactory(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/overlay/bl;

    move-result-object v5

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;-><init>(Ljava/lang/String;IIILcom/google/android/apps/youtube/core/player/overlay/bl;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->MONOSPACED_SERIF:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    new-instance v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    const-string v4, "PROPORTIONAL_SERIF"

    sget v6, Lcom/google/android/youtube/p;->ek:I

    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->createTypefaceFactory(Landroid/graphics/Typeface;)Lcom/google/android/apps/youtube/core/player/overlay/bl;

    move-result-object v8

    move v5, v9

    move v7, v9

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;-><init>(Ljava/lang/String;IIILcom/google/android/apps/youtube/core/player/overlay/bl;)V

    sput-object v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->PROPORTIONAL_SERIF:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    new-instance v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    const-string v4, "MONOSPACED_SANS_SERIF"

    sget v6, Lcom/google/android/youtube/p;->eh:I

    sget-object v0, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->createTypefaceFactory(Landroid/graphics/Typeface;)Lcom/google/android/apps/youtube/core/player/overlay/bl;

    move-result-object v8

    move v5, v10

    move v7, v10

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;-><init>(Ljava/lang/String;IIILcom/google/android/apps/youtube/core/player/overlay/bl;)V

    sput-object v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->MONOSPACED_SANS_SERIF:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    new-instance v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    const-string v4, "PROPORTIONAL_SANS_SERIF"

    sget v6, Lcom/google/android/youtube/p;->ej:I

    sget-object v0, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->createTypefaceFactory(Landroid/graphics/Typeface;)Lcom/google/android/apps/youtube/core/player/overlay/bl;

    move-result-object v8

    move v5, v11

    move v7, v11

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;-><init>(Ljava/lang/String;IIILcom/google/android/apps/youtube/core/player/overlay/bl;)V

    sput-object v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->PROPORTIONAL_SANS_SERIF:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    new-instance v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    const-string v4, "CASUAL"

    sget v6, Lcom/google/android/youtube/p;->ef:I

    const-string v0, "fonts/ComingSoon-Regular.ttf"

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->createTypefaceFactory(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/overlay/bl;

    move-result-object v8

    move v5, v12

    move v7, v12

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;-><init>(Ljava/lang/String;IIILcom/google/android/apps/youtube/core/player/overlay/bl;)V

    sput-object v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->CASUAL:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    new-instance v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    const-string v4, "CURSIVE"

    const/4 v5, 0x5

    sget v6, Lcom/google/android/youtube/p;->eg:I

    const/4 v7, 0x5

    const-string v0, "fonts/DancingScript-Regular.ttf"

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->createTypefaceFactory(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/overlay/bl;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;-><init>(Ljava/lang/String;IIILcom/google/android/apps/youtube/core/player/overlay/bl;)V

    sput-object v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->CURSIVE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    new-instance v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    const-string v4, "SMALL_CAPITALS"

    const/4 v5, 0x6

    sget v6, Lcom/google/android/youtube/p;->el:I

    const/4 v7, 0x6

    const-string v0, "fonts/CarroisGothicSC-Regular.ttf"

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->createTypefaceFactory(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/overlay/bl;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;-><init>(Ljava/lang/String;IIILcom/google/android/apps/youtube/core/player/overlay/bl;)V

    sput-object v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->SMALL_CAPITALS:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->MONOSPACED_SERIF:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->PROPORTIONAL_SERIF:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->MONOSPACED_SANS_SERIF:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->PROPORTIONAL_SANS_SERIF:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->CASUAL:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->CURSIVE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->SMALL_CAPITALS:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->$VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILcom/google/android/apps/youtube/core/player/overlay/bl;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->fontStringId:I

    iput p4, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->fontValue:I

    iput-object p5, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->typefaceFactory:Lcom/google/android/apps/youtube/core/player/overlay/bl;

    return-void
.end method

.method private static createTypefaceFactory(Landroid/graphics/Typeface;)Lcom/google/android/apps/youtube/core/player/overlay/bl;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/bk;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/player/overlay/bk;-><init>(Landroid/graphics/Typeface;)V

    return-object v0
.end method

.method private static createTypefaceFactory(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/overlay/bl;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/bj;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/player/overlay/bj;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static getDefaultFontValue()I
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    move-result-object v0

    const/4 v1, 0x3

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->fontValue:I

    return v0
.end method

.method public static getFontEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->fontEntryStrings:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    move-result-object v1

    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->fontEntryStrings:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->fontEntryStrings:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->fontStringId:I

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->fontEntryStrings:[Ljava/lang/String;

    return-object v0
.end method

.method public static getFontValueStrings()[Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->fontValueStrings:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    move-result-object v1

    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->fontValueStrings:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->fontValueStrings:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->fontValue:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->fontValueStrings:[Ljava/lang/String;

    return-object v0
.end method

.method public static getTypefaceFromFontValue(ILandroid/content/res/AssetManager;)Landroid/graphics/Typeface;
    .locals 4

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_2

    aget-object v2, v1, v0

    iget v2, v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->fontValue:I

    if-ne v2, p0, :cond_1

    aget-object v2, v1, v0

    iget-object v2, v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->typeface:Landroid/graphics/Typeface;

    if-nez v2, :cond_0

    aget-object v2, v1, v0

    aget-object v3, v1, v0

    iget-object v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->typefaceFactory:Lcom/google/android/apps/youtube/core/player/overlay/bl;

    invoke-interface {v3, p1}, Lcom/google/android/apps/youtube/core/player/overlay/bl;->a(Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->typeface:Landroid/graphics/Typeface;

    :cond_0
    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->typeface:Landroid/graphics/Typeface;

    :goto_1
    return-object v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->$VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;

    return-object v0
.end method
