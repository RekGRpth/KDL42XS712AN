.class final Lcom/google/android/apps/youtube/core/transfer/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/transfer/w;

.field private final b:Lcom/google/android/apps/youtube/core/transfer/x;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/transfer/w;Lcom/google/android/apps/youtube/core/transfer/x;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/y;->a:Lcom/google/android/apps/youtube/core/transfer/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/x;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/y;->b:Lcom/google/android/apps/youtube/core/transfer/x;

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/y;->a:Lcom/google/android/apps/youtube/core/transfer/w;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/transfer/w;->b(Lcom/google/android/apps/youtube/core/transfer/w;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/y;->b:Lcom/google/android/apps/youtube/core/transfer/x;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/transfer/x;->k(Lcom/google/android/apps/youtube/core/transfer/x;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/y;->a:Lcom/google/android/apps/youtube/core/transfer/w;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/transfer/w;->c(Lcom/google/android/apps/youtube/core/transfer/w;)Lcom/google/android/apps/youtube/core/transfer/z;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/youtube/core/transfer/z;->a(Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/y;->b:Lcom/google/android/apps/youtube/core/transfer/x;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/x;->g(Lcom/google/android/apps/youtube/core/transfer/x;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/y;->a:Lcom/google/android/apps/youtube/core/transfer/w;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/transfer/w;->a(Lcom/google/android/apps/youtube/core/transfer/w;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/y;->b:Lcom/google/android/apps/youtube/core/transfer/x;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/transfer/x;->a(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/y;->b:Lcom/google/android/apps/youtube/core/transfer/x;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/transfer/x;->c(Lcom/google/android/apps/youtube/core/transfer/x;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/y;->a:Lcom/google/android/apps/youtube/core/transfer/w;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/y;->b:Lcom/google/android/apps/youtube/core/transfer/x;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/w;->a(Lcom/google/android/apps/youtube/core/transfer/w;Lcom/google/android/apps/youtube/core/transfer/x;)V

    return-void
.end method
