.class public final Lcom/google/android/apps/youtube/core/async/aa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/converter/n;

.field private final b:Lcom/google/android/apps/youtube/core/converter/e;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/converter/n;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/converter/n;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/aa;->a:Lcom/google/android/apps/youtube/core/converter/n;

    invoke-static {}, Lcom/google/android/apps/youtube/core/utils/aa;->a()Lcom/google/android/apps/youtube/core/converter/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/aa;->b:Lcom/google/android/apps/youtube/core/converter/e;

    return-void
.end method

.method private static a(Ljava/io/InputStream;)V
    .locals 1

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 4

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->offlineSubtitlesPath:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/io/File;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->offlineSubtitlesPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/16 v0, 0x1000

    invoke-direct {v1, v3, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/aa;->a:Lcom/google/android/apps/youtube/core/converter/n;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/async/aa;->b:Lcom/google/android/apps/youtube/core/converter/e;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/n;->a(Ljava/io/InputStream;Lcom/google/android/apps/youtube/core/converter/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/r;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/r;->build()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/async/aa;->a(Ljava/io/InputStream;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_1
    :try_start_2
    invoke-interface {p2, p1, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/async/aa;->a(Ljava/io/InputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    invoke-static {v1}, Lcom/google/android/apps/youtube/core/async/aa;->a(Ljava/io/InputStream;)V

    throw v0

    :cond_0
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    invoke-interface {p2, p1, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1
.end method
