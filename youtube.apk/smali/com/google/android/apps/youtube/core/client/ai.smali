.class public final Lcom/google/android/apps/youtube/core/client/ai;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:Z

.field public final e:Z

.field public final f:Landroid/graphics/Bitmap$Config;

.field public final g:Landroid/graphics/Bitmap$Config;


# direct methods
.method public constructor <init>(IIIZZ)V
    .locals 8

    const/4 v6, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/client/ai;-><init>(IIIZZLandroid/graphics/Bitmap$Config;Landroid/graphics/Bitmap$Config;)V

    return-void
.end method

.method private constructor <init>(IIIZZLandroid/graphics/Bitmap$Config;Landroid/graphics/Bitmap$Config;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/youtube/core/client/ai;->a:I

    iput p2, p0, Lcom/google/android/apps/youtube/core/client/ai;->b:I

    iput-boolean p4, p0, Lcom/google/android/apps/youtube/core/client/ai;->d:Z

    iput-boolean p5, p0, Lcom/google/android/apps/youtube/core/client/ai;->e:Z

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ai;->f:Landroid/graphics/Bitmap$Config;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ai;->g:Landroid/graphics/Bitmap$Config;

    iput p3, p0, Lcom/google/android/apps/youtube/core/client/ai;->c:I

    return-void
.end method
