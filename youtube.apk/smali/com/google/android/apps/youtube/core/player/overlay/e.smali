.class final Lcom/google/android/apps/youtube/core/player/overlay/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/overlay/c;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/c;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/e;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/c;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/e;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/c;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Couldn\'t retrieve user profile info from [uri="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v0, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->thumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/e;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/overlay/d;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/e;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/d;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/c;B)V

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/c;->b(Lcom/google/android/apps/youtube/core/player/overlay/c;Lcom/google/android/apps/youtube/common/a/d;)Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/e;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/c;->g(Lcom/google/android/apps/youtube/core/player/overlay/c;)Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->thumbnailUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/e;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/overlay/c;->c(Lcom/google/android/apps/youtube/core/player/overlay/c;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/overlay/e;->a:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/player/overlay/c;->f(Lcom/google/android/apps/youtube/core/player/overlay/c;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_0
    return-void
.end method
