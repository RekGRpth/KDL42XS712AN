.class final Lcom/google/android/apps/youtube/core/player/as;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/identity/ah;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/ar;

.field private final b:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

.field private final c:Lcom/google/android/apps/youtube/core/player/y;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/player/ar;Lcom/google/android/apps/youtube/datalib/innertube/model/w;Lcom/google/android/apps/youtube/core/player/y;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/as;->a:Lcom/google/android/apps/youtube/core/player/ar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/as;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/as;->c:Lcom/google/android/apps/youtube/core/player/y;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/as;->a:Lcom/google/android/apps/youtube/core/player/ar;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/ar;->a(Lcom/google/android/apps/youtube/core/player/ar;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/as;->c:Lcom/google/android/apps/youtube/core/player/y;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/y;->b()V

    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/as;->a:Lcom/google/android/apps/youtube/core/player/ar;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/ar;->a(Lcom/google/android/apps/youtube/core/player/ar;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/as;->c:Lcom/google/android/apps/youtube/core/player/y;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/as;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/w;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/w;)Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/y;->a(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/as;->a:Lcom/google/android/apps/youtube/core/player/ar;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/ar;->a(Lcom/google/android/apps/youtube/core/player/ar;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/as;->c:Lcom/google/android/apps/youtube/core/player/y;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/as;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/w;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/w;)Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/y;->a(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    return-void
.end method
