.class final Lcom/google/android/apps/youtube/core/player/overlay/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/h;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/overlay/i;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/i;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/i;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/o;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/i;)V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->e(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/a/a/a/a/be;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->e(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/a/a/a/a/be;

    move-result-object v0

    iget-object v0, v0, Lcom/google/a/a/a/a/be;->i:Lcom/google/a/a/a/a/kz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->c(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/android/apps/youtube/datalib/d/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/overlay/i;->e(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/a/a/a/a/be;

    move-result-object v1

    iget-object v1, v1, Lcom/google/a/a/a/a/be;->i:Lcom/google/a/a/a/a/kz;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/datalib/d/a;->a(Lcom/google/a/a/a/a/kz;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->f(Lcom/google/android/apps/youtube/core/player/overlay/i;)[Z

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/overlay/i;->g(Lcom/google/android/apps/youtube/core/player/overlay/i;)I

    move-result v1

    aput-boolean v2, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/core/player/overlay/i;->b(Lcom/google/android/apps/youtube/core/player/overlay/i;Z)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->h(Lcom/google/android/apps/youtube/core/player/overlay/i;)[Z

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/overlay/i;->g(Lcom/google/android/apps/youtube/core/player/overlay/i;)I

    move-result v1

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->i(Lcom/google/android/apps/youtube/core/player/overlay/i;)V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->b(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/a/a/a/a/ne;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->b(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/a/a/a/a/ne;

    move-result-object v0

    iget-object v0, v0, Lcom/google/a/a/a/a/ne;->b:Lcom/google/a/a/a/a/fc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->b(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/a/a/a/a/ne;

    move-result-object v0

    iget-object v0, v0, Lcom/google/a/a/a/a/ne;->b:Lcom/google/a/a/a/a/fc;

    iget-object v0, v0, Lcom/google/a/a/a/a/fc;->g:Lcom/google/a/a/a/a/kz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->c(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/android/apps/youtube/datalib/d/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/overlay/i;->b(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/a/a/a/a/ne;

    move-result-object v1

    iget-object v1, v1, Lcom/google/a/a/a/a/ne;->b:Lcom/google/a/a/a/a/fc;

    iget-object v1, v1, Lcom/google/a/a/a/a/fc;->g:Lcom/google/a/a/a/a/kz;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/datalib/d/a;->a(Lcom/google/a/a/a/a/kz;)V

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/i;->a(Lcom/google/android/apps/youtube/core/player/overlay/i;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->d(Lcom/google/android/apps/youtube/core/player/overlay/i;)V

    return-void
.end method

.method public final e()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->j(Lcom/google/android/apps/youtube/core/player/overlay/i;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/overlay/i;->j(Lcom/google/android/apps/youtube/core/player/overlay/i;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/i;->a(Lcom/google/android/apps/youtube/core/player/overlay/i;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->k(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->l()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/overlay/i;->m(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/android/apps/youtube/core/player/s;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/overlay/i;->l(Lcom/google/android/apps/youtube/core/player/overlay/i;)Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/o;->a:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/player/overlay/i;->j(Lcom/google/android/apps/youtube/core/player/overlay/i;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/s;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;I)V

    :cond_0
    return-void
.end method
