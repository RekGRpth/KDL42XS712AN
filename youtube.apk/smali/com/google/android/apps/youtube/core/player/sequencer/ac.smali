.class final Lcom/google/android/apps/youtube/core/player/sequencer/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

.field final synthetic b:Lcom/google/android/apps/youtube/core/player/sequencer/y;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/y;Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ac;->b:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ac;->a:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ac;->b:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a(Lcom/google/android/apps/youtube/core/player/sequencer/y;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ac;->b:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ac;->a:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    iput-object v1, v0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ac;->b:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ac;->b:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ac;->b:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    iget-object v2, v2, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a:Lcom/google/android/apps/youtube/core/player/sequencer/u;

    iget-object v2, v2, Lcom/google/android/apps/youtube/core/player/sequencer/u;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->getCurrentWatchEndpoint()Lcom/google/a/a/a/a/kz;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/ac;->b:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/player/sequencer/y;->b(Lcom/google/android/apps/youtube/core/player/sequencer/y;)Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getWatchFeature()Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Lcom/google/a/a/a/a/kz;Lcom/google/android/apps/youtube/core/client/WatchFeature;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/sequencer/u;Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    goto :goto_0
.end method
