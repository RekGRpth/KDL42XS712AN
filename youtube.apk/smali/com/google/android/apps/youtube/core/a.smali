.class public abstract Lcom/google/android/apps/youtube/core/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/client/bd;
.implements Lcom/google/android/apps/youtube/core/offline/store/r;
.implements Lcom/google/android/apps/youtube/core/player/fetcher/d;


# instance fields
.field private final A:Lcom/google/android/apps/youtube/common/e/f;

.field private final B:Lcom/google/android/apps/youtube/common/e/f;

.field private final C:Lcom/google/android/apps/youtube/common/e/f;

.field private final D:Lcom/google/android/apps/youtube/common/e/f;

.field private final E:Lcom/google/android/apps/youtube/common/e/f;

.field private final F:Lcom/google/android/apps/youtube/common/e/f;

.field private final G:Lcom/google/android/apps/youtube/common/e/f;

.field private final H:Lcom/google/android/apps/youtube/common/e/f;

.field private final I:Lcom/google/android/apps/youtube/common/e/f;

.field private final J:Lcom/google/android/apps/youtube/common/e/f;

.field private final K:Lcom/google/android/apps/youtube/common/e/f;

.field private final L:Lcom/google/android/apps/youtube/common/e/f;

.field private final M:Lcom/google/android/apps/youtube/common/e/f;

.field private final N:Lcom/google/android/apps/youtube/common/e/f;

.field private final O:Lcom/google/android/apps/youtube/common/e/f;

.field private final P:Lcom/google/android/apps/youtube/common/e/f;

.field private final Q:Lcom/google/android/apps/youtube/common/e/f;

.field private final R:Lcom/google/android/apps/youtube/common/e/f;

.field private final S:Lcom/google/android/apps/youtube/common/e/f;

.field private final T:Lcom/google/android/apps/youtube/common/e/f;

.field private final U:Lcom/google/android/apps/youtube/common/e/f;

.field private final V:Lcom/google/android/apps/youtube/common/e/f;

.field protected final a:Landroid/content/Context;

.field protected final b:Landroid/content/res/Resources;

.field private c:Lcom/google/android/exoplayer/upstream/cache/a;

.field private d:Lcom/google/android/exoplayer/upstream/cache/a;

.field private final e:Lcom/google/android/apps/youtube/common/e/f;

.field private final f:Lcom/google/android/apps/youtube/common/e/f;

.field private final g:Lcom/google/android/apps/youtube/common/e/f;

.field private final h:Lcom/google/android/apps/youtube/common/e/f;

.field private final i:Lcom/google/android/apps/youtube/common/e/f;

.field private final j:Lcom/google/android/apps/youtube/common/e/f;

.field private final k:Lcom/google/android/apps/youtube/common/e/f;

.field private final l:Lcom/google/android/apps/youtube/common/e/f;

.field private final m:Lcom/google/android/apps/youtube/common/e/f;

.field private final n:Lcom/google/android/apps/youtube/common/e/f;

.field private final o:Lcom/google/android/apps/youtube/common/e/f;

.field private final p:Lcom/google/android/apps/youtube/common/e/f;

.field private final q:Lcom/google/android/apps/youtube/common/e/f;

.field private final r:Lcom/google/android/apps/youtube/common/e/f;

.field private final s:Lcom/google/android/apps/youtube/common/e/f;

.field private final t:Lcom/google/android/apps/youtube/common/e/f;

.field private final u:Lcom/google/android/apps/youtube/common/e/f;

.field private final v:Lcom/google/android/apps/youtube/common/e/f;

.field private final w:Lcom/google/android/apps/youtube/common/e/f;

.field private final x:Lcom/google/android/apps/youtube/common/e/f;

.field private final y:Lcom/google/android/apps/youtube/common/e/f;

.field private final z:Lcom/google/android/apps/youtube/common/e/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/youtube/core/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/b;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->e:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/m;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/m;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->f:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/y;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/y;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->g:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/aj;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/aj;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->h:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/ap;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/ap;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->i:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/aq;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/aq;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->j:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/ar;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/ar;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->k:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/as;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/as;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->l:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/at;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/at;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->m:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/c;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->n:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/d;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->o:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/e;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->p:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/f;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->q:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/g;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->r:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/h;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->s:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/i;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->t:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/j;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->u:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/k;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/k;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->v:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/l;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/l;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->w:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/n;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/n;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->x:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/o;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/o;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->y:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/p;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/p;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->z:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/q;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/q;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->A:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/s;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/s;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->B:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/t;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/t;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->C:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/u;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/u;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->D:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/v;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->E:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/w;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/w;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->F:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/x;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/x;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->G:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/z;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/z;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->H:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/aa;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/aa;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->I:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/ab;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/ab;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->J:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/ac;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/ac;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->K:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/ad;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/ad;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->L:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/ae;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/ae;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->M:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/af;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/af;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->N:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/ag;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/ag;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->O:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/ah;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/ah;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->P:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/ai;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/ai;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->Q:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/ak;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/ak;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->R:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/al;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/al;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->S:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/am;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/am;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->T:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/an;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/an;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->U:Lcom/google/android/apps/youtube/common/e/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/ao;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/ao;-><init>(Lcom/google/android/apps/youtube/core/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->V:Lcom/google/android/apps/youtube/common/e/f;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->b:Landroid/content/res/Resources;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/a;)Lcom/google/android/apps/youtube/common/e/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->L:Lcom/google/android/apps/youtube/common/e/f;

    return-object v0
.end method

.method static synthetic a(III)Ljava/util/concurrent/Executor;
    .locals 8

    const/16 v1, 0x3c

    const/16 v2, 0xa

    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    int-to-long v3, v1

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v6}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v7, Lcom/google/android/apps/youtube/core/utils/r;

    invoke-direct {v7, v2}, Lcom/google/android/apps/youtube/core/utils/r;-><init>(I)V

    move v1, p0

    move v2, p0

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    return-object v0
.end method

.method private final a()Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a;->bs()Lcom/google/android/apps/youtube/common/e/k;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/common/e/k;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a;->aP()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "offline_use_sd_card"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/a;)Lcom/google/android/apps/youtube/common/e/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->P:Lcom/google/android/apps/youtube/common/e/f;

    return-object v0
.end method

.method private b()Ljava/io/File;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/l;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/a;)Lcom/google/android/apps/youtube/common/e/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->N:Lcom/google/android/apps/youtube/common/e/f;

    return-object v0
.end method

.method private c()Ljava/io/File;
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a;->bs()Lcom/google/android/apps/youtube/common/e/k;

    move-result-object v4

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/common/e/k;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/File;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/common/e/k;->c()Ljava/io/File;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "offline"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "streams"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :goto_0
    if-eqz v0, :cond_1

    :goto_1
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/a;)Lcom/google/android/apps/youtube/common/e/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->O:Lcom/google/android/apps/youtube/common/e/f;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/core/a;)Lcom/google/android/apps/youtube/common/e/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->M:Lcom/google/android/apps/youtube/common/e/f;

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/youtube/core/client/l;)Lcom/google/android/apps/youtube/core/Analytics;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/core/av;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/youtube/core/av;-><init>(Lcom/google/android/apps/youtube/core/client/l;Lcom/google/android/apps/youtube/common/network/h;)V

    return-object v0
.end method

.method public final aA()Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->O:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

    return-object v0
.end method

.method public final aB()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->N:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    return-object v0
.end method

.method public final aC()Lcom/google/android/apps/youtube/core/identity/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->L:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/h;

    return-object v0
.end method

.method public final aD()Lcom/google/android/apps/youtube/core/identity/al;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->K:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/al;

    return-object v0
.end method

.method public final aE()Lcom/google/android/apps/youtube/datalib/f/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->P:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/f/a;

    return-object v0
.end method

.method public final aF()Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->e:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;

    return-object v0
.end method

.method public final aG()Lcom/google/android/apps/youtube/common/e/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->f:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/n;

    return-object v0
.end method

.method protected final aH()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->g:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    return-object v0
.end method

.method public final aI()Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->h:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final aJ()Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->i:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final aK()Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->j:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final aL()Lorg/apache/http/client/HttpClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->k:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    return-object v0
.end method

.method public final aM()Lorg/apache/http/impl/client/AbstractHttpClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->l:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/impl/client/AbstractHttpClient;

    return-object v0
.end method

.method public final aN()Lcom/google/android/apps/youtube/core/converter/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->m:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/converter/n;

    return-object v0
.end method

.method public final aO()Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->n:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method

.method public final aP()Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->o:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public final aQ()Lcom/google/android/apps/youtube/core/identity/as;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->p:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/as;

    return-object v0
.end method

.method public final aR()Lcom/google/android/apps/youtube/core/identity/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->q:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/b;

    return-object v0
.end method

.method public final aS()Lcom/google/android/apps/youtube/core/identity/k;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->r:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/k;

    return-object v0
.end method

.method public final aT()Lcom/google/android/apps/youtube/core/identity/l;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->s:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    return-object v0
.end method

.method public final aU()Lcom/google/android/apps/youtube/core/identity/ar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->t:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/ar;

    return-object v0
.end method

.method public final aV()Lcom/google/android/apps/youtube/core/identity/ak;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->u:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/ak;

    return-object v0
.end method

.method public final aW()Lcom/google/android/apps/youtube/core/utils/v;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->v:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/utils/v;

    return-object v0
.end method

.method public final aX()Lcom/google/android/apps/youtube/common/network/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->w:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    return-object v0
.end method

.method public final aY()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->x:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final aZ()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->y:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public abstract ac()Lcom/google/android/apps/youtube/core/player/ae;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method protected aj()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected ak()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected am()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a;->au()Lcom/google/android/apps/youtube/core/au;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/core/au;->E()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract au()Lcom/google/android/apps/youtube/core/au;
.end method

.method public final av()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final aw()Lcom/google/android/apps/youtube/core/client/bj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->R:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    return-object v0
.end method

.method public final ax()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->V:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final ay()Lcom/google/android/apps/youtube/datalib/config/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->T:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/config/e;

    return-object v0
.end method

.method public final az()Lcom/google/android/apps/youtube/datalib/config/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->U:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/config/a;

    return-object v0
.end method

.method public final ba()Lcom/google/android/apps/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->z:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/Analytics;

    return-object v0
.end method

.method public final bb()Lcom/google/android/apps/youtube/common/fromguava/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->A:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/fromguava/e;

    return-object v0
.end method

.method public final bc()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->B:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    return-object v0
.end method

.method public final bd()Lcom/google/android/apps/youtube/core/player/a/l;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->C:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/a/l;

    return-object v0
.end method

.method public final be()Lcom/google/android/apps/youtube/medialib/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->D:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/medialib/a;

    return-object v0
.end method

.method public final bf()Lcom/google/android/apps/youtube/common/c/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->E:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    return-object v0
.end method

.method public final declared-synchronized bg()Lcom/google/android/apps/youtube/common/database/c;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->F:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/database/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final bh()Lcom/google/android/apps/youtube/common/d/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->G:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/d/j;

    return-object v0
.end method

.method public final bi()Lcom/google/android/apps/youtube/common/d/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->H:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/d/d;

    return-object v0
.end method

.method public final bj()Lcom/google/android/apps/youtube/core/player/a/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->I:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/a/a;

    return-object v0
.end method

.method public final bk()Lcom/google/android/apps/youtube/core/identity/aa;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->J:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/aa;

    return-object v0
.end method

.method public final bl()Lcom/google/android/exoplayer/upstream/cache/a;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Using SD card for offline content."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a;->bo()Lcom/google/android/exoplayer/upstream/cache/a;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Using Primary Stroage for offline content."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a;->bn()Lcom/google/android/exoplayer/upstream/cache/a;

    move-result-object v0

    goto :goto_0
.end method

.method public final bm()Ljava/io/File;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/a;->c()Ljava/io/File;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/a;->b()Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method public final bn()Lcom/google/android/exoplayer/upstream/cache/a;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/a;->b()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offline cache dir: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/exoplayer/upstream/cache/g;

    new-instance v2, Lcom/google/android/exoplayer/upstream/cache/f;

    invoke-direct {v2}, Lcom/google/android/exoplayer/upstream/cache/f;-><init>()V

    invoke-direct {v1, v0, v2}, Lcom/google/android/exoplayer/upstream/cache/g;-><init>(Ljava/io/File;Lcom/google/android/exoplayer/upstream/cache/c;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/a;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    return-object v0
.end method

.method public final bo()Lcom/google/android/exoplayer/upstream/cache/a;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->d:Lcom/google/android/exoplayer/upstream/cache/a;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a;->bs()Lcom/google/android/apps/youtube/common/e/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/k;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/a;->c()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offline cache dir: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/exoplayer/upstream/cache/g;

    new-instance v2, Lcom/google/android/exoplayer/upstream/cache/f;

    invoke-direct {v2}, Lcom/google/android/exoplayer/upstream/cache/f;-><init>()V

    invoke-direct {v1, v0, v2}, Lcom/google/android/exoplayer/upstream/cache/g;-><init>(Ljava/io/File;Lcom/google/android/exoplayer/upstream/cache/c;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/a;->d:Lcom/google/android/exoplayer/upstream/cache/a;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->d:Lcom/google/android/exoplayer/upstream/cache/a;

    return-object v0
.end method

.method public final bp()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/a;->c:Lcom/google/android/exoplayer/upstream/cache/a;

    return-void
.end method

.method protected final bq()Lcom/google/android/apps/youtube/core/identity/as;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/core/identity/as;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/apps/youtube/core/identity/ak;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/as;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method

.method protected final br()Lcom/google/android/apps/youtube/core/player/a/l;
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a;->aP()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/h;->a(Landroid/content/SharedPreferences;)Ljava/security/Key;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a;->bn()Lcom/google/android/exoplayer/upstream/cache/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a;->bo()Lcom/google/android/exoplayer/upstream/cache/a;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a;->be()Lcom/google/android/apps/youtube/medialib/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/medialib/a;->c()Lcom/google/android/apps/youtube/common/fromguava/e;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a;->bj()Lcom/google/android/apps/youtube/core/player/a/a;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/a;->aG()Lcom/google/android/apps/youtube/common/e/n;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/player/a/b;->a(Landroid/content/Context;Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/cache/a;Ljava/security/Key;Lcom/google/android/apps/youtube/common/fromguava/e;Lcom/google/android/apps/youtube/core/player/a/a;Lcom/google/android/apps/youtube/common/e/b;)Lcom/google/android/apps/youtube/core/player/a/b;

    move-result-object v0

    return-object v0
.end method

.method public final bs()Lcom/google/android/apps/youtube/common/e/k;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/common/e/k;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/a;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/common/e/k;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public abstract e()Lcom/google/android/apps/youtube/datalib/config/b;
.end method

.method public final f()Lcom/google/android/apps/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->S:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    return-object v0
.end method

.method public abstract g()Ljava/lang/Class;
.end method

.method public q()Lcom/google/android/apps/youtube/core/offline/store/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/a;->Q:Lcom/google/android/apps/youtube/common/e/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/q;

    return-object v0
.end method
