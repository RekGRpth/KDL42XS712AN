.class public final Lcom/google/android/apps/youtube/core/client/ax;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/apps/youtube/core/utils/am;

.field private final d:Lorg/apache/http/client/HttpClient;

.field private final e:Ljava/util/concurrent/Executor;

.field private final f:Lcom/google/android/apps/youtube/core/identity/l;

.field private final g:Lcom/google/android/apps/youtube/core/identity/b;

.field private h:Z


# direct methods
.method public constructor <init>(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/b;Lcom/google/android/apps/youtube/core/au;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->d:Lorg/apache/http/client/HttpClient;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->e:Ljava/util/concurrent/Executor;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->f:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->g:Lcom/google/android/apps/youtube/core/identity/b;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->h:Z

    new-instance v0, Lcom/google/android/apps/youtube/core/utils/am;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/utils/am;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->c:Lcom/google/android/apps/youtube/core/utils/am;

    invoke-interface {p5}, Lcom/google/android/apps/youtube/core/au;->H()Landroid/util/Pair;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->a:Ljava/lang/String;

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->b:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    iput-object v2, p0, Lcom/google/android/apps/youtube/core/client/ax;->a:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/client/ax;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/client/ax;)Lcom/google/android/apps/youtube/core/identity/l;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->f:Lcom/google/android/apps/youtube/core/identity/l;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/client/ax;)Lcom/google/android/apps/youtube/core/identity/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->g:Lcom/google/android/apps/youtube/core/identity/b;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/client/ax;)Lorg/apache/http/client/HttpClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->d:Lorg/apache/http/client/HttpClient;

    return-object v0
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->e:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/client/ay;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/client/ay;-><init>(Lcom/google/android/apps/youtube/core/client/ax;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/core/utils/am;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->c:Lcom/google/android/apps/youtube/core/utils/am;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->c:Lcom/google/android/apps/youtube/core/utils/am;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/ax;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/ax;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/am;->d(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/ax;->c()V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->c:Lcom/google/android/apps/youtube/core/utils/am;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/ax;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/ax;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/am;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onSignIn(Lcom/google/android/apps/youtube/core/identity/ai;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->h:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->c:Lcom/google/android/apps/youtube/core/utils/am;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/ax;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/ax;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/am;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/client/ax;->c()V

    goto :goto_0
.end method

.method public final onSignOut(Lcom/google/android/apps/youtube/core/identity/aj;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->h:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/ax;->c:Lcom/google/android/apps/youtube/core/utils/am;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/ax;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/ax;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/am;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
