.class public final Lcom/google/android/apps/youtube/core/async/GDataRequest;
.super Lcom/google/android/apps/youtube/core/async/w;
.source "SourceFile"


# direct methods
.method private constructor <init>(Landroid/net/Uri;Ljava/util/Map;[B)V
    .locals 2

    const-string v0, "http"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "stage.gdata.youtube.com"

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "dev.gdata.youtube.com"

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "https"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/async/w;-><init>(Landroid/net/Uri;Ljava/util/Map;[B)V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    invoke-direct {v0, p0, v1, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;[B)V

    return-object v0
.end method

.method public static a(Landroid/net/Uri;Lcom/google/android/apps/youtube/core/async/GDataRequest;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;->b:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Lcom/google/android/apps/youtube/core/async/w;)[B

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/youtube/core/async/GDataRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;[B)V

    return-object v0
.end method

.method public static a(Landroid/net/Uri;Ljava/util/Map;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;[B)V

    return-object v0
.end method

.method public static a(Landroid/net/Uri;Ljava/util/Map;[B)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/youtube/core/async/GDataRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;[B)V

    return-object v0
.end method

.method public static a(Landroid/net/Uri;[B)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/apps/youtube/core/async/GDataRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;[B)V

    return-object v0
.end method


# virtual methods
.method public final b(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/GDataRequest;->b:Ljava/util/Map;

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Lcom/google/android/apps/youtube/core/async/w;)[B

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/youtube/core/async/GDataRequest;-><init>(Landroid/net/Uri;Ljava/util/Map;[B)V

    return-object v0
.end method
