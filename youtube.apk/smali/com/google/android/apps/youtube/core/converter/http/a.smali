.class public final Lcom/google/android/apps/youtube/core/converter/http/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/converter/c;


# instance fields
.field private a:Lcom/google/android/apps/youtube/core/utils/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/utils/a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/converter/http/a;->a:Lcom/google/android/apps/youtube/core/utils/a;

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Landroid/net/Uri;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/a;->a:Lcom/google/android/apps/youtube/core/utils/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/utils/a;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->createHttpRequest(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method
