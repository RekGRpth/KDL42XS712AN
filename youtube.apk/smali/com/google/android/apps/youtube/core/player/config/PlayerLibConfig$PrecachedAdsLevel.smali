.class public final enum Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

.field public static final enum NONE:Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

.field public static final enum PRECACHE_AD_METADATA:Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

.field public static final enum PRECACHE_AD_STREAM:Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

.field public static final enum SIMULATE_AD_LOAD:Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;->NONE:Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    const-string v1, "SIMULATE_AD_LOAD"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;->SIMULATE_AD_LOAD:Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    const-string v1, "PRECACHE_AD_METADATA"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;->PRECACHE_AD_METADATA:Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    const-string v1, "PRECACHE_AD_STREAM"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;->PRECACHE_AD_STREAM:Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;->NONE:Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;->SIMULATE_AD_LOAD:Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;->PRECACHE_AD_METADATA:Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;->PRECACHE_AD_STREAM:Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;->$VALUES:[Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;->$VALUES:[Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/player/config/PlayerLibConfig$PrecachedAdsLevel;

    return-object v0
.end method
