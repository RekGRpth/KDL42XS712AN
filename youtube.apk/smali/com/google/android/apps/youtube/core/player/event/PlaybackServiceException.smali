.class public final Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;
.super Ljava/lang/Exception;
.source "SourceFile"


# static fields
.field private static final ANALYTICS_ACTION_BASE_STRING:Ljava/lang/String; = "PlaybackError - "


# instance fields
.field public final isRetriable:Z

.field public final reason:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;ZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->reason:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    iput-boolean p2, p0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->isRetriable:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;ZLjava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p3, p4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->reason:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    iput-boolean p2, p0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->isRetriable:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;ZLjava/lang/Throwable;)V
    .locals 0

    invoke-direct {p0, p3}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->reason:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    iput-boolean p2, p0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->isRetriable:Z

    return-void
.end method

.method private causeExceptionSimpleClassNameForAnalytics()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v1, v0, Ljava/util/concurrent/ExecutionException;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const-string v0, "UnknownCause"

    goto :goto_0
.end method

.method private detailsForAnalytics()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "UnknownDetails"

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final recordExceptionInAnalytics(Lcom/google/android/apps/youtube/core/Analytics;)V
    .locals 6

    const/4 v5, 0x0

    const-string v0, "PlaybackError - "

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/d;->a:[I

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->reason:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    invoke-interface {p1, v0, v5}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;I)V

    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Lcom/android/volley/VolleyError;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "InnerTube: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->causeExceptionSimpleClassNameForAnalytics()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "GData: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->causeExceptionSimpleClassNameForAnalytics()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "WatchNext: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->causeExceptionSimpleClassNameForAnalytics()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "PlayabilityFailed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->reason:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "HeartbeatServer: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->causeExceptionSimpleClassNameForAnalytics()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MissingStreams"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Unknown: %s - %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->causeExceptionSimpleClassNameForAnalytics()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->detailsForAnalytics()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final shouldSkipVideo()Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->reason:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->UNPLAYABLE:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->VIDEO_ERROR:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->USER_CHECK_FAILED:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->UNPLAYABLE_IN_BACKGROUND:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->NO_STREAMS:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->isIn([Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;)Z

    move-result v0

    return v0
.end method
