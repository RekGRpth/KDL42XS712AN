.class public Lcom/google/android/apps/youtube/core/player/Interval;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/apps/youtube/core/player/v;

.field protected final b:Lcom/google/android/apps/youtube/core/player/v;

.field protected final c:Ljava/lang/String;

.field protected final d:I


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Interval["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Interval;->a:Lcom/google/android/apps/youtube/core/player/v;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/v;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/Interval;->b:Lcom/google/android/apps/youtube/core/player/v;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/v;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Interval;->a:Lcom/google/android/apps/youtube/core/player/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/v;->a()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/Interval;->b:Lcom/google/android/apps/youtube/core/player/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/v;->a()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    const-string v0, "]"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ")"

    goto :goto_0
.end method
