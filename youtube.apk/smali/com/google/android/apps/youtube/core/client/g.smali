.class final Lcom/google/android/apps/youtube/core/client/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/google/android/apps/youtube/common/a/b;

.field final synthetic d:Lcom/google/android/apps/youtube/core/client/e;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/client/e;Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/g;->d:Lcom/google/android/apps/youtube/core/client/e;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/client/g;->a:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/client/g;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/youtube/core/client/g;->c:Lcom/google/android/apps/youtube/common/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/g;->a:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/g;->d:Lcom/google/android/apps/youtube/core/client/e;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/client/e;->a(Lcom/google/android/apps/youtube/core/client/e;)Lcom/google/android/apps/youtube/core/client/AdsClient;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/g;->a:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/g;->b:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/youtube/core/client/AdsClient;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/g;->c:Lcom/google/android/apps/youtube/common/a/b;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/g;->c:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/client/g;->d:Lcom/google/android/apps/youtube/core/client/e;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/client/g;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/client/g;->a:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getInstreamAdPlayerResponseMap()Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v3, v0, v4, v5}, Lcom/google/android/apps/youtube/core/client/e;->a(Ljava/util/List;Ljava/lang/String;Ljava/util/Map;)Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/android/apps/youtube/core/client/AdsClient$VmapException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/apps/youtube/core/client/AdsClient$VastException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/g;->c:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v2, v1, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/g;->c:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v2, v1, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
