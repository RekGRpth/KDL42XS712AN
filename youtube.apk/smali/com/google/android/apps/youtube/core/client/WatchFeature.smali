.class public final enum Lcom/google/android/apps/youtube/core/client/WatchFeature;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum AD:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum ANNOTATION:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum ARTIST_VIDEOS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum BROWSE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum CHANNEL_ACTIVITY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum CHANNEL_FAVORITE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum CHANNEL_PLAYLIST:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum CHANNEL_UPLOAD:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum EXTERNAL_URL:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CATEGORY_ANIMALS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CATEGORY_AUTOS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CATEGORY_COMEDY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CATEGORY_EDUCATION:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CATEGORY_ENTERTAINMENT:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CATEGORY_FILM:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CATEGORY_GAMES:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CATEGORY_HOWTO:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CATEGORY_MUSIC:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CATEGORY_NEWS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CATEGORY_NONPROFIT:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CATEGORY_PEOPLE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CATEGORY_POPULAR:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CATEGORY_RECOMMENDED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CATEGORY_SCIENCE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CATEGORY_SPORTS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CATEGORY_TRAVEL:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CHANNEL_ACTIVITY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CHANNEL_ACTIVITY_UPLOAD:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_CHANNEL_UPLOADS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_RIVER_ACTIVITY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_RIVER_ACTIVITY_UPLOAD:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_RIVER_RECOMMENDED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum GUIDE_RIVER_UPLOADS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum LIVE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum LIVE_TEASER:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum MY_FAVORITES:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum MY_UPLOADS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum NO_FEATURE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum OFFLINE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum PLAYER_EMBEDDED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum PLAYLISTS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum RELATED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum RELATED_ARTIST:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum REMOTE_QR_SCAN:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum REMOTE_QUEUE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum SEARCH:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum UPLOAD_NOTIFICATION:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum VIDEO_NOTIFICATION:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum WATCH_HISTORY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum WATCH_LATER:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum WIDGET:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum YT_REMOTE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field public static final enum YT_REMOTE_DIAL:Lcom/google/android/apps/youtube/core/client/WatchFeature;


# instance fields
.field private final featureString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "NO_FEATURE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->NO_FEATURE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_RIVER_UPLOADS"

    const-string v2, "g-all-u"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_RIVER_UPLOADS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_RIVER_ACTIVITY"

    const-string v2, "g-all-a"

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_RIVER_ACTIVITY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_RIVER_ACTIVITY_UPLOAD"

    const-string v2, "g-all-au"

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_RIVER_ACTIVITY_UPLOAD:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_RIVER_RECOMMENDED"

    const-string v2, "g-all-r"

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_RIVER_RECOMMENDED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CHANNEL_UPLOADS"

    const/4 v2, 0x5

    const-string v3, "g-user-u"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CHANNEL_UPLOADS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CHANNEL_ACTIVITY"

    const/4 v2, 0x6

    const-string v3, "g-user-a"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CHANNEL_ACTIVITY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CHANNEL_ACTIVITY_UPLOAD"

    const/4 v2, 0x7

    const-string v3, "g-user-au"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CHANNEL_ACTIVITY_UPLOAD:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CATEGORY_POPULAR"

    const/16 v2, 0x8

    const-string v3, "g-pop"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_POPULAR:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CATEGORY_RECOMMENDED"

    const/16 v2, 0x9

    const-string v3, "g-vrec"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_RECOMMENDED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CATEGORY_AUTOS"

    const/16 v2, 0xa

    const-string v3, "g-auto"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_AUTOS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CATEGORY_COMEDY"

    const/16 v2, 0xb

    const-string v3, "g-comedy"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_COMEDY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CATEGORY_EDUCATION"

    const/16 v2, 0xc

    const-string v3, "g-edu"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_EDUCATION:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CATEGORY_ENTERTAINMENT"

    const/16 v2, 0xd

    const-string v3, "g-ent"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_ENTERTAINMENT:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CATEGORY_FILM"

    const/16 v2, 0xe

    const-string v3, "g-film"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_FILM:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CATEGORY_GAMES"

    const/16 v2, 0xf

    const-string v3, "g-games"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_GAMES:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CATEGORY_MUSIC"

    const/16 v2, 0x10

    const-string v3, "g-music"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_MUSIC:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CATEGORY_NEWS"

    const/16 v2, 0x11

    const-string v3, "g-news"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_NEWS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CATEGORY_NONPROFIT"

    const/16 v2, 0x12

    const-string v3, "g-npo"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_NONPROFIT:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CATEGORY_PEOPLE"

    const/16 v2, 0x13

    const-string v3, "g-people"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_PEOPLE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CATEGORY_ANIMALS"

    const/16 v2, 0x14

    const-string v3, "g-pets"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_ANIMALS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CATEGORY_SCIENCE"

    const/16 v2, 0x15

    const-string v3, "g-sci"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_SCIENCE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CATEGORY_SPORTS"

    const/16 v2, 0x16

    const-string v3, "g-sports"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_SPORTS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CATEGORY_HOWTO"

    const/16 v2, 0x17

    const-string v3, "g-howto"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_HOWTO:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "GUIDE_CATEGORY_TRAVEL"

    const/16 v2, 0x18

    const-string v3, "g-travel"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_TRAVEL:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "CHANNEL_ACTIVITY"

    const/16 v2, 0x19

    const-string v3, "c-activity"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->CHANNEL_ACTIVITY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "CHANNEL_FAVORITE"

    const/16 v2, 0x1a

    const-string v3, "c-favorite"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->CHANNEL_FAVORITE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "CHANNEL_PLAYLIST"

    const/16 v2, 0x1b

    const-string v3, "c-playlist"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->CHANNEL_PLAYLIST:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "CHANNEL_UPLOAD"

    const/16 v2, 0x1c

    const-string v3, "c-upload"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->CHANNEL_UPLOAD:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "AD"

    const/16 v2, 0x1d

    const-string v3, "ad"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->AD:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "ANNOTATION"

    const/16 v2, 0x1e

    const-string v3, "annotation"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->ANNOTATION:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "EXTERNAL_URL"

    const/16 v2, 0x1f

    const-string v3, "custom_uri"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->EXTERNAL_URL:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "LIVE"

    const/16 v2, 0x20

    const-string v3, "g-all-lsb"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->LIVE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "LIVE_TEASER"

    const/16 v2, 0x21

    const-string v3, "g-all-lss"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->LIVE_TEASER:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "MY_FAVORITES"

    const/16 v2, 0x22

    const-string v3, "my_favorites"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->MY_FAVORITES:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "MY_UPLOADS"

    const/16 v2, 0x23

    const-string v3, "my_uploads"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->MY_UPLOADS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "OFFLINE"

    const/16 v2, 0x24

    const-string v3, "offline"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->OFFLINE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "PLAYER_EMBEDDED"

    const/16 v2, 0x25

    const-string v3, "player_embedded"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->PLAYER_EMBEDDED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "PLAYLISTS"

    const/16 v2, 0x26

    const-string v3, "playlist"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->PLAYLISTS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "RELATED"

    const/16 v2, 0x27

    const-string v3, "related"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->RELATED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "WATCH_LATER"

    const/16 v2, 0x28

    const-string v3, "my_watch_later"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->WATCH_LATER:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "WATCH_HISTORY"

    const/16 v2, 0x29

    const-string v3, "my_history"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->WATCH_HISTORY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "ARTIST_VIDEOS"

    const/16 v2, 0x2a

    const-string v3, "artist_video"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->ARTIST_VIDEOS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "RELATED_ARTIST"

    const/16 v2, 0x2b

    const-string v3, "related_artist"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->RELATED_ARTIST:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "REMOTE_QR_SCAN"

    const/16 v2, 0x2c

    const-string v3, "remote_qr_scan"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->REMOTE_QR_SCAN:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "REMOTE_QUEUE"

    const/16 v2, 0x2d

    const-string v3, "remote_queue"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->REMOTE_QUEUE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "UPLOAD_NOTIFICATION"

    const/16 v2, 0x2e

    const-string v3, "upload_notification"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->UPLOAD_NOTIFICATION:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "VIDEO_NOTIFICATION"

    const/16 v2, 0x2f

    const-string v3, "push-notification"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->VIDEO_NOTIFICATION:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "YT_REMOTE"

    const/16 v2, 0x30

    const-string v3, "ytremote"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->YT_REMOTE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "YT_REMOTE_DIAL"

    const/16 v2, 0x31

    const-string v3, "ytremote_d"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->YT_REMOTE_DIAL:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "WIDGET"

    const/16 v2, 0x32

    const-string v3, "widget"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->WIDGET:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "SEARCH"

    const/16 v2, 0x33

    const-string v3, "search"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->SEARCH:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const-string v1, "BROWSE"

    const/16 v2, 0x34

    const-string v3, "browse"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/client/WatchFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->BROWSE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    const/16 v0, 0x35

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/client/WatchFeature;

    sget-object v1, Lcom/google/android/apps/youtube/core/client/WatchFeature;->NO_FEATURE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_RIVER_UPLOADS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_RIVER_ACTIVITY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_RIVER_ACTIVITY_UPLOAD:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_RIVER_RECOMMENDED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CHANNEL_UPLOADS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CHANNEL_ACTIVITY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CHANNEL_ACTIVITY_UPLOAD:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_POPULAR:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_RECOMMENDED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_AUTOS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_COMEDY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_EDUCATION:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_ENTERTAINMENT:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_FILM:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_GAMES:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_MUSIC:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_NEWS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_NONPROFIT:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_PEOPLE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_ANIMALS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_SCIENCE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_SPORTS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_HOWTO:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_TRAVEL:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->CHANNEL_ACTIVITY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->CHANNEL_FAVORITE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->CHANNEL_PLAYLIST:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->CHANNEL_UPLOAD:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->AD:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->ANNOTATION:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->EXTERNAL_URL:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->LIVE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->LIVE_TEASER:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->MY_FAVORITES:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->MY_UPLOADS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->OFFLINE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->PLAYER_EMBEDDED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->PLAYLISTS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->RELATED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->WATCH_LATER:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->WATCH_HISTORY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->ARTIST_VIDEOS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->RELATED_ARTIST:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->REMOTE_QR_SCAN:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->REMOTE_QUEUE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->UPLOAD_NOTIFICATION:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->VIDEO_NOTIFICATION:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->YT_REMOTE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->YT_REMOTE_DIAL:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->WIDGET:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->SEARCH:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->BROWSE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->$VALUES:[Lcom/google/android/apps/youtube/core/client/WatchFeature;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->featureString:Ljava/lang/String;

    return-void
.end method

.method public static fromOrdinal(I)Lcom/google/android/apps/youtube/core/client/WatchFeature;
    .locals 1

    if-ltz p0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/client/WatchFeature;->values()[Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-result-object v0

    array-length v0, v0

    if-lt p0, v0, :cond_1

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->NO_FEATURE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcom/google/android/apps/youtube/core/client/WatchFeature;->values()[Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-result-object v0

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/WatchFeature;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/client/WatchFeature;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->$VALUES:[Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/client/WatchFeature;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/client/WatchFeature;

    return-object v0
.end method


# virtual methods
.method public final getFeatureString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->featureString:Ljava/lang/String;

    return-object v0
.end method
