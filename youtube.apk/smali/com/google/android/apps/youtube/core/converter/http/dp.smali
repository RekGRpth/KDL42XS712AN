.class public final Lcom/google/android/apps/youtube/core/converter/http/dp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/converter/c;


# static fields
.field public static final a:Lcom/google/android/apps/youtube/core/converter/http/dp;


# instance fields
.field private final b:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/dp;

    sget-object v1, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/converter/http/dp;-><init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/converter/http/dp;->a:Lcom/google/android/apps/youtube/core/converter/http/dp;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/dp;->b:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Landroid/net/Uri;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/dp;->b:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->createHttpRequest(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method
