.class final Lcom/google/android/apps/youtube/core/async/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/async/g;

.field private final b:Ljava/lang/Object;

.field private final c:Ljava/lang/Object;

.field private final d:Lcom/google/android/apps/youtube/common/a/b;

.field private e:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/async/g;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/async/h;->a:Lcom/google/android/apps/youtube/core/async/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/async/h;->b:Ljava/lang/Object;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/async/h;->c:Ljava/lang/Object;

    iput-object p4, p0, Lcom/google/android/apps/youtube/core/async/h;->d:Lcom/google/android/apps/youtube/common/a/b;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/h;->d:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/h;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/async/h;->e:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/h;->a:Lcom/google/android/apps/youtube/core/async/g;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/g;->a(Lcom/google/android/apps/youtube/core/async/g;)Lcom/google/android/apps/youtube/core/converter/d;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/h;->a:Lcom/google/android/apps/youtube/core/async/g;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/g;->b(Lcom/google/android/apps/youtube/core/async/g;)Ljava/util/concurrent/Executor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/h;->a:Lcom/google/android/apps/youtube/core/async/g;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/g;->b(Lcom/google/android/apps/youtube/core/async/g;)Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/async/h;->run()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/h;->d:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/h;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final run()V
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/h;->a:Lcom/google/android/apps/youtube/core/async/g;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/g;->a(Lcom/google/android/apps/youtube/core/async/g;)Lcom/google/android/apps/youtube/core/converter/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/h;->e:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/converter/d;->a_(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/h;->d:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/async/h;->b:Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/apps/youtube/core/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/h;->a:Lcom/google/android/apps/youtube/core/async/g;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/async/h;->b:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/async/h;->c:Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/async/h;->d:Lcom/google/android/apps/youtube/common/a/b;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/apps/youtube/core/async/g;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;Ljava/lang/Exception;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/h;->a:Lcom/google/android/apps/youtube/core/async/g;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/async/h;->b:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/async/h;->c:Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/async/h;->d:Lcom/google/android/apps/youtube/common/a/b;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/apps/youtube/core/async/g;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;Ljava/lang/Exception;)V

    goto :goto_0
.end method
