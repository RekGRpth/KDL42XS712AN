.class public final enum Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

.field public static final enum BLACK_ON_WHITE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

.field public static final enum CUSTOM:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

.field public static final enum WHITE_ON_BLACK:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

.field public static final enum YELLOW_ON_BLACK:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

.field public static final enum YELLOW_ON_BLUE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

.field private static styleEntryStrings:[Ljava/lang/String;

.field private static styleValueStrings:[Ljava/lang/String;


# instance fields
.field private styleStringId:I

.field private styleValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    const-string v1, "WHITE_ON_BLACK"

    sget v2, Lcom/google/android/youtube/p;->eA:I

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->WHITE_ON_BLACK:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    const-string v1, "BLACK_ON_WHITE"

    sget v2, Lcom/google/android/youtube/p;->ey:I

    invoke-direct {v0, v1, v4, v2, v4}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->BLACK_ON_WHITE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    const-string v1, "YELLOW_ON_BLACK"

    sget v2, Lcom/google/android/youtube/p;->eB:I

    invoke-direct {v0, v1, v5, v2, v5}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->YELLOW_ON_BLACK:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    const-string v1, "YELLOW_ON_BLUE"

    sget v2, Lcom/google/android/youtube/p;->eC:I

    invoke-direct {v0, v1, v6, v2, v6}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->YELLOW_ON_BLUE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    const-string v1, "CUSTOM"

    sget v2, Lcom/google/android/youtube/p;->ez:I

    invoke-direct {v0, v1, v7, v2, v7}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->CUSTOM:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->WHITE_ON_BLACK:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->BLACK_ON_WHITE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->YELLOW_ON_BLACK:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->YELLOW_ON_BLUE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->CUSTOM:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->$VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleStringId:I

    iput p4, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleValue:I

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleValue:I

    return v0
.end method

.method public static getCustomStyleValue()I
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    move-result-object v0

    const/4 v1, 0x4

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleValue:I

    return v0
.end method

.method public static getDefaultStyleValue()I
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleValue:I

    return v0
.end method

.method public static getStyleEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleEntryStrings:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    move-result-object v1

    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleEntryStrings:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleEntryStrings:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleStringId:I

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleEntryStrings:[Ljava/lang/String;

    return-object v0
.end method

.method public static getStyleValueStrings()[Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleValueStrings:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    move-result-object v1

    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleValueStrings:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleValueStrings:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleValue:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->styleValueStrings:[Ljava/lang/String;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->$VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;

    return-object v0
.end method
