.class final Lcom/google/android/apps/youtube/core/player/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/Director;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/player/Director;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/k;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    check-cast p1, Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error loading ad [request="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/k;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/k;->a:Lcom/google/android/apps/youtube/core/player/Director;

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/core/player/Director;->a(Lcom/google/android/apps/youtube/core/player/Director;Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;)V

    return-void
.end method
