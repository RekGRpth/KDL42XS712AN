.class public interface abstract Lcom/google/android/apps/youtube/core/client/bc;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;
.end method

.method public abstract a(ILcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract a(ILjava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract a(Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;ILcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ComplaintReason;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract a(Ljava/lang/String;ZLcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract b()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract b(ILcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract b(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract b(Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract b(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract c()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract c(ILcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract c(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract c(Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract c(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract d(ILcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract d(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract d(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract e(ILcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract e(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract e(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract f()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract f(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract f(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract g()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract g(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract h()V
.end method

.method public abstract h(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract i()V
.end method

.method public abstract i(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract j()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract j(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract k()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract k(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract l()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract l(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract m()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract m(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract n()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract n(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract o()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract p()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract q()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract r()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract s()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract t()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract u()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract v()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract w()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract x()Lcom/google/android/apps/youtube/core/async/af;
.end method

.method public abstract y()Lcom/google/android/apps/youtube/core/async/af;
.end method
