.class final Lcom/google/android/apps/youtube/core/converter/http/dm;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;)V
    .locals 4

    invoke-static {}, Lcom/google/android/apps/youtube/core/converter/http/dj;->a()Ljava/util/Map;

    move-result-object v0

    const-string v1, "type"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    if-nez v0, :cond_2

    const-string v0, "Invalid survey type encountered"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->UNSUPPORTED:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    move-object v1, v0

    :goto_0
    invoke-static {}, Lcom/google/android/apps/youtube/core/converter/http/dj;->b()Ljava/util/Map;

    move-result-object v0

    const-string v2, "randomize_option"

    invoke-interface {p2, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    if-nez v0, :cond_0

    const-string v0, "Invalid randomize type encountered"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->UNKNOWN:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    :cond_0
    new-instance v2, Lcom/google/android/apps/youtube/datalib/legacy/model/au;

    const-string v3, "text"

    invoke-interface {p2, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/converter/http/dj;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;)Lcom/google/android/apps/youtube/datalib/legacy/model/au;

    move-result-object v0

    const-string v1, "api_context"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/au;

    move-result-object v0

    const-string v1, "video_timeout_seconds"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_0
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->b(I)Lcom/google/android/apps/youtube/datalib/legacy/model/au;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->add(Ljava/lang/Object;)Z

    return-void

    :catch_0
    move-exception v2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid value for duration: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 2

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;

    move-result-object v1

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;)Lcom/google/android/apps/youtube/datalib/legacy/model/ar;

    return-void
.end method
