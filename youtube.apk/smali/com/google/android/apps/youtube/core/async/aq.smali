.class public final Lcom/google/android/apps/youtube/core/async/aq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/common/cache/a;

.field private final b:Lcom/google/android/apps/youtube/core/async/af;

.field private final c:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

.field private final d:Lcom/google/android/apps/youtube/common/e/b;

.field private final e:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;Lcom/google/android/apps/youtube/common/e/b;J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/cache/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/aq;->a:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/aq;->b:Lcom/google/android/apps/youtube/core/async/af;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/aq;->c:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/aq;->d:Lcom/google/android/apps/youtube/common/e/b;

    iput-wide p5, p0, Lcom/google/android/apps/youtube/core/async/aq;->e:J

    return-void
.end method

.method private a(Ljava/util/List;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "<?xml version=\'1.0\' encoding=\'UTF-8\'?>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "<feed xmlns=\'http://www.w3.org/2005/Atom\' xmlns:batch=\'http://schemas.google.com/gdata/batch\' xmlns:yt=\'http://gdata.youtube.com/schemas/2007\'>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "<batch:operation type=\'query\'/>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/aq;->c:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->b()Landroid/net/Uri;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "<entry>"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "<id>"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "</id>"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "</entry>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    const-string v0, "</feed>"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/aq;->c:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->c()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/e/m;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;[B)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/async/aq;)Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/aq;->c:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/async/aq;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;
    .locals 1

    invoke-static {p1, p2, p3}, Lcom/google/android/apps/youtube/core/async/aq;->a(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry$Builder;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry$Builder;-><init>()V

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry$Builder;->setResult(Ljava/lang/Object;)Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry$Builder;

    move-result-object v0

    const/16 v3, 0xc8

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry$Builder;->setStatusCode(I)Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry$Builder;->build()Lcom/google/android/apps/youtube/datalib/legacy/model/BatchEntry;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/async/aq;)Lcom/google/android/apps/youtube/common/e/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/aq;->d:Lcom/google/android/apps/youtube/common/e/b;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/async/aq;)Lcom/google/android/apps/youtube/common/cache/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/aq;->a:Lcom/google/android/apps/youtube/common/cache/a;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 12

    move-object v2, p1

    check-cast v2, Ljava/util/List;

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/async/aq;->c:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/async/aq;->a:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-interface {v6, v1}, Lcom/google/android/apps/youtube/common/cache/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/async/Timestamped;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/async/aq;->d:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v6}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v6

    if-eqz v1, :cond_0

    iget-wide v8, v1, Lcom/google/android/apps/youtube/core/async/Timestamped;->timestamp:J

    cmp-long v8, v6, v8

    if-ltz v8, :cond_0

    iget-wide v8, v1, Lcom/google/android/apps/youtube/core/async/Timestamped;->timestamp:J

    iget-wide v10, p0, Lcom/google/android/apps/youtube/core/async/aq;->e:J

    add-long/2addr v8, v10

    cmp-long v6, v8, v6

    if-ltz v6, :cond_0

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/async/Timestamped;->element:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    invoke-static {v2, v4, v0}, Lcom/google/android/apps/youtube/core/async/aq;->a(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    invoke-interface {p2, v2, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_1
    return-void

    :cond_2
    iget-object v7, p0, Lcom/google/android/apps/youtube/core/async/aq;->b:Lcom/google/android/apps/youtube/core/async/af;

    invoke-direct {p0, v5}, Lcom/google/android/apps/youtube/core/async/aq;->a(Ljava/util/List;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v8

    new-instance v0, Lcom/google/android/apps/youtube/core/async/ar;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/async/ar;-><init>(Lcom/google/android/apps/youtube/core/async/aq;Ljava/util/List;Lcom/google/android/apps/youtube/common/a/b;Ljava/util/Map;Ljava/util/List;B)V

    invoke-interface {v7, v8, v0}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_1
.end method
