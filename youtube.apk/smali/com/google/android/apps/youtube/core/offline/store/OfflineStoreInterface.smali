.class public interface abstract Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;
.end method

.method public abstract a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;
.end method

.method public abstract a(J)V
.end method

.method public abstract a(Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract a(Ljava/lang/String;J)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract a(Ljava/util/Collection;)V
.end method

.method public abstract a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)Z
.end method

.method public abstract a(Lcom/google/android/apps/youtube/datalib/legacy/model/v;)Z
.end method

.method public abstract b(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;
.end method

.method public abstract b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
.end method

.method public abstract b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/s;
.end method

.method public abstract b(Ljava/lang/String;J)Lcom/google/android/apps/youtube/datalib/legacy/model/u;
.end method

.method public abstract b(Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract b(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract c(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/v;
.end method

.method public abstract c(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public abstract c(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract d(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public abstract d()Lcom/google/android/apps/youtube/core/offline/store/i;
.end method

.method public abstract d(Ljava/lang/String;)Ljava/util/List;
.end method

.method public abstract e()Ljava/util/Collection;
.end method

.method public abstract e(Ljava/lang/String;)V
.end method

.method public abstract f()V
.end method

.method public abstract f(Ljava/lang/String;)V
.end method

.method public abstract g()Ljava/util/Map;
.end method

.method public abstract g(Ljava/lang/String;)V
.end method

.method public abstract h(Ljava/lang/String;)V
.end method

.method public abstract i()J
.end method

.method public abstract i(Ljava/lang/String;)Landroid/util/Pair;
.end method

.method public abstract j(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video;
.end method

.method public abstract k(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;
.end method

.method public abstract l(Ljava/lang/String;)Ljava/util/List;
.end method

.method public abstract m(Ljava/lang/String;)V
.end method

.method public abstract n(Ljava/lang/String;)V
.end method

.method public abstract o(Ljava/lang/String;)I
.end method

.method public abstract p(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;
.end method
