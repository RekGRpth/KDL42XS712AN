.class public final Lcom/google/android/apps/youtube/core/player/event/v;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

.field private final b:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

.field private final c:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/event/v;->a:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/event/v;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/event/v;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/event/v;->a:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/event/v;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/event/v;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    return-object v0
.end method
