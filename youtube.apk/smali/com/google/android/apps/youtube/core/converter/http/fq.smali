.class final Lcom/google/android/apps/youtube/core/converter/http/fq;
.super Lcom/google/android/apps/youtube/core/converter/o;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/converter/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;)V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->offer(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/common/e/l;Lorg/xml/sax/Attributes;Ljava/lang/String;)V
    .locals 2

    const-class v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->build()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v1

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/common/e/l;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->addEntry(Ljava/lang/Object;)Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;

    return-void
.end method
