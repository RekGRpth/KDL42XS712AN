.class public final Lcom/google/android/apps/youtube/core/identity/z;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private volatile a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/z;->b:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/z;->c:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/identity/z;->d:Landroid/net/Uri;

    return-void
.end method

.method public static a(Lcom/google/android/apps/youtube/core/identity/f;Lcom/google/android/apps/youtube/core/identity/UserProfile;)Lcom/google/android/apps/youtube/core/identity/z;
    .locals 4

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/z;

    iget-object v2, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->displayUsername:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/identity/f;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    iget-object v3, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->thumbnailUri:Landroid/net/Uri;

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/apps/youtube/core/identity/z;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/identity/f;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/z;->b:Ljava/lang/String;

    return-object v0
.end method

.method final a(Landroid/content/SharedPreferences;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/z;->d:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "profile_display_email"

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/identity/z;->c:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "profile_display_name"

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/identity/z;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "profile_thumbnail_uri"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/z;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/z;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/z;->d:Landroid/net/Uri;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/youtube/core/identity/z;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/apps/youtube/core/identity/z;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/z;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/youtube/core/identity/z;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/z;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/youtube/core/identity/z;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/z;->d:Landroid/net/Uri;

    iget-object v3, p1, Lcom/google/android/apps/youtube/core/identity/z;->d:Landroid/net/Uri;

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/apps/youtube/core/identity/z;->a:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/z;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/z;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/z;->d:Landroid/net/Uri;

    if-nez v2, :cond_3

    :goto_2
    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/youtube/core/identity/z;->a:I

    :cond_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/z;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/z;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/z;->d:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->hashCode()I

    move-result v1

    goto :goto_2
.end method
