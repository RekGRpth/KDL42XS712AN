.class public final Lcom/google/android/apps/youtube/core/player/overlay/q;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/ae;

.field private final b:Lcom/google/android/apps/youtube/common/c/a;

.field private final c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

.field private d:Lcom/google/android/apps/youtube/core/player/overlay/t;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/ae;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->a:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->b:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/s;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/s;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/q;B)V

    invoke-interface {p3, v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setListener(Lcom/google/android/apps/youtube/core/player/overlay/p;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->a:Lcom/google/android/apps/youtube/core/player/ae;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/core/player/overlay/t;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->d:Lcom/google/android/apps/youtube/core/player/overlay/t;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/player/overlay/q;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->e:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/core/player/overlay/q;)Lcom/google/android/apps/youtube/common/c/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->b:Lcom/google/android/apps/youtube/common/c/a;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/core/player/overlay/t;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->d:Lcom/google/android/apps/youtube/core/player/overlay/t;

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->a(Ljava/util/List;)V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setHasCc(Z)V

    return-void
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setCcEnabled(Z)V

    return-void
.end method

.method public final handleAudioOnlyEvent(Lcom/google/android/apps/youtube/core/player/event/a;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/a;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->e:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->e:Z

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setAudioOnlyEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    instance-of v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->e:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setAlwaysShowControls(Z)V

    :cond_0
    return-void
.end method

.method public final handleFormatStreamChangeEvent(Lcom/google/android/apps/youtube/medialib/player/l;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/medialib/player/l;->c()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setHQ(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/medialib/player/l;->b()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->isHD()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setHQisHD(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/medialib/player/l;->d()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setSupportsQualityToggle(Z)V

    return-void
.end method

.method public final handlePlaybackServiceException(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->reason:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->isCritical()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->isRetriable:Z

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setErrorAndShowMessage(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method public final handleSequencerHasPreviousNextEvent(Lcom/google/android/apps/youtube/core/player/event/u;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/u;->a()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setHasPrevious(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/u;->b()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setHasNext(Z)V

    return-void
.end method

.method public final handleVideoControlsStateEvent(Lcom/google/android/apps/youtube/core/player/event/z;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/r;->a:[I

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/player/event/z;->a:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setInitial()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setPlaying()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setAndShowPaused()V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setLoading()V

    goto :goto_0

    :pswitch_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This method shouldn\'t be used for errors."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setAndShowEnded()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final handleVideoFullscreenEvent(Lcom/google/android/apps/youtube/core/player/event/ab;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ab;->a()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setFullscreen(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ab;->b()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setShowFullscreen(Z)V

    return-void
.end method

.method public final handleVideoStageEvent(Lcom/google/android/apps/youtube/core/player/event/ac;)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->NEW:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v2, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->g()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->YOUTUBE:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->READY:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setScrubbingEnabled(Z)V

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->AD_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->AD:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    invoke-interface {v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->e()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->e()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClickthroughUri()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_2

    :goto_1
    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setLearnMoreEnabled(Z)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isPlayingVideo()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getInfoCardCollection()Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    move-result-object v3

    if-eqz v3, :cond_4

    :goto_2
    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setHasInfoCard(Z)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->f()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->YOUTUBE:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->LIVE:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V

    goto :goto_0

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isPlayingAd()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->e()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->e()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adInfoCards:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    if-eqz v3, :cond_7

    :goto_3
    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setHasInfoCard(Z)V

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto :goto_3
.end method

.method public final handleVideoTimeEvent(Lcom/google/android/apps/youtube/core/player/event/ae;)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/q;->c:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ae;->a()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ae;->b()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ae;->c()I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->setTimes(III)V

    return-void
.end method
