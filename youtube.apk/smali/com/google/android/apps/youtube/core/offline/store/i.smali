.class public final Lcom/google/android/apps/youtube/core/offline/store/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/offline/store/s;


# instance fields
.field a:Lcom/google/android/apps/youtube/core/offline/store/c;

.field b:Lcom/google/android/apps/youtube/core/offline/store/f;

.field c:Lcom/google/android/apps/youtube/core/offline/store/d;

.field final d:Lcom/google/android/apps/youtube/core/offline/store/x;

.field private final e:Landroid/content/Context;

.field private final f:Ljava/lang/String;

.field private g:Landroid/database/sqlite/SQLiteDatabase;

.field private final h:Lcom/google/android/apps/youtube/core/offline/store/l;

.field private final i:Lcom/google/android/apps/youtube/core/offline/store/j;

.field private j:Lcom/google/android/apps/youtube/core/offline/store/u;

.field private k:Lcom/google/android/apps/youtube/core/offline/store/aa;

.field private l:Lcom/google/android/apps/youtube/core/offline/store/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/l;Lcom/google/android/apps/youtube/core/offline/store/j;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->e:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->f:Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->h:Lcom/google/android/apps/youtube/core/offline/store/l;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/j;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->i:Lcom/google/android/apps/youtube/core/offline/store/j;

    new-instance v0, Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/offline/store/x;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V
    .locals 7

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "media_status"

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->value()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "videos"

    const-string v3, "media_status = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->value()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    .locals 2

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->p(Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->m(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->i:Lcom/google/android/apps/youtube/core/offline/store/j;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/j;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->owner:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->p(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->i:Lcom/google/android/apps/youtube/core/offline/store/j;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->owner:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/j;->c(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Ljava/util/List;)V
    .locals 2

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/x;->f(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/aa;->d(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->y(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->x(Ljava/lang/String;)V

    :cond_1
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/List;ILjava/util/HashSet;)V
    .locals 5

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v3, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v4, p1, v3, v1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v4, v3}, Lcom/google/android/apps/youtube/core/offline/store/aa;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v3, v0, p3}, Lcom/google/android/apps/youtube/core/offline/store/aa;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;I)V

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {p4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private x(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->j:Lcom/google/android/apps/youtube/core/offline/store/u;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/u;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->i:Lcom/google/android/apps/youtube/core/offline/store/j;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/j;->d(Ljava/lang/String;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "Error deleting streams"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->i:Lcom/google/android/apps/youtube/core/offline/store/j;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/j;->d(Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->i:Lcom/google/android/apps/youtube/core/offline/store/j;

    invoke-interface {v1, p1}, Lcom/google/android/apps/youtube/core/offline/store/j;->d(Ljava/lang/String;)V

    throw v0
.end method

.method private y(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->b:Lcom/google/android/apps/youtube/core/offline/store/f;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/f;->b(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/t;)Lcom/google/android/apps/youtube/datalib/legacy/model/u;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->j:Lcom/google/android/apps/youtube/core/offline/store/u;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/offline/store/u;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/t;)Lcom/google/android/apps/youtube/datalib/legacy/model/u;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/core/offline/store/m;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->e:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->i:Lcom/google/android/apps/youtube/core/offline/store/j;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/offline/store/m;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/j;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/m;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->ACTIVE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->FAILED_UNKNOWN:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/offline/store/u;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/u;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->j:Lcom/google/android/apps/youtube/core/offline/store/u;

    new-instance v0, Lcom/google/android/apps/youtube/core/offline/store/aa;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->h:Lcom/google/android/apps/youtube/core/offline/store/l;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/offline/store/aa;-><init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/youtube/core/offline/store/l;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    new-instance v0, Lcom/google/android/apps/youtube/core/offline/store/v;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/v;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->l:Lcom/google/android/apps/youtube/core/offline/store/v;

    new-instance v0, Lcom/google/android/apps/youtube/core/offline/store/c;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/c;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->a:Lcom/google/android/apps/youtube/core/offline/store/c;

    new-instance v0, Lcom/google/android/apps/youtube/core/offline/store/f;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/f;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->b:Lcom/google/android/apps/youtube/core/offline/store/f;

    new-instance v0, Lcom/google/android/apps/youtube/core/offline/store/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/d;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->c:Lcom/google/android/apps/youtube/core/offline/store/d;

    :cond_1
    return-void
.end method

.method public final varargs a([Lcom/google/android/exoplayer/upstream/cache/a;)V
    .locals 13

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/x;->b()V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->ACTIVE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->FAILED_UNKNOWN:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "videos"

    const-string v3, "media_status != ?"

    new-array v4, v11, [Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->DELETED:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->value()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v12

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_0
    new-instance v0, Lcom/google/android/apps/youtube/core/offline/store/k;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->h:Lcom/google/android/apps/youtube/core/offline/store/l;

    const/4 v4, 0x0

    invoke-direct {v0, p0, v1, v3, v4}, Lcom/google/android/apps/youtube/core/offline/store/k;-><init>(Lcom/google/android/apps/youtube/core/offline/store/i;Landroid/database/Cursor;Lcom/google/android/apps/youtube/core/offline/store/l;B)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/core/offline/store/k;->a(Lcom/google/android/apps/youtube/core/offline/store/k;Lcom/google/android/apps/youtube/core/offline/store/x;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/aa;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/core/offline/store/x;->e(Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/aa;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "playlist_video"

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/String;

    const-string v0, "playlist_id"

    aput-object v0, v5, v12

    const-string v0, "video_id"

    aput-object v0, v5, v11

    const-string v6, "playlist_id IS NOT NULL"

    move-object v7, v2

    move-object v8, v2

    move-object v9, v2

    move-object v10, v2

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_1
    const-string v0, "playlist_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    const-string v3, "video_id"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/x;->c()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/z;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/z;->c()Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    move-result-object v1

    sget-object v4, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->COMPLETE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    if-ne v1, v4, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->j:Lcom/google/android/apps/youtube/core/offline/store/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/z;->a()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v1, v4, v2}, Lcom/google/android/apps/youtube/core/offline/store/u;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/t;)Lcom/google/android/apps/youtube/datalib/legacy/model/u;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    move-result-object v1

    if-nez v1, :cond_4

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    move-result-object v1

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/z;->a()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->c()I

    move-result v5

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getLastModified()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/youtube/core/utils/p;->a(Ljava/lang/String;IJ)Ljava/lang/String;

    move-result-object v4

    array-length v5, p1

    move v1, v12

    :goto_4
    if-ge v1, v5, :cond_7

    aget-object v6, p1, v1

    if-eqz v6, :cond_5

    invoke-interface {v6}, Lcom/google/android/exoplayer/upstream/cache/a;->a()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    move v1, v11

    :goto_5
    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/z;->a(Z)V

    goto :goto_3

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_6
    return-void

    :cond_7
    move v1, v12

    goto :goto_5
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;I)Z
    .locals 5

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v1, p1, p3}, Lcom/google/android/apps/youtube/core/offline/store/aa;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;I)V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-direct {p0, v0, p2, p3, v1}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Ljava/util/List;ILjava/util/HashSet;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v3, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    iget-object v4, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "Error inserting playlist"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x1

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;Ljava/util/List;)Z
    .locals 6

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->e(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->i(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->n(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->o(Ljava/lang/String;)V

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v5, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v3, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "Error syncing playlist"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_0
    :try_start_2
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    :cond_1
    :try_start_3
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-direct {p0, v0, p3}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Ljava/util/List;)V

    goto :goto_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, p1, v2}, Lcom/google/android/apps/youtube/core/offline/store/aa;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;I)V

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-direct {p0, v1, p2, v2, v3}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Ljava/util/List;ILjava/util/HashSet;)V

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/youtube/core/offline/store/x;->b(Ljava/lang/String;)V

    goto :goto_4

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/x;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    iget-object v4, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x1

    goto/16 :goto_1
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;I)Z
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/offline/store/aa;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->k(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/x;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "Error inserting single video"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->p(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;I)Z
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->j:Lcom/google/android/apps/youtube/core/offline/store/u;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/offline/store/u;->a(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "Error deleting stream"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;IJ)Z
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->j:Lcom/google/android/apps/youtube/core/offline/store/u;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/core/offline/store/u;->a(Ljava/lang/String;IJ)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "Error updating stream progress"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;J)Z
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/z;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/apps/youtube/core/offline/store/aa;->a(Ljava/lang/String;J)V

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/youtube/core/offline/store/z;->a(J)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "Error updating last playback timestamp"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;JJ)Z
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    cmp-long v0, p2, v3

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    cmp-long v0, p4, v3

    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    cmp-long v0, p2, p4

    if-gtz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/z;

    move-result-object v0

    if-eqz v0, :cond_3

    :try_start_0
    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/google/android/apps/youtube/core/offline/store/z;->a(JJ)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v1, "Error updating media progress"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;J)Z
    .locals 8

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/z;

    move-result-object v7

    if-eqz v7, :cond_0

    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->sanitizeForOffline()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    move-object v1, p1

    move-wide v3, p3

    move-wide v5, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/offline/store/aa;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;JJ)V

    move-object v1, v7

    move-wide v3, p3

    move-wide v5, p3

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/youtube/core/offline/store/z;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;JJ)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->i:Lcom/google/android/apps/youtube/core/offline/store/j;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/core/offline/store/j;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "Error updating player response for offline"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v1, "Error inserting player response"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Z)Z
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->j:Lcom/google/android/apps/youtube/core/offline/store/u;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/youtube/core/offline/store/u;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Z)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "Error inserting stream"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)Z
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/z;->c()Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    move-result-object v1

    if-eq v1, p2, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/youtube/core/offline/store/aa;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/offline/store/z;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "Error updating media status"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;I)Z
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/z;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/youtube/core/offline/store/aa;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v1, p1, p3}, Lcom/google/android/apps/youtube/core/offline/store/aa;->a(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/z;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/offline/store/z;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    :catch_0
    move-exception v0

    const-string v1, "Error undeleting video"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Z
    .locals 4

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/z;

    move-result-object v1

    if-eqz v1, :cond_3

    const/4 v0, 0x0

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->ACTIVE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)Z

    :cond_1
    invoke-virtual {v1, p2}, Lcom/google/android/apps/youtube/core/offline/store/z;->a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    iget-object v2, p2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-ne v2, v3, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->COMPLETE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/offline/store/x;->g(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Z
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->a:Lcom/google/android/apps/youtube/core/offline/store/c;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/c;->b(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->b:Lcom/google/android/apps/youtube/core/offline/store/f;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/offline/store/f;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V

    invoke-virtual {p3}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p3}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->c:Lcom/google/android/apps/youtube/core/offline/store/d;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/d;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->c:Lcom/google/android/apps/youtube/core/offline/store/d;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/d;->a(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;)Z
    .locals 4

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->h(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->e(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->n(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->o(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->i:Lcom/google/android/apps/youtube/core/offline/store/j;

    invoke-interface {v2, p1}, Lcom/google/android/apps/youtube/core/offline/store/j;->b(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    iget-object v3, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->author:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/core/offline/store/aa;->p(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->i:Lcom/google/android/apps/youtube/core/offline/store/j;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->author:Ljava/lang/String;

    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/core/offline/store/j;->c(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Ljava/util/List;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "Error deleting playlist"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_1
    :try_start_2
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/offline/store/x;->b(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    :cond_2
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/x;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->i:Lcom/google/android/apps/youtube/core/offline/store/j;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/j;->a()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final a(Ljava/util/List;)Z
    .locals 4

    const/4 v1, 0x1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->l:Lcom/google/android/apps/youtube/core/offline/store/v;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/core/offline/store/v;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "Error inserting subtitle tracks"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 4

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->b:Lcom/google/android/apps/youtube/core/offline/store/f;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/youtube/core/offline/store/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error loading ad [originalVideoId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error loading ad [originalVideoId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->f(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/aa;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->c:Lcom/google/android/apps/youtube/core/offline/store/d;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/d;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->c:Lcom/google/android/apps/youtube/core/offline/store/d;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/offline/store/d;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V

    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Z
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->a:Lcom/google/android/apps/youtube/core/offline/store/c;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/c;->b(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->b:Lcom/google/android/apps/youtube/core/offline/store/f;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/offline/store/f;->b(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public final b(Ljava/lang/String;Ljava/util/List;)Z
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->a(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->a:Lcom/google/android/apps/youtube/core/offline/store/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/offline/store/c;->a(Ljava/lang/String;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public final c()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/aa;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Ljava/util/List;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->e(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->b:Lcom/google/android/apps/youtube/core/offline/store/f;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/offline/store/f;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->h(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/x;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->b:Lcom/google/android/apps/youtube/core/offline/store/f;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/offline/store/f;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final e(Ljava/lang/String;)I
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->g(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final e()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->b:Lcom/google/android/apps/youtube/core/offline/store/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/f;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->b:Lcom/google/android/apps/youtube/core/offline/store/f;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/offline/store/f;->e(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final f(Ljava/lang/String;)I
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->i(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final f(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->b:Lcom/google/android/apps/youtube/core/offline/store/f;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/offline/store/f;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/g;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, v0, Lcom/google/android/apps/youtube/core/offline/store/g;->g:I

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/z;->b()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/z;->f()Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/y;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/y;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j(Ljava/lang/String;)Ljava/util/Set;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->h(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final k(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/v;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/z;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/z;->e()Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l(Ljava/lang/String;)Ljava/util/List;
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->j(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final m(Ljava/lang/String;)Ljava/util/List;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->l:Lcom/google/android/apps/youtube/core/offline/store/v;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/v;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final n(Ljava/lang/String;)Z
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/z;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->f(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/z;->c()Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->DELETED:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    if-eq v0, v1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->k(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "Error inserting existing video as single video"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o(Ljava/lang/String;)Z
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->f(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->l(Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->y(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/offline/store/i;->x(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/x;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->d:Lcom/google/android/apps/youtube/core/offline/store/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/offline/store/x;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->i:Lcom/google/android/apps/youtube/core/offline/store/j;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/j;->a()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_3
    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "Error deleting video"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    const/4 v0, 0x0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method public final p(Ljava/lang/String;)Z
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->l:Lcom/google/android/apps/youtube/core/offline/store/v;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/v;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "Error deleting subtitle tracks"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/aa;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final r(Ljava/lang/String;)Ljava/util/List;
    .locals 4

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->a:Lcom/google/android/apps/youtube/core/offline/store/c;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/offline/store/c;->a(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error loading ad breaks [originalVideoId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error loading ad breaks [originalVideoId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final s(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->c:Lcom/google/android/apps/youtube/core/offline/store/d;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/d;->e(Ljava/lang/String;)V

    return-void
.end method

.method public final t(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->c:Lcom/google/android/apps/youtube/core/offline/store/d;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/d;->f(Ljava/lang/String;)V

    return-void
.end method

.method public final u(Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->c:Lcom/google/android/apps/youtube/core/offline/store/d;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/d;->d(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/e;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, v0, Lcom/google/android/apps/youtube/core/offline/store/e;->b:I

    goto :goto_0
.end method

.method public final v(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->c:Lcom/google/android/apps/youtube/core/offline/store/d;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/d;->d(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/e;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->DELETED:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/youtube/core/offline/store/e;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    goto :goto_0
.end method

.method public final w(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->b:Lcom/google/android/apps/youtube/core/offline/store/f;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/f;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->b:Lcom/google/android/apps/youtube/core/offline/store/f;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/offline/store/f;->b(Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->c:Lcom/google/android/apps/youtube/core/offline/store/d;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/offline/store/d;->b(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->k:Lcom/google/android/apps/youtube/core/offline/store/aa;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/offline/store/aa;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/offline/store/i;->x(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->b:Lcom/google/android/apps/youtube/core/offline/store/f;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/f;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->a:Lcom/google/android/apps/youtube/core/offline/store/c;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/c;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/offline/store/i;->g:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void
.end method
