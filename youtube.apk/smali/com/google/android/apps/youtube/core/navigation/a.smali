.class public final Lcom/google/android/apps/youtube/core/navigation/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/d/a;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/navigation/a;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/a/a/a/a/kz;)V
    .locals 4

    :try_start_0
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->n:Lcom/google/a/a/a/a/y;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Settings not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/navigation/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->r:Lcom/google/a/a/a/a/aa;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Artist not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->c:Lcom/google/a/a/a/a/am;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Browse not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->q:Lcom/google/a/a/a/a/bi;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Capture not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->m:Lcom/google/a/a/a/a/bj;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Category not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->d:Lcom/google/a/a/a/a/dr;

    if-eqz v0, :cond_5

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Create Channel not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->e:Lcom/google/a/a/a/a/fw;

    if-eqz v0, :cond_6

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Channel Store not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->f:Lcom/google/a/a/a/a/hm;

    if-eqz v0, :cond_7

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Inbox not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->o:Lcom/google/a/a/a/a/jx;

    if-eqz v0, :cond_8

    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->o:Lcom/google/a/a/a/a/jx;

    iget-object v0, v0, Lcom/google/a/a/a/a/jx;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/navigation/a;->a:Landroid/content/Context;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://www.youtube.com/user/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/core/utils/m;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0

    :cond_8
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->t:Lcom/google/a/a/a/a/jy;

    if-eqz v0, :cond_9

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Playlist not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->p:Lcom/google/a/a/a/a/jz;

    if-eqz v0, :cond_a

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Feed not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->s:Lcom/google/a/a/a/a/ls;

    if-eqz v0, :cond_b

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Offline not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->w:Lcom/google/a/a/a/a/me;

    if-eqz v0, :cond_c

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Offline Watch not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->j:Lcom/google/a/a/a/a/pm;

    if-eqz v0, :cond_d

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Purchases not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->g:Lcom/google/a/a/a/a/qf;

    if-eqz v0, :cond_e

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Search not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->l:Lcom/google/a/a/a/a/ri;

    if-eqz v0, :cond_f

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Sign in not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->h:Lcom/google/a/a/a/a/sj;

    if-eqz v0, :cond_10

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Subscription Manager not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->k:Lcom/google/a/a/a/a/tu;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/navigation/a;->a:Landroid/content/Context;

    iget-object v1, p1, Lcom/google/a/a/a/a/kz;->k:Lcom/google/a/a/a/a/tu;

    iget-object v1, v1, Lcom/google/a/a/a/a/tu;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/utils/m;->b(Landroid/content/Context;Landroid/net/Uri;)V

    goto/16 :goto_0

    :cond_11
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->i:Lcom/google/a/a/a/a/wb;

    if-eqz v0, :cond_12

    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->i:Lcom/google/a/a/a/a/wb;

    iget-object v0, v0, Lcom/google/a/a/a/a/wb;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/navigation/a;->a:Landroid/content/Context;

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->NO_FEATURE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/youtube/core/utils/m;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/youtube/core/client/WatchFeature;)V

    goto/16 :goto_0

    :cond_12
    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->u:Lcom/google/a/a/a/a/wl;

    if-eqz v0, :cond_13

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Watch Playlist not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_13
    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Unknown Navigation"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException; {:try_start_1 .. :try_end_1} :catch_0
.end method
