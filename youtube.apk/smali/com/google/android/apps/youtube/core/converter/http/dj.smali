.class public final Lcom/google/android/apps/youtube/core/converter/http/dj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Map;

.field private static final b:Ljava/util/Map;

.field private static final c:Lcom/google/android/apps/youtube/core/converter/e;


# instance fields
.field private final d:Lcom/google/android/apps/youtube/core/converter/n;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/SurveyConverter$1;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/converter/http/SurveyConverter$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/converter/http/dj;->a:Ljava/util/Map;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/SurveyConverter$2;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/converter/http/SurveyConverter$2;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/converter/http/dj;->b:Ljava/util/Map;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/f;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/converter/f;-><init>()V

    const-string v1, "/document"

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/dn;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/converter/http/dn;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    const-string v1, "/document/question"

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/dm;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/converter/http/dm;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    const-string v1, "/document/question/options"

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/dl;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/converter/http/dl;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    const-string v1, "/document/question/additional_beacon_urls"

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/dk;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/converter/http/dk;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/converter/f;->a()Lcom/google/android/apps/youtube/core/converter/e;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/core/converter/http/dj;->c:Lcom/google/android/apps/youtube/core/converter/e;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/converter/n;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/converter/n;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/dj;->d:Lcom/google/android/apps/youtube/core/converter/n;

    return-void
.end method

.method static synthetic a()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/converter/http/dj;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/converter/http/dj;->b:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;
    .locals 3

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/converter/http/dj;->d:Lcom/google/android/apps/youtube/core/converter/n;

    sget-object v2, Lcom/google/android/apps/youtube/core/converter/http/dj;->c:Lcom/google/android/apps/youtube/core/converter/e;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/core/converter/n;->a(Ljava/io/InputStream;Lcom/google/android/apps/youtube/core/converter/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ar;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/ConverterException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/ConverterException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/ConverterException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
