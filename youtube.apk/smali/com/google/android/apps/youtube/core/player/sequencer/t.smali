.class public final Lcom/google/android/apps/youtube/core/player/sequencer/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/sequencer/s;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/s;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/t;->a:Lcom/google/android/apps/youtube/core/player/sequencer/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/t;->a:Lcom/google/android/apps/youtube/core/player/sequencer/s;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/t;->a:Lcom/google/android/apps/youtube/core/player/sequencer/s;

    iget v1, v1, Lcom/google/android/apps/youtube/core/player/sequencer/s;->q:I

    iput v1, v0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->p:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/t;->a:Lcom/google/android/apps/youtube/core/player/sequencer/s;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/t;->a:Lcom/google/android/apps/youtube/core/player/sequencer/s;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->REQUEST_FAILED:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/t;->a:Lcom/google/android/apps/youtube/core/player/sequencer/s;

    iget-object v4, v4, Lcom/google/android/apps/youtube/core/player/sequencer/s;->e:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v4, p2}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, p2}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;-><init>(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;ZLjava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->a(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/t;->a:Lcom/google/android/apps/youtube/core/player/sequencer/s;

    iput-object p2, v0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/t;->a:Lcom/google/android/apps/youtube/core/player/sequencer/s;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/t;->a:Lcom/google/android/apps/youtube/core/player/sequencer/s;

    iget v1, v1, Lcom/google/android/apps/youtube/core/player/sequencer/s;->q:I

    iput v1, v0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->p:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/t;->a:Lcom/google/android/apps/youtube/core/player/sequencer/s;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/t;->a:Lcom/google/android/apps/youtube/core/player/sequencer/s;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/sequencer/s;->b:Lcom/google/android/apps/youtube/core/player/sequencer/r;

    invoke-interface {v0, p2}, Lcom/google/android/apps/youtube/core/player/sequencer/r;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;)V

    return-void
.end method
