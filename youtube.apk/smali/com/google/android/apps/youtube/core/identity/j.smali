.class final Lcom/google/android/apps/youtube/core/identity/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/identity/f;

.field final synthetic b:Lcom/google/android/apps/youtube/common/a/b;

.field final synthetic c:Lcom/google/android/apps/youtube/core/identity/h;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/identity/h;Lcom/google/android/apps/youtube/core/identity/f;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/identity/j;->c:Lcom/google/android/apps/youtube/core/identity/h;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/identity/j;->a:Lcom/google/android/apps/youtube/core/identity/f;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/identity/j;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    const/4 v2, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/j;->c:Lcom/google/android/apps/youtube/core/identity/h;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/identity/h;->a(Lcom/google/android/apps/youtube/core/identity/h;)Lcom/google/android/apps/youtube/core/identity/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/j;->a:Lcom/google/android/apps/youtube/core/identity/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/identity/b;->a(Lcom/google/android/apps/youtube/core/identity/f;)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/j;->a:Lcom/google/android/apps/youtube/core/identity/f;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/f;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/j;->a:Lcom/google/android/apps/youtube/core/identity/f;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/f;->a()Ljava/lang/String;

    move-result-object v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/identity/j;->c:Lcom/google/android/apps/youtube/core/identity/h;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/identity/h;->d(Lcom/google/android/apps/youtube/core/identity/h;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/identity/j;->c:Lcom/google/android/apps/youtube/core/identity/h;

    invoke-static {v4}, Lcom/google/android/apps/youtube/core/identity/h;->b(Lcom/google/android/apps/youtube/core/identity/h;)Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->d()Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {v4, v0, v1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/util/Pair;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/j;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v3, v0, v1}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_1
    return-void

    :cond_0
    move-object v1, v2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/j;->b:Lcom/google/android/apps/youtube/common/a/b;

    const/4 v1, 0x0

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Authentication unsuccessful."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, v3}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/j;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_1

    :catch_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/j;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_1
.end method
