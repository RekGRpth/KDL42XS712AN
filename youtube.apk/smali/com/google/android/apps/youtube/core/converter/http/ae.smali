.class public final Lcom/google/android/apps/youtube/core/converter/http/ae;
.super Lcom/google/android/apps/youtube/core/converter/http/ap;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/apps/youtube/core/converter/e;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/converter/n;)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/converter/http/ap;-><init>(Lcom/google/android/apps/youtube/core/converter/n;)V

    const-string v0, "/feed"

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/converter/g;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    const-string v1, "/feed/entry"

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/am;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/core/converter/http/am;-><init>(Lcom/google/android/apps/youtube/core/converter/http/ae;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    const-string v1, "/feed/entry/author/name"

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/al;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/core/converter/http/al;-><init>(Lcom/google/android/apps/youtube/core/converter/http/ae;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    const-string v1, "/feed/entry/author/uri"

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/ak;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/core/converter/http/ak;-><init>(Lcom/google/android/apps/youtube/core/converter/http/ae;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    const-string v1, "/feed/entry/category"

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/aj;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/core/converter/http/aj;-><init>(Lcom/google/android/apps/youtube/core/converter/http/ae;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    const-string v1, "/feed/entry/updated"

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/ai;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/core/converter/http/ai;-><init>(Lcom/google/android/apps/youtube/core/converter/http/ae;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    const-string v1, "/feed/entry/yt:groupId"

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/ah;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/core/converter/http/ah;-><init>(Lcom/google/android/apps/youtube/core/converter/http/ae;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    const-string v1, "/feed/entry/yt:videoid"

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/ag;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/core/converter/http/ag;-><init>(Lcom/google/android/apps/youtube/core/converter/http/ae;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    const-string v1, "/feed/entry/yt:username"

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/af;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/core/converter/http/af;-><init>(Lcom/google/android/apps/youtube/core/converter/http/ae;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    move-result-object v0

    const-string v1, "/feed/entry/link"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/entry"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/core/converter/http/fp;->d(Lcom/google/android/apps/youtube/core/converter/f;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/apps/youtube/core/converter/http/an;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/core/converter/http/an;-><init>(Lcom/google/android/apps/youtube/core/converter/http/ae;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/converter/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/converter/r;)Lcom/google/android/apps/youtube/core/converter/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/converter/f;->a()Lcom/google/android/apps/youtube/core/converter/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/ae;->c:Lcom/google/android/apps/youtube/core/converter/e;

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/apps/youtube/core/converter/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/ae;->c:Lcom/google/android/apps/youtube/core/converter/e;

    return-object v0
.end method
