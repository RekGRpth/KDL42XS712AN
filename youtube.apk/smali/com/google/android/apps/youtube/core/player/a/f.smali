.class public final Lcom/google/android/apps/youtube/core/player/a/f;
.super Lorg/apache/http/entity/AbstractHttpEntity;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/exoplayer/upstream/j;

.field private final b:Lcom/google/android/exoplayer/upstream/i;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/j;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lorg/apache/http/entity/AbstractHttpEntity;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/a/f;->b:Lcom/google/android/exoplayer/upstream/i;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/a/f;->a:Lcom/google/android/exoplayer/upstream/j;

    const-string v0, "contentType cannot be empty."

    invoke-static {p3, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/a/f;->setContentType(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getContent()Ljava/io/InputStream;
    .locals 3

    new-instance v0, Lcom/google/android/exoplayer/e/c;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/a/f;->b:Lcom/google/android/exoplayer/upstream/i;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/a/f;->a:Lcom/google/android/exoplayer/upstream/j;

    invoke-direct {v0, v1, v2}, Lcom/google/android/exoplayer/e/c;-><init>(Lcom/google/android/exoplayer/upstream/i;Lcom/google/android/exoplayer/upstream/j;)V

    return-object v0
.end method

.method public final getContentLength()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/a/f;->a:Lcom/google/android/exoplayer/upstream/j;

    iget-wide v0, v0, Lcom/google/android/exoplayer/upstream/j;->e:J

    return-wide v0
.end method

.method public final isRepeatable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final isStreaming()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/a/f;->getContent()Ljava/io/InputStream;

    move-result-object v0

    :try_start_0
    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/common/fromguava/a;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    throw v1
.end method
