.class public final Lcom/google/android/apps/youtube/core/converter/http/dh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/common/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/a/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/dh;->a:Lcom/google/android/apps/youtube/common/a/b;

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/dh;->a:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    move-object v3, p1

    check-cast v3, Ljava/lang/String;

    check-cast p2, Ljava/util/List;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v8

    move v6, v0

    :goto_0
    if-ge v6, v8, :cond_0

    invoke-interface {p2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    iget-object v0, v5, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v1, v5, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageName:Ljava/lang/String;

    iget-object v2, v5, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->trackName:Ljava/lang/String;

    iget v4, v5, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->format:I

    iget-object v5, v5, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->vssId:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->create(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/dh;->a:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, v3, v7}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
