.class public final Lcom/google/android/apps/youtube/core/client/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private final b:Ljava/util/regex/Pattern;

.field private final c:Lcom/google/android/apps/youtube/datalib/e/b;

.field private final d:Lcom/google/android/apps/youtube/datalib/e/b;

.field private final e:Lcom/google/android/apps/youtube/common/e/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "^NO_MATCH_REGEX$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/core/client/j;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/datalib/e/b;Ljava/util/regex/Pattern;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/j;->c:Lcom/google/android/apps/youtube/datalib/e/b;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/j;->d:Lcom/google/android/apps/youtube/datalib/e/b;

    if-nez p3, :cond_0

    sget-object p3, Lcom/google/android/apps/youtube/core/client/j;->a:Ljava/util/regex/Pattern;

    :cond_0
    iput-object p3, p0, Lcom/google/android/apps/youtube/core/client/j;->b:Ljava/util/regex/Pattern;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/j;->e:Lcom/google/android/apps/youtube/common/e/b;

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/e/f;
    .locals 3

    const v2, 0x37046bc

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/j;->b:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/j;->c:Lcom/google/android/apps/youtube/datalib/e/b;

    invoke-static {p2, v2}, Lcom/google/android/apps/youtube/datalib/e/b;->a(Ljava/lang/String;I)Lcom/google/android/apps/youtube/datalib/e/f;

    move-result-object v0

    :goto_0
    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/e/f;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/e/f;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/j;->d:Lcom/google/android/apps/youtube/datalib/e/b;

    invoke-static {p2, v2}, Lcom/google/android/apps/youtube/datalib/e/b;->a(Ljava/lang/String;I)Lcom/google/android/apps/youtube/datalib/e/f;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/e/f;Lcom/android/volley/n;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/j;->b:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/e/f;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/j;->c:Lcom/google/android/apps/youtube/datalib/e/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/datalib/e/b;->a(Lcom/google/android/apps/youtube/datalib/e/f;Lcom/android/volley/n;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/e/f;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".doubleclick.net"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "doubleclick.net"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/e/f;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/e/f;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ts"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/j;->e:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/datalib/e/f;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/e/f;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/j;->d:Lcom/google/android/apps/youtube/datalib/e/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/datalib/e/b;->a(Lcom/google/android/apps/youtube/datalib/e/f;Lcom/android/volley/n;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
