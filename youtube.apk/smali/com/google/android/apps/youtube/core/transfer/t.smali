.class public final Lcom/google/android/apps/youtube/core/transfer/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

.field public d:I

.field public e:J

.field public f:J

.field public g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

.field public h:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

.field public i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "filePath may not be empty"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    const-string v0, "networkUri may not be empty"

    invoke-static {p3, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/t;->b:Ljava/lang/String;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/t;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->PENDING:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/t;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/transfer/t;->d:I

    iput-wide v1, p0, Lcom/google/android/apps/youtube/core/transfer/t;->e:J

    iput-wide v1, p0, Lcom/google/android/apps/youtube/core/transfer/t;->f:J

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/t;->h:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/transfer/t;->i:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;
    .locals 12

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/t;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/t;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/transfer/t;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    iget v4, p0, Lcom/google/android/apps/youtube/core/transfer/t;->d:I

    iget-wide v5, p0, Lcom/google/android/apps/youtube/core/transfer/t;->e:J

    iget-wide v7, p0, Lcom/google/android/apps/youtube/core/transfer/t;->f:J

    iget-object v9, p0, Lcom/google/android/apps/youtube/core/transfer/t;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    iget-object v10, p0, Lcom/google/android/apps/youtube/core/transfer/t;->h:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    iget-object v11, p0, Lcom/google/android/apps/youtube/core/transfer/t;->i:Ljava/lang/String;

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;IJJLcom/google/android/apps/youtube/datalib/model/transfer/a;Lcom/google/android/apps/youtube/datalib/model/transfer/a;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/t;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/t;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->FAILED:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
