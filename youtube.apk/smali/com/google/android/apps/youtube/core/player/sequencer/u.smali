.class public final Lcom/google/android/apps/youtube/core/player/sequencer/u;
.super Lcom/google/android/apps/youtube/core/player/sequencer/a;
.source "SourceFile"


# instance fields
.field private final l:Lcom/google/android/apps/youtube/core/player/fetcher/e;

.field private final m:Landroid/os/Handler;

.field private final n:Ljava/util/concurrent/Executor;

.field private o:Lcom/google/a/a/a/a/ag;

.field private p:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

.field private q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

.field private volatile r:Lcom/google/android/apps/youtube/core/player/sequencer/y;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/Director;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;)V
    .locals 8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p8

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/youtube/core/player/sequencer/a;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;)V

    invoke-static/range {p9 .. p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->n:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p9

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->currentPlaybackPair:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-object/from16 v0, p9

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->currentWatchNextResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    move-object/from16 v0, p9

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->currentPlaybackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->p:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    move-object/from16 v0, p9

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->pendingPlaybackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    move-object/from16 v0, p9

    iget-boolean v1, v0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->shuffle:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->k:Z

    move-object/from16 v0, p9

    iget-boolean v1, v0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->loop:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->j:Z

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/player/fetcher/e;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->l:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->m:Landroid/os/Handler;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->NEW:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->i()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V
    .locals 7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p8

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/player/sequencer/a;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;)V

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->n:Ljava/util/concurrent/Executor;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/fetcher/e;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->l:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->m:Landroid/os/Handler;

    invoke-static/range {p9 .. p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->NEW:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->i()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/sequencer/u;)Lcom/google/android/apps/youtube/core/player/fetcher/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->l:Lcom/google/android/apps/youtube/core/player/fetcher/e;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/sequencer/u;Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->p:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    return-object p1
.end method

.method private a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Z)V
    .locals 2

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a()V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_LOADING:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Malformed WatchEndpoint [videoId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",playlistId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",playlistIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/core/player/sequencer/y;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-direct {v0, p0, v1, p2}, Lcom/google/android/apps/youtube/core/player/sequencer/y;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/u;Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Z)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->r:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->n:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->r:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/sequencer/u;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->m:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->r:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->r:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->r:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_LOADING:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->NEW:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V
    .locals 4

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->JUMP:Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->b:Lcom/google/android/apps/youtube/core/player/sequencer/r;

    invoke-interface {v1, v0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/r;->a(ZI)V

    const/4 v0, 0x1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/event/s;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/youtube/core/player/event/s;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Z)V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->k:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q()V

    return-void
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->j:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q()V

    return-void
.end method

.method protected final b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final c()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->p:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->p:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoId()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected final d()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->p:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->p:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistIndex()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistIndex()I

    move-result v0

    goto :goto_0
.end method

.method protected final e()[B
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->p:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->p:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getClickTrackingParams()[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getClickTrackingParams()[B

    move-result-object v0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->p:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->p:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistIndex()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final l()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->l()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->b:Lcom/google/android/apps/youtube/core/player/sequencer/r;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/r;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Z)V

    goto :goto_0
.end method

.method public final m()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->r:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->r:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/sequencer/w;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/core/player/sequencer/w;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/u;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a(Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    iget-object v0, v0, Lcom/google/a/a/a/a/ag;->d:Lcom/google/a/a/a/a/kz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    iget-object v0, v0, Lcom/google/a/a/a/a/ag;->d:Lcom/google/a/a/a/a/kz;

    iget-object v0, v0, Lcom/google/a/a/a/a/kz;->i:Lcom/google/a/a/a/a/wb;

    if-eqz v0, :cond_1

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->m()V

    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    iget-object v1, v1, Lcom/google/a/a/a/a/ag;->d:Lcom/google/a/a/a/a/kz;

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->PLAYLISTS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Lcom/google/a/a/a/a/kz;Lcom/google/android/apps/youtube/core/client/WatchFeature;)V

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Z)V

    :cond_1
    return-void
.end method

.method public final n()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->r:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->r:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/sequencer/x;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/core/player/sequencer/x;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/u;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a(Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    iget-object v0, v0, Lcom/google/a/a/a/a/ag;->e:Lcom/google/a/a/a/a/kz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    iget-object v0, v0, Lcom/google/a/a/a/a/ag;->e:Lcom/google/a/a/a/a/kz;

    iget-object v0, v0, Lcom/google/a/a/a/a/kz;->i:Lcom/google/a/a/a/a/wb;

    if-eqz v0, :cond_1

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->n()V

    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    iget-object v1, v1, Lcom/google/a/a/a/a/ag;->e:Lcom/google/a/a/a/a/kz;

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->PLAYLISTS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Lcom/google/a/a/a/a/kz;Lcom/google/android/apps/youtube/core/client/WatchFeature;)V

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Z)V

    :cond_1
    return-void
.end method

.method public final o()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->r:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->r:Lcom/google/android/apps/youtube/core/player/sequencer/y;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/sequencer/v;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/core/player/sequencer/v;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/u;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/y;->a(Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    iget-object v0, v0, Lcom/google/a/a/a/a/ag;->c:Lcom/google/a/a/a/a/kz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    iget-object v0, v0, Lcom/google/a/a/a/a/ag;->c:Lcom/google/a/a/a/a/kz;

    iget-object v0, v0, Lcom/google/a/a/a/a/kz;->i:Lcom/google/a/a/a/a/wb;

    if-eqz v0, :cond_1

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->o()V

    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    iget-object v1, v1, Lcom/google/a/a/a/a/ag;->c:Lcom/google/a/a/a/a/kz;

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->PLAYLISTS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Lcom/google/a/a/a/a/kz;Lcom/google/android/apps/youtube/core/client/WatchFeature;)V

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Z)V

    :cond_1
    return-void
.end method

.method protected final o_()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->p:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->p:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlayerParams()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlayerParams()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final p()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/a;->p()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Z)V

    :cond_0
    return-void
.end method

.method public final p_()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Z)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final q()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->j:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->isLoopSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->j:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->k:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->isShuffleSupported()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    :goto_3
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->k:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->j:Z

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->k:Z

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->b:Lcom/google/android/apps/youtube/core/player/sequencer/r;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/core/player/sequencer/r;->u()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->getAutoplaySet(ZZZ)Lcom/google/a/a/a/a/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/u;->h()V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3
.end method

.method public final q_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    iget-object v0, v0, Lcom/google/a/a/a/a/ag;->d:Lcom/google/a/a/a/a/kz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic r()Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerState;
    .locals 7

    new-instance v0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->i:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->p:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iget-boolean v5, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->k:Z

    iget-boolean v6, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->j:Z

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;ZZ)V

    return-object v0
.end method

.method public final r_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    iget-object v0, v0, Lcom/google/a/a/a/a/ag;->e:Lcom/google/a/a/a/a/kz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_WATCH_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->p:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->p:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->q:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final s_()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/u;->o:Lcom/google/a/a/a/a/ag;

    iget-object v0, v0, Lcom/google/a/a/a/a/ag;->c:Lcom/google/a/a/a/a/kz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
