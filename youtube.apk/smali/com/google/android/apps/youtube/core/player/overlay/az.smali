.class public final Lcom/google/android/apps/youtube/core/player/overlay/az;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/am;

.field private final b:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/am;Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/am;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/az;->a:Lcom/google/android/apps/youtube/core/player/am;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/az;->b:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/ba;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/ba;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/az;B)V

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/core/player/am;->setListener(Lcom/google/android/apps/youtube/core/player/an;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/overlay/az;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/az;->b:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    return-object v0
.end method


# virtual methods
.method public final handleYouTubePlayerStateEvent(Lcom/google/android/apps/youtube/core/player/event/af;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/af;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/az;->a:Lcom/google/android/apps/youtube/core/player/am;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/am;->setKeepScreenOn(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/az;->a:Lcom/google/android/apps/youtube/core/player/am;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/am;->setKeepScreenOn(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
