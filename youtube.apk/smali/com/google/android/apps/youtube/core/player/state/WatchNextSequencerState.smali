.class public Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerState;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final currentPlaybackPair:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

.field public currentPlaybackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

.field public final currentWatchNextResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

.field public final loop:Z

.field public pendingPlaybackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

.field public final shuffle:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/state/d;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/player/state/d;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->currentPlaybackPair:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->currentWatchNextResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->currentPlaybackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->pendingPlaybackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->shuffle:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->loop:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;ZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->currentPlaybackPair:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->currentWatchNextResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->currentPlaybackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iput-object p4, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->pendingPlaybackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iput-boolean p5, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->shuffle:Z

    iput-boolean p6, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->loop:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->currentPlaybackPair:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->currentWatchNextResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->currentPlaybackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->pendingPlaybackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->shuffle:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/state/WatchNextSequencerState;->loop:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
