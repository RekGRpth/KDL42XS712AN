.class public Lcom/google/android/apps/youtube/core/player/state/GDataPlaylistSequencerState;
.super Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final nextPageRequest:Lcom/google/android/apps/youtube/core/async/GDataRequest;

.field public final playlistId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/state/a;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/player/state/a;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/state/GDataPlaylistSequencerState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;-><init>(Landroid/os/Parcel;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/GDataPlaylistSequencerState;->nextPageRequest:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/GDataPlaylistSequencerState;->playlistId:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>([Ljava/lang/String;[BLjava/lang/String;IILcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/lang/String;Z)V
    .locals 8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move/from16 v7, p9

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;-><init>([Ljava/lang/String;[BLjava/lang/String;IILcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Z)V

    invoke-static/range {p8 .. p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/GDataPlaylistSequencerState;->playlistId:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/youtube/core/player/state/GDataPlaylistSequencerState;->nextPageRequest:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/GDataPlaylistSequencerState;->nextPageRequest:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/GDataPlaylistSequencerState;->nextPageRequest:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/state/GDataPlaylistSequencerState;->playlistId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
