.class final Lcom/google/android/apps/youtube/core/player/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/ae;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/player/ae;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/ah;->a:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/player/ae;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/player/ah;-><init>(Lcom/google/android/apps/youtube/core/player/ae;)V

    return-void
.end method


# virtual methods
.method public final onAudioFocusChange(I)V
    .locals 3

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ah;->a:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->l()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ah;->a:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/core/player/ae;->a(Lcom/google/android/apps/youtube/core/player/ae;Z)Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ah;->a:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/ae;->d(Lcom/google/android/apps/youtube/core/player/ae;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v0

    const v1, 0x3dcccccd    # 0.1f

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ah;->a:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/core/player/ae;->a(Lcom/google/android/apps/youtube/core/player/ae;Z)Z

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ah;->a:Lcom/google/android/apps/youtube/core/player/ae;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/core/player/ae;->a(Lcom/google/android/apps/youtube/core/player/ae;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/ah;->a:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/ae;->d(Lcom/google/android/apps/youtube/core/player/ae;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/x;->a(F)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
