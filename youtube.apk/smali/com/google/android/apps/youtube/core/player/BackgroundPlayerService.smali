.class public Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

.field private b:Lcom/google/android/apps/youtube/core/player/ae;

.field private c:Lcom/google/android/apps/youtube/common/c/a;

.field private d:Z

.field private e:J

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->e:J

    return-wide v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcom/google/android/apps/youtube/core/player/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b:Lcom/google/android/apps/youtube/core/player/ae;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->d:Z

    return v0
.end method

.method private handlePlaybackServiceException(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->ERROR:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->stopForeground(Z)V

    return-void
.end method

.method private handleSequencerHasPreviousNextEvent(Lcom/google/android/apps/youtube/core/player/event/u;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/u;->a()Z

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/u;->b()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a(ZZ)V

    return-void
.end method

.method private handleVideoControlsStateEvent(Lcom/google/android/apps/youtube/core/player/event/z;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->STOPPED:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/f;->a:[I

    iget-object v2, p1, Lcom/google/android/apps/youtube/core/player/event/z;->a:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;)V

    return-void

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->BUFFERING:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->PLAYING:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->PAUSED:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->ENDED:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->ERROR:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method private handleVideoStageEvent(Lcom/google/android/apps/youtube/core/player/event/ac;)V
    .locals 5
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->onAd()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->b(Z)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    new-array v3, v1, [Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v4, v3, v2

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isIn([Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->stopForeground(Z)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    new-array v1, v1, [Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isIn([Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->f:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/Director$VideoStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->f:Z

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private handleVideoTimeEvent(Lcom/google/android/apps/youtube/core/player/event/ae;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ae;->a()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->e:J

    return-void
.end method

.method private handleYouTubePlayerStateEvent(Lcom/google/android/apps/youtube/core/player/event/af;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/af;->a()I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/af;->a()I

    move-result v0

    const/4 v2, 0x6

    if-ne v0, v2, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->d:Z

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/af;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->w()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a()V

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->stopForeground(Z)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 15

    const/4 v13, 0x0

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->getApplication()Landroid/app/Application;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/youtube/core/BaseApplication;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/BaseApplication;->y()Lcom/google/android/apps/youtube/core/a;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/apps/youtube/core/a;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/BaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v14}, Lcom/google/android/apps/youtube/core/a;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v2

    invoke-virtual {v14}, Lcom/google/android/apps/youtube/core/a;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v3

    invoke-virtual {v14}, Lcom/google/android/apps/youtube/core/a;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v4

    invoke-virtual {v14}, Lcom/google/android/apps/youtube/core/a;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v5

    invoke-virtual {v14}, Lcom/google/android/apps/youtube/core/a;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v6

    invoke-virtual {v14}, Lcom/google/android/apps/youtube/core/a;->aY()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v14}, Lcom/google/android/apps/youtube/core/a;->g()Ljava/lang/Class;

    move-result-object v8

    new-instance v9, Lcom/google/android/apps/youtube/core/player/g;

    invoke-direct {v9, p0, v13}, Lcom/google/android/apps/youtube/core/player/g;-><init>(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;B)V

    sget v10, Lcom/google/android/youtube/p;->z:I

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    invoke-virtual {v14}, Lcom/google/android/apps/youtube/core/a;->aY()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v13

    invoke-virtual {p0, v10, v11}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    sget v11, Lcom/google/android/youtube/h;->ae:I

    const/4 v12, 0x0

    invoke-direct/range {v0 .. v13}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/common/network/h;Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/apps/youtube/core/player/notification/j;Ljava/lang/String;ILcom/google/android/apps/youtube/core/player/notification/c;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-virtual {v14}, Lcom/google/android/apps/youtube/core/a;->ac()Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b:Lcom/google/android/apps/youtube/core/player/ae;

    sget v0, Lcom/google/android/exoplayer/e/k;->a:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/player/e;-><init>(Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->k()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    const-string v0, "background_mode"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->f:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->v()V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a()V

    :cond_0
    :goto_0
    const/4 v0, 0x2

    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->a:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->b()V

    goto :goto_0
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->b:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->E()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/BackgroundPlayerService;->stopSelf()V

    return-void
.end method
