.class public Lcom/google/android/apps/youtube/core/ax;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/au;


# instance fields
.field protected final b:Landroid/content/ContentResolver;

.field protected final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/ax;->b:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/ax;->c:Ljava/lang/String;

    return-void
.end method

.method private static a(Lorg/json/JSONObject;)Lcom/google/a/a/a/a/ih;
    .locals 5

    new-instance v1, Lcom/google/a/a/a/a/ih;

    invoke-direct {v1}, Lcom/google/a/a/a/a/ih;-><init>()V

    const-string v0, "capability"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, Lcom/google/a/a/a/a/ih;->b:I

    const-string v0, "features"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, v1, Lcom/google/a/a/a/a/ih;->c:[I

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, v1, Lcom/google/a/a/a/a/ih;->c:[I

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getInt(I)I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static a(Ljava/lang/String;)[I
    .locals 6

    const/4 v1, 0x0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-array v0, v1, [I

    :cond_0
    return-object v0

    :cond_1
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v0, v3

    new-array v0, v0, [I

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    :try_start_0
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v0, v2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Improperly formatted experiment ID string encountered."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static b(Ljava/lang/String;)Lcom/google/a/a/a/a/bg;
    .locals 7

    const/4 v1, 0x0

    new-instance v0, Lcom/google/a/a/a/a/bg;

    invoke-direct {v0}, Lcom/google/a/a/a/a/bg;-><init>()V

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    return-object v0

    :cond_1
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v2, "supportedCapabilities"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "supportedCapabilities"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v2

    new-array v2, v2, [Lcom/google/a/a/a/a/ih;

    iput-object v2, v0, Lcom/google/a/a/a/a/bg;->c:[Lcom/google/a/a/a/a/ih;

    move v2, v1

    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    iget-object v5, v0, Lcom/google/a/a/a/a/bg;->c:[Lcom/google/a/a/a/a/ih;

    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/youtube/core/ax;->a(Lorg/json/JSONObject;)Lcom/google/a/a/a/a/ih;

    move-result-object v6

    aput-object v6, v5, v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Improper JSON syntax encountered in capabilities override."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_2
    :try_start_2
    const-string v2, "disabledCapabilities"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "disabledCapabilities"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    new-array v3, v3, [Lcom/google/a/a/a/a/ih;

    iput-object v3, v0, Lcom/google/a/a/a/a/bg;->d:[Lcom/google/a/a/a/a/ih;

    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_0

    iget-object v3, v0, Lcom/google/a/a/a/a/bg;->d:[Lcom/google/a/a/a/a/ih;

    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/youtube/core/ax;->a(Lorg/json/JSONObject;)Lcom/google/a/a/a/a/ih;

    move-result-object v4

    aput-object v4, v3, v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Improper capabilities override syntax encountered."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final A()I
    .locals 2

    const-string v0, "analytics_sample_ratio"

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/ax;->g()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2

    const-string v0, "analytics_category_suffix"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final C()Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;
    .locals 2

    const-string v0, "gdata_version"

    const-string v1, "2.1"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;->parse(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;

    move-result-object v0

    return-object v0
.end method

.method public final D()Z
    .locals 2

    const-string v0, "enable_device_retention"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final E()Ljava/lang/String;
    .locals 2

    const-string v0, "experiment_id"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final F()Z
    .locals 2

    const-string v0, "use_innertube_playlist_service"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final G()Z
    .locals 2

    const-string v0, "device_supports_3d_playback"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final H()Landroid/util/Pair;
    .locals 4

    const/4 v0, 0x0

    const-string v1, "doritos_cookie_domain"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "doritos_cookie_name"

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final I()Z
    .locals 2

    const-string v0, "device_supports_720p_playback"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final J()Z
    .locals 2

    const-string v0, "can_use_texture_surface"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final K()I
    .locals 2

    const-string v0, "pudl_ad_frequency_cap"

    const/16 v1, 0x1a4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final L()I
    .locals 2

    const-string v0, "pudl_ad_asset_frequency_cap"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final M()I
    .locals 2

    const-string v0, "pudl_ad_asset_time_to_live"

    const v1, 0x3f480

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final N()I
    .locals 2

    const-string v0, "pudl_ad_lact_skippable"

    const/16 v1, 0x708

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final O()I
    .locals 2

    const-string v0, "pudl_ad_lact_nonskippable"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final P()Z
    .locals 2

    const-string v0, "use_offline_http_service_for_pings"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final Q()I
    .locals 2

    const-string v0, "offline_resync_continuation_deferred_service_threshold_seconds"

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final R()Z
    .locals 2

    const-string v0, "attempt_offline_resync_on_expired_continuation"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final S()I
    .locals 2

    const-string v0, "maximum_consecutive_skipped_unplayable_videos"

    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final T()Ljava/lang/String;
    .locals 2

    const-string v0, "adsense_query_domain"

    const-string v1, "googleads.g.doubleclick.net"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final U()Ljava/lang/String;
    .locals 2

    const-string v0, "adsense_query_domain"

    const-string v1, "/pagead/ads"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final V()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ax;->b:Landroid/content/ContentResolver;

    const-string v1, "device_country"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final W()Z
    .locals 2

    const-string v0, "companion_ad_enabled"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected final a(Ljava/lang/String;I)I
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ax;->b:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/ax;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method protected final a(Ljava/lang/String;J)J
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ax;->b:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/ax;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2, p3}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ax;->b:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/ax;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Ljava/lang/String;Z)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/ax;->b:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/ax;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Context;)[I
    .locals 2

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "innertube_experiment_ids"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;)[I

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/Context;)Lcom/google/a/a/a/a/bg;
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "innertube_capability_overrides"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/ax;->b(Ljava/lang/String;)Lcom/google/a/a/a/a/bg;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v1, Lcom/google/a/a/a/a/bg;->e:Lcom/google/a/a/a/a/bf;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/a/a/a/a/ih;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ih;-><init>()V

    const v2, 0x31e0800

    iput v2, v0, Lcom/google/a/a/a/a/ih;->b:I

    new-array v2, v5, [I

    const v3, 0x2e6ea0a

    aput v3, v2, v4

    iput-object v2, v0, Lcom/google/a/a/a/a/ih;->c:[I

    iget-object v2, v1, Lcom/google/a/a/a/a/bg;->c:[Lcom/google/a/a/a/a/ih;

    new-array v3, v5, [Lcom/google/a/a/a/a/ih;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/e/c;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/a/a/a/a/ih;

    iput-object v0, v1, Lcom/google/a/a/a/a/bg;->c:[Lcom/google/a/a/a/a/ih;

    :cond_1
    return-object v1
.end method

.method public final c(Landroid/content/Context;)Z
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v0, "csi_enabled"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 2

    const-string v0, "analytics_property_id"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected g()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final g_()Z
    .locals 2

    const-string v0, "is_low_end_mobile_network"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public t()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final x()J
    .locals 4

    const-string v0, "task_master_delay_before_startup_millis"

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final y()Z
    .locals 2

    const-string v0, "analytics_enabled"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final z()I
    .locals 2

    const-string v0, "analytics_update_secs"

    const/16 v1, 0x1e

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/core/ax;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
