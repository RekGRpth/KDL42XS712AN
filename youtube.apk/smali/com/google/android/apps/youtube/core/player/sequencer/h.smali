.class public final Lcom/google/android/apps/youtube/core/player/sequencer/h;
.super Lcom/google/android/apps/youtube/core/player/sequencer/s;
.source "SourceFile"


# instance fields
.field private final r:Lcom/google/android/apps/youtube/core/async/af;

.field private final s:Landroid/os/Handler;

.field private final t:Lcom/google/android/apps/youtube/core/player/sequencer/i;

.field private final u:Ljava/lang/String;

.field private v:Lcom/google/android/apps/youtube/core/async/GDataRequest;

.field private w:Lcom/google/android/apps/youtube/common/a/d;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/player/state/GDataPlaylistSequencerState;)V
    .locals 9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/youtube/core/player/sequencer/s;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/player/state/VideoIdsSequencerState;)V

    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface/range {p7 .. p7}, Lcom/google/android/apps/youtube/core/client/bc;->c()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->r:Lcom/google/android/apps/youtube/core/async/af;

    move-object/from16 v0, p8

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/player/state/GDataPlaylistSequencerState;->nextPageRequest:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->v:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-object/from16 v0, p8

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/player/state/GDataPlaylistSequencerState;->playlistId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->u:Ljava/lang/String;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->s:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/sequencer/i;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/core/player/sequencer/i;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/h;B)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->t:Lcom/google/android/apps/youtube/core/player/sequencer/i;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->i()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/bc;Ljava/lang/String;I)V
    .locals 12

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v8

    sget-object v10, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a:[B

    const-string v11, ""

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v9, p9

    invoke-direct/range {v1 .. v11}, Lcom/google/android/apps/youtube/core/player/sequencer/s;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/r;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/aw;Ljava/util/List;I[BLjava/lang/String;)V

    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p8 .. p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->u:Ljava/lang/String;

    invoke-interface/range {p7 .. p7}, Lcom/google/android/apps/youtube/core/client/bc;->c()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->r:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface/range {p7 .. p7}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v1

    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->g(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->v:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->s:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/sequencer/i;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/core/player/sequencer/i;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/h;B)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->t:Lcom/google/android/apps/youtube/core/player/sequencer/i;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->NEW:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->i()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/sequencer/h;Lcom/google/android/apps/youtube/common/a/d;)Lcom/google/android/apps/youtube/common/a/d;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->w:Lcom/google/android/apps/youtube/common/a/d;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/sequencer/h;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->v:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/sequencer/h;Lcom/google/android/apps/youtube/core/async/GDataRequest;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->v:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/sequencer/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->u()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/player/sequencer/h;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->v()V

    return-void
.end method

.method private u()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->t:Lcom/google/android/apps/youtube/core/player/sequencer/i;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->w:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->r:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->v:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->s:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->w:Lcom/google/android/apps/youtube/common/a/d;

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method private v()V
    .locals 8

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->q:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->q:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->l:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/sequencer/t;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/player/sequencer/t;-><init>(Lcom/google/android/apps/youtube/core/player/sequencer/s;)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->o:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->a:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->l:[Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->q:I

    aget-object v1, v1, v2

    sget-object v2, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a:[B

    const-string v3, ""

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->u:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->q:I

    const/4 v6, -0x1

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->o:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/common/a/b;)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->SEQUENCE_EMPTY:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/s;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->w:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->w:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->w:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    return-void
.end method

.method protected final b(I)I
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->r:Lcom/google/android/apps/youtube/core/async/af;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->v:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->l:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_1
    return v0

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0

    :cond_1
    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1
.end method

.method protected final c(I)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->b(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->q:I

    if-gez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_LOADING:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->l:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->v()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->v:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->u()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->l:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->q:I

    iput v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->p:I

    sget-object v0, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->SEQUENCE_EMPTY:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/player/sequencer/h;->a(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->c:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/event/t;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/player/event/t;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected final d()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->g:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->VIDEO_PLAYBACK_LOADED:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->isOrPast(Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->p:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->q:I

    goto :goto_0
.end method

.method protected final e()[B
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a:[B

    return-object v0
.end method

.method public final r()Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerState;
    .locals 10

    new-instance v0, Lcom/google/android/apps/youtube/core/player/state/GDataPlaylistSequencerState;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->l:[Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->m:[B

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->n:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->p:I

    iget v5, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->q:I

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->v:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v8, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->u:Ljava/lang/String;

    iget-boolean v9, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->j:Z

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/youtube/core/player/state/GDataPlaylistSequencerState;-><init>([Ljava/lang/String;[BLjava/lang/String;IILcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->u:Ljava/lang/String;

    return-object v0
.end method

.method protected final t()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->p:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->l:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/sequencer/h;->v:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
