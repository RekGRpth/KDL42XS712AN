.class public final Lcom/google/android/apps/youtube/core/player/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/player/Interval;

.field private b:Lcom/google/android/apps/youtube/core/player/Interval$EdgeType;

.field private c:J


# virtual methods
.method public final a()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/player/v;->c:J

    return-wide v0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 4

    check-cast p1, Lcom/google/android/apps/youtube/core/player/v;

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/player/v;->c:J

    iget-wide v2, p1, Lcom/google/android/apps/youtube/core/player/v;->c:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/player/v;->c:J

    iget-wide v2, p1, Lcom/google/android/apps/youtube/core/player/v;->c:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/v;->a:Lcom/google/android/apps/youtube/core/player/Interval;

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/Interval;->d:I

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/player/v;->a:Lcom/google/android/apps/youtube/core/player/Interval;

    iget v1, v1, Lcom/google/android/apps/youtube/core/player/Interval;->d:I

    if-eq v0, v1, :cond_2

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/player/v;->a:Lcom/google/android/apps/youtube/core/player/Interval;

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/Interval;->d:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/v;->a:Lcom/google/android/apps/youtube/core/player/Interval;

    iget v1, v1, Lcom/google/android/apps/youtube/core/player/Interval;->d:I

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/v;->b:Lcom/google/android/apps/youtube/core/player/Interval$EdgeType;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Interval$EdgeType;->ordinal()I

    move-result v0

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/player/v;->b:Lcom/google/android/apps/youtube/core/player/Interval$EdgeType;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/Interval$EdgeType;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/v;->b:Lcom/google/android/apps/youtube/core/player/Interval$EdgeType;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/Interval$EdgeType;->ordinal()I

    move-result v0

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/player/v;->b:Lcom/google/android/apps/youtube/core/player/Interval$EdgeType;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/Interval$EdgeType;->ordinal()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/v;->a:Lcom/google/android/apps/youtube/core/player/Interval;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/Interval;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/player/v;->a:Lcom/google/android/apps/youtube/core/player/Interval;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/player/Interval;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/v;->b:Lcom/google/android/apps/youtube/core/player/Interval$EdgeType;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/Interval$EdgeType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/youtube/core/player/v;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
