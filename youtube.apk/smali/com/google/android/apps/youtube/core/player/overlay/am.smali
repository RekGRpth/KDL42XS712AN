.class public final Lcom/google/android/apps/youtube/core/player/overlay/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/al;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/overlay/ak;

.field private final b:Lcom/google/android/apps/youtube/core/player/ae;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/ak;Lcom/google/android/apps/youtube/core/player/ae;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/ak;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/am;->a:Lcom/google/android/apps/youtube/core/player/overlay/ak;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/ae;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/am;->b:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-interface {p1, p0}, Lcom/google/android/apps/youtube/core/player/overlay/ak;->setListener(Lcom/google/android/apps/youtube/core/player/overlay/al;)V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/am;->b:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->n()Z

    return-void
.end method

.method public final handleVideoStageEvent(Lcom/google/android/apps/youtube/core/player/event/ac;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/an;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/am;->a:Lcom/google/android/apps/youtube/core/player/overlay/ak;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ak;->d()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->isLive()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
