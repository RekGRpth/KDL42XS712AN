.class public final Lcom/google/android/apps/youtube/core/transfer/aa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/transfer/l;


# static fields
.field private static final a:Ljava/util/regex/Pattern;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/app/NotificationManager;

.field private final d:Lcom/google/android/apps/youtube/core/Analytics;

.field private final e:Lorg/apache/http/client/HttpClient;

.field private final f:Lcom/google/android/apps/youtube/core/client/bc;

.field private final g:Lcom/google/android/apps/youtube/core/identity/ak;

.field private final h:Lcom/google/android/apps/youtube/core/identity/l;

.field private final i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

.field private final j:Ljava/lang/String;

.field private final k:Lcom/google/android/apps/youtube/core/converter/http/fo;

.field private final l:Lcom/google/android/apps/youtube/core/transfer/m;

.field private final m:Ljava/util/concurrent/Executor;

.field private final n:Ljava/lang/Object;

.field private volatile o:Z

.field private volatile p:Lorg/apache/http/client/methods/HttpUriRequest;

.field private volatile q:Z

.field private r:J

.field private s:J

.field private final t:Landroid/os/ConditionVariable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "bytes=(\\d+)-(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/core/transfer/aa;->a:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/converter/http/fo;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;Lcom/google/android/apps/youtube/core/transfer/m;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->b:Landroid/content/Context;

    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->c:Landroid/app/NotificationManager;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->e:Lorg/apache/http/client/HttpClient;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->m:Ljava/util/concurrent/Executor;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->f:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/ak;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->g:Lcom/google/android/apps/youtube/core/identity/ak;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->h:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->d:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/converter/http/fo;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->k:Lcom/google/android/apps/youtube/core/converter/http/fo;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v0, p9, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->j:Ljava/lang/String;

    invoke-static {p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/m;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->l:Lcom/google/android/apps/youtube/core/transfer/m;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->n:Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->o:Z

    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->t:Landroid/os/ConditionVariable;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/transfer/aa;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->s:J

    return-wide p1
.end method

.method private a(Ljava/lang/String;)Landroid/net/Uri;
    .locals 7

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v6, 0x0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v6

    const-string v3, "_data=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "_id"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const-string v2, "content://media/external/video/media"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a()Lorg/apache/http/HttpResponse;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lorg/apache/http/client/methods/HttpPut;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    const-string v1, "Content-Range"

    const-string v2, "bytes */*"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->n:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->e:Lorg/apache/http/client/HttpClient;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    :goto_0
    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/aa;->b()V

    new-instance v1, Lcom/google/android/apps/youtube/core/transfer/TransferException;

    invoke-direct {v1, v0, v3}, Lcom/google/android/apps/youtube/core/transfer/TransferException;-><init>(Ljava/lang/Throwable;Z)V

    throw v1

    :catch_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->n:Ljava/lang/Object;

    monitor-enter v1

    :try_start_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "Range request was aborted"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_0
    :try_start_3
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    new-instance v2, Lcom/google/android/apps/youtube/core/transfer/TransferException;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/youtube/core/transfer/TransferException;-><init>(Ljava/lang/Throwable;Z)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1
.end method

.method private a(Ljava/io/InputStream;JZ)Lorg/apache/http/HttpResponse;
    .locals 10

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->q:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-wide/16 v0, 0x1

    add-long v1, p2, v0

    iget-wide v3, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->r:J

    const-wide/16 v5, 0x1

    sub-long/2addr v3, v5

    if-eqz p4, :cond_1

    const/4 v0, 0x0

    :goto_1
    int-to-long v5, v0

    sub-long/2addr v3, v5

    if-nez p4, :cond_2

    cmp-long v0, v1, v3

    if-lez v0, :cond_2

    new-instance v0, Lorg/apache/http/message/BasicHttpResponse;

    new-instance v1, Lorg/apache/http/message/BasicStatusLine;

    sget-object v2, Lorg/apache/http/HttpVersion;->HTTP_1_1:Lorg/apache/http/HttpVersion;

    const/16 v3, 0x134

    const-string v4, "Already uploaded all possible content for a gated upload."

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/http/message/BasicStatusLine;-><init>(Lorg/apache/http/ProtocolVersion;ILjava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/apache/http/message/BasicHttpResponse;-><init>(Lorg/apache/http/StatusLine;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->l:Lcom/google/android/apps/youtube/core/transfer/m;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v5, v5, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v0, v5, p2, p3}, Lcom/google/android/apps/youtube/core/transfer/m;->b(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->l:Lcom/google/android/apps/youtube/core/transfer/m;

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v5, v5, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    iget-wide v6, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->r:J

    invoke-interface {v0, v5, v6, v7}, Lcom/google/android/apps/youtube/core/transfer/m;->a(Ljava/lang/String;J)V

    new-instance v7, Lorg/apache/http/client/methods/HttpPut;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->b:Ljava/lang/String;

    invoke-direct {v7, v0}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    const-string v0, "Content-Type"

    const-string v5, "application/octet-stream"

    invoke-virtual {v7, v0, v5}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Content-Range"

    const-string v5, "bytes %d-%d/%d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v6, v8

    const/4 v8, 0x1

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v6, v8

    const/4 v3, 0x2

    iget-wide v8, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->r:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v0, v3}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p1, v1, v2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/TransferException;

    const-string v1, "unable to skip to upload position"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/transfer/TransferException;-><init>(Ljava/lang/String;Z)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/core/transfer/TransferException;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/youtube/core/transfer/TransferException;-><init>(Ljava/lang/Throwable;Z)V

    throw v1

    :cond_3
    :try_start_1
    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/ac;

    iget-wide v3, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->r:J

    sub-long v2, v3, v1

    if-eqz p4, :cond_4

    const/4 v1, 0x0

    :goto_2
    int-to-long v4, v1

    sub-long v3, v2, v4

    move-object v1, p0

    move-object v2, p1

    move-wide v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/core/transfer/ac;-><init>(Lcom/google/android/apps/youtube/core/transfer/aa;Ljava/io/InputStream;JJ)V

    invoke-virtual {v7, v0}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->n:Ljava/lang/Object;

    monitor-enter v1
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    iput-object v7, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->e:Lorg/apache/http/client/HttpClient;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    const/4 v1, 0x1

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_1
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/aa;->b()V

    new-instance v1, Lcom/google/android/apps/youtube/core/transfer/TransferException;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/youtube/core/transfer/TransferException;-><init>(Ljava/lang/Throwable;Z)V

    throw v1

    :catch_2
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->n:Ljava/lang/Object;

    monitor-enter v1

    :try_start_4
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v0, "Upload request was aborted"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    const/4 v0, 0x0

    monitor-exit v1

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_5
    :try_start_5
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    new-instance v2, Lcom/google/android/apps/youtube/core/transfer/TransferException;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/youtube/core/transfer/TransferException;-><init>(Ljava/lang/Throwable;Z)V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/transfer/aa;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/aa;->b()V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    .locals 10

    const/4 v1, 0x0

    const/4 v9, 0x1

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-eq v0, v2, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PROCESSING:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-ne v0, v2, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    const/16 v2, 0xa

    if-ge v0, v2, :cond_5

    add-int/lit8 v2, v0, 0x1

    const-wide/16 v3, 0x7530

    :try_start_0
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->h:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->g:Lcom/google/android/apps/youtube/core/identity/ak;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/ak;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->j:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->h:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->g:Lcom/google/android/apps/youtube/core/identity/ak;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/ak;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->j:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/youtube/h;->ag:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->b:Landroid/content/Context;

    sget v3, Lcom/google/android/youtube/p;->gs:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->b:Landroid/content/Context;

    sget v4, Lcom/google/android/youtube/p;->gs:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    iget-object v8, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/apps/youtube/common/e/p;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->b:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v6}, Landroid/content/Intent;->getFlags()I

    move-result v7

    const/high16 v8, 0x10000000

    or-int/2addr v7, v8

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v7, "authenticate"

    invoke-virtual {v6, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v7, "uploader_notification"

    invoke-virtual {v6, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v7, "feature"

    sget-object v8, Lcom/google/android/apps/youtube/core/client/WatchFeature;->UPLOAD_NOTIFICATION:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->b:Landroid/content/Context;

    const/high16 v8, 0x40000000    # 2.0f

    invoke-static {v7, v1, v6, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    new-instance v6, Landroid/app/Notification;

    invoke-direct {v6, v0, v2, v4, v5}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->b:Landroid/content/Context;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v4, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->title:Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0, v3, v2, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const/16 v0, 0x10

    iput v0, v6, Landroid/app/Notification;->flags:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->c:Landroid/app/NotificationManager;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v9, v6}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    :cond_2
    return-void

    :cond_3
    :try_start_2
    invoke-static {}, Lcom/google/android/apps/youtube/common/a/c;->a()Lcom/google/android/apps/youtube/common/a/c;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->f:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v4, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-interface {v3, v4, v0}, Lcom/google/android/apps/youtube/core/client/bc;->c(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v3, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v4, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PROCESSING:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-eq v3, v4, :cond_4

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->streams:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "Upload transcoding finished"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v0

    const-string v0, "Upload streams not found yet"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    move v0, v2

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->d:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v2, "UploadTranscodingWaitAbort"

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    const-string v0, "Upload streams not found, polling aborted"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_1
    move-exception v0

    goto/16 :goto_1
.end method

.method private static a(Lorg/apache/http/HttpResponse;)V
    .locals 1

    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->consumeContent()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/transfer/aa;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->n:Ljava/lang/Object;

    return-object v0
.end method

.method private b()V
    .locals 6

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->n:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->r:J

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v4, "metadata_updated"

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    int-to-long v4, v0

    sub-long/2addr v2, v4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v4, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->s:J

    cmp-long v0, v4, v2

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->p:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b(Lorg/apache/http/HttpResponse;)V
    .locals 14

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->l:Lcom/google/android/apps/youtube/core/transfer/m;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    iget-wide v3, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->r:J

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/transfer/m;->b(Ljava/lang/String;J)V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->k:Lcom/google/android/apps/youtube/core/converter/http/fo;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/converter/http/fo;->b(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-object v11, v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v2, "metadata_updated"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/apps/youtube/core/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    if-eqz v1, :cond_2

    :try_start_1
    const-string v1, "Executing metadata update"

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/apps/youtube/common/a/c;->a()Lcom/google/android/apps/youtube/common/a/c;

    move-result-object v12

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->h:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->g:Lcom/google/android/apps/youtube/core/identity/ak;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/ak;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const-string v1, "Error updating metadata, auth is null"

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    move-object v1, v11

    :goto_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/transfer/aa;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/apps/youtube/core/converter/ConverterException; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->f:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/client/bc;->h()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v2, "upload_start_time_millis"

    const-wide/16 v3, -0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;J)J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-lez v3, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long v1, v3, v1

    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->d:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v4, "UploadCompleted"

    const/4 v5, 0x0

    long-to-int v1, v1

    invoke-interface {v3, v4, v5, v1}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->l:Lcom/google/android/apps/youtube/core/transfer/m;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v3, v3, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->h:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/youtube/core/transfer/m;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)V

    return-void

    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->f:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v3, "upload_title"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v3, v3, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v4, "upload_description"

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v11, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->categoryTerm:Ljava/lang/String;

    iget-object v5, v11, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->categoryLabel:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v6, v6, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v7, "upload_keywords"

    invoke-virtual {v6, v7}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v7, v7, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v8, "upload_privacy"

    invoke-virtual {v7, v8}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    move-result-object v7

    iget-object v8, v11, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->accessControl:Ljava/util/Map;

    iget-object v9, v11, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->location:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v10, v10, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v13, "upload_location"

    invoke-virtual {v10, v13}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iget-object v11, v11, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->editUri:Landroid/net/Uri;

    invoke-interface/range {v1 .. v12}, Lcom/google/android/apps/youtube/core/client/bc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-virtual {v12}, Lcom/google/android/apps/youtube/common/a/c;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->h:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v3, "video_id"

    iget-object v4, v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/apps/youtube/core/converter/ConverterException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_0
    move-exception v1

    :try_start_3
    new-instance v1, Lcom/google/android/apps/youtube/core/transfer/TransferException;

    const-string v2, "Error updating video metadata after upload"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/youtube/core/transfer/TransferException;-><init>(Ljava/lang/String;Z)V

    throw v1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/android/apps/youtube/core/converter/ConverterException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_1
    move-exception v1

    const-string v2, "error parsing uploaded video"

    invoke-static {v2, v1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :cond_2
    :try_start_4
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->h:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v2, "video_id"

    iget-object v3, v11, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v11}, Lcom/google/android/apps/youtube/core/transfer/aa;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/google/android/apps/youtube/core/converter/ConverterException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_1

    :catch_2
    move-exception v1

    const-string v2, "error parsing uploaded video"

    invoke-static {v2, v1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :cond_3
    const-wide/16 v1, -0x1

    goto/16 :goto_2
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/transfer/aa;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->r:J

    return-wide v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/transfer/aa;)Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/core/transfer/aa;)Lcom/google/android/apps/youtube/core/transfer/m;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->l:Lcom/google/android/apps/youtube/core/transfer/m;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->q:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->m:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/transfer/ab;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/transfer/ab;-><init>(Lcom/google/android/apps/youtube/core/transfer/aa;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->t:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    return-void
.end method

.method public final run()V
    .locals 8

    const/16 v7, 0xc9

    const/16 v6, 0xc8

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Upload starting ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->q:Z

    if-eqz v1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Upload cancelled before the task started ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/apps/youtube/core/transfer/TransferException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->t:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    :goto_1
    return-void

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v3, "content"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "file"

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    :cond_2
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/transfer/aa;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_2
    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cannot resolve as content uri: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/google/android/apps/youtube/core/transfer/TransferException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "failure uploading"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->l:Lcom/google/android/apps/youtube/core/transfer/m;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    new-instance v3, Lcom/google/android/apps/youtube/core/transfer/TransferException;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/youtube/core/transfer/TransferException;-><init>(Ljava/lang/Throwable;Z)V

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/youtube/core/transfer/m;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/transfer/TransferException;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->t:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    goto :goto_1

    :cond_3
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_size"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Upload cursor is null: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/google/android/apps/youtube/core/transfer/TransferException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :catch_1
    move-exception v0

    :try_start_4
    const-string v1, "FATAL failure uploading"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->d:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v2, "UploadFatalError"

    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->l:Lcom/google/android/apps/youtube/core/transfer/m;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    new-instance v3, Lcom/google/android/apps/youtube/core/transfer/TransferException;

    const/4 v4, 0x1

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/youtube/core/transfer/TransferException;-><init>(Ljava/lang/Throwable;Z)V

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/youtube/core/transfer/m;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/transfer/TransferException;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->t:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    goto/16 :goto_1

    :cond_4
    :try_start_5
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Upload cursor is empty: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lcom/google/android/apps/youtube/core/transfer/TransferException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    :catch_2
    move-exception v0

    :try_start_6
    const-string v1, "failure uploading"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->d:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v2, "UploadError"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->l:Lcom/google/android/apps/youtube/core/transfer/m;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    new-instance v3, Lcom/google/android/apps/youtube/core/transfer/TransferException;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/youtube/core/transfer/TransferException;-><init>(Ljava/lang/Throwable;Z)V

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/youtube/core/transfer/m;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/transfer/TransferException;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->t:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    goto/16 :goto_1

    :cond_5
    const/4 v2, 0x0

    :try_start_7
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->r:J

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->l:Lcom/google/android/apps/youtube/core/transfer/m;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    iget-wide v3, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->r:J

    invoke-interface {v0, v2, v3, v4}, Lcom/google/android/apps/youtube/core/transfer/m;->a(Ljava/lang/String;J)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->q:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->o:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/aa;->a()Lorg/apache/http/HttpResponse;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->q:Z

    if-nez v2, :cond_0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    const/16 v3, 0x134

    if-ne v2, v3, :cond_b

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/transfer/aa;->a(Lorg/apache/http/HttpResponse;)V

    const-string v2, "range"

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    if-nez v0, :cond_7

    const-wide/16 v2, -0x1

    :goto_3
    iget-object v4, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->n:Ljava/lang/Object;

    monitor-enter v4
    :try_end_7
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Lcom/google/android/apps/youtube/core/transfer/TransferException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    iput-wide v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->s:J

    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v4, "metadata_updated"

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;Z)Z

    move-result v1

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/google/android/apps/youtube/core/transfer/aa;->a(Ljava/io/InputStream;JZ)Lorg/apache/http/HttpResponse;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->q:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    if-eq v1, v6, :cond_6

    if-ne v1, v7, :cond_9

    :cond_6
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/transfer/aa;->b(Lorg/apache/http/HttpResponse;)V
    :try_end_9
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Lcom/google/android/apps/youtube/core/transfer/TransferException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    :catch_3
    move-exception v0

    :try_start_a
    const-string v1, "failure uploading"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->d:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v2, "UploadError"

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/TransferException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->l:Lcom/google/android/apps/youtube/core/transfer/m;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/youtube/core/transfer/m;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/transfer/TransferException;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->t:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    goto/16 :goto_1

    :cond_7
    :try_start_b
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/youtube/core/transfer/aa;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-nez v3, :cond_8

    new-instance v1, Ljava/io/IOException;

    const-string v2, "malformed range header=%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_b
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_b .. :try_end_b} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catch Lcom/google/android/apps/youtube/core/transfer/TransferException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->t:Landroid/os/ConditionVariable;

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->open()V

    throw v0

    :cond_8
    const/4 v0, 0x2

    :try_start_c
    invoke-virtual {v2, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    goto/16 :goto_3

    :catchall_1
    move-exception v0

    monitor-exit v4

    throw v0

    :cond_9
    const/16 v2, 0x134

    if-ne v1, v2, :cond_a

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/transfer/aa;->a(Lorg/apache/http/HttpResponse;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v1, "metadata_updated"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->s:J

    iget-wide v2, p0, Lcom/google/android/apps/youtube/core/transfer/aa;->r:J

    const-wide/16 v4, 0x2

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/TransferException;

    const-string v1, "upload request got http status: 308"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/transfer/TransferException;-><init>(Ljava/lang/String;Z)V

    throw v0

    :cond_a
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/transfer/aa;->a(Lorg/apache/http/HttpResponse;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/TransferException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "upload request got http status: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/transfer/TransferException;-><init>(Ljava/lang/String;Z)V

    throw v0

    :cond_b
    if-eq v2, v6, :cond_c

    if-ne v2, v7, :cond_d

    :cond_c
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/transfer/aa;->b(Lorg/apache/http/HttpResponse;)V

    goto/16 :goto_0

    :cond_d
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/transfer/aa;->a(Lorg/apache/http/HttpResponse;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/TransferException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "range request got http status: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/core/transfer/TransferException;-><init>(Ljava/lang/String;Z)V

    throw v0
    :try_end_c
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_c .. :try_end_c} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_c .. :try_end_c} :catch_1
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2
    .catch Lcom/google/android/apps/youtube/core/transfer/TransferException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :cond_e
    move-object v1, v0

    goto/16 :goto_2
.end method
