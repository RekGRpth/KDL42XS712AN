.class public abstract Lcom/google/android/apps/youtube/core/client/at;
.super Lcom/google/android/apps/youtube/core/client/m;
.source "SourceFile"


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final h:Landroid/net/Uri;


# instance fields
.field private final i:Landroid/content/SharedPreferences;

.field private final j:Lcom/google/android/apps/youtube/core/client/bc;

.field private final k:Lcom/google/android/apps/youtube/core/identity/l;

.field private final l:Lcom/google/android/apps/youtube/core/async/af;

.field private final m:Lcom/google/android/apps/youtube/datalib/e/b;

.field private final n:Lcom/google/android/apps/youtube/core/au;

.field private final o:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

.field private final p:Landroid/net/ConnectivityManager;

.field private final q:Landroid/telephony/TelephonyManager;

.field private final r:Lcom/google/android/apps/youtube/common/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "https://www.youtube.com/leanback_ajax?action_environment=1"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/core/client/at;->a:Landroid/net/Uri;

    const-string v0, "https://www.youtube-nocookie.com/device_204"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/core/client/at;->h:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/common/e/b;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/au;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Lcom/google/android/apps/youtube/datalib/e/b;)V
    .locals 2

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/apps/youtube/core/client/m;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/common/e/b;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/client/av;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/client/av;-><init>(Lcom/google/android/apps/youtube/core/client/at;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->r:Lcom/google/android/apps/youtube/common/a/b;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->p:Landroid/net/ConnectivityManager;

    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->q:Landroid/telephony/TelephonyManager;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->j:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->k:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/au;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->n:Lcom/google/android/apps/youtube/core/au;

    const-string v0, "deviceClassification cannot be null"

    invoke-static {p9, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->o:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    new-instance v0, Lcom/google/android/apps/youtube/core/client/aw;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/client/aw;-><init>(Lcom/google/android/apps/youtube/core/client/at;)V

    sget-object v1, Lcom/google/android/apps/youtube/core/converter/http/dp;->a:Lcom/google/android/apps/youtube/core/converter/http/dp;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/core/client/at;->a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/client/at;->a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->l:Lcom/google/android/apps/youtube/core/async/af;

    invoke-static {p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->m:Lcom/google/android/apps/youtube/datalib/e/b;

    return-void
.end method

.method private a(J)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->d:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    sub-long v2, v0, p1

    const-wide/32 v4, 0xdbba00

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/core/client/at;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/at;->n:Lcom/google/android/apps/youtube/core/au;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/core/au;->D()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "dev_retention_last_ping_time_ms"

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->l:Lcom/google/android/apps/youtube/core/async/af;

    sget-object v1, Lcom/google/android/apps/youtube/core/client/at;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/at;->r:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/client/at;Ljava/lang/String;J)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->k:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/youtube/core/client/at;->a(Ljava/lang/String;JLcom/google/android/apps/youtube/core/identity/UserProfile;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->j:Lcom/google/android/apps/youtube/core/client/bc;

    new-instance v1, Lcom/google/android/apps/youtube/core/client/au;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/client/au;-><init>(Lcom/google/android/apps/youtube/core/client/at;Ljava/lang/String;J)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/client/bc;->a(Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/client/at;Ljava/lang/String;JLcom/google/android/apps/youtube/core/identity/UserProfile;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/core/client/at;->a(Ljava/lang/String;JLcom/google/android/apps/youtube/core/identity/UserProfile;)V

    return-void
.end method

.method private a(Ljava/lang/String;JLcom/google/android/apps/youtube/core/identity/UserProfile;)V
    .locals 11

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    const-wide/16 v9, 0x0

    if-eqz p4, :cond_b

    move v0, v1

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    const-string v5, "dev_retention_first_geo"

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    move v2, v1

    :cond_0
    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v4, "dev_retention_first_geo"

    invoke-interface {v2, v4, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v4, "dev_retention_first_active"

    invoke-interface {v2, v4, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    const-string v4, "dev_retention_first_login"

    invoke-interface {v2, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v4, "dev_retention_first_login"

    invoke-interface {v2, v4, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_2
    sget-object v2, Lcom/google/android/apps/youtube/core/client/at;->h:Landroid/net/Uri;

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v4

    const-string v5, "app_anon_id"

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    const-string v6, "dev_retention_uuid"

    invoke-interface {v2, v6, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "dev_retention_uuid"

    invoke-interface {v6, v7, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_3
    invoke-virtual {v4, v5, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v4

    const-string v2, "firstactive"

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    const-string v6, "dev_retention_first_active"

    invoke-interface {v5, v6, v9, v10}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v2

    const-string v5, "firstactivegeo"

    iget-object v6, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    const-string v7, "dev_retention_first_geo"

    const-string v8, ""

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    const-string v5, "dev_retention_first_login"

    invoke-interface {v2, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "firstlogin"

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    const-string v6, "dev_retention_first_login"

    invoke-interface {v5, v6, v9, v10}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    const-string v5, "dev_retention_prev_active"

    invoke-interface {v2, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "prevactive"

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    const-string v6, "dev_retention_prev_active"

    invoke-interface {v5, v6, v9, v10}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    const-string v5, "dev_retention_prev_login"

    invoke-interface {v2, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "prevlogin"

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    const-string v6, "dev_retention_prev_login"

    invoke-interface {v5, v6, v9, v10}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    const-string v5, "dev_retention_intercepted_url"

    invoke-interface {v2, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "intercepted_url"

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    const-string v6, "dev_retention_intercepted_url"

    const-string v7, ""

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v5, "dev_retention_intercepted_url"

    invoke-interface {v2, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_7
    const-string v5, "loginstate"

    if-eqz p4, :cond_c

    const-string v2, "1"

    :goto_1
    invoke-virtual {v4, v5, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    if-eqz p4, :cond_8

    invoke-virtual {p4}, Lcom/google/android/apps/youtube/core/identity/UserProfile;->hasAge()Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p4, Lcom/google/android/apps/youtube/core/identity/UserProfile;->gender:Lcom/google/android/apps/youtube/core/identity/UserProfile$Gender;

    sget-object v5, Lcom/google/android/apps/youtube/core/identity/UserProfile$Gender;->UNKNOWN:Lcom/google/android/apps/youtube/core/identity/UserProfile$Gender;

    if-eq v2, v5, :cond_8

    iget-object v2, p4, Lcom/google/android/apps/youtube/core/identity/UserProfile;->gender:Lcom/google/android/apps/youtube/core/identity/UserProfile$Gender;

    sget-object v5, Lcom/google/android/apps/youtube/core/identity/UserProfile$Gender;->MALE:Lcom/google/android/apps/youtube/core/identity/UserProfile$Gender;

    if-ne v2, v5, :cond_d

    const-string v2, "m"

    :goto_2
    const-string v5, "uga"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, p4, Lcom/google/android/apps/youtube/core/identity/UserProfile;->age:I

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    :cond_8
    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/at;->o:Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/youtube/core/client/DeviceClassification;->a(Lcom/google/android/apps/youtube/common/e/o;)Lcom/google/android/apps/youtube/common/e/o;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/at;->p:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    if-eqz v2, :cond_14

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    if-ne v5, v1, :cond_e

    const-string v1, "wifi"

    :goto_3
    if-eqz v1, :cond_9

    const-string v2, "cnetwork"

    invoke-virtual {v4, v2, v1}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    :cond_9
    invoke-virtual {v4}, Lcom/google/android/apps/youtube/common/e/o;->a()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Retention ping: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/at;->m:Lcom/google/android/apps/youtube/datalib/e/b;

    const-string v2, "drp"

    const v3, 0x323467f

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/datalib/e/b;->a(Ljava/lang/String;I)Lcom/google/android/apps/youtube/datalib/e/f;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/datalib/e/f;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/e/f;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/at;->m:Lcom/google/android/apps/youtube/datalib/e/b;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/a/b;->b:Lcom/android/volley/n;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/e/b;->a(Lcom/google/android/apps/youtube/datalib/e/f;Lcom/android/volley/n;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "dev_retention_prev_active"

    invoke-interface {v1, v2, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    if-eqz v0, :cond_a

    const-string v0, "dev_retention_prev_login"

    invoke-interface {v1, v0, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    :cond_a
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void

    :cond_b
    move v0, v2

    goto/16 :goto_0

    :cond_c
    const-string v2, "0"

    goto/16 :goto_1

    :cond_d
    const-string v2, "f"

    goto :goto_2

    :cond_e
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-eqz v1, :cond_f

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const/4 v5, 0x4

    if-eq v1, v5, :cond_f

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const/4 v5, 0x5

    if-eq v1, v5, :cond_f

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const/4 v5, 0x2

    if-eq v1, v5, :cond_f

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const/4 v5, 0x3

    if-ne v1, v5, :cond_11

    :cond_f
    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/at;->q:Landroid/telephony/TelephonyManager;

    if-eqz v1, :cond_10

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/at;->q:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_10
    const-string v1, "mobile"

    goto/16 :goto_3

    :cond_11
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const/4 v5, 0x7

    if-ne v1, v5, :cond_12

    const-string v1, "bluetooth"

    goto/16 :goto_3

    :cond_12
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const/16 v5, 0x9

    if-ne v1, v5, :cond_13

    const-string v1, "ethernet"

    goto/16 :goto_3

    :cond_13
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_14

    const-string v1, "wimax"

    goto/16 :goto_3

    :cond_14
    move-object v1, v3

    goto/16 :goto_3
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 4

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "dev_retention_intercepted_url"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    const-string v1, "dev_retention_last_ping_time_ms"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/client/at;->a(J)V

    return-void
.end method

.method protected abstract a()Z
.end method

.method public final b()V
    .locals 2

    const-string v0, "Resetting all information associated with device retention"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "dev_retention_uuid"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "dev_retention_first_active"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "dev_retention_first_geo"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "dev_retention_first_login"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "dev_retention_prev_active"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "dev_retention_prev_login"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "dev_retention_last_ping_time_ms"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final c()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/at;->i:Landroid/content/SharedPreferences;

    const-string v1, "dev_retention_last_ping_time_ms"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/client/at;->a(J)V

    return-void
.end method

.method public final f()V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/core/client/at;->a(J)V

    return-void
.end method
