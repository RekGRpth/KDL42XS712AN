.class public final Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/au;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/az;

.field private final b:Ljava/util/concurrent/Executor;

.field private final c:Lcom/google/android/apps/youtube/core/Analytics;

.field private final d:Lcom/google/android/apps/youtube/common/e/b;

.field private volatile e:Z

.field private volatile f:Lcom/google/android/apps/youtube/core/player/ba;

.field private volatile g:Lcom/google/android/apps/youtube/core/player/av;

.field private h:Z

.field private final i:Ljava/util/concurrent/LinkedBlockingQueue;

.field private final j:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/player/az;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->i:Ljava/util/concurrent/LinkedBlockingQueue;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/ax;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/player/ax;-><init>(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->j:Ljava/lang/Runnable;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->a:Lcom/google/android/apps/youtube/core/player/az;

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->b:Ljava/util/concurrent/Executor;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->c:Lcom/google/android/apps/youtube/core/Analytics;

    iput-object p4, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->d:Lcom/google/android/apps/youtube/common/e/b;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->e:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->f:Lcom/google/android/apps/youtube/core/player/ba;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->e:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->i:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/av;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->g:Lcom/google/android/apps/youtube/core/player/av;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->g:Lcom/google/android/apps/youtube/core/player/av;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/apps/youtube/core/player/ba;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/player/ba;-><init>(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->f:Lcom/google/android/apps/youtube/core/player/ba;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->h:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->h:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->a:Lcom/google/android/apps/youtube/core/player/az;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/az;->l()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->g:Lcom/google/android/apps/youtube/core/player/av;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->f:Lcom/google/android/apps/youtube/core/player/ba;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/av;->a(Lcom/google/android/apps/youtube/core/player/at;)V

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->h:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->a:Lcom/google/android/apps/youtube/core/player/az;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/az;->m()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;Lcom/google/android/apps/youtube/core/player/ba;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->f:Lcom/google/android/apps/youtube/core/player/ba;

    if-ne p1, v0, :cond_0

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->f:Lcom/google/android/apps/youtube/core/player/ba;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->g:Lcom/google/android/apps/youtube/core/player/av;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->c()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;)Lcom/google/android/apps/youtube/common/e/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->d:Lcom/google/android/apps/youtube/common/e/b;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;Lcom/google/android/apps/youtube/core/player/ba;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/ay;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/core/player/ay;-><init>(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;Lcom/google/android/apps/youtube/core/player/ba;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;)Lcom/google/android/apps/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->c:Lcom/google/android/apps/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;Lcom/google/android/apps/youtube/core/player/ba;)Lcom/google/android/apps/youtube/core/player/ba;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->f:Lcom/google/android/apps/youtube/core/player/ba;

    return-object v0
.end method

.method private c()V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->j:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->b:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->j:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->c()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;)Lcom/google/android/apps/youtube/core/player/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->f:Lcom/google/android/apps/youtube/core/player/ba;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->g:Lcom/google/android/apps/youtube/core/player/av;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->g:Lcom/google/android/apps/youtube/core/player/av;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/av;->a()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->g:Lcom/google/android/apps/youtube/core/player/av;

    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->f:Lcom/google/android/apps/youtube/core/player/ba;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->h:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->i:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/av;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->i:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->c()V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->e:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->c()V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/VideoInterruptManager;->f:Lcom/google/android/apps/youtube/core/player/ba;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/player/ba;->a(Lcom/google/android/apps/youtube/core/player/ba;)Lcom/google/android/apps/youtube/core/player/aw;

    return-void
.end method
