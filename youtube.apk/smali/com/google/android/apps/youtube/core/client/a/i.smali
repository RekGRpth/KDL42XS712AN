.class final Lcom/google/android/apps/youtube/core/client/a/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/c/d;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/client/a/h;

.field private final b:Lcom/google/android/apps/youtube/core/client/a/d;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/client/a/h;Lcom/google/android/apps/youtube/core/client/a/d;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/a/i;->a:Lcom/google/android/apps/youtube/core/client/a/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/a/d;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/i;->b:Lcom/google/android/apps/youtube/core/client/a/d;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/i;->b:Lcom/google/android/apps/youtube/core/client/a/d;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/client/a/d;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/a/i;->a:Lcom/google/android/apps/youtube/core/client/a/h;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/client/a/h;->a(Lcom/google/android/apps/youtube/core/client/a/h;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/a/a;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/youtube/core/client/a/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method
