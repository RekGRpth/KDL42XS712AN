.class public final Lcom/google/android/apps/youtube/core/identity/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/identity/l;

.field private final b:Lcom/google/android/apps/youtube/core/identity/al;

.field private final c:Lcom/google/android/apps/youtube/core/identity/as;

.field private final d:Lcom/google/android/apps/youtube/core/identity/b;

.field private final e:Lcom/google/android/apps/youtube/core/identity/h;

.field private final f:Ljava/util/concurrent/Executor;

.field private final g:Ljava/util/concurrent/Executor;

.field private final h:Lcom/google/android/apps/youtube/common/c/a;

.field private final i:Landroid/content/SharedPreferences;

.field private final j:Ljava/util/Set;

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/al;Lcom/google/android/apps/youtube/core/identity/as;Lcom/google/android/apps/youtube/core/identity/b;Lcom/google/android/apps/youtube/core/identity/h;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/common/c/a;Landroid/content/SharedPreferences;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->a:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/al;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->b:Lcom/google/android/apps/youtube/core/identity/al;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/as;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->c:Lcom/google/android/apps/youtube/core/identity/as;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->d:Lcom/google/android/apps/youtube/core/identity/b;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->e:Lcom/google/android/apps/youtube/core/identity/h;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->f:Ljava/util/concurrent/Executor;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->g:Ljava/util/concurrent/Executor;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->h:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->i:Landroid/content/SharedPreferences;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->j:Ljava/util/Set;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/identity/aa;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->j:Ljava/util/Set;

    return-object v0
.end method

.method private a(Landroid/app/Activity;)V
    .locals 8

    const/4 v0, 0x0

    const/4 v3, 0x1

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string v1, "allowSkip"

    invoke-virtual {v7, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/identity/aa;->c:Lcom/google/android/apps/youtube/core/identity/as;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/identity/as;->b()[Landroid/accounts/Account;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    new-array v2, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/identity/aa;->c:Lcom/google/android/apps/youtube/core/identity/as;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/core/identity/as;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/identity/aa;->c:Lcom/google/android/apps/youtube/core/identity/as;

    iget-object v5, v4, Lcom/google/android/apps/youtube/core/identity/as;->c:Ljava/lang/String;

    move-object v4, v0

    move-object v6, v0

    invoke-static/range {v0 .. v7}, Landroid/accounts/AccountManager;->newChooseAccountIntent(Landroid/accounts/Account;Ljava/util/ArrayList;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x387

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private a(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/core/identity/f;

    const/4 v1, 0x0

    invoke-direct {v0, p2, v1}, Lcom/google/android/apps/youtube/core/identity/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/identity/aa;->g:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/apps/youtube/core/identity/ag;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/apps/youtube/core/identity/ag;-><init>(Lcom/google/android/apps/youtube/core/identity/aa;Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/f;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/identity/aa;Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/f;Landroid/content/Intent;)V
    .locals 1

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/core/identity/f;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->k:Ljava/lang/String;

    const/16 v0, 0x389

    invoke-virtual {p1, p3, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/identity/f;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->e:Lcom/google/android/apps/youtube/core/identity/h;

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/ac;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/core/identity/ac;-><init>(Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/identity/f;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/core/identity/h;->b(Lcom/google/android/apps/youtube/core/identity/f;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/identity/aa;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->i:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "username"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_channel_id"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->i:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_signed_out"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/identity/aa;)Lcom/google/android/apps/youtube/core/identity/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->d:Lcom/google/android/apps/youtube/core/identity/b;

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->a:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/identity/aa;->c()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->a:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->h()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/ae;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/identity/ae;-><init>(Lcom/google/android/apps/youtube/core/identity/aa;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->i:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_account"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "username"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_channel_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_identity"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "delegate_cache"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->i:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "profile_display_email"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "profile_display_name"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "profile_thumbnail_uri"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ah;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->j:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method final a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/f;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->b:Lcom/google/android/apps/youtube/core/identity/al;

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/ab;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/identity/ab;-><init>(Lcom/google/android/apps/youtube/core/identity/aa;)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/apps/youtube/core/identity/al;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/f;Lcom/google/android/apps/youtube/core/identity/aq;)V

    return-void
.end method

.method final a(Lcom/google/android/apps/youtube/core/identity/f;Lcom/google/android/apps/youtube/core/identity/z;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->a:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Signed in as new account"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->i:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_account"

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/identity/f;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_identity"

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/identity/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->i:Landroid/content/SharedPreferences;

    invoke-virtual {p2, v0}, Lcom/google/android/apps/youtube/core/identity/z;->a(Landroid/content/SharedPreferences;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->a:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/identity/l;->a(Lcom/google/android/apps/youtube/core/identity/f;Lcom/google/android/apps/youtube/core/identity/z;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->h:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/ai;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/identity/ai;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/ad;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/identity/ad;-><init>(Lcom/google/android/apps/youtube/core/identity/aa;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method final a(Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->a:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/identity/aa;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->a:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->h()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/af;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/core/identity/af;-><init>(Lcom/google/android/apps/youtube/core/identity/aa;Ljava/lang/Exception;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Signing out because: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/identity/aa;->c()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->a:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->h()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->h:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/identity/aj;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/identity/aj;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    return-void
.end method

.method public final a()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->i:Landroid/content/SharedPreferences;

    const-string v1, "user_signed_out"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/app/Activity;IILandroid/content/Intent;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v3, -0x1

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    if-eq p3, v3, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/identity/aa;->b()V

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_0
    if-nez p4, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Could not get account choice result."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Ljava/lang/Exception;)V

    goto :goto_1

    :cond_1
    const-string v0, "exception"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    if-eqz v0, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Ljava/lang/Exception;)V

    goto :goto_1

    :cond_2
    const-string v0, "authAccount"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Error fetching account name."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Ljava/lang/Exception;)V

    goto :goto_1

    :cond_3
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->k:Ljava/lang/String;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/youtube/core/identity/aa;->k:Ljava/lang/String;

    if-eq p3, v3, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/identity/aa;->b()V

    :goto_2
    move v0, v1

    goto :goto_0

    :cond_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Auth recovery without a saved account name."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Ljava/lang/Exception;)V

    goto :goto_2

    :cond_5
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x387
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ah;)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->j:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/identity/aa;->c:Lcom/google/android/apps/youtube/core/identity/as;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/as;->b()[Landroid/accounts/Account;

    move-result-object v0

    array-length v1, v0

    if-ne v1, v2, :cond_1

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;)V

    goto :goto_0
.end method
