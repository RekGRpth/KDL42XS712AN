.class public final Lcom/google/android/apps/youtube/core/async/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/async/af;

.field private final b:Ljava/util/concurrent/atomic/AtomicLong;

.field private final c:Lcom/google/android/apps/youtube/common/e/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/async/af;Ljava/util/concurrent/atomic/AtomicLong;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/y;->a:Lcom/google/android/apps/youtube/core/async/af;

    const-string v0, "lastEventTime may not be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicLong;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/y;->b:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/async/y;->c:Lcom/google/android/apps/youtube/common/e/b;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/async/y;)Lcom/google/android/apps/youtube/common/e/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/y;->c:Lcom/google/android/apps/youtube/common/e/b;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/core/async/y;)Ljava/util/concurrent/atomic/AtomicLong;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/y;->b:Ljava/util/concurrent/atomic/AtomicLong;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/async/y;->a:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v1, Lcom/google/android/apps/youtube/core/async/z;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/youtube/core/async/z;-><init>(Lcom/google/android/apps/youtube/core/async/y;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
