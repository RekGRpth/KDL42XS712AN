.class public final Lcom/google/android/apps/youtube/core/utils/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/ThreadFactory;


# instance fields
.field private final a:I

.field private final b:Ljava/util/concurrent/ThreadFactory;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/youtube/core/utils/r;->a:I

    invoke-static {}, Ljava/util/concurrent/Executors;->defaultThreadFactory()Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/utils/r;->b:Ljava/util/concurrent/ThreadFactory;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/utils/r;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/utils/r;->a:I

    return v0
.end method


# virtual methods
.method public final newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/utils/r;->b:Ljava/util/concurrent/ThreadFactory;

    new-instance v1, Lcom/google/android/apps/youtube/core/utils/s;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/core/utils/s;-><init>(Lcom/google/android/apps/youtube/core/utils/r;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ThreadFactory;->newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;

    move-result-object v0

    return-object v0
.end method
