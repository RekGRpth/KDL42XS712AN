.class public Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14$RemoteControlIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->b()Lcom/google/android/apps/youtube/core/player/notification/j;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    invoke-virtual {v0}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    invoke-static {}, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->b()Lcom/google/android/apps/youtube/core/player/notification/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/notification/j;->b()V

    goto :goto_0

    :sswitch_1
    invoke-static {}, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->b()Lcom/google/android/apps/youtube/core/player/notification/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/notification/j;->c()V

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->b()Lcom/google/android/apps/youtube/core/player/notification/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/notification/j;->d()V

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->b()Lcom/google/android/apps/youtube/core/player/notification/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/notification/j;->f()V

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->b()Lcom/google/android/apps/youtube/core/player/notification/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/notification/j;->e()V

    goto :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->b()Lcom/google/android/apps/youtube/core/player/notification/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/notification/j;->g()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x55 -> :sswitch_2
        0x56 -> :sswitch_5
        0x57 -> :sswitch_3
        0x58 -> :sswitch_4
        0x7e -> :sswitch_0
        0x7f -> :sswitch_1
    .end sparse-switch
.end method
