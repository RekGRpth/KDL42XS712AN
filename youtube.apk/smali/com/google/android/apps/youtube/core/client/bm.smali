.class public final Lcom/google/android/apps/youtube/core/client/bm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;
.implements Lcom/google/android/apps/youtube/core/client/AdStatsClient;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/client/j;

.field private final b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

.field private final c:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

.field private final d:Lcom/google/android/apps/youtube/core/player/b;

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Ljava/util/List;

.field private i:I

.field private j:I

.field private k:Ljava/util/PriorityQueue;

.field private l:Lcom/google/android/apps/youtube/core/player/al;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/client/j;Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->l:Lcom/google/android/apps/youtube/core/player/al;

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/bm;->a:Lcom/google/android/apps/youtube/core/client/j;

    iput-object p3, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iput-object p2, p0, Lcom/google/android/apps/youtube/core/client/bm;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->h:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->j:I

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->j:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bm;->g(I)Ljava/util/PriorityQueue;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->k:Ljava/util/PriorityQueue;

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/c;->a()Lcom/google/android/apps/youtube/core/player/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->d:Lcom/google/android/apps/youtube/core/player/b;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->d:Lcom/google/android/apps/youtube/core/player/b;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p4}, Lcom/google/android/apps/youtube/core/player/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->d:Lcom/google/android/apps/youtube/core/player/b;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/player/b;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->d:Lcom/google/android/apps/youtube/core/player/b;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/youtube/core/player/b;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->d:Lcom/google/android/apps/youtube/core/player/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/bm;->l:Lcom/google/android/apps/youtube/core/player/al;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/b;->a(Lcom/google/android/apps/youtube/core/player/al;)V

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/youtube/core/client/j;Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;IZZZLjava/util/List;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/core/client/bm;-><init>(Lcom/google/android/apps/youtube/core/client/j;Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/lang/String;)V

    iput p5, p0, Lcom/google/android/apps/youtube/core/client/bm;->i:I

    iput-boolean p6, p0, Lcom/google/android/apps/youtube/core/client/bm;->e:Z

    iput-boolean p7, p0, Lcom/google/android/apps/youtube/core/client/bm;->f:Z

    iput-boolean p8, p0, Lcom/google/android/apps/youtube/core/client/bm;->g:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->h:Ljava/util/List;

    iput p10, p0, Lcom/google/android/apps/youtube/core/client/bm;->j:I

    invoke-direct {p0, p10}, Lcom/google/android/apps/youtube/core/client/bm;->g(I)Ljava/util/PriorityQueue;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->k:Ljava/util/PriorityQueue;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/client/bm;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-object v0
.end method

.method private a(Landroid/net/Uri;)V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/AdError;->a:Lcom/google/android/apps/youtube/core/player/AdError;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/client/bm;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/core/player/AdError;)V

    return-void
.end method

.method private a(Landroid/net/Uri;Lcom/google/android/apps/youtube/core/player/AdError;)V
    .locals 4

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->d:Lcom/google/android/apps/youtube/core/player/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/player/b;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/core/player/AdError;)Landroid/net/Uri;
    :try_end_0
    .catch Lcom/google/android/apps/youtube/core/converter/ConverterException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Pinging "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->a:Lcom/google/android/apps/youtube/core/client/j;

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/client/bm;->a:Lcom/google/android/apps/youtube/core/client/j;

    const-string v2, "vastad"

    invoke-virtual {v1, p1, v2}, Lcom/google/android/apps/youtube/core/client/j;->a(Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/e/f;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getShouldAllowQueuedOfflinePings()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/e/f;->a(Z)Lcom/google/android/apps/youtube/datalib/e/f;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExpirationTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/e/f;->a(J)Lcom/google/android/apps/youtube/datalib/e/f;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/datalib/a/b;->a:Lcom/android/volley/n;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/j;->a(Lcom/google/android/apps/youtube/datalib/e/f;Lcom/android/volley/n;)V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to substitute URI macros "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->createCustomAdSenseConversionUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/bm;->a(Landroid/net/Uri;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/util/List;)Z
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/AdError;->a:Lcom/google/android/apps/youtube/core/player/AdError;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;Lcom/google/android/apps/youtube/core/player/AdError;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/util/List;Lcom/google/android/apps/youtube/core/player/AdError;)Z
    .locals 2

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/youtube/core/client/bm;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/core/player/AdError;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private f(I)Lcom/google/android/apps/youtube/datalib/innertube/model/m;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adInfoCards:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adInfoCards:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->getInfoCards()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adInfoCards:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->getInfoCards()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;

    goto :goto_0
.end method

.method private g(I)Ljava/util/PriorityQueue;
    .locals 5

    new-instance v2, Ljava/util/PriorityQueue;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getProgressPings()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-instance v1, Lcom/google/android/apps/youtube/core/client/bn;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/core/client/bn;-><init>(Lcom/google/android/apps/youtube/core/client/bm;)V

    invoke-direct {v2, v0, v1}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getProgressPings()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    iget-object v4, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getDuration()I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getTimeOffsetMilliseconds(I)I

    move-result v4

    if-le v4, p1, :cond_0

    invoke-virtual {v2, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_2
    return-object v2
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getBreakStartPingUris()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;)Z

    return-void
.end method

.method public final a(I)V
    .locals 6

    const/4 v1, 0x0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->k:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->k:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getDuration()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getTimeOffsetMilliseconds(I)I

    move-result v0

    if-lt p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->k:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getPingUri()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bm;->a(Landroid/net/Uri;)V

    goto :goto_0

    :cond_0
    iput p1, p0, Lcom/google/android/apps/youtube/core/client/bm;->j:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getDuration()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    if-lez v0, :cond_1

    mul-int/lit8 v2, p1, 0x4

    div-int v0, v2, v0

    :goto_1
    iget v2, p0, Lcom/google/android/apps/youtube/core/client/bm;->i:I

    if-lt v0, v2, :cond_4

    move v3, v0

    :goto_2
    iget v2, p0, Lcom/google/android/apps/youtube/core/client/bm;->i:I

    if-lt v3, v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-object v4, v2

    move v5, v1

    :goto_3
    if-eqz v4, :cond_2

    packed-switch v3, :pswitch_data_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    :goto_4
    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;)Z

    move-result v2

    or-int/2addr v5, v2

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v2

    move-object v4, v2

    goto :goto_3

    :cond_1
    move v0, v1

    goto :goto_1

    :pswitch_0
    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getFirstQuartilePingUris()Ljava/util/List;

    move-result-object v2

    goto :goto_4

    :pswitch_1
    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getMidpointPingUris()Ljava/util/List;

    move-result-object v2

    goto :goto_4

    :pswitch_2
    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getThirdQuartilePingUris()Ljava/util/List;

    move-result-object v2

    goto :goto_4

    :cond_2
    if-nez v5, :cond_3

    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->i:I

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->e:Z

    if-nez v0, :cond_6

    const/16 v0, 0x7530

    if-lt p1, v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    :goto_5
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getEngagedViewPingUris()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;)Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_5

    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->e:Z

    :cond_6
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->d:Lcom/google/android/apps/youtube/core/player/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/player/b;->a(II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSkipPingUris()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;)Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/AdError;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getErrorPingUris()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;Lcom/google/android/apps/youtube/core/player/AdError;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/al;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/client/bm;->l:Lcom/google/android/apps/youtube/core/player/al;

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->d:Lcom/google/android/apps/youtube/core/player/b;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/b;->a(Lcom/google/android/apps/youtube/core/player/al;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getInfoCards()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getInfoCards()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getEvents()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardTrackingEvent;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardTrackingEvent;->getType()I

    move-result v2

    if-ne v2, p2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardTrackingEvent;->getBaseUrl()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bm;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getInfoCards()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getInfoCards()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getActionTrackingUris()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bm;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/aw;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSurvey()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSurvey()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;)Z

    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    check-cast p1, Lcom/google/android/apps/youtube/core/async/w;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Ping failed "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getBreakEndPingUris()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;)Z

    return-void
.end method

.method public final b(I)V
    .locals 6

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/client/bm;->f(I)Lcom/google/android/apps/youtube/datalib/innertube/model/m;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->a()Lcom/google/a/a/a/a/rn;

    move-result-object v0

    iget-object v1, v0, Lcom/google/a/a/a/a/rn;->d:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    :try_start_0
    invoke-static {v3}, Lcom/google/android/apps/youtube/common/e/p;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/apps/youtube/core/client/bm;->a(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v4

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid info card teaser impression ping uri was ignored: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final b(Lcom/google/android/apps/youtube/core/player/AdError;)V
    .locals 3

    const/4 v2, 0x5

    iget v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->i:I

    if-eq v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClosePingUris()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;Lcom/google/android/apps/youtube/core/player/AdError;)Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getErrorPingUris()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;Lcom/google/android/apps/youtube/core/player/AdError;)Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_0

    :cond_0
    iput v2, p0, Lcom/google/android/apps/youtube/core/client/bm;->i:I

    :cond_1
    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isForecastingAd()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getImpressionUris()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->f:Z

    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/client/bm;->f(I)Lcom/google/android/apps/youtube/datalib/innertube/model/m;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->b()Lcom/google/a/a/a/a/rm;

    move-result-object v0

    iget-object v1, v0, Lcom/google/a/a/a/a/rm;->g:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    :try_start_0
    invoke-static {v3}, Lcom/google/android/apps/youtube/common/e/p;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/youtube/core/client/bm;->a(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v3, "Invalid info card teaser impression ping uri - ignored"

    invoke-static {v3}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final d()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->g:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSkipShownPingUris()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;)Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->g:Z

    :cond_1
    return-void
.end method

.method public final d(I)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/client/bm;->f(I)Lcom/google/android/apps/youtube/datalib/innertube/model/m;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->d()Lcom/google/a/a/a/a/rl;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->d()Lcom/google/a/a/a/a/rl;

    move-result-object v0

    iget-object v1, v0, Lcom/google/a/a/a/a/rl;->b:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    :try_start_0
    invoke-static {v3}, Lcom/google/android/apps/youtube/common/e/p;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/youtube/core/client/bm;->a(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v3, "Invalid info card uri - ignored"

    invoke-static {v3}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClickTrackingPingUris()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;)Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final e(I)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/client/bm;->f(I)Lcom/google/android/apps/youtube/datalib/innertube/model/m;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->c()Lcom/google/a/a/a/a/rk;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->c()Lcom/google/a/a/a/a/rk;

    move-result-object v0

    iget-object v1, v0, Lcom/google/a/a/a/a/rk;->d:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    :try_start_0
    invoke-static {v3}, Lcom/google/android/apps/youtube/common/e/p;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/youtube/core/client/bm;->a(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v3, "Invalid info card action ping uri - ignored"

    invoke-static {v3}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final f()V
    .locals 3

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->f:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getImpressionUris()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;)Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_0

    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/core/client/bm;->f:Z

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->i:I

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getStartPingUris()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;)Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_1

    :cond_2
    iput v2, p0, Lcom/google/android/apps/youtube/core/client/bm;->i:I

    :cond_3
    return-void

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    :goto_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getResumePingUris()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;)Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_2
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClosePingUris()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;)Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPausePingUris()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;)Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final handleAdCompanionClickEvent(Lcom/google/android/apps/youtube/core/client/b;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const-string v0, "clickcompanionad"

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/core/client/bm;->e:Z

    if-nez v1, :cond_1

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getEngagedViewPingUris()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;)Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->e:Z

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->k:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->k:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getPingUri()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bm;->a(Landroid/net/Uri;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    :goto_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getCompletePingUris()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/util/List;)Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_2

    :cond_3
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/apps/youtube/core/client/bm;->i:I

    return-void
.end method

.method public final j()Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;
    .locals 8

    new-instance v0, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;

    iget v1, p0, Lcom/google/android/apps/youtube/core/client/bm;->i:I

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/core/client/bm;->e:Z

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/core/client/bm;->f:Z

    iget-boolean v4, p0, Lcom/google/android/apps/youtube/core/client/bm;->g:Z

    iget-object v5, p0, Lcom/google/android/apps/youtube/core/client/bm;->h:Ljava/util/List;

    iget v6, p0, Lcom/google/android/apps/youtube/core/client/bm;->j:I

    sget-object v7, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;->INSTREAM:Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState;-><init>(IZZZLjava/util/List;ILcom/google/android/apps/youtube/core/client/AdStatsClient$AdStatsClientState$Kind;)V

    return-object v0
.end method

.method public final k()V
    .locals 1

    const-string v0, "clickchannel"

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/client/bm;->a(Ljava/lang/String;)V

    return-void
.end method
