.class public final enum Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

.field public static final enum BLACK:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

.field public static final enum BLUE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

.field public static final enum CYAN:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

.field public static final enum GREEN:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

.field public static final enum MAGENTA:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

.field public static final enum NONE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

.field public static final enum RED:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

.field public static final enum WHITE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

.field public static final enum YELLOW:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

.field private static backgroundColorEntryStrings:[Ljava/lang/String;

.field private static backgroundColorValueStrings:[Ljava/lang/String;

.field private static backgroundColorValues:[I

.field private static colorEntryStrings:[Ljava/lang/String;

.field private static colorValueStrings:[Ljava/lang/String;

.field private static colorValues:[I


# instance fields
.field private colorStringId:I

.field private colorValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    const-string v1, "NONE"

    sget v2, Lcom/google/android/youtube/p;->eo:I

    invoke-direct {v0, v1, v5, v2, v5}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->NONE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    const-string v1, "WHITE"

    sget v2, Lcom/google/android/youtube/p;->eE:I

    const/4 v3, -0x1

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->WHITE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    const-string v1, "BLACK"

    sget v2, Lcom/google/android/youtube/p;->ea:I

    const/high16 v3, -0x1000000

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->BLACK:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    const-string v1, "RED"

    sget v2, Lcom/google/android/youtube/p;->es:I

    const/high16 v3, -0x10000

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->RED:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    const-string v1, "YELLOW"

    sget v2, Lcom/google/android/youtube/p;->eF:I

    const/16 v3, -0x100

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->YELLOW:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    const-string v1, "GREEN"

    const/4 v2, 0x5

    sget v3, Lcom/google/android/youtube/p;->em:I

    const v4, -0xff0100

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->GREEN:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    const-string v1, "CYAN"

    const/4 v2, 0x6

    sget v3, Lcom/google/android/youtube/p;->ec:I

    const v4, -0xff0001

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->CYAN:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    const-string v1, "BLUE"

    const/4 v2, 0x7

    sget v3, Lcom/google/android/youtube/p;->eb:I

    const v4, -0xffff01

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->BLUE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    const-string v1, "MAGENTA"

    const/16 v2, 0x8

    sget v3, Lcom/google/android/youtube/p;->en:I

    const v4, -0xff01

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->MAGENTA:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->NONE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->WHITE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->BLACK:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->RED:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->YELLOW:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->GREEN:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->CYAN:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->BLUE:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->MAGENTA:Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->$VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorStringId:I

    iput p4, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I

    return v0
.end method

.method public static getBackgroundColorEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->backgroundColorEntryStrings:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    move-result-object v1

    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->backgroundColorEntryStrings:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->backgroundColorEntryStrings:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->backgroundColorEntryStrings:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorStringId:I

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->backgroundColorEntryStrings:[Ljava/lang/String;

    return-object v0
.end method

.method public static getBackgroundColorValueStrings()[Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->backgroundColorValueStrings:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    move-result-object v1

    array-length v0, v1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->backgroundColorValueStrings:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->backgroundColorValueStrings:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->backgroundColorValueStrings:[Ljava/lang/String;

    aget-object v3, v1, v0

    iget v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->backgroundColorValueStrings:[Ljava/lang/String;

    return-object v0
.end method

.method public static getBackgroundColorValues()[I
    .locals 4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->backgroundColorValues:[I

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    move-result-object v1

    array-length v0, v1

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->backgroundColorValues:[I

    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->backgroundColorValues:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->backgroundColorValues:[I

    aget-object v3, v1, v0

    iget v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I

    aput v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->backgroundColorValues:[I

    return-object v0
.end method

.method public static getColorEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorEntryStrings:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    move-result-object v1

    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorEntryStrings:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorEntryStrings:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorEntryStrings:[Ljava/lang/String;

    add-int/lit8 v3, v0, 0x1

    aget-object v3, v1, v3

    iget v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorStringId:I

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorEntryStrings:[Ljava/lang/String;

    return-object v0
.end method

.method public static getColorValueStrings()[Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValueStrings:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    move-result-object v1

    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValueStrings:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValueStrings:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValueStrings:[Ljava/lang/String;

    add-int/lit8 v3, v0, 0x1

    aget-object v3, v1, v3

    iget v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValueStrings:[Ljava/lang/String;

    return-object v0
.end method

.method public static getColorValues()[I
    .locals 4

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValues:[I

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    move-result-object v1

    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValues:[I

    const/4 v0, 0x0

    :goto_0
    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValues:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValues:[I

    add-int/lit8 v3, v0, 0x1

    aget-object v3, v1, v3

    iget v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I

    aput v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValues:[I

    return-object v0
.end method

.method public static getDefaultBackgroundColorValue()I
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    move-result-object v0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I

    return v0
.end method

.method public static getDefaultEdgeColorValue()I
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    move-result-object v0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I

    return v0
.end method

.method public static getDefaultTextColorValue()I
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I

    return v0
.end method

.method public static getDefaultWindowColorValue()I
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->colorValue:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->$VALUES:[Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;

    return-object v0
.end method
