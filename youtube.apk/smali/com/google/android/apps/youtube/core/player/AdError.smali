.class public final Lcom/google/android/apps/youtube/core/player/AdError;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/apps/youtube/core/player/AdError;

.field private static final c:Landroid/util/SparseArray;


# instance fields
.field public final b:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/core/player/AdError;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->NONE:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/player/AdError;-><init>(Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;)V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/AdError;->a:Lcom/google/android/apps/youtube/core/player/AdError;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/a;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/player/a;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/core/player/AdError;->c:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/core/player/AdError;->b:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    return-void
.end method

.method public static a(II)Lcom/google/android/apps/youtube/core/player/AdError;
    .locals 2

    const/4 v0, 0x1

    if-ne p0, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/AdError;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/AdError;

    if-eqz v0, :cond_0

    :goto_0
    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/core/player/AdError;-><init>(Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;)V

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->VIDEO_PLAYBACK_UNKNOWN_ERROR:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/core/player/AdError;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->VIDEO_PLAYBACK_UNKNOWN_ERROR:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/player/AdError;-><init>(Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/AdError;->b:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->internalError:I

    return v0
.end method

.method public final b()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/AdError;->b:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->vastError:I

    return v0
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/AdError;->b:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->monetizationError:I

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/player/AdError;->b:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    iget v0, v0, Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;->internalError:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AdError: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/core/player/AdError;->b:Lcom/google/android/apps/youtube/core/player/AdError$ErrorType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
