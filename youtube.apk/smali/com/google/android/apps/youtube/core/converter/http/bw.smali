.class public Lcom/google/android/apps/youtube/core/converter/http/bw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/converter/c;


# instance fields
.field public final c:Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

.field public final d:Ljava/util/List;

.field public final e:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

.field public final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->c:Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->d:Ljava/util/List;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->e:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->c:Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->d:Ljava/util/List;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->e:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    const-string v0, "contentType can\'t be null or empty"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;Ljava/util/List;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->c:Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->d:Ljava/util/List;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    iput-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->e:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    iput-object v1, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/core/async/w;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/converter/http/bw;->b(Lcom/google/android/apps/youtube/core/async/w;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/google/android/apps/youtube/core/async/w;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->e:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/async/w;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->createHttpRequest(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/google/android/apps/youtube/core/async/w;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 4

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/async/w;->c:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->e:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->supportsPayload()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Content not allowed [method="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->e:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/core/converter/http/bw;->a(Lcom/google/android/apps/youtube/core/async/w;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v2

    const-string v0, "Accept-Encoding"

    const-string v1, "gzip"

    invoke-interface {v2, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/async/w;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v1, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->c:Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

    if-eqz v0, :cond_2

    :try_start_0
    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "X-GData-Device"

    iget-object v3, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->c:Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

    invoke-interface {v3, v0}, Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis$DeviceRegistrationException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/a/p;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/datalib/a/p;->a(Lorg/apache/http/client/methods/HttpUriRequest;)V

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/ConverterException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/core/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_3
    iget-object v0, p1, Lcom/google/android/apps/youtube/core/async/w;->c:[B

    if-eqz v0, :cond_4

    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    iget-object v0, p1, Lcom/google/android/apps/youtube/core/async/w;->c:[B

    invoke-direct {v1, v0}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/core/converter/http/bw;->f:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    move-object v0, v2

    check-cast v0, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;->setEntity(Lorg/apache/http/HttpEntity;)V

    :cond_4
    return-object v2
.end method
