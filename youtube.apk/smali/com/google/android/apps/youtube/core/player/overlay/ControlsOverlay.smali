.class public interface abstract Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/ax;


# virtual methods
.method public abstract a(Ljava/util/List;)V
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public abstract f()V
.end method

.method public abstract g()V
.end method

.method public abstract onKeyDown(ILandroid/view/KeyEvent;)Z
.end method

.method public abstract onKeyUp(ILandroid/view/KeyEvent;)Z
.end method

.method public abstract setAndShowEnded()V
.end method

.method public abstract setAndShowPaused()V
.end method

.method public abstract setAudioOnlyEnabled(Z)V
.end method

.method public abstract setCcEnabled(Z)V
.end method

.method public abstract setErrorAndShowMessage(Ljava/lang/String;Z)V
.end method

.method public abstract setFullscreen(Z)V
.end method

.method public abstract setHQ(Z)V
.end method

.method public abstract setHQisHD(Z)V
.end method

.method public abstract setHasCc(Z)V
.end method

.method public abstract setHasInfoCard(Z)V
.end method

.method public abstract setHasNext(Z)V
.end method

.method public abstract setHasPrevious(Z)V
.end method

.method public abstract setInitial()V
.end method

.method public abstract setLearnMoreEnabled(Z)V
.end method

.method public abstract setListener(Lcom/google/android/apps/youtube/core/player/overlay/p;)V
.end method

.method public abstract setLoading()V
.end method

.method public abstract setPlaying()V
.end method

.method public abstract setScrubbingEnabled(Z)V
.end method

.method public abstract setShowFullscreen(Z)V
.end method

.method public abstract setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V
.end method

.method public abstract setSupportsQualityToggle(Z)V
.end method

.method public abstract setTimes(III)V
.end method
