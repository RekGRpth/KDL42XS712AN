.class public final Lcom/google/android/apps/youtube/a/a/i;
.super Lcom/google/protobuf/micro/c;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Lcom/google/android/apps/youtube/a/a/d;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Ljava/lang/String;

.field private q:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/i;->b:Lcom/google/android/apps/youtube/a/a/d;

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->d:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->f:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->h:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->j:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->l:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->n:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/i;->p:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/a/a/i;->q:I

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/a/a/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/i;->b:Lcom/google/android/apps/youtube/a/a/d;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/a/a/d;)Lcom/google/android/apps/youtube/a/a/i;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->a:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/a/a/i;->b:Lcom/google/android/apps/youtube/a/a/d;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/i;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->o:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/a/a/i;->p:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Z)Lcom/google/android/apps/youtube/a/a/i;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->c:Z

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/a/a/i;->d:Z

    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/micro/b;)Lcom/google/protobuf/micro/c;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/android/apps/youtube/a/a/d;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/d;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/b;->a(Lcom/google/protobuf/micro/c;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/i;->a(Lcom/google/android/apps/youtube/a/a/d;)Lcom/google/android/apps/youtube/a/a/i;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/i;->a(Z)Lcom/google/android/apps/youtube/a/a/i;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/i;->b(Z)Lcom/google/android/apps/youtube/a/a/i;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/i;->c(Z)Lcom/google/android/apps/youtube/a/a/i;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/i;->d(Z)Lcom/google/android/apps/youtube/a/a/i;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/i;->e(Z)Lcom/google/android/apps/youtube/a/a/i;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/i;->f(Z)Lcom/google/android/apps/youtube/a/a/i;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/i;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/i;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/i;->b:Lcom/google/android/apps/youtube/a/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILcom/google/protobuf/micro/c;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(IZ)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(IZ)V

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->g:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(IZ)V

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->i:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(IZ)V

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->k:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->l:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(IZ)V

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->m:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->n:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(IZ)V

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->o:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/i;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILjava/lang/String;)V

    :cond_7
    return-void
.end method

.method public final b(Z)Lcom/google/android/apps/youtube/a/a/i;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->e:Z

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/a/a/i;->f:Z

    return-object p0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->d:Z

    return v0
.end method

.method public final c(Z)Lcom/google/android/apps/youtube/a/a/i;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->g:Z

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/a/a/i;->h:Z

    return-object p0
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->f:Z

    return v0
.end method

.method public final d(Z)Lcom/google/android/apps/youtube/a/a/i;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->i:Z

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/a/a/i;->j:Z

    return-object p0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->h:Z

    return v0
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/a/a/i;->q:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/a/a/i;->f()I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/a/a/i;->q:I

    return v0
.end method

.method public final e(Z)Lcom/google/android/apps/youtube/a/a/i;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->k:Z

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/a/a/i;->l:Z

    return-object p0
.end method

.method public final f()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/i;->b:Lcom/google/android/apps/youtube/a/a/d;

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILcom/google/protobuf/micro/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/a/a/i;->d:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->e:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/a/a/i;->f:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->g:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/a/a/i;->h:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->i:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/a/a/i;->j:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->k:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/a/a/i;->l:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->m:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/a/a/i;->n:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/i;->o:Z

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/i;->p:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iput v0, p0, Lcom/google/android/apps/youtube/a/a/i;->q:I

    return v0
.end method

.method public final f(Z)Lcom/google/android/apps/youtube/a/a/i;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->m:Z

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/a/a/i;->n:Z

    return-object p0
.end method

.method public final g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->j:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->l:Z

    return v0
.end method

.method public final i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/i;->n:Z

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/i;->p:Ljava/lang/String;

    return-object v0
.end method
