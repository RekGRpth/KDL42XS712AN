.class public final Lcom/google/android/apps/youtube/a/a/g;
.super Lcom/google/protobuf/micro/c;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:J

.field private e:Z

.field private f:J

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/g;->b:Ljava/lang/String;

    iput-wide v1, p0, Lcom/google/android/apps/youtube/a/a/g;->d:J

    iput-wide v1, p0, Lcom/google/android/apps/youtube/a/a/g;->f:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/a/a/g;->g:I

    return-void
.end method


# virtual methods
.method public final a(J)Lcom/google/android/apps/youtube/a/a/g;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/g;->c:Z

    iput-wide p1, p0, Lcom/google/android/apps/youtube/a/a/g;->d:J

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/g;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/g;->a:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/a/a/g;->b:Ljava/lang/String;

    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/micro/b;)Lcom/google/protobuf/micro/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/g;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/g;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/a/a/g;->a(J)Lcom/google/android/apps/youtube/a/a/g;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/a/a/g;->b(J)Lcom/google/android/apps/youtube/a/a/g;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/g;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/g;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/g;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILjava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/g;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    iget-wide v1, p0, Lcom/google/android/apps/youtube/a/a/g;->d:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(IJ)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/g;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x4

    iget-wide v1, p0, Lcom/google/android/apps/youtube/a/a/g;->f:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(IJ)V

    :cond_2
    return-void
.end method

.method public final b(J)Lcom/google/android/apps/youtube/a/a/g;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/g;->e:Z

    iput-wide p1, p0, Lcom/google/android/apps/youtube/a/a/g;->f:J

    return-object p0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/g;->a:Z

    return v0
.end method

.method public final c()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/a/a/g;->d:J

    return-wide v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/g;->c:Z

    return v0
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/a/a/g;->g:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/a/a/g;->f()I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/a/a/g;->g:I

    return v0
.end method

.method public final f()I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/g;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/g;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/g;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/apps/youtube/a/a/g;->d:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/g;->e:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/apps/youtube/a/a/g;->f:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/google/android/apps/youtube/a/a/g;->g:I

    return v0
.end method

.method public final g()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/a/a/g;->f:J

    return-wide v0
.end method
