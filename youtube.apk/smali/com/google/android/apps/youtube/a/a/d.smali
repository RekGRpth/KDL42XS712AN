.class public final Lcom/google/android/apps/youtube/a/a/d;
.super Lcom/google/protobuf/micro/c;
.source "SourceFile"


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:I

.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Ljava/util/List;

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:I

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:Z

.field private k:Lcom/google/protobuf/micro/a;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:I

.field private t:Z

.field private u:I

.field private v:Z

.field private w:Ljava/lang/String;

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->b:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->c:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->e:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/apps/youtube/a/a/d;->g:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->i:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/micro/a;->a:Lcom/google/protobuf/micro/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->k:Lcom/google/protobuf/micro/a;

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->m:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->o:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->q:Z

    iput v1, p0, Lcom/google/android/apps/youtube/a/a/d;->s:I

    iput v1, p0, Lcom/google/android/apps/youtube/a/a/d;->u:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->w:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->y:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->A:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->C:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/a/a/d;->D:I

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/youtube/a/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->f:Z

    iput p1, p0, Lcom/google/android/apps/youtube/a/a/d;->g:I

    return-object p0
.end method

.method public final a(Lcom/google/protobuf/micro/a;)Lcom/google/android/apps/youtube/a/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->j:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/a/a/d;->k:Lcom/google/protobuf/micro/a;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->a:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/a/a/d;->b:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Z)Lcom/google/android/apps/youtube/a/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->l:Z

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/a/a/d;->m:Z

    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/micro/b;)Lcom/google/protobuf/micro/c;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/d;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/d;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/d;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/d;->a(I)Lcom/google/android/apps/youtube/a/a/d;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/d;->d(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->f()Lcom/google/protobuf/micro/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/d;->a(Lcom/google/protobuf/micro/a;)Lcom/google/android/apps/youtube/a/a/d;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/d;->a(Z)Lcom/google/android/apps/youtube/a/a/d;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/d;->b(Z)Lcom/google/android/apps/youtube/a/a/d;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/d;->c(Z)Lcom/google/android/apps/youtube/a/a/d;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/d;->b(I)Lcom/google/android/apps/youtube/a/a/d;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/d;->c(I)Lcom/google/android/apps/youtube/a/a/d;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/d;->e(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/d;->d(Z)Lcom/google/android/apps/youtube/a/a/d;

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/d;->e(Z)Lcom/google/android/apps/youtube/a/a/d;

    goto :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/d;->f(Z)Lcom/google/android/apps/youtube/a/a/d;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
    .end sparse-switch
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/d;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->d:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/d;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILjava/lang/String;)V

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->f:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/apps/youtube/a/a/d;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(II)V

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->h:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/d;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILjava/lang/String;)V

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->j:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/d;->k:Lcom/google/protobuf/micro/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILcom/google/protobuf/micro/a;)V

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->l:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(IZ)V

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->n:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->o:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(IZ)V

    :cond_7
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->p:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->q:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(IZ)V

    :cond_8
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->r:Z

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget v1, p0, Lcom/google/android/apps/youtube/a/a/d;->s:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(II)V

    :cond_9
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->t:Z

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget v1, p0, Lcom/google/android/apps/youtube/a/a/d;->u:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(II)V

    :cond_a
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->v:Z

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/d;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILjava/lang/String;)V

    :cond_b
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->x:Z

    if-eqz v0, :cond_c

    const/16 v0, 0xd

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->y:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(IZ)V

    :cond_c
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->z:Z

    if-eqz v0, :cond_d

    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->A:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(IZ)V

    :cond_d
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->B:Z

    if-eqz v0, :cond_e

    const/16 v0, 0xf

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->C:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(IZ)V

    :cond_e
    return-void
.end method

.method public final b(I)Lcom/google/android/apps/youtube/a/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->r:Z

    iput p1, p0, Lcom/google/android/apps/youtube/a/a/d;->s:I

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->c:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final b(Z)Lcom/google/android/apps/youtube/a/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->n:Z

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/a/a/d;->o:Z

    return-object p0
.end method

.method public final b()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->c:Ljava/util/List;

    return-object v0
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final c(I)Lcom/google/android/apps/youtube/a/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->t:Z

    iput p1, p0, Lcom/google/android/apps/youtube/a/a/d;->u:I

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->d:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/a/a/d;->e:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Z)Lcom/google/android/apps/youtube/a/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->p:Z

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/a/a/d;->q:Z

    return-object p0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->h:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/a/a/d;->i:Ljava/lang/String;

    return-object p0
.end method

.method public final d(Z)Lcom/google/android/apps/youtube/a/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->x:Z

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/a/a/d;->y:Z

    return-object p0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/a/a/d;->D:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/a/a/d;->f()I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/a/a/d;->D:I

    return v0
.end method

.method public final e(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->v:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/a/a/d;->w:Ljava/lang/String;

    return-object p0
.end method

.method public final e(Z)Lcom/google/android/apps/youtube/a/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->z:Z

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/a/a/d;->A:Z

    return-object p0
.end method

.method public final f()I
    .locals 4

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->a:Z

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/d;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v2, v0

    goto :goto_1

    :cond_0
    add-int v0, v1, v2

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/d;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->d:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/d;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->f:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/apps/youtube/a/a/d;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->h:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/d;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->j:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/d;->k:Lcom/google/protobuf/micro/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILcom/google/protobuf/micro/a;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->l:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/a/a/d;->m:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->n:Z

    if-eqz v1, :cond_6

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/a/a/d;->o:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->p:Z

    if-eqz v1, :cond_7

    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/a/a/d;->q:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->r:Z

    if-eqz v1, :cond_8

    const/16 v1, 0xa

    iget v2, p0, Lcom/google/android/apps/youtube/a/a/d;->s:I

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->t:Z

    if-eqz v1, :cond_9

    const/16 v1, 0xb

    iget v2, p0, Lcom/google/android/apps/youtube/a/a/d;->u:I

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->v:Z

    if-eqz v1, :cond_a

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/d;->w:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->x:Z

    if-eqz v1, :cond_b

    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/a/a/d;->y:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->z:Z

    if-eqz v1, :cond_c

    const/16 v1, 0xe

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/a/a/d;->A:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/d;->B:Z

    if-eqz v1, :cond_d

    const/16 v1, 0xf

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/a/a/d;->C:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iput v0, p0, Lcom/google/android/apps/youtube/a/a/d;->D:I

    return v0

    :cond_e
    move v1, v2

    goto/16 :goto_0
.end method

.method public final f(Z)Lcom/google/android/apps/youtube/a/a/d;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->B:Z

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/a/a/d;->C:Z

    return-object p0
.end method

.method public final g()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/a/a/d;->g:I

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Lcom/google/protobuf/micro/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->k:Lcom/google/protobuf/micro/a;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->m:Z

    return v0
.end method

.method public final k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->o:Z

    return v0
.end method

.method public final l()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->q:Z

    return v0
.end method

.method public final m()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/a/a/d;->s:I

    return v0
.end method

.method public final n()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/a/a/d;->u:I

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/d;->w:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->y:Z

    return v0
.end method

.method public final q()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->A:Z

    return v0
.end method

.method public final r()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/d;->C:Z

    return v0
.end method
