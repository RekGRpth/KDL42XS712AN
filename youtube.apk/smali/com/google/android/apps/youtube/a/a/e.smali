.class public final Lcom/google/android/apps/youtube/a/a/e;
.super Lcom/google/protobuf/micro/c;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Lcom/google/android/apps/youtube/a/a/h;

.field private c:Z

.field private d:Lcom/google/android/apps/youtube/a/a/h;

.field private e:Z

.field private f:Lcom/google/android/apps/youtube/a/a/h;

.field private g:Z

.field private h:Lcom/google/android/apps/youtube/a/a/h;

.field private i:Ljava/util/List;

.field private j:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/e;->b:Lcom/google/android/apps/youtube/a/a/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/e;->d:Lcom/google/android/apps/youtube/a/a/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/e;->f:Lcom/google/android/apps/youtube/a/a/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/e;->h:Lcom/google/android/apps/youtube/a/a/h;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/e;->i:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/a/a/e;->j:I

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/a/a/h;)Lcom/google/android/apps/youtube/a/a/e;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/e;->a:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/a/a/e;->b:Lcom/google/android/apps/youtube/a/a/h;

    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/micro/b;)Lcom/google/protobuf/micro/c;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/android/apps/youtube/a/a/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/h;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/b;->a(Lcom/google/protobuf/micro/c;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/e;->a(Lcom/google/android/apps/youtube/a/a/h;)Lcom/google/android/apps/youtube/a/a/e;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/android/apps/youtube/a/a/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/h;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/b;->a(Lcom/google/protobuf/micro/c;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/e;->b(Lcom/google/android/apps/youtube/a/a/h;)Lcom/google/android/apps/youtube/a/a/e;

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/android/apps/youtube/a/a/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/h;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/b;->a(Lcom/google/protobuf/micro/c;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/e;->c(Lcom/google/android/apps/youtube/a/a/h;)Lcom/google/android/apps/youtube/a/a/e;

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/android/apps/youtube/a/a/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/h;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/b;->a(Lcom/google/protobuf/micro/c;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/e;->d(Lcom/google/android/apps/youtube/a/a/h;)Lcom/google/android/apps/youtube/a/a/e;

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/android/apps/youtube/a/a/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/h;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/b;->a(Lcom/google/protobuf/micro/c;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/e;->e(Lcom/google/android/apps/youtube/a/a/h;)Lcom/google/android/apps/youtube/a/a/e;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/e;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/e;->b:Lcom/google/android/apps/youtube/a/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILcom/google/protobuf/micro/c;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/e;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/e;->d:Lcom/google/android/apps/youtube/a/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILcom/google/protobuf/micro/c;)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/e;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/e;->f:Lcom/google/android/apps/youtube/a/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILcom/google/protobuf/micro/c;)V

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/e;->g:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/e;->h:Lcom/google/android/apps/youtube/a/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILcom/google/protobuf/micro/c;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/e;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/h;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILcom/google/protobuf/micro/c;)V

    goto :goto_0

    :cond_4
    return-void
.end method

.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/e;->a:Z

    return v0
.end method

.method public final b(Lcom/google/android/apps/youtube/a/a/h;)Lcom/google/android/apps/youtube/a/a/e;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/e;->c:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/a/a/e;->d:Lcom/google/android/apps/youtube/a/a/h;

    return-object p0
.end method

.method public final b()Lcom/google/android/apps/youtube/a/a/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/e;->b:Lcom/google/android/apps/youtube/a/a/h;

    return-object v0
.end method

.method public final c(Lcom/google/android/apps/youtube/a/a/h;)Lcom/google/android/apps/youtube/a/a/e;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/e;->e:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/a/a/e;->f:Lcom/google/android/apps/youtube/a/a/h;

    return-object p0
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/e;->c:Z

    return v0
.end method

.method public final d(Lcom/google/android/apps/youtube/a/a/h;)Lcom/google/android/apps/youtube/a/a/e;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/e;->g:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/a/a/e;->h:Lcom/google/android/apps/youtube/a/a/h;

    return-object p0
.end method

.method public final d()Lcom/google/android/apps/youtube/a/a/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/e;->d:Lcom/google/android/apps/youtube/a/a/h;

    return-object v0
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/a/a/e;->j:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/a/a/e;->f()I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/a/a/e;->j:I

    return v0
.end method

.method public final e(Lcom/google/android/apps/youtube/a/a/h;)Lcom/google/android/apps/youtube/a/a/e;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/e;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/e;->i:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/e;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final f()I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/e;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/e;->b:Lcom/google/android/apps/youtube/a/a/h;

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILcom/google/protobuf/micro/c;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/e;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/e;->d:Lcom/google/android/apps/youtube/a/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILcom/google/protobuf/micro/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/e;->e:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/e;->f:Lcom/google/android/apps/youtube/a/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILcom/google/protobuf/micro/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/e;->g:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/e;->h:Lcom/google/android/apps/youtube/a/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILcom/google/protobuf/micro/c;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/e;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/h;

    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILcom/google/protobuf/micro/c;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_4
    iput v1, p0, Lcom/google/android/apps/youtube/a/a/e;->j:I

    return v1
.end method

.method public final g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/e;->e:Z

    return v0
.end method

.method public final h()Lcom/google/android/apps/youtube/a/a/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/e;->f:Lcom/google/android/apps/youtube/a/a/h;

    return-object v0
.end method

.method public final i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/e;->g:Z

    return v0
.end method

.method public final j()Lcom/google/android/apps/youtube/a/a/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/e;->h:Lcom/google/android/apps/youtube/a/a/h;

    return-object v0
.end method

.method public final k()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/e;->i:Ljava/util/List;

    return-object v0
.end method
