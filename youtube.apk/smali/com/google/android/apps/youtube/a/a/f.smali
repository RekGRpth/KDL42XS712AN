.class public final Lcom/google/android/apps/youtube/a/a/f;
.super Lcom/google/protobuf/micro/c;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Lcom/google/protobuf/micro/a;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    sget-object v0, Lcom/google/protobuf/micro/a;->a:Lcom/google/protobuf/micro/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/f;->b:Lcom/google/protobuf/micro/a;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/f;->d:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/a/a/f;->e:I

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/micro/a;)Lcom/google/android/apps/youtube/a/a/f;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/f;->a:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/a/a/f;->b:Lcom/google/protobuf/micro/a;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/f;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/f;->c:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/a/a/f;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final a()Lcom/google/protobuf/micro/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/f;->b:Lcom/google/protobuf/micro/a;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/micro/b;)Lcom/google/protobuf/micro/c;
    .locals 1

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->f()Lcom/google/protobuf/micro/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/f;->a(Lcom/google/protobuf/micro/a;)Lcom/google/android/apps/youtube/a/a/f;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/f;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/f;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/f;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/f;->b:Lcom/google/protobuf/micro/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILcom/google/protobuf/micro/a;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/f;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/f;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILjava/lang/String;)V

    :cond_1
    return-void
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/f;->a:Z

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/f;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/f;->c:Z

    return v0
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/a/a/f;->e:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/a/a/f;->f()I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/a/a/f;->e:I

    return v0
.end method

.method public final f()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/f;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/f;->b:Lcom/google/protobuf/micro/a;

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILcom/google/protobuf/micro/a;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/f;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/f;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iput v0, p0, Lcom/google/android/apps/youtube/a/a/f;->e:I

    return v0
.end method
