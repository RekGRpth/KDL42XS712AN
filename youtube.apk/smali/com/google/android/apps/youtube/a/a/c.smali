.class public final Lcom/google/android/apps/youtube/a/a/c;
.super Lcom/google/protobuf/micro/c;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:I

.field private e:Z

.field private f:I

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Ljava/util/List;

.field private j:Z

.field private k:Lcom/google/protobuf/micro/a;

.field private l:Z

.field private m:J

.field private n:Z

.field private o:Ljava/lang/String;

.field private p:Z

.field private q:J

.field private r:Z

.field private s:I

.field private t:Z

.field private u:J

.field private v:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/micro/c;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/c;->b:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/apps/youtube/a/a/c;->d:I

    iput v1, p0, Lcom/google/android/apps/youtube/a/a/c;->f:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/c;->h:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/c;->i:Ljava/util/List;

    sget-object v0, Lcom/google/protobuf/micro/a;->a:Lcom/google/protobuf/micro/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/c;->k:Lcom/google/protobuf/micro/a;

    iput-wide v2, p0, Lcom/google/android/apps/youtube/a/a/c;->m:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/c;->o:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/android/apps/youtube/a/a/c;->q:J

    iput v1, p0, Lcom/google/android/apps/youtube/a/a/c;->s:I

    iput-wide v2, p0, Lcom/google/android/apps/youtube/a/a/c;->u:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/a/a/c;->v:I

    return-void
.end method

.method public static a([B)Lcom/google/android/apps/youtube/a/a/c;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/a/a/c;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/c;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/a/a/c;->b([B)Lcom/google/protobuf/micro/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/c;

    return-object v0
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/youtube/a/a/c;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->c:Z

    iput p1, p0, Lcom/google/android/apps/youtube/a/a/c;->d:I

    return-object p0
.end method

.method public final a(J)Lcom/google/android/apps/youtube/a/a/c;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->l:Z

    iput-wide p1, p0, Lcom/google/android/apps/youtube/a/a/c;->m:J

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/a/a/b;)Lcom/google/android/apps/youtube/a/a/c;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/c;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/c;->i:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/c;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/c;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->a:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/a/a/c;->b:Ljava/lang/String;

    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/micro/b;)Lcom/google/protobuf/micro/c;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/c;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/c;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/c;->a(I)Lcom/google/android/apps/youtube/a/a/c;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/c;->b(I)Lcom/google/android/apps/youtube/a/a/c;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/c;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/c;

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/android/apps/youtube/a/a/b;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/b;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/micro/b;->a(Lcom/google/protobuf/micro/c;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/c;->a(Lcom/google/android/apps/youtube/a/a/b;)Lcom/google/android/apps/youtube/a/a/c;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->f()Lcom/google/protobuf/micro/a;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/c;->j:Z

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/c;->k:Lcom/google/protobuf/micro/a;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/a/a/c;->a(J)Lcom/google/android/apps/youtube/a/a/c;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/c;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/c;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/a/a/c;->b(J)Lcom/google/android/apps/youtube/a/a/c;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/a/a/c;->c(I)Lcom/google/android/apps/youtube/a/a/c;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/micro/b;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/a/a/c;->c(J)Lcom/google/android/apps/youtube/a/a/c;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/google/protobuf/micro/CodedOutputStreamMicro;)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/c;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILjava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/apps/youtube/a/a/c;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(II)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/apps/youtube/a/a/c;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(II)V

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->g:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/c;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILjava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/c;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/b;

    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILcom/google/protobuf/micro/c;)V

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->j:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/c;->k:Lcom/google/protobuf/micro/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILcom/google/protobuf/micro/a;)V

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->l:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-wide v1, p0, Lcom/google/android/apps/youtube/a/a/c;->m:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(IJ)V

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->n:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/c;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(ILjava/lang/String;)V

    :cond_7
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->p:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-wide v1, p0, Lcom/google/android/apps/youtube/a/a/c;->q:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(IJ)V

    :cond_8
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->r:Z

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget v1, p0, Lcom/google/android/apps/youtube/a/a/c;->s:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(II)V

    :cond_9
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->t:Z

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget-wide v1, p0, Lcom/google/android/apps/youtube/a/a/c;->u:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->a(IJ)V

    :cond_a
    return-void
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/a/a/c;->d:I

    return v0
.end method

.method public final b(I)Lcom/google/android/apps/youtube/a/a/c;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->e:Z

    iput p1, p0, Lcom/google/android/apps/youtube/a/a/c;->f:I

    return-object p0
.end method

.method public final b(J)Lcom/google/android/apps/youtube/a/a/c;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->p:Z

    iput-wide p1, p0, Lcom/google/android/apps/youtube/a/a/c;->q:J

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/c;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->g:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/a/a/c;->h:Ljava/lang/String;

    return-object p0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/a/a/c;->f:I

    return v0
.end method

.method public final c(I)Lcom/google/android/apps/youtube/a/a/c;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->r:Z

    iput p1, p0, Lcom/google/android/apps/youtube/a/a/c;->s:I

    return-object p0
.end method

.method public final c(J)Lcom/google/android/apps/youtube/a/a/c;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->t:Z

    iput-wide p1, p0, Lcom/google/android/apps/youtube/a/a/c;->u:J

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/c;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->n:Z

    iput-object p1, p0, Lcom/google/android/apps/youtube/a/a/c;->o:Ljava/lang/String;

    return-object p0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/c;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final e()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/a/a/c;->v:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/a/a/c;->f()I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/a/a/c;->v:I

    return v0
.end method

.method public final f()I
    .locals 4

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/c;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/c;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/c;->c:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/youtube/a/a/c;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/c;->e:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/apps/youtube/a/a/c;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/a/a/c;->g:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/c;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/c;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/b;

    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILcom/google/protobuf/micro/c;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->j:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/c;->k:Lcom/google/protobuf/micro/a;

    invoke-static {v0, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILcom/google/protobuf/micro/a;)I

    move-result v0

    add-int/2addr v1, v0

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->l:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-wide v2, p0, Lcom/google/android/apps/youtube/a/a/c;->m:J

    invoke-static {v0, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(IJ)I

    move-result v0

    add-int/2addr v1, v0

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->n:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/c;->o:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    :cond_7
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->p:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-wide v2, p0, Lcom/google/android/apps/youtube/a/a/c;->q:J

    invoke-static {v0, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(IJ)I

    move-result v0

    add-int/2addr v1, v0

    :cond_8
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->r:Z

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget v2, p0, Lcom/google/android/apps/youtube/a/a/c;->s:I

    invoke-static {v0, v2}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(II)I

    move-result v0

    add-int/2addr v1, v0

    :cond_9
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->t:Z

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget-wide v2, p0, Lcom/google/android/apps/youtube/a/a/c;->u:J

    invoke-static {v0, v2, v3}, Lcom/google/protobuf/micro/CodedOutputStreamMicro;->b(IJ)I

    move-result v0

    add-int/2addr v1, v0

    :cond_a
    iput v1, p0, Lcom/google/android/apps/youtube/a/a/c;->v:I

    return v1
.end method

.method public final g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->g:Z

    return v0
.end method

.method public final h()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/c;->i:Ljava/util/List;

    return-object v0
.end method

.method public final i()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/a/a/c;->m:J

    return-wide v0
.end method

.method public final j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/a/a/c;->l:Z

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/c;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final l()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/a/a/c;->q:J

    return-wide v0
.end method

.method public final m()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/a/a/c;->s:I

    return v0
.end method

.method public final n()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/a/a/c;->u:J

    return-wide v0
.end method
