.class public final Lcom/google/android/apps/youtube/a/a/a/c;
.super Lcom/google/protobuf/nano/b;
.source "SourceFile"


# static fields
.field public static final a:[Lcom/google/android/apps/youtube/a/a/a/c;


# instance fields
.field public b:Ljava/lang/String;

.field public c:[Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:I

.field public f:[Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:I

.field public i:I

.field public j:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/apps/youtube/a/a/a/c;

    sput-object v0, Lcom/google/android/apps/youtube/a/a/a/c;->a:[Lcom/google/android/apps/youtube/a/a/a/c;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/nano/b;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->b:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/f;->j:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->d:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->e:I

    sget-object v0, Lcom/google/protobuf/nano/f;->j:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->f:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->g:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->h:I

    iput v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->i:I

    sget-object v0, Lcom/google/protobuf/nano/f;->e:[I

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    invoke-static {v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    add-int/2addr v0, v3

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/apps/youtube/a/a/a/c;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->e:I

    if-eqz v2, :cond_3

    const/16 v2, 0x8

    iget v3, p0, Lcom/google/android/apps/youtube/a/a/a/c;->e:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->f:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->f:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/youtube/a/a/a/c;->f:[Ljava/lang/String;

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_4

    aget-object v6, v4, v2

    invoke-static {v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v3, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    add-int/2addr v0, v3

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->f:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->g:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/android/apps/youtube/a/a/a/c;->g:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->h:I

    if-eqz v2, :cond_7

    const/16 v2, 0xd

    iget v3, p0, Lcom/google/android/apps/youtube/a/a/a/c;->h:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->i:I

    if-eqz v2, :cond_8

    const/16 v2, 0x3e9

    iget v3, p0, Lcom/google/android/apps/youtube/a/a/a/c;->i:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    array-length v2, v2

    if-lez v2, :cond_a

    iget-object v3, p0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v1, v4, :cond_9

    aget v5, v3, v1

    invoke-static {v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(I)I

    move-result v5

    add-int/2addr v2, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_9
    add-int/2addr v0, v2

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->dl:Ljava/util/List;

    invoke-static {v1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->dm:I

    return v0

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
    .locals 4

    const/4 v3, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->dl:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->dl:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->dl:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    :cond_3
    iput v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->e:I

    goto :goto_0

    :cond_4
    iput v3, p0, Lcom/google/android/apps/youtube/a/a/a/c;->e:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x52

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->f:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->f:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->f:[Ljava/lang/String;

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->f:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->h:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->i:I

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x1f50

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/f;->a(Lcom/google/protobuf/nano/a;I)I

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()I

    move-result v2

    aput v2, v1, v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x40 -> :sswitch_4
        0x52 -> :sswitch_5
        0x5a -> :sswitch_6
        0x68 -> :sswitch_7
        0x1f48 -> :sswitch_8
        0x1f50 -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_2
    iget v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->e:I

    if-eqz v1, :cond_3

    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->e:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->f:[Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->f:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    const/16 v5, 0xa

    invoke-virtual {p1, v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(ILjava/lang/String;)V

    :cond_5
    iget v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->h:I

    if-eqz v1, :cond_6

    const/16 v1, 0xd

    iget v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->h:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_6
    iget v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->i:I

    if-eqz v1, :cond_7

    const/16 v1, 0x3e9

    iget v2, p0, Lcom/google/android/apps/youtube/a/a/a/c;->i:I

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_8

    aget v3, v1, v0

    const/16 v4, 0x3ea

    invoke-virtual {p1, v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/a/a/a/c;->dl:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/protobuf/nano/f;->a(Ljava/util/List;Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    return-void
.end method
