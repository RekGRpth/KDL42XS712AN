.class final Lcom/google/android/apps/youtube/api/jar/a/ex;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field private a:Lcom/google/android/apps/youtube/api/b/a/bd;

.field private b:Lcom/google/android/apps/youtube/api/jar/a/ey;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/api/b/a/bd;Lcom/google/android/apps/youtube/api/jar/a/ey;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/b/a/bd;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ex;->a:Lcom/google/android/apps/youtube/api/b/a/bd;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/a/ey;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ex;->b:Lcom/google/android/apps/youtube/api/jar/a/ey;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ex;->a:Lcom/google/android/apps/youtube/api/b/a/bd;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ex;->b:Lcom/google/android/apps/youtube/api/jar/a/ey;

    return-void
.end method

.method public final onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ex;->a:Lcom/google/android/apps/youtube/api/b/a/bd;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/view/Surface;

    invoke-direct {v0, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/ex;->a:Lcom/google/android/apps/youtube/api/b/a/bd;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/api/b/a/bd;->a(Landroid/view/Surface;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/ex;->b:Lcom/google/android/apps/youtube/api/jar/a/ey;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/api/jar/a/ey;->a(Landroid/view/Surface;)V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ex;->a:Lcom/google/android/apps/youtube/api/b/a/bd;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ex;->a:Lcom/google/android/apps/youtube/api/b/a/bd;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/b/a/bd;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ex;->b:Lcom/google/android/apps/youtube/api/jar/a/ey;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ey;->f()V

    :cond_0
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ex;->a:Lcom/google/android/apps/youtube/api/b/a/bd;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ex;->a:Lcom/google/android/apps/youtube/api/b/a/bd;

    invoke-interface {v0, p2, p3}, Lcom/google/android/apps/youtube/api/b/a/bd;->a(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ex;->a:Lcom/google/android/apps/youtube/api/b/a/bd;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ex;->a:Lcom/google/android/apps/youtube/api/b/a/bd;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/b/a/bd;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
