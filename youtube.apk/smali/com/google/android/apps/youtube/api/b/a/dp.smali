.class public final Lcom/google/android/apps/youtube/api/b/a/dp;
.super Lcom/google/android/apps/youtube/api/b/a/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/api/b/a/df;


# instance fields
.field private final b:Lcom/google/android/apps/youtube/api/b/a/de;

.field private c:Landroid/view/Surface;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/api/b/a/de;Lcom/google/android/apps/youtube/api/jar/a/ct;)V
    .locals 1

    invoke-direct {p0, p2}, Lcom/google/android/apps/youtube/api/b/a/a;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ct;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/b/a/de;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->b:Lcom/google/android/apps/youtube/api/b/a/de;

    invoke-virtual {p1, p0}, Lcom/google/android/apps/youtube/api/b/a/de;->a(Lcom/google/android/apps/youtube/api/b/a/df;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Surface;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->c:Landroid/view/Surface;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->a:Lcom/google/android/apps/youtube/medialib/player/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->a:Lcom/google/android/apps/youtube/medialib/player/z;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/z;->a()V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/medialib/player/n;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->c:Landroid/view/Surface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MediaPlayer should only be attached after Surface has been created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->c:Landroid/view/Surface;

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/medialib/player/n;->a(Landroid/view/Surface;)V

    return-void
.end method

.method public final a(Lcom/google/android/exoplayer/d;Lcom/google/android/exoplayer/ak;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->c:Landroid/view/Surface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ExoPlayer should only be attached after Surface has been created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->c:Landroid/view/Surface;

    invoke-interface {p1, p2, v0, v1}, Lcom/google/android/exoplayer/d;->a(Lcom/google/android/exoplayer/e;ILjava/lang/Object;)V

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/medialib/player/n;)V
    .locals 1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/medialib/player/n;->a(Landroid/view/Surface;)V

    return-void
.end method

.method public final b(Lcom/google/android/exoplayer/d;Lcom/google/android/exoplayer/ak;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-interface {p1, p2, v0, v1}, Lcom/google/android/exoplayer/d;->a(Lcom/google/android/exoplayer/e;ILjava/lang/Object;)V

    return-void
.end method

.method public final b_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->a:Lcom/google/android/apps/youtube/medialib/player/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->a:Lcom/google/android/apps/youtube/medialib/player/z;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/z;->b()V

    :cond_0
    return-void
.end method

.method public final c_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->a:Lcom/google/android/apps/youtube/medialib/player/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->a:Lcom/google/android/apps/youtube/medialib/player/z;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/z;->b()V

    :cond_0
    return-void
.end method

.method public final d_()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->c:Landroid/view/Surface;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->a:Lcom/google/android/apps/youtube/medialib/player/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->a:Lcom/google/android/apps/youtube/medialib/player/z;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/z;->c()V

    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->c:Landroid/view/Surface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->c:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    :cond_0
    return-void
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dp;->c:Landroid/view/Surface;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
