.class final Lcom/google/android/apps/youtube/api/jar/x;
.super Lcom/google/android/apps/youtube/api/jar/w;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/ui/h;


# instance fields
.field private final f:Lcom/google/android/apps/youtube/core/ui/g;

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/a;Lcom/google/android/apps/youtube/api/jar/z;Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;)V
    .locals 3

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/jar/w;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/a;Lcom/google/android/apps/youtube/api/jar/z;)V

    const-string v0, "playerOverlaysLayout cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/ui/g;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/api/jar/a;->d()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/api/jar/a;->a()Landroid/app/ActionBar;

    move-result-object v2

    invoke-direct {v0, v1, v2, p4, p0}, Lcom/google/android/apps/youtube/core/ui/g;-><init>(Landroid/view/Window;Landroid/app/ActionBar;Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;Lcom/google/android/apps/youtube/core/ui/h;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/x;->f:Lcom/google/android/apps/youtube/core/ui/g;

    return-void
.end method

.method private j()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/x;->e:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/x;->f:Lcom/google/android/apps/youtube/core/ui/g;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/x;->g:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/x;->h:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/ui/g;->c(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/x;->f:Lcom/google/android/apps/youtube/core/ui/g;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/g;->a()V

    invoke-super {p0}, Lcom/google/android/apps/youtube/api/jar/w;->a()V

    return-void
.end method

.method public final a(I)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/api/jar/w;->a(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/x;->f:Lcom/google/android/apps/youtube/core/ui/g;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/api/jar/x;->d:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/g;->d(Z)V

    return-void
.end method

.method public final d(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/api/jar/x;->h:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/x;->j()V

    return-void
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/x;->g:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/x;->j()V

    return-void
.end method

.method public final g()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/x;->g:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/x;->j()V

    return-void
.end method

.method final h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/x;->f:Lcom/google/android/apps/youtube/core/ui/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/g;->b(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/x;->j()V

    return-void
.end method

.method final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/x;->f:Lcom/google/android/apps/youtube/core/ui/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/g;->b(Z)V

    return-void
.end method
