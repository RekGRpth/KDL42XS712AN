.class public final Lcom/google/android/apps/youtube/api/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/client/as;

.field private final b:Ljava/util/concurrent/Executor;

.field private final c:Landroid/content/SharedPreferences;

.field private final d:Landroid/os/Handler;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private g:Lcom/google/android/apps/youtube/datalib/legacy/model/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/client/as;Ljava/util/concurrent/Executor;Landroid/os/Handler;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "appVersion cannot be null or empty"

    invoke-static {p6, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v0, "appDeveloperKey cannot be null or empty"

    invoke-static {p7, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    const-string v0, "deviceRegistrationClient cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/as;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/a;->a:Lcom/google/android/apps/youtube/core/client/as;

    const-string v0, "executor cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/a;->b:Ljava/util/concurrent/Executor;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/a;->d:Landroid/os/Handler;

    const-string v0, "preferences cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/a;->c:Landroid/content/SharedPreferences;

    const-string v0, "appPackage cannot be null or empty"

    invoke-static {p5, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/a;->f:Ljava/lang/String;

    const-string v0, "_%s_%s_%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p5, v1, v2

    const/4 v2, 0x1

    aput-object p6, v1, v2

    const/4 v2, 0x2

    aput-object p7, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/a;->e:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/a;)Lcom/google/android/apps/youtube/datalib/legacy/model/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/b;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/a;Lcom/google/android/apps/youtube/datalib/legacy/model/b;)Lcom/google/android/apps/youtube/datalib/legacy/model/b;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/a;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/b;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/a;Lcom/google/android/apps/youtube/api/f;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/d;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/d;-><init>(Lcom/google/android/apps/youtube/api/a;Lcom/google/android/apps/youtube/api/f;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/a;Lcom/google/android/apps/youtube/api/f;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/e;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/api/e;-><init>(Lcom/google/android/apps/youtube/api/a;Lcom/google/android/apps/youtube/api/f;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/a;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a;->c:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/api/a;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/api/a;)Lcom/google/android/apps/youtube/core/client/as;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a;->a:Lcom/google/android/apps/youtube/core/client/as;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/b;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Need to call init() first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/b;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/b;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "%s, client-id=\"%s\""

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/a;->f:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/api/f;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/b;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/youtube/api/f;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a;->c:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/a;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/b;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/a;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/b;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a;->g:Lcom/google/android/apps/youtube/datalib/legacy/model/b;

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/google/android/apps/youtube/api/f;->a()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/a;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/api/b;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/b;-><init>(Lcom/google/android/apps/youtube/api/a;Lcom/google/android/apps/youtube/api/f;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
