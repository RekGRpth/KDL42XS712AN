.class public final Lcom/google/android/apps/youtube/api/jar/a/ae;
.super Lcom/google/android/apps/youtube/api/jar/a/ci;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/overlay/g;

.field private final b:Landroid/os/Handler;

.field private c:Lcom/google/android/apps/youtube/api/jar/a/ay;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/g;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/ci;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/g;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->a:Lcom/google/android/apps/youtube/core/player/overlay/g;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/ae;)Lcom/google/android/apps/youtube/api/jar/a/ay;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->c:Lcom/google/android/apps/youtube/api/jar/a/ay;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/ae;Lcom/google/android/apps/youtube/api/jar/a/ay;)Lcom/google/android/apps/youtube/api/jar/a/ay;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->c:Lcom/google/android/apps/youtube/api/jar/a/ay;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/jar/a/ae;)Lcom/google/android/apps/youtube/core/player/overlay/g;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->a:Lcom/google/android/apps/youtube/core/player/overlay/g;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->c:Lcom/google/android/apps/youtube/api/jar/a/ay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->c:Lcom/google/android/apps/youtube/api/jar/a/ay;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/a/ay;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->c:Lcom/google/android/apps/youtube/api/jar/a/ay;

    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/as;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/as;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/api/b/a/ai;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/af;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/af;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;Lcom/google/android/apps/youtube/api/b/a/ai;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/an;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/an;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/av;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/av;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ah;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/ah;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ar;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ar;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/aq;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/aq;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Landroid/graphics/Bitmap;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/aw;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/aw;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/aj;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/aj;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/at;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/at;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c(Landroid/graphics/Bitmap;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ai;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ai;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ap;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ap;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/au;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/au;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d(Landroid/graphics/Bitmap;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ao;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ao;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ax;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/ax;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ag;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/ag;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ak;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/ak;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/al;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/al;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ae;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/am;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/am;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ae;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
