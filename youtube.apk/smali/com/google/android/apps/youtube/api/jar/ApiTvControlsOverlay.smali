.class public Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/youtube/api/jar/b;


# static fields
.field private static final a:Ljava/util/Map;


# instance fields
.field private final b:Lcom/google/android/apps/youtube/api/jar/aa;

.field private final c:Landroid/view/View;

.field private final d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

.field private final e:Landroid/view/View;

.field private final f:Landroid/view/View;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/Button;

.field private final i:Landroid/widget/Button;

.field private final j:Lcom/google/android/apps/youtube/core/player/overlay/bb;

.field private final k:I

.field private final l:Landroid/os/Handler;

.field private m:Lcom/google/android/apps/youtube/core/player/overlay/p;

.field private n:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

.field private o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:I

.field private t:Z

.field private final u:Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const/16 v1, 0x5a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;->FAST_FORWARD:Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x59

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;->REWIND:Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x56

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;->PAUSE:Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x7f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;->PAUSE:Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x7e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;->PLAY:Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x58

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;->PREVIOUS:Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x57

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;->NEXT:Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/apps/youtube/api/jar/aa;)V
    .locals 4

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->c:Landroid/view/View;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/aa;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b:Lcom/google/android/apps/youtube/api/jar/aa;

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->NEW:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/api/e;->b:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/api/d;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->k:I

    new-instance v0, Lcom/google/android/apps/youtube/api/jar/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/api/jar/j;-><init>(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->l:Landroid/os/Handler;

    sget v0, Lcom/google/android/youtube/api/c;->c:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    sget v0, Lcom/google/android/youtube/api/c;->n:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->e:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/api/c;->e:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->f:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/api/c;->g:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->g:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/api/c;->f:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->h:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->h:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/youtube/api/jar/l;

    const/4 v0, 0x0

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/youtube/api/jar/l;-><init>(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;B)V

    sget v0, Lcom/google/android/youtube/api/c;->m:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/overlay/as;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->u:Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->u:Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->setMediaActionHelper(Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    sget v1, Lcom/google/android/youtube/api/c;->l:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    sget v1, Lcom/google/android/youtube/api/c;->k:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->i:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->i:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/bb;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/bb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->j:Lcom/google/android/apps/youtube/core/player/overlay/bb;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->f()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    return-object v0
.end method

.method private a(I)Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;
    .locals 2

    sget-object v0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x55

    if-eq p1, v0, :cond_1

    const/16 v0, 0x3e

    if-ne p1, v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;->PAUSE:Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;->PLAY:Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->m:Lcom/google/android/apps/youtube/core/player/overlay/p;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->s:I

    return v0
.end method

.method private h()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->l:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->l:Landroid/os/Handler;

    iget v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->k:I

    int-to-long v1, v1

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->j:Lcom/google/android/apps/youtube/core/player/overlay/bb;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/k;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/k;-><init>(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/core/player/overlay/bb;->a(Ljava/util/List;Lcom/google/android/apps/youtube/core/player/overlay/bd;)V

    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final c()Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 2

    const/4 v1, -0x1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a()V

    return-void
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->h()V

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 6

    const/16 v3, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->q:Z

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v5, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v2, v5, :cond_4

    sget-object v2, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView$PlaybackState;->PLAYING:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView$PlaybackState;

    :goto_1
    invoke-virtual {v4, v2}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->setPlaybackState(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView$PlaybackState;)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->e:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v5, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->LOADING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v2, v5, :cond_5

    move v2, v1

    :goto_2
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->f:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isError()Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v1

    :goto_3
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isError()Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->setErrorState(Z)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v5, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->LOADING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-eq v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v5, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->NEW:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-eq v4, v5, :cond_1

    move v3, v1

    :cond_1
    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->setVisibility(I)V

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->p:Z

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isError()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isError()Z

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->f()V

    :goto_4
    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->h()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->m:Lcom/google/android/apps/youtube/core/player/overlay/p;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->m:Lcom/google/android/apps/youtube/core/player/overlay/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->j()V

    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView$PlaybackState;->PAUSED:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView$PlaybackState;

    goto :goto_1

    :cond_5
    move v2, v3

    goto :goto_2

    :cond_6
    move v2, v3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_8

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;->PLAY:Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->setFocus(Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;)V

    :cond_8
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->q:Z

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->setVisibility(I)V

    goto :goto_4
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->q:Z

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->m:Lcom/google/android/apps/youtube/core/player/overlay/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->m:Lcom/google/android/apps/youtube/core/player/overlay/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->k()V

    :cond_0
    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->j:Lcom/google/android/apps/youtube/core/player/overlay/bb;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/bb;->a()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->h:Landroid/widget/Button;

    if-ne p1, v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->r:Z

    if-eqz v1, :cond_1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->r:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->m:Lcom/google/android/apps/youtube/core/player/overlay/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->l()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->i:Landroid/widget/Button;

    if-ne p1, v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->t:Z

    if-nez v1, :cond_2

    const/4 v0, 0x1

    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->setFullscreen(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->m:Lcom/google/android/apps/youtube/core/player/overlay/p;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->t:Z

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/p;->b(Z)V

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->e()V

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->a(I)Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->u:Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper;

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->n:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper;->a(Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->u:Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper;->a(Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;)V

    goto :goto_1

    :cond_3
    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_1

    :sswitch_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->q:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->e()V

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_1

    :sswitch_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->q:Z

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->f()V

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->a(I)Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->u:Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper;->b(Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper$Action;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 v0, 0x1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->q:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->e()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAndShowEnded()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->ENDED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->e()V

    return-void
.end method

.method public setAndShowPaused()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PAUSED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->l:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->e()V

    return-void
.end method

.method public setAudioOnlyEnabled(Z)V
    .locals 0

    return-void
.end method

.method public setCcEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->setCcEnabled(Z)V

    return-void
.end method

.method public setControlsPermanentlyHidden(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->p:Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->f()V

    :cond_0
    return-void
.end method

.method public setErrorAndShowMessage(IZ)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->setErrorAndShowMessage(Ljava/lang/String;Z)V

    return-void
.end method

.method public setErrorAndShowMessage(Ljava/lang/String;Z)V
    .locals 2

    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->RECOVERABLE_ERROR:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->h:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->e()V

    :goto_0
    iput-boolean p2, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->r:Z

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->UNRECOVERABLE_ERROR:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->h:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->e()V

    goto :goto_0
.end method

.method public setFullscreen(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->t:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->i:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setSelected(Z)V

    return-void
.end method

.method public setHQ(Z)V
    .locals 0

    return-void
.end method

.method public setHQisHD(Z)V
    .locals 0

    return-void
.end method

.method public setHasAudioOnly(Z)V
    .locals 0

    return-void
.end method

.method public setHasCc(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->setShowCcButton(Z)V

    return-void
.end method

.method public setHasInfoCard(Z)V
    .locals 0

    return-void
.end method

.method public setHasNext(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->setNextEnabled(Z)V

    return-void
.end method

.method public setHasPrevious(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->setPreviousEnabled(Z)V

    return-void
.end method

.method public setInitial()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->NEW:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->e()V

    return-void
.end method

.method public setLearnMoreEnabled(Z)V
    .locals 0

    return-void
.end method

.method public setListener(Lcom/google/android/apps/youtube/core/player/overlay/p;)V
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/api/jar/ai;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b:Lcom/google/android/apps/youtube/api/jar/aa;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/youtube/api/jar/ai;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/p;Lcom/google/android/apps/youtube/api/jar/aa;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->m:Lcom/google/android/apps/youtube/core/player/overlay/p;

    return-void
.end method

.method public setLoading()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->LOADING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->e()V

    return-void
.end method

.method public setMinimal(Z)V
    .locals 2

    if-eqz p1, :cond_0

    const-string v0, "MINIMAL mode is not supported for Google TV controls"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/youtube/player/internal/b/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public setOnPlayInYouTubeListener(Lcom/google/android/apps/youtube/api/jar/c;)V
    .locals 0

    return-void
.end method

.method public setPlaying()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->e()V

    return-void
.end method

.method public setScrubbingEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->setScrubbingEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->u:Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/MediaActionHelper;->a(Z)V

    return-void
.end method

.method public setShowAudioOnly(Z)V
    .locals 0

    return-void
.end method

.method public setShowFullscreen(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->i:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->n:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V

    return-void
.end method

.method public setSupportsQualityToggle(Z)V
    .locals 0

    return-void
.end method

.method public setTimes(III)V
    .locals 1

    iput p1, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->s:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsView;->a(III)V

    return-void
.end method

.method public setVideoTitle(Ljava/lang/String;)V
    .locals 0

    return-void
.end method
