.class public final Lcom/google/android/apps/youtube/api/ApiPlayer;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/api/p;

.field private final b:Lcom/google/android/apps/youtube/api/j;

.field private final c:Lcom/google/android/apps/youtube/core/player/ae;

.field private final d:Lcom/google/android/apps/youtube/common/c/a;

.field private final e:Lcom/google/android/apps/youtube/medialib/player/x;

.field private final f:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

.field private final g:Lcom/google/android/apps/youtube/core/player/overlay/c;

.field private final h:Lcom/google/android/apps/youtube/core/player/overlay/bo;

.field private final i:Lcom/google/android/apps/youtube/core/player/overlay/i;

.field private final j:Lcom/google/android/apps/youtube/core/player/overlay/q;

.field private final k:Lcom/google/android/apps/youtube/core/player/overlay/am;

.field private final l:Lcom/google/android/apps/youtube/core/player/overlay/az;

.field private final m:Lcom/google/android/apps/youtube/core/player/overlay/bs;

.field private final n:Lcom/google/android/apps/youtube/api/q;

.field private final o:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

.field private final p:Lcom/google/android/apps/youtube/core/player/overlay/bf;

.field private final q:Landroid/os/Handler;

.field private r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

.field private s:Z

.field private t:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/p;Lcom/google/android/apps/youtube/api/j;Lcom/google/android/apps/youtube/core/player/am;Lcom/google/android/apps/youtube/medialib/player/y;Lcom/google/android/apps/youtube/core/player/overlay/a;Lcom/google/android/apps/youtube/core/player/overlay/bm;Lcom/google/android/apps/youtube/core/player/overlay/g;Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;Lcom/google/android/apps/youtube/core/player/overlay/ak;Lcom/google/android/apps/youtube/core/player/overlay/be;Lcom/google/android/apps/youtube/core/player/overlay/br;)V
    .locals 25

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    invoke-static/range {p5 .. p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/api/p;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/api/j;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->b:Lcom/google/android/apps/youtube/api/j;

    invoke-static/range {p4 .. p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p6 .. p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p9 .. p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->f:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-static/range {p10 .. p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p11 .. p11}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p12 .. p12}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->e()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->d:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->j()Lcom/google/android/apps/youtube/medialib/a;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/medialib/a;->a(Landroid/content/Context;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    move-object/from16 v0, p5

    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Lcom/google/android/apps/youtube/medialib/player/y;)V

    new-instance v2, Landroid/os/Handler;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->q:Landroid/os/Handler;

    sget-object v2, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->UNINITIALIZED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    new-instance v2, Lcom/google/android/apps/youtube/api/q;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/youtube/api/q;-><init>(Lcom/google/android/apps/youtube/api/ApiPlayer;B)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->n:Lcom/google/android/apps/youtube/api/q;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->t()Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->o:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v8

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->h()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v23

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->a()Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-result-object v20

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->k()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v12

    new-instance v24, Lcom/google/android/apps/youtube/core/navigation/a;

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/a;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->n()Landroid/content/SharedPreferences;

    move-result-object v19

    new-instance v2, Lcom/google/android/apps/youtube/core/player/ae;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->d:Lcom/google/android/apps/youtube/common/c/a;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->o:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    const/4 v7, 0x0

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->g()Lcom/google/android/apps/youtube/core/client/e;

    move-result-object v9

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->m()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v10

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->l()Lcom/google/android/apps/youtube/core/au;

    move-result-object v11

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->c()Ljava/util/concurrent/Executor;

    move-result-object v13

    invoke-static {}, Lcom/google/android/apps/youtube/core/identity/l;->a()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v14

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->o()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v15

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->p()Lcom/google/android/apps/youtube/datalib/offline/n;

    move-result-object v16

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v17

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->r()Lcom/google/android/apps/youtube/core/player/w;

    move-result-object v18

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->s()Lcom/google/android/apps/youtube/core/player/ad;

    move-result-object v21

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->d()Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;

    move-result-object v22

    move-object/from16 v3, p1

    invoke-direct/range {v2 .. v22}, Lcom/google/android/apps/youtube/core/player/ae;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/medialib/player/x;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/e;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/au;Lcom/google/android/apps/youtube/core/Analytics;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/datalib/offline/n;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/player/w;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    new-instance v4, Lcom/google/android/apps/youtube/core/player/overlay/c;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    move-object/from16 v5, p6

    move-object/from16 v6, p1

    move-object/from16 v9, v23

    move-object v10, v12

    move-object/from16 v11, v24

    invoke-direct/range {v4 .. v11}, Lcom/google/android/apps/youtube/core/player/overlay/c;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/a;Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/ae;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/datalib/d/a;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->g:Lcom/google/android/apps/youtube/core/player/overlay/c;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/bo;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->b()Lcom/google/android/apps/youtube/common/e/n;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/player/ae;->b()Lcom/google/android/apps/youtube/core/player/au;

    move-result-object v6

    move-object/from16 v3, v20

    move-object/from16 v4, p7

    move-object/from16 v7, p4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/youtube/core/player/overlay/bo;-><init>(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;Lcom/google/android/apps/youtube/core/player/overlay/bm;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/player/au;Lcom/google/android/apps/youtube/core/player/am;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->h:Lcom/google/android/apps/youtube/core/player/overlay/bo;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/i;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p1

    move-object/from16 v4, p8

    move-object/from16 v5, v23

    move-object/from16 v6, v24

    move-object/from16 v7, p4

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/youtube/core/player/overlay/i;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/overlay/g;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/core/player/am;Lcom/google/android/apps/youtube/core/player/ae;Lcom/google/android/apps/youtube/datalib/innertube/t;Landroid/support/v4/app/l;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->i:Lcom/google/android/apps/youtube/core/player/overlay/i;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/q;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->d:Lcom/google/android/apps/youtube/common/c/a;

    move-object/from16 v0, p9

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/apps/youtube/core/player/overlay/q;-><init>(Lcom/google/android/apps/youtube/core/player/ae;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->j:Lcom/google/android/apps/youtube/core/player/overlay/q;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/am;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    move-object/from16 v0, p10

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/youtube/core/player/overlay/am;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/ak;Lcom/google/android/apps/youtube/core/player/ae;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->k:Lcom/google/android/apps/youtube/core/player/overlay/am;

    new-instance v8, Lcom/google/android/apps/youtube/core/player/overlay/bf;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->i()Lcom/google/android/apps/youtube/core/client/ce;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->j:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/youtube/api/j;->e()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v15

    move-object/from16 v9, p11

    move-object/from16 v13, v19

    move-object/from16 v14, p1

    invoke-direct/range {v8 .. v15}, Lcom/google/android/apps/youtube/core/player/overlay/bf;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/be;Lcom/google/android/apps/youtube/core/client/ce;Lcom/google/android/apps/youtube/core/player/overlay/q;Lcom/google/android/apps/youtube/core/Analytics;Landroid/content/SharedPreferences;Landroid/content/Context;Lcom/google/android/apps/youtube/common/c/a;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->p:Lcom/google/android/apps/youtube/core/player/overlay/bf;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/az;

    move-object/from16 v0, p4

    move-object/from16 v1, p9

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/az;-><init>(Lcom/google/android/apps/youtube/core/player/am;Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->l:Lcom/google/android/apps/youtube/core/player/overlay/az;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/bs;

    const/4 v3, 0x1

    move-object/from16 v0, p12

    move-object/from16 v1, v23

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/apps/youtube/core/player/overlay/bs;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/br;Lcom/google/android/apps/youtube/core/client/bj;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->m:Lcom/google/android/apps/youtube/core/player/overlay/bs;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/ApiPlayer;->n:Lcom/google/android/apps/youtube/api/q;

    invoke-interface {v2, v3}, Lcom/google/android/apps/youtube/medialib/player/x;->a(Landroid/os/Handler;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->s()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/ApiPlayer;)Lcom/google/android/apps/youtube/api/ApiPlayer$State;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    return-object v0
.end method

.method private a(Lcom/google/android/apps/youtube/api/ApiPlayer$State;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can not leave the DESTROYED state"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(ZLjava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/ApiPlayer;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->s:Z

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/api/ApiPlayer;)Lcom/google/android/apps/youtube/core/player/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/api/ApiPlayer;)Lcom/google/android/apps/youtube/api/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/api/ApiPlayer;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->s:Z

    return-void
.end method

.method private handlePlaybackServiceException(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    sget-object v0, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->UNINITIALIZED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(Lcom/google/android/apps/youtube/api/ApiPlayer$State;)V

    sget-object v0, Lcom/google/android/apps/youtube/api/o;->a:[I

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->reason:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "Unhandled ErrorReason in onError"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->UNKNOWN:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/api/p;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->NOT_PLAYABLE:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/api/p;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->b:Lcom/google/android/apps/youtube/api/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/j;->o()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->INTERNAL_ERROR:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/api/p;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->NETWORK_ERROR:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/api/p;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->USER_DECLINED_RESTRICTED_CONTENT:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/api/p;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleSequencerEndedEvent(Lcom/google/android/apps/youtube/core/player/event/t;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/p;->c()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->s:Z

    return-void
.end method

.method private handleSequencerNavigationRequestEvent(Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    sget-object v0, Lcom/google/android/apps/youtube/api/o;->b:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/SequencerNavigationRequestEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/p;->b()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/p;->a()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handleSequencerStageEvent(Lcom/google/android/apps/youtube/core/player/event/v;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/v;->a()Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->SEQUENCE_EMPTY:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->EMPTY_PLAYLIST:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/api/p;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->s:Z

    :cond_0
    return-void
.end method

.method private handleVideoControlsVisibilityEvent(Lcom/google/android/apps/youtube/core/player/event/aa;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-boolean v0, p1, Lcom/google/android/apps/youtube/core/player/event/aa;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/p;->j()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/p;->k()V

    goto :goto_0
.end method

.method private handleVideoFullscreenEvent(Lcom/google/android/apps/youtube/core/player/event/ab;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ab;->a()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/api/p;->a(Z)V

    return-void
.end method

.method private handleVideoStageEvent(Lcom/google/android/apps/youtube/core/player/event/ac;)V
    .locals 7
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->NEW:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->LOADING:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(Lcom/google/android/apps/youtube/api/ApiPlayer$State;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/p;->d()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->READY:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(Lcom/google/android/apps/youtube/api/ApiPlayer$State;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getTitle()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/player/ae;->e()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/player/ae;->f()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/core/player/ae;->p()Z

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/core/player/ae;->q()Z

    move-result v6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/youtube/api/p;->a(Ljava/lang/String;Ljava/lang/String;IIZZ)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->AD_LOADED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_3

    sget-object v0, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(Lcom/google/android/apps/youtube/api/ApiPlayer$State;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_AD_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/p;->e()V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_PLAYING_AD:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/p;->i()V

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->MEDIA_VIDEO_PLAY_REQUESTED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/p;->f()V

    goto :goto_0

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ENDED:Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/p;->g()V

    goto/16 :goto_0
.end method

.method private handleVideoTimeEvent(Lcom/google/android/apps/youtube/core/player/event/ae;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ae;->a()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ae;->b()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/api/p;->b(II)V

    :cond_0
    return-void
.end method

.method private r()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v2, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "This YouTubePlayer has been released - ignoring command."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, Lcom/google/android/youtube/player/internal/b/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private s()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->t:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->b:Lcom/google/android/apps/youtube/api/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/j;->e()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->b:Lcom/google/android/apps/youtube/api/j;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/api/j;->s()Lcom/google/android/apps/youtube/core/player/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->g:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->i:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->j:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->k:Lcom/google/android/apps/youtube/core/player/overlay/am;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->l:Lcom/google/android/apps/youtube/core/player/overlay/az;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->p:Lcom/google/android/apps/youtube/core/player/overlay/bf;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->h:Lcom/google/android/apps/youtube/core/player/overlay/bo;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->m:Lcom/google/android/apps/youtube/core/player/overlay/bs;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->t:Z

    :cond_0
    return-void
.end method

.method private t()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->b:Lcom/google/android/apps/youtube/api/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/j;->e()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->b:Lcom/google/android/apps/youtube/api/j;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/api/j;->s()Lcom/google/android/apps/youtube/core/player/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->g:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->i:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->j:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->k:Lcom/google/android/apps/youtube/core/player/overlay/am;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->l:Lcom/google/android/apps/youtube/core/player/overlay/az;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->p:Lcom/google/android/apps/youtube/core/player/overlay/bf;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->h:Lcom/google/android/apps/youtube/core/player/overlay/bo;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->m:Lcom/google/android/apps/youtube/core/player/overlay/bs;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->t:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->i()V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/ae;->c(I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/ae;->a(Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;)V

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 5

    const/4 v4, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const/4 v2, -0x1

    sget-object v3, Lcom/google/android/apps/youtube/core/client/WatchFeature;->PLAYER_EMBEDDED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-direct {v0, v1, v2, p2, v3}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Ljava/util/List;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->setWatchNextDisabled(Z)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->setStartPaused(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/ae;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 7

    const/4 v6, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    const-string v1, ""

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->PLAYER_EMBEDDED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Ljava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V

    invoke-virtual {v0, v6}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->setWatchNextDisabled(Z)V

    invoke-virtual {v0, v6}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->setStartPaused(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/ae;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;II)V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    sget-object v1, Lcom/google/android/apps/youtube/core/client/WatchFeature;->PLAYER_EMBEDDED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Ljava/util/List;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->setWatchNextDisabled(Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->setStartPaused(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/ae;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/ae;->c(Z)V

    return-void
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->f:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->d:Lcom/google/android/apps/youtube/common/c/a;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->PLAYER_CONTROL:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->i()V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/ae;->d(I)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 5

    const/4 v4, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const/4 v2, -0x1

    sget-object v3, Lcom/google/android/apps/youtube/core/client/WatchFeature;->PLAYER_EMBEDDED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-direct {v0, v1, v2, p2, v3}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Ljava/util/List;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->setWatchNextDisabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->d:Lcom/google/android/apps/youtube/common/c/a;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->NAVIGATION:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->setScriptedPlay(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/ae;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;II)V
    .locals 7

    const/4 v6, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    const-string v1, ""

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->PLAYER_EMBEDDED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Ljava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V

    invoke-virtual {v0, v6}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->setWatchNextDisabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->d:Lcom/google/android/apps/youtube/common/c/a;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->NAVIGATION:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    invoke-virtual {v0, v6}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->setScriptedPlay(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/ae;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    goto :goto_0
.end method

.method public final b(Ljava/util/List;II)V
    .locals 4

    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    sget-object v1, Lcom/google/android/apps/youtube/core/client/WatchFeature;->PLAYER_EMBEDDED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Ljava/util/List;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->setWatchNextDisabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->d:Lcom/google/android/apps/youtube/common/c/a;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->NAVIGATION:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->setScriptedPlay(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/ae;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/ae;->d(Z)V

    return-void
.end method

.method public final b(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->f:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->l()V

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/ae;->f(Z)V

    return-void
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->s:Z

    return v0
.end method

.method public final e()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->LOADING:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->q()Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->LOADING:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->p()Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->e()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Ignoring call to next() on YouTubePlayer as already at end of playlist."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/youtube/player/internal/b/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v0, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->LOADING:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(Lcom/google/android/apps/youtube/api/ApiPlayer$State;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->d:Lcom/google/android/apps/youtube/common/c/a;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->NAVIGATION:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->s()V

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->f()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Ignoring call to next() on YouTubePlayer as already at end of playlist."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/youtube/player/internal/b/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v0, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->LOADING:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(Lcom/google/android/apps/youtube/api/ApiPlayer$State;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->d:Lcom/google/android/apps/youtube/common/c/a;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;->NAVIGATION:Lcom/google/android/apps/youtube/core/player/event/ScriptedPlaybackEvent;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->r()V

    goto :goto_0
.end method

.method public final i()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->e()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->f()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->LOADED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->k()V

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->g()Z

    move-result v0

    return v0
.end method

.method public final m()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->f:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;->e()V

    return-void
.end method

.method public final n()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->s:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->C()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->t()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->e:Lcom/google/android/apps/youtube/medialib/player/x;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->n:Lcom/google/android/apps/youtube/api/q;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/medialib/player/x;->b(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->n:Lcom/google/android/apps/youtube/api/q;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/api/q;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->q:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->p:Lcom/google/android/apps/youtube/core/player/overlay/bf;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/bf;->a()V

    sget-object v0, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(Lcom/google/android/apps/youtube/api/ApiPlayer$State;)V

    :cond_0
    return-void
.end method

.method public final o()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->s()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->v()V

    :cond_0
    return-void
.end method

.method public final p()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->r:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->a:Lcom/google/android/apps/youtube/api/p;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/ae;->e()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/api/p;->a(I)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->t()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->A()V

    :cond_0
    return-void
.end method

.method public final q()Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiPlayer;->c:Lcom/google/android/apps/youtube/core/player/ae;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ae;->g(Z)Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    move-result-object v0

    return-object v0
.end method
