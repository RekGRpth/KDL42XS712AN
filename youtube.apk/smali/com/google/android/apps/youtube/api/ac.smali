.class public final Lcom/google/android/apps/youtube/api/ac;
.super Landroid/app/Dialog;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Z

.field private final c:Lcom/google/android/apps/youtube/api/ApiPlayer;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/apps/youtube/api/ApiPlayer;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;IIZZZ)V
    .locals 5

    invoke-static/range {p10 .. p11}, Lcom/google/android/apps/youtube/api/ac;->a(ZZ)I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p6, :cond_2

    invoke-virtual {p6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "One of videoId, playlistId or videoIds must not be null or empty"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ac;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/ApiPlayer;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ac;->c:Lcom/google/android/apps/youtube/api/ApiPlayer;

    iput-boolean p10, p0, Lcom/google/android/apps/youtube/api/ac;->b:Z

    if-eqz p10, :cond_3

    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/ac;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const v1, 0x1080011    # android.R.drawable.dialog_frame

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    const/16 v4, 0x11

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, p2, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/f;->b:I

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    move-object p2, v0

    :goto_1
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/ac;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    if-eqz p10, :cond_4

    const/4 v0, -0x2

    :goto_2
    const/16 v4, 0x11

    invoke-direct {v2, v3, v0, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v1, p2, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/api/ac;->setContentView(Landroid/view/View;)V

    if-eqz p6, :cond_6

    if-eqz p9, :cond_5

    invoke-virtual {p3, p6, p7, p8}, Lcom/google/android/apps/youtube/api/ApiPlayer;->b(Ljava/util/List;II)V

    :cond_1
    :goto_3
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/high16 v0, -0x1000000

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_1

    :cond_4
    const/4 v0, -0x1

    goto :goto_2

    :cond_5
    invoke-virtual {p3, p6, p7, p8}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(Ljava/util/List;II)V

    goto :goto_3

    :cond_6
    if-eqz p5, :cond_8

    if-eqz p9, :cond_7

    invoke-virtual {p3, p5, p7, p8}, Lcom/google/android/apps/youtube/api/ApiPlayer;->b(Ljava/lang/String;II)V

    goto :goto_3

    :cond_7
    invoke-virtual {p3, p5, p7, p8}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(Ljava/lang/String;II)V

    goto :goto_3

    :cond_8
    if-eqz p4, :cond_1

    if-eqz p9, :cond_9

    invoke-virtual {p3, p4, p8}, Lcom/google/android/apps/youtube/api/ApiPlayer;->b(Ljava/lang/String;I)V

    goto :goto_3

    :cond_9
    invoke-virtual {p3, p4, p8}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(Ljava/lang/String;I)V

    goto :goto_3
.end method

.method public static a(ZZ)I
    .locals 1

    if-eqz p0, :cond_1

    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/youtube/q;->d:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/youtube/q;->c:I

    goto :goto_0

    :cond_1
    sget v0, Lcom/google/android/youtube/q;->e:I

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ac;->c:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->p()V

    return-void
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/ac;->b:Z

    return v0
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ac;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ac;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final onStop()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ac;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ac;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    return-void
.end method
