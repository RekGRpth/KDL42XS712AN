.class public final Lcom/google/android/apps/youtube/api/b/a/dl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/bm;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/api/b/a/dm;

.field private final b:Lcom/google/android/apps/youtube/api/jar/a/di;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/apps/youtube/api/jar/a/di;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "client cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/a/di;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dl;->b:Lcom/google/android/apps/youtube/api/jar/a/di;

    new-instance v0, Lcom/google/android/apps/youtube/api/b/a/dm;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/dm;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dl;->a:Lcom/google/android/apps/youtube/api/b/a/dm;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dl;->a:Lcom/google/android/apps/youtube/api/b/a/dm;

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/api/jar/a/di;->a(Lcom/google/android/apps/youtube/api/b/a/bg;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dl;->b:Lcom/google/android/apps/youtube/api/jar/a/di;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dl;->b:Lcom/google/android/apps/youtube/api/jar/a/di;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/di;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dl;->b:Lcom/google/android/apps/youtube/api/jar/a/di;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dl;->b:Lcom/google/android/apps/youtube/api/jar/a/di;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/di;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dl;->b:Lcom/google/android/apps/youtube/api/jar/a/di;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dl;->b:Lcom/google/android/apps/youtube/api/jar/a/di;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/jar/a/di;->a(Ljava/lang/String;Ljava/util/List;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dl;->b:Lcom/google/android/apps/youtube/api/jar/a/di;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dl;->b:Lcom/google/android/apps/youtube/api/jar/a/di;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/di;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setListener(Lcom/google/android/apps/youtube/core/player/overlay/bn;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dl;->a:Lcom/google/android/apps/youtube/api/b/a/dm;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/dm;->a(Lcom/google/android/apps/youtube/core/player/overlay/bn;)V

    return-void
.end method

.method public final setVisible(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dl;->b:Lcom/google/android/apps/youtube/api/jar/a/di;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dl;->b:Lcom/google/android/apps/youtube/api/jar/a/di;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/di;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
