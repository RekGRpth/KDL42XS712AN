.class final Lcom/google/android/apps/youtube/api/b/a/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/api/p;


# instance fields
.field private a:Lcom/google/android/apps/youtube/api/jar/a/ck;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/api/jar/a/ck;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ck;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ck;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/ck;->a(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-virtual {p1}, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/api/jar/a/ck;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;IIZZ)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/youtube/api/jar/a/ck;->a(Ljava/lang/String;Ljava/lang/String;IIZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ck;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(ZI)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/ck;->a(ZI)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ck;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ck;->b(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/ck;->b(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ck;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ck;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ck;->e()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ck;->f()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ck;->g()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ck;->h()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ck;->i()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final j()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ck;->j()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final k()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ck;->k()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final l()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/ae;->a:Lcom/google/android/apps/youtube/api/jar/a/ck;

    return-void
.end method
