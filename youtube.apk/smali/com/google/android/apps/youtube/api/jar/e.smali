.class final Lcom/google/android/apps/youtube/api/jar/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/au;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/api/jar/e;-><init>(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->b(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->f(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setScrubbing(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;->setScrubbing(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ai;->c()V

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->f(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setScrubberTime(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;->setScrubberTime(I)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ai;->b()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PAUSED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ai;->a()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->ENDED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ai;->m()V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->f(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setScrubberTime(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;->setScrubberTime(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->f(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setScrubbing(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;->setScrubbing(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/ai;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ai;->a(I)V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PAUSED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ai;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->ENDED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ai;->m()V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ai;->b()V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->i(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/e;->a:Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ai;->g()V

    :cond_0
    return-void
.end method
