.class public abstract Lcom/google/android/apps/youtube/api/jar/ApiVideoView;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 5

    iget v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->a:I

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->getDefaultSize(II)I

    move-result v1

    iget v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->b:I

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->getDefaultSize(II)I

    move-result v0

    iget v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->a:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->b:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->a:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->b:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    int-to-float v3, v1

    int-to-float v4, v0

    div-float/2addr v3, v4

    div-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    const v3, 0x3c23d70a    # 0.01f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->b:I

    mul-int/2addr v0, v1

    iget v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->a:I

    div-int/2addr v0, v2

    :cond_0
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->setMeasuredDimension(II)V

    return-void

    :cond_1
    const v3, -0x43dc28f6    # -0.01f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->a:I

    mul-int/2addr v1, v0

    iget v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->b:I

    div-int/2addr v1, v2

    goto :goto_0
.end method

.method public final setVideoSize(II)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->a:I

    iput p2, p0, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->b:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->requestLayout()V

    return-void
.end method
