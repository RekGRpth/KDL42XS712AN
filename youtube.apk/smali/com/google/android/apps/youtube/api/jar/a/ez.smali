.class public final Lcom/google/android/apps/youtube/api/jar/a/ez;
.super Lcom/google/android/apps/youtube/api/jar/a/dj;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/overlay/bm;

.field private final b:Landroid/os/Handler;

.field private c:Lcom/google/android/apps/youtube/api/jar/a/fg;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/bm;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/dj;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/bm;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ez;->a:Lcom/google/android/apps/youtube/core/player/overlay/bm;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ez;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/ez;)Lcom/google/android/apps/youtube/api/jar/a/fg;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ez;->c:Lcom/google/android/apps/youtube/api/jar/a/fg;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/ez;Lcom/google/android/apps/youtube/api/jar/a/fg;)Lcom/google/android/apps/youtube/api/jar/a/fg;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/a/ez;->c:Lcom/google/android/apps/youtube/api/jar/a/fg;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/jar/a/ez;)Lcom/google/android/apps/youtube/core/player/overlay/bm;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ez;->a:Lcom/google/android/apps/youtube/core/player/overlay/bm;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ez;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/fb;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/fb;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ez;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ez;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/fe;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/fe;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ez;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/api/b/a/bg;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ez;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/fa;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/fa;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ez;Lcom/google/android/apps/youtube/api/b/a/bg;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ez;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/fd;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/jar/a/fd;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ez;Ljava/lang/String;Ljava/util/List;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ez;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/fc;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/fc;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ez;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ez;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ff;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/ff;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ez;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
