.class public abstract Lcom/google/android/apps/youtube/api/b/a/am;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/api/b/a/al;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "com.google.android.apps.youtube.api.service.jar.IApiPlayerFactoryService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/apps/youtube/api/b/a/am;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/os/IBinder;)Lcom/google/android/apps/youtube/api/b/a/al;
    .locals 2

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.google.android.apps.youtube.api.service.jar.IApiPlayerFactoryService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/apps/youtube/api/b/a/al;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/apps/youtube/api/b/a/al;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/api/b/a/an;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/api/b/a/an;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 15

    sparse-switch p1, :sswitch_data_0

    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    :goto_0
    return v1

    :sswitch_0
    const-string v1, "com.google.android.apps.youtube.api.service.jar.IApiPlayerFactoryService"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    const/4 v1, 0x1

    goto :goto_0

    :sswitch_1
    const-string v1, "com.google.android.apps.youtube.api.service.jar.IApiPlayerFactoryService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/jar/a/cl;->a(Landroid/os/IBinder;)Lcom/google/android/apps/youtube/api/jar/a/ck;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/jar/a/cx;->a(Landroid/os/IBinder;)Lcom/google/android/apps/youtube/api/jar/a/cw;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/jar/a/dd;->a(Landroid/os/IBinder;)Lcom/google/android/apps/youtube/api/jar/a/dc;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/jar/a/dg;->a(Landroid/os/IBinder;)Lcom/google/android/apps/youtube/api/jar/a/df;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/jar/a/cu;->a(Landroid/os/IBinder;)Lcom/google/android/apps/youtube/api/jar/a/ct;

    move-result-object v6

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/jar/a/cf;->a(Landroid/os/IBinder;)Lcom/google/android/apps/youtube/api/jar/a/ce;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/jar/a/dj;->a(Landroid/os/IBinder;)Lcom/google/android/apps/youtube/api/jar/a/di;

    move-result-object v8

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/jar/a/ci;->a(Landroid/os/IBinder;)Lcom/google/android/apps/youtube/api/jar/a/ch;

    move-result-object v9

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/jar/a/co;->a(Landroid/os/IBinder;)Lcom/google/android/apps/youtube/api/jar/a/cn;

    move-result-object v10

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/jar/a/cr;->a(Landroid/os/IBinder;)Lcom/google/android/apps/youtube/api/jar/a/cq;

    move-result-object v11

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/jar/a/da;->a(Landroid/os/IBinder;)Lcom/google/android/apps/youtube/api/jar/a/cz;

    move-result-object v12

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/jar/a/dm;->a(Landroid/os/IBinder;)Lcom/google/android/apps/youtube/api/jar/a/dl;

    move-result-object v13

    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v14, 0x1

    :goto_1
    move-object v1, p0

    invoke-virtual/range {v1 .. v14}, Lcom/google/android/apps/youtube/api/b/a/am;->a(Lcom/google/android/apps/youtube/api/jar/a/ck;Lcom/google/android/apps/youtube/api/jar/a/cw;Lcom/google/android/apps/youtube/api/jar/a/dc;Lcom/google/android/apps/youtube/api/jar/a/df;Lcom/google/android/apps/youtube/api/jar/a/ct;Lcom/google/android/apps/youtube/api/jar/a/ce;Lcom/google/android/apps/youtube/api/jar/a/di;Lcom/google/android/apps/youtube/api/jar/a/ch;Lcom/google/android/apps/youtube/api/jar/a/cn;Lcom/google/android/apps/youtube/api/jar/a/cq;Lcom/google/android/apps/youtube/api/jar/a/cz;Lcom/google/android/apps/youtube/api/jar/a/dl;Z)Lcom/google/android/apps/youtube/api/b/a/ao;

    move-result-object v1

    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/google/android/apps/youtube/api/b/a/ao;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_2
    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_0
    const/4 v14, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
