.class public final Lcom/google/android/apps/youtube/api/jar/a/bb;
.super Lcom/google/android/apps/youtube/api/jar/a/co;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

.field private final b:Landroid/os/Handler;

.field private c:Lcom/google/android/apps/youtube/api/jar/a/cd;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/co;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->a:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/bb;)Lcom/google/android/apps/youtube/api/jar/a/cd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->c:Lcom/google/android/apps/youtube/api/jar/a/cd;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/bb;Lcom/google/android/apps/youtube/api/jar/a/cd;)Lcom/google/android/apps/youtube/api/jar/a/cd;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->c:Lcom/google/android/apps/youtube/api/jar/a/cd;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/jar/a/bb;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->a:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->c:Lcom/google/android/apps/youtube/api/jar/a/cd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->c:Lcom/google/android/apps/youtube/api/jar/a/cd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/a/cd;->o()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->c:Lcom/google/android/apps/youtube/api/jar/a/cd;

    :cond_0
    return-void
.end method

.method public final a(III)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bo;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/jar/a/bo;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;III)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/api/b/a/ar;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bc;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/bc;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;Lcom/google/android/apps/youtube/api/b/a/ar;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/youtube/api/jar/a/bn;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/youtube/api/jar/a/bn;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bk;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/bk;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bv;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/bv;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bw;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/bw;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/br;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/br;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;ILandroid/view/KeyEvent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bd;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/bd;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bx;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/bx;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(ILandroid/view/KeyEvent;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bs;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/bs;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;ILandroid/view/KeyEvent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/be;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/be;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/by;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/by;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bf;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/bf;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bz;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/bz;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bg;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/bg;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final e(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ca;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ca;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bh;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/bh;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final f(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/cb;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cb;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bi;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/bi;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final g(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/cc;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cc;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bj;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/bj;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final h(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bl;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/bl;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bp;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/bp;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final i(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bm;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/bm;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final j()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bq;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/bq;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final j(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bt;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/bt;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final k(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/bb;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/bu;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/bu;-><init>(Lcom/google/android/apps/youtube/api/jar/a/bb;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
