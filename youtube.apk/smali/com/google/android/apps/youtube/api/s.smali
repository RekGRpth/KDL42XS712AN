.class public final Lcom/google/android/apps/youtube/api/s;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lcom/google/android/apps/youtube/core/client/bc;

.field private final c:Lcom/google/android/apps/youtube/core/client/bj;

.field private final d:Lcom/google/android/apps/youtube/common/network/h;

.field private e:Lcom/google/android/apps/youtube/core/async/GDataRequest;

.field private f:I

.field private g:I

.field private h:Ljava/util/List;

.field private i:Lcom/google/android/apps/youtube/common/a/d;

.field private j:Lcom/google/android/apps/youtube/common/a/d;

.field private k:Lcom/google/android/apps/youtube/common/a/d;

.field private final l:Lcom/google/android/apps/youtube/api/t;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/api/t;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;)V
    .locals 2

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/apps/youtube/api/s;->f:I

    iput v0, p0, Lcom/google/android/apps/youtube/api/s;->g:I

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/t;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/s;->l:Lcom/google/android/apps/youtube/api/t;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/s;->a:Landroid/os/Handler;

    iput-object p2, p0, Lcom/google/android/apps/youtube/api/s;->b:Lcom/google/android/apps/youtube/core/client/bc;

    iput-object p3, p0, Lcom/google/android/apps/youtube/api/s;->c:Lcom/google/android/apps/youtube/core/client/bj;

    iput-object p4, p0, Lcom/google/android/apps/youtube/api/s;->d:Lcom/google/android/apps/youtube/common/network/h;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/s;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/api/s;->f:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/s;Lcom/google/android/apps/youtube/core/async/GDataRequest;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/s;->e:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/s;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/s;->h()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/s;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/api/s;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    .locals 4

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->hqThumbnailUri:Landroid/net/Uri;

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/s;->h()V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/api/v;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/api/v;-><init>(Lcom/google/android/apps/youtube/api/s;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/s;->k:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->c:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->hqThumbnailUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/s;->a:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/s;->k:Lcom/google/android/apps/youtube/common/a/d;

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bj;->b(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/s;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->h:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/api/s;)Lcom/google/android/apps/youtube/core/async/GDataRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->e:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/api/s;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/api/s;->g:I

    return v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/api/s;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/s;->g()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/api/s;)Lcom/google/android/apps/youtube/api/t;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->l:Lcom/google/android/apps/youtube/api/t;

    return-object v0
.end method

.method private f()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->j:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->j:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/api/s;->j:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->i:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->i:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/api/s;->i:Lcom/google/android/apps/youtube/common/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->k:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->k:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/api/s;->k:Lcom/google/android/apps/youtube/common/a/d;

    :cond_2
    iput-object v1, p0, Lcom/google/android/apps/youtube/api/s;->e:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/api/s;->g:I

    return-void
.end method

.method private g()V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->e:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    iget v1, p0, Lcom/google/android/apps/youtube/api/s;->g:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/api/s;->g:I

    iget v0, p0, Lcom/google/android/apps/youtube/api/s;->g:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/s;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/api/s;->g:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/api/s;->g:I

    iput v0, p0, Lcom/google/android/apps/youtube/api/s;->f:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->h:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/youtube/api/s;->g:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/api/s;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    :goto_1
    return-void

    :cond_0
    const v0, 0x7fffffff

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->e:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/apps/youtube/api/u;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/youtube/api/u;-><init>(Lcom/google/android/apps/youtube/api/s;B)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/s;->i:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->b:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->c()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/s;->e:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/s;->a:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/s;->i:Lcom/google/android/apps/youtube/common/a/d;

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/s;->h()V

    goto :goto_1
.end method

.method private h()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->d:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/player/YouTubeThumbnailLoader$ErrorReason;->NETWORK_ERROR:Lcom/google/android/youtube/player/YouTubeThumbnailLoader$ErrorReason;

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/s;->f()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/s;->l:Lcom/google/android/apps/youtube/api/t;

    invoke-virtual {v0}, Lcom/google/android/youtube/player/YouTubeThumbnailLoader$ErrorReason;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/s;->e()Z

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/s;->d()Z

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/youtube/api/t;->a(Ljava/lang/String;ZZ)V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/player/YouTubeThumbnailLoader$ErrorReason;->INTERNAL_ERROR:Lcom/google/android/youtube/player/YouTubeThumbnailLoader$ErrorReason;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/s;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->h:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v0, "due to no playlist being set."

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring call to next() on YouTubeThumbnailView "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/youtube/player/internal/b/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    return-void

    :cond_0
    const-string v0, "as already at the end of the playlist."

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/api/s;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/api/s;->g:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/s;->g()V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    const/4 v1, -0x1

    const-string v0, "videoId cannot be empty"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/s;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->b:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/s;->e:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/s;->h:Ljava/util/List;

    iput v1, p0, Lcom/google/android/apps/youtube/api/s;->f:I

    iput v1, p0, Lcom/google/android/apps/youtube/api/s;->g:I

    new-instance v0, Lcom/google/android/apps/youtube/api/w;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/api/w;-><init>(Lcom/google/android/apps/youtube/api/s;B)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/s;->j:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->b:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->b()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/s;->e:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/s;->a:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/s;->j:Lcom/google/android/apps/youtube/common/a/d;

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 1

    const-string v0, "playlistId cannot be null or empty"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/s;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->b:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->g(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/s;->e:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    add-int/lit8 v0, p2, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/api/s;->f:I

    add-int/lit8 v0, p2, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/api/s;->g:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/s;->h:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/s;->a()V

    return-void
.end method

.method public final b()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/s;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->h:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v0, "due to no playlist being set."

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring call to previous() on YouTubeThumbnailView "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/youtube/player/internal/b/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    return-void

    :cond_0
    const-string v0, "as already at the start of the playlist."

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/api/s;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/api/s;->g:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/s;->g()V

    goto :goto_1
.end method

.method public final c()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/s;->h:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v0, "Ignoring call to first() on YouTubeThumbnailView due to no playlist being set."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/youtube/player/internal/b/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iput v1, p0, Lcom/google/android/apps/youtube/api/s;->g:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/s;->g()V

    goto :goto_0
.end method

.method public final d()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/s;->h:Ljava/util/List;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/apps/youtube/api/s;->f:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/s;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-lt v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/s;->e:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/s;->h:Ljava/util/List;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/apps/youtube/api/s;->f:I

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
