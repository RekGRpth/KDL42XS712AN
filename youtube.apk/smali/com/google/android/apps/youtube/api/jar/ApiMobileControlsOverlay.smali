.class public final Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lcom/google/android/apps/youtube/api/jar/b;


# instance fields
.field private a:Lcom/google/android/apps/youtube/api/jar/ai;

.field private b:Lcom/google/android/apps/youtube/api/jar/c;

.field private final c:Lcom/google/android/apps/youtube/api/jar/aa;

.field private final d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

.field private final e:Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;

.field private final f:Landroid/widget/ProgressBar;

.field private final g:Landroid/widget/ImageButton;

.field private final h:Landroid/widget/ImageButton;

.field private final i:Landroid/widget/ImageButton;

.field private final j:Landroid/widget/ImageView;

.field private final k:Landroid/widget/TextView;

.field private final l:Lcom/google/android/apps/youtube/core/player/overlay/at;

.field private final m:Landroid/os/Handler;

.field private final n:Landroid/view/animation/Animation;

.field private final o:Lcom/google/android/apps/youtube/core/player/overlay/bb;

.field private p:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

.field private q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/aa;)V
    .locals 5

    const/4 v4, -0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/aa;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->c:Lcom/google/android/apps/youtube/api/jar/aa;

    new-instance v0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/f;

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/youtube/api/jar/f;-><init>(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;B)V

    new-instance v2, Lcom/google/android/apps/youtube/api/jar/g;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/youtube/api/jar/g;-><init>(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;B)V

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/r;Lcom/google/android/apps/youtube/api/jar/af;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    new-instance v0, Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->e:Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->e:Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->addView(Landroid/view/View;)V

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->NEW:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    new-instance v0, Landroid/widget/ProgressBar;

    invoke-direct {v0, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->f:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->f:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->f:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/ImageButton;

    invoke-direct {v0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/youtube/api/b;->i:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/youtube/api/f;->h:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/api/b;->l:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->k:Landroid/widget/TextView;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->k:Landroid/widget/TextView;

    sget v1, Lcom/google/android/youtube/api/b;->u:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinimumHeight(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->k:Landroid/widget/TextView;

    const/4 v1, -0x2

    invoke-virtual {p0, v0, v4, v1}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->addView(Landroid/view/View;II)V

    new-instance v0, Landroid/widget/ImageButton;

    invoke-direct {v0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/youtube/api/b;->g:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/youtube/api/f;->f:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/ImageButton;

    invoke-direct {v0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->i:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->i:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/youtube/api/b;->j:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->i:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/youtube/api/f;->j:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->i:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->m:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/at;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/e;

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/youtube/api/jar/e;-><init>(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;B)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/at;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/au;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->l:Lcom/google/android/apps/youtube/core/player/overlay/at;

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->n:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->n:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->n:Landroid/view/animation/Animation;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/bb;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/bb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/bb;

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->YOUTUBE:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->f()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a:Lcom/google/android/apps/youtube/api/jar/ai;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->n:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method private static a(Landroid/view/View;II)V
    .locals 4

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int v0, p1, v0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int v1, p2, v1

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    return-void
.end method

.method private static a(Landroid/view/View;Z)V
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->z:Z

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->n:Landroid/view/animation/Animation;

    if-eqz p1, :cond_1

    const-wide/16 v0, 0x64

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->u:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->i:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;)V

    return-void

    :cond_1
    const-wide/16 v0, 0x1f4

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->v:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->i()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->b:Lcom/google/android/apps/youtube/api/jar/c;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/ControlsBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->e:Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    return-object v0
.end method

.method private h()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->LOADING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->y:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->m:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->m:Landroid/os/Handler;

    const-wide/16 v1, 0x9c4

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_1
    return-void
.end method

.method private i()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->z:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->m:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->n:Landroid/view/animation/Animation;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->u:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->clearAnimation()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->i:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->n:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    return-void
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->r:Z

    return v0
.end method

.method private j()V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PAUSED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v3, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    sget v3, Lcom/google/android/youtube/api/b;->i:I

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/youtube/api/f;->h:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->v:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->getTop()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getBottom()I

    move-result v3

    if-ge v0, v3, :cond_7

    move v0, v1

    :goto_1
    iget-boolean v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->y:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isError()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->NEW:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v3, v4, :cond_d

    :cond_0
    move v3, v2

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->getChildCount()I

    move-result v4

    if-ge v3, v4, :cond_9

    invoke-virtual {p0, v3}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->f:Landroid/widget/ProgressBar;

    if-ne v5, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v6, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->LOADING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-eq v4, v6, :cond_4

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j:Landroid/widget/ImageView;

    if-eq v5, v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->k:Landroid/widget/TextView;

    if-ne v5, v4, :cond_3

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isError()Z

    move-result v4

    if-nez v4, :cond_4

    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->e:Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;

    if-ne v5, v4, :cond_8

    iget-boolean v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->u:Z

    if-eqz v4, :cond_8

    :cond_4
    move v4, v1

    :goto_3
    invoke-static {v5, v4}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;Z)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v3, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    sget v3, Lcom/google/android/youtube/api/b;->h:I

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/youtube/api/f;->g:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    sget v3, Lcom/google/android/youtube/api/b;->k:I

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/youtube/api/f;->k:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_1

    :cond_8
    move v4, v2

    goto :goto_3

    :cond_9
    iget-boolean v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->u:Z

    if-eqz v3, :cond_a

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->x:Z

    if-eqz v3, :cond_b

    :cond_a
    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->LOADING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-eq v3, v4, :cond_b

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isError()Z

    move-result v3

    if-eqz v3, :cond_c

    :cond_b
    move v3, v1

    :goto_4
    invoke-static {p0, v3}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;Z)V

    :goto_5
    iget-boolean v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->y:Z

    if-nez v3, :cond_14

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v4, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->LOADING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-eq v3, v4, :cond_14

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->p:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-boolean v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->supportsNextPrevious:Z

    if-eqz v3, :cond_14

    if-nez v0, :cond_14

    move v0, v1

    :goto_6
    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h:Landroid/widget/ImageButton;

    if-eqz v0, :cond_15

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->s:Z

    if-eqz v3, :cond_15

    move v3, v1

    :goto_7
    invoke-static {v4, v3}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->i:Landroid/widget/ImageButton;

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->t:Z

    if-eqz v0, :cond_16

    :goto_8
    invoke-static {v3, v1}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;Z)V

    return-void

    :cond_c
    move v3, v2

    goto :goto_4

    :cond_d
    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isError()Z

    move-result v3

    if-eqz v3, :cond_e

    if-nez v0, :cond_e

    move v3, v1

    :goto_9
    invoke-static {v4, v3}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;Z)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->k:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isError()Z

    move-result v3

    if-eqz v3, :cond_f

    if-nez v0, :cond_f

    move v3, v1

    :goto_a
    invoke-static {v4, v3}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;Z)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->f:Landroid/widget/ProgressBar;

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v5, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->LOADING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v3, v5, :cond_10

    if-nez v0, :cond_10

    move v3, v1

    :goto_b
    invoke-static {v4, v3}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;Z)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->isError()Z

    move-result v3

    if-nez v3, :cond_11

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v5, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->LOADING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-eq v3, v5, :cond_11

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->p:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-boolean v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->supportsPlayHQCC:Z

    if-eqz v3, :cond_11

    if-nez v0, :cond_11

    move v3, v1

    :goto_c
    invoke-static {v4, v3}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;Z)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->u:Z

    if-nez v3, :cond_12

    move v3, v1

    :goto_d
    invoke-static {v4, v3}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;Z)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->e:Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->p:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-boolean v3, v3, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->supportsTimeBar:Z

    if-eqz v3, :cond_13

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->u:Z

    if-eqz v3, :cond_13

    move v3, v1

    :goto_e
    invoke-static {v4, v3}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;Z)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->setVisibility(I)V

    goto/16 :goto_5

    :cond_e
    move v3, v2

    goto :goto_9

    :cond_f
    move v3, v2

    goto :goto_a

    :cond_10
    move v3, v2

    goto :goto_b

    :cond_11
    move v3, v2

    goto :goto_c

    :cond_12
    move v3, v2

    goto :goto_d

    :cond_13
    move v3, v2

    goto :goto_e

    :cond_14
    move v0, v2

    goto/16 :goto_6

    :cond_15
    move v3, v2

    goto/16 :goto_7

    :cond_16
    move v1, v2

    goto/16 :goto_8
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->c()I

    move-result v0

    return v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/bb;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/d;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/d;-><init>(Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;)V

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/core/player/overlay/bb;->a(Ljava/util/List;Lcom/google/android/apps/youtube/core/player/overlay/bd;)V

    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final c()Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 3

    const/4 v2, -0x1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public final d()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v0, v1, v1, v1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setTimes(III)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->e:Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;

    invoke-virtual {v0, v1, v1, v1}, Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;->setTimes(III)V

    return-void
.end method

.method public final e()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->x:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->y:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->i()V

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->y:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j()V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->setFocusable(Z)V

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a:Lcom/google/android/apps/youtube/api/jar/ai;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a:Lcom/google/android/apps/youtube/api/jar/ai;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ai;->j()V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h()V

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->i()V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->y:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->y:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->a()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j()V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->setFocusable(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->requestFocus()Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a:Lcom/google/android/apps/youtube/api/jar/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a:Lcom/google/android/apps/youtube/api/jar/ai;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ai;->k()V

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->o:Lcom/google/android/apps/youtube/core/player/overlay/bb;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/bb;->a()V

    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v0, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Z)V

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->z:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->f()V

    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a:Lcom/google/android/apps/youtube/api/jar/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a:Lcom/google/android/apps/youtube/api/jar/ai;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ai;->d()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->i:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a:Lcom/google/android/apps/youtube/api/jar/ai;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ai;->e()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->ENDED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a:Lcom/google/android/apps/youtube/api/jar/ai;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ai;->m()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a:Lcom/google/android/apps/youtube/api/jar/ai;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ai;->b()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PAUSED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a:Lcom/google/android/apps/youtube/api/jar/ai;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ai;->a()V

    goto :goto_0
.end method

.method public final onFilterTouchEventForSecurity(Landroid/view/MotionEvent;)Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a:Lcom/google/android/apps/youtube/api/jar/ai;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getFlags()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/api/jar/ai;->a(Z)V

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onFilterTouchEventForSecurity(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/player/overlay/at;->b(I)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->e()V

    :cond_1
    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v3, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->RECOVERABLE_ERROR:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v2, v3, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a:Lcom/google/android/apps/youtube/api/jar/ai;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/api/jar/ai;->l()V

    :cond_2
    :goto_1
    return v0

    :cond_3
    move v2, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->l:Lcom/google/android/apps/youtube/core/player/overlay/at;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/apps/youtube/core/player/overlay/at;->a(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_1
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->l:Lcom/google/android/apps/youtube/core/player/overlay/at;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/at;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final onLayout(ZIIII)V
    .locals 7

    sub-int v0, p5, p3

    sub-int v1, p4, p2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->getPaddingBottom()I

    move-result v3

    sub-int v3, v0, v3

    div-int/lit8 v1, v1, 0x2

    div-int/lit8 v4, v0, 0x2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->e:Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;

    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v3, v5

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v2

    invoke-virtual {v0, v2, v5, v6, v3}, Landroid/view/View;->layout(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j:Landroid/widget/ImageView;

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->f:Landroid/widget/ProgressBar;

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->i:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getLeft()I

    move-result v1

    iget-object v5, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->i:Landroid/widget/ImageButton;

    invoke-virtual {v5}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v1, v5

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getRight()I

    move-result v1

    iget-object v5, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {v5}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v1, v5

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->k:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v3, v1

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->k:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {v0, v2, v1, v4, v3}, Landroid/widget/TextView;->layout(IIII)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    goto :goto_0
.end method

.method protected final onMeasure(II)V
    .locals 4

    const/4 v1, 0x0

    invoke-static {v1, p1}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->getDefaultSize(II)I

    move-result v0

    invoke-static {v1, p2}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->getDefaultSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->setMeasuredDimension(II)V

    mul-int/lit8 v0, v0, 0xa

    div-int/lit8 v0, v0, 0x64

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->k:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->k:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->k:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v3

    invoke-virtual {v1, v0, v2, v0, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->f:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->measureChild(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->g:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->measureChild(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->measureChild(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->k:Landroid/widget/TextView;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->measureChild(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->measureChild(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->i:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->measureChild(Landroid/view/View;II)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->e:Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;

    :goto_0
    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->measureChild(Landroid/view/View;II)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v3

    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->y:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->u:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->e:Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;

    :goto_1
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->u:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->v:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->getBottom()I

    move-result v1

    iget-object v6, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->b()I

    move-result v6

    sub-int/2addr v1, v6

    :goto_2
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v6

    int-to-float v6, v6

    cmpg-float v6, v6, v4

    if-gtz v6, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v6

    int-to-float v6, v6

    cmpg-float v4, v4, v6

    if-gtz v4, :cond_3

    int-to-float v1, v1

    cmpg-float v1, v1, v5

    if-gtz v1, :cond_3

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v5, v0

    if-gtz v0, :cond_3

    move v0, v3

    :goto_3
    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->z:Z

    if-eqz v0, :cond_4

    :cond_0
    move v0, v3

    :goto_4
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->w:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->e()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->RECOVERABLE_ERROR:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a:Lcom/google/android/apps/youtube/api/jar/ai;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ai;->l()V

    :cond_5
    :goto_5
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->w:Z

    goto :goto_0

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->y:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->w:Z

    if-nez v0, :cond_5

    invoke-direct {p0, v3}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a(Z)V

    goto :goto_5

    :pswitch_3
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->w:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final setAndShowEnded()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->ENDED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->ENDED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->e()V

    goto :goto_0
.end method

.method public final setAndShowPaused()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PAUSED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PAUSED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->e()V

    goto :goto_0
.end method

.method public final setAudioOnlyEnabled(Z)V
    .locals 0

    return-void
.end method

.method public final setCcEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setCcEnabled(Z)V

    return-void
.end method

.method public final setControlsPermanentlyHidden(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->x:Z

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->f()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PAUSED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->ENDED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->e()V

    goto :goto_0
.end method

.method public final setErrorAndShowMessage(IZ)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->setErrorAndShowMessage(Ljava/lang/String;Z)V

    return-void
.end method

.method public final setErrorAndShowMessage(Ljava/lang/String;Z)V
    .locals 2

    if-eqz p2, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->RECOVERABLE_ERROR:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j:Landroid/widget/ImageView;

    if-eqz p2, :cond_1

    sget v0, Lcom/google/android/youtube/api/b;->k:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->e()V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->UNRECOVERABLE_ERROR:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    goto :goto_0

    :cond_1
    sget v0, Lcom/google/android/youtube/api/b;->l:I

    goto :goto_1
.end method

.method public final setFullscreen(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setFullscreen(Z)V

    return-void
.end method

.method public final setHQ(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setHQ(Z)V

    return-void
.end method

.method public final setHQisHD(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setHQisHD(Z)V

    return-void
.end method

.method public final setHasAudioOnly(Z)V
    .locals 0

    return-void
.end method

.method public final setHasCc(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->r:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setHasCc(Z)V

    return-void
.end method

.method public final setHasInfoCard(Z)V
    .locals 0

    return-void
.end method

.method public final setHasNext(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->s:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j()V

    return-void
.end method

.method public final setHasPrevious(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->t:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j()V

    return-void
.end method

.method public final setInitial()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->NEW:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->NEW:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j()V

    goto :goto_0
.end method

.method public final setLearnMoreEnabled(Z)V
    .locals 0

    return-void
.end method

.method public final setListener(Lcom/google/android/apps/youtube/core/player/overlay/p;)V
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/api/jar/ai;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->c:Lcom/google/android/apps/youtube/api/jar/aa;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/youtube/api/jar/ai;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/p;Lcom/google/android/apps/youtube/api/jar/aa;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a:Lcom/google/android/apps/youtube/api/jar/ai;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->a:Lcom/google/android/apps/youtube/api/jar/ai;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setControlsListener(Lcom/google/android/apps/youtube/core/player/overlay/p;)V

    return-void
.end method

.method public final setLoading()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->LOADING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->LOADING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h()V

    goto :goto_0
.end method

.method public final setMinimal(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->u:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j()V

    return-void
.end method

.method public final setOnPlayInYouTubeListener(Lcom/google/android/apps/youtube/api/jar/c;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->b:Lcom/google/android/apps/youtube/api/jar/c;

    return-void
.end method

.method public final setPlaying()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->q:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->j()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->h()V

    goto :goto_0
.end method

.method public final setScrubbingEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setScrubbingEnabled(Z)V

    return-void
.end method

.method public final setShowAudioOnly(Z)V
    .locals 0

    return-void
.end method

.method public final setShowFullscreen(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setShowFullscreenButton(Z)V

    return-void
.end method

.method public final setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->p:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->e:Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;->setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V

    return-void
.end method

.method public final setSupportsQualityToggle(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setSupportsQualityToggle(Z)V

    return-void
.end method

.method public final setTimes(III)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setTimes(III)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->e:Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/jar/MinimalTimeBar;->setTimes(III)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->l:Lcom/google/android/apps/youtube/core/player/overlay/at;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/player/overlay/at;->a(II)V

    return-void
.end method

.method public final setVideoTitle(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;->d:Lcom/google/android/apps/youtube/api/jar/ControlsBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setVideoTitle(Ljava/lang/String;)V

    return-void
.end method
