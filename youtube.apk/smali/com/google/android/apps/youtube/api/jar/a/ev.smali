.class public final Lcom/google/android/apps/youtube/api/jar/a/ev;
.super Lcom/google/android/apps/youtube/api/jar/a/dg;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/api/jar/a/ey;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/os/Handler;

.field private d:Lcom/google/android/apps/youtube/api/jar/a/ex;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/api/jar/a/ey;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/dg;-><init>()V

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/a/ey;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ev;->a:Lcom/google/android/apps/youtube/api/jar/a/ey;

    const-string v0, "context cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ev;->b:Landroid/content/Context;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ev;->c:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/ev;Lcom/google/android/apps/youtube/api/jar/a/ex;)Lcom/google/android/apps/youtube/api/jar/a/ex;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/a/ev;->d:Lcom/google/android/apps/youtube/api/jar/a/ex;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/ev;)Lcom/google/android/apps/youtube/api/jar/a/ey;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ev;->a:Lcom/google/android/apps/youtube/api/jar/a/ey;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/jar/a/ev;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ev;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/api/jar/a/ev;)Lcom/google/android/apps/youtube/api/jar/a/ex;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ev;->d:Lcom/google/android/apps/youtube/api/jar/a/ex;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ev;->a:Lcom/google/android/apps/youtube/api/jar/a/ey;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ey;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ev;->d:Lcom/google/android/apps/youtube/api/jar/a/ex;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ev;->d:Lcom/google/android/apps/youtube/api/jar/a/ex;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/a/ex;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ev;->d:Lcom/google/android/apps/youtube/api/jar/a/ex;

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/api/b/a/bd;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ev;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ew;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ew;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ev;Lcom/google/android/apps/youtube/api/b/a/bd;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
