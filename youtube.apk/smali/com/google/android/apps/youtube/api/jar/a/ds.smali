.class final Lcom/google/android/apps/youtube/api/jar/a/ds;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/al;


# instance fields
.field private a:Lcom/google/android/apps/youtube/api/b/a/au;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/api/b/a/au;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "service cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/b/a/au;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ds;->a:Lcom/google/android/apps/youtube/api/b/a/au;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ds;->a:Lcom/google/android/apps/youtube/api/b/a/au;

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ds;->a:Lcom/google/android/apps/youtube/api/b/a/au;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ds;->a:Lcom/google/android/apps/youtube/api/b/a/au;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/b/a/au;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
