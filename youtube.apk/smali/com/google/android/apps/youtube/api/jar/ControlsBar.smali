.class public Lcom/google/android/apps/youtube/api/jar/ControlsBar;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;

.field private final e:Landroid/view/View;

.field private final f:Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;

.field private final g:Landroid/widget/ImageButton;

.field private final h:Landroid/widget/ImageButton;

.field private i:Lcom/google/android/apps/youtube/core/player/overlay/p;

.field private j:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/r;Lcom/google/android/apps/youtube/api/jar/af;)V
    .locals 7

    const/4 v6, 0x0

    const/high16 v3, 0x3f000000    # 0.5f

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    const-string v0, "optionsViewListener cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x42480000    # 50.0f

    mul-float/2addr v2, v1

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->a:I

    const/high16 v2, 0x42340000    # 45.0f

    mul-float/2addr v2, v1

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->b:I

    const/high16 v2, 0x40e00000    # 7.0f

    mul-float/2addr v1, v2

    add-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->c:I

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/r;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->d:Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->d:Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->e:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->e:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/api/b;->u:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->e:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->addView(Landroid/view/View;)V

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;

    invoke-direct {v1, p1, p3}, Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/af;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->f:Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->f:Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->j:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->j:Landroid/widget/TextView;

    sget v2, Lcom/google/android/youtube/api/b;->o:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->j:Landroid/widget/TextView;

    const-string v2, "LIVE"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->j:Landroid/widget/TextView;

    const/high16 v2, 0x41800000    # 16.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->j:Landroid/widget/TextView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->j:Landroid/widget/TextView;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->j:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    sget-object v2, Landroid/view/View;->PRESSED_ENABLED_STATE_SET:[I

    sget v3, Lcom/google/android/youtube/api/b;->q:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    sget-object v2, Landroid/view/View;->ENABLED_STATE_SET:[I

    sget v3, Lcom/google/android/youtube/api/b;->p:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    new-instance v2, Landroid/widget/ImageButton;

    invoke-direct {v2, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->g:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->g:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->g:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->g:Landroid/widget/ImageButton;

    iget v2, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->c:I

    iget v3, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->c:I

    iget v4, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->c:I

    iget v5, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->c:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/ImageButton;->setPadding(IIII)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->g:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->g:Landroid/widget/ImageButton;

    sget v2, Lcom/google/android/youtube/api/f;->l:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->g:Landroid/widget/ImageButton;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    sget-object v2, Landroid/view/View;->PRESSED_ENABLED_SELECTED_STATE_SET:[I

    sget v3, Lcom/google/android/youtube/api/b;->s:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    sget-object v2, Landroid/view/View;->ENABLED_SELECTED_STATE_SET:[I

    sget v3, Lcom/google/android/youtube/api/b;->r:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    sget-object v2, Landroid/view/View;->PRESSED_ENABLED_STATE_SET:[I

    sget v3, Lcom/google/android/youtube/api/b;->n:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    sget-object v2, Landroid/view/View;->ENABLED_STATE_SET:[I

    sget v3, Lcom/google/android/youtube/api/b;->m:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    new-instance v0, Landroid/widget/ImageButton;

    invoke-direct {v0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->h:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->g:Landroid/widget/ImageButton;

    iget v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->c:I

    iget v2, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->c:I

    iget v3, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->c:I

    iget v4, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->c:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageButton;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->h:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/youtube/api/f;->b:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->addView(Landroid/view/View;)V

    return-void
.end method

.method private a(Landroid/view/View;I)I
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    div-int/lit8 v2, v0, 0x2

    add-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, p2

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->e:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v3, v0

    invoke-virtual {p1, p2, v1, v2, v0}, Landroid/view/View;->layout(IIII)V

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->d:Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;->setVisibility(I)V

    return-void
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->a:I

    return v0
.end method

.method public final c()I
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->a:I

    iget v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->b:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x2

    return v0
.end method

.method public gatherTransparentRegion(Landroid/graphics/Region;)Z
    .locals 7

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v0, 0x2

    new-array v0, v0, [I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->getLocationInWindow([I)V

    aget v1, v0, v3

    aget v2, v0, v6

    aget v3, v0, v3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->getRight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->getLeft()I

    move-result v4

    sub-int/2addr v3, v4

    aget v0, v0, v6

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->getBottom()I

    move-result v4

    add-int/2addr v0, v4

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->getTop()I

    move-result v4

    sub-int v4, v0, v4

    sget-object v5, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Region;->op(IIIILandroid/graphics/Region$Op;)Z

    return v6
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->i:Lcom/google/android/apps/youtube/core/player/overlay/p;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "listener not set for ControlsOverlay"

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->h:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setFullscreen(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->i:Lcom/google/android/apps/youtube/core/player/overlay/p;

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/p;->b(Z)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->g:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->d:Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;->a()V

    goto :goto_2
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x0

    sub-int v0, p4, p2

    sub-int v1, p5, p3

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->e:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->e:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v1, v3

    invoke-virtual {v2, v4, v3, v0, v1}, Landroid/view/View;->layout(IIII)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->d:Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->d:Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v1, v3

    invoke-virtual {v2, v4, v3, v0, v1}, Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;->layout(IIII)V

    iget v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->c:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->f:Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;->getVisibility()I

    move-result v1

    if-eq v1, v5, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->f:Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->a(Landroid/view/View;I)I

    move-result v1

    add-int/2addr v0, v1

    :goto_0
    iget v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->c:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->g:Landroid/widget/ImageButton;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->a(Landroid/view/View;I)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v1

    if-eq v1, v5, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->h:Landroid/widget/ImageButton;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->a(Landroid/view/View;I)I

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->j:Landroid/widget/TextView;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->a(Landroid/view/View;I)I

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->f:Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 6

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v0, 0x0

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->getDefaultSize(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->c()I

    move-result v1

    invoke-static {v1, p2}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->setMeasuredDimension(II)V

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->d:Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;

    iget v3, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->b:I

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v1, v3}, Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;->measure(II)V

    iget v2, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->a:I

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->a:I

    const/high16 v4, -0x80000000

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->e:Landroid/view/View;

    invoke-virtual {v4, v1, v2}, Landroid/view/View;->measure(II)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->g:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3, v3}, Landroid/widget/ImageButton;->measure(II)V

    iget v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->c:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->g:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3, v3}, Landroid/widget/ImageButton;->measure(II)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    :cond_0
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->f:Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v1, v0, v3}, Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;->measure(II)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v3}, Landroid/widget/TextView;->measure(II)V

    return-void
.end method

.method public setCcEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->d:Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;->setCcEnabled(Z)V

    return-void
.end method

.method public setControlsListener(Lcom/google/android/apps/youtube/core/player/overlay/p;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->i:Lcom/google/android/apps/youtube/core/player/overlay/p;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->d:Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;->setControlsListener(Lcom/google/android/apps/youtube/core/player/overlay/p;)V

    return-void
.end method

.method public setFullscreen(Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->h:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/youtube/api/f;->c:I

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    sget v0, Lcom/google/android/youtube/api/f;->b:I

    goto :goto_0
.end method

.method public setHQ(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->d:Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;->setHq(Z)V

    return-void
.end method

.method public setHQisHD(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->d:Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;->setHQisHD(Z)V

    return-void
.end method

.method public setHasCc(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->d:Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;->setHasCc(Z)V

    return-void
.end method

.method public setScrubberTime(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->f:Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;->setScrubberTime(I)V

    return-void
.end method

.method public setScrubbing(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->f:Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;->setScrubbing(Z)V

    return-void
.end method

.method public setScrubbingEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->f:Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;->setEnabled(Z)V

    return-void
.end method

.method public setShowFullscreenButton(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->h:Landroid/widget/ImageButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->LIVE:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->f:Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->f:Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;->setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->f:Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSupportsQualityToggle(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->d:Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;->setSupportsQualityToggle(Z)V

    return-void
.end method

.method public setTimes(III)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->f:Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/jar/NormalTimeBar;->setTimes(III)V

    return-void
.end method

.method public setVideoTitle(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ControlsBar;->d:Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/ControlsOptionsView;->setVideoTitle(Ljava/lang/String;)V

    return-void
.end method
