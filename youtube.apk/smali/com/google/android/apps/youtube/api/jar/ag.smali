.class public abstract Lcom/google/android/apps/youtube/api/jar/ag;
.super Landroid/graphics/drawable/Drawable;
.source "SourceFile"


# static fields
.field public static final a:[I

.field public static final b:[I

.field public static final c:[I

.field protected static final d:I

.field protected static final e:I

.field protected static final f:I

.field protected static final g:I

.field protected static final h:I


# instance fields
.field protected i:Landroid/graphics/Shader;

.field protected j:Landroid/graphics/Shader;

.field protected k:Landroid/graphics/Shader;

.field protected l:I

.field protected m:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v4, 0x7f

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [I

    aput v2, v0, v2

    sput-object v0, Lcom/google/android/apps/youtube/api/jar/ag;->a:[I

    new-array v0, v3, [I

    aput v3, v0, v2

    sput-object v0, Lcom/google/android/apps/youtube/api/jar/ag;->b:[I

    new-array v0, v3, [I

    const/4 v1, 0x2

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/apps/youtube/api/jar/ag;->c:[I

    const/16 v0, 0xda

    const/16 v1, 0xe

    invoke-static {v0, v2, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/google/android/apps/youtube/api/jar/ag;->d:I

    const/16 v0, 0x82

    const/16 v1, 0xa

    invoke-static {v0, v3, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/google/android/apps/youtube/api/jar/ag;->e:I

    const/16 v0, 0xff

    const/16 v1, 0xcc

    const/16 v2, 0x30

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/google/android/apps/youtube/api/jar/ag;->f:I

    const/16 v0, 0xb9

    const/16 v1, 0x94

    const/16 v2, 0x22

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/google/android/apps/youtube/api/jar/ag;->g:I

    invoke-static {v4, v4, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/google/android/apps/youtube/api/jar/ag;->h:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected final a(Landroid/graphics/Rect;[I)V
    .locals 8

    const/4 v1, 0x0

    new-instance v0, Landroid/graphics/LinearGradient;

    iget v2, p1, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v3

    sget v5, Lcom/google/android/apps/youtube/api/jar/ag;->d:I

    sget v6, Lcom/google/android/apps/youtube/api/jar/ag;->e:I

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v3, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ag;->i:Landroid/graphics/Shader;

    new-instance v0, Landroid/graphics/LinearGradient;

    iget v2, p1, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v3

    sget v5, Lcom/google/android/apps/youtube/api/jar/ag;->f:I

    sget v6, Lcom/google/android/apps/youtube/api/jar/ag;->g:I

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v3, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ag;->j:Landroid/graphics/Shader;

    sget-object v0, Lcom/google/android/apps/youtube/api/jar/ag;->b:[I

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ag;->j:Landroid/graphics/Shader;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ag;->k:Landroid/graphics/Shader;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/ag;->i:Landroid/graphics/Shader;

    goto :goto_0
.end method

.method public final getOpacity()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method protected onLevelChange(I)Z
    .locals 3

    const/16 v2, 0x64

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ag;->getState()[I

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/api/jar/ag;->c:[I

    if-ne v0, v1, :cond_0

    iput v2, p0, Lcom/google/android/apps/youtube/api/jar/ag;->m:I

    iput v2, p0, Lcom/google/android/apps/youtube/api/jar/ag;->l:I

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ag;->a()V

    const/4 v0, 0x1

    return v0

    :cond_0
    div-int/lit16 v0, p1, 0x3e8

    iput v0, p0, Lcom/google/android/apps/youtube/api/jar/ag;->m:I

    rem-int/lit16 v0, p1, 0x3e8

    iput v0, p0, Lcom/google/android/apps/youtube/api/jar/ag;->l:I

    goto :goto_0
.end method

.method protected final onStateChange([I)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/ag;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/youtube/api/jar/ag;->a(Landroid/graphics/Rect;[I)V

    const/4 v0, 0x1

    return v0
.end method

.method public final setAlpha(I)V
    .locals 0

    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    return-void
.end method
