.class final Lcom/google/android/apps/youtube/api/b/a/cs;
.super Lcom/google/android/apps/youtube/api/b/a/ay;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/an;


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Lcom/google/android/apps/youtube/core/player/an;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/b/a/ay;-><init>()V

    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cs;->a:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/b/a/cs;)Lcom/google/android/apps/youtube/core/player/an;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cs;->b:Lcom/google/android/apps/youtube/core/player/an;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/core/player/an;)V
    .locals 1

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/an;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cs;->b:Lcom/google/android/apps/youtube/core/player/an;

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cs;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/ct;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/ct;-><init>(Lcom/google/android/apps/youtube/api/b/a/cs;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
