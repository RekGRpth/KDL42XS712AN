.class final Lcom/google/android/apps/youtube/api/b/a/bk;
.super Lcom/google/android/apps/youtube/api/b/a/ag;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/b;


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Lcom/google/android/apps/youtube/core/player/overlay/b;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/b/a/ag;-><init>()V

    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bk;->a:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/b/a/bk;)Lcom/google/android/apps/youtube/core/player/overlay/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bk;->b:Lcom/google/android/apps/youtube/core/player/overlay/b;

    return-object v0
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bk;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/bn;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/api/b/a/bn;-><init>(Lcom/google/android/apps/youtube/api/b/a/bk;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/overlay/b;)V
    .locals 1

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bk;->b:Lcom/google/android/apps/youtube/core/player/overlay/b;

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bk;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/bl;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/bl;-><init>(Lcom/google/android/apps/youtube/api/b/a/bk;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bk;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/bm;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/bm;-><init>(Lcom/google/android/apps/youtube/api/b/a/bk;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bk;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/bo;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/bo;-><init>(Lcom/google/android/apps/youtube/api/b/a/bk;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
