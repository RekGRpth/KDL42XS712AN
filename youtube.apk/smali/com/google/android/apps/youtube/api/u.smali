.class final Lcom/google/android/apps/youtube/api/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/api/s;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/api/s;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/u;->a:Lcom/google/android/apps/youtube/api/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/api/s;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/api/u;-><init>(Lcom/google/android/apps/youtube/api/s;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    const-string v0, "Error loading DefaultThumbnailLoader"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/u;->a:Lcom/google/android/apps/youtube/api/s;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/u;->a:Lcom/google/android/apps/youtube/api/s;

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/s;->d(Lcom/google/android/apps/youtube/api/s;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/api/s;->a(Lcom/google/android/apps/youtube/api/s;I)I

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/u;->a:Lcom/google/android/apps/youtube/api/s;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/s;->a(Lcom/google/android/apps/youtube/api/s;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/u;->a:Lcom/google/android/apps/youtube/api/s;

    invoke-static {v2}, Lcom/google/android/apps/youtube/api/s;->b(Lcom/google/android/apps/youtube/api/s;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->nextUri:Landroid/net/Uri;

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/u;->a:Lcom/google/android/apps/youtube/api/s;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/api/s;->a(Lcom/google/android/apps/youtube/api/s;Lcom/google/android/apps/youtube/core/async/GDataRequest;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/u;->a:Lcom/google/android/apps/youtube/api/s;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/s;->d(Lcom/google/android/apps/youtube/api/s;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/u;->a:Lcom/google/android/apps/youtube/api/s;

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/s;->b(Lcom/google/android/apps/youtube/api/s;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/u;->a:Lcom/google/android/apps/youtube/api/s;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/u;->a:Lcom/google/android/apps/youtube/api/s;

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/s;->d(Lcom/google/android/apps/youtube/api/s;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/api/s;->a(Lcom/google/android/apps/youtube/api/s;I)I

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/u;->a:Lcom/google/android/apps/youtube/api/s;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/u;->a:Lcom/google/android/apps/youtube/api/s;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/s;->b(Lcom/google/android/apps/youtube/api/s;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/u;->a:Lcom/google/android/apps/youtube/api/s;

    invoke-static {v2}, Lcom/google/android/apps/youtube/api/s;->d(Lcom/google/android/apps/youtube/api/s;)I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/api/s;->a(Lcom/google/android/apps/youtube/api/s;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    :goto_2
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/u;->a:Lcom/google/android/apps/youtube/api/s;

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->nextUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/u;->a:Lcom/google/android/apps/youtube/api/s;

    invoke-static {v2}, Lcom/google/android/apps/youtube/api/s;->c(Lcom/google/android/apps/youtube/api/s;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/core/async/GDataRequest;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/api/s;->a(Lcom/google/android/apps/youtube/api/s;Lcom/google/android/apps/youtube/core/async/GDataRequest;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/u;->a:Lcom/google/android/apps/youtube/api/s;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/s;->e(Lcom/google/android/apps/youtube/api/s;)V

    goto :goto_2
.end method
