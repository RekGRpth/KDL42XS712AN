.class public final Lcom/google/android/apps/youtube/api/b/a/cu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/be;


# instance fields
.field private a:Lcom/google/android/apps/youtube/api/jar/a/cz;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/api/jar/a/cz;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "client cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/a/cz;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cu;->a:Lcom/google/android/apps/youtube/api/jar/a/cz;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cu;->a:Lcom/google/android/apps/youtube/api/jar/a/cz;

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cu;->a:Lcom/google/android/apps/youtube/api/jar/a/cz;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cu;->a:Lcom/google/android/apps/youtube/api/jar/a/cz;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cz;->a(Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cu;->a:Lcom/google/android/apps/youtube/api/jar/a/cz;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cu;->a:Lcom/google/android/apps/youtube/api/jar/a/cz;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/cz;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cu;->a:Lcom/google/android/apps/youtube/api/jar/a/cz;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cu;->a:Lcom/google/android/apps/youtube/api/jar/a/cz;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/cz;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setFontScale(F)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cu;->a:Lcom/google/android/apps/youtube/api/jar/a/cz;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cu;->a:Lcom/google/android/apps/youtube/api/jar/a/cz;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cz;->a(F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setSubtitlesStyle(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cu;->a:Lcom/google/android/apps/youtube/api/jar/a/cz;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cu;->a:Lcom/google/android/apps/youtube/api/jar/a/cz;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cz;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
