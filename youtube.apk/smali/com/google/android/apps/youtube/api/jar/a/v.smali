.class public final Lcom/google/android/apps/youtube/api/jar/a/v;
.super Lcom/google/android/apps/youtube/api/jar/a/cf;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/overlay/a;

.field private final b:Landroid/os/Handler;

.field private c:Lcom/google/android/apps/youtube/api/jar/a/ad;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/a;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/cf;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/v;->a:Lcom/google/android/apps/youtube/core/player/overlay/a;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/v;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/v;)Lcom/google/android/apps/youtube/api/jar/a/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/v;->c:Lcom/google/android/apps/youtube/api/jar/a/ad;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/v;Lcom/google/android/apps/youtube/api/jar/a/ad;)Lcom/google/android/apps/youtube/api/jar/a/ad;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/a/v;->c:Lcom/google/android/apps/youtube/api/jar/a/ad;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/jar/a/v;)Lcom/google/android/apps/youtube/core/player/overlay/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/v;->a:Lcom/google/android/apps/youtube/core/player/overlay/a;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/v;->c:Lcom/google/android/apps/youtube/api/jar/a/ad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/v;->c:Lcom/google/android/apps/youtube/api/jar/a/ad;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/a/ad;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/v;->c:Lcom/google/android/apps/youtube/api/jar/a/ad;

    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/v;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ab;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/ab;-><init>(Lcom/google/android/apps/youtube/api/jar/a/v;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/v;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ac;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ac;-><init>(Lcom/google/android/apps/youtube/api/jar/a/v;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/api/b/a/af;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/v;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/w;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/w;-><init>(Lcom/google/android/apps/youtube/api/jar/a/v;Lcom/google/android/apps/youtube/api/b/a/af;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/v;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/z;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/z;-><init>(Lcom/google/android/apps/youtube/api/jar/a/v;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 7

    iget-object v6, p0, Lcom/google/android/apps/youtube/api/jar/a/v;->b:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/youtube/api/jar/a/y;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/api/jar/a/y;-><init>(Lcom/google/android/apps/youtube/api/jar/a/v;Ljava/lang/String;ZZLjava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/v;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/aa;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/aa;-><init>(Lcom/google/android/apps/youtube/api/jar/a/v;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/v;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/x;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/x;-><init>(Lcom/google/android/apps/youtube/api/jar/a/v;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
