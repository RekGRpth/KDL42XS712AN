.class public final Lcom/google/android/apps/youtube/api/b/a/bp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/g;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/api/b/a/bq;

.field private b:Lcom/google/android/apps/youtube/api/jar/a/ch;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/apps/youtube/api/jar/a/ch;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "client cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/a/ch;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    new-instance v0, Lcom/google/android/apps/youtube/api/b/a/bq;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/bq;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->a:Lcom/google/android/apps/youtube/api/b/a/bq;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->a:Lcom/google/android/apps/youtube/api/b/a/bq;

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/api/jar/a/ch;->a(Lcom/google/android/apps/youtube/api/b/a/ai;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ch;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ch;->b(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ch;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ch;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ch;->e()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ch;->f()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ch;->g()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ch;->h()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final j()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ch;->i()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setAdStyle(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ch;->c(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setCallToActionImage(Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ch;->c(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setCallToActionText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/ch;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setFeaturedChannelImage(Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ch;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setFeaturedVideoImage(Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ch;->b(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setFeaturedVideoTitle(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ch;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setInfoCardTeaserImage(Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ch;->d(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setInfoCardTeaserMessage(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ch;->a(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setListener(Lcom/google/android/apps/youtube/core/player/overlay/h;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->a:Lcom/google/android/apps/youtube/api/b/a/bq;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/bq;->a(Lcom/google/android/apps/youtube/core/player/overlay/h;)V

    return-void
.end method

.method public final setVisible(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bp;->b:Lcom/google/android/apps/youtube/api/jar/a/ch;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ch;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
