.class public final Lcom/google/android/apps/youtube/api/b/a/cr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/am;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/api/b/a/cs;

.field private b:Lcom/google/android/apps/youtube/api/jar/a/cw;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/apps/youtube/api/jar/a/cw;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "client cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/a/cw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cr;->b:Lcom/google/android/apps/youtube/api/jar/a/cw;

    new-instance v0, Lcom/google/android/apps/youtube/api/b/a/cs;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/cs;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cr;->a:Lcom/google/android/apps/youtube/api/b/a/cs;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cr;->a:Lcom/google/android/apps/youtube/api/b/a/cs;

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/api/jar/a/cw;->a(Lcom/google/android/apps/youtube/api/b/a/ax;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cr;->b:Lcom/google/android/apps/youtube/api/jar/a/cw;

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cr;->b:Lcom/google/android/apps/youtube/api/jar/a/cw;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cr;->b:Lcom/google/android/apps/youtube/api/jar/a/cw;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cw;->b(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setKeepScreenOn(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cr;->b:Lcom/google/android/apps/youtube/api/jar/a/cw;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cr;->b:Lcom/google/android/apps/youtube/api/jar/a/cw;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cw;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setListener(Lcom/google/android/apps/youtube/core/player/an;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cr;->a:Lcom/google/android/apps/youtube/api/b/a/cs;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/cs;->a(Lcom/google/android/apps/youtube/core/player/an;)V

    return-void
.end method
