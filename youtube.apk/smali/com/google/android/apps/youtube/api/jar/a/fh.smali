.class public final Lcom/google/android/apps/youtube/api/jar/a/fh;
.super Lcom/google/android/apps/youtube/api/jar/a/dm;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/overlay/br;

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/br;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/dm;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/br;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/fh;->a:Lcom/google/android/apps/youtube/core/player/overlay/br;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/fh;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/fh;)Lcom/google/android/apps/youtube/core/player/overlay/br;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/fh;->a:Lcom/google/android/apps/youtube/core/player/overlay/br;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/fh;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/fi;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/fi;-><init>(Lcom/google/android/apps/youtube/api/jar/a/fh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/fh;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/fl;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/fl;-><init>(Lcom/google/android/apps/youtube/api/jar/a/fh;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/fh;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/fj;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/fj;-><init>(Lcom/google/android/apps/youtube/api/jar/a/fh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/fh;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/fk;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/fk;-><init>(Lcom/google/android/apps/youtube/api/jar/a/fh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
