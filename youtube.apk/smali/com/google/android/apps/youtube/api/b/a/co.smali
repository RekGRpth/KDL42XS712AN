.class public final Lcom/google/android/apps/youtube/api/b/a/co;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/ak;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/api/b/a/cp;

.field private b:Lcom/google/android/apps/youtube/api/jar/a/cq;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/apps/youtube/api/jar/a/cq;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "client cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/a/cq;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/co;->b:Lcom/google/android/apps/youtube/api/jar/a/cq;

    new-instance v0, Lcom/google/android/apps/youtube/api/b/a/cp;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/cp;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/co;->a:Lcom/google/android/apps/youtube/api/b/a/cp;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/co;->a:Lcom/google/android/apps/youtube/api/b/a/cp;

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/api/jar/a/cq;->a(Lcom/google/android/apps/youtube/api/b/a/au;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/co;->b:Lcom/google/android/apps/youtube/api/jar/a/cq;

    return-void
.end method

.method public final a(JZZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/co;->b:Lcom/google/android/apps/youtube/api/jar/a/cq;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/co;->b:Lcom/google/android/apps/youtube/api/jar/a/cq;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/api/jar/a/cq;->a(JZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/co;->b:Lcom/google/android/apps/youtube/api/jar/a/cq;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/co;->b:Lcom/google/android/apps/youtube/api/jar/a/cq;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/cq;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setListener(Lcom/google/android/apps/youtube/core/player/overlay/al;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/co;->a:Lcom/google/android/apps/youtube/api/b/a/cp;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/cp;->a(Lcom/google/android/apps/youtube/core/player/overlay/al;)V

    return-void
.end method
