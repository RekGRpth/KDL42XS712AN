.class final Lcom/google/android/apps/youtube/api/jar/a/fg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/bn;


# instance fields
.field private a:Lcom/google/android/apps/youtube/api/b/a/bg;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/api/b/a/bg;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "service cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/b/a/bg;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/fg;->a:Lcom/google/android/apps/youtube/api/b/a/bg;

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/fg;->a:Lcom/google/android/apps/youtube/api/b/a/bg;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/fg;->a:Lcom/google/android/apps/youtube/api/b/a/bg;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/api/b/a/bg;->a(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "survey call to onClickSkipButton failed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a([I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/fg;->a:Lcom/google/android/apps/youtube/api/b/a/bg;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/fg;->a:Lcom/google/android/apps/youtube/api/b/a/bg;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/bg;->a([I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "survey call to onAnswer failed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_0
.end method
