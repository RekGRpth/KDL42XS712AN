.class public final Lcom/google/android/apps/youtube/api/b/a/dq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/br;


# instance fields
.field private a:Lcom/google/android/apps/youtube/api/jar/a/dl;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/api/jar/a/dl;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "client cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/a/dl;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dq;->a:Lcom/google/android/apps/youtube/api/jar/a/dl;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dq;->a:Lcom/google/android/apps/youtube/api/jar/a/dl;

    return-void
.end method

.method public final b()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dq;->a:Lcom/google/android/apps/youtube/api/jar/a/dl;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dq;->a:Lcom/google/android/apps/youtube/api/jar/a/dl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/dl;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dq;->a:Lcom/google/android/apps/youtube/api/jar/a/dl;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dq;->a:Lcom/google/android/apps/youtube/api/jar/a/dl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/dl;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dq;->a:Lcom/google/android/apps/youtube/api/jar/a/dl;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dq;->a:Lcom/google/android/apps/youtube/api/jar/a/dl;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/dl;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setThumbnail(Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dq;->a:Lcom/google/android/apps/youtube/api/jar/a/dl;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dq;->a:Lcom/google/android/apps/youtube/api/jar/a/dl;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/dl;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
