.class public final Lcom/google/android/apps/youtube/api/b/a;
.super Lcom/google/android/youtube/player/internal/ac;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lcom/google/android/apps/youtube/api/b/h;

.field private final c:Lcom/google/android/apps/youtube/api/s;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/apps/youtube/api/j;Lcom/google/android/youtube/player/internal/y;)V
    .locals 5

    invoke-direct {p0}, Lcom/google/android/youtube/player/internal/ac;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a;->a:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/youtube/api/b/h;

    invoke-direct {v0, p0, p3}, Lcom/google/android/apps/youtube/api/b/h;-><init>(Lcom/google/android/apps/youtube/api/b/a;Lcom/google/android/youtube/player/internal/y;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a;->b:Lcom/google/android/apps/youtube/api/b/h;

    new-instance v0, Lcom/google/android/apps/youtube/api/s;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/b/a;->b:Lcom/google/android/apps/youtube/api/b/h;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/api/j;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/api/j;->h()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/api/j;->o()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/api/s;-><init>(Lcom/google/android/apps/youtube/api/t;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a;->c:Lcom/google/android/apps/youtube/api/s;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/b/a;)Lcom/google/android/apps/youtube/api/s;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a;->c:Lcom/google/android/apps/youtube/api/s;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/b/a;)Lcom/google/android/apps/youtube/api/b/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a;->b:Lcom/google/android/apps/youtube/api/b/h;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/d;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/d;-><init>(Lcom/google/android/apps/youtube/api/b/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/b;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/b/b;-><init>(Lcom/google/android/apps/youtube/api/b/a;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/c;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/api/b/c;-><init>(Lcom/google/android/apps/youtube/api/b/a;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/e;-><init>(Lcom/google/android/apps/youtube/api/b/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/f;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/f;-><init>(Lcom/google/android/apps/youtube/api/b/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/g;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/g;-><init>(Lcom/google/android/apps/youtube/api/b/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
