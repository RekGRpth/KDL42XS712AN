.class final Lcom/google/android/apps/youtube/api/b/a/dm;
.super Lcom/google/android/apps/youtube/api/b/a/bh;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Lcom/google/android/apps/youtube/core/player/overlay/bn;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/b/a/bh;-><init>()V

    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dm;->a:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/b/a/dm;)Lcom/google/android/apps/youtube/core/player/overlay/bn;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dm;->b:Lcom/google/android/apps/youtube/core/player/overlay/bn;

    return-object v0
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dm;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/dn;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/api/b/a/dn;-><init>(Lcom/google/android/apps/youtube/api/b/a/dm;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/overlay/bn;)V
    .locals 1

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/bn;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dm;->b:Lcom/google/android/apps/youtube/core/player/overlay/bn;

    return-void
.end method

.method public final a([I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dm;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/do;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/b/a/do;-><init>(Lcom/google/android/apps/youtube/api/b/a/dm;[I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
