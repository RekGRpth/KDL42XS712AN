.class public final Lcom/google/android/apps/youtube/api/jar/a/ej;
.super Lcom/google/android/apps/youtube/api/jar/a/dd;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/api/jar/a/et;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/os/Handler;

.field private d:Lcom/google/android/apps/youtube/api/jar/a/es;

.field private e:Landroid/view/SurfaceHolder;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/api/jar/a/et;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/dd;-><init>()V

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/a/et;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->a:Lcom/google/android/apps/youtube/api/jar/a/et;

    const-string v0, "context cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->b:Landroid/content/Context;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->c:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/ej;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->e:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/ej;Lcom/google/android/apps/youtube/api/jar/a/es;)Lcom/google/android/apps/youtube/api/jar/a/es;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->d:Lcom/google/android/apps/youtube/api/jar/a/es;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/ej;)Lcom/google/android/apps/youtube/api/jar/a/et;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->a:Lcom/google/android/apps/youtube/api/jar/a/et;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/jar/a/ej;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->b:Landroid/content/Context;

    return-object v0
.end method

.method public static b(Z)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eqz p0, :cond_2

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xd

    if-lt v2, v3, :cond_0

    const-string v2, "ka"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "eagle"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "asura"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-ge v2, v3, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/api/jar/a/ej;)Lcom/google/android/apps/youtube/api/jar/a/es;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->d:Lcom/google/android/apps/youtube/api/jar/a/es;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/api/jar/a/ej;)Landroid/view/SurfaceHolder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->e:Landroid/view/SurfaceHolder;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ep;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ep;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ej;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/eo;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/eo;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ej;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/api/b/a/ba;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ek;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ek;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ej;Lcom/google/android/apps/youtube/api/b/a/ba;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/el;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/el;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ej;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a()Z
    .locals 4

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    new-instance v1, Landroid/os/ConditionVariable;

    invoke-direct {v1}, Landroid/os/ConditionVariable;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->c:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/apps/youtube/api/jar/a/en;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/apps/youtube/api/jar/a/en;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ej;Ljava/util/concurrent/atomic/AtomicBoolean;Landroid/os/ConditionVariable;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public final b()Landroid/graphics/Rect;
    .locals 4

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    new-instance v1, Landroid/os/ConditionVariable;

    invoke-direct {v1}, Landroid/os/ConditionVariable;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->c:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/apps/youtube/api/jar/a/em;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/apps/youtube/api/jar/a/em;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ej;Ljava/util/concurrent/atomic/AtomicReference;Landroid/os/ConditionVariable;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {v1}, Landroid/os/ConditionVariable;->block()V

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    return-object v0
.end method

.method public final b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/er;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/er;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ej;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/eq;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/eq;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ej;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->a:Lcom/google/android/apps/youtube/api/jar/a/et;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/et;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->d:Lcom/google/android/apps/youtube/api/jar/a/es;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->d:Lcom/google/android/apps/youtube/api/jar/a/es;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/a/es;->a()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->d:Lcom/google/android/apps/youtube/api/jar/a/es;

    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/ej;->e:Landroid/view/SurfaceHolder;

    return-void
.end method
