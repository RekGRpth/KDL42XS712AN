.class public final Lcom/google/android/apps/youtube/api/jar/a/dy;
.super Lcom/google/android/apps/youtube/api/jar/a/cx;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/am;

.field private final b:Landroid/os/Handler;

.field private c:Lcom/google/android/apps/youtube/api/jar/a/ec;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/am;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/cx;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/am;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dy;->a:Lcom/google/android/apps/youtube/core/player/am;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dy;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/dy;)Lcom/google/android/apps/youtube/api/jar/a/ec;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dy;->c:Lcom/google/android/apps/youtube/api/jar/a/ec;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/dy;Lcom/google/android/apps/youtube/api/jar/a/ec;)Lcom/google/android/apps/youtube/api/jar/a/ec;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/a/dy;->c:Lcom/google/android/apps/youtube/api/jar/a/ec;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/jar/a/dy;)Lcom/google/android/apps/youtube/core/player/am;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dy;->a:Lcom/google/android/apps/youtube/core/player/am;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dy;->c:Lcom/google/android/apps/youtube/api/jar/a/ec;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dy;->c:Lcom/google/android/apps/youtube/api/jar/a/ec;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/a/ec;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dy;->c:Lcom/google/android/apps/youtube/api/jar/a/ec;

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/api/b/a/ax;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dy;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/dz;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/dz;-><init>(Lcom/google/android/apps/youtube/api/jar/a/dy;Lcom/google/android/apps/youtube/api/b/a/ax;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dy;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/ea;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ea;-><init>(Lcom/google/android/apps/youtube/api/jar/a/dy;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dy;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/eb;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/eb;-><init>(Lcom/google/android/apps/youtube/api/jar/a/dy;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
