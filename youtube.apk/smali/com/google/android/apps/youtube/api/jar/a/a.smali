.class public abstract Lcom/google/android/apps/youtube/api/jar/a/a;
.super Lcom/google/android/youtube/player/internal/h;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/os/Handler;

.field protected final b:Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

.field protected final c:Lcom/google/android/apps/youtube/api/jar/b;

.field protected final d:Lcom/google/android/apps/youtube/core/player/overlay/a;

.field protected final e:Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;

.field protected final f:Lcom/google/android/apps/youtube/core/player/overlay/g;

.field protected final g:Lcom/google/android/apps/youtube/core/player/overlay/ak;

.field protected final h:Lcom/google/android/apps/youtube/core/player/overlay/be;

.field protected final i:Lcom/google/android/apps/youtube/core/player/overlay/br;

.field private final j:Landroid/content/Context;

.field private final k:Lcom/google/android/apps/youtube/api/jar/a;

.field private final l:Lcom/google/android/apps/youtube/api/jar/w;

.field private final m:Lcom/google/android/apps/youtube/api/jar/u;

.field private final n:Lcom/google/android/apps/youtube/api/jar/aa;

.field private o:Lcom/google/android/youtube/player/internal/s;

.field private p:Lcom/google/android/youtube/player/internal/p;

.field private q:Lcom/google/android/youtube/player/internal/m;

.field private r:Lcom/google/android/youtube/player/internal/j;

.field private s:Ljava/lang/String;

.field private t:Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;

.field private u:Z

.field private v:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/a;Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/google/android/youtube/player/internal/h;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->j:Landroid/content/Context;

    const-string v0, "activityProxy cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->k:Lcom/google/android/apps/youtube/api/jar/a;

    const-string v0, "playerOverlaysLayout cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->b:Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->a:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/youtube/api/jar/aa;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/t;

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/youtube/api/jar/a/t;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;B)V

    invoke-direct {v0, p3, p2, v1}, Lcom/google/android/apps/youtube/api/jar/aa;-><init>(Landroid/view/View;Lcom/google/android/apps/youtube/api/jar/a;Lcom/google/android/apps/youtube/api/jar/ac;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->n:Lcom/google/android/apps/youtube/api/jar/aa;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->n:Lcom/google/android/apps/youtube/api/jar/aa;

    invoke-direct {v0, p1, p3, v1}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/google/android/apps/youtube/api/jar/aa;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->c:Lcom/google/android/apps/youtube/api/jar/b;

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->c:Lcom/google/android/apps/youtube/api/jar/b;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/u;

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/youtube/api/jar/a/u;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;B)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/api/jar/b;->setOnPlayInYouTubeListener(Lcom/google/android/apps/youtube/api/jar/c;)V

    new-instance v0, Lcom/google/android/apps/youtube/api/jar/a/s;

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/youtube/api/jar/a/s;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;B)V

    invoke-static {p1, p2, v0, p3}, Lcom/google/android/apps/youtube/api/jar/w;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/a;Lcom/google/android/apps/youtube/api/jar/z;Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;)Lcom/google/android/apps/youtube/api/jar/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->l:Lcom/google/android/apps/youtube/api/jar/w;

    new-instance v0, Lcom/google/android/apps/youtube/api/jar/u;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/r;

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/youtube/api/jar/a/r;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;B)V

    invoke-direct {v0, p1, v1, p3}, Lcom/google/android/apps/youtube/api/jar/u;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/v;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->m:Lcom/google/android/apps/youtube/api/jar/u;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/t;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/api/jar/t;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->c:Lcom/google/android/apps/youtube/api/jar/b;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/api/jar/b;->a()I

    move-result v2

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/Analytics;I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->d:Lcom/google/android/apps/youtube/core/player/overlay/a;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->e:Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAnnotationOverlay;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->f:Lcom/google/android/apps/youtube/core/player/overlay/g;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultLiveOverlay;

    sget v1, Lcom/google/android/youtube/api/b;->i:I

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultLiveOverlay;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->g:Lcom/google/android/apps/youtube/core/player/overlay/ak;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->h:Lcom/google/android/apps/youtube/core/player/overlay/be;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultThumbnailOverlay;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultThumbnailOverlay;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->i:Lcom/google/android/apps/youtube/core/player/overlay/br;

    sget-object v0, Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;->DEFAULT:Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->t:Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/player/overlay/ax;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->i:Lcom/google/android/apps/youtube/core/player/overlay/br;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->h:Lcom/google/android/apps/youtube/core/player/overlay/be;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->c:Lcom/google/android/apps/youtube/api/jar/b;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->f:Lcom/google/android/apps/youtube/core/player/overlay/g;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->d:Lcom/google/android/apps/youtube/core/player/overlay/a;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->e:Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->g:Lcom/google/android/apps/youtube/core/player/overlay/ak;

    aput-object v2, v0, v1

    invoke-virtual {p3, v0}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;->a([Lcom/google/android/apps/youtube/core/player/overlay/ax;)V

    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->n:Lcom/google/android/apps/youtube/api/jar/aa;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/youtube/api/jar/ApiMobileControlsOverlay;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/aa;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->c:Lcom/google/android/apps/youtube/api/jar/b;

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/a;)Lcom/google/android/youtube/player/internal/s;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->o:Lcom/google/android/youtube/player/internal/s;

    return-object v0
.end method

.method private a()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This YouTubePlayer has been released"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/a;Ljava/lang/String;Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->n()V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v0}, Lcom/google/android/youtube/player/internal/b/a;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0, p2}, Lcom/google/android/apps/youtube/api/jar/a/a;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/a;Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->L()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->u:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->n()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->m:Lcom/google/android/apps/youtube/api/jar/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/u;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->m:Lcom/google/android/apps/youtube/api/jar/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/u;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->m:Lcom/google/android/apps/youtube/api/jar/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/u;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->m:Lcom/google/android/apps/youtube/api/jar/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/u;->dismiss()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/jar/a/a;)Lcom/google/android/youtube/player/internal/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->p:Lcom/google/android/youtube/player/internal/p;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/api/jar/a/a;)Lcom/google/android/youtube/player/internal/m;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->q:Lcom/google/android/youtube/player/internal/m;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/api/jar/a/a;)Lcom/google/android/youtube/player/internal/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->r:Lcom/google/android/youtube/player/internal/j;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/api/jar/a/a;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->u:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/api/jar/a/a;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->j:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/api/jar/a/a;)Lcom/google/android/apps/youtube/api/jar/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->k:Lcom/google/android/apps/youtube/api/jar/a;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/api/jar/a/a;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->s:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected abstract A()V
.end method

.method protected abstract B()Z
.end method

.method protected abstract C()Z
.end method

.method protected abstract D()Z
.end method

.method protected abstract E()V
.end method

.method protected abstract F()V
.end method

.method protected abstract G()I
.end method

.method protected abstract H()I
.end method

.method protected abstract I()V
.end method

.method protected abstract J()V
.end method

.method protected abstract K()V
.end method

.method protected abstract L()Z
.end method

.method protected abstract M()V
.end method

.method protected final N()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->l:Lcom/google/android/apps/youtube/api/jar/w;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/jar/w;->d(Z)V

    goto :goto_0
.end method

.method protected final O()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->l:Lcom/google/android/apps/youtube/api/jar/w;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/jar/w;->d(Z)V

    goto :goto_0
.end method

.method protected final P()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->l:Lcom/google/android/apps/youtube/api/jar/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/w;->f()V

    goto :goto_0
.end method

.method protected final Q()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->l:Lcom/google/android/apps/youtube/api/jar/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/w;->g()V

    goto :goto_0
.end method

.method protected final R()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->n:Lcom/google/android/apps/youtube/api/jar/aa;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/aa;->a()V

    goto :goto_0
.end method

.method protected final S()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->n:Lcom/google/android/apps/youtube/api/jar/aa;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/aa;->b()V

    goto :goto_0
.end method

.method protected final T()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/b;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/b;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final U()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/j;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/j;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final V()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/k;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/k;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final W()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/l;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/l;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final X()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/n;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/n;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final Y()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/o;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/o;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final Z()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/p;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/p;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(I)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->l:Lcom/google/android/apps/youtube/api/jar/w;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/w;->a(I)V

    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->l:Lcom/google/android/apps/youtube/api/jar/w;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/w;->a(Landroid/content/res/Configuration;)V

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/q;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/q;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/player/internal/j;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->r:Lcom/google/android/youtube/player/internal/j;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/player/internal/m;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->q:Lcom/google/android/youtube/player/internal/m;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/player/internal/p;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->p:Lcom/google/android/youtube/player/internal/p;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/player/internal/s;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->o:Lcom/google/android/youtube/player/internal/s;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;->valueOf(Ljava/lang/String;)Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    sget-object v1, Lcom/google/android/apps/youtube/api/jar/a/i;->a:[I

    invoke-virtual {v0}, Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const-string v0, "Unhandled PlayerStyle"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->t:Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->t:Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;

    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->c:Lcom/google/android/apps/youtube/api/jar/b;

    invoke-interface {v1, v3}, Lcom/google/android/apps/youtube/api/jar/b;->setMinimal(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->c:Lcom/google/android/apps/youtube/api/jar/b;

    invoke-interface {v1, v3}, Lcom/google/android/apps/youtube/api/jar/b;->setControlsPermanentlyHidden(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->b:Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;->setFocusable(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->c:Lcom/google/android/apps/youtube/api/jar/b;

    invoke-interface {v1, v4}, Lcom/google/android/apps/youtube/api/jar/b;->setMinimal(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->c:Lcom/google/android/apps/youtube/api/jar/b;

    invoke-interface {v1, v3}, Lcom/google/android/apps/youtube/api/jar/b;->setControlsPermanentlyHidden(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->b:Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;->setFocusable(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->c:Lcom/google/android/apps/youtube/api/jar/b;

    invoke-interface {v1, v4}, Lcom/google/android/apps/youtube/api/jar/b;->setControlsPermanentlyHidden(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->b:Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;->setFocusable(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/a;->c(Ljava/lang/String;I)V

    return-void
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/jar/a/a;->c(Ljava/lang/String;II)V

    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->s:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->c:Lcom/google/android/apps/youtube/api/jar/b;

    invoke-interface {v0, p2}, Lcom/google/android/apps/youtube/api/jar/b;->setVideoTitle(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Ljava/util/List;II)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/jar/a/a;->c(Ljava/util/List;II)V

    return-void
.end method

.method public final a(Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/a;->c(Z)V

    return-void
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/a;->c(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const-string v0, "playerStyle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->l:Lcom/google/android/apps/youtube/api/jar/w;

    const-string v1, "fullscreenHelperState"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/jar/w;->a(Landroid/os/Bundle;)V

    const-string v0, "apiPlayerState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a([B)Z

    move-result v0

    goto :goto_0
.end method

.method protected abstract a([B)Z
.end method

.method protected final aa()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/c;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/c;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final ab()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/d;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/d;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected final ac()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/e;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(I)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->l:Lcom/google/android/apps/youtube/api/jar/w;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->k()I

    move-result v1

    or-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/jar/w;->a(I)V

    return-void
.end method

.method protected final b(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/m;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/m;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/a;->d(Ljava/lang/String;I)V

    return-void
.end method

.method public final b(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/jar/a/a;->d(Ljava/lang/String;II)V

    return-void
.end method

.method public final b(Ljava/util/List;II)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/jar/a/a;->d(Ljava/util/List;II)V

    return-void
.end method

.method public final b(Z)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/a;->g(Z)V

    return-void
.end method

.method public final b(ILandroid/view/KeyEvent;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/a;->d(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c(I)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/a;->e(I)V

    return-void
.end method

.method protected abstract c(Ljava/lang/String;I)V
.end method

.method protected abstract c(Ljava/lang/String;II)V
.end method

.method protected abstract c(Ljava/util/List;II)V
.end method

.method public final c(Z)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->v:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->l:Lcom/google/android/apps/youtube/api/jar/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/w;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->n:Lcom/google/android/apps/youtube/api/jar/aa;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/aa;->b()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->o:Lcom/google/android/youtube/player/internal/s;

    iput-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->p:Lcom/google/android/youtube/player/internal/p;

    iput-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->q:Lcom/google/android/youtube/player/internal/m;

    iput-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->r:Lcom/google/android/youtube/player/internal/j;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/a;->i(Z)V

    goto :goto_0
.end method

.method protected abstract c(ILandroid/view/KeyEvent;)Z
.end method

.method public final d(I)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/a;->f(I)V

    return-void
.end method

.method protected abstract d(Ljava/lang/String;I)V
.end method

.method protected abstract d(Ljava/lang/String;II)V
.end method

.method protected abstract d(Ljava/util/List;II)V
.end method

.method public final d(Z)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/a;->f(Z)V

    return-void
.end method

.method protected d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->v:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract d(ILandroid/view/KeyEvent;)Z
.end method

.method public final e()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->l:Lcom/google/android/apps/youtube/api/jar/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/w;->b()V

    goto :goto_0
.end method

.method protected abstract e(I)V
.end method

.method public final e(Z)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/a;->h(Z)V

    return-void
.end method

.method public final f()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->I()V

    goto :goto_0
.end method

.method protected abstract f(I)V
.end method

.method protected abstract f(Z)V
.end method

.method public final g()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->J()V

    goto :goto_0
.end method

.method protected final g(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/g;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/g;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected abstract g(Z)V
.end method

.method public final h()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->u:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->m:Lcom/google/android/apps/youtube/api/jar/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/u;->dismiss()V

    goto :goto_0
.end method

.method protected abstract h(Z)V
.end method

.method public final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->p:Lcom/google/android/youtube/player/internal/p;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->v:Z

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->p:Lcom/google/android/youtube/player/internal/p;

    sget-object v1, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->UNEXPECTED_SERVICE_DISCONNECTION:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-virtual {v1}, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/player/internal/p;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/a/a;->c(Z)V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/youtube/player/internal/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/player/internal/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method protected abstract i(Z)V
.end method

.method public final j()Lcom/google/android/youtube/player/internal/dynamic/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->b:Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-static {v0}, Lcom/google/android/youtube/player/internal/dynamic/d;->a(Ljava/lang/Object;)Lcom/google/android/youtube/player/internal/dynamic/a;

    move-result-object v0

    return-object v0
.end method

.method protected final j(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->l:Lcom/google/android/apps/youtube/api/jar/w;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/jar/w;->a(Z)V

    goto :goto_0
.end method

.method public final k()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->l:Lcom/google/android/apps/youtube/api/jar/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/w;->e()I

    move-result v0

    return v0
.end method

.method protected final k(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/f;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/f;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final l()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->y()V

    return-void
.end method

.method protected final l(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/h;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/h;-><init>(Lcom/google/android/apps/youtube/api/jar/a/a;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final m()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->z()V

    return-void
.end method

.method public final n()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->A()V

    return-void
.end method

.method public final o()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->B()Z

    move-result v0

    return v0
.end method

.method public final p()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->C()Z

    move-result v0

    return v0
.end method

.method public final q()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->D()Z

    move-result v0

    return v0
.end method

.method public final r()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->p()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Called next at end of playlist"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->E()V

    return-void
.end method

.method public final s()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->q()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Called previous at start of playlist"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->F()V

    return-void
.end method

.method public final t()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->G()I

    move-result v0

    return v0
.end method

.method public final u()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->H()I

    move-result v0

    return v0
.end method

.method public final v()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->a()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->K()V

    return-void
.end method

.method public final w()Landroid/os/Bundle;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "playerStyle"

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->t:Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;

    invoke-virtual {v2}, Lcom/google/android/youtube/player/YouTubePlayer$PlayerStyle;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "fullscreenHelperState"

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/a/a;->l:Lcom/google/android/apps/youtube/api/jar/w;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/api/jar/w;->d()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v1, "apiPlayerState"

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->x()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    goto :goto_0
.end method

.method protected abstract x()[B
.end method

.method protected abstract y()V
.end method

.method protected abstract z()V
.end method
