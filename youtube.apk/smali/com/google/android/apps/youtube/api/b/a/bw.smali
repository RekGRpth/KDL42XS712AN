.class public final Lcom/google/android/apps/youtube/api/b/a/bw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/youtube/api/b/a/bx;

.field private c:Lcom/google/android/apps/youtube/api/jar/a/cn;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/apps/youtube/api/jar/a/cn;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->a:Landroid/content/Context;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "client cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/a/cn;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    new-instance v0, Lcom/google/android/apps/youtube/api/b/a/bx;

    invoke-direct {v0, p2}, Lcom/google/android/apps/youtube/api/b/a/bx;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->b:Lcom/google/android/apps/youtube/api/b/a/bx;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->b:Lcom/google/android/apps/youtube/api/b/a/bx;

    invoke-interface {p3, v0}, Lcom/google/android/apps/youtube/api/jar/a/cn;->a(Lcom/google/android/apps/youtube/api/b/a/ar;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cn;->a(Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/cn;->i()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/cn;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/cn;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/cn;->j()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/cn;->a(ILandroid/view/KeyEvent;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/cn;->b(ILandroid/view/KeyEvent;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setAndShowEnded()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/cn;->h()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setAndShowPaused()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/cn;->g()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setAudioOnlyEnabled(Z)V
    .locals 0

    return-void
.end method

.method public final setCcEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cn;->k(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setErrorAndShowMessage(Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/cn;->a(Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setFullscreen(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cn;->d(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setHQ(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cn;->b(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setHQisHD(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cn;->c(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setHasCc(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cn;->j(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setHasInfoCard(Z)V
    .locals 0

    return-void
.end method

.method public final setHasNext(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cn;->f(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setHasPrevious(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cn;->g(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setInitial()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/cn;->f()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setLearnMoreEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cn;->i(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setListener(Lcom/google/android/apps/youtube/core/player/overlay/p;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->b:Lcom/google/android/apps/youtube/api/b/a/bx;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/bx;->a(Lcom/google/android/apps/youtube/core/player/overlay/p;)V

    return-void
.end method

.method public final setLoading()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/cn;->e()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setPlaying()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/cn;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setScrubbingEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cn;->h(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setShowFullscreen(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cn;->e(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/api/jar/a/cn;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setSupportsQualityToggle(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/cn;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setTimes(III)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bw;->c:Lcom/google/android/apps/youtube/api/jar/a/cn;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/jar/a/cn;->a(III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
