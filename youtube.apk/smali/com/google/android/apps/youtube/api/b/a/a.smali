.class public abstract Lcom/google/android/apps/youtube/api/b/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/medialib/player/y;


# instance fields
.field protected a:Lcom/google/android/apps/youtube/medialib/player/z;

.field private b:Lcom/google/android/apps/youtube/api/jar/a/ct;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/api/jar/a/ct;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "client cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/a/ct;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/a;->b:Lcom/google/android/apps/youtube/api/jar/a/ct;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/b/a/a;->g()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/a;->b:Lcom/google/android/apps/youtube/api/jar/a/ct;

    return-void
.end method

.method public final a(Z)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final b()I
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Access to video frame dimensions not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()I
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Access to video frame dimensions not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/a;->b:Lcom/google/android/apps/youtube/api/jar/a/ct;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/a;->b:Lcom/google/android/apps/youtube/api/jar/a/ct;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ct;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/a;->b:Lcom/google/android/apps/youtube/api/jar/a/ct;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/a;->b:Lcom/google/android/apps/youtube/api/jar/a/ct;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ct;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/a;->b:Lcom/google/android/apps/youtube/api/jar/a/ct;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/a;->a:Lcom/google/android/apps/youtube/medialib/player/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/a;->a:Lcom/google/android/apps/youtube/medialib/player/z;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/medialib/player/z;->c()V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/a;->b:Lcom/google/android/apps/youtube/api/jar/a/ct;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ct;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setListener(Lcom/google/android/apps/youtube/medialib/player/z;)V
    .locals 1

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/medialib/player/z;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/a;->a:Lcom/google/android/apps/youtube/medialib/player/z;

    return-void
.end method

.method public setOnLetterboxChangedListener(Lcom/google/android/apps/youtube/medialib/player/aa;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Zoom not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setVideoSize(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/a;->b:Lcom/google/android/apps/youtube/api/jar/a/ct;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/a;->b:Lcom/google/android/apps/youtube/api/jar/a/ct;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/ct;->a(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setZoom(I)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Zoom not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
