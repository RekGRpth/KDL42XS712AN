.class final Lcom/google/android/apps/youtube/api/y;
.super Ljavax/crypto/CipherSpi;
.source "SourceFile"


# instance fields
.field private a:[B

.field private b:I

.field private c:I

.field private d:[B


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljavax/crypto/CipherSpi;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/y;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/y;->d:[B

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/api/y;->a([B)V

    return-void
.end method

.method private a([B)V
    .locals 7

    const/16 v6, 0x100

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/y;->d:[B

    iput v0, p0, Lcom/google/android/apps/youtube/api/y;->b:I

    iput v0, p0, Lcom/google/android/apps/youtube/api/y;->c:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/y;->a:[B

    if-nez v1, :cond_0

    new-array v1, v6, [B

    iput-object v1, p0, Lcom/google/android/apps/youtube/api/y;->a:[B

    :cond_0
    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/y;->a:[B

    int-to-byte v3, v1

    aput-byte v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v1, v0

    move v2, v0

    :goto_1
    if-ge v0, v6, :cond_2

    aget-byte v3, p1, v2

    and-int/lit16 v3, v3, 0xff

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/y;->a:[B

    aget-byte v4, v4, v0

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    and-int/lit16 v1, v1, 0xff

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/y;->a:[B

    aget-byte v3, v3, v0

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/y;->a:[B

    iget-object v5, p0, Lcom/google/android/apps/youtube/api/y;->a:[B

    aget-byte v5, v5, v1

    aput-byte v5, v4, v0

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/y;->a:[B

    aput-byte v3, v4, v1

    add-int/lit8 v2, v2, 0x1

    array-length v3, p1

    rem-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method

.method private a([BII[BI)V
    .locals 7

    add-int v0, p2, p3

    array-length v1, p1

    if-le v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Input buffer too short"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    add-int v0, p5, p3

    array-length v1, p4

    if-le v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Output buffer too short"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_2

    iget v1, p0, Lcom/google/android/apps/youtube/api/y;->b:I

    add-int/lit8 v1, v1, 0x1

    and-int/lit16 v1, v1, 0xff

    iput v1, p0, Lcom/google/android/apps/youtube/api/y;->b:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/y;->a:[B

    iget v2, p0, Lcom/google/android/apps/youtube/api/y;->b:I

    aget-byte v1, v1, v2

    iget v2, p0, Lcom/google/android/apps/youtube/api/y;->c:I

    add-int/2addr v1, v2

    and-int/lit16 v1, v1, 0xff

    iput v1, p0, Lcom/google/android/apps/youtube/api/y;->c:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/y;->a:[B

    iget v2, p0, Lcom/google/android/apps/youtube/api/y;->b:I

    aget-byte v1, v1, v2

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/y;->a:[B

    iget v3, p0, Lcom/google/android/apps/youtube/api/y;->b:I

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/y;->a:[B

    iget v5, p0, Lcom/google/android/apps/youtube/api/y;->c:I

    aget-byte v4, v4, v5

    aput-byte v4, v2, v3

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/y;->a:[B

    iget v3, p0, Lcom/google/android/apps/youtube/api/y;->c:I

    aput-byte v1, v2, v3

    add-int v1, v0, p5

    add-int v2, v0, p2

    aget-byte v2, p1, v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/y;->a:[B

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/y;->a:[B

    iget v5, p0, Lcom/google/android/apps/youtube/api/y;->b:I

    aget-byte v4, v4, v5

    iget-object v5, p0, Lcom/google/android/apps/youtube/api/y;->a:[B

    iget v6, p0, Lcom/google/android/apps/youtube/api/y;->c:I

    aget-byte v5, v5, v6

    add-int/2addr v4, v5

    and-int/lit16 v4, v4, 0xff

    aget-byte v3, v3, v4

    xor-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, p4, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method protected final engineDoFinal([BII[BI)I
    .locals 0

    if-eqz p3, :cond_0

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/youtube/api/y;->a([BII[BI)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/y;->a()V

    return p3
.end method

.method protected final engineDoFinal([BII)[B
    .locals 1

    if-eqz p3, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/y;->engineUpdate([BII)[B

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/y;->a()V

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/y;->a()V

    const/4 v0, 0x0

    new-array v0, v0, [B

    goto :goto_0
.end method

.method protected final engineGetBlockSize()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final engineGetIV()[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final engineGetKeySize(Ljava/security/Key;)I
    .locals 1

    invoke-interface {p1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x8

    return v0
.end method

.method protected final engineGetOutputSize(I)I
    .locals 0

    return p1
.end method

.method protected final engineGetParameters()Ljava/security/AlgorithmParameters;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final engineInit(ILjava/security/Key;Ljava/security/AlgorithmParameters;Ljava/security/SecureRandom;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p4}, Lcom/google/android/apps/youtube/api/y;->engineInit(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V

    return-void
.end method

.method protected final engineInit(ILjava/security/Key;Ljava/security/SecureRandom;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/google/android/apps/youtube/api/y;->engineInit(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V

    return-void
.end method

.method protected final engineInit(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V
    .locals 2

    invoke-interface {p2}, Ljava/security/Key;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/security/InvalidKeyException;

    const-string v1, "Must be provided with an RC4 key."

    invoke-direct {v0, v1}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-interface {p2}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/y;->d:[B

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/y;->d:[B

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/api/y;->a([B)V

    return-void
.end method

.method protected final engineSetMode(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "can\'t support mode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final engineSetPadding(Ljava/lang/String;)V
    .locals 3

    const-string v0, "NoPadding"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljavax/crypto/NoSuchPaddingException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Padding "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " unknown."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/crypto/NoSuchPaddingException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method protected final engineUpdate([BII[BI)I
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/youtube/api/y;->a([BII[BI)V

    return p3
.end method

.method protected final engineUpdate([BII)[B
    .locals 6

    new-array v4, p3, [B

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/api/y;->a([BII[BI)V

    return-object v4
.end method
