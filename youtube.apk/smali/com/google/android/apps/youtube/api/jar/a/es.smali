.class final Lcom/google/android/apps/youtube/api/jar/a/es;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field private a:Lcom/google/android/apps/youtube/api/b/a/ba;

.field private b:Lcom/google/android/apps/youtube/api/jar/a/et;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/api/b/a/ba;Lcom/google/android/apps/youtube/api/jar/a/et;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/b/a/ba;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/es;->a:Lcom/google/android/apps/youtube/api/b/a/ba;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/a/et;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/es;->b:Lcom/google/android/apps/youtube/api/jar/a/et;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/IBinder;Landroid/os/IBinder;Landroid/view/WindowManager$LayoutParams;IIIZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/content/res/Configuration;Landroid/view/Surface;)I
    .locals 13

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/es;->a:Lcom/google/android/apps/youtube/api/b/a/ba;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/es;->a:Lcom/google/android/apps/youtube/api/b/a/ba;

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    invoke-interface/range {v0 .. v12}, Lcom/google/android/apps/youtube/api/b/a/ba;->a(Landroid/os/IBinder;Landroid/os/IBinder;Landroid/view/WindowManager$LayoutParams;IIIZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/content/res/Configuration;Landroid/view/Surface;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/es;->a:Lcom/google/android/apps/youtube/api/b/a/ba;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/es;->b:Lcom/google/android/apps/youtube/api/jar/a/et;

    return-void
.end method

.method public final surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/es;->a:Lcom/google/android/apps/youtube/api/b/a/ba;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/es;->a:Lcom/google/android/apps/youtube/api/b/a/ba;

    invoke-interface {v0, p2, p3, p4}, Lcom/google/android/apps/youtube/api/b/a/ba;->a(III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/es;->a:Lcom/google/android/apps/youtube/api/b/a/ba;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/es;->a:Lcom/google/android/apps/youtube/api/b/a/ba;

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/api/b/a/ba;->a(Landroid/view/Surface;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/es;->b:Lcom/google/android/apps/youtube/api/jar/a/et;

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/api/jar/a/et;->a(Landroid/view/Surface;)V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/es;->a:Lcom/google/android/apps/youtube/api/b/a/ba;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/es;->a:Lcom/google/android/apps/youtube/api/b/a/ba;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/b/a/ba;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/es;->b:Lcom/google/android/apps/youtube/api/jar/a/et;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/et;->f()V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
