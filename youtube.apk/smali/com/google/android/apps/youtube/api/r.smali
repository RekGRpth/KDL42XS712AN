.class public final Lcom/google/android/apps/youtube/api/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/a/f;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/r;->a:Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/r;->b:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->i(Landroid/content/Context;)[B

    move-result-object v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/r;->c:Ljava/lang/String;

    return-void
.end method

.method private b()Lcom/google/a/a/a/a/su;
    .locals 2

    new-instance v0, Lcom/google/a/a/a/a/su;

    invoke-direct {v0}, Lcom/google/a/a/a/a/su;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/r;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/su;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/r;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/su;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/r;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/su;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/r;->b()Lcom/google/a/a/a/a/su;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/Util;->b([B)[B

    move-result-object v0

    const/16 v1, 0xb

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/a/a/a/a/ii;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/r;->b()Lcom/google/a/a/a/a/su;

    move-result-object v0

    iput-object v0, p1, Lcom/google/a/a/a/a/ii;->g:Lcom/google/a/a/a/a/su;

    return-void
.end method
