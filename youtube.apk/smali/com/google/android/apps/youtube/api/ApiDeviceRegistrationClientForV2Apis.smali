.class public final Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;
.super Lcom/google/android/apps/youtube/core/client/m;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/client/as;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/async/af;

.field private final h:Lcom/google/android/apps/youtube/core/async/af;

.field private final i:Ljava/lang/String;

.field private final j:[B

.field private final k:[B

.field private final l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;[B[B)V
    .locals 2

    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/youtube/core/client/m;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V

    const-string v0, "developerKey cannot be null or empty"

    invoke-static {p4, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->i:Ljava/lang/String;

    const-string v0, "serial cannot be null or empty"

    invoke-static {p5, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->l:Ljava/lang/String;

    const-string v0, "playerApiKey cannot be null"

    invoke-static {p6, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->k:[B

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->h(Landroid/content/Context;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->j:[B

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/dp;

    sget-object v1, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->POST:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/converter/http/dp;-><init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;)V

    new-instance v1, Lcom/google/android/apps/youtube/core/converter/http/ad;

    invoke-direct {v1, p7}, Lcom/google/android/apps/youtube/core/converter/http/ad;-><init>([B)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->a:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v0, Lcom/google/android/apps/youtube/api/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/api/h;-><init>(Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;B)V

    invoke-virtual {p0, v0, v0}, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->h:Lcom/google/android/apps/youtube/core/async/af;

    return-void
.end method

.method private a()[B
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->b()Ljavax/crypto/Cipher;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->j:[B

    invoke-virtual {v0, v1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;)[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->j:[B

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->i:Ljava/lang/String;

    return-object v0
.end method

.method private b()Ljavax/crypto/Cipher;
    .locals 5

    const/16 v4, 0x8

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->l:Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    new-instance v2, Ljava/util/zip/CRC32;

    invoke-direct {v2}, Ljava/util/zip/CRC32;-><init>()V

    invoke-virtual {v2, v1}, Ljava/util/zip/CRC32;->update([B)V

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v2}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    array-length v2, v1

    if-ne v2, v4, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v0, "RC4"

    invoke-direct {v2, v1, v0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    :try_start_1
    const-string v0, "RC4"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    :goto_1
    const/4 v1, 0x1

    :try_start_2
    invoke-virtual {v0, v1, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V
    :try_end_2
    .catch Ljava/security/InvalidKeyException; {:try_start_2 .. :try_end_2} :catch_3

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v0, Lcom/google/android/apps/youtube/api/x;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/api/x;-><init>()V

    goto :goto_1

    :catch_2
    move-exception v0

    new-instance v0, Lcom/google/android/apps/youtube/api/x;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/api/x;-><init>()V

    goto :goto_1

    :catch_3
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;)Lcom/google/android/apps/youtube/core/async/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->h:Lcom/google/android/apps/youtube/core/async/af;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;)[B
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->a()[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 4

    :try_start_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->k:[B

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v1, "https://www.google.com/youtube/accounts/registerDevice?type=GDATA&developer=%s&serialNumber=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->l:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;->a:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v2, Lcom/google/android/apps/youtube/api/i;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/youtube/api/i;-><init>(Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
