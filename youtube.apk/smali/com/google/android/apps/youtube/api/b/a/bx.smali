.class final Lcom/google/android/apps/youtube/api/b/a/bx;
.super Lcom/google/android/apps/youtube/api/b/a/as;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Lcom/google/android/apps/youtube/core/player/overlay/p;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/b/a/as;-><init>()V

    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->a:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/b/a/bx;)Lcom/google/android/apps/youtube/core/player/overlay/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->b:Lcom/google/android/apps/youtube/core/player/overlay/p;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/by;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/by;-><init>(Lcom/google/android/apps/youtube/api/b/a/bx;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/ch;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/b/a/ch;-><init>(Lcom/google/android/apps/youtube/api/b/a/bx;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/player/overlay/p;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/p;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->b:Lcom/google/android/apps/youtube/core/player/overlay/p;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/cd;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/b/a/cd;-><init>(Lcom/google/android/apps/youtube/api/b/a/bx;Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/ca;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/b/a/ca;-><init>(Lcom/google/android/apps/youtube/api/b/a/bx;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/cf;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/cf;-><init>(Lcom/google/android/apps/youtube/api/b/a/bx;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/cg;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/cg;-><init>(Lcom/google/android/apps/youtube/api/b/a/bx;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/ci;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/ci;-><init>(Lcom/google/android/apps/youtube/api/b/a/bx;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/cj;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/cj;-><init>(Lcom/google/android/apps/youtube/api/b/a/bx;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/ck;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/ck;-><init>(Lcom/google/android/apps/youtube/api/b/a/bx;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/cl;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/cl;-><init>(Lcom/google/android/apps/youtube/api/b/a/bx;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/cm;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/cm;-><init>(Lcom/google/android/apps/youtube/api/b/a/bx;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/bz;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/bz;-><init>(Lcom/google/android/apps/youtube/api/b/a/bx;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final j()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/cb;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/cb;-><init>(Lcom/google/android/apps/youtube/api/b/a/bx;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final k()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/cc;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/cc;-><init>(Lcom/google/android/apps/youtube/api/b/a/bx;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final l()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bx;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/ce;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/ce;-><init>(Lcom/google/android/apps/youtube/api/b/a/bx;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
