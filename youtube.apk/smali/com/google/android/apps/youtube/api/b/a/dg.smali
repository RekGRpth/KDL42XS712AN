.class final Lcom/google/android/apps/youtube/api/b/a/dg;
.super Lcom/google/android/apps/youtube/api/b/a/be;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/api/b/a/de;

.field private final b:Landroid/os/Handler;

.field private c:Lcom/google/android/apps/youtube/api/b/a/df;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/api/b/a/de;Landroid/os/Handler;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/b/a/dg;->a:Lcom/google/android/apps/youtube/api/b/a/de;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/b/a/be;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dg;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/b/a/dg;)Lcom/google/android/apps/youtube/api/b/a/df;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dg;->c:Lcom/google/android/apps/youtube/api/b/a/df;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dg;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/dj;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/dj;-><init>(Lcom/google/android/apps/youtube/api/b/a/dg;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dg;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/di;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/api/b/a/di;-><init>(Lcom/google/android/apps/youtube/api/b/a/dg;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dg;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/dh;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/b/a/dh;-><init>(Lcom/google/android/apps/youtube/api/b/a/dg;Landroid/view/Surface;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/api/b/a/df;)V
    .locals 1

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/b/a/df;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dg;->c:Lcom/google/android/apps/youtube/api/b/a/df;

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/dg;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/dk;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/dk;-><init>(Lcom/google/android/apps/youtube/api/b/a/dg;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
