.class public final Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;
.super Lcom/google/android/apps/youtube/api/jar/ApiVideoView;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/api/jar/a/ey;
.implements Lcom/google/android/apps/youtube/api/jar/h;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/api/jar/ah;

.field private b:Landroid/view/TextureView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/ah;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;-><init>(Landroid/content/Context;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/ah;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->a:Lcom/google/android/apps/youtube/api/jar/ah;

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->a:Lcom/google/android/apps/youtube/api/jar/ah;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/ah;->a()V

    return-void
.end method

.method public final a(Landroid/view/TextureView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->b:Landroid/view/TextureView;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->addView(Landroid/view/View;)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->b:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->b:Landroid/view/TextureView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->b:Landroid/view/TextureView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->addView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 0

    return-void
.end method

.method public final d()V
    .locals 0

    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->b:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->b:Landroid/view/TextureView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->removeView(Landroid/view/View;)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->b:Landroid/view/TextureView;

    return-void
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->a:Lcom/google/android/apps/youtube/api/jar/ah;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/ah;->b()V

    return-void
.end method

.method protected final onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->isHardwareAccelerated()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->a:Lcom/google/android/apps/youtube/api/jar/ah;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/ah;->c()V

    :cond_0
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->b:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->b:Landroid/view/TextureView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->b:Landroid/view/TextureView;

    invoke-virtual {v1}, Landroid/view/TextureView;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->b:Landroid/view/TextureView;

    invoke-virtual {v2}, Landroid/view/TextureView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/TextureView;->layout(IIII)V

    :cond_0
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 3

    const/high16 v2, 0x40000000    # 2.0f

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/ApiVideoView;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->getMeasuredWidth()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->b:Landroid/view/TextureView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;->b:Landroid/view/TextureView;

    invoke-virtual {v2, v0, v1}, Landroid/view/TextureView;->measure(II)V

    :cond_0
    return-void
.end method
