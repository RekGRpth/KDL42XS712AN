.class final Lcom/google/android/apps/youtube/api/b/a/cz;
.super Lcom/google/android/apps/youtube/api/b/a/cw;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/google/android/apps/youtube/api/b/a/cv;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/api/b/a/cv;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/b/a/cz;->b:Lcom/google/android/apps/youtube/api/b/a/cv;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/api/b/a/cw;-><init>(Lcom/google/android/apps/youtube/api/b/a/cv;B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/api/b/a/cv;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/api/b/a/cz;-><init>(Lcom/google/android/apps/youtube/api/b/a/cv;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/IBinder;Landroid/os/IBinder;Landroid/view/WindowManager$LayoutParams;IIIZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/content/res/Configuration;Landroid/view/Surface;)I
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "remoteSurfaceViewRelayout should not be called post ICS"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cz;->b:Lcom/google/android/apps/youtube/api/b/a/cv;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/b/a/cv;->a(Lcom/google/android/apps/youtube/api/b/a/cv;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/da;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/b/a/da;-><init>(Lcom/google/android/apps/youtube/api/b/a/cz;Landroid/view/Surface;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
