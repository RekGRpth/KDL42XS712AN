.class public final Lcom/google/android/apps/youtube/api/ab;
.super Lcom/google/android/apps/youtube/core/client/ba;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/client/bc;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

.field private final h:Lcom/google/android/apps/youtube/core/converter/http/ao;

.field private i:Lcom/google/android/apps/youtube/core/converter/http/fo;

.field private j:Lcom/google/android/apps/youtube/core/converter/http/dx;

.field private k:Lcom/google/android/apps/youtube/common/cache/a;

.field private l:Lcom/google/android/apps/youtube/common/cache/a;

.field private final m:Lcom/google/android/apps/youtube/core/async/af;

.field private final n:Lcom/google/android/apps/youtube/core/async/af;

.field private final o:Lcom/google/android/apps/youtube/core/async/af;

.field private final p:Lcom/google/android/apps/youtube/core/async/af;

.field private final q:Lcom/google/android/apps/youtube/core/async/af;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;Lcom/google/android/apps/youtube/core/identity/ar;)V
    .locals 6

    invoke-direct {p0, p1, p2, p4, p3}, Lcom/google/android/apps/youtube/core/client/ba;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;)V

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->a:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/ao;

    sget-object v1, Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;->GET:Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;

    move-object v2, p6

    move-object v3, p7

    move-object v4, p8

    move-object v5, p9

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/converter/http/ao;-><init>(Lcom/google/android/apps/youtube/core/converter/http/HttpMethod;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;Lcom/google/android/apps/youtube/core/identity/ar;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->h:Lcom/google/android/apps/youtube/core/converter/http/ao;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/fo;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ab;->g:Lcom/google/android/apps/youtube/core/converter/n;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/converter/http/fo;-><init>(Lcom/google/android/apps/youtube/core/converter/n;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->i:Lcom/google/android/apps/youtube/core/converter/http/fo;

    const/16 v0, 0x1f4

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/ab;->a(I)Lcom/google/android/apps/youtube/common/cache/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->k:Lcom/google/android/apps/youtube/common/cache/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->h:Lcom/google/android/apps/youtube/core/converter/http/ao;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ab;->i:Lcom/google/android/apps/youtube/core/converter/http/fo;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/api/ab;->a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/ab;->a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ab;->k:Lcom/google/android/apps/youtube/common/cache/a;

    const-wide/32 v2, 0xdbba00

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/apps/youtube/api/ab;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->m:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/fn;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ab;->g:Lcom/google/android/apps/youtube/core/converter/n;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/converter/http/fn;-><init>(Lcom/google/android/apps/youtube/core/converter/n;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ab;->h:Lcom/google/android/apps/youtube/core/converter/http/ao;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/api/ab;->a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/ab;->a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->o:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/dx;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ab;->g:Lcom/google/android/apps/youtube/core/converter/n;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/converter/http/dx;-><init>(Lcom/google/android/apps/youtube/core/converter/n;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->j:Lcom/google/android/apps/youtube/core/converter/http/dx;

    const/16 v0, 0x64

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/ab;->a(I)Lcom/google/android/apps/youtube/common/cache/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->l:Lcom/google/android/apps/youtube/common/cache/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->h:Lcom/google/android/apps/youtube/core/converter/http/ao;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ab;->j:Lcom/google/android/apps/youtube/core/converter/http/dx;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/api/ab;->a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/ab;->a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ab;->l:Lcom/google/android/apps/youtube/common/cache/a;

    const-wide/32 v2, 0x1b7740

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/apps/youtube/api/ab;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->n:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/f;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ab;->g:Lcom/google/android/apps/youtube/core/converter/n;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/converter/http/f;-><init>(Lcom/google/android/apps/youtube/core/converter/n;)V

    const/16 v1, 0x14

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/ab;->a(I)Lcom/google/android/apps/youtube/common/cache/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/ab;->h:Lcom/google/android/apps/youtube/core/converter/http/ao;

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/youtube/api/ab;->a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/ab;->a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v0

    const-wide/32 v2, 0x6ddd00

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/apps/youtube/api/ab;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/core/async/af;J)Lcom/google/android/apps/youtube/core/async/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->p:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v0, Lcom/google/android/apps/youtube/core/converter/http/bl;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ab;->g:Lcom/google/android/apps/youtube/core/converter/n;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/converter/http/bl;-><init>(Lcom/google/android/apps/youtube/core/converter/n;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ab;->h:Lcom/google/android/apps/youtube/core/converter/http/ao;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/api/ab;->a(Lcom/google/android/apps/youtube/core/converter/c;Lcom/google/android/apps/youtube/core/converter/http/ay;)Lcom/google/android/apps/youtube/core/async/u;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/api/ab;->a(Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->q:Lcom/google/android/apps/youtube/core/async/af;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->a:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    return-object v0
.end method

.method public final a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->n:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ab;->a:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->m:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ab;->a:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final b()Lcom/google/android/apps/youtube/core/async/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->m:Lcom/google/android/apps/youtube/core/async/af;

    return-object v0
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->p:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/ab;->a:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->j(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final c()Lcom/google/android/apps/youtube/core/async/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/ab;->o:Lcom/google/android/apps/youtube/core/async/af;

    return-object v0
.end method
