.class public abstract Lcom/google/android/apps/youtube/uilib/innertube/e;
.super Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/uilib/a/h;

.field private final b:Lcom/google/android/apps/youtube/uilib/innertube/o;

.field private c:Lcom/google/android/apps/youtube/common/fromguava/d;

.field private d:Z

.field private e:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/uilib/a/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;)V
    .locals 4

    invoke-static {}, Lcom/google/android/apps/youtube/common/c/a;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, p1, p3, v0, p4}, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/common/c/a;Ljava/lang/Object;Lcom/google/android/apps/youtube/core/aw;)V

    invoke-static {}, Lcom/google/android/apps/youtube/common/e/g;->a()Lcom/google/android/apps/youtube/common/fromguava/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->c:Lcom/google/android/apps/youtube/common/fromguava/d;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/a/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->d:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->e:Z

    const-class v0, Lcom/google/android/apps/youtube/uilib/innertube/e;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/e;->b()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p3, p0, v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)V

    new-instance v0, Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/e;->b()Ljava/lang/Object;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/uilib/innertube/f;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/uilib/innertube/f;-><init>(Lcom/google/android/apps/youtube/uilib/innertube/e;)V

    new-instance v3, Lcom/google/android/apps/youtube/uilib/innertube/g;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/uilib/innertube/g;-><init>(Lcom/google/android/apps/youtube/uilib/innertube/e;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/uilib/innertube/o;-><init>(Ljava/lang/Object;Landroid/view/View$OnClickListener;Lcom/google/android/apps/youtube/uilib/innertube/q;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->b:Lcom/google/android/apps/youtube/uilib/innertube/o;

    return-void
.end method

.method private j()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->d:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/innertube/e;->k()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/innertube/e;->l()V

    goto :goto_0
.end method

.method private k()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/innertube/e;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/innertube/e;->n()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(I)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private l()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/innertube/e;->m()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->b:Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private m()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/uilib/a/h;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/innertube/e;->n()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/uilib/a/h;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->b:Lcom/google/android/apps/youtube/uilib/innertube/o;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private n()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method


# virtual methods
.method protected final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->b()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/e;->g()V

    return-void
.end method

.method protected final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->b:Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/uilib/innertube/o;->a(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected final a(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->b:Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/uilib/innertube/o;->a(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->notifyDataSetChanged()V

    return-void
.end method

.method protected final a(Ljava/lang/Object;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/innertube/e;->k()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->c:Lcom/google/android/apps/youtube/common/fromguava/d;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/fromguava/d;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/innertube/e;->j()V

    return-void
.end method

.method protected final a(Ljava/util/Collection;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/innertube/e;->k()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->c:Lcom/google/android/apps/youtube/common/fromguava/d;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/util/Collection;Lcom/google/android/apps/youtube/common/fromguava/d;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/innertube/e;->j()V

    return-void
.end method

.method protected final a(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->d:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->d:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/innertube/e;->j()V

    :cond_0
    return-void
.end method

.method public onContentEvent(Lcom/google/android/apps/youtube/uilib/innertube/b;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->b:Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/uilib/innertube/o;->a(Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->e:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->d:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/innertube/e;->k()V

    :cond_0
    return-void
.end method

.method public onErrorEvent(Lcom/google/android/apps/youtube/uilib/innertube/c;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->b:Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/uilib/innertube/o;->a(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->e:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/innertube/e;->l()V

    return-void
.end method

.method public onLoadingEvent(Lcom/google/android/apps/youtube/uilib/innertube/d;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->b:Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/uilib/innertube/o;->a(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/e;->e:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/innertube/e;->l()V

    return-void
.end method
