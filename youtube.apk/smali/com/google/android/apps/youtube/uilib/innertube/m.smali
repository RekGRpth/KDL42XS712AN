.class public final Lcom/google/android/apps/youtube/uilib/innertube/m;
.super Lcom/google/android/apps/youtube/uilib/innertube/e;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/uilib/innertube/i;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/uilib/a/h;Lcom/google/android/apps/youtube/uilib/innertube/i;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;)V
    .locals 1

    invoke-direct {p0, p1, p2, p4, p5}, Lcom/google/android/apps/youtube/uilib/innertube/e;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/uilib/a/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;)V

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/innertube/i;

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/m;->a:Lcom/google/android/apps/youtube/uilib/innertube/i;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/uilib/innertube/m;)Lcom/google/android/apps/youtube/uilib/innertube/i;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/m;->a:Lcom/google/android/apps/youtube/uilib/innertube/i;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/p;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/m;->a()V

    new-instance v0, Lcom/google/android/apps/youtube/uilib/innertube/l;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/uilib/innertube/l;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/l;->a(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/m;->a(Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/m;->a(Ljava/util/Collection;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->b()Lcom/google/a/a/a/a/qu;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/p;->d()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/m;->a(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/google/android/apps/youtube/uilib/innertube/n;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/n;-><init>(Lcom/google/android/apps/youtube/uilib/innertube/m;Lcom/google/a/a/a/a/qu;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/m;->a(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/m;->a(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/m;->a(Z)V

    goto :goto_0
.end method
