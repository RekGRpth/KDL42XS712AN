.class public final Lcom/google/android/apps/youtube/uilib/a/b;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Ljava/util/LinkedHashSet;

.field private final c:Landroid/database/DataSetObserver;

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/b;->a:Ljava/util/List;

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/b;->b:Ljava/util/LinkedHashSet;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/uilib/a/b;->d:I

    new-instance v0, Lcom/google/android/apps/youtube/uilib/a/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/uilib/a/c;-><init>(Lcom/google/android/apps/youtube/uilib/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/b;->c:Landroid/database/DataSetObserver;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/uilib/a/b;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/a/b;->c()V

    return-void
.end method

.method private b(I)Lcom/google/android/apps/youtube/uilib/a/d;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/a/d;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/d;->a(Lcom/google/android/apps/youtube/uilib/a/d;I)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/a/b;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/a/d;

    iput v1, v0, Lcom/google/android/apps/youtube/uilib/a/d;->b:I

    iget-object v0, v0, Lcom/google/android/apps/youtube/uilib/a/d;->a:Lcom/google/android/apps/youtube/uilib/a/e;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/uilib/a/e;->getCount()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    iput v1, p0, Lcom/google/android/apps/youtube/uilib/a/b;->d:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/b;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/a/d;

    iget-object v0, v0, Lcom/google/android/apps/youtube/uilib/a/d;->a:Lcom/google/android/apps/youtube/uilib/a/e;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/a/b;->c:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/e;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/b;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/uilib/a/e;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/b;->a:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/youtube/uilib/a/d;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/apps/youtube/uilib/a/d;-><init>(Lcom/google/android/apps/youtube/uilib/a/e;B)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/b;->c:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Lcom/google/android/apps/youtube/uilib/a/e;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/a/b;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/b;->b:Ljava/util/LinkedHashSet;

    invoke-interface {p1}, Lcom/google/android/apps/youtube/uilib/a/e;->a()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/a/b;->notifyDataSetChanged()V

    return-void
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/a/d;

    iget-object v0, v0, Lcom/google/android/apps/youtube/uilib/a/d;->a:Lcom/google/android/apps/youtube/uilib/a/e;

    iget-object v2, p0, Lcom/google/android/apps/youtube/uilib/a/b;->c:Landroid/database/DataSetObserver;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/uilib/a/e;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/b;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/uilib/a/b;->c()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/a/b;->notifyDataSetChanged()V

    return-void
.end method

.method public final getCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/uilib/a/b;->d:I

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/uilib/a/b;->b(I)Lcom/google/android/apps/youtube/uilib/a/d;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/youtube/uilib/a/d;->a:Lcom/google/android/apps/youtube/uilib/a/e;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/d;->c(Lcom/google/android/apps/youtube/uilib/a/d;I)I

    move-result v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/uilib/a/e;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/uilib/a/b;->b(I)Lcom/google/android/apps/youtube/uilib/a/d;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/youtube/uilib/a/d;->a:Lcom/google/android/apps/youtube/uilib/a/e;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/d;->c(Lcom/google/android/apps/youtube/uilib/a/d;I)I

    move-result v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/uilib/a/e;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 5

    const/4 v2, -0x1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/uilib/a/b;->b(I)Lcom/google/android/apps/youtube/uilib/a/d;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/d;->b(Lcom/google/android/apps/youtube/uilib/a/d;I)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/youtube/uilib/a/d;->a:Lcom/google/android/apps/youtube/uilib/a/e;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/d;->c(Lcom/google/android/apps/youtube/uilib/a/d;I)I

    move-result v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/uilib/a/e;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/b;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, v3}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/a/b;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v1}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    if-eq v0, v3, :cond_0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/uilib/a/b;->b(I)Lcom/google/android/apps/youtube/uilib/a/d;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/youtube/uilib/a/d;->a:Lcom/google/android/apps/youtube/uilib/a/e;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/d;->c(Lcom/google/android/apps/youtube/uilib/a/d;I)I

    move-result v0

    invoke-interface {v1, v0, p2, p3}, Lcom/google/android/apps/youtube/uilib/a/e;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/b;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->size()I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method
