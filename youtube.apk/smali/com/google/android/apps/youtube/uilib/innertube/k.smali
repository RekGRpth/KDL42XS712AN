.class public final Lcom/google/android/apps/youtube/uilib/innertube/k;
.super Lcom/google/android/apps/youtube/uilib/innertube/e;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/uilib/a/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/uilib/innertube/e;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/uilib/a/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;)V

    return-void
.end method

.method private b(Lcom/google/android/apps/youtube/datalib/innertube/model/o;)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->b()Lcom/google/a/a/a/a/dp;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/k;->a(Lcom/google/a/a/a/a/dp;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/k;->a(Ljava/util/Collection;)V

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/a/a/a/a/dq;Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/uilib/innertube/e;->a(Lcom/google/a/a/a/a/dq;Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;)V

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/a/a/a/a/dq;->c:Lcom/google/a/a/a/a/it;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;

    iget-object v1, p1, Lcom/google/a/a/a/a/dq;->c:Lcom/google/a/a/a/a/it;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/o;-><init>(Lcom/google/a/a/a/a/it;)V

    sget-object v1, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;->RELOAD:Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;

    if-ne p2, v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/k;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/o;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/k;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/o;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/o;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/k;->a()V

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/uilib/innertube/k;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/o;)V

    return-void
.end method
