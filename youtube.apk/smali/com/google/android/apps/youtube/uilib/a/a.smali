.class public abstract Lcom/google/android/apps/youtube/uilib/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/youtube/uilib/a/g;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/datalib/d/a;

.field private b:Lcom/google/a/a/a/a/kz;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/d/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/d/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/a;->a:Lcom/google/android/apps/youtube/datalib/d/a;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/uilib/a/i;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/uilib/a/a;-><init>(Lcom/google/android/apps/youtube/datalib/d/a;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p2, p0}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/u;)Landroid/view/View;
    .locals 1

    invoke-interface {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/u;->e()Lcom/google/a/a/a/a/kz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/a;->b:Lcom/google/a/a/a/a/kz;

    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/u;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/uilib/a/a;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/u;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/a;->b:Lcom/google/a/a/a/a/kz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/a;->a:Lcom/google/android/apps/youtube/datalib/d/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/a/a;->b:Lcom/google/a/a/a/a/kz;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/datalib/d/a;->a(Lcom/google/a/a/a/a/kz;)V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/a/a;->b()V

    return-void
.end method
