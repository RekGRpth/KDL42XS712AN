.class public final Lcom/google/android/apps/youtube/uilib/innertube/r;
.super Lcom/google/android/apps/youtube/uilib/innertube/e;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/uilib/a/h;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/uilib/a/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/uilib/innertube/e;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/uilib/a/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;)V

    iput-object p2, p0, Lcom/google/android/apps/youtube/uilib/innertube/r;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {p3, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    return-void
.end method

.method private b(Lcom/google/android/apps/youtube/datalib/innertube/model/ag;)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;->b()Lcom/google/a/a/a/a/dp;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/r;->a(Lcom/google/a/a/a/a/dp;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/r;->a(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method private handleVideoRemovedFromPlaylistEvent(Lcom/google/android/apps/youtube/datalib/innertube/ap;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/r;->a:Lcom/google/android/apps/youtube/uilib/a/h;

    new-instance v1, Lcom/google/android/apps/youtube/uilib/innertube/s;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/uilib/innertube/s;-><init>(Lcom/google/android/apps/youtube/uilib/innertube/r;Lcom/google/android/apps/youtube/datalib/innertube/ap;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Lcom/google/android/apps/youtube/common/fromguava/d;)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/a/a/a/a/dq;Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/uilib/innertube/e;->a(Lcom/google/a/a/a/a/dq;Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;)V

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/a/a/a/a/dq;->g:Lcom/google/a/a/a/a/os;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;

    iget-object v1, p1, Lcom/google/a/a/a/a/dq;->g:Lcom/google/a/a/a/a/os;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;-><init>(Lcom/google/a/a/a/a/os;)V

    sget-object v1, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;->RELOAD:Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;

    if-ne p2, v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/r;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ag;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/r;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/ag;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/ag;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/uilib/innertube/r;->a()V

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/uilib/innertube/r;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/ag;)V

    return-void
.end method
