.class final Lcom/google/android/apps/youtube/uilib/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/apps/youtube/uilib/a/e;

.field b:I


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/uilib/a/e;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/a/e;

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/d;->a:Lcom/google/android/apps/youtube/uilib/a/e;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/uilib/a/d;->b:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/uilib/a/e;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/uilib/a/d;-><init>(Lcom/google/android/apps/youtube/uilib/a/e;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/uilib/a/d;I)Z
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/youtube/uilib/a/d;->b:I

    if-ge p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/apps/youtube/uilib/a/d;->b:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/uilib/a/d;->a:Lcom/google/android/apps/youtube/uilib/a/e;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/uilib/a/e;->getCount()I

    move-result v2

    add-int/2addr v1, v2

    if-ge p1, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/uilib/a/d;I)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/a/d;->a:Lcom/google/android/apps/youtube/uilib/a/e;

    iget v1, p0, Lcom/google/android/apps/youtube/uilib/a/d;->b:I

    sub-int v1, p1, v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/e;->getItemViewType(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/uilib/a/d;I)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/uilib/a/d;->b:I

    sub-int v0, p1, v0

    return v0
.end method
