.class public abstract Lcom/google/android/apps/youtube/uilib/innertube/h;
.super Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;

.field private b:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/common/c/a;Ljava/lang/Object;Lcom/google/android/apps/youtube/core/aw;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/common/c/a;Ljava/lang/Object;Lcom/google/android/apps/youtube/core/aw;)V

    const-class v0, Lcom/google/android/apps/youtube/uilib/innertube/h;

    invoke-virtual {p2, p0, v0, p3}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/google/android/apps/youtube/uilib/innertube/h;->b:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/uilib/innertube/h;->a:Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;

    return-void
.end method

.method protected final b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/h;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/h;->a:Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/h;->a:Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->e()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->e()V

    goto :goto_0
.end method

.method public handleContentEvent(Lcom/google/android/apps/youtube/uilib/innertube/b;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    return-void
.end method

.method public handleErrorEvent(Lcom/google/android/apps/youtube/uilib/innertube/c;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    return-void
.end method

.method public handleLoadingEvent(Lcom/google/android/apps/youtube/uilib/innertube/d;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    return-void
.end method
