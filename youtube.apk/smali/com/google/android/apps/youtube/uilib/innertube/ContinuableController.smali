.class public abstract Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/datalib/innertube/i;

.field private final b:Lcom/google/android/apps/youtube/core/aw;

.field private final c:Lcom/google/android/apps/youtube/common/c/a;

.field private final d:Ljava/lang/Object;

.field private e:Lcom/google/a/a/a/a/li;

.field private f:Lcom/google/a/a/a/a/pr;

.field private g:Ljava/lang/String;

.field private h:Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;

.field private i:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/common/c/a;Ljava/lang/Object;Lcom/google/android/apps/youtube/core/aw;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/i;

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->a:Lcom/google/android/apps/youtube/datalib/innertube/i;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->b:Lcom/google/android/apps/youtube/core/aw;

    iput-object p3, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->d:Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/uilib/innertube/b;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/uilib/innertube/b;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->a(Ljava/lang/Object;)V

    return-void
.end method

.method private a(Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->d:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->c:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->g:Ljava/lang/String;

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->g:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->h:Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;

    new-instance v0, Lcom/google/android/apps/youtube/uilib/innertube/d;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/uilib/innertube/d;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->a:Lcom/google/android/apps/youtube/datalib/innertube/i;

    new-instance v1, Lcom/google/android/apps/youtube/uilib/innertube/a;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/youtube/uilib/innertube/a;-><init>(Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/datalib/innertube/i;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/a/l;)V

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/android/volley/VolleyError;)V
    .locals 3

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->i:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/uilib/innertube/b;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/uilib/innertube/b;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->a(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/uilib/innertube/c;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->b:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/uilib/innertube/c;-><init>(Ljava/lang/CharSequence;Z)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->a(Ljava/lang/Object;)V

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->i:Z

    goto :goto_0
.end method

.method protected final a(Lcom/google/a/a/a/a/dp;)V
    .locals 1

    iget-object v0, p1, Lcom/google/a/a/a/a/dp;->c:Lcom/google/a/a/a/a/li;

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->e:Lcom/google/a/a/a/a/li;

    iget-object v0, p1, Lcom/google/a/a/a/a/dp;->f:Lcom/google/a/a/a/a/pr;

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->f:Lcom/google/a/a/a/a/pr;

    return-void
.end method

.method protected a(Lcom/google/a/a/a/a/dq;Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->i:Z

    new-instance v0, Lcom/google/android/apps/youtube/uilib/innertube/b;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/uilib/innertube/b;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->a(Ljava/lang/Object;)V

    return-void
.end method

.method protected b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->d:Ljava/lang/Object;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->e:Lcom/google/a/a/a/a/li;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->f:Lcom/google/a/a/a/a/pr;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->e:Lcom/google/a/a/a/a/li;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->e:Lcom/google/a/a/a/a/li;

    iget-object v0, v0, Lcom/google/a/a/a/a/li;->b:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;->NEXT:Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;)V

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->f:Lcom/google/a/a/a/a/pr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->f:Lcom/google/a/a/a/a/pr;

    iget-object v0, v0, Lcom/google/a/a/a/a/pr;->b:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;->RELOAD:Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;)V

    :cond_0
    return-void
.end method

.method protected final g()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->e:Lcom/google/a/a/a/a/li;

    iput-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->f:Lcom/google/a/a/a/a/pr;

    return-void
.end method

.method protected final h()Lcom/google/android/apps/youtube/datalib/innertube/i;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->a:Lcom/google/android/apps/youtube/datalib/innertube/i;

    return-object v0
.end method

.method protected final i()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->g:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->h:Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;

    iput-object v2, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->g:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->h:Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/uilib/innertube/ContinuableController$ContinuationType;)V

    return-void
.end method
