.class final Lcom/google/android/apps/youtube/app/ui/gi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/gi;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/gi;-><init>(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gi;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->c(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gi;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->b(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    check-cast p2, Lcom/google/android/apps/youtube/core/async/Optional;

    invoke-interface {p2}, Lcom/google/android/apps/youtube/core/async/Optional;->get()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gi;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->c(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->x:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/aw;->a(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gi;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gi;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->NOT_SUBSCRIBED:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->b(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gi;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;->editUri:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gi;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gi;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->h(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gi;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->g(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/gi;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->f(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)Landroid/app/Activity;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/gh;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/gi;->a:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/youtube/app/ui/gh;-><init>(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;B)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->e(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method
