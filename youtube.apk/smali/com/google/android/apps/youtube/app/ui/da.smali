.class final Lcom/google/android/apps/youtube/app/ui/da;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/cr;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/ui/cr;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/da;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/ui/cr;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/da;-><init>(Lcom/google/android/apps/youtube/app/ui/cr;)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/da;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cr;->a(Lcom/google/android/apps/youtube/app/ui/cr;)Lcom/google/android/apps/youtube/app/ui/db;

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/da;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cr;->k(Lcom/google/android/apps/youtube/app/ui/cr;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/da;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->contentUri:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/cr;->a(Lcom/google/android/apps/youtube/app/ui/cr;Landroid/net/Uri;)Landroid/net/Uri;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/da;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/app/ui/cr;->a(Lcom/google/android/apps/youtube/app/ui/cr;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/da;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cr;->l(Lcom/google/android/apps/youtube/app/ui/cr;)Landroid/widget/ImageView;

    move-result-object v1

    iget-boolean v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->isPrivate:Z

    if-eqz v0, :cond_3

    const v0, 0x3e4ccccd    # 0.2f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(F)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/da;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cr;->m(Lcom/google/android/apps/youtube/app/ui/cr;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/da;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/app/ui/cr;->b(Lcom/google/android/apps/youtube/app/ui/cr;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/da;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cr;->a(Lcom/google/android/apps/youtube/app/ui/cr;)Lcom/google/android/apps/youtube/app/ui/db;

    return-void

    :cond_3
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method
