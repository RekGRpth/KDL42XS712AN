.class public final Lcom/google/android/apps/youtube/app/honeycomb/ui/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/remote/bi;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

.field private final b:Landroid/content/SharedPreferences;

.field private final c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

.field private final d:Landroid/os/Handler;

.field private e:Lcom/google/android/apps/youtube/app/ui/TutorialView;

.field private f:Landroid/view/View;

.field private g:Z

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->b:Landroid/content/SharedPreferences;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/ui/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/i;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/ui/g;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->d:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/ui/g;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->b:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/honeycomb/ui/g;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->c()V

    return-void
.end method

.method private c()V
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->f:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->g:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->h:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->i:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->b:Landroid/content/SharedPreferences;

    const-string v3, "show_dial_screen_tutorial"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->b:Landroid/content/SharedPreferences;

    const-string v3, "show_channel_store_turorial"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->e:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    sget v3, Lcom/google/android/youtube/l;->br:I

    invoke-virtual {v1, v3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/j;->fL:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/TutorialView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->e:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->e:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    new-instance v1, Lcom/google/android/apps/youtube/app/honeycomb/ui/h;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/h;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/ui/g;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->setDismissListener(Lcom/google/android/apps/youtube/app/ui/gz;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->e:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    sget v3, Lcom/google/android/youtube/p;->aQ:I

    invoke-virtual {v1, v3}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->e:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->setAlignTextToCling(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->e:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->f:Landroid/view/View;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->setTargetView(Landroid/view/View;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->e:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->setVisibility(I)V

    :goto_2
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->a()V

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->e:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->e:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->f:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->c()V

    return-void
.end method

.method public final synthetic a(Lcom/google/android/apps/youtube/app/remote/bg;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->c()V

    return-void
.end method

.method public final a(Z)V
    .locals 4

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->g:Z

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->b:Landroid/content/SharedPreferences;

    const-string v1, "show_dial_screen_tutorial"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->d:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->b:Landroid/content/SharedPreferences;

    const-string v1, "show_dial_screen_tutorial"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->d:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void
.end method

.method public final synthetic b(Lcom/google/android/apps/youtube/app/remote/bg;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->c()V

    return-void
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->h:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->c()V

    return-void
.end method

.method public final c(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->i:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->c()V

    return-void
.end method
