.class public final Lcom/google/android/apps/youtube/app/ui/presenter/cw;
.super Lcom/google/android/apps/youtube/uilib/a/a;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Lcom/google/android/apps/youtube/app/ui/gr;

.field private final f:Landroid/view/View;

.field private final g:Landroid/view/View;

.field private final h:Landroid/view/View;

.field private final i:Landroid/view/View;

.field private final j:Lcom/google/android/apps/youtube/app/ui/presenter/y;

.field private final k:Landroid/widget/LinearLayout;

.field private final l:Landroid/view/View;

.field private final m:Landroid/content/Context;

.field private final n:Lcom/google/android/apps/youtube/app/ui/v;

.field private o:Lcom/google/android/apps/youtube/datalib/innertube/model/as;

.field private p:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/app/ui/v;)V
    .locals 2

    invoke-direct {p0, p3}, Lcom/google/android/apps/youtube/uilib/a/a;-><init>(Lcom/google/android/apps/youtube/datalib/d/a;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->m:Landroid/content/Context;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/v;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->n:Lcom/google/android/apps/youtube/app/ui/v;

    sget v0, Lcom/google/android/youtube/l;->bw:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aP:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->z:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-direct {v1, p2, v0}, Lcom/google/android/apps/youtube/app/ui/gr;-><init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->e:Lcom/google/android/apps/youtube/app/ui/gr;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fA:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->g:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->eu:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aU:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aE:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->i:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->bn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->k:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->bm:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->l:Landroid/view/View;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/y;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a:Landroid/view/View;

    invoke-direct {v0, p2, v1}, Lcom/google/android/apps/youtube/app/ui/presenter/y;-><init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->j:Lcom/google/android/apps/youtube/app/ui/presenter/y;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->l:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/cx;

    invoke-direct {v1, p0, p3}, Lcom/google/android/apps/youtube/app/ui/presenter/cx;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/cw;Lcom/google/android/apps/youtube/datalib/d/a;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private a()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->m:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/l;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->m:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/as;)Landroid/view/View;
    .locals 12

    const/4 v11, 0x1

    const/4 v10, -0x1

    const/4 v7, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/uilib/a/a;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/u;)Landroid/view/View;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->o:Lcom/google/android/apps/youtube/datalib/innertube/model/as;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->j:Lcom/google/android/apps/youtube/app/ui/presenter/y;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/y;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/i;)Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a()I

    move-result v2

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->p:I

    if-eq v2, v0, :cond_0

    iput v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->p:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->m:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->h:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    if-ne v2, v9, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->g:Landroid/view/View;

    sget v4, Lcom/google/android/youtube/g;->K:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->g:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingTop()I

    move-result v5

    sget v6, Lcom/google/android/youtube/g;->L:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    sget v7, Lcom/google/android/youtube/g;->J:I

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->h:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->h:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingLeft()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->h:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->h:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v2, v4, v8, v5, v6}, Landroid/view/View;->setPadding(IIII)V

    iput v8, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    sget v2, Lcom/google/android/youtube/k;->l:I

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iput v8, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    sget v0, Lcom/google/android/youtube/k;->k:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->f:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/as;->f()Lcom/google/android/apps/youtube/datalib/innertube/model/au;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->b:Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->d:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a()I

    move-result v0

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->d()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->g()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v2, :cond_2

    move-object v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->f()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->c:Landroid/widget/TextView;

    sget v1, Lcom/google/android/youtube/p;->cl:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->e:Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->b()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/gr;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->i:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->n:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->i:Landroid/view/View;

    invoke-static {v0, v1, p2}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/ui/v;Landroid/view/View;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a:Landroid/view/View;

    return-object v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v11}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->g:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->g:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    invoke-virtual {v2, v8, v4, v8, v8}, Landroid/view/View;->setPadding(IIII)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->h:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->h:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingLeft()I

    move-result v4

    sget v5, Lcom/google/android/youtube/g;->I:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->h:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->h:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v2, v4, v3, v5, v6}, Landroid/view/View;->setPadding(IIII)V

    iput v10, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iput v10, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    iput v7, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_2
    if-nez v1, :cond_3

    move-object v0, v2

    goto :goto_1

    :cond_3
    if-ne v0, v9, :cond_4

    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/CharSequence;

    aput-object v2, v5, v8

    aput-object v0, v5, v11

    aput-object v1, v5, v9

    invoke-static {v5}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    :cond_4
    const-string v0, " \u00b7 "

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->c:Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/au;->f()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/presenter/cw;)Lcom/google/android/apps/youtube/datalib/innertube/model/as;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->o:Lcom/google/android/apps/youtube/datalib/innertube/model/as;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/u;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/as;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/as;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/as;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/cw;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/as;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
