.class public final Lcom/google/android/apps/youtube/app/adapter/ad;
.super Lcom/google/android/apps/youtube/app/adapter/at;
.source "SourceFile"


# instance fields
.field private final b:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    const/4 v1, 0x2

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/at;-><init>(Landroid/content/Context;I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/app/adapter/ad;->b:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/adapter/ad;->d:I

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    iput v1, p0, Lcom/google/android/apps/youtube/app/adapter/ad;->e:I

    :goto_0
    add-int/lit8 v1, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/adapter/ad;->f:I

    iput v1, p0, Lcom/google/android/apps/youtube/app/adapter/ad;->g:I

    return-void

    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/adapter/ad;->e:I

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/adapter/ad;->g:I

    if-lt p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/adapter/ad;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/adapter/ad;->g:I

    sub-int v0, p1, v0

    invoke-super {p0, v0}, Lcom/google/android/apps/youtube/app/adapter/at;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    goto :goto_0
.end method

.method public final b(I)Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/adapter/ad;->f:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/adapter/ad;->d:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(I)Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/adapter/ad;->b:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(I)Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/adapter/ad;->e:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/adapter/at;->getCount()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/adapter/ad;->g:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ad;->a(I)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/ad;->a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/au;

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/adapter/au;->c:Landroid/view/View;

    if-nez p1, :cond_0

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    iget v1, p0, Lcom/google/android/apps/youtube/app/adapter/ad;->g:I

    if-lt p1, v1, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/app/adapter/ad;->g:I

    sub-int v0, p1, v0

    invoke-super {p0, v0}, Lcom/google/android/apps/youtube/app/adapter/at;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    invoke-virtual {p0, v0, v2, p3}, Lcom/google/android/apps/youtube/app/adapter/ad;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ad;->d(I)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/adapter/au;->a:Landroid/widget/TextView;

    sget v3, Lcom/google/android/youtube/p;->aj:I

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/adapter/au;->b:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/h;->K:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_2
    :goto_2
    move-object v0, v2

    goto :goto_1

    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ad;->c(I)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/adapter/au;->a:Landroid/widget/TextView;

    sget v3, Lcom/google/android/youtube/p;->ad:I

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/adapter/au;->b:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/h;->H:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ad;->e(I)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/adapter/au;->a:Landroid/widget/TextView;

    sget v3, Lcom/google/android/youtube/p;->ch:I

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/adapter/au;->b:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/h;->I:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ad;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/adapter/au;->a:Landroid/widget/TextView;

    sget v3, Lcom/google/android/youtube/p;->aD:I

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/adapter/au;->b:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/h;->G:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2
.end method
