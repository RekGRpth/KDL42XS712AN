.class final Lcom/google/android/apps/youtube/app/b/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/client/a/d;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/b/e;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/b/e;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/b/i;->a:Lcom/google/android/apps/youtube/app/b/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/util/List;
    .locals 4

    check-cast p1, Lcom/google/android/apps/youtube/medialib/player/l;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    new-instance v1, Landroid/util/Pair;

    const-string v2, "fmt"

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/medialib/player/l;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Landroid/util/Pair;

    const-string v2, "mod_local"

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/medialib/player/l;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->isLocal()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
