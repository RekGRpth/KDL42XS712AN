.class public final Lcom/google/android/apps/youtube/app/ui/es;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/remote/aw;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/Analytics;

.field private final b:Landroid/widget/Toast;

.field private final c:Landroid/widget/ProgressBar;

.field private final d:Landroid/widget/ImageView;

.field private final e:I

.field private f:I

.field private g:Lcom/google/android/apps/youtube/app/remote/RemoteControl;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/Analytics;I)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/google/android/apps/youtube/app/ui/es;->f:I

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->a:Lcom/google/android/apps/youtube/core/Analytics;

    if-lez p3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "stepPercent must be strictly positive"

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    mul-int/lit8 v0, p3, 0x64

    div-int/lit8 v0, v0, 0x64

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->e:I

    new-instance v0, Landroid/widget/Toast;

    invoke-direct {v0, p1}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->b:Landroid/widget/Toast;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->bB:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->b:Landroid/widget/Toast;

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->b:Landroid/widget/Toast;

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->b:Landroid/widget/Toast;

    const/16 v3, 0x30

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/youtube/g;->ag:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v0, v3, v2, v4}, Landroid/widget/Toast;->setGravity(III)V

    sget v0, Lcom/google/android/youtube/j;->gh:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->d:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/youtube/j;->et:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->c:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->c:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->g:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->g:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b(Lcom/google/android/apps/youtube/app/remote/aw;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->g:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/es;->a()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->g:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->g:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0, p0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->a(Lcom/google/android/apps/youtube/app/remote/aw;)V

    return-void
.end method

.method public final b(I)Z
    .locals 4

    const/16 v3, 0x19

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/es;->g:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/es;->g:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eq p1, v3, :cond_2

    const/16 v1, 0x18

    if-ne p1, v1, :cond_0

    :cond_2
    if-ne p1, v3, :cond_3

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/es;->f:I

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/es;->e:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/es;->f:I

    :goto_1
    const/16 v1, 0x64

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/es;->g:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->s()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/es;->f:I

    add-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/es;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->d:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/h;->p:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->b:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/es;->f:I

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/es;->e:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/es;->f:I

    goto :goto_1
.end method

.method public final c(I)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/es;->g:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/es;->g:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0x19

    if-eq p1, v1, :cond_2

    const/16 v1, 0x18

    if-ne p1, v1, :cond_0

    :cond_2
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/es;->f:I

    if-lez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/es;->a:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v2, "RemoteVolumeUp"

    invoke-interface {v1, v2}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/es;->g:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/es;->f:I

    invoke-interface {v1, v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b(I)V

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/es;->f:I

    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/es;->f:I

    if-gez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/es;->a:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v2, "RemoteVolumeDown"

    invoke-interface {v1, v2}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    goto :goto_1
.end method
