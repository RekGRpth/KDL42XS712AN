.class final Lcom/google/android/apps/youtube/app/ui/bw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/bv;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/ui/bv;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/bw;->a:Lcom/google/android/apps/youtube/app/ui/bv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bw;->a:Lcom/google/android/apps/youtube/app/ui/bv;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/bv;->a(Lcom/google/android/apps/youtube/app/ui/bv;)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bw;->a:Lcom/google/android/apps/youtube/app/ui/bv;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/bv;->b(Lcom/google/android/apps/youtube/app/ui/bv;)Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->dg:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/bw;->a:Lcom/google/android/apps/youtube/app/ui/bv;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/bv;->c(Lcom/google/android/apps/youtube/app/ui/bv;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/bw;->a:Lcom/google/android/apps/youtube/app/ui/bv;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/bv;->d(Lcom/google/android/apps/youtube/app/ui/bv;)Lcom/google/android/apps/youtube/app/offline/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/offline/p;->a(Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/bw;->a:Lcom/google/android/apps/youtube/app/ui/bv;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/bv;->e(Lcom/google/android/apps/youtube/app/ui/bv;)Lcom/google/android/apps/youtube/app/ui/bx;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/app/ui/bx;->a(Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bw;->a:Lcom/google/android/apps/youtube/app/ui/bv;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/bv;->f(Lcom/google/android/apps/youtube/app/ui/bv;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    goto :goto_0
.end method
