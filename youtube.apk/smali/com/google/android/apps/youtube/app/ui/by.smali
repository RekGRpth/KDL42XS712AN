.class final Lcom/google/android/apps/youtube/app/ui/by;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/bv;

.field private final b:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

.field private final c:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

.field private final d:Landroid/view/View;

.field private final e:Landroid/view/View;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Z


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/ui/bv;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/by;->a:Lcom/google/android/apps/youtube/app/ui/bv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    invoke-static {p2}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getOfflineStreamQualityForFormatType(Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->c:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->d:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->eV:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->e:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->e:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->eW:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->e:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->eX:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->g:Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/by;->a(Z)V

    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/ui/bv;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;Landroid/view/View;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/ui/by;-><init>(Lcom/google/android/apps/youtube/app/ui/bv;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/by;)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->c:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/by;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->f:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->g:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/by;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/by;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->h:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/h;->r:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->h:Landroid/graphics/drawable/Drawable;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->e:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/by;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/by;->i:Z

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/by;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/by;)Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/by;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/by;->c:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getSettingStringId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/ui/by;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->i:Z

    return v0
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/by;->a:Lcom/google/android/apps/youtube/app/ui/bv;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/by;->c:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/bv;->a(Lcom/google/android/apps/youtube/app/ui/bv;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V

    return-void
.end method
