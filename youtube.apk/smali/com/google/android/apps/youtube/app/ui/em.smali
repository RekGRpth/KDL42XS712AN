.class final Lcom/google/android/apps/youtube/app/ui/em;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/ei;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/ui/ei;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/em;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/em;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->j(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/em;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->j(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/em;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->u()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/em;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/em;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->k(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "RemoteQueueRemoveVideoWatchPage"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/em;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/em;->a:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->k(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "RemoteQueueAddVideo"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
