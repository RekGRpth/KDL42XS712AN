.class public Lcom/google/android/apps/youtube/app/d/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field private final a:Landroid/view/animation/Animation;

.field private final b:Landroid/widget/ImageView;

.field private final c:Landroid/content/res/Resources;

.field private final d:Lcom/google/android/apps/youtube/app/d/e;

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/ImageView;Lcom/google/android/apps/youtube/app/d/e;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->c:Landroid/content/res/Resources;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/d/c;->b:Landroid/widget/ImageView;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/d/c;->d:Lcom/google/android/apps/youtube/app/d/e;

    const/high16 v0, 0x10a0000    # android.R.anim.fade_in

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->a:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->a:Landroid/view/animation/Animation;

    new-instance v1, Lcom/google/android/apps/youtube/app/d/d;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/app/d/d;-><init>(Lcom/google/android/apps/youtube/app/d/c;B)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->c:Landroid/content/res/Resources;

    const/high16 v1, 0x10e0000    # android.R.integer.config_shortAnimTime

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/d/c;->e:I

    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    sget v0, Lcom/google/android/youtube/j;->E:I

    invoke-virtual {p2, v0, p0}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->b:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/j;->E:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->d:Lcom/google/android/apps/youtube/app/d/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->d:Lcom/google/android/apps/youtube/app/d/e;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/c;->b:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/d/e;->a(Landroid/widget/ImageView;)V

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;Landroid/graphics/Bitmap;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->b:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/j;->E:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->d:Lcom/google/android/apps/youtube/app/d/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->d:Lcom/google/android/apps/youtube/app/d/e;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/c;->b:Landroid/widget/ImageView;

    invoke-interface {v0, p2}, Lcom/google/android/apps/youtube/app/d/e;->a(Landroid/graphics/Bitmap;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->b:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/j;->F:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/d/c;->a()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->b:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/j;->F:I

    invoke-virtual {v0, v1, p1}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->a:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->a:Landroid/view/animation/Animation;

    iget v1, p0, Lcom/google/android/apps/youtube/app/d/c;->e:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->b:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/c;->a:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->b:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/j;->E:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->d:Lcom/google/android/apps/youtube/app/d/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/c;->d:Lcom/google/android/apps/youtube/app/d/e;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/c;->b:Landroid/widget/ImageView;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/d/e;->a()V

    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/net/Uri;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/app/d/c;->a(Landroid/net/Uri;Landroid/graphics/Bitmap;)V

    return-void
.end method
