.class public final Lcom/google/android/apps/youtube/app/adapter/bb;
.super Lcom/google/android/apps/youtube/app/adapter/g;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/adapter/ai;

.field private final b:Lcom/google/android/apps/youtube/app/adapter/ay;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/adapter/ai;Lcom/google/android/apps/youtube/app/adapter/ay;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/adapter/g;-><init>()V

    const-string v0, "videoRendererFactory cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/ai;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bb;->a:Lcom/google/android/apps/youtube/app/adapter/ai;

    const-string v0, "transferRendererFactory cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/ay;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bb;->b:Lcom/google/android/apps/youtube/app/adapter/ay;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/youtube/app/adapter/ae;
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/bc;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/bb;->a:Lcom/google/android/apps/youtube/app/adapter/ai;

    invoke-interface {v1, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/ai;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/youtube/app/adapter/ae;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/bb;->b:Lcom/google/android/apps/youtube/app/adapter/ay;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/ay;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/youtube/app/adapter/ae;

    move-result-object v5

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/adapter/bc;-><init>(Lcom/google/android/apps/youtube/app/adapter/bb;Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/apps/youtube/app/adapter/ae;Lcom/google/android/apps/youtube/app/adapter/ae;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bb;->b:Lcom/google/android/apps/youtube/app/adapter/ay;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/ay;->a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Z

    move-result v0

    return v0
.end method
