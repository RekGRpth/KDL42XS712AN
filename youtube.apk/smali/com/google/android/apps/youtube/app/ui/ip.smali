.class final Lcom/google/android/apps/youtube/app/ui/ip;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/is;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/ij;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Lcom/google/android/apps/youtube/app/ui/presenter/ax;

.field private final h:Landroid/widget/TextView;

.field private final i:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/ij;Landroid/view/View;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ip;->a:Lcom/google/android/apps/youtube/app/ui/ij;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->b:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->bD:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    sget v0, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->c:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->cg:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->d:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->aM:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->e:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->cN:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->f:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/ij;->a(Lcom/google/android/apps/youtube/app/ui/ij;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/ax;

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/ij;->b(Lcom/google/android/apps/youtube/app/ui/ij;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-direct {v0, p2, v2}, Lcom/google/android/apps/youtube/app/ui/presenter/ax;-><init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->g:Lcom/google/android/apps/youtube/app/ui/presenter/ax;

    :goto_0
    sget v0, Lcom/google/android/youtube/j;->aH:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->h:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->ba:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->i:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->d:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/ij;->c(Lcom/google/android/apps/youtube/app/ui/ij;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->e:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/ij;->d(Lcom/google/android/apps/youtube/app/ui/ij;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/iq;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/youtube/app/ui/iq;-><init>(Lcom/google/android/apps/youtube/app/ui/ip;Lcom/google/android/apps/youtube/app/ui/ij;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->g:Lcom/google/android/apps/youtube/app/ui/presenter/ax;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/ip;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/ip;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->e:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->g:Lcom/google/android/apps/youtube/app/ui/presenter/ax;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->g:Lcom/google/android/apps/youtube/app/ui/presenter/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/presenter/ax;->d()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->a:Lcom/google/android/apps/youtube/app/ui/ij;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ip;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ij;->c(Landroid/view/View;)V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method public final a(ZLcom/google/android/apps/youtube/datalib/legacy/model/x;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->g:Lcom/google/android/apps/youtube/app/ui/presenter/ax;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->g:Lcom/google/android/apps/youtube/app/ui/presenter/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/presenter/ax;->a()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->g:Lcom/google/android/apps/youtube/app/ui/presenter/ax;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/ax;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)Landroid/view/View;

    goto :goto_0
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->a:Lcom/google/android/apps/youtube/app/ui/ij;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ip;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ij;->c(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->h:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ip;->h:Landroid/widget/TextView;

    if-eqz p1, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ip;->i:Landroid/widget/ImageView;

    if-eqz p1, :cond_3

    sget v0, Lcom/google/android/youtube/h;->e:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ip;->a:Lcom/google/android/apps/youtube/app/ui/ij;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ip;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ij;->c(Landroid/view/View;)V

    :cond_1
    return-void

    :cond_2
    const/16 v0, 0x8

    goto :goto_0

    :cond_3
    sget v0, Lcom/google/android/youtube/h;->d:I

    goto :goto_1
.end method
