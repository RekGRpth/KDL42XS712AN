.class public final Lcom/google/android/apps/youtube/app/ui/gr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/client/bj;

.field private final b:Landroid/widget/ImageView;

.field private c:Landroid/os/Handler;

.field private final d:Ljava/lang/Runnable;

.field private e:Lcom/google/android/apps/youtube/app/d/e;

.field private f:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/widget/ImageView;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gr;->a:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gr;->b:Landroid/widget/ImageView;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/gs;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/gs;-><init>(Lcom/google/android/apps/youtube/app/ui/gr;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gr;->d:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/gr;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gr;->b:Landroid/widget/ImageView;

    return-object v0
.end method

.method private b()Landroid/os/Handler;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gr;->c:Landroid/os/Handler;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gr;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_1

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gr;->c:Landroid/os/Handler;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gr;->c:Landroid/os/Handler;

    return-object v0

    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/gr;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/gr;->c()V

    return-void
.end method

.method private c()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/gr;->b()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gr;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/gr;->b()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gr;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/gr;)V
    .locals 5

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/gr;->b()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gr;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gr;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gr;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/gr;->f:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/gr;->f:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;II)Lcom/google/android/apps/youtube/datalib/innertube/model/ao;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/gr;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/gr;->a:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->a()Landroid/net/Uri;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/gr;->b:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/gr;->e:Lcom/google/android/apps/youtube/app/d/e;

    invoke-static {v1, v2, v0, v3, v4}, Lcom/google/android/apps/youtube/app/d/a;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Landroid/net/Uri;Landroid/widget/ImageView;Lcom/google/android/apps/youtube/app/d/e;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gr;->e:Lcom/google/android/apps/youtube/app/d/e;

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gr;->b:Landroid/widget/ImageView;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/youtube/app/ui/gr;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;Lcom/google/android/apps/youtube/app/d/e;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;Lcom/google/android/apps/youtube/app/d/e;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/gr;->f:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gr;->b:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/j;->fz:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gr;->b:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/gr;->b:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/j;->fz:I

    invoke-virtual {v0, v1, p1}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    :cond_0
    if-eqz p1, :cond_1

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/gr;->e:Lcom/google/android/apps/youtube/app/d/e;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/gr;->c()V

    :cond_1
    return-void
.end method
