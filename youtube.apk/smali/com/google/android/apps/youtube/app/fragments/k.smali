.class final Lcom/google/android/apps/youtube/app/fragments/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/a/l;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/common/c/a;

.field final synthetic b:[B

.field final synthetic c:Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;Lcom/google/android/apps/youtube/common/c/a;[B)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/k;->c:Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/fragments/k;->a:Lcom/google/android/apps/youtube/common/c/a;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/fragments/k;->b:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/VolleyError;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/k;->c:Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->e(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/k;->c:Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->d(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Ljava/lang/CharSequence;Z)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 5

    check-cast p1, Lcom/google/android/apps/youtube/datalib/innertube/model/b;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/k;->a:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/app/b/p;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/app/b/p;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/k;->c:Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->e(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->cS:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/k;->c:Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->b()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/k;->c:Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->c()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/k;->b:[B

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;Ljava/lang/Object;[B)Landroid/view/View;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;Ljava/util/List;Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/k;->c:Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/b;->c()Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->d()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->b(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/k;->c:Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->e(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/k;->c:Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->F()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/k;->a:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Lcom/google/android/apps/youtube/app/b/o;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/app/b/o;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    goto :goto_0

    :cond_1
    instance-of v2, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/j;

    if-eqz v2, :cond_2

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/j;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    instance-of v2, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    if-eqz v2, :cond_3

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
