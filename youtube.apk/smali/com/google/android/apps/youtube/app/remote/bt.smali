.class final Lcom/google/android/apps/youtube/app/remote/bt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/ytremote/logic/e;


# instance fields
.field final synthetic a:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

.field final synthetic b:Lcom/google/android/apps/youtube/app/remote/bp;

.field final synthetic c:Lcom/google/android/apps/youtube/app/remote/bs;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/remote/bs;Lcom/google/android/apps/ytremote/model/YouTubeDevice;Lcom/google/android/apps/youtube/app/remote/bp;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/bt;->c:Lcom/google/android/apps/youtube/app/remote/bs;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/remote/bt;->a:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/remote/bt;->b:Lcom/google/android/apps/youtube/app/remote/bp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bt;->c:Lcom/google/android/apps/youtube/app/remote/bs;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;I)Lcom/google/android/apps/youtube/app/remote/as;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bt;->c:Lcom/google/android/apps/youtube/app/remote/bs;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not find cloud screen corresponding to DIAL device  "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/bt;->a:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bt;->c:Lcom/google/android/apps/youtube/app/remote/bs;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->x()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasDevice()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bt;->a:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bt;->c:Lcom/google/android/apps/youtube/app/remote/bs;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Connection to DIAL device "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bt;->a:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was canceled in the meantime. Will not move to error state."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bt;->c:Lcom/google/android/apps/youtube/app/remote/bs;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/remote/bk;->c(Lcom/google/android/apps/youtube/app/remote/bk;Z)Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bt;->c:Lcom/google/android/apps/youtube/app/remote/bs;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/as;)V

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bt;->c:Lcom/google/android/apps/youtube/app/remote/bs;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Successfully launched YouTube TV on DIAL device "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bt;->a:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bt;->c:Lcom/google/android/apps/youtube/app/remote/bs;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->f(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bt;->a:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/ytremote/model/CloudScreen;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bt;->c:Lcom/google/android/apps/youtube/app/remote/bs;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Found corresponding cloud screen "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for DIAL device "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bt;->a:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bt;->c:Lcom/google/android/apps/youtube/app/remote/bs;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bt;->a:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/youtube/app/remote/bs;->a(Lcom/google/android/apps/youtube/app/remote/bs;Lcom/google/android/apps/ytremote/model/YouTubeDevice;Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bt;->c:Lcom/google/android/apps/youtube/app/remote/bs;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->x()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bt;->a:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bt;->c:Lcom/google/android/apps/youtube/app/remote/bs;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Connection to DIAL device "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bt;->a:Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was canceled. Will not connect to the cloud"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bt;->c:Lcom/google/android/apps/youtube/app/remote/bs;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->c(Lcom/google/android/apps/youtube/app/remote/bk;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bt;->c:Lcom/google/android/apps/youtube/app/remote/bs;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bt;->c:Lcom/google/android/apps/youtube/app/remote/bs;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->c(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/ytremote/model/CloudScreen;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bt;->c:Lcom/google/android/apps/youtube/app/remote/bs;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bt;->b:Lcom/google/android/apps/youtube/app/remote/bp;

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/youtube/app/remote/bs;->a(Lcom/google/android/apps/youtube/app/remote/bs;Lcom/google/android/apps/ytremote/model/CloudScreen;Lcom/google/android/apps/youtube/app/remote/bp;)V

    goto :goto_0
.end method
