.class final Lcom/google/android/apps/youtube/app/fragments/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

.field final synthetic b:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/am;->b:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/fragments/am;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/am;->b:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->c(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/am;->b:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->d(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/am;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/am;->b:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->d(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/am;->b:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->e(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)Lcom/google/android/apps/youtube/app/adapter/af;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/am;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/af;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/am;->b:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->f(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/cq;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/app/ui/cq;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/am;->b:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->aM:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    return-void
.end method
