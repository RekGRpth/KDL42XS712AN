.class public final Lcom/google/android/apps/youtube/app/adapter/bd;
.super Lcom/google/android/apps/youtube/core/a/b;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final c:Landroid/view/LayoutInflater;

.field private final d:I

.field private e:Lcom/google/android/apps/youtube/app/adapter/be;

.field private f:Lcom/google/android/apps/youtube/app/adapter/bf;

.field private final g:I

.field private final h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/a/e;Landroid/view/LayoutInflater;Lcom/google/android/apps/youtube/core/a/g;II)V
    .locals 7

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/adapter/bd;-><init>(Lcom/google/android/apps/youtube/core/a/e;Landroid/view/LayoutInflater;Lcom/google/android/apps/youtube/core/a/g;III)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/youtube/core/a/e;Landroid/view/LayoutInflater;Lcom/google/android/apps/youtube/core/a/g;III)V
    .locals 1

    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/apps/youtube/core/a/b;-><init>(Lcom/google/android/apps/youtube/core/a/e;Lcom/google/android/apps/youtube/core/a/g;I)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->c:Landroid/view/LayoutInflater;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->h:I

    iput p5, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->d:I

    iput p4, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->g:I

    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    const/4 v2, 0x0

    if-eqz p2, :cond_2

    check-cast p2, Landroid/widget/LinearLayout;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/adapter/bd;->a()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    sget v0, Lcom/google/android/youtube/j;->gc:I

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/view/View;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/adapter/bd;->a()I

    move-result v0

    new-array v0, v0, [Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->gc:I

    invoke-virtual {p2, v1, v0}, Landroid/widget/LinearLayout;->setTag(ILjava/lang/Object;)V

    :cond_0
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/bd;->c(I)I

    move-result v4

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/bd;->d(I)I

    move-result v1

    sub-int v5, v1, v4

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->b:Lcom/google/android/apps/youtube/core/a/e;

    add-int v6, v4, v3

    aget-object v7, v0, v3

    invoke-virtual {v1, v6, v7, p2}, Lcom/google/android/apps/youtube/core/a/e;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v7, v2, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v7, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    if-nez v3, :cond_3

    iget v1, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->i:I

    :goto_2
    iput v1, v7, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget v1, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->j:I

    iput v1, v7, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget v1, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->g:I

    add-int/lit8 v1, v1, -0x1

    if-ne v3, v1, :cond_4

    iget v1, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->k:I

    :goto_3
    iput v1, v7, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    iget v1, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->l:I

    iput v1, v7, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v6, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    add-int v1, v4, v3

    sget v7, Lcom/google/android/youtube/j;->gb:I

    iget v8, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->h:I

    add-int/2addr v1, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v6, v7, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->f:Lcom/google/android/apps/youtube/app/adapter/bf;

    if-eqz v1, :cond_1

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v6, v1}, Landroid/view/View;->setFocusable(Z)V

    aput-object v6, v0, v3

    invoke-virtual {p2, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->c:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/youtube/l;->bz:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    move-object p2, v0

    goto/16 :goto_0

    :cond_3
    iget v1, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->d:I

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3

    :cond_5
    return-object p2
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->b:Lcom/google/android/apps/youtube/core/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/a/e;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(IIII)V
    .locals 1

    iput p1, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->i:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->j:I

    iput p3, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->k:I

    iput p4, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->l:I

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    sget v0, Lcom/google/android/youtube/j;->gd:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->e(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    sget v0, Lcom/google/android/youtube/j;->gb:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    :cond_1
    sget v1, Lcom/google/android/youtube/j;->gd:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->e:Lcom/google/android/apps/youtube/app/adapter/be;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->e:Lcom/google/android/apps/youtube/app/adapter/be;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    :cond_2
    return-void
.end method

.method public final onLongClick(Landroid/view/View;)Z
    .locals 4

    const/4 v3, 0x1

    sget v0, Lcom/google/android/youtube/j;->gd:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    sget v1, Lcom/google/android/youtube/j;->gd:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->f:Lcom/google/android/apps/youtube/app/adapter/bf;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/bd;->f:Lcom/google/android/apps/youtube/app/adapter/bf;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    goto :goto_0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    :pswitch_1
    sget v0, Lcom/google/android/youtube/j;->gd:I

    sget v1, Lcom/google/android/youtube/j;->gb:I

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    :pswitch_2
    sget v0, Lcom/google/android/youtube/j;->gd:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
