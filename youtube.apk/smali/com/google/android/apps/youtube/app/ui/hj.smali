.class public final Lcom/google/android/apps/youtube/app/ui/hj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/ad;


# instance fields
.field protected final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/core/Analytics;

.field private final c:Ljava/util/LinkedList;

.field private d:Lcom/google/android/apps/youtube/app/ui/hy;

.field private e:Lcom/google/android/apps/youtube/app/ui/ic;

.field private final f:Lcom/google/android/apps/youtube/core/client/bc;

.field private final g:Lcom/google/android/apps/youtube/core/async/af;

.field private final h:Lcom/google/android/apps/youtube/datalib/innertube/v;

.field private final i:Lcom/google/android/apps/youtube/datalib/innertube/al;

.field private final j:Lcom/google/android/apps/youtube/core/identity/ak;

.field private final k:Lcom/google/android/apps/youtube/core/identity/o;

.field private final l:Lcom/google/android/apps/youtube/core/identity/l;

.field private final m:Lcom/google/android/apps/youtube/core/identity/aa;

.field private final n:Lcom/google/android/apps/youtube/core/aw;

.field private final o:Lcom/google/android/apps/youtube/app/ui/hp;

.field private final p:Lcom/google/android/apps/youtube/app/ui/hr;

.field private final q:Lcom/google/android/apps/youtube/app/ui/bd;

.field private final r:Ljava/util/concurrent/atomic/AtomicReference;

.field private final s:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/datalib/innertube/v;Lcom/google/android/apps/youtube/datalib/innertube/al;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/core/identity/o;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/app/ui/hp;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/concurrent/Executor;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->b:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/ak;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->j:Lcom/google/android/apps/youtube/core/identity/ak;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/o;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->k:Lcom/google/android/apps/youtube/core/identity/o;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->l:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/aa;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->m:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->f:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/v;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->h:Lcom/google/android/apps/youtube/datalib/innertube/v;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/al;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->i:Lcom/google/android/apps/youtube/datalib/innertube/al;

    invoke-static {p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->n:Lcom/google/android/apps/youtube/core/aw;

    invoke-static {p12}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/hp;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->o:Lcom/google/android/apps/youtube/app/ui/hp;

    invoke-static {p14}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->s:Ljava/util/concurrent/Executor;

    invoke-static {p11}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p2}, Lcom/google/android/apps/youtube/core/client/bc;->o()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->g:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/bd;

    invoke-direct {v0, p1, p2, p10, p11}, Lcom/google/android/apps/youtube/app/ui/bd;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/c/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->q:Lcom/google/android/apps/youtube/app/ui/bd;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/hr;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/hr;-><init>(Lcom/google/android/apps/youtube/app/ui/hj;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->p:Lcom/google/android/apps/youtube/app/ui/hr;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->c:Ljava/util/LinkedList;

    iput-object p13, p0, Lcom/google/android/apps/youtube/app/ui/hj;->r:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/hj;->a()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->n:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method

.method private static a(Landroid/view/View;Z)V
    .locals 1

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/view/View;->setSelected(Z)V

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setClickable(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/hj;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/hj;Landroid/view/View;Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/hj;Ljava/lang/String;Lcom/google/android/apps/youtube/app/ui/LikeAction;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/app/ui/LikeAction;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/app/ui/ic;)V
    .locals 4

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/hj;->e:Lcom/google/android/apps/youtube/app/ui/ic;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/view/View;

    invoke-static {v1, v3}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Landroid/view/View;Z)V

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Landroid/view/View;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/android/apps/youtube/app/ui/LikeAction;)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/hj;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/android/apps/youtube/app/ui/LikeAction;->LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    if-ne p2, v2, :cond_0

    move v2, v0

    :goto_0
    sget-object v3, Lcom/google/android/apps/youtube/app/ui/LikeAction;->DISLIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    if-ne p2, v3, :cond_1

    move v3, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/view/View;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Landroid/view/View;Z)V

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Landroid/view/View;Z)V

    goto :goto_2

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v3, v1

    goto :goto_1

    :cond_2
    new-instance v0, Lcom/google/android/apps/youtube/app/ui/hz;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/apps/youtube/app/ui/hz;-><init>(Lcom/google/android/apps/youtube/app/ui/hj;Lcom/google/android/apps/youtube/app/ui/LikeAction;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/hn;->a:[I

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/app/ui/LikeAction;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_3
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hj;->h:Lcom/google/android/apps/youtube/datalib/innertube/v;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/v;->a()Lcom/google/android/apps/youtube/datalib/innertube/z;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/datalib/innertube/z;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/x;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/z;->a:[B

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/z;->a([B)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/hj;->h:Lcom/google/android/apps/youtube/datalib/innertube/v;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/v;->a(Lcom/google/android/apps/youtube/datalib/innertube/z;Lcom/google/android/apps/youtube/datalib/a/l;)V

    goto :goto_3

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hj;->h:Lcom/google/android/apps/youtube/datalib/innertube/v;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/v;->b()Lcom/google/android/apps/youtube/datalib/innertube/y;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/datalib/innertube/y;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/x;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/y;->a:[B

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/y;->a([B)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/hj;->h:Lcom/google/android/apps/youtube/datalib/innertube/v;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/v;->a(Lcom/google/android/apps/youtube/datalib/innertube/y;Lcom/google/android/apps/youtube/datalib/a/l;)V

    goto :goto_3

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hj;->h:Lcom/google/android/apps/youtube/datalib/innertube/v;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/v;->c()Lcom/google/android/apps/youtube/datalib/innertube/aa;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/datalib/innertube/aa;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/x;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/aa;->a:[B

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/aa;->a([B)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/hj;->h:Lcom/google/android/apps/youtube/datalib/innertube/v;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/v;->a(Lcom/google/android/apps/youtube/datalib/innertube/aa;Lcom/google/android/apps/youtube/datalib/a/l;)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/hj;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/hj;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/app/ui/hr;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->p:Lcom/google/android/apps/youtube/app/ui/hr;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/hj;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    return-void
.end method

.method private b(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->e:Lcom/google/android/apps/youtube/app/ui/ic;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->e:Lcom/google/android/apps/youtube/app/ui/ic;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ic;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/app/ui/hy;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->d:Lcom/google/android/apps/youtube/app/ui/hy;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/hj;)Ljava/util/LinkedList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->c:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->f:Lcom/google/android/apps/youtube/core/client/bc;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->b:Lcom/google/android/apps/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/async/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->g:Lcom/google/android/apps/youtube/core/async/af;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/identity/o;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->k:Lcom/google/android/apps/youtube/core/identity/o;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/app/ui/hp;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->o:Lcom/google/android/apps/youtube/app/ui/hp;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/app/ui/bd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->q:Lcom/google/android/apps/youtube/app/ui/bd;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/apps/youtube/app/ui/hj;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->r:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/ic;)V

    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->c:Ljava/util/LinkedList;

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/ui/LikeAction;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->e:Lcom/google/android/apps/youtube/app/ui/ic;

    if-nez v0, :cond_0

    const-string v0, "Like video without action target."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->e:Lcom/google/android/apps/youtube/app/ui/ic;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ic;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hj;->b:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ui/LikeAction;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hj;->l:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/app/ui/LikeAction;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hj;->m:Lcom/google/android/apps/youtube/core/identity/aa;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/hk;

    invoke-direct {v3, p0, v0, p1}, Lcom/google/android/apps/youtube/app/ui/hk;-><init>(Lcom/google/android/apps/youtube/app/ui/hj;Ljava/lang/String;Lcom/google/android/apps/youtube/app/ui/LikeAction;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ah;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/app/ui/hy;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/hy;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->d:Lcom/google/android/apps/youtube/app/ui/hy;

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->p:Lcom/google/android/apps/youtube/app/ui/hr;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/hr;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->l:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->p:Lcom/google/android/apps/youtube/app/ui/hr;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/hr;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->m:Lcom/google/android/apps/youtube/core/identity/aa;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/hm;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/ui/hm;-><init>(Lcom/google/android/apps/youtube/app/ui/hj;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ah;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->l:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/hq;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/youtube/app/ui/hq;-><init>(Lcom/google/android/apps/youtube/app/ui/hj;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hj;->i:Lcom/google/android/apps/youtube/datalib/innertube/al;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/al;->a()Lcom/google/android/apps/youtube/datalib/innertube/an;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/an;->a:[B

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/an;->a([B)V

    iget-object v2, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/an;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/an;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/datalib/innertube/an;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/an;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/hj;->i:Lcom/google/android/apps/youtube/datalib/innertube/al;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/al;->a(Lcom/google/android/apps/youtube/datalib/innertube/an;Lcom/google/android/apps/youtube/datalib/a/l;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/ic;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/ic;-><init>(Lcom/google/android/apps/youtube/app/ui/hj;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/ic;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/app/ui/v;)V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lcom/google/android/apps/youtube/app/ui/v;->a(Z)V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/ia;

    invoke-direct {v0, p0, p3}, Lcom/google/android/apps/youtube/app/ui/ia;-><init>(Lcom/google/android/apps/youtube/app/ui/hj;Lcom/google/android/apps/youtube/app/ui/v;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hj;->i:Lcom/google/android/apps/youtube/datalib/innertube/al;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/al;->a()Lcom/google/android/apps/youtube/datalib/innertube/an;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/an;->a:[B

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/an;->a([B)V

    invoke-virtual {v1, p2}, Lcom/google/android/apps/youtube/datalib/innertube/an;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/an;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/datalib/innertube/an;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/an;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/hj;->i:Lcom/google/android/apps/youtube/datalib/innertube/al;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/al;->a(Lcom/google/android/apps/youtube/datalib/innertube/an;Lcom/google/android/apps/youtube/datalib/a/l;)V

    return-void
.end method

.method public final b()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->e:Lcom/google/android/apps/youtube/app/ui/ic;

    if-nez v0, :cond_0

    const-string v0, "Share video without action target."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->e:Lcom/google/android/apps/youtube/app/ui/ic;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ic;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hj;->e:Lcom/google/android/apps/youtube/app/ui/ic;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/ic;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/hj;->b:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v3, "Share"

    invoke-interface {v2, v3}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/e/p;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/youtube/core/utils/m;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->e:Lcom/google/android/apps/youtube/app/ui/ic;

    if-nez v0, :cond_0

    const-string v0, "Flag without action target."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->e:Lcom/google/android/apps/youtube/app/ui/ic;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ic;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hj;->b:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v2, "Flag"

    invoke-interface {v1, v2}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/hw;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/app/ui/hw;-><init>(Lcom/google/android/apps/youtube/app/ui/hj;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->l:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/hw;->a()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->m:Lcom/google/android/apps/youtube/core/identity/aa;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/hl;

    invoke-direct {v3, p0, v1}, Lcom/google/android/apps/youtube/app/ui/hl;-><init>(Lcom/google/android/apps/youtube/app/ui/hj;Lcom/google/android/apps/youtube/app/ui/hw;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ah;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->e:Lcom/google/android/apps/youtube/app/ui/ic;

    if-nez v0, :cond_0

    const-string v0, "Add to without action target."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hj;->e:Lcom/google/android/apps/youtube/app/ui/ic;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ic;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
