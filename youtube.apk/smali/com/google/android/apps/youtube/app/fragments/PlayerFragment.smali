.class public Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/KeyEvent$Callback;
.implements Lcom/google/android/apps/youtube/app/honeycomb/d;
.implements Lcom/google/android/apps/youtube/app/remote/am;
.implements Lcom/google/android/apps/youtube/app/remote/ap;
.implements Lcom/google/android/apps/youtube/app/ui/dn;
.implements Lcom/google/android/apps/youtube/app/ui/ep;
.implements Lcom/google/android/apps/youtube/core/player/overlay/ac;


# instance fields
.field private Y:Lcom/google/android/apps/youtube/core/player/overlay/c;

.field private Z:Lcom/google/android/apps/youtube/core/player/overlay/bo;

.field private a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

.field private aA:Lcom/google/android/apps/youtube/app/remote/aj;

.field private aB:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;

.field private aC:Lcom/google/android/apps/youtube/app/m2ts/TextOverlay;

.field private aD:Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

.field private aE:Ljava/lang/String;

.field private aa:Lcom/google/android/apps/youtube/core/player/overlay/am;

.field private ab:Lcom/google/android/apps/youtube/core/player/overlay/i;

.field private ac:Lcom/google/android/apps/youtube/core/player/overlay/bf;

.field private ad:Lcom/google/android/apps/youtube/core/player/overlay/bs;

.field private ae:Landroid/content/SharedPreferences;

.field private af:Lcom/google/android/apps/youtube/core/player/w;

.field private ag:Lcom/google/android/apps/youtube/core/player/z;

.field private ah:Lcom/google/android/apps/youtube/core/Analytics;

.field private ai:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

.field private aj:Z

.field private ak:Z

.field private al:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private am:Landroid/widget/Toast;

.field private an:Lcom/google/android/apps/youtube/core/client/bc;

.field private ao:Lcom/google/android/apps/youtube/app/remote/an;

.field private ap:Lcom/google/android/apps/youtube/app/remote/bk;

.field private aq:Lcom/google/android/apps/youtube/app/ui/ei;

.field private ar:Lcom/google/android/apps/youtube/app/YouTubeApplication;

.field private as:Lcom/google/android/apps/youtube/app/ax;

.field private at:Lcom/google/android/apps/youtube/core/player/ae;

.field private au:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

.field private av:I

.field private aw:I

.field private ax:I

.field private ay:I

.field private az:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

.field private b:Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;

.field private c:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

.field private d:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;

.field private e:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field private f:Lcom/google/android/apps/youtube/core/aw;

.field private g:Lcom/google/android/apps/youtube/core/player/PlayerView;

.field private h:Lcom/google/android/apps/youtube/core/player/overlay/q;

.field private i:Lcom/google/android/apps/youtube/core/player/overlay/az;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private P()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->c:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aD:Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    return-void
.end method

.method private Q()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ei;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V

    return-void
.end method

.method private R()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->U()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ao:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/an;->e()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setHasAudioOnly(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private S()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ak:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ae;->d(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->D()V

    return-void
.end method

.method private T()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ar:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->w()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ae:Landroid/content/SharedPreferences;

    const-string v2, "background_audio_enabled"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private U()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->T()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;)Lcom/google/android/apps/youtube/app/WatchWhileActivity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    return-object v0
.end method

.method private a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Z)V
    .locals 2

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ar:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->e()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ei;->c(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getWatchFeature()Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->e:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->e:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ei;->a(Lcom/google/android/apps/youtube/core/client/WatchFeature;)V

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aD:Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aD:Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ae;->a(Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;)V

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aD:Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aj:Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->c(Z)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/ae;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->v()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;)Lcom/google/android/apps/youtube/app/remote/bk;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ap:Lcom/google/android/apps/youtube/app/remote/bk;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;)Lcom/google/android/apps/youtube/app/ui/ei;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->f:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;)Lcom/google/android/apps/youtube/app/remote/an;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ao:Lcom/google/android/apps/youtube/app/remote/an;

    return-object v0
.end method

.method private handlePlaybackServiceException(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/bd;->a:[I

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->reason:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->gm:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/ei;->b(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private handleSequencerEndedEvent(Lcom/google/android/apps/youtube/core/player/event/t;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->S()V

    return-void
.end method

.method private handleSequencerStageEvent(Lcom/google/android/apps/youtube/core/player/event/v;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/v;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/v;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/v;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ao:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/an;->e()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->an:Lcom/google/android/apps/youtube/core/client/bc;

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/bc;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/fragments/bc;-><init>(Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;)V

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/youtube/core/client/bc;->l(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aE:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aE:Ljava/lang/String;

    new-instance v1, Lcom/google/android/apps/ytremote/model/SsdpId;

    invoke-direct {v1, v0}, Lcom/google/android/apps/ytremote/model/SsdpId;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->au:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/az;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/fragments/az;-><init>(Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/ytremote/model/SsdpId;Lcom/google/android/apps/youtube/app/remote/ck;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aE:Ljava/lang/String;

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->R()V

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/ei;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/v;->a()Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->SEQUENCE_EMPTY:Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->S()V

    goto :goto_1
.end method

.method private handleUnplayableVideoSkipped(Lcom/google/android/apps/youtube/core/player/event/y;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->am:Landroid/widget/Toast;

    sget v1, Lcom/google/android/youtube/p;->dP:I

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->am:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private handleVideoFullscreenEvent(Lcom/google/android/apps/youtube/core/player/event/ab;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ak:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ab;->a()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->c(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ab;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e(Z)V

    :cond_0
    return-void
.end method

.method private handleVideoSyncToAudioEvent(Lcom/google/android/apps/youtube/core/player/event/ad;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aC:Lcom/google/android/apps/youtube/app/m2ts/TextOverlay;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ad;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/m2ts/TextOverlay;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final E()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->d:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->d:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;->pop()Lcom/google/android/apps/youtube/app/fragments/navigation/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/ae;->E()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->P()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/navigation/a;->b:Landroid/os/Parcelable;

    check-cast v0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/ae;->a(Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final F()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ak:Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->c(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->E()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Q()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->b:Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->d:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;->popAll()Lcom/google/android/apps/youtube/app/fragments/navigation/a;

    return-void
.end method

.method public final G()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a()V

    return-void
.end method

.method public final H()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->C()V

    return-void
.end method

.method public final I()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->r()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final J()Lcom/google/android/apps/youtube/core/player/ae;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    return-object v0
.end method

.method public final K()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->t()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->e()I

    move-result v0

    goto :goto_0
.end method

.method public final L()Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ac:Lcom/google/android/apps/youtube/core/player/overlay/bf;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/bf;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v0

    return-object v0
.end method

.method public final M()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final N()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    return-object v0
.end method

.method public final O()Lcom/google/android/apps/youtube/core/player/PlayerView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->g:Lcom/google/android/apps/youtube/core/player/PlayerView;

    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 24

    sget v2, Lcom/google/android/youtube/l;->aF:I

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v19

    sget v2, Lcom/google/android/youtube/j;->gq:I

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/core/player/PlayerView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->g:Lcom/google/android/apps/youtube/core/player/PlayerView;

    new-instance v2, Lcom/google/android/apps/youtube/app/m2ts/TextOverlay;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v2, v3}, Lcom/google/android/apps/youtube/app/m2ts/TextOverlay;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aC:Lcom/google/android/apps/youtube/app/m2ts/TextOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aC:Lcom/google/android/apps/youtube/app/m2ts/TextOverlay;

    sget v3, Lcom/google/android/youtube/p;->fY:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/app/m2ts/TextOverlay;->setText(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->E()Lcom/google/android/apps/youtube/core/client/AdsClient;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->M()Lcom/google/android/apps/youtube/core/client/ce;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->b(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/common/c/a;)Lcom/google/android/apps/youtube/app/navigation/AppNavigator;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aa()Lcom/google/android/apps/youtube/core/player/w;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->af:Lcom/google/android/apps/youtube/core/player/w;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/z;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v2, v3}, Lcom/google/android/apps/youtube/core/player/z;-><init>(Landroid/app/Activity;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ag:Lcom/google/android/apps/youtube/core/player/z;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ar:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->b()Lcom/google/android/apps/youtube/app/aw;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/aw;->t()Z

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setShowAudioOnly(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setUiListener(Lcom/google/android/apps/youtube/core/player/overlay/ac;)V

    new-instance v3, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ah:Lcom/google/android/apps/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->h()I

    move-result v5

    invoke-direct {v3, v2, v4, v5}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultAdOverlay;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/Analytics;I)V

    new-instance v12, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v12, v2}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Q()Lcom/google/android/apps/youtube/app/honeycomb/phone/i;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->e()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a(Landroid/view/View;)V

    new-instance v21, Lcom/google/android/apps/youtube/app/player/RobotoAnnotationOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, v21

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/app/player/RobotoAnnotationOverlay;-><init>(Landroid/content/Context;)V

    new-instance v16, Lcom/google/android/apps/youtube/core/player/overlay/DefaultLiveOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultLiveOverlay;-><init>(Landroid/content/Context;)V

    new-instance v22, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultSubtitlesOverlay;-><init>(Landroid/content/Context;)V

    new-instance v23, Lcom/google/android/apps/youtube/core/player/overlay/DefaultThumbnailOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ar:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultThumbnailOverlay;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->g:Lcom/google/android/apps/youtube/core/player/PlayerView;

    const/16 v4, 0x8

    new-array v4, v4, [Lcom/google/android/apps/youtube/core/player/overlay/ax;

    const/4 v5, 0x0

    aput-object v23, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aC:Lcom/google/android/apps/youtube/app/m2ts/TextOverlay;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object v22, v4, v5

    const/4 v5, 0x3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    aput-object v6, v4, v5

    const/4 v5, 0x4

    aput-object v21, v4, v5

    const/4 v5, 0x5

    aput-object v3, v4, v5

    const/4 v5, 0x6

    aput-object v12, v4, v5

    const/4 v5, 0x7

    aput-object v16, v4, v5

    invoke-virtual {v2, v4}, Lcom/google/android/apps/youtube/core/player/PlayerView;->a([Lcom/google/android/apps/youtube/core/player/overlay/ax;)V

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/q;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-direct {v2, v4, v5, v6}, Lcom/google/android/apps/youtube/core/player/overlay/q;-><init>(Lcom/google/android/apps/youtube/core/player/ae;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->h:Lcom/google/android/apps/youtube/core/player/overlay/q;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/az;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->g:Lcom/google/android/apps/youtube/core/player/PlayerView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-direct {v2, v4, v5}, Lcom/google/android/apps/youtube/core/player/overlay/az;-><init>(Lcom/google/android/apps/youtube/core/player/am;Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->i:Lcom/google/android/apps/youtube/core/player/overlay/az;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/c;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->an:Lcom/google/android/apps/youtube/core/client/bc;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ah:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/youtube/core/player/overlay/c;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/a;Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/ae;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/datalib/d/a;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Y:Lcom/google/android/apps/youtube/core/player/overlay/c;

    new-instance v10, Lcom/google/android/apps/youtube/core/player/overlay/bo;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/ae;->a()Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aG()Lcom/google/android/apps/youtube/common/e/n;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/ae;->b()Lcom/google/android/apps/youtube/core/player/au;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->g:Lcom/google/android/apps/youtube/core/player/PlayerView;

    invoke-direct/range {v10 .. v15}, Lcom/google/android/apps/youtube/core/player/overlay/bo;-><init>(Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;Lcom/google/android/apps/youtube/core/player/overlay/bm;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/player/au;Lcom/google/android/apps/youtube/core/player/am;)V

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lcom/google/android/apps/youtube/core/player/overlay/bo;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/am;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    move-object/from16 v0, v16

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/youtube/core/player/overlay/am;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/ak;Lcom/google/android/apps/youtube/core/player/ae;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcom/google/android/apps/youtube/core/player/overlay/am;

    new-instance v10, Lcom/google/android/apps/youtube/core/player/overlay/i;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->g:Lcom/google/android/apps/youtube/core/player/PlayerView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->as()Lcom/google/android/apps/youtube/datalib/innertube/t;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->k()Landroid/support/v4/app/l;

    move-result-object v18

    move-object/from16 v12, v21

    move-object v13, v7

    move-object v14, v9

    invoke-direct/range {v10 .. v18}, Lcom/google/android/apps/youtube/core/player/overlay/i;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/overlay/g;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/core/player/am;Lcom/google/android/apps/youtube/core/player/ae;Lcom/google/android/apps/youtube/datalib/innertube/t;Landroid/support/v4/app/l;)V

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ab:Lcom/google/android/apps/youtube/core/player/overlay/i;

    new-instance v8, Lcom/google/android/apps/youtube/core/player/overlay/bf;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->h:Lcom/google/android/apps/youtube/core/player/overlay/q;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ah:Lcom/google/android/apps/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ae:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v15

    move-object/from16 v9, v22

    move-object/from16 v10, v20

    invoke-direct/range {v8 .. v15}, Lcom/google/android/apps/youtube/core/player/overlay/bf;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/be;Lcom/google/android/apps/youtube/core/client/ce;Lcom/google/android/apps/youtube/core/player/overlay/q;Lcom/google/android/apps/youtube/core/Analytics;Landroid/content/SharedPreferences;Landroid/content/Context;Lcom/google/android/apps/youtube/common/c/a;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ac:Lcom/google/android/apps/youtube/core/player/overlay/bf;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/bs;

    const/4 v3, 0x0

    move-object/from16 v0, v23

    invoke-direct {v2, v0, v7, v3}, Lcom/google/android/apps/youtube/core/player/overlay/bs;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/br;Lcom/google/android/apps/youtube/core/client/bj;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/core/player/overlay/bs;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Y:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setAdActionsListener(Lcom/google/android/apps/youtube/core/player/overlay/y;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ao:Lcom/google/android/apps/youtube/app/remote/an;

    new-instance v2, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setAlwaysShowControls(Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Q()Lcom/google/android/apps/youtube/app/honeycomb/phone/i;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/app/honeycomb/phone/i;->a(Landroid/content/Context;)Landroid/support/v7/app/MediaRouteButton;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a(Landroid/view/View;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->az:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->az:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setUiListener(Lcom/google/android/apps/youtube/core/player/overlay/ac;)V

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/ei;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->ap()Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ah:Lcom/google/android/apps/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->az:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->g:Lcom/google/android/apps/youtube/core/player/PlayerView;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v14

    sget v2, Lcom/google/android/youtube/j;->ee:I

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    move-object/from16 v8, v20

    move-object/from16 v12, p0

    invoke-direct/range {v3 .. v15}, Lcom/google/android/apps/youtube/app/ui/ei;-><init>(Lcom/google/android/apps/youtube/core/player/ae;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/client/ce;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;Landroid/view/View;Lcom/google/android/apps/youtube/app/ui/ep;Lcom/google/android/apps/youtube/app/ui/eq;Lcom/google/android/apps/youtube/app/am;Landroid/view/View;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ar:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->l()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->au:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const-string v3, ""

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->am:Landroid/widget/Toast;

    return-object v19
.end method

.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->n()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ae;->g(Z)Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->playbackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Z)V

    :cond_0
    return-void
.end method

.method public final a(F)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/ae;->a(F)V

    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    check-cast p1, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ar:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ar:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ah:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aP()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ae:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->f:Lcom/google/android/apps/youtube/core/aw;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->an:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->P()Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ap:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->X()Lcom/google/android/apps/youtube/app/remote/aj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aA:Lcom/google/android/apps/youtube/app/remote/aj;

    new-instance v0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ae:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J()Lcom/google/android/apps/youtube/app/ui/hh;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;-><init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/app/ui/hh;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aB:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->ac()Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    if-eqz p1, :cond_0

    const-string v0, "watch_back_stack"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->d:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;

    const-string v0, "playback_service_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aD:Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aB:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->b(Landroid/os/Bundle;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->d:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->d:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ae;->b(I)Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    move-result-object v0

    if-eqz v0, :cond_2

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aD:Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    :cond_2
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V
    .locals 5

    const/4 v4, 0x1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->getPlaybackStartDescriptor()Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ae;->b(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->m()Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->c:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->getPlaybackStartDescriptor()Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ae;->c(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->P()V

    :goto_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->getTvId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aE:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->getPlaybackStartDescriptor()Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/ae;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->shouldKeepHistory()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/core/player/ae;->g(Z)Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->d:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/navigation/f;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/f;-><init>(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;->push(Lcom/google/android/apps/youtube/app/fragments/navigation/a;)V

    :cond_3
    :goto_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->getPlaybackStartDescriptor()Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->k()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->P()V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->d:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;->popAll()Lcom/google/android/apps/youtube/app/fragments/navigation/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->E()V

    goto :goto_2
.end method

.method public final a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;Z)V
    .locals 3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aj:Z

    iput-boolean p2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ak:Z

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    if-nez p2, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/player/ae;->c(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ae;->d(Z)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;Z)V
    .locals 4

    if-nez p1, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->s()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/ei;->u()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v2

    if-eqz v2, :cond_2

    const/4 v0, 0x0

    sget-object v3, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v2, v3, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/ei;->t()I

    move-result v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/ae;->c(I)V

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v2, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->PAUSED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v2, v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->l()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->v()Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ac:Lcom/google/android/apps/youtube/core/player/overlay/bf;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/overlay/bf;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->o()V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->af:Lcom/google/android/apps/youtube/core/player/w;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ag:Lcom/google/android/apps/youtube/core/player/z;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/w;->a(Lcom/google/android/apps/youtube/core/player/z;)V

    :cond_5
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->R()V

    return-void

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/ei;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->af:Lcom/google/android/apps/youtube/core/player/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/w;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ei;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->g:Lcom/google/android/apps/youtube/core/player/PlayerView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/PlayerView;->setMinimized(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/ae;->e(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/ei;->a(Z)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->o()Z

    return-void
.end method

.method public final b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->g:Lcom/google/android/apps/youtube/core/player/PlayerView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/PlayerView;->setSliding(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/ei;->c(Z)V

    return-void
.end method

.method public final c(Z)V
    .locals 5

    const/4 v3, -0x1

    const/4 v2, 0x0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aj:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aj:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ae;->d(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->g:Lcom/google/android/apps/youtube/core/player/PlayerView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/PlayerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aj:Z

    if-eqz v1, :cond_0

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    iput v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    iput v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->av:I

    iput v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aw:I

    iput v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ax:I

    iput v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ay:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->q()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/j;->gi:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->av:I

    iget v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aw:I

    iget v3, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ax:I

    iget v4, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ay:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->d()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aA:Lcom/google/android/apps/youtube/app/remote/aj;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/remote/aj;->a(Lcom/google/android/apps/youtube/app/remote/am;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->b:Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;->b()Lcom/google/android/apps/youtube/app/ui/hj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setMenuActionsListener(Lcom/google/android/apps/youtube/core/player/overlay/ad;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->az:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setMenuActionsListener(Lcom/google/android/apps/youtube/core/player/overlay/ad;)V

    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->d(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->k()Landroid/support/v4/app/l;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/j;->fX:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/l;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->b:Lcom/google/android/apps/youtube/app/fragments/VideoInfoFragment;

    return-void
.end method

.method public final d(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ak:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->c(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e(Z)V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Q()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aA:Lcom/google/android/apps/youtube/app/remote/aj;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/remote/aj;->b(Lcom/google/android/apps/youtube/app/remote/am;)V

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->e()V

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ae;->g(Z)Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    move-result-object v0

    const-string v1, "watch_back_stack"

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->d:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "playback_service_state"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aB:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->a(Landroid/os/Bundle;)V

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    return-void
.end method

.method public final e(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/ae;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/ae;->q()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setHasNext(Z)V

    return-void
.end method

.method public final f()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->B()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ac:Lcom/google/android/apps/youtube/core/player/overlay/bf;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/bf;->a()V

    return-void
.end method

.method public final f(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/ae;->b(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/ae;->q()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setHasNext(Z)V

    return-void
.end method

.method public final g(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ae;->c(Z)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ai:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final r()V
    .locals 3

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->r()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getTaskId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/ae;->a(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/b/w;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/app/b/w;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Y:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ab:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->h:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcom/google/android/apps/youtube/core/player/overlay/am;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ac:Lcom/google/android/apps/youtube/core/player/overlay/bf;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lcom/google/android/apps/youtube/core/player/overlay/bo;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->i:Lcom/google/android/apps/youtube/core/player/overlay/az;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/core/player/overlay/bs;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->v()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ao:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/an;->e()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/ei;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->af:Lcom/google/android/apps/youtube/core/player/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/w;->a()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->p()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->w()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->g:Lcom/google/android/apps/youtube/core/player/PlayerView;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/PlayerView;->b()Lcom/google/android/apps/youtube/medialib/player/y;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/core/player/ae;->a(Lcom/google/android/apps/youtube/medialib/player/y;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->c:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->c:Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aB:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->c()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->o()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->af:Lcom/google/android/apps/youtube/core/player/w;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ag:Lcom/google/android/apps/youtube/core/player/z;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/w;->a(Lcom/google/android/apps/youtube/core/player/z;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aD:Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aD:Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;->playbackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;Z)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final s()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->af:Lcom/google/android/apps/youtube/core/player/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/w;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->as:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Y:Lcom/google/android/apps/youtube/core/player/overlay/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ab:Lcom/google/android/apps/youtube/core/player/overlay/i;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->h:Lcom/google/android/apps/youtube/core/player/overlay/q;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aa:Lcom/google/android/apps/youtube/core/player/overlay/am;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ac:Lcom/google/android/apps/youtube/core/player/overlay/bf;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lcom/google/android/apps/youtube/core/player/overlay/bo;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->i:Lcom/google/android/apps/youtube/core/player/overlay/az;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->ad:Lcom/google/android/apps/youtube/core/player/overlay/bs;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->T()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->U()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->x()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aB:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->a()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->at:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->A()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aq:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->q()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->Z:Lcom/google/android/apps/youtube/core/player/overlay/bo;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/bo;->b()V

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->s()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aB:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->al:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->i()Lcom/google/a/a/a/a/ai;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->a(Lcom/google/a/a/a/a/ai;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->aB:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->b()V

    goto :goto_0
.end method
