.class final Lcom/google/android/apps/youtube/app/ui/hq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/a/l;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/hj;

.field private final b:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/hj;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/hq;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/hq;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/VolleyError;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hq;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->k(Lcom/google/android/apps/youtube/app/ui/hj;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/hq;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hq;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/hj;->k(Lcom/google/android/apps/youtube/app/ui/hj;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    :cond_0
    const-string v0, "Error adding video to playlist"

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hq;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hq;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->k(Lcom/google/android/apps/youtube/app/ui/hj;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hq;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hq;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hq;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    sget v2, Lcom/google/android/youtube/p;->s:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/hq;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v5, v5, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->title:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/hj;->b(Lcom/google/android/apps/youtube/app/ui/hj;Ljava/lang/String;)V

    return-void
.end method
