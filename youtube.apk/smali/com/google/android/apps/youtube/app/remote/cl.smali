.class final Lcom/google/android/apps/youtube/app/remote/cl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/remote/au;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/cl;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/cl;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/cl;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasDevice()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/cl;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->m(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/cl;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/cl;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    :cond_0
    return-void
.end method
