.class public final Lcom/google/android/apps/youtube/app/adapter/u;
.super Lcom/google/android/apps/youtube/app/adapter/af;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/google/android/apps/youtube/app/adapter/ai;Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/af;-><init>(Landroid/content/Context;ILcom/google/android/apps/youtube/app/adapter/ai;)V

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/adapter/u;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/u;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Landroid/net/Uri;)V

    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/u;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Landroid/net/Uri;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/u;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)V

    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/u;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->b(Landroid/net/Uri;)V

    return-void
.end method

.method public final notifyDataSetChanged()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/adapter/af;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/u;->a:Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;->a()V

    return-void
.end method
