.class final Lcom/google/android/apps/youtube/app/bs;
.super Lcom/google/android/apps/youtube/common/e/f;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ax;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/ax;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/common/e/f;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 22

    new-instance v1, Lcom/google/android/apps/youtube/core/player/ae;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->af()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/ax;->l(Lcom/google/android/apps/youtube/app/ax;)Lcom/google/android/apps/youtube/medialib/player/x;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->ao()Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/app/ax;->u()Lcom/google/android/apps/youtube/core/player/fetcher/e;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/app/ax;->J()Lcom/google/android/apps/youtube/core/client/e;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v10}, Lcom/google/android/apps/youtube/app/ax;->b()Lcom/google/android/apps/youtube/app/aw;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v11}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v12}, Lcom/google/android/apps/youtube/app/ax;->aJ()Ljava/util/concurrent/Executor;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v14}, Lcom/google/android/apps/youtube/app/ax;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v15}, Lcom/google/android/apps/youtube/app/ax;->m()Lcom/google/android/apps/youtube/datalib/offline/n;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/youtube/app/ax;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/youtube/app/ax;->aa()Lcom/google/android/apps/youtube/core/player/w;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/youtube/app/ax;->aP()Landroid/content/SharedPreferences;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/youtube/app/ax;->an()Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/youtube/app/ax;->ad()Lcom/google/android/apps/youtube/core/player/ad;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/bs;->a:Lcom/google/android/apps/youtube/app/ax;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/youtube/app/ax;->aF()Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;

    move-result-object v21

    invoke-direct/range {v1 .. v21}, Lcom/google/android/apps/youtube/core/player/ae;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/medialib/player/x;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/core/player/fetcher/e;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/e;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/au;Lcom/google/android/apps/youtube/core/Analytics;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/datalib/offline/n;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/player/w;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;Lcom/google/android/apps/youtube/core/player/ad;Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;)V

    return-object v1
.end method
