.class final Lcom/google/android/apps/youtube/app/fragments/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/adapter/ae;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

.field private final b:Landroid/view/View;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/f;->a:Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/fragments/f;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/f;->b:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->eu:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/f;->c:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/f;->b:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->dZ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/f;->d:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 2

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/ArtistSnippet;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/f;->c:Landroid/view/View;

    if-nez p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/f;->d:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/ArtistSnippet;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/f;->b:Landroid/view/View;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
