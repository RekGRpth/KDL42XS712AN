.class public Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;
.super Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
.source "SourceFile"


# static fields
.field public static final a:Ljava/util/Map;

.field public static final b:Ljava/util/Map;


# instance fields
.field private Y:Landroid/view/View;

.field private Z:Lcom/google/android/apps/youtube/core/ui/PagedListView;

.field private aa:Lcom/google/android/apps/youtube/app/remote/an;

.field private ab:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

.field private ac:Lcom/google/android/apps/youtube/app/ui/ie;

.field private d:Lcom/google/android/apps/youtube/app/ax;

.field private e:Landroid/content/res/Resources;

.field private f:Lcom/google/android/apps/youtube/core/aw;

.field private g:Ljava/lang/String;

.field private h:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field private i:Lcom/google/android/apps/youtube/app/ui/et;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/LinkedHashMap;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->a:Ljava/util/Map;

    const-string v1, "Autos"

    sget v2, Lcom/google/android/youtube/p;->P:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->a:Ljava/util/Map;

    const-string v1, "Comedy"

    sget v2, Lcom/google/android/youtube/p;->Q:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->a:Ljava/util/Map;

    const-string v1, "Education"

    sget v2, Lcom/google/android/youtube/p;->R:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->a:Ljava/util/Map;

    const-string v1, "Entertainment"

    sget v2, Lcom/google/android/youtube/p;->S:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->a:Ljava/util/Map;

    const-string v1, "Film"

    sget v2, Lcom/google/android/youtube/p;->T:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->a:Ljava/util/Map;

    const-string v1, "Games"

    sget v2, Lcom/google/android/youtube/p;->U:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->a:Ljava/util/Map;

    const-string v1, "Music"

    sget v2, Lcom/google/android/youtube/p;->W:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->a:Ljava/util/Map;

    const-string v1, "News"

    sget v2, Lcom/google/android/youtube/p;->X:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->a:Ljava/util/Map;

    const-string v1, "Nonprofit"

    sget v2, Lcom/google/android/youtube/p;->Y:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->a:Ljava/util/Map;

    const-string v1, "People"

    sget v2, Lcom/google/android/youtube/p;->Z:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->a:Ljava/util/Map;

    const-string v1, "Animals"

    sget v2, Lcom/google/android/youtube/p;->O:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->a:Ljava/util/Map;

    const-string v1, "Tech"

    sget v2, Lcom/google/android/youtube/p;->ab:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->a:Ljava/util/Map;

    const-string v1, "Sports"

    sget v2, Lcom/google/android/youtube/p;->aa:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->a:Ljava/util/Map;

    const-string v1, "Howto"

    sget v2, Lcom/google/android/youtube/p;->V:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->a:Ljava/util/Map;

    const-string v1, "Travel"

    sget v2, Lcom/google/android/youtube/p;->ac:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/LinkedHashMap;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->b:Ljava/util/Map;

    const-string v1, "Autos"

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_AUTOS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->b:Ljava/util/Map;

    const-string v1, "Comedy"

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_COMEDY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->b:Ljava/util/Map;

    const-string v1, "Education"

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_EDUCATION:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->b:Ljava/util/Map;

    const-string v1, "Entertainment"

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_ENTERTAINMENT:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->b:Ljava/util/Map;

    const-string v1, "Film"

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_FILM:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->b:Ljava/util/Map;

    const-string v1, "Games"

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_GAMES:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->b:Ljava/util/Map;

    const-string v1, "Music"

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_MUSIC:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->b:Ljava/util/Map;

    const-string v1, "News"

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_NEWS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->b:Ljava/util/Map;

    const-string v1, "Nonprofit"

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_NONPROFIT:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->b:Ljava/util/Map;

    const-string v1, "People"

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_PEOPLE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->b:Ljava/util/Map;

    const-string v1, "Animals"

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_ANIMALS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->b:Ljava/util/Map;

    const-string v1, "Tech"

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_SCIENCE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->b:Ljava/util/Map;

    const-string v1, "Sports"

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_SPORTS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->b:Ljava/util/Map;

    const-string v1, "Howto"

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_HOWTO:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->b:Ljava/util/Map;

    const-string v1, "Travel"

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CATEGORY_TRAVEL:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;-><init>()V

    return-void
.end method

.method private L()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->e:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/k;->m:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/et;->a(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 14

    sget v2, Lcom/google/android/youtube/l;->p:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->q()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {p1, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->Y:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->Y:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->gf:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/ui/PagedListView;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->Z:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->g:Ljava/lang/String;

    const-string v2, "Music"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->d:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->af()Landroid/content/Context;

    move-result-object v1

    const-string v2, "1001680686"

    const-string v3, "4dahCKKczAYQrt7R3QM"

    const-string v4, "<Android_YT_Music_Page>"

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/ads/conversiontracking/GoogleConversionPing;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->d:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v13

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->d:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v2

    invoke-static {v1, v2, v13}, Lcom/google/android/apps/youtube/app/adapter/ag;->b(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/af;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v2, v1}, Lcom/google/android/apps/youtube/app/ui/et;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/a/a;)Lcom/google/android/apps/youtube/app/ui/et;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/ie;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->Z:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->d:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/youtube/core/client/bc;->k()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->f:Lcom/google/android/apps/youtube/core/aw;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v8

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->h:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget-object v11, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v11}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v11

    sget-object v12, Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;->HomeFeed:Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;

    invoke-direct/range {v1 .. v12}, Lcom/google/android/apps/youtube/app/ui/ie;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;ZLcom/google/android/apps/youtube/app/am;ZLcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->ac:Lcom/google/android/apps/youtube/app/ui/ie;

    if-eqz p3, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->ac:Lcom/google/android/apps/youtube/app/ui/ie;

    const-string v2, "videos_helper"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/ie;->a(Landroid/os/Bundle;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->ac:Lcom/google/android/apps/youtube/app/ui/ie;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/apps/youtube/core/async/GDataRequest;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->d:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_POPULAR:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->g:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v7}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;->THIS_WEEK:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/ie;->a([Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->aa:Lcom/google/android/apps/youtube/app/remote/an;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->f:Lcom/google/android/apps/youtube/core/aw;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->h:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->d:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v6

    move-object v2, v13

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->ab:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->Y:Landroid/view/View;

    return-object v1
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->a:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->g:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->h()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v1, "category_term"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "categoryTerm cannot be null"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->g:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->b:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->g:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->h:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->d:Lcom/google/android/apps/youtube/app/ax;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->e:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->d:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->f:Lcom/google/android/apps/youtube/core/aw;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->d:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->aa:Lcom/google/android/apps/youtube/app/remote/an;

    return-void
.end method

.method public final d()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->d()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->L()V

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->e(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->ac:Lcom/google/android/apps/youtube/app/ui/ie;

    if-eqz v0, :cond_0

    const-string v0, "videos_helper"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->ac:Lcom/google/android/apps/youtube/app/ui/ie;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/ie;->d()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->L()V

    return-void
.end method

.method public final r()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->r()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->ab:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b()V

    return-void
.end method

.method public final t()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->t()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;->ab:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a()V

    return-void
.end method
