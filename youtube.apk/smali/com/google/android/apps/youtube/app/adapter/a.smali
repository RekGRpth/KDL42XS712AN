.class public abstract Lcom/google/android/apps/youtube/app/adapter/a;
.super Lcom/google/android/apps/youtube/app/adapter/k;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/client/bc;

.field private final b:Lcom/google/android/apps/youtube/core/client/bj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;)V
    .locals 1

    sget v0, Lcom/google/android/youtube/j;->ah:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/app/adapter/k;-><init>(Landroid/content/Context;I)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/a;->a:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/a;->b:Lcom/google/android/apps/youtube/core/client/bj;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/a;)Lcom/google/android/apps/youtube/core/client/bj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/a;->b:Lcom/google/android/apps/youtube/core/client/bj;

    return-object v0
.end method


# virtual methods
.method protected abstract a(Ljava/lang/Object;)Landroid/net/Uri;
.end method

.method protected a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/a;->a:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/a;->a(Ljava/lang/Object;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/app/adapter/b;

    invoke-direct {v2, p0, p3}, Lcom/google/android/apps/youtube/app/adapter/b;-><init>(Lcom/google/android/apps/youtube/app/adapter/a;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
