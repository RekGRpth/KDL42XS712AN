.class public final Lcom/google/android/apps/youtube/app/ui/bm;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final b:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "http://m.youtube.com/merge_identity"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/bm;->a:Landroid/net/Uri;

    const-string v0, "http://m.youtube.com/create_channel"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/bm;->b:Landroid/net/Uri;

    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/UserProfile;Lcom/google/android/apps/youtube/core/client/bc;)Landroid/app/Dialog;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p3, Lcom/google/android/apps/youtube/core/identity/UserProfile;->isLightweight:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/core/ui/aa;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/ui/aa;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/youtube/p;->ae:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/aa;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->cd:I

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/bq;

    invoke-direct {v2, p4, p0}, Lcom/google/android/apps/youtube/app/ui/bq;-><init>(Lcom/google/android/apps/youtube/core/client/bc;Landroid/app/Activity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->ao:I

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/bp;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/app/ui/bp;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p3, Lcom/google/android/apps/youtube/core/identity/UserProfile;->plusUserId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/core/ui/aa;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/core/ui/aa;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/youtube/p;->ap:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/aa;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->aq:I

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/bo;

    invoke-direct {v2, p4, p0, p1}, Lcom/google/android/apps/youtube/app/ui/bo;-><init>(Lcom/google/android/apps/youtube/core/client/bc;Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ak;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->ao:I

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/bn;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/app/ui/bn;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/bm;->a:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic b()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/bm;->b:Landroid/net/Uri;

    return-object v0
.end method
