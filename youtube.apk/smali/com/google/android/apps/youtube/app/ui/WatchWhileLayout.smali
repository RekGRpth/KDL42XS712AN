.class public Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# instance fields
.field private A:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

.field private final B:Landroid/widget/Scroller;

.field private final C:Landroid/widget/Scroller;

.field private final D:Lcom/google/android/apps/youtube/app/ui/ix;

.field private final E:Landroid/view/animation/DecelerateInterpolator;

.field private F:Lcom/google/android/apps/youtube/app/ui/iw;

.field private final G:Landroid/graphics/Paint;

.field private final H:Landroid/graphics/drawable/Drawable;

.field private final I:Landroid/graphics/drawable/Drawable;

.field private final J:I

.field private K:Landroid/graphics/Rect;

.field private L:Landroid/graphics/Rect;

.field private M:Z

.field private N:Z

.field private final O:Lcom/google/android/apps/youtube/app/ui/iy;

.field private final P:Lcom/google/android/apps/youtube/core/Analytics;

.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Ljava/util/LinkedList;

.field private l:I

.field private m:I

.field private final n:I

.field private final o:I

.field private p:Z

.field private q:I

.field private r:I

.field private s:I

.field private final t:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

.field private u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

.field private final v:Landroid/graphics/Rect;

.field private final w:Landroid/graphics/Rect;

.field private final x:Landroid/graphics/Rect;

.field private y:F

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->N:Z

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    new-instance v0, Landroid/widget/Scroller;

    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-direct {v0, p1, v4}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/widget/Scroller;

    new-instance v0, Landroid/widget/Scroller;

    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-direct {v0, p1, v4}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/widget/Scroller;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/ix;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/youtube/app/ui/ix;-><init>(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:Lcom/google/android/apps/youtube/app/ui/ix;

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->E:Landroid/view/animation/DecelerateInterpolator;

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;->NONE:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    sget v0, Lcom/google/android/youtube/g;->ai:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:I

    sget v0, Lcom/google/android/youtube/g;->ah:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o:I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->G:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->G:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    move-object v0, p1

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/BaseApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/BaseApplication;->D()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->P:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/BaseApplication;->y()Lcom/google/android/apps/youtube/core/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->au()Lcom/google/android/apps/youtube/core/au;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/l;->a(Lcom/google/android/apps/youtube/core/au;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/youtube/h;->ak:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->H:Landroid/graphics/drawable/Drawable;

    :goto_0
    sget v0, Lcom/google/android/youtube/h;->al:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->I:Landroid/graphics/drawable/Drawable;

    sget v0, Lcom/google/android/youtube/g;->aj:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->J:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->K:Landroid/graphics/Rect;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/iy;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/youtube/app/ui/iy;-><init>(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->O:Lcom/google/android/apps/youtube/app/ui/iy;

    sget-object v0, Lcom/google/android/youtube/r;->K:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    invoke-virtual {v3, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:I

    const/4 v0, 0x2

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:I

    const/4 v0, 0x3

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c:I

    const/4 v0, 0x4

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d:I

    const/4 v0, 0x5

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;->TABLET:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->t:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

    const/4 v0, 0x6

    const/high16 v4, 0x43700000    # 240.0f

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:I

    const/4 v0, 0x7

    const/high16 v4, 0x41400000    # 12.0f

    invoke-virtual {v3, v0, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f:I

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:I

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    const-string v3, "playerViewId must be specified"

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:I

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    const-string v3, "metadataViewId must be specified"

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c:I

    if-eqz v0, :cond_5

    :goto_4
    const-string v0, "metadataLandscapeTitleViewId must be specified"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->H:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;->PHONE:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_4
.end method

.method private static a(FII)I
    .locals 2

    const/4 v0, 0x0

    invoke-static {p0, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    sub-int v1, p2, p1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/2addr v0, p1

    return v0
.end method

.method private a(IIIZ)I
    .locals 2

    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-ne v0, p2, :cond_0

    :goto_0
    return p3

    :cond_0
    int-to-float v0, v0

    int-to-float v1, p2

    div-float/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1, p3}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v0

    if-eqz p4, :cond_1

    const/high16 v1, 0x3f400000    # 0.75f

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    :cond_1
    const/16 v1, 0x32

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p3

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;)Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    return-object v0
.end method

.method private a(II)V
    .locals 11

    const/high16 v10, 0x40000000    # 2.0f

    const v8, 0x3fe374bc    # 1.777f

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingLeft()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingTop()I

    move-result v6

    sub-int v0, p1, v5

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingRight()I

    move-result v1

    sub-int v7, v0, v1

    sub-int v0, p2, v6

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingBottom()I

    move-result v1

    sub-int v3, v0, v1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:I

    int-to-float v0, v0

    div-float/2addr v0, v8

    float-to-int v1, v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/l;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x3f333333    # 0.7f

    int-to-float v2, v7

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    :goto_0
    int-to-float v2, v0

    div-float/2addr v2, v8

    float-to-int v2, v2

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    invoke-static {v8, v5, v6, v0, v2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Landroid/graphics/Rect;IIII)V

    move v0, v1

    move v1, v2

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f:I

    sub-int v5, v7, v5

    iget v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:I

    sub-int/2addr v5, v6

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingBottom()I

    move-result v6

    sub-int v6, p2, v6

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f:I

    sub-int/2addr v6, v7

    sub-int/2addr v6, v0

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:I

    invoke-static {v2, v5, v6, v7, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Landroid/graphics/Rect;IIII)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v5

    div-int/lit8 v2, v2, 0x2

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    sub-int v2, v6, v2

    sub-int v5, v7, v5

    mul-int/lit8 v6, v2, 0x2

    if-le v5, v6, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:Lcom/google/android/apps/youtube/app/ui/ix;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Lcom/google/android/apps/youtube/app/ui/ix;->a(F)V

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f:I

    sub-int v2, v3, v2

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v2, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_6

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getFinalY()I

    move-result v0

    if-gtz v0, :cond_5

    invoke-direct {p0, v4, v4}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(ZZ)V

    :cond_0
    :goto_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    invoke-direct {p0, v4}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k()V

    invoke-static {p1, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {p2, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->measure(II)V

    return-void

    :cond_1
    const v0, 0x3f266666    # 0.65f

    int-to-float v2, v7

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    invoke-static {v0, v5, v6, v7, v3}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Landroid/graphics/Rect;IIII)V

    move v0, v1

    move v1, v3

    goto/16 :goto_1

    :cond_3
    int-to-float v0, v7

    div-float/2addr v0, v8

    float-to-int v1, v0

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e:I

    int-to-float v0, v0

    div-float/2addr v0, v8

    float-to-int v0, v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    invoke-static {v2, v5, v6, v7, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Landroid/graphics/Rect;IIII)V

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:Lcom/google/android/apps/youtube/app/ui/ix;

    int-to-double v6, v5

    int-to-double v8, v5

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    double-to-float v1, v6

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ix;->a(F)V

    mul-int v0, v2, v2

    mul-int v1, v5, v5

    add-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    goto :goto_2

    :cond_5
    invoke-direct {p0, v4, v4}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(ZZ)V

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Z

    if-eqz v0, :cond_7

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    :goto_4
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V

    goto :goto_3

    :cond_7
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-ne v0, v1, :cond_9

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    :goto_5
    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    goto :goto_3

    :cond_9
    move v0, v4

    goto :goto_5
.end method

.method private static a(Landroid/graphics/Rect;IIII)V
    .locals 2

    add-int v0, p1, p3

    add-int v1, p2, p4

    invoke-virtual {p0, p1, p2, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method private static a(Landroid/view/View;F)V
    .locals 0

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/view/View;->setAlpha(F)V

    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-eq v0, p1, :cond_2

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v2, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:Z

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v2, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MAXIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-ne v0, v2, :cond_4

    move v0, v1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(I)Z

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lcom/google/android/apps/youtube/app/ui/iw;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lcom/google/android/apps/youtube/app/ui/iw;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/app/ui/iw;->a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V

    :cond_2
    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lcom/google/android/apps/youtube/app/ui/iw;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lcom/google/android/apps/youtube/app/ui/iw;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/iw;->E()V

    :cond_3
    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->setHorizontalDisplacement(I)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k()V

    return-void

    :cond_4
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    goto :goto_0
.end method

.method private a(Z)V
    .locals 6

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    const/16 v3, 0xfa

    invoke-direct {p0, v0, v1, v3, p1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(IIIZ)I

    move-result v5

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/widget/Scroller;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    neg-int v3, v3

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate()V

    goto :goto_0
.end method

.method private a(ZZ)V
    .locals 6

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v2, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-ne v0, v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    rsub-int/lit8 v4, v0, 0x0

    if-nez v4, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MAXIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v2, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MAXIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-eq v0, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->P:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v2, "PlayerMaximizeManual"

    const-string v3, "Swipe"

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->q()I

    move-result v2

    invoke-direct {p0, v4, v0, v2, p1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(IIIZ)I

    move-result v5

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/widget/Scroller;

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate()V

    goto :goto_0
.end method

.method private a(I)Z
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    if-eq v1, p1, :cond_0

    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lcom/google/android/apps/youtube/app/ui/iw;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lcom/google/android/apps/youtube/app/ui/iw;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingLeft()I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/iw;->a(IF)V

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    return v0
.end method

.method private b(Z)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r()Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/iv;->a:[I

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(ZZ)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(ZZ)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private b(ZZ)V
    .locals 6

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v2, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-ne v0, v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    sub-int v4, v0, v2

    if-nez v4, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v2, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-eq v0, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->P:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v2, "PlayerMinimizeManual"

    const-string v3, "Swipe"

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->q()I

    move-result v2

    invoke-direct {p0, v4, v0, v2, p1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(IIIZ)I

    move-result v5

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/widget/Scroller;

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate()V

    goto :goto_0
.end method

.method private b(I)Z
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    if-eq v1, p1, :cond_0

    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lcom/google/android/apps/youtube/app/ui/iw;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lcom/google/android/apps/youtube/app/ui/iw;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingLeft()I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/iw;->a(IF)V

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    return v0
.end method

.method private c(Z)V
    .locals 5

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->N:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->M:Z

    if-eq v0, p1, :cond_2

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->M:Z

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getChildCount()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:Landroid/view/View;

    if-eq v3, v4, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private c(ZZ)V
    .locals 6

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->P:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "PlayerDismissManual"

    const-string v3, "Swipe"

    invoke-interface {v0, v1, v3}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const/16 v5, 0xfa

    if-eqz p1, :cond_3

    const/16 v5, 0xbb

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Z

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->q:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/widget/Scroller;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    if-gez v3, :cond_4

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o:I

    neg-int v3, v3

    :goto_1
    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate()V

    goto :goto_0

    :cond_4
    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o:I

    goto :goto_1
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;)Lcom/google/android/apps/youtube/app/ui/iw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lcom/google/android/apps/youtube/app/ui/iw;

    return-object v0
.end method

.method private h()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->t:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;->TABLET:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()V
    .locals 7

    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    if-gtz v0, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate()V

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    if-ge v0, v1, :cond_2

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1

    :cond_2
    iput v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Z

    if-eqz v1, :cond_5

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->q:I

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o:I

    if-lt v0, v1, :cond_4

    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    :cond_3
    :goto_2
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    if-gt v0, v1, :cond_7

    const/4 v0, 0x0

    :goto_3
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->right:I

    goto/16 :goto_1

    :cond_4
    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    add-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    goto :goto_2

    :cond_5
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:I

    if-lt v0, v1, :cond_6

    iput v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    goto :goto_2

    :cond_6
    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    add-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    goto :goto_2

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    goto :goto_3
.end method

.method private l()V
    .locals 9

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x8

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v4, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-eq v0, v4, :cond_9

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j()Z

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i()Z

    move-result v6

    if-nez v0, :cond_6

    if-eqz v6, :cond_6

    move v4, v1

    :goto_2
    if-eqz v0, :cond_7

    if-eqz v6, :cond_7

    move v0, v1

    :goto_3
    iget v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    if-ge v6, v7, :cond_e

    if-nez v4, :cond_d

    if-eqz v0, :cond_8

    move v0, v2

    :goto_4
    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    const v6, 0x3dcccccd    # 0.1f

    cmpl-float v4, v4, v6

    if-lez v4, :cond_c

    const v4, 0x3f8ccccd    # 1.1f

    iget v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    sub-float/2addr v4, v6

    :goto_5
    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    invoke-static {v6, v4}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Landroid/view/View;F)V

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    invoke-static {v6, v4}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Landroid/view/View;F)V

    move v4, v2

    :goto_6
    iget v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    if-lez v6, :cond_b

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->E:Landroid/view/animation/DecelerateInterpolator;

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    sub-float/2addr v5, v7

    invoke-virtual {v6, v5}, Landroid/view/animation/DecelerateInterpolator;->getInterpolation(F)F

    move-result v5

    const v6, 0x3f666666    # 0.9f

    mul-float/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    invoke-static {v6, v5}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Landroid/view/View;F)V

    :cond_3
    move v5, v2

    :goto_7
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->N:Z

    :goto_8
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    if-lez v1, :cond_4

    move v3, v2

    :cond_4
    :goto_9
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_6
    move v4, v2

    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_3

    :cond_8
    move v0, v3

    goto :goto_4

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->K:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate(Landroid/graphics/Rect;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    move v5, v3

    move v0, v3

    move v4, v3

    move v8, v3

    move v3, v2

    move v2, v8

    goto :goto_9

    :cond_a
    return-void

    :cond_b
    move v5, v3

    goto :goto_7

    :cond_c
    move v4, v5

    goto :goto_5

    :cond_d
    move v0, v3

    move v4, v3

    goto :goto_6

    :cond_e
    move v5, v3

    move v0, v3

    move v4, v3

    goto :goto_8
.end method

.method private m()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;->NONE:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;->NONE:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lcom/google/android/apps/youtube/app/ui/iw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lcom/google/android/apps/youtube/app/ui/iw;

    :cond_0
    return-void
.end method

.method private o()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p()V

    return-void
.end method

.method private p()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Z

    return-void
.end method

.method private q()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x190

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x15e

    goto :goto_0
.end method

.method private r()Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:I

    if-lt v0, v1, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    div-int/lit8 v1, v1, 0x2

    if-lt v0, v1, :cond_3

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MAXIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MAXIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V

    return-void
.end method

.method public final b()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V

    return-void
.end method

.method public final c()Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    return-object v0
.end method

.method public computeScroll()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(I)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    if-gtz v0, :cond_3

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MAXIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V

    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate()V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    if-lt v0, v1, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(I)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->p:Z

    if-eqz v0, :cond_6

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V

    :cond_5
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate()V

    goto :goto_0

    :cond_6
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V

    goto :goto_2

    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    if-eqz v0, :cond_8

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    if-eq v0, v1, :cond_8

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(Z)V

    goto :goto_0

    :cond_8
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r()Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-ne v0, v1, :cond_0

    invoke-direct {p0, v2, v2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(ZZ)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V

    goto :goto_0
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:Landroid/view/View;

    if-ne p2, v0, :cond_1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v2, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->I:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->H:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->H:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(ZZ)V

    return-void
.end method

.method public final g()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(ZZ)V

    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(II)V

    return-void
.end method

.method public onFinishInflate()V
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getChildCount()I

    move-result v4

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v0, 0x4

    move v3, v0

    :goto_0
    if-lt v4, v3, :cond_1

    move v0, v1

    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "WatchWhileLayout must have at least "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " children"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k:Ljava/util/LinkedList;

    move v0, v2

    :goto_2
    if-ge v0, v4, :cond_6

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:Landroid/view/View;

    if-nez v7, :cond_2

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a:I

    if-ne v7, v6, :cond_2

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:Landroid/view/View;

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_0
    const/4 v0, 0x3

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    if-nez v7, :cond_3

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b:I

    if-ne v7, v6, :cond_3

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    goto :goto_3

    :cond_3
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    if-nez v7, :cond_4

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c:I

    if-ne v7, v6, :cond_4

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    goto :goto_3

    :cond_4
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    if-nez v7, :cond_5

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d:I

    if-lez v7, :cond_5

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d:I

    if-ne v7, v6, :cond_5

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    goto :goto_3

    :cond_5
    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k:Ljava/util/LinkedList;

    invoke-virtual {v6, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v5, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_a

    :goto_4
    const-string v0, "contentViews cannot be empty"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->O:Lcom/google/android/apps/youtube/app/ui/iy;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->O:Lcom/google/android/apps/youtube/app/ui/iy;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->bringChildToFront(Landroid/view/View;)V

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->bringChildToFront(Landroid/view/View;)V

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->bringChildToFront(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->bringChildToFront(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l()V

    return-void

    :cond_a
    move v1, v2

    goto :goto_4
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    :cond_3
    :goto_1
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m()Z

    move-result v0

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:Lcom/google/android/apps/youtube/app/ui/ix;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/app/ui/ix;->f(Landroid/view/MotionEvent;)Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;->NONE:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    if-eq v1, v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    if-eq v2, v1, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->O:Lcom/google/android/apps/youtube/app/ui/iy;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/iy;->a(Lcom/google/android/apps/youtube/app/ui/iy;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(Z)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lcom/google/android/apps/youtube/app/ui/iw;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lcom/google/android/apps/youtube/app/ui/iw;

    goto :goto_1

    :pswitch_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:Lcom/google/android/apps/youtube/app/ui/ix;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/youtube/app/ui/ix;->b(Landroid/view/MotionEvent;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->B:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->isFinished()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->C:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->isFinished()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_4
    move v0, v1

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:Lcom/google/android/apps/youtube/app/ui/ix;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ix;->a()V

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:Lcom/google/android/apps/youtube/app/ui/ix;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/ix;->e(Landroid/view/MotionEvent;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 15

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l()V

    iget-boolean v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:Landroid/view/View;

    move/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    move/from16 v3, p5

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/view/View;->layout(IIII)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->K:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    :cond_2
    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    if-gtz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->K:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->K:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    :goto_2
    const/4 v4, 0x0

    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    const/high16 v6, 0x3f800000    # 1.0f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_7

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    :cond_3
    :goto_3
    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->H:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->H:Landroid/graphics/drawable/Drawable;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->H:Landroid/graphics/drawable/Drawable;

    invoke-static {v4}, Lcom/google/android/apps/youtube/core/utils/ah;->a(F)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :cond_4
    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->I:Landroid/graphics/drawable/Drawable;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->K:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->I:Landroid/graphics/drawable/Drawable;

    invoke-static {v4}, Lcom/google/android/apps/youtube/core/utils/ah;->a(F)I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->invalidate(IIII)V

    iget-boolean v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->M:Z

    if-eqz v4, :cond_a

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getHeight()I

    move-result v5

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j()Z

    move-result v7

    if-eqz v7, :cond_8

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    sub-int/2addr v4, v9

    invoke-static {v7, v8, v4}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v4

    add-int/2addr v4, v6

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    sub-int v6, v4, v6

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    iget v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v5, v10

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    mul-int/lit8 v10, v10, 0x2

    add-int/2addr v5, v10

    invoke-static {v8, v9, v5}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v5

    add-int/2addr v5, v7

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v8

    sub-int/2addr v4, v8

    int-to-float v4, v4

    invoke-virtual {v7, v4}, Landroid/view/View;->setTranslationX(F)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v6, v6

    invoke-virtual {v4, v6}, Landroid/view/View;->setTranslationX(F)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationY(F)V

    goto/16 :goto_0

    :cond_5
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->K:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->J:I

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->J:I

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    iget v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->J:I

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    iget v9, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->J:I

    add-int/2addr v8, v9

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_1

    :cond_6
    new-instance v4, Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->K:Landroid/graphics/Rect;

    invoke-direct {v4, v5}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->L:Landroid/graphics/Rect;

    goto/16 :goto_2

    :cond_7
    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    const/high16 v6, 0x40000000    # 2.0f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_3

    const/high16 v4, 0x40000000    # 2.0f

    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    sub-float/2addr v4, v5

    goto/16 :goto_3

    :cond_8
    if-eqz v6, :cond_9

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingBottom()I

    move-result v4

    sub-int v4, v5, v4

    :goto_4
    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v4, v5

    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    int-to-float v4, v4

    invoke-virtual {v5, v4}, Landroid/view/View;->setTranslationY(F)V

    goto/16 :goto_0

    :cond_9
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    const/4 v6, 0x0

    iget v7, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f:I

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v5, v6, v7}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v6

    sub-int v6, v4, v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_4

    :cond_a
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationX(F)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationY(F)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationX(F)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationY(F)V

    :cond_b
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationX(F)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationY(F)V

    :cond_c
    sub-int v4, p4, p2

    sub-int v5, p5, p3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingLeft()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingTop()I

    move-result v7

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i()Z

    move-result v8

    if-eqz v8, :cond_e

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j()Z

    move-result v9

    if-eqz v9, :cond_e

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    iget v9, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    sub-int v11, v4, v11

    invoke-static {v9, v10, v11}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v9

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int v9, v8, v9

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    iget v11, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->bottom:I

    sub-int v13, v5, v13

    iget-object v14, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    mul-int/lit8 v14, v14, 0x2

    add-int/2addr v13, v14

    invoke-static {v11, v12, v13}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v11

    add-int/2addr v10, v11

    iget-object v11, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    iget-object v12, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v12

    add-int/2addr v12, v8

    iget-object v13, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    add-int/2addr v13, v7

    invoke-virtual {v11, v8, v7, v12, v13}, Landroid/view/View;->layout(IIII)V

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    iget-object v11, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    add-int/2addr v11, v9

    iget-object v12, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v12, v10

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/view/View;->layout(IIII)V

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    if-eqz v8, :cond_d

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10, v4, v5}, Landroid/view/View;->layout(IIII)V

    :cond_d
    :goto_5
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v6

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v7

    invoke-virtual {v4, v6, v7, v8, v9}, Landroid/view/View;->layout(IIII)V

    goto :goto_6

    :cond_e
    if-eqz v8, :cond_f

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingBottom()I

    :goto_7
    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    if-eqz v8, :cond_d

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10, v4, v5}, Landroid/view/View;->layout(IIII)V

    goto :goto_5

    :cond_f
    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    iget v9, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->y:F

    const/4 v10, 0x0

    iget v11, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f:I

    iget-object v12, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    invoke-virtual {v12}, Landroid/graphics/Rect;->height()I

    move-result v12

    add-int/2addr v11, v12

    invoke-static {v9, v10, v11}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(FII)I

    move-result v9

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    add-int/2addr v10, v6

    iget-object v11, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    add-int/2addr v11, v8

    invoke-virtual {v9, v6, v8, v10, v11}, Landroid/view/View;->layout(IIII)V

    goto :goto_7
.end method

.method protected onMeasure(II)V
    .locals 8

    const/high16 v7, 0x40000000    # 2.0f

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eq v0, v7, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "WatchWhileLayout can only be used in EXACTLY mode."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eq v0, v7, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "WatchWhileLayout can only be used in EXACTLY mode."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:Landroid/view/View;

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/view/View;->measure(II)V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->x:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/view/View;->measure(II)V

    iget-boolean v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->M:Z

    if-nez v4, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    sub-int/2addr v0, v5

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v4, v0, v3}, Landroid/view/View;->measure(II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->i:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int/2addr v1, v5

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, v4, v1}, Landroid/view/View;->measure(II)V

    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->j:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->measure(II)V

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->v:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v1, v4

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/view/View;->measure(II)V

    goto :goto_0
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MAXIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    instance-of v0, p1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void

    :cond_0
    check-cast p1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    # getter for: Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;->watchState:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;
    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;->access$200(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;)Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;-><init>(Landroid/os/Parcelable;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r()Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    move-result-object v0

    :goto_0
    # setter for: Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;->watchState:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;
    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;->access$202(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$SavedState;Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    return-object v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(II)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:Lcom/google/android/apps/youtube/app/ui/ix;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/ix;->a(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v1

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:Lcom/google/android/apps/youtube/app/ui/ix;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/ix;->b(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    sget-object v3, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;->DISMISS:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:Lcom/google/android/apps/youtube/app/ui/ix;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/ix;->c(Landroid/view/MotionEvent;)I

    move-result v0

    neg-int v0, v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v3, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    add-int/2addr v0, v2

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(I)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:Lcom/google/android/apps/youtube/app/ui/ix;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/ix;->c(Landroid/view/MotionEvent;)I

    move-result v3

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/ix;->d(Landroid/view/MotionEvent;)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/youtube/app/ui/ix;->a(II)I

    move-result v0

    neg-int v0, v0

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    add-int/2addr v0, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(I)Z

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    sget-object v3, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;->DISMISS:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    if-ne v0, v3, :cond_f

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:Lcom/google/android/apps/youtube/app/ui/ix;

    sget-object v3, Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Orientation;->HORIZONTAL:Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Orientation;

    invoke-virtual {v0, p1, v3, v1}, Lcom/google/android/apps/youtube/app/ui/ix;->a(Landroid/view/MotionEvent;Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Orientation;Z)Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;

    move-result-object v4

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;->FORWARD:Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;

    if-ne v4, v0, :cond_3

    move v0, v1

    :goto_1
    sget-object v3, Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;->BACK:Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;

    if-ne v4, v3, :cond_4

    move v3, v1

    :goto_2
    sget-object v5, Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;->NONE:Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;

    if-ne v4, v5, :cond_5

    move v4, v1

    :goto_3
    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    iget v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:I

    neg-int v6, v6

    if-ge v5, v6, :cond_8

    if-eqz v3, :cond_6

    move v2, v1

    move-object v0, p0

    :goto_4
    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Z)V

    :cond_2
    :goto_5
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:Lcom/google/android/apps/youtube/app/ui/ix;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ix;->a()V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v3, v2

    goto :goto_2

    :cond_5
    move v4, v2

    goto :goto_3

    :cond_6
    if-nez v4, :cond_7

    move v2, v1

    :cond_7
    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(ZZ)V

    goto :goto_5

    :cond_8
    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    iget v6, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n:I

    if-le v5, v6, :cond_b

    if-eqz v0, :cond_9

    move v2, v1

    move-object v0, p0

    goto :goto_4

    :cond_9
    if-nez v4, :cond_a

    move v2, v1

    :cond_a
    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(ZZ)V

    goto :goto_5

    :cond_b
    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    const/16 v6, -0x14

    if-ge v5, v6, :cond_c

    if-eqz v0, :cond_c

    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(ZZ)V

    goto :goto_5

    :cond_c
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->m:I

    const/16 v5, 0x14

    if-le v0, v5, :cond_d

    if-eqz v3, :cond_d

    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(ZZ)V

    goto :goto_5

    :cond_d
    if-nez v4, :cond_e

    move v2, v1

    move-object v0, p0

    goto :goto_4

    :cond_e
    move-object v0, p0

    goto :goto_4

    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->A:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    sget-object v2, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;->MIN_MAX:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:Lcom/google/android/apps/youtube/app/ui/ix;

    sget-object v2, Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Orientation;->VERTICAL:Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Orientation;

    invoke-virtual {v0, p1, v2, v1}, Lcom/google/android/apps/youtube/app/ui/ix;->a(Landroid/view/MotionEvent;Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Orientation;Z)Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;->BACK:Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;

    if-ne v0, v2, :cond_10

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v3, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MAXIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-ne v2, v3, :cond_10

    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(ZZ)V

    goto :goto_5

    :cond_10
    sget-object v2, Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;->FORWARD:Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;

    if-ne v0, v2, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v2, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-ne v0, v2, :cond_11

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->l:I

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->r:I

    if-ge v0, v2, :cond_11

    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(ZZ)V

    goto :goto_5

    :cond_11
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(Z)V

    goto :goto_5

    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->n()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:Lcom/google/android/apps/youtube/app/ui/ix;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ix;->a()V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:Lcom/google/android/apps/youtube/app/ui/ix;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/ix;->e(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->D:Lcom/google/android/apps/youtube/app/ui/ix;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ix;->a()V

    return-void
.end method

.method public setFullscreen(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->u:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->z:Z

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MAXIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->o()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setHorizontalDisplacement(I)V
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    if-eq v0, p1, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->w:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    if-le p1, v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->s:I

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->k()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setListener(Lcom/google/android/apps/youtube/app/ui/iw;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->F:Lcom/google/android/apps/youtube/app/ui/iw;

    return-void
.end method
