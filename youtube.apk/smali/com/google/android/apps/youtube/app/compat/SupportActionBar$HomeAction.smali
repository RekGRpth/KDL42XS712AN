.class public final enum Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

.field public static final enum DRAWER_TOGGLE:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

.field public static final enum NONE:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

.field public static final enum UP:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->NONE:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    const-string v1, "UP"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->UP:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    const-string v1, "DRAWER_TOGGLE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->DRAWER_TOGGLE:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    sget-object v1, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->NONE:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->UP:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->DRAWER_TOGGLE:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->$VALUES:[Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->$VALUES:[Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    return-object v0
.end method
