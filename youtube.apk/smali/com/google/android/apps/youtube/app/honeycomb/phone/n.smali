.class final Lcom/google/android/apps/youtube/app/honeycomb/phone/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/honeycomb/phone/m;

.field private final b:Lcom/google/android/apps/youtube/datalib/legacy/model/Page;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/m;Lcom/google/android/apps/youtube/datalib/legacy/model/Page;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/n;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/n;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/m;Lcom/google/android/apps/youtube/datalib/legacy/model/Page;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/honeycomb/phone/n;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/m;Lcom/google/android/apps/youtube/datalib/legacy/model/Page;)V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7

    const/4 v3, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/n;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/n;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/m;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/m;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/m;)Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->feature:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/n;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/m;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/m;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b(Ljava/util/List;)V

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/n;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/m;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/m;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v2, v1, v3}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/n;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/m;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/m;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/n;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/m;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/honeycomb/phone/m;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->r()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/n;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/m;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/honeycomb/phone/m;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/m;)Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    move-result-object v4

    iget-object v5, v4, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->feature:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move v4, v3

    move v6, v3

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/am;->a(Ljava/lang/String;Ljava/lang/String;IZLcom/google/android/apps/youtube/core/client/WatchFeature;Z)V

    return-void
.end method
