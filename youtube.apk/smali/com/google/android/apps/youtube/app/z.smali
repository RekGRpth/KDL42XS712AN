.class final Lcom/google/android/apps/youtube/app/z;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/z;->a:Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/z;->a:Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/z;->a:Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->a(Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;)Lcom/google/android/apps/youtube/datalib/offline/i;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->a(Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;Lcom/google/android/apps/youtube/datalib/offline/i;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/z;->a:Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->b(Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "Queue is empty"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/z;->a:Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->b(Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/z;->a:Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->c(Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;)Lcom/google/android/apps/youtube/uilib/a/h;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/z;->a:Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->c(Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;)Lcom/google/android/apps/youtube/uilib/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->notifyDataSetChanged()V

    goto :goto_0
.end method
