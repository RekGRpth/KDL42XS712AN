.class public final enum Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

.field public static final enum AMODO_ONLY:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

.field public static final enum HD:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

.field public static final enum SD:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;


# instance fields
.field private final formatType:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

.field private final settingStringId:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    const-string v1, "AMODO_ONLY"

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;->AMODO_ONLY:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    invoke-direct {v0, v1, v4, v2, v4}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;-><init>(Ljava/lang/String;ILcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->AMODO_ONLY:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    const-string v1, "SD"

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;->SD:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    sget v3, Lcom/google/android/youtube/p;->du:I

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;-><init>(Ljava/lang/String;ILcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->SD:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    const-string v1, "HD"

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;->HD:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    sget v3, Lcom/google/android/youtube/p;->dv:I

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;-><init>(Ljava/lang/String;ILcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->HD:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    sget-object v1, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->AMODO_ONLY:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->SD:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->HD:Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->$VALUES:[Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->formatType:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    iput p4, p0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->settingStringId:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->formatType:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    return-object v0
.end method

.method public static getOfflineStreamQualityForFormatType(Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;
    .locals 1

    invoke-static {}, Lcom/google/android/apps/youtube/app/offline/q;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    return-object v0
.end method

.method public static getOfflineStreamQualityForQualityValue(I)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;
    .locals 1

    invoke-static {}, Lcom/google/android/apps/youtube/app/offline/q;->b()Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->$VALUES:[Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    return-object v0
.end method


# virtual methods
.method public final getFormatType()Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->formatType:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    return-object v0
.end method

.method public final getQualityValue()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->formatType:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;->getQualityValue()I

    move-result v0

    return v0
.end method

.method public final getSettingStringId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->settingStringId:I

    return v0
.end method
