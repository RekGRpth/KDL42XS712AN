.class public final Lcom/google/android/apps/youtube/app/ui/presenter/ax;
.super Lcom/google/android/apps/youtube/app/ui/presenter/a;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/a;-><init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)Landroid/view/View;
    .locals 4

    const/16 v3, 0x64

    const/4 v2, 0x0

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/ax;->b()V

    :goto_0
    const/4 v0, 0x0

    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/ax;->c()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->t()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/youtube/h;->Z:I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->o()I

    move-result v1

    invoke-virtual {p0, v0, v2, v1, v3}, Lcom/google/android/apps/youtube/app/ui/presenter/ax;->a(IIII)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/ax;->b()V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->o()I

    move-result v0

    invoke-virtual {p0, v2, v0, v3}, Lcom/google/android/apps/youtube/app/ui/presenter/ax;->a(ZII)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/ax;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
