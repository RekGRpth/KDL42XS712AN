.class public final Lcom/google/android/apps/youtube/app/fragments/navigation/d;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(I)Landroid/os/Bundle;
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    and-int/lit8 v1, p0, 0x1

    if-eqz v1, :cond_0

    const-string v1, "must_authenticate"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    and-int/lit8 v1, p0, 0x2

    if-eqz v1, :cond_1

    const-string v1, "guide_entry"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_1
    return-object v0
.end method

.method public static final a()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(I)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "search_type"

    sget-object v2, Lcom/google/android/apps/youtube/app/search/SearchType;->CHANNEL:Lcom/google/android/apps/youtube/app/search/SearchType;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/search/SearchType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static final a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/e/p;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(Ljava/lang/String;Z)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public static final a(Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(I)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "search_type"

    sget-object v2, Lcom/google/android/apps/youtube/app/search/SearchType;->CHANNEL:Lcom/google/android/apps/youtube/app/search/SearchType;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/search/SearchType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "channel_feed_uri"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "category"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static a(Lcom/google/a/a/a/a/qu;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(I)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "section_list_without_preview_proto"

    invoke-static {p0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/InnerTubeRelatedFragment;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static final a(Lcom/google/android/apps/youtube/app/fragments/PaneFragment;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->h()Landroid/os/Bundle;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eqz v0, :cond_0

    :goto_0
    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method

.method public static final a(Lcom/google/android/apps/youtube/app/search/SearchType;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    const-string v0, "query cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(I)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "search_type"

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/search/SearchType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "search_query"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    const-string v1, "time_filter"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(Ljava/lang/String;Z)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(I)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "artist_id"

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "artist_name"

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v0
.end method

.method public static final a(Ljava/lang/String;Z)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(I)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "channel_username"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final b()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(I)Landroid/os/Bundle;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static final b(Landroid/net/Uri;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/q;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/utils/q;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/utils/q;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->b(Ljava/lang/String;Z)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public static final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(I)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "category_term"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/CategoryFragment;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static b(Ljava/lang/String;Z)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    if-eqz p1, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(I)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "browse_id"

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final c()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(I)Landroid/os/Bundle;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static final c(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "playlist_id"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/OfflinePlaylistFragment;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static final d()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(I)Landroid/os/Bundle;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/WatchHistoryFragment;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static d(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "artist_id"

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static final e()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(I)Landroid/os/Bundle;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/MyUploadsFragment;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static final f()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(I)Landroid/os/Bundle;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static final g()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(I)Landroid/os/Bundle;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static final h()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(I)Landroid/os/Bundle;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/WatchLaterFragment;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static final i()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 3

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(I)Landroid/os/Bundle;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v2, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method
