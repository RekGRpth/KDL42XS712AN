.class public final Lcom/google/android/apps/youtube/app/ui/ev;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/app/remote/bw;

.field private final c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

.field private final d:Lcom/google/android/apps/youtube/common/a/a;

.field private final e:Landroid/content/res/Resources;

.field private final f:Lcom/google/android/apps/youtube/core/aw;

.field private final g:Lcom/google/android/apps/youtube/core/Analytics;

.field private final h:Landroid/widget/Button;

.field private final i:Landroid/widget/EditText;

.field private final j:Landroid/widget/ImageView;

.field private final k:Landroid/widget/ProgressBar;

.field private final l:Landroid/view/View;

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/remote/bw;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/app/ui/fg;Lcom/google/android/apps/youtube/core/aw;I)V
    .locals 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->a:Landroid/app/Activity;

    const-string v1, "screensClient can not be null"

    invoke-static {p3, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/remote/bw;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->b:Lcom/google/android/apps/youtube/app/remote/bw;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/Analytics;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->g:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static/range {p8 .. p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/aw;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->f:Lcom/google/android/apps/youtube/core/aw;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/ew;

    move-object v2, p0

    move-object v3, p6

    move-object v4, p2

    move-object v5, p1

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/youtube/app/ui/ew;-><init>(Lcom/google/android/apps/youtube/app/ui/ev;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/app/remote/an;Landroid/app/Activity;Lcom/google/android/apps/youtube/app/ui/fg;Lcom/google/android/apps/youtube/core/aw;)V

    invoke-static {p1, v1}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->d:Lcom/google/android/apps/youtube/common/a/a;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->e:Landroid/content/res/Resources;

    move/from16 v0, p9

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setContentView(I)V

    sget v1, Lcom/google/android/youtube/j;->dh:I

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->i:Landroid/widget/EditText;

    sget v1, Lcom/google/android/youtube/j;->em:I

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->l:Landroid/view/View;

    invoke-virtual {p1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "android.hardware.camera"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->l:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    sget v1, Lcom/google/android/youtube/j;->en:I

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->j:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/j;->eo:I

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->k:Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->i:Landroid/widget/EditText;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/ey;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/ui/ey;-><init>(Lcom/google/android/apps/youtube/app/ui/ev;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    sget v1, Lcom/google/android/youtube/j;->aO:I

    invoke-virtual {p1, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->h:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->h:Landroid/widget/Button;

    sget-object v2, Lcom/google/android/apps/youtube/core/utils/Typefaces;->ROBOTO_LIGHT:Lcom/google/android/apps/youtube/core/utils/Typefaces;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/youtube/core/utils/Typefaces;->toTypeface(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->i:Landroid/widget/EditText;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/ff;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/google/android/apps/youtube/app/ui/ff;-><init>(B)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->i:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ev;->e:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/youtube/p;->m:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v1, v2, v3}, Landroid/widget/EditText;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->i:Landroid/widget/EditText;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/ez;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/ui/ez;-><init>(Lcom/google/android/apps/youtube/app/ui/ev;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->h:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/fa;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/ui/fa;-><init>(Lcom/google/android/apps/youtube/app/ui/ev;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->j:Landroid/widget/ImageView;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/fb;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/youtube/app/ui/fb;-><init>(Lcom/google/android/apps/youtube/app/ui/ev;Landroid/app/Activity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/ev;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ev;->i:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/ev;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ev;->m:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/ev;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ev;->l:Landroid/view/View;

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    :try_start_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "pairingCode"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/net/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-eqz v0, :cond_0

    move-object p0, v0

    :cond_0
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/ev;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ev;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ev;->f:Lcom/google/android/apps/youtube/core/aw;

    sget v1, Lcom/google/android/youtube/p;->bk:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/aw;->a(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->g:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v2, "RemoteAddScreenPressed"

    invoke-interface {v1, v2}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->b:Lcom/google/android/apps/youtube/app/remote/bw;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ev;->a:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/fe;

    const-string v4, ""

    const/4 v5, 0x0

    invoke-direct {v3, p0, v4, v0, v5}, Lcom/google/android/apps/youtube/app/ui/fe;-><init>(Lcom/google/android/apps/youtube/app/ui/ev;Ljava/lang/String;Ljava/lang/String;B)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/app/remote/bw;->b(Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/ev;)Landroid/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ev;->k:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/ui/ev;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ev;->j:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/ui/ev;)Lcom/google/android/apps/youtube/common/a/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ev;->d:Lcom/google/android/apps/youtube/common/a/a;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/ui/ev;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ev;->c:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/app/ui/ev;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ev;->a:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public final a(I)Landroid/app/Dialog;
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ev;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->a:Landroid/app/Activity;

    sget v2, Lcom/google/android/youtube/p;->aZ:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, ""

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/core/ui/aa;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/ev;->a:Landroid/app/Activity;

    invoke-direct {v2, v3}, Lcom/google/android/apps/youtube/core/ui/aa;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/google/android/youtube/p;->m:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/core/ui/aa;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/p;->fj:I

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/fd;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/youtube/app/ui/fd;-><init>(Lcom/google/android/apps/youtube/app/ui/ev;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->K:I

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/fc;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/ui/fc;-><init>(Lcom/google/android/apps/youtube/app/ui/ev;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x403
        :pswitch_0
    .end packed-switch
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ev;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->m:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ev;->i:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final a(IILandroid/content/Intent;)Z
    .locals 3

    const/4 v0, 0x0

    const/16 v1, 0x6b6

    if-ne p1, v1, :cond_1

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    const-string v1, "SCAN_RESULT"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/ev;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->m:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->i:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ev;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->k:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ev;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ev;->k:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ev;->j:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method
