.class public Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;
.super Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/bg;


# instance fields
.field private Y:Lcom/google/android/apps/youtube/app/ui/bd;

.field private Z:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;->Z:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;

    return-object p1
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;->Z:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;->Z:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->g(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)Lcom/google/android/apps/youtube/app/ui/do;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/do;->b(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/bd;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    invoke-direct {v2, v1, v3, v4, v0}, Lcom/google/android/apps/youtube/app/ui/bd;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/c/a;)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;->Y:Lcom/google/android/apps/youtube/app/ui/bd;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$CreatePlaylistDialogFragment;->Y:Lcom/google/android/apps/youtube/app/ui/bd;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/ui/bd;->a(Lcom/google/android/apps/youtube/app/ui/bg;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method
