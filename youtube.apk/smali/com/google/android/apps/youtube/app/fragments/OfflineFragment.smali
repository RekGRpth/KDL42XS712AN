.class public Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;
.super Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
.source "SourceFile"


# instance fields
.field private Y:Lcom/google/android/apps/youtube/app/ui/TabbedView;

.field private Z:Landroid/widget/ProgressBar;

.field private a:Lcom/google/android/apps/youtube/app/ax;

.field private aa:I

.field private ab:I

.field private ac:Lcom/google/android/apps/youtube/app/fragments/OfflineFragment$ClearOfflineDialogFragment;

.field private b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

.field private d:Lcom/google/android/apps/youtube/common/c/a;

.field private e:Lcom/google/android/apps/youtube/core/client/bj;

.field private f:Lcom/google/android/apps/youtube/common/network/h;

.field private g:Lcom/google/android/apps/youtube/app/offline/p;

.field private h:Lcom/google/android/apps/youtube/app/ui/ch;

.field private i:Lcom/google/android/apps/youtube/app/ui/cc;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->aa:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->aa:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->ab:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, -0x1

    iget v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->aa:I

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->ab:I

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->aa:I

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->ab:I

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->Y:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->Y:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->Z:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_1
    return-void

    :cond_2
    iget v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->aa:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->ab:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->Y:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->ab:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->f()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 26

    sget v2, Lcom/google/android/youtube/l;->as:I

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v24

    sget v2, Lcom/google/android/youtube/j;->ft:I

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/ui/TabbedView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->Y:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    sget v2, Lcom/google/android/youtube/j;->co:I

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->Z:Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->Y:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    sget v3, Lcom/google/android/youtube/p;->do:I

    sget v4, Lcom/google/android/youtube/l;->ay:I

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(II)Landroid/view/View;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v5

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v2}, Lcom/google/android/apps/youtube/core/offline/store/q;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    :goto_0
    new-instance v10, Lcom/google/android/apps/youtube/app/ui/bv;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->g:Lcom/google/android/apps/youtube/app/offline/p;

    invoke-direct {v10, v2, v3}, Lcom/google/android/apps/youtube/app/ui/bv;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/offline/p;)V

    new-instance v2, Lcom/google/android/apps/youtube/app/offline/r;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->f:Lcom/google/android/apps/youtube/common/network/h;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->g:Lcom/google/android/apps/youtube/app/offline/p;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v11}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J()Lcom/google/android/apps/youtube/app/ui/hh;

    move-result-object v11

    invoke-direct/range {v2 .. v11}, Lcom/google/android/apps/youtube/app/offline/r;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/app/ui/bv;Lcom/google/android/apps/youtube/app/ui/hh;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->aa()Lcom/google/android/apps/youtube/core/player/w;

    move-result-object v22

    new-instance v3, Lcom/google/android/apps/youtube/core/player/z;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v3, v6}, Lcom/google/android/apps/youtube/core/player/z;-><init>(Landroid/app/Activity;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/core/player/w;->a(Lcom/google/android/apps/youtube/core/player/z;)V

    new-instance v11, Lcom/google/android/apps/youtube/app/offline/f;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->f:Lcom/google/android/apps/youtube/common/network/h;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->g:Lcom/google/android/apps/youtube/app/offline/p;

    move-object/from16 v18, v0

    move-object v13, v4

    move-object v14, v5

    move-object/from16 v19, v10

    invoke-direct/range {v11 .. v19}, Lcom/google/android/apps/youtube/app/offline/f;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/app/ui/bv;)V

    new-instance v12, Lcom/google/android/apps/youtube/app/ui/ch;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J()Lcom/google/android/apps/youtube/app/ui/hh;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->d:Lcom/google/android/apps/youtube/common/c/a;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->e:Lcom/google/android/apps/youtube/core/client/bj;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->f:Lcom/google/android/apps/youtube/common/network/h;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->g:Lcom/google/android/apps/youtube/app/offline/p;

    move-object/from16 v21, v0

    new-instance v23, Lcom/google/android/apps/youtube/app/fragments/aw;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/aw;-><init>(Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;)V

    move-object/from16 v16, v2

    invoke-direct/range {v12 .. v23}, Lcom/google/android/apps/youtube/app/ui/ch;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/app/offline/r;Lcom/google/android/apps/youtube/app/ui/hh;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/app/ui/ci;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->h:Lcom/google/android/apps/youtube/app/ui/ch;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->h:Lcom/google/android/apps/youtube/app/ui/ch;

    move-object/from16 v0, v25

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/app/ui/ch;->a(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->Y:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    sget v3, Lcom/google/android/youtube/p;->dm:I

    sget v4, Lcom/google/android/youtube/l;->av:I

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(II)Landroid/view/View;

    move-result-object v2

    new-instance v8, Lcom/google/android/apps/youtube/app/ui/cc;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->e:Lcom/google/android/apps/youtube/core/client/bj;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->d:Lcom/google/android/apps/youtube/common/c/a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->f:Lcom/google/android/apps/youtube/common/network/h;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->g:Lcom/google/android/apps/youtube/app/offline/p;

    move-object/from16 v16, v0

    new-instance v17, Lcom/google/android/apps/youtube/app/fragments/ax;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/ax;-><init>(Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;)V

    invoke-direct/range {v8 .. v17}, Lcom/google/android/apps/youtube/app/ui/cc;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/app/offline/f;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/app/ui/cf;)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->i:Lcom/google/android/apps/youtube/app/ui/cc;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->i:Lcom/google/android/apps/youtube/app/ui/cc;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/youtube/app/ui/cc;->a(Landroid/view/View;)V

    if-eqz p3, :cond_0

    const-string v2, "offline_selected_tab_index"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->Y:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(I)V

    :cond_0
    return-object v24

    :cond_1
    invoke-interface {v5}, Lcom/google/android/apps/youtube/core/offline/store/q;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->dn:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->H()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->d:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->e:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->f:Lcom/google/android/apps/youtube/common/network/h;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->Z()Lcom/google/android/apps/youtube/app/offline/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->g:Lcom/google/android/apps/youtube/app/offline/p;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/j;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Lcom/google/android/apps/youtube/app/compat/j;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->M()Lcom/google/android/apps/youtube/app/compat/o;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/m;->g:I

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/youtube/app/compat/o;->a(ILcom/google/android/apps/youtube/app/compat/j;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/q;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-interface {p1}, Lcom/google/android/apps/youtube/app/compat/q;->e()I

    move-result v1

    sget v2, Lcom/google/android/youtube/j;->ct:I

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->ac:Lcom/google/android/apps/youtube/app/fragments/OfflineFragment$ClearOfflineDialogFragment;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->ac:Lcom/google/android/apps/youtube/app/fragments/OfflineFragment$ClearOfflineDialogFragment;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c()Landroid/support/v4/app/l;

    move-result-object v2

    const-string v3, "ClearOfflineDialogFragment"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment$ClearOfflineDialogFragment;->a(Landroid/support/v4/app/l;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1}, Lcom/google/android/apps/youtube/app/compat/q;->e()I

    move-result v1

    sget v2, Lcom/google/android/youtube/j;->cw:I

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v2, Lcom/google/android/youtube/p;->gB:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/utils/m;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Lcom/google/android/apps/youtube/app/compat/q;)Z

    move-result v0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->d()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c()Landroid/support/v4/app/l;

    move-result-object v0

    const-string v1, "ClearOfflineDialogFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/l;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment$ClearOfflineDialogFragment;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->ac:Lcom/google/android/apps/youtube/app/fragments/OfflineFragment$ClearOfflineDialogFragment;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->ac:Lcom/google/android/apps/youtube/app/fragments/OfflineFragment$ClearOfflineDialogFragment;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment$ClearOfflineDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment$ClearOfflineDialogFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->ac:Lcom/google/android/apps/youtube/app/fragments/OfflineFragment$ClearOfflineDialogFragment;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->ac:Lcom/google/android/apps/youtube/app/fragments/OfflineFragment$ClearOfflineDialogFragment;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment$ClearOfflineDialogFragment;->a(Lcom/google/android/apps/youtube/app/fragments/OfflineFragment$ClearOfflineDialogFragment;Ljava/lang/ref/WeakReference;)Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->d:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->h:Lcom/google/android/apps/youtube/app/ui/ch;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->d:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->i:Lcom/google/android/apps/youtube/app/ui/cc;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final e()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->d:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->h:Lcom/google/android/apps/youtube/app/ui/ch;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->d:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->i:Lcom/google/android/apps/youtube/app/ui/cc;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->e(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->Y:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    if-eqz v0, :cond_0

    const-string v0, "offline_selected_tab_index"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->Y:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public final r()V
    .locals 3

    const/4 v2, -0x1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->r()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->Y:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->Z:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iput v2, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->aa:I

    iput v2, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->ab:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->h:Lcom/google/android/apps/youtube/app/ui/ch;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ch;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/OfflineFragment;->i:Lcom/google/android/apps/youtube/app/ui/cc;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/cc;->a()V

    return-void
.end method
