.class public final Lcom/google/android/apps/youtube/app/honeycomb/ui/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/transfer/z;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private A:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

.field private B:Z

.field private final C:Ljava/util/List;

.field private D:I

.field private E:Z

.field private F:J

.field private final G:Lcom/google/android/apps/youtube/core/client/bc;

.field private H:Z

.field private I:Z

.field private J:Lcom/google/android/apps/youtube/app/compat/q;

.field private final b:Landroid/app/Activity;

.field private final c:Landroid/content/ContentResolver;

.field private final d:Landroid/content/SharedPreferences;

.field private final e:Lcom/google/android/apps/youtube/core/Analytics;

.field private final f:Lcom/google/android/apps/youtube/app/honeycomb/ui/n;

.field private final g:Lcom/google/android/apps/youtube/core/transfer/w;

.field private final h:Lcom/google/android/apps/youtube/core/aw;

.field private final i:Lcom/google/android/apps/youtube/app/compat/o;

.field private final j:Lcom/google/android/apps/youtube/common/a/b;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Landroid/graphics/Bitmap;

.field private final n:Landroid/widget/ImageView;

.field private final o:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

.field private final p:Landroid/widget/CheckBox;

.field private final q:Landroid/view/View;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/widget/EditText;

.field private final t:Landroid/widget/EditText;

.field private final u:Landroid/widget/EditText;

.field private final v:Landroid/widget/TextView;

.field private final w:Lcom/google/android/apps/youtube/app/ui/hb;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "latitude"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "longitude"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/app/honeycomb/ui/n;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/compat/o;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->G:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->b:Landroid/app/Activity;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->f:Lcom/google/android/apps/youtube/app/honeycomb/ui/n;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->h:Lcom/google/android/apps/youtube/core/aw;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/compat/o;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->i:Lcom/google/android/apps/youtube/app/compat/o;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->c:Landroid/content/ContentResolver;

    const-string v1, "youtube"

    invoke-virtual {p1, v1, v3}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->d:Landroid/content/SharedPreferences;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->D()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->e:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->d:Landroid/content/SharedPreferences;

    const-string v1, "upload_privacy"

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->PRIVATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->A:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    sget v0, Lcom/google/android/youtube/j;->aP:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->r:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->n:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/youtube/j;->fG:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->s:Landroid/widget/EditText;

    sget v0, Lcom/google/android/youtube/j;->aI:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->t:Landroid/widget/EditText;

    sget v0, Lcom/google/android/youtube/j;->bV:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->u:Landroid/widget/EditText;

    sget v0, Lcom/google/android/youtube/j;->dP:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->o:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->o:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->A:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->setPrivacy(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;)V

    sget v0, Lcom/google/android/youtube/j;->bH:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->p:Landroid/widget/CheckBox;

    sget v0, Lcom/google/android/youtube/j;->bI:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->q:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->fv:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->v:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->v:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/hb;

    new-instance v1, Lcom/google/android/apps/youtube/app/honeycomb/ui/k;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/k;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)V

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/youtube/app/ui/hb;-><init>(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->w:Lcom/google/android/apps/youtube/app/ui/hb;

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/ui/o;

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/youtube/app/honeycomb/ui/o;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;B)V

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->j:Lcom/google/android/apps/youtube/common/a/b;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->C:Ljava/util/List;

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/w;

    invoke-direct {v0, p1, p3}, Lcom/google/android/apps/youtube/core/transfer/w;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bc;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->g:Lcom/google/android/apps/youtube/core/transfer/w;

    return-void
.end method

.method private a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/app/honeycomb/ui/p;
    .locals 8

    const/4 v6, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->c:Landroid/content/ContentResolver;

    sget-object v2, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->a:[Ljava/lang/String;

    const-string v3, "mime_type LIKE \'video/%\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-nez v1, :cond_0

    move-object v0, v6

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error resolving content from URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    move-object v0, v6

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto :goto_0

    :cond_1
    :try_start_2
    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;-><init>(B)V

    const-string v2, "_id"

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;Ljava/lang/Long;)Ljava/lang/Long;

    const-string v2, "mime_type"

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "duration"

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->b(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;Ljava/lang/Long;)Ljava/lang/Long;

    const-string v2, "latitude"

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->b(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;Ljava/lang/String;)Ljava/lang/String;

    const-string v2, "longitude"

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->c(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->m:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->m:Landroid/graphics/Bitmap;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    :cond_2
    :goto_1
    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;Landroid/net/Uri;)Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->d(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->g(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "video/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "invalid file type ["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->g(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto/16 :goto_0

    :cond_3
    :try_start_3
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->b:Landroid/app/Activity;

    invoke-static {v2, p1}, Landroid/provider/DocumentsContract;->isDocumentUri(Landroid/content/Context;Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->c:Landroid/content/ContentResolver;

    new-instance v3, Landroid/graphics/Point;

    const/16 v4, 0x60

    const/16 v5, 0x60

    invoke-direct {v3, v4, v5}, Landroid/graphics/Point;-><init>(II)V

    const/4 v4, 0x0

    invoke-static {v2, p1, v3, v4}, Landroid/provider/DocumentsContract;->getDocumentThumbnail(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/graphics/Point;Landroid/os/CancellationSignal;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    :cond_4
    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->c(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->c:Landroid/content/ContentResolver;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->f(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const/4 v5, 0x3

    const/4 v7, 0x0

    invoke-static {v2, v3, v4, v5, v7}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;Landroid/net/Uri;)Lcom/google/android/apps/youtube/app/honeycomb/ui/p;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/app/honeycomb/ui/p;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Lcom/google/android/apps/youtube/datalib/model/transfer/a;
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->l:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "username"

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->e(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v1, "upload_title"

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->e(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-static {p1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->c(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->c(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Landroid/graphics/Bitmap;)[B

    move-result-object v1

    const-string v2, "upload_file_thumbnail"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;[B)V

    :cond_2
    invoke-static {p1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->d(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v1, "upload_file_duration"

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->d(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;J)V

    :cond_3
    const-string v1, "upload_start_time_millis"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;J)V

    const-string v1, "authAccount"

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "upload_description"

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->y:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "upload_keywords"

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "upload_privacy"

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->A:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->B:Z

    if-eqz v1, :cond_4

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->b(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Landroid/util/Pair;

    move-result-object v1

    if-eqz v1, :cond_4

    const-string v2, "upload_location"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    return-object v0
.end method

.method private static a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;
    .locals 2

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->l:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->w:Lcom/google/android/apps/youtube/app/ui/hb;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/hb;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->g:Lcom/google/android/apps/youtube/core/transfer/w;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/core/transfer/w;->a(Lcom/google/android/apps/youtube/core/transfer/z;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->b:Landroid/app/Activity;

    const/16 v1, 0x3fd

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->E:Z

    return v0
.end method

.method private static b(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Landroid/util/Pair;
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->b(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->b(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->C:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->I:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->b:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->n:Landroid/widget/ImageView;

    return-object v0
.end method

.method private d()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "upload_privacy"

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->A:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->f:Lcom/google/android/apps/youtube/app/honeycomb/ui/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->f:Lcom/google/android/apps/youtube/app/honeycomb/ui/n;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/n;->e()V

    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->r:Landroid/widget/TextView;

    return-object v0
.end method

.method private e()V
    .locals 13

    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->C:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->D:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->s:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->x:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->t:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->y:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->u:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->z:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->o:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->a()Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->A:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->p:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->B:Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->x:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->b:Landroid/app/Activity;

    sget v1, Lcom/google/android/youtube/p;->bd:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->J:Lcom/google/android/apps/youtube/app/compat/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->J:Lcom/google/android/apps/youtube/app/compat/q;

    sget v1, Lcom/google/android/youtube/p;->gq:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/compat/q;->c(I)Lcom/google/android/apps/youtube/app/compat/q;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->C:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->C:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->x:Ljava/lang/String;

    invoke-static {v9, v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->e(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->g:Lcom/google/android/apps/youtube/core/transfer/w;

    invoke-static {v9}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->h(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v9}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->i(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->A:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->x:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->y:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->z:Ljava/lang/String;

    iget-boolean v8, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->B:Z

    if-eqz v8, :cond_2

    invoke-static {v9}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->b(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Landroid/util/Pair;

    move-result-object v8

    :goto_1
    invoke-direct {p0, v9}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    move-result-object v9

    iget-boolean v10, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->E:Z

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/apps/youtube/core/transfer/w;->a(Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Lcom/google/android/apps/youtube/datalib/model/transfer/a;Z)V

    goto :goto_0

    :cond_2
    move-object v8, v6

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->C:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move v1, v0

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->x:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->e(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;Ljava/lang/String;)Ljava/lang/String;

    add-int/lit8 v11, v1, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->g:Lcom/google/android/apps/youtube/core/transfer/w;

    invoke-static {v9}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->h(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v9}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->i(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->A:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    invoke-static {v9}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->e(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->y:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->z:Ljava/lang/String;

    iget-boolean v8, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->B:Z

    if-eqz v8, :cond_4

    invoke-static {v9}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->b(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Landroid/util/Pair;

    move-result-object v8

    :goto_3
    invoke-direct {p0, v9}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    move-result-object v9

    iget-boolean v10, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->E:Z

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/apps/youtube/core/transfer/w;->a(Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/util/Pair;Lcom/google/android/apps/youtube/datalib/model/transfer/a;Z)V

    move v1, v11

    goto :goto_2

    :cond_4
    move-object v8, v6

    goto :goto_3
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->q:Landroid/view/View;

    return-object v0
.end method

.method private f()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->H:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->I:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->J:Lcom/google/android/apps/youtube/app/compat/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->J:Lcom/google/android/apps/youtube/app/compat/q;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/compat/q;->a(Z)Lcom/google/android/apps/youtube/app/compat/q;

    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Lcom/google/android/apps/youtube/app/compat/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->J:Lcom/google/android/apps/youtube/app/compat/q;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->F:J

    return-wide v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Lcom/google/android/apps/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->e:Lcom/google/android/apps/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->e()V

    return-void
.end method

.method static synthetic k(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->f()V

    return-void
.end method

.method static synthetic l(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->h:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method


# virtual methods
.method public final a(I)Landroid/app/Dialog;
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->w:Lcom/google/android/apps/youtube/app/ui/hb;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/hb;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3fd
        :pswitch_0
    .end packed-switch
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->g:Lcom/google/android/apps/youtube/core/transfer/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/w;->c()V

    return-void
.end method

.method public final a(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 4

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->k:Ljava/lang/String;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.google.android.youtube.intent.action.UPLOAD"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v0, "data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->m:Landroid/graphics/Bitmap;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "no media content uri(s)"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :goto_1
    return-void

    :cond_1
    const-string v2, "android.intent.action.SEND"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const-string v2, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->G:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->j:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/core/client/bc;->a(Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->F:J

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->e:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v2, "UploadFormShown"

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/util/List;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 4

    const/4 v2, 0x1

    const-string v0, "Error requesting location for upload"

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->D:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->D:I

    iget v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->D:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->h:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->J:Lcom/google/android/apps/youtube/app/compat/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->J:Lcom/google/android/apps/youtube/app/compat/q;

    sget v1, Lcom/google/android/youtube/p;->gq:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/compat/q;->c(I)Lcom/google/android/apps/youtube/app/compat/q;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->J:Lcom/google/android/apps/youtube/app/compat/q;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/app/compat/q;->a(Z)Lcom/google/android/apps/youtube/app/compat/q;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->e:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "UploadDestinationError"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->d()V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    iget v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->D:I

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    sub-int v0, v1, v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->D:I

    iget v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->D:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->d()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/j;)Z
    .locals 3

    const/4 v0, 0x0

    sget v1, Lcom/google/android/youtube/j;->cy:I

    invoke-virtual {p1, v1}, Lcom/google/android/apps/youtube/app/compat/j;->c(I)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/app/compat/q;->b(Z)Lcom/google/android/apps/youtube/app/compat/q;

    :cond_0
    sget v1, Lcom/google/android/youtube/j;->cv:I

    invoke-virtual {p1, v1}, Lcom/google/android/apps/youtube/app/compat/j;->c(I)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/app/compat/q;->b(Z)Lcom/google/android/apps/youtube/app/compat/q;

    :cond_1
    sget v1, Lcom/google/android/youtube/j;->cz:I

    invoke-virtual {p1, v1}, Lcom/google/android/apps/youtube/app/compat/j;->c(I)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/app/compat/q;->b(Z)Lcom/google/android/apps/youtube/app/compat/q;

    :cond_2
    sget v1, Lcom/google/android/youtube/j;->cw:I

    invoke-virtual {p1, v1}, Lcom/google/android/apps/youtube/app/compat/j;->c(I)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/app/compat/q;->b(Z)Lcom/google/android/apps/youtube/app/compat/q;

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->i:Lcom/google/android/apps/youtube/app/compat/o;

    sget v2, Lcom/google/android/youtube/m;->d:I

    invoke-virtual {v1, v2, p1}, Lcom/google/android/apps/youtube/app/compat/o;->a(ILcom/google/android/apps/youtube/app/compat/j;)V

    sget v1, Lcom/google/android/youtube/j;->cC:I

    invoke-virtual {p1, v1}, Lcom/google/android/apps/youtube/app/compat/j;->c(I)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->J:Lcom/google/android/apps/youtube/app/compat/q;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->J:Lcom/google/android/apps/youtube/app/compat/q;

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->J:Lcom/google/android/apps/youtube/app/compat/q;

    new-instance v1, Lcom/google/android/apps/youtube/app/honeycomb/ui/m;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/m;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/compat/q;->a(Lcom/google/android/apps/youtube/app/compat/s;)Lcom/google/android/apps/youtube/app/compat/q;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->f()V

    const/4 v0, 0x1

    :cond_4
    return v0
.end method

.method public final b()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->e()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->H:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->f()V

    return-void
.end method

.method public final c()V
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->D:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->D:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->C:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->b:Landroid/app/Activity;

    sget v1, Lcom/google/android/youtube/p;->bO:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->f:Lcom/google/android/apps/youtube/app/honeycomb/ui/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->f:Lcom/google/android/apps/youtube/app/honeycomb/ui/n;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/n;->f()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->D:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->d()V

    goto :goto_0
.end method
