.class final Lcom/google/android/apps/youtube/app/ui/ds;
.super Lcom/google/android/apps/youtube/core/a/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ds;->a:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/a/a;-><init>()V

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->values()[Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/ds;->b(Ljava/lang/Iterable;)V

    return-void
.end method


# virtual methods
.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ds;->a:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->a(Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->aN:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/dt;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/youtube/app/ui/dt;-><init>(Lcom/google/android/apps/youtube/app/ui/ds;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/ui/ds;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/dt;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;)V

    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/dt;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ds;->a:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->a(Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->aO:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/du;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/youtube/app/ui/du;-><init>(Lcom/google/android/apps/youtube/app/ui/ds;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/ui/ds;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/du;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;)V

    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/du;

    move-object v1, v0

    goto :goto_0
.end method
