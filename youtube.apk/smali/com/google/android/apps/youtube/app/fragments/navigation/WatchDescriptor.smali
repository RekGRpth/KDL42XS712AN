.class public Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final EXTRA_INVENTORY_IDENTIFIER:Ljava/lang/String; = "android.intent.extra.inventory_identifier"


# instance fields
.field private final localProto:Lcom/google/android/apps/youtube/a/a/i;

.field private final playbackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/navigation/g;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/g;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/youtube/a/a/i;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/i;->a()Lcom/google/android/apps/youtube/a/a/d;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Lcom/google/android/apps/youtube/a/a/d;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->playbackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/a/a/i;Lcom/google/android/apps/youtube/app/fragments/navigation/g;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;-><init>(Lcom/google/android/apps/youtube/a/a/i;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/youtube/a/a/i;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->playbackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    return-void
.end method

.method public static final createFromExternalIntent(Landroid/content/Intent;)Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;
    .locals 6

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    :try_start_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/al;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/utils/al;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iget-object v3, v0, Lcom/google/android/apps/youtube/core/utils/al;->a:Ljava/util/List;

    const/4 v4, -0x1

    iget v0, v0, Lcom/google/android/apps/youtube/core/utils/al;->c:I

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->EXTERNAL_URL:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-direct {v2, v3, v4, v0, v5}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Ljava/util/List;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;-><init>(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    const-string v2, "finish_on_ended"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->setFinishOnEnded(Z)V

    const-string v2, "force_fullscreen"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->setForceFullscreen(Z)V

    const-string v2, "must_authenticate"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->setMustAuthenticate(Z)V

    const-string v2, "no_animation"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->setNoAnimation(Z)V

    const-string v2, "skip_remote_route_dialog"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->setSkipRemoteDialog(Z)V

    const-string v2, "youtube_tv_uid"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->setTvId(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final createFromMediaSearchIntent(Landroid/content/Intent;)Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;
    .locals 6

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    :try_start_0
    const-string v2, "android.intent.extra.inventory_identifier"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v2, v0

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/al;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/utils/al;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    iget-object v3, v0, Lcom/google/android/apps/youtube/core/utils/al;->a:Ljava/util/List;

    const/4 v4, -0x1

    iget v0, v0, Lcom/google/android/apps/youtube/core/utils/al;->c:I

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->EXTERNAL_URL:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-direct {v2, v3, v4, v0, v5}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Ljava/util/List;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;-><init>(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method public static final createFromPlaylistId(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;
    .locals 6

    if-nez p1, :cond_0

    const-string v1, ""

    :goto_0
    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    const/4 v3, -0x1

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->NO_FEATURE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Ljava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;-><init>(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    return-object v1

    :cond_0
    move-object v1, p1

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getPlaybackStartDescriptor()Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->playbackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    return-object v0
.end method

.method public getTvId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/i;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public mustAuthenticate()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/i;->b()Z

    move-result v0

    return v0
.end method

.method public noAnimation()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/i;->g()Z

    move-result v0

    return v0
.end method

.method public setFinishOnEnded(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/a/a/i;->b(Z)Lcom/google/android/apps/youtube/a/a/i;

    return-void
.end method

.method public setForceFullscreen(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/a/a/i;->c(Z)Lcom/google/android/apps/youtube/a/a/i;

    return-void
.end method

.method public setKeepHistory(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/a/a/i;->f(Z)Lcom/google/android/apps/youtube/a/a/i;

    return-void
.end method

.method public setMustAuthenticate(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/a/a/i;->a(Z)Lcom/google/android/apps/youtube/a/a/i;

    return-void
.end method

.method public setNoAnimation(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/a/a/i;->d(Z)Lcom/google/android/apps/youtube/a/a/i;

    return-void
.end method

.method public setSkipRemoteDialog(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/a/a/i;->e(Z)Lcom/google/android/apps/youtube/a/a/i;

    return-void
.end method

.method public setTvId(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/a/a/i;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/i;

    return-void
.end method

.method public shouldFinishOnEnded()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/i;->c()Z

    move-result v0

    return v0
.end method

.method public shouldForceFullscreen()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/i;->d()Z

    move-result v0

    return v0
.end method

.method public shouldKeepHistory()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/i;->i()Z

    move-result v0

    return v0
.end method

.method public shouldSkipRemoteDialog()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/i;->h()Z

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->playbackStartDescriptor:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaybackStartDescriptorProto()Lcom/google/android/apps/youtube/a/a/d;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/i;->a(Lcom/google/android/apps/youtube/a/a/d;)Lcom/google/android/apps/youtube/a/a/i;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->localProto:Lcom/google/android/apps/youtube/a/a/i;

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/e/j;->a(Landroid/os/Parcel;Lcom/google/protobuf/micro/c;)V

    return-void
.end method
