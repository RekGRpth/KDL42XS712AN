.class public Lcom/google/android/apps/youtube/app/ui/RemoteControlView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/TextView;

.field private c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

.field private final d:Landroid/view/LayoutInflater;

.field private e:Landroid/view/View$OnClickListener;

.field private f:Landroid/view/View;

.field private g:Lcom/google/android/apps/youtube/core/ui/u;

.field private h:Landroid/view/View;

.field private i:Landroid/widget/ImageView;

.field private j:Z

.field private k:Landroid/widget/Button;

.field private l:Z

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/ImageView;

.field private o:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;Lcom/google/android/apps/youtube/core/ui/v;Landroid/view/View$OnClickListener;)V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, -0x2

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->d:Landroid/view/LayoutInflater;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->i:Landroid/widget/ImageView;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->i:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->i:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/f;->n:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->d:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/youtube/l;->aS:I

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->m:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->cb:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->d:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/youtube/l;->aG:I

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->m:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->m:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->m:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->h()I

    move-result v1

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->addView(Landroid/view/View;)V

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setShowFullscreen(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->d:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/youtube/l;->aT:I

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->h:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->h:Landroid/view/View;

    invoke-static {v0, v1, p3}, Lcom/google/android/apps/youtube/core/ui/u;->a(Landroid/content/Context;Landroid/view/View;Lcom/google/android/apps/youtube/core/ui/v;)Lcom/google/android/apps/youtube/core/ui/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->g:Lcom/google/android/apps/youtube/core/ui/u;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->g:Lcom/google/android/apps/youtube/core/ui/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/u;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->h:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->ek:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->k:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->h:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->n:Landroid/widget/ImageView;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->n:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->n:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/h;->q:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->n:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->addView(Landroid/view/View;)V

    return-void
.end method

.method private f()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setMinimized(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->e()V

    return-void
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->b()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private h()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->g:Lcom/google/android/apps/youtube/core/ui/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/u;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->j:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setHasCc(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->l:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setCcEnabled(Z)V

    return-void
.end method

.method private i()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->o:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->g()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->i:Landroid/widget/ImageView;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;Lcom/google/android/apps/youtube/app/remote/ar;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;Lcom/google/android/apps/youtube/app/remote/ar;)V
    .locals 4

    const/16 v3, 0x8

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->o:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->d()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;->REMOTE:Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setStyle(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay$Style;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/er;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->h()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setPlaying()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->h()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setAndShowPaused()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->h()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setAndShowEnded()V

    goto :goto_0

    :pswitch_3
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->o:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->d()V

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->g:Lcom/google/android/apps/youtube/core/ui/u;

    sget v2, Lcom/google/android/youtube/p;->u:I

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/youtube/core/ui/u;->a(IZ)V

    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->g()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->g:Lcom/google/android/apps/youtube/core/ui/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/u;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setPlaying()V

    iget-object v0, p2, Lcom/google/android/apps/youtube/app/remote/ar;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->b:Landroid/widget/TextView;

    iget-object v2, p2, Lcom/google/android/apps/youtube/app/remote/ar;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    iget-object v0, p2, Lcom/google/android/apps/youtube/app/remote/ar;->b:Landroid/net/Uri;

    if-eqz v0, :cond_4

    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    iget-object v2, p2, Lcom/google/android/apps/youtube/app/remote/ar;->b:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->f:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->b:Landroid/widget/TextView;

    sget v2, Lcom/google/android/youtube/p;->u:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setLoading()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->g:Lcom/google/android/apps/youtube/core/ui/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/u;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->j:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setHasCc(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->l:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setCcEnabled(Z)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/as;Ljava/lang/String;)V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p1, Lcom/google/android/apps/youtube/app/remote/as;->b:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p1, Lcom/google/android/apps/youtube/app/remote/as;->c:Z

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    const/16 v2, 0x8

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->o:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->d()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->k:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->g:Lcom/google/android/apps/youtube/core/ui/u;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/core/ui/u;->a(Ljava/lang/CharSequence;Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->g()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 1

    if-eqz p2, :cond_0

    sget v0, Lcom/google/android/youtube/p;->fs:I

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a(Ljava/lang/String;I)V

    return-void

    :cond_0
    sget v0, Lcom/google/android/youtube/p;->K:I

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->a(Ljava/util/List;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->i()V

    return-void
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->o:Z

    return v0
.end method

.method public final c()V
    .locals 4

    const/16 v3, 0x8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->g:Lcom/google/android/apps/youtube/core/ui/u;

    sget v1, Lcom/google/android/youtube/p;->az:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/ui/u;->a(IZ)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->g()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public final d()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->g:Lcom/google/android/apps/youtube/core/ui/u;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/u;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->n:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setMinimized(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->g()V

    return-void
.end method

.method public final e()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;Lcom/google/android/apps/youtube/app/remote/ar;)V

    return-void
.end method

.method public setCcEnabled(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->l:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setCcEnabled(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->i()V

    return-void
.end method

.method public setHasCc(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->j:Z

    return-void
.end method

.method public setHasNext(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setHasNext(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->i()V

    return-void
.end method

.method public setHasPrevious(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setHasPrevious(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->i()V

    return-void
.end method

.method public setMinimized(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->o:Z

    return-void
.end method

.method public setPlayingOnText(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->m:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setScrubbingEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setScrubbingEnabled(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->i()V

    return-void
.end method

.method public setTimes(III)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->c:Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/player/overlay/DefaultControlsOverlay;->setTimes(III)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->i()V

    return-void
.end method
