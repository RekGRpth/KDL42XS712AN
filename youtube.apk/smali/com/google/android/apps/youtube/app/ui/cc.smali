.class public final Lcom/google/android/apps/youtube/app/ui/cc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

.field private final c:Lcom/google/android/apps/youtube/app/offline/f;

.field private final d:Lcom/google/android/apps/youtube/core/client/bj;

.field private final e:Lcom/google/android/apps/youtube/common/c/a;

.field private final f:Lcom/google/android/apps/youtube/app/am;

.field private final g:Lcom/google/android/apps/youtube/common/network/h;

.field private final h:Lcom/google/android/apps/youtube/app/offline/p;

.field private final i:Lcom/google/android/apps/youtube/app/ui/cf;

.field private j:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field private k:Landroid/widget/ListView;

.field private l:Lcom/google/android/apps/youtube/uilib/a/h;

.field private m:Lcom/google/android/apps/youtube/common/a/d;

.field private final n:Ljava/util/HashSet;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/app/offline/f;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/app/ui/cf;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/f;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->c:Lcom/google/android/apps/youtube/app/offline/f;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->d:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->e:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/am;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->f:Lcom/google/android/apps/youtube/app/am;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->g:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/p;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->h:Lcom/google/android/apps/youtube/app/offline/p;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/cf;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->i:Lcom/google/android/apps/youtube/app/ui/cf;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->n:Ljava/util/HashSet;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/cc;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/cc;)Lcom/google/android/apps/youtube/app/offline/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->c:Lcom/google/android/apps/youtube/app/offline/f;

    return-object v0
.end method

.method private b()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->m:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->m:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->m:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->j:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a()V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/cg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/cg;-><init>(Lcom/google/android/apps/youtube/app/ui/cc;B)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->m:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cc;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/cc;->m:Lcom/google/android/apps/youtube/common/a/d;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->b(Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/cc;)Lcom/google/android/apps/youtube/uilib/a/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->l:Lcom/google/android/apps/youtube/uilib/a/h;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/cc;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->n:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/ui/cc;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->j:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/ui/cc;)Lcom/google/android/apps/youtube/app/ui/cf;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->i:Lcom/google/android/apps/youtube/app/ui/cf;

    return-object v0
.end method

.method private handleOfflinePlaylistAddEvent(Lcom/google/android/apps/youtube/app/offline/a/q;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/q;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cc;->n:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/cc;->b()V

    :cond_0
    return-void
.end method

.method private handleOfflinePlaylistDeleteEvent(Lcom/google/android/apps/youtube/app/offline/a/s;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->l:Lcom/google/android/apps/youtube/uilib/a/h;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/ce;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/ui/ce;-><init>(Lcom/google/android/apps/youtube/app/ui/cc;Lcom/google/android/apps/youtube/app/offline/a/s;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Lcom/google/android/apps/youtube/common/fromguava/d;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/cc;->b()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/cc;->b()V

    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 9

    new-instance v8, Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->a:Landroid/app/Activity;

    invoke-direct {v8, v0}, Lcom/google/android/apps/youtube/app/ui/v;-><init>(Landroid/app/Activity;)V

    sget v0, Lcom/google/android/youtube/p;->fa:I

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/cd;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/ui/cd;-><init>(Lcom/google/android/apps/youtube/app/ui/cc;)V

    invoke-virtual {v8, v0, v1}, Lcom/google/android/apps/youtube/app/ui/v;->a(ILcom/google/android/apps/youtube/app/ui/ab;)I

    sget v0, Lcom/google/android/youtube/j;->cm:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->j:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    sget v0, Lcom/google/android/youtube/j;->dI:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->k:Landroid/widget/ListView;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cc;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/cc;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/cc;->e:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/cc;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/cc;->g:Lcom/google/android/apps/youtube/common/network/h;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/cc;->f:Lcom/google/android/apps/youtube/app/am;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/cc;->h:Lcom/google/android/apps/youtube/app/offline/p;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/app/ui/presenter/aw;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/app/ui/v;)V

    new-instance v1, Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/uilib/a/h;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cc;->l:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cc;->l:Lcom/google/android/apps/youtube/uilib/a/h;

    const-class v2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cc;->k:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cc;->l:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method
