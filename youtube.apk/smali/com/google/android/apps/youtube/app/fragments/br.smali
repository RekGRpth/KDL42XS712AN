.class final Lcom/google/android/apps/youtube/app/fragments/br;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/fragments/SearchFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/br;->a:Lcom/google/android/apps/youtube/app/fragments/SearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/br;-><init>(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->VIDEO:Lcom/google/android/apps/youtube/app/search/SearchType;

    sget-object v1, Lcom/google/android/apps/youtube/app/search/SearchType;->CHANNEL:Lcom/google/android/apps/youtube/app/search/SearchType;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/search/SearchType;->ordinal()I

    move-result v1

    if-ne v1, p3, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->CHANNEL:Lcom/google/android/apps/youtube/app/search/SearchType;

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/br;->a:Lcom/google/android/apps/youtube/app/fragments/SearchFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->g(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)Lcom/google/android/apps/youtube/app/search/SearchType;

    move-result-object v1

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/br;->a:Lcom/google/android/apps/youtube/app/fragments/SearchFragment;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;Lcom/google/android/apps/youtube/app/search/SearchType;)Lcom/google/android/apps/youtube/app/search/SearchType;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/br;->a:Lcom/google/android/apps/youtube/app/fragments/SearchFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->f(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)V

    :cond_1
    return-void

    :cond_2
    sget-object v1, Lcom/google/android/apps/youtube/app/search/SearchType;->PLAYLISTS:Lcom/google/android/apps/youtube/app/search/SearchType;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/search/SearchType;->ordinal()I

    move-result v1

    if-ne v1, p3, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->PLAYLISTS:Lcom/google/android/apps/youtube/app/search/SearchType;

    goto :goto_0
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method
