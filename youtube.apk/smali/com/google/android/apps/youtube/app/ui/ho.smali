.class final Lcom/google/android/apps/youtube/app/ui/ho;
.super Lcom/google/android/apps/youtube/app/ui/ib;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/hj;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/hj;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ho;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/app/ui/ib;-><init>(Lcom/google/android/apps/youtube/app/ui/hj;B)V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/ho;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ho;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->f(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "Favorite"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ho;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->e(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ho;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ho;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v2, v2, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    invoke-static {v2, p0}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->j(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 4

    const-string v0, "GData"

    const-string v1, "InvalidEntryException"

    const/4 v2, 0x0

    const-string v3, "Video already in favorite list."

    invoke-static {p2, v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ho;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    sget v1, Lcom/google/android/youtube/p;->j:I

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hj;I)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Error adding to favorites"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ho;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ho;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    sget v1, Lcom/google/android/youtube/p;->i:I

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hj;I)V

    return-void
.end method
