.class final Lcom/google/android/apps/youtube/app/ui/fe;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/ev;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/ui/ev;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/fe;->a:Lcom/google/android/apps/youtube/app/ui/ev;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/fe;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/fe;->c:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/ui/ev;Ljava/lang/String;Ljava/lang/String;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/ui/fe;-><init>(Lcom/google/android/apps/youtube/app/ui/ev;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "Error loading existing screens"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    check-cast p2, Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fe;->b:Ljava/lang/String;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreenName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fe;->a:Lcom/google/android/apps/youtube/app/ui/ev;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ev;->h(Lcom/google/android/apps/youtube/app/ui/ev;)Landroid/app/Activity;

    move-result-object v0

    const/16 v1, 0x403

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fe;->a:Lcom/google/android/apps/youtube/app/ui/ev;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ev;->g(Lcom/google/android/apps/youtube/app/ui/ev;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fe;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/ytremote/model/PairingCode;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/fe;->c:Ljava/lang/String;

    const-string v4, "\\D"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/ytremote/model/PairingCode;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/fe;->a:Lcom/google/android/apps/youtube/app/ui/ev;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/ui/ev;->f(Lcom/google/android/apps/youtube/app/ui/ev;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Ljava/lang/String;Lcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_1
.end method
