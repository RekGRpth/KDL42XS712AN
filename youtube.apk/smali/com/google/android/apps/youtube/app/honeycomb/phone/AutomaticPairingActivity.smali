.class public Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;
.super Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"


# instance fields
.field protected n:Lcom/google/android/apps/youtube/app/am;

.field private o:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

.field private p:Lcom/google/android/apps/youtube/core/Analytics;

.field private q:Lcom/google/android/apps/ytremote/model/CloudScreen;

.field private r:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

.field private s:Lcom/google/android/apps/youtube/app/remote/an;

.field private t:Lcom/google/android/apps/youtube/app/YouTubeApplication;

.field private u:Landroid/os/Handler;

.field private v:Lcom/google/android/apps/youtube/app/remote/bk;

.field private w:Lcom/google/android/apps/youtube/app/honeycomb/phone/f;

.field private x:Lcom/google/android/apps/youtube/common/a/b;

.field private y:Lcom/google/android/apps/youtube/common/a/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;)Lcom/google/android/apps/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->p:Lcom/google/android/apps/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->v:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->w:Lcom/google/android/apps/youtube/app/honeycomb/phone/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/aq;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->s:Lcom/google/android/apps/youtube/app/remote/an;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->y:Lcom/google/android/apps/youtube/common/a/b;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/app/remote/an;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;)V
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity$ConnectionErrorDialogFragment;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity$ConnectionErrorDialogFragment;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->c()Landroid/support/v4/app/l;

    move-result-object v1

    const-string v2, "automatic_connect_failed"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity$ConnectionErrorDialogFragment;->a(Landroid/support/v4/app/l;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;)Lcom/google/android/apps/youtube/app/honeycomb/phone/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->w:Lcom/google/android/apps/youtube/app/honeycomb/phone/f;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;)Lcom/google/android/apps/youtube/app/remote/bk;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->v:Lcom/google/android/apps/youtube/app/remote/bk;

    return-object v0
.end method

.method private e()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->u:Landroid/os/Handler;

    const/16 v1, 0x2710

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->u:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->e()V

    return-void
.end method


# virtual methods
.method public final d()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_auto_add_screen"

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v0, "cloud_screen"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/CloudScreen;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->q:Lcom/google/android/apps/ytremote/model/CloudScreen;

    const-string v0, "yt_tv_screen"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->r:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->t:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->t:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->p()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->s:Lcom/google/android/apps/youtube/app/remote/an;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->t:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->k()Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->v:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->t:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->l()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->o:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->t:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->D()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->p:Lcom/google/android/apps/youtube/core/Analytics;

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/f;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/f;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->w:Lcom/google/android/apps/youtube/app/honeycomb/phone/f;

    sget v0, Lcom/google/android/youtube/p;->C:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->b(I)V

    sget v0, Lcom/google/android/youtube/l;->j:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->n:Lcom/google/android/apps/youtube/app/am;

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/a;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->y:Lcom/google/android/apps/youtube/common/a/b;

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/b;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->x:Lcom/google/android/apps/youtube/common/a/b;

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/c;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->u:Landroid/os/Handler;

    return-void
.end method

.method protected onStart()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->r:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->v:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->r:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bg;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->v:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->e()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->v:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->v:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->w:Lcom/google/android/apps/youtube/app/honeycomb/phone/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/aq;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->r:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->q:Lcom/google/android/apps/ytremote/model/CloudScreen;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->o:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->q:Lcom/google/android/apps/ytremote/model/CloudScreen;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->x:Lcom/google/android/apps/youtube/common/a/b;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/ytremote/model/CloudScreen;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->v:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->w:Lcom/google/android/apps/youtube/app/honeycomb/phone/f;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->b(Lcom/google/android/apps/youtube/app/remote/aq;)V

    return-void
.end method
