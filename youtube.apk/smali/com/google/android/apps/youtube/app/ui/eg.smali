.class final Lcom/google/android/apps/youtube/app/ui/eg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/Analytics;

.field private final b:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

.field private final c:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field private final d:Lcom/google/android/apps/youtube/core/aw;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/app/remote/RemoteControl;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/aw;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/eg;->a:Lcom/google/android/apps/youtube/core/Analytics;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/eg;->b:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/eg;->c:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/ui/eg;->d:Lcom/google/android/apps/youtube/core/aw;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/app/remote/RemoteControl;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/aw;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/app/ui/eg;-><init>(Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/app/remote/RemoteControl;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/aw;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/eg;->d:Lcom/google/android/apps/youtube/core/aw;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/eg;->d:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/aw;->b(Ljava/lang/Throwable;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/eg;->b:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/eg;->c:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/eg;->d:Lcom/google/android/apps/youtube/core/aw;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/eg;->a:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {v0, p2, v1, v2, v3}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/Analytics;)V

    return-void
.end method
