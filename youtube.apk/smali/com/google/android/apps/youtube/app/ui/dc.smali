.class final Lcom/google/android/apps/youtube/app/ui/dc;
.super Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;
.source "SourceFile"


# instance fields
.field private Y:Lcom/google/android/apps/youtube/core/client/bj;

.field private Z:Landroid/view/View;

.field private aa:Landroid/widget/ImageView;

.field private ab:Landroid/widget/TextView;

.field private ac:Landroid/widget/TextView;

.field private ad:Landroid/widget/TextView;

.field private ae:Landroid/widget/TextView;

.field private af:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/dc;-><init>()V

    return-void
.end method

.method private E()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->af:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->sdThumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->af:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->sdThumbnailUri:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->af:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->hqThumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->af:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->hqThumbnailUri:Landroid/net/Uri;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->af:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->thumbnailUri:Landroid/net/Uri;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/dc;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->Z:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    sget v0, Lcom/google/android/youtube/l;->aK:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    sget v0, Lcom/google/android/youtube/j;->fA:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->Z:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->aa:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->ab:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->ag:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->ac:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->eK:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->ad:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->aH:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->ae:Landroid/widget/TextView;

    return-object v1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/dc;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->Y:Lcom/google/android/apps/youtube/core/client/bj;

    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;->c(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    return-object v0
.end method

.method public final d()V
    .locals 7

    const/4 v6, 0x0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;->d()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/dc;->h()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "playlist"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/dc;->a()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v1, "playlist"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    :try_start_0
    new-instance v1, Ljava/io/ObjectInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->af:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;
    :try_end_0
    .catch Ljava/io/StreamCorruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->ab:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/dc;->af:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->ac:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/dc;->af:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->author:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->ad:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/dc;->j()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/o;->j:I

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/dc;->af:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget v3, v3, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/dc;->af:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget v5, v5, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->af:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->summary:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->ae:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/dc;->E()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/dc;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/dc;->Y:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/dc;->E()Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/dc;->aa:Landroid/widget/ImageView;

    new-instance v4, Lcom/google/android/apps/youtube/app/ui/dd;

    invoke-direct {v4, p0}, Lcom/google/android/apps/youtube/app/ui/dd;-><init>(Lcom/google/android/apps/youtube/app/ui/dc;)V

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/app/d/a;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Landroid/net/Uri;Landroid/widget/ImageView;Lcom/google/android/apps/youtube/app/d/e;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/dc;->a()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/dc;->a()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/dc;->a()V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->ae:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/dc;->ae:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/dc;->af:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->summary:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
