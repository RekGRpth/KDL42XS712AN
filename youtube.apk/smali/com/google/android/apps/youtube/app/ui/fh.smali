.class public final Lcom/google/android/apps/youtube/app/ui/fh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Landroid/view/View;

.field private final c:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

.field private final d:Landroid/widget/LinearLayout;

.field private final e:Landroid/widget/ImageView;

.field private f:Lcom/google/android/apps/youtube/core/ui/PagedListView;

.field private g:Lcom/google/android/apps/youtube/app/ui/fk;

.field private h:Z

.field private final i:Landroid/widget/TextView;

.field private final j:Landroid/widget/TextView;

.field private final k:Landroid/widget/TextView;

.field private final l:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/res/Resources;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/fh;->h:Z

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->a:Landroid/content/res/Resources;

    sget v0, Lcom/google/android/youtube/j;->ey:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "parentView must contain the set_bar"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->b:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->eA:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "parentView must contain the set_content"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->d:Landroid/widget/LinearLayout;

    sget v0, Lcom/google/android/youtube/j;->eB:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "parentView must contain the set_content_slider"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->c:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->c:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(IZ)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->c:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/fj;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/app/ui/fj;-><init>(Lcom/google/android/apps/youtube/app/ui/fh;B)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->setListener(Lcom/google/android/apps/youtube/app/ui/ft;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->c:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->setEnabled(Z)V

    sget v0, Lcom/google/android/youtube/j;->ez:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->e:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->e:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/fl;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/app/ui/fl;-><init>(Lcom/google/android/apps/youtube/app/ui/fh;B)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->b:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/fl;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/app/ui/fl;-><init>(Lcom/google/android/apps/youtube/app/ui/fh;B)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->b:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->eH:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->i:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->b:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->eG:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->b:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->eD:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->b:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aE:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->l:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->j:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/fh;)Lcom/google/android/apps/youtube/app/ui/SliderLayout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->c:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/fh;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/fh;->h:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/fh;)Lcom/google/android/apps/youtube/app/ui/fk;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->g:Lcom/google/android/apps/youtube/app/ui/fk;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/fh;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->h:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/fh;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->e:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fh;->a:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/h;->t:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/ui/fk;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/fh;->g:Lcom/google/android/apps/youtube/app/ui/fk;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/datalib/innertube/model/ac;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->l:Landroid/widget/ImageView;

    invoke-virtual {p1, v0, p2}, Lcom/google/android/apps/youtube/app/ui/v;->a(Landroid/view/View;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/ui/PagedListView;)V
    .locals 3

    const/4 v2, -0x1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->f:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->f:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->d:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fh;->f:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/fh;->f:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->d:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p1, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final a(Z)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/fh;->b:Landroid/view/View;

    if-eqz p1, :cond_3

    move v3, v2

    :goto_1
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    if-nez p1, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/fh;->c:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(IZ)V

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/fh;->h:Z

    :cond_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->c:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(IZ)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->c:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/fi;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/ui/fi;-><init>(Lcom/google/android/apps/youtube/app/ui/fh;)V

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    const/16 v3, 0x8

    goto :goto_1
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fh;->a:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/h;->s:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->j:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->j:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->j:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fh;->l:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->j:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/fh;->b(Z)V

    return-void
.end method

.method public final c(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fh;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
