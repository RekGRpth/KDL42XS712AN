.class public final Lcom/google/android/apps/youtube/app/ui/df;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/fk;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/ae;

.field private final b:Lcom/google/android/apps/youtube/core/ui/PagedListView;

.field private final c:Landroid/widget/FrameLayout;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/ImageView;

.field private final f:Landroid/widget/ImageView;

.field private final g:Lcom/google/android/apps/youtube/uilib/a/h;

.field private h:Lcom/google/android/apps/youtube/app/ui/fh;

.field private final i:Landroid/content/res/Resources;

.field private final j:Lcom/google/android/apps/youtube/core/Analytics;

.field private final k:Lcom/google/android/apps/youtube/app/ui/dn;

.field private final l:Lcom/google/android/apps/youtube/app/ui/v;

.field private final m:Lcom/google/android/apps/youtube/app/ui/dm;

.field private final n:Lcom/google/android/apps/youtube/app/ui/dm;

.field private final o:Lcom/google/android/apps/youtube/app/ui/dm;

.field private final p:Lcom/google/android/apps/youtube/app/ui/cl;

.field private final q:Lcom/google/android/apps/youtube/app/offline/f;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lcom/google/android/apps/youtube/app/ui/dn;Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;Ljava/util/concurrent/atomic/AtomicReference;)V
    .locals 11

    const/4 v10, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->ac()Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/ae;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->a:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->i:Landroid/content/res/Resources;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->j:Lcom/google/android/apps/youtube/core/Analytics;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/df;->k:Lcom/google/android/apps/youtube/app/ui/dn;

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->Z()Lcom/google/android/apps/youtube/app/offline/p;

    move-result-object v7

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/f;

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v2

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v3

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v4

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v5

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v6

    new-instance v8, Lcom/google/android/apps/youtube/app/ui/bv;

    invoke-direct {v8, p1, v7}, Lcom/google/android/apps/youtube/app/ui/bv;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/offline/p;)V

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/app/offline/f;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/app/ui/bv;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->q:Lcom/google/android/apps/youtube/app/offline/f;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/cl;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/df;->j:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v3

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v4

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->x()Lcom/google/android/apps/youtube/datalib/innertube/v;

    move-result-object v5

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v6

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v7

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/app/ui/cl;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/datalib/innertube/v;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/c/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->p:Lcom/google/android/apps/youtube/app/ui/cl;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/dm;

    sget v1, Lcom/google/android/youtube/p;->cC:I

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/dm;-><init>(Lcom/google/android/apps/youtube/app/ui/df;I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->m:Lcom/google/android/apps/youtube/app/ui/dm;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/dm;

    sget v1, Lcom/google/android/youtube/p;->cG:I

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/dm;-><init>(Lcom/google/android/apps/youtube/app/ui/df;I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->n:Lcom/google/android/apps/youtube/app/ui/dm;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/dm;

    sget v1, Lcom/google/android/youtube/p;->cz:I

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/dm;-><init>(Lcom/google/android/apps/youtube/app/ui/df;I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->o:Lcom/google/android/apps/youtube/app/ui/dm;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/v;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/app/ui/v;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->l:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->l:Lcom/google/android/apps/youtube/app/ui/v;

    sget v1, Lcom/google/android/youtube/p;->cD:I

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/dg;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/ui/dg;-><init>(Lcom/google/android/apps/youtube/app/ui/df;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/v;->a(ILcom/google/android/apps/youtube/app/ui/ab;)I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->l:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/df;->m:Lcom/google/android/apps/youtube/app/ui/dm;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/dh;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/ui/dh;-><init>(Lcom/google/android/apps/youtube/app/ui/df;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/v;->a(Lcom/google/android/apps/youtube/app/ui/aa;Lcom/google/android/apps/youtube/app/ui/ab;)I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->l:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/df;->n:Lcom/google/android/apps/youtube/app/ui/dm;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/di;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/ui/di;-><init>(Lcom/google/android/apps/youtube/app/ui/df;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/v;->a(Lcom/google/android/apps/youtube/app/ui/aa;Lcom/google/android/apps/youtube/app/ui/ab;)I

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->l:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/df;->o:Lcom/google/android/apps/youtube/app/ui/dm;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/dj;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/ui/dj;-><init>(Lcom/google/android/apps/youtube/app/ui/df;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/v;->a(Lcom/google/android/apps/youtube/app/ui/aa;Lcom/google/android/apps/youtube/app/ui/ab;)I

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    sget v0, Lcom/google/android/youtube/l;->be:I

    invoke-virtual {v1, v0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->b:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    sget v0, Lcom/google/android/youtube/l;->bd:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/df;->b:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->c:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->b:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/df;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->b:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    sget v2, Lcom/google/android/youtube/l;->bc:I

    invoke-virtual {v1, v2, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->c:Landroid/widget/FrameLayout;

    sget v1, Lcom/google/android/youtube/j;->eC:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->d:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/dk;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/ui/dk;-><init>(Lcom/google/android/apps/youtube/app/ui/df;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->c:Landroid/widget/FrameLayout;

    sget v2, Lcom/google/android/youtube/j;->eE:I

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->e:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->c:Landroid/widget/FrameLayout;

    sget v2, Lcom/google/android/youtube/j;->eF:I

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->f:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/uilib/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->g:Lcom/google/android/apps/youtube/uilib/a/h;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/bz;

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q()Lcom/google/android/apps/youtube/datalib/d/a;

    move-result-object v3

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/df;->g:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-object v1, p1

    move-object v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/app/ui/presenter/bz;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/uilib/a/h;Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/df;->g:Lcom/google/android/apps/youtube/uilib/a/h;

    const-class v2, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->b:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/df;->g:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/df;)Lcom/google/android/apps/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->j:Lcom/google/android/apps/youtube/core/Analytics;

    return-object v0
.end method

.method private a(Lcom/google/android/apps/youtube/app/ui/LikeAction;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/dl;->c:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ui/LikeAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->m:Lcom/google/android/apps/youtube/app/ui/dm;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/ui/dm;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->n:Lcom/google/android/apps/youtube/app/ui/dm;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/dm;->a(Z)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->m:Lcom/google/android/apps/youtube/app/ui/dm;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/dm;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->n:Lcom/google/android/apps/youtube/app/ui/dm;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/ui/dm;->a(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/df;)Lcom/google/android/apps/youtube/app/ui/cl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->p:Lcom/google/android/apps/youtube/app/ui/cl;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/df;)Lcom/google/android/apps/youtube/app/offline/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->q:Lcom/google/android/apps/youtube/app/offline/f;

    return-object v0
.end method

.method private c()V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/fh;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/fh;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->g:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->b:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->b:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->d:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/df;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->e:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/ui/df;)Lcom/google/android/apps/youtube/app/ui/dn;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->k:Lcom/google/android/apps/youtube/app/ui/dn;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/ui/df;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->f:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/ui/df;)Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->i:Landroid/content/res/Resources;

    return-object v0
.end method

.method private handlePlaybackServiceException(Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/dl;->b:[I

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException;->reason:Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/event/PlaybackServiceException$ErrorReason;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/df;->c()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private handlePlaylistLikeActionEvent(Lcom/google/android/apps/youtube/app/ui/de;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ui/de;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/df;->a:Lcom/google/android/apps/youtube/core/player/ae;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/ae;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ui/de;->b()Lcom/google/android/apps/youtube/app/ui/LikeAction;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/LikeAction;->LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LikeAction;->REMOVE_LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/df;->a(Lcom/google/android/apps/youtube/app/ui/LikeAction;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, Lcom/google/android/apps/youtube/app/ui/LikeAction;->REMOVE_LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LikeAction;->LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/df;->a(Lcom/google/android/apps/youtube/app/ui/LikeAction;)V

    goto :goto_0
.end method

.method private handleRequestingWatchDataEvent(Lcom/google/android/apps/youtube/core/player/event/r;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/df;->c()V

    return-void
.end method

.method private handleSequencerHasPreviousNextEvent(Lcom/google/android/apps/youtube/core/player/event/u;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->e:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/u;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->f:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/u;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    return-void
.end method

.method private handleSequencerStageEvent(Lcom/google/android/apps/youtube/core/player/event/v;)V
    .locals 12
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const/16 v2, 0x8

    const/4 v11, 0x1

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/dl;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/v;->a()Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/player/sequencer/PlaybackSequencer$SequencerStage;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/df;->c()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/v;->c()Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->getPlaylistPanel()Lcom/google/android/apps/youtube/datalib/innertube/model/ac;

    move-result-object v4

    if-nez v4, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/fh;->a(Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    invoke-virtual {v0, v11}, Lcom/google/android/apps/youtube/app/ui/fh;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/df;->l:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-virtual {v0, v5, v4}, Lcom/google/android/apps/youtube/app/ui/fh;->a(Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/datalib/innertube/model/ac;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/apps/youtube/app/ui/fh;->a(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->e()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/apps/youtube/app/ui/fh;->b(Ljava/lang/CharSequence;)V

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->o:Lcom/google/android/apps/youtube/app/ui/dm;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/dm;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/youtube/app/ui/fh;->c(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/fh;->a()V

    :cond_2
    :goto_1
    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LikeAction;->LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/df;->a(Lcom/google/android/apps/youtube/app/ui/LikeAction;)V

    :goto_2
    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/df;->e:Landroid/widget/ImageView;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->isLoopSupported()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->f:Landroid/widget/ImageView;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->isShuffleSupported()Z

    move-result v3

    if-eqz v3, :cond_6

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->g:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->g:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->b:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->a(I)V

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->c()I

    move-result v0

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->f()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/df;->o:Lcom/google/android/apps/youtube/app/ui/dm;

    invoke-virtual {v6, v11}, Lcom/google/android/apps/youtube/app/ui/dm;->a(Z)V

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/df;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v6, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/df;->d:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/df;->i:Landroid/content/res/Resources;

    sget v8, Lcom/google/android/youtube/o;->l:I

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v1

    invoke-virtual {v7, v8, v5, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/app/ui/fh;->b()V

    if-ltz v0, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/df;->i:Landroid/content/res/Resources;

    sget v8, Lcom/google/android/youtube/p;->fz:I

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v9, v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v9, v11

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/android/apps/youtube/app/ui/fh;->c(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :pswitch_2
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LikeAction;->REMOVE_LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/df;->a(Lcom/google/android/apps/youtube/app/ui/LikeAction;)V

    goto/16 :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->m:Lcom/google/android/apps/youtube/app/ui/dm;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/dm;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->n:Lcom/google/android/apps/youtube/app/ui/dm;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/dm;->a(Z)V

    goto/16 :goto_2

    :cond_5
    move v0, v2

    goto/16 :goto_3

    :cond_6
    move v1, v2

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/fh;->b(Z)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/ui/fh;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/fh;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/df;->b:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/fh;->a(Lcom/google/android/apps/youtube/core/ui/PagedListView;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/ui/fh;->a(Lcom/google/android/apps/youtube/app/ui/fk;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/df;->c()V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/df;->h:Lcom/google/android/apps/youtube/app/ui/fh;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/fh;->b(Z)V

    :cond_0
    return-void
.end method
