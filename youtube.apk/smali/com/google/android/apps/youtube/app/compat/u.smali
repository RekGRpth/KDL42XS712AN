.class public final Lcom/google/android/apps/youtube/app/compat/u;
.super Lcom/google/android/apps/youtube/app/compat/t;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/MenuItem;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/compat/t;-><init>(Landroid/content/Context;Landroid/view/MenuItem;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/app/compat/r;)Lcom/google/android/apps/youtube/app/compat/q;
    .locals 2

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/u;->a:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;

    :goto_0
    return-object p0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/u;->a:Landroid/view/MenuItem;

    new-instance v1, Lcom/google/android/apps/youtube/app/compat/v;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/compat/v;-><init>(Lcom/google/android/apps/youtube/app/compat/u;Lcom/google/android/apps/youtube/app/compat/r;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/u;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->collapseActionView()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/u;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->expandActionView()Z

    move-result v0

    return v0
.end method

.method public final d()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/u;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/u;->a:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->isActionViewExpanded()Z

    move-result v0

    return v0
.end method
