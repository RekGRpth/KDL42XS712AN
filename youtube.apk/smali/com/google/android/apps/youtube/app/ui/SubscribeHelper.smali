.class public final Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/app/YouTubeApplication;

.field private final c:Lcom/google/android/apps/youtube/core/Analytics;

.field private final d:Lcom/google/android/apps/youtube/core/identity/ak;

.field private final e:Lcom/google/android/apps/youtube/core/identity/l;

.field private final f:Lcom/google/android/apps/youtube/core/identity/aa;

.field private final g:Lcom/google/android/apps/youtube/core/client/bc;

.field private final h:Lcom/google/android/apps/youtube/common/c/a;

.field private final i:Lcom/google/android/apps/youtube/core/aw;

.field private final j:Lcom/google/android/apps/youtube/app/ui/gf;

.field private final k:Lcom/google/android/apps/youtube/common/a/b;

.field private final l:Lcom/google/android/apps/youtube/common/a/b;

.field private m:Lcom/google/android/apps/youtube/core/identity/UserProfile;

.field private n:Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;

.field private o:Ljava/lang/String;

.field private p:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

.field private q:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

.field private r:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

.field private s:Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;

.field private t:Landroid/net/Uri;

.field private u:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/app/ui/gf;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->c:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/ak;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->d:Lcom/google/android/apps/youtube/core/identity/ak;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->e:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/aa;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->f:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->g:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->i:Lcom/google/android/apps/youtube/core/aw;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->h:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/gf;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->j:Lcom/google/android/apps/youtube/app/ui/gf;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->b:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/ge;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/ge;-><init>(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;B)V

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->k:Lcom/google/android/apps/youtube/common/a/b;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/gd;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/gd;-><init>(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;B)V

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->l:Lcom/google/android/apps/youtube/common/a/b;

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->WORKING:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->q:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->t:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->p:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;)Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->n:Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->s:Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;

    return-object p1
.end method

.method private a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->q:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->r:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->q:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->j:Lcom/google/android/apps/youtube/app/ui/gf;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->q:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/ui/gf;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->f()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/youtube/core/Analytics;Ljava/lang/String;IZ)V
    .locals 1

    if-eqz p3, :cond_1

    const-string v0, "UnsubscribeFromChannel"

    :goto_0
    invoke-interface {p0, v0}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    if-ltz p2, :cond_0

    if-nez p3, :cond_0

    const-string v0, "SubscribeDetails"

    invoke-interface {p0, v0, p1, p2}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_0
    return-void

    :cond_1
    const-string v0, "SubscribeToChannel"

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->j()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->i:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->i()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)Lcom/google/android/apps/youtube/common/c/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->h:Lcom/google/android/apps/youtube/common/c/a;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    return-object v0
.end method

.method private f()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->m:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->c:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->o:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->b:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->v()I

    move-result v2

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/core/Analytics;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->g:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->m:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->username:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/gg;

    invoke-direct {v3, p0, v4}, Lcom/google/android/apps/youtube/app/ui/gg;-><init>(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;B)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->m(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->t:Landroid/net/Uri;

    return-object v0
.end method

.method private g()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->n:Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->n:Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;->paidContent:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)Lcom/google/android/apps/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->g:Lcom/google/android/apps/youtube/core/client/bc;

    return-object v0
.end method

.method private h()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->m:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->e:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->m:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/identity/UserProfile;->username:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->d:Lcom/google/android/apps/youtube/core/identity/ak;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/ak;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->n:Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->p:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->p:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V

    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)V
    .locals 5

    const/4 v4, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->WORKING:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->e:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->c:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->o:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->b:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->v()I

    move-result v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/core/Analytics;Ljava/lang/String;IZ)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->t:Landroid/net/Uri;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->g:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->m:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->username:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/gi;

    invoke-direct {v3, p0, v4}, Lcom/google/android/apps/youtube/app/ui/gi;-><init>(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;B)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->g(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->g:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->t:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/gh;

    invoke-direct {v3, p0, v4}, Lcom/google/android/apps/youtube/app/ui/gh;-><init>(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;B)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->e(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->j()V

    goto :goto_0
.end method

.method private j()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->r:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->q:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->r:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->j:Lcom/google/android/apps/youtube/app/ui/gf;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->q:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/ui/gf;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V

    return-void
.end method

.method private k()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->g:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->m:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->username:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/client/bc;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://m.youtube.com/offer_details?it=U&ii="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->m:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v1, v1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->channelId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/core/utils/m;->b(Landroid/app/Activity;Landroid/net/Uri;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->WORKING:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->m:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/core/identity/UserProfile;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/core/identity/UserProfile;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->m:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->e:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->g:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->username:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->k:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->g(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->g:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/identity/UserProfile;->channelId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->l:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->i(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->NOT_SUBSCRIBED:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->p:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->i()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->o:Ljava/lang/String;

    return-void
.end method

.method public final b()V
    .locals 7

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->q:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->SUBSCRIBED:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->k()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->u:Landroid/app/Dialog;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/gc;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/gc;-><init>(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)V

    new-instance v1, Lcom/google/android/apps/youtube/core/ui/aa;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/core/ui/aa;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    sget v3, Lcom/google/android/youtube/p;->gn:I

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->m:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v6, v6, Lcom/google/android/apps/youtube/core/identity/UserProfile;->displayUsername:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/core/ui/aa;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040013    # android.R.string.yes

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040009    # android.R.string.no

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->u:Landroid/app/Dialog;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->u:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    sget v1, Lcom/google/android/youtube/p;->dE:I

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->k()V

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->WORKING:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->e:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->f()V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->f:Lcom/google/android/apps/youtube/core/identity/aa;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/gb;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/ui/gb;-><init>(Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ah;)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->q:Lcom/google/android/apps/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SubscribeHelper;->s:Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;

    return-object v0
.end method
