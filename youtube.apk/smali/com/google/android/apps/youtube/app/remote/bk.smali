.class public final Lcom/google/android/apps/youtube/app/remote/bk;
.super Lcom/google/android/apps/youtube/app/remote/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/client/bp;


# static fields
.field private static final a:Lorg/json/JSONObject;

.field private static final b:Ljava/util/regex/Pattern;

.field private static final c:Landroid/content/IntentFilter;

.field private static final d:Landroid/content/IntentFilter;

.field private static final e:Ljava/util/Map;

.field private static final f:Ljava/util/Random;


# instance fields
.field private A:I

.field private B:D

.field private C:J

.field private D:Z

.field private E:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

.field private final F:Lcom/google/android/apps/youtube/core/identity/ak;

.field private final G:Ljava/util/concurrent/Executor;

.field private final H:Lcom/google/android/apps/youtube/core/identity/b;

.field private final I:Lcom/google/android/apps/youtube/core/identity/l;

.field private final J:Lcom/google/android/apps/ytremote/logic/a;

.field private final K:Ljava/util/Map;

.field private L:Ljava/util/Set;

.field private M:Z

.field private final N:Ljava/lang/String;

.field private final O:Lcom/google/android/apps/youtube/app/ac;

.field private final P:Z

.field private final Q:Z

.field private final R:Z

.field private S:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

.field private final T:Ljava/util/List;

.field private final U:Z

.field private V:Ljava/lang/String;

.field private final W:Z

.field private final g:Landroid/content/Context;

.field private final h:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

.field private final i:Lcom/google/android/apps/ytremote/logic/d;

.field private final j:Lcom/google/android/apps/youtube/common/network/h;

.field private final k:Lcom/google/android/apps/youtube/app/remote/bo;

.field private final l:Lcom/google/android/apps/ytremote/backend/a/e;

.field private final m:Lcom/google/android/apps/ytremote/backend/logic/d;

.field private final n:Ljava/util/Map;

.field private final o:Ljava/util/Map;

.field private final p:Landroid/content/SharedPreferences;

.field private q:Z

.field private r:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

.field private s:Z

.field private t:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

.field private u:Lcom/google/android/apps/ytremote/model/CloudScreen;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private final x:Ljava/util/List;

.field private final y:Ljava/util/Set;

.field private final z:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/app/remote/bk;->a:Lorg/json/JSONObject;

    const-string v0, ".*#dial$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/app/remote/bk;->b:Ljava/util/regex/Pattern;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/app/remote/bk;->c:Landroid/content/IntentFilter;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/app/remote/bk;->d:Landroid/content/IntentFilter;

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/bk;->c:Landroid/content/IntentFilter;

    sget-object v1, Lcom/google/android/apps/ytremote/intent/Intents$IntentAction;->LOUNGE_SERVER_CONNECTION_ERROR:Lcom/google/android/apps/ytremote/intent/Intents$IntentAction;

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/intent/Intents$IntentAction;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/bk;->c:Landroid/content/IntentFilter;

    sget-object v1, Lcom/google/android/apps/ytremote/intent/Intents$IntentAction;->CLOUD_SERVICE_NO_NETWORK:Lcom/google/android/apps/ytremote/intent/Intents$IntentAction;

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/intent/Intents$IntentAction;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/bk;->c:Landroid/content/IntentFilter;

    sget-object v1, Lcom/google/android/apps/ytremote/intent/Intents$IntentAction;->CLOUD_SERVICE_IPV6_ERROR:Lcom/google/android/apps/ytremote/intent/Intents$IntentAction;

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/intent/Intents$IntentAction;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/bk;->d:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/bk;->d:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "LOUNGE_SCREEN"

    sget-object v2, Lcom/google/android/apps/ytremote/backend/model/DeviceType;->LOUNGE_SCREEN:Lcom/google/android/apps/ytremote/backend/model/DeviceType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "REMOTE_CONTROL"

    sget-object v2, Lcom/google/android/apps/ytremote/backend/model/DeviceType;->REMOTE_CONTROL:Lcom/google/android/apps/ytremote/backend/model/DeviceType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/app/remote/bk;->e:Ljava/util/Map;

    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/app/remote/bk;->f:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/network/h;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/b;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/client/bc;ZZZZLcom/google/android/apps/youtube/app/ac;Z)V
    .locals 17

    const/4 v15, 0x1

    const-string v1, "remote_id"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    move/from16 v12, p11

    move/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v1 .. v16}, Lcom/google/android/apps/youtube/app/remote/bk;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/network/h;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/b;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/client/bc;ZZZZLcom/google/android/apps/youtube/app/ac;ZLjava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/network/h;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/b;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/client/bc;ZZZZLcom/google/android/apps/youtube/app/ac;ZLjava/lang/String;)V
    .locals 9

    move-object/from16 v0, p8

    invoke-direct {p0, p1, p5, v0}, Lcom/google/android/apps/youtube/app/remote/e;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/client/bc;)V

    const/16 v1, 0x1e

    iput v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->A:I

    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->P:Z

    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->Q:Z

    move/from16 v0, p11

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->R:Z

    move/from16 v0, p12

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->W:Z

    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->U:Z

    invoke-static/range {p13 .. p13}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/ac;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->O:Lcom/google/android/apps/youtube/app/ac;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->p:Landroid/content/SharedPreferences;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/remote/bk;->F:Lcom/google/android/apps/youtube/core/identity/ak;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->I:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/identity/b;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->H:Lcom/google/android/apps/youtube/core/identity/b;

    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->G:Ljava/util/concurrent/Executor;

    invoke-static/range {p15 .. p15}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->N:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->g:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->j:Lcom/google/android/apps/youtube/common/network/h;

    new-instance v8, Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    sget-object v2, Lcom/google/android/apps/ytremote/backend/model/DeviceType;->REMOTE_CONTROL:Lcom/google/android/apps/ytremote/backend/model/DeviceType;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    new-instance v6, Lcom/google/android/apps/youtube/app/remote/br;

    const/4 v1, 0x0

    invoke-direct {v6, p0, v1}, Lcom/google/android/apps/youtube/app/remote/br;-><init>(Lcom/google/android/apps/youtube/app/remote/bk;B)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v1, "android-"

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "tablet"

    :goto_0
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "-"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v5, "device"

    invoke-virtual {v2}, Lcom/google/android/apps/ytremote/backend/model/DeviceType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "id"

    move-object/from16 v0, p15

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "name"

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "app"

    invoke-interface {v3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->W:Z

    if-eqz v1, :cond_0

    const-string v1, "mdx-version"

    const-string v2, "3"

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v7

    new-instance v1, Lcom/google/android/apps/ytremote/backend/browserchannel/t;

    const-string v3, "www.youtube.com"

    const/16 v4, 0x50

    const-string v5, "/api/lounge/bc/"

    move-object v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/ytremote/backend/browserchannel/t;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/apps/ytremote/backend/browserchannel/u;Ljava/util/Map;)V

    invoke-direct {v8, p1, v1}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;-><init>(Landroid/content/Context;Lcom/google/android/apps/ytremote/backend/browserchannel/c;)V

    iput-object v8, p0, Lcom/google/android/apps/youtube/app/remote/bk;->h:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    new-instance v1, Lcom/google/android/apps/ytremote/b/d;

    new-instance v2, Lcom/google/android/apps/ytremote/backend/a/l;

    invoke-direct {v2}, Lcom/google/android/apps/ytremote/backend/a/l;-><init>()V

    invoke-direct {v1, v2}, Lcom/google/android/apps/ytremote/b/d;-><init>(Lcom/google/android/apps/ytremote/backend/logic/b;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->i:Lcom/google/android/apps/ytremote/logic/d;

    new-instance v1, Lcom/google/android/apps/ytremote/b/c;

    invoke-direct {v1}, Lcom/google/android/apps/ytremote/b/c;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->J:Lcom/google/android/apps/ytremote/logic/a;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->n:Ljava/util/Map;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->o:Ljava/util/Map;

    new-instance v1, Lcom/google/android/apps/ytremote/backend/a/e;

    invoke-direct {v1}, Lcom/google/android/apps/ytremote/backend/a/e;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->l:Lcom/google/android/apps/ytremote/backend/a/e;

    new-instance v1, Lcom/google/android/apps/ytremote/backend/a/j;

    invoke-direct {v1}, Lcom/google/android/apps/ytremote/backend/a/j;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->m:Lcom/google/android/apps/ytremote/backend/logic/d;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/bo;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/app/remote/bo;-><init>(Lcom/google/android/apps/youtube/app/remote/bk;B)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->k:Lcom/google/android/apps/youtube/app/remote/bo;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->y:Ljava/util/Set;

    new-instance v1, Landroid/os/HandlerThread;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    new-instance v2, Lcom/google/android/apps/youtube/app/remote/bs;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v3, 0x0

    invoke-direct {v2, p0, v1, v3}, Lcom/google/android/apps/youtube/app/remote/bs;-><init>(Lcom/google/android/apps/youtube/app/remote/bk;Landroid/os/Looper;B)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->z:Landroid/os/Handler;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->r:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->K:Ljava/util/Map;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/bu;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/app/remote/bu;-><init>(Lcom/google/android/apps/youtube/app/remote/bk;B)V

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/bk;->d:Landroid/content/IntentFilter;

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->T:Ljava/util/List;

    return-void

    :cond_1
    const-string v1, "phone"

    goto/16 :goto_0
.end method

.method static synthetic A(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->v:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic A()Lorg/json/JSONObject;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/bk;->a:Lorg/json/JSONObject;

    return-object v0
.end method

.method static synthetic B(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->S:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    return-object v0
.end method

.method static synthetic B()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/bk;->e:Ljava/util/Map;

    return-object v0
.end method

.method private C()Ljava/lang/String;
    .locals 2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "RQ"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->u:Lcom/google/android/apps/ytremote/model/CloudScreen;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->u:Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getLoungeToken()Lcom/google/android/apps/ytremote/model/LoungeToken;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method static synthetic C(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    return-object v0
.end method

.method static synthetic D(Lcom/google/android/apps/youtube/app/remote/bk;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->A:I

    return v0
.end method

.method private D()Ljava/lang/String;
    .locals 3

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->I:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->H:Lcom/google/android/apps/youtube/core/identity/b;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->I:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/identity/b;->a(Lcom/google/android/apps/youtube/core/identity/l;)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/a;->c()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic E(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->T:Ljava/util/List;

    return-object v0
.end method

.method static synthetic F(Lcom/google/android/apps/youtube/app/remote/bk;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->D:Z

    return v0
.end method

.method static synthetic G(Lcom/google/android/apps/youtube/app/remote/bk;)D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->B:D

    return-wide v0
.end method

.method static synthetic H(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/common/network/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->j:Lcom/google/android/apps/youtube/common/network/h;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/bk;I)Lcom/google/android/apps/youtube/app/remote/as;
    .locals 1

    packed-switch p1, :pswitch_data_0

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/at;->c:Lcom/google/android/apps/youtube/app/remote/as;

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/youtube/app/remote/bj;->b:Lcom/google/android/apps/youtube/app/remote/as;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/youtube/app/remote/bj;->d:Lcom/google/android/apps/youtube/app/remote/as;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/android/apps/youtube/app/remote/bj;->c:Lcom/google/android/apps/youtube/app/remote/as;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/android/apps/youtube/app/remote/at;->a:Lcom/google/android/apps/youtube/app/remote/as;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->E:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/ytremote/model/CloudScreen;)Lcom/google/android/apps/ytremote/model/CloudScreen;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->u:Lcom/google/android/apps/ytremote/model/CloudScreen;

    return-object p1
.end method

.method public static a(Landroid/content/SharedPreferences;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-interface {p0, p1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    invoke-interface {p0, p1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/math/BigInteger;

    const/16 v1, 0x82

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/bk;->f:Ljava/util/Random;

    invoke-direct {v0, v1, v2}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/bk;->D()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/util/Set;)Ljava/util/Set;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->L:Ljava/util/Set;

    return-object p1
.end method

.method private a(D)V
    .locals 2

    iput-wide p1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->B:D

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->C:J

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/bk;D)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/remote/bk;->a(D)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;)V
    .locals 6

    const-wide/16 v2, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/bn;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->r:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v0, v1, :cond_1

    iput-wide v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->B:D

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->r:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v0, v1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->C:J

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->r:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->B:D

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/youtube/app/remote/bk;->C:J

    sub-long/2addr v2, v4

    long-to-double v2, v2

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->B:D

    goto :goto_0

    :pswitch_2
    iput-wide v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->B:D

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/bk;->e(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Sending "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->h:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/bk;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->D:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/remote/bk;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->A:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/remote/bk;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->g:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;)Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->r:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->S:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->w:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/remote/bk;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->q:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/model/CloudScreen;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->u:Lcom/google/android/apps/ytremote/model/CloudScreen;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->v:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/remote/bk;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->s:Z

    return p1
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/backend/browserchannel/k;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->h:Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/remote/bk;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->M:Z

    return p1
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/logic/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->i:Lcom/google/android/apps/ytremote/logic/d;

    return-object v0
.end method

.method private e(Ljava/lang/String;)V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->P:Z

    if-nez v0, :cond_0

    const-string v0, "true"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->O:Lcom/google/android/apps/youtube/app/ac;

    const-string v2, "enable_mdx_logs"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/app/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "YouTube MDX"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->K:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->r:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/app/remote/bk;)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/ytremote/backend/model/Method;->GET_NOW_PLAYING:Lcom/google/android/apps/ytremote/backend/model/Method;

    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/Params;->a:Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    return-void
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/app/remote/bk;)Z
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Connected remotes are "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->L:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->L:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/backend/model/c;

    iget-object v2, v0, Lcom/google/android/apps/ytremote/backend/model/c;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/bk;->N:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/ytremote/backend/model/c;->b:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/bk;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/logic/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->J:Lcom/google/android/apps/ytremote/logic/a;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/apps/youtube/app/remote/bk;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->q:Z

    return v0
.end method

.method static synthetic l(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/app/remote/bo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->k:Lcom/google/android/apps/youtube/app/remote/bo;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->y:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/apps/youtube/app/remote/bk;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->W:Z

    return v0
.end method

.method static synthetic o(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->n:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic p(Lcom/google/android/apps/youtube/app/remote/bk;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->p:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic q(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/backend/logic/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->m:Lcom/google/android/apps/ytremote/backend/logic/d;

    return-object v0
.end method

.method static synthetic r(Lcom/google/android/apps/youtube/app/remote/bk;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->U:Z

    return v0
.end method

.method static synthetic s(Lcom/google/android/apps/youtube/app/remote/bk;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->z:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic t(Lcom/google/android/apps/youtube/app/remote/bk;)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->U:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->F:Lcom/google/android/apps/youtube/core/identity/ak;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/ak;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->I:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->G:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/google/android/apps/youtube/app/remote/bl;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/youtube/app/remote/bl;-><init>(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic u(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->o:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic v(Lcom/google/android/apps/youtube/app/remote/bk;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->s:Z

    return v0
.end method

.method static synthetic w(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->t:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    return-object v0
.end method

.method static synthetic x(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->L:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic y(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->w:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic z()Landroid/content/IntentFilter;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/bk;->c:Landroid/content/IntentFilter;

    return-object v0
.end method

.method static synthetic z(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->E:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)Lcom/google/android/apps/ytremote/model/CloudScreen;
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/CloudScreen;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->hasAppStatus()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getAppStatus()Lcom/google/android/apps/ytremote/model/AppStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/AppStatus;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v0

    :goto_1
    if-nez v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/SsdpId;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->p:Landroid/content/SharedPreferences;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/apps/ytremote/model/ScreenId;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/bk;->p:Landroid/content/SharedPreferences;

    const-string v4, ""

    invoke-interface {v3, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/ytremote/model/ScreenId;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->l:Lcom/google/android/apps/ytremote/backend/a/e;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/apps/ytremote/model/ScreenId;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/ytremote/backend/a/e;->a(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/LoungeToken;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Unable to retrieve lounge token for screenId "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_0

    :cond_1
    move-object v0, v2

    goto :goto_1

    :cond_2
    move-object v0, v2

    goto :goto_0

    :cond_3
    new-instance v3, Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v1, v4, v2, v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;-><init>(Lcom/google/android/apps/ytremote/model/ScreenId;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ClientName;Lcom/google/android/apps/ytremote/model/LoungeToken;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->n:Ljava/util/Map;

    invoke-interface {v0, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v3

    goto :goto_0

    :cond_4
    move-object v1, v0

    goto :goto_2
.end method

.method protected final a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Remote control moved to state "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->e(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/remote/e;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Ljava/lang/String;JLjava/lang/String;ILcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V
    .locals 10

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/bk;->C()Ljava/lang/String;

    move-result-object v7

    :goto_0
    invoke-virtual {p0, p2, v7}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->S:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->t:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {p1, v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    move/from16 v0, p6

    invoke-virtual {p0, p2, v7, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->t:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->L:Ljava/util/Set;

    const-wide/16 v1, 0x1388

    cmp-long v1, p3, v1

    if-gez v1, :cond_2

    const-wide/16 v5, 0x0

    :goto_2
    iget-object v9, p0, Lcom/google/android/apps/youtube/app/remote/bk;->G:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/bm;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/youtube/app/remote/bm;-><init>(Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Ljava/lang/String;JLjava/lang/String;I)V

    invoke-interface {v9, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_2
    move-wide v5, p3

    goto :goto_2

    :cond_3
    move-object v7, p5

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/apps/youtube/app/remote/as;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Remote control moved to error state with error "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->e(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/remote/e;->a(Lcom/google/android/apps/youtube/app/remote/as;)V

    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x0

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->V:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->w:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->E:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->r:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->r:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->v:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->z:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->z:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/remote/bk;->b(Ljava/lang/String;)V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->UNSTARTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->r:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8

    const-wide/16 v3, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/bk;->C()Ljava/lang/String;

    move-result-object v5

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->ERROR:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->t:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/remote/bk;->S:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-object v0, p0

    move-object v2, p1

    move v6, p3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Ljava/lang/String;JLjava/lang/String;ILcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->W:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->V:Ljava/lang/String;

    if-nez v5, :cond_3

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/backend/model/Params;-><init>()V

    const-string v1, "listId"

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v1, "currentTime"

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v1, "currentIndex"

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/Method;->SET_PLAYLIST:Lcom/google/android/apps/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    invoke-virtual {p0, p1, v5}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    invoke-virtual {v5, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->r:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/ytremote/backend/model/Method;->PLAY:Lcom/google/android/apps/ytremote/backend/model/Method;

    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/Params;->a:Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/backend/model/Params;-><init>()V

    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v1, "videoSources"

    const-string v2, "XX"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/Method;->INSERT_VIDEO:Lcom/google/android/apps/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/backend/model/Params;-><init>()V

    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v1, "currentTime"

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/Method;->SET_VIDEO:Lcom/google/android/apps/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/bk;->C()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->r:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/ytremote/backend/model/Method;->PLAY:Lcom/google/android/apps/ytremote/backend/model/Method;

    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/Params;->a:Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    goto/16 :goto_1

    :cond_8
    move-object v5, p2

    goto/16 :goto_0
.end method

.method public final a(Ljava/util/List;I)V
    .locals 4

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-ltz p2, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    new-instance v1, Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {v1}, Lcom/google/android/apps/ytremote/backend/model/Params;-><init>()V

    const-string v2, "videoId"

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v0, "currentTime"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v0, "videoSources"

    const-string v2, "XX"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v0, "videoIds"

    const-string v2, ","

    invoke-static {v2, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    sget-object v0, Lcom/google/android/apps/ytremote/backend/model/Method;->SET_PLAYLIST:Lcom/google/android/apps/ytremote/backend/model/Method;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized a(Z)V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->z:Landroid/os/Handler;

    const/4 v1, 0x4

    new-instance v2, Lcom/google/android/apps/youtube/app/remote/bq;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/bk;->t:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-direct {v2, v3, p1}, Lcom/google/android/apps/youtube/app/remote/bq;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Z)V

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->M:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->s:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->t:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->u:Lcom/google/android/apps/ytremote/model/CloudScreen;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->y:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    const/16 v1, 0x1e

    iput v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->A:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->T:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->z:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->z:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->privacy:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;->PRIVATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    if-eq v2, v3, :cond_0

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PRIVATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-ne v2, v3, :cond_4

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->Q:Z

    if-eqz v2, :cond_6

    :cond_1
    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->NOT_AVAILABLE_ON_MOBILE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-eq v2, v3, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_6

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isLive()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->R:Z

    if-eqz v2, :cond_6

    :cond_2
    iget-boolean v2, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->adultContent:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->y:Ljava/util/Set;

    iget-object v3, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-eq v2, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isLive()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PROCESSING:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-ne v2, v3, :cond_6

    :cond_3
    :goto_2
    return v0

    :cond_4
    move v2, v1

    goto :goto_0

    :cond_5
    move v2, v1

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public final b(I)V
    .locals 3

    const/16 v0, 0x64

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->A:I

    add-int/2addr v2, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->A:I

    new-instance v0, Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/backend/model/Params;-><init>()V

    const-string v1, "delta"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v1, "volume"

    iget v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->A:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/Method;->SET_VOLUME:Lcom/google/android/apps/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    iget v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->A:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->T:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->A:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V
    .locals 4

    new-instance v0, Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/backend/model/Params;-><init>()V

    const-string v1, "format"

    iget v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->format:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v1, "kind"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v1, "languageCode"

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v1, "languageName"

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->languageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v1, "sourceLanguageCode"

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->sourceLanguageCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v1, "trackName"

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v1, "videoId"

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->g:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/bk;->p:Landroid/content/SharedPreferences;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->a()F

    move-result v2

    new-instance v3, Lorg/json/JSONObject;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitlesStyle;->getMdxFormat(F)Ljava/util/HashMap;

    move-result-object v1

    invoke-direct {v3, v1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    const-string v1, "style"

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/Method;->SET_SUBTITLES_TRACK:Lcom/google/android/apps/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    return-void
.end method

.method public final b(Ljava/util/List;)V
    .locals 3

    new-instance v0, Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/backend/model/Params;-><init>()V

    const-string v1, "videoIds"

    const-string v2, ","

    invoke-static {v2, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v1, "videoSources"

    const-string v2, "XX"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/Method;->ADD_VIDEOS:Lcom/google/android/apps/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    return-void
.end method

.method public final c(I)V
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->D:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->z:Landroid/os/Handler;

    const/4 v1, 0x5

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    new-instance v0, Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/backend/model/Params;-><init>()V

    const-string v1, "newTime"

    div-int/lit16 v2, p1, 0x3e8

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/Method;->SEEK_TO:Lcom/google/android/apps/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    int-to-double v0, p1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(D)V

    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/backend/model/Params;-><init>()V

    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v1, "videoSources"

    const-string v2, "XX"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/Method;->ADD_VIDEO:Lcom/google/android/apps/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    return-void
.end method

.method protected final d()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->z:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->SLEEP:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->t:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    if-nez v0, :cond_1

    const-string v0, "We should reconnect, but we lost the screen"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->q:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->g:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->k:Lcom/google/android/apps/youtube/app/remote/bo;

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/bk;->c:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->z:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public final d(I)V
    .locals 3

    new-instance v0, Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/backend/model/Params;-><init>()V

    const-string v1, "volume"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    iput p1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->A:I

    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/Method;->SET_VOLUME:Lcom/google/android/apps/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->T:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/backend/model/Params;-><init>()V

    const-string v1, "videoId"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/Method;->REMOVE_VIDEO:Lcom/google/android/apps/ytremote/backend/model/Method;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    return-void
.end method

.method public final e()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/ytremote/backend/model/Method;->PLAY:Lcom/google/android/apps/ytremote/backend/model/Method;

    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/Params;->a:Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    return-void
.end method

.method public final f()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/ytremote/backend/model/Method;->PAUSE:Lcom/google/android/apps/ytremote/backend/model/Method;

    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/Params;->a:Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    return-void
.end method

.method public final g()V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/ytremote/backend/model/Method;->STOP:Lcom/google/android/apps/ytremote/backend/model/Method;

    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/Params;->a:Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    return-void
.end method

.method public final h()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->v:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->v:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final i()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/bk;->h()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->v:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->V:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {v1}, Lcom/google/android/apps/ytremote/backend/model/Params;-><init>()V

    const-string v2, "videoId"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v0, "currentTime"

    const-string v2, "0"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    sget-object v0, Lcom/google/android/apps/ytremote/backend/model/Method;->SET_VIDEO:Lcom/google/android/apps/ytremote/backend/model/Method;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    goto :goto_0
.end method

.method public final j()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/bk;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->v:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final k()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->v:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->v:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final l()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/bk;->k()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->v:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final m()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/bk;->k()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->w:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->V:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {v1}, Lcom/google/android/apps/ytremote/backend/model/Params;-><init>()V

    const-string v2, "videoId"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v0, "currentTime"

    const-string v2, "0"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    sget-object v0, Lcom/google/android/apps/ytremote/backend/model/Method;->SET_VIDEO:Lcom/google/android/apps/ytremote/backend/model/Method;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    goto :goto_0
.end method

.method public final n()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->r:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    return-object v0
.end method

.method public final bridge synthetic o()Lcom/google/android/apps/youtube/app/remote/bg;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->t:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    return-object v0
.end method

.method public final onSignOut(Lcom/google/android/apps/youtube/core/identity/aj;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->U:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/ytremote/backend/model/Method;->UPDATE_USERNAME:Lcom/google/android/apps/ytremote/backend/model/Method;

    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/Params;->a:Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/backend/model/Method;Lcom/google/android/apps/ytremote/backend/model/Params;)V

    :cond_0
    return-void
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final q()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->v:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final r()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->u:Lcom/google/android/apps/ytremote/model/CloudScreen;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->u:Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getLoungeToken()Lcom/google/android/apps/ytremote/model/LoungeToken;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RQ"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->u:Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getLoungeToken()Lcom/google/android/apps/ytremote/model/LoungeToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->A:I

    return v0
.end method

.method public final t()D
    .locals 6

    iget-wide v2, p0, Lcom/google/android/apps/youtube/app/remote/bk;->B:D

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->r:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v0, v1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/google/android/apps/youtube/app/remote/bk;->C:J

    sub-long/2addr v0, v4

    :goto_0
    long-to-double v0, v0

    add-double/2addr v0, v2

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final u()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->x:Ljava/util/List;

    return-object v0
.end method

.method public final v()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->M:Z

    return v0
.end method

.method public final w()Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->E:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    return-object v0
.end method

.method public final x()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->t:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    return-object v0
.end method

.method public final y()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->t:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->t:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreen()Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->o:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bk;->t:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreen()Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/ClientName;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/ClientName;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bk;->t:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreen()Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getClientName()Lcom/google/android/apps/ytremote/model/ClientName;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/ClientName;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
