.class public final Lcom/google/android/apps/youtube/app/adapter/ay;
.super Lcom/google/android/apps/youtube/app/adapter/g;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/youtube/app/ui/v;

.field private final c:Ljava/util/WeakHashMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/ui/v;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/adapter/g;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ay;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/v;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ay;->b:Lcom/google/android/apps/youtube/app/ui/v;

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ay;->c:Ljava/util/WeakHashMap;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/ay;)Ljava/util/WeakHashMap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ay;->c:Ljava/util/WeakHashMap;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/adapter/ay;)Lcom/google/android/apps/youtube/app/ui/v;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ay;->b:Lcom/google/android/apps/youtube/app/ui/v;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/adapter/ay;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ay;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/youtube/app/adapter/ae;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/az;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/az;-><init>(Lcom/google/android/apps/youtube/app/adapter/ay;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->RUNNING:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-eq v0, v2, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ay;->c:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/az;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/az;->a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
