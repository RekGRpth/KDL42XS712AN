.class final Lcom/google/android/apps/youtube/app/g;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/g;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/g;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/g;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->b(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->d()Lcom/google/android/apps/youtube/core/offline/store/i;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/g;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->b(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->e()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/core/offline/store/i;->r(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAdBreakId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v5, v1}, Lcom/google/android/apps/youtube/core/offline/store/i;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/offline/store/i;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/h;

    iget-object v4, v0, Lcom/google/android/apps/youtube/core/offline/store/h;->b:Lcom/google/android/apps/youtube/core/offline/store/e;

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/google/android/apps/youtube/core/offline/store/h;->b:Lcom/google/android/apps/youtube/core/offline/store/e;

    iget-object v4, v4, Lcom/google/android/apps/youtube/core/offline/store/e;->a:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, v0, Lcom/google/android/apps/youtube/core/offline/store/h;->b:Lcom/google/android/apps/youtube/core/offline/store/e;

    iget-object v4, v4, Lcom/google/android/apps/youtube/core/offline/store/e;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/youtube/core/offline/store/i;->s(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/offline/store/h;->b:Lcom/google/android/apps/youtube/core/offline/store/e;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/offline/store/e;->a:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/g;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    const-string v1, "All offline ad playback counts have been incremented!"

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    new-instance v0, Lcom/google/android/apps/youtube/app/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/g;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/app/h;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;B)V

    new-array v1, v2, [Ljava/lang/Void;

    const/4 v2, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/h;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
