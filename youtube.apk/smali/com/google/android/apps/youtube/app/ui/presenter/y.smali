.class public final Lcom/google/android/apps/youtube/app/ui/presenter/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/g;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/ui/gr;

.field private final b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/view/View;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/google/android/youtube/j;->ah:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-direct {v1, p1, v0}, Lcom/google/android/apps/youtube/app/ui/gr;-><init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/y;->a:Lcom/google/android/apps/youtube/app/ui/gr;

    sget v0, Lcom/google/android/youtube/j;->bo:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/y;->b:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/i;)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/y;->a:Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/i;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/gr;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/y;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/i;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/i;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/y;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/i;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
