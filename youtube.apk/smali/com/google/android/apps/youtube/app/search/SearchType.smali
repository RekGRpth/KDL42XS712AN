.class public final enum Lcom/google/android/apps/youtube/app/search/SearchType;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/app/search/SearchType;

.field public static final enum CHANNEL:Lcom/google/android/apps/youtube/app/search/SearchType;

.field public static final CHANNEL_SEARCH_MODIFIER:Ljava/lang/String; = "is:channel"

.field public static final DEFAULT_SEARCH_TYPE:Lcom/google/android/apps/youtube/app/search/SearchType;

.field public static final enum PLAYLISTS:Lcom/google/android/apps/youtube/app/search/SearchType;

.field public static final PLAYLISTS_SEARCH_MODIFIER:Ljava/lang/String; = "is:playlists"

.field public static final enum VIDEO:Lcom/google/android/apps/youtube/app/search/SearchType;


# instance fields
.field private final descriptionId:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/app/search/SearchType;

    const-string v1, "VIDEO"

    sget v2, Lcom/google/android/youtube/p;->hf:I

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/youtube/app/search/SearchType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->VIDEO:Lcom/google/android/apps/youtube/app/search/SearchType;

    new-instance v0, Lcom/google/android/apps/youtube/app/search/SearchType;

    const-string v1, "CHANNEL"

    sget v2, Lcom/google/android/youtube/p;->ak:I

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/youtube/app/search/SearchType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->CHANNEL:Lcom/google/android/apps/youtube/app/search/SearchType;

    new-instance v0, Lcom/google/android/apps/youtube/app/search/SearchType;

    const-string v1, "PLAYLISTS"

    sget v2, Lcom/google/android/youtube/p;->dQ:I

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/youtube/app/search/SearchType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->PLAYLISTS:Lcom/google/android/apps/youtube/app/search/SearchType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/youtube/app/search/SearchType;

    sget-object v1, Lcom/google/android/apps/youtube/app/search/SearchType;->VIDEO:Lcom/google/android/apps/youtube/app/search/SearchType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/app/search/SearchType;->CHANNEL:Lcom/google/android/apps/youtube/app/search/SearchType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/app/search/SearchType;->PLAYLISTS:Lcom/google/android/apps/youtube/app/search/SearchType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->$VALUES:[Lcom/google/android/apps/youtube/app/search/SearchType;

    sget-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->VIDEO:Lcom/google/android/apps/youtube/app/search/SearchType;

    sput-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->DEFAULT_SEARCH_TYPE:Lcom/google/android/apps/youtube/app/search/SearchType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/app/search/SearchType;->descriptionId:I

    return-void
.end method

.method public static fromQuery(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/search/SearchType;
    .locals 1

    if-eqz p0, :cond_1

    const-string v0, "is:channel"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->CHANNEL:Lcom/google/android/apps/youtube/app/search/SearchType;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "is:playlists"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->PLAYLISTS:Lcom/google/android/apps/youtube/app/search/SearchType;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->DEFAULT_SEARCH_TYPE:Lcom/google/android/apps/youtube/app/search/SearchType;

    goto :goto_0
.end method

.method public static fromString(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/search/SearchType;
    .locals 2

    if-nez p0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->DEFAULT_SEARCH_TYPE:Lcom/google/android/apps/youtube/app/search/SearchType;

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/youtube/app/search/SearchType;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/search/SearchType;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Attempted to search with unsupported SEARCH_TYPE: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->DEFAULT_SEARCH_TYPE:Lcom/google/android/apps/youtube/app/search/SearchType;

    goto :goto_0
.end method

.method public static removeModifiersFromQuery(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string v0, "is:channel"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "is:playlists"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/search/SearchType;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/app/search/SearchType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/search/SearchType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/app/search/SearchType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->$VALUES:[Lcom/google/android/apps/youtube/app/search/SearchType;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/app/search/SearchType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/app/search/SearchType;

    return-object v0
.end method


# virtual methods
.method public final getDescriptionStringResourceId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/search/SearchType;->descriptionId:I

    return v0
.end method
