.class public Lcom/google/android/apps/youtube/app/fragments/GuideFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/d/ai;


# instance fields
.field private Y:Lcom/google/android/apps/youtube/app/ax;

.field private Z:Lcom/google/android/apps/youtube/core/aw;

.field private a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

.field private aa:Lcom/google/android/apps/youtube/datalib/innertube/j;

.field private ab:Lcom/google/android/apps/youtube/datalib/offline/n;

.field private ac:Lcom/google/android/apps/youtube/core/identity/l;

.field private ad:Lcom/google/android/apps/youtube/app/navigation/AppNavigator;

.field private ae:Lcom/google/android/apps/youtube/app/d/ae;

.field private af:Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

.field private ag:Lcom/google/android/apps/youtube/uilib/a/h;

.field private ah:Ljava/util/ArrayList;

.field private b:Landroid/widget/ListView;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/ProgressBar;

.field private e:Landroid/widget/LinearLayout;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/Button;

.field private i:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;)Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->af:Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;Ljava/lang/String;Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 4

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->f:Landroid/widget/ImageView;

    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->h:Landroid/widget/Button;

    if-eqz p2, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private a(Z)V
    .locals 3

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ab:Lcom/google/android/apps/youtube/datalib/offline/n;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/offline/n;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->b(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ac:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ae:Lcom/google/android/apps/youtube/app/d/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/d/ae;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->i:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/f;->f:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ae:Lcom/google/android/apps/youtube/app/d/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/d/ae;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->b(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->g:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ae:Lcom/google/android/apps/youtube/app/d/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/d/ae;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->aa:Lcom/google/android/apps/youtube/datalib/innertube/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/j;->a()Lcom/google/android/apps/youtube/datalib/innertube/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/l;->a(Z)Lcom/google/android/apps/youtube/datalib/innertube/l;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->aa:Lcom/google/android/apps/youtube/datalib/innertube/j;

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/ab;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/fragments/ab;-><init>(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/j;->a(Lcom/google/android/apps/youtube/datalib/innertube/l;Lcom/google/android/apps/youtube/datalib/a/l;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)Lcom/google/android/apps/youtube/app/WatchWhileActivity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    return-object v0
.end method

.method private b(Z)V
    .locals 4

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->i:Landroid/view/View;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->j()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/youtube/p;->cg:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ac:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a(Ljava/lang/String;Z)V

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->b:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)Lcom/google/android/apps/youtube/app/navigation/AppNavigator;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ad:Lcom/google/android/apps/youtube/app/navigation/AppNavigator;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)Lcom/google/android/apps/youtube/app/ax;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->Y:Lcom/google/android/apps/youtube/app/ax;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)Lcom/google/android/apps/youtube/uilib/a/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ag:Lcom/google/android/apps/youtube/uilib/a/h;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ag:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ah:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    instance-of v0, v1, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ag:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ah:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)V
    .locals 3

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->b:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a()V

    return-void
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->Z:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    sget v0, Lcom/google/android/youtube/l;->af:I

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ae:Lcom/google/android/apps/youtube/app/d/ae;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/d/ae;->a(Landroid/view/View;)V

    sget v0, Lcom/google/android/youtube/j;->eR:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->c:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->c:Landroid/view/View;

    const-string v2, "progressbar"

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->d:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->c:Landroid/view/View;

    const-string v2, "error"

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->e:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->c:Landroid/view/View;

    const-string v2, "error_message"

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->c:Landroid/view/View;

    const-string v2, "alert_error"

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->f:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->c:Landroid/view/View;

    const-string v2, "retry_button"

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->h:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->h:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/t;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/fragments/t;-><init>(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->by:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->b:Landroid/widget/ListView;

    new-instance v0, Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/uilib/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ag:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ag:Lcom/google/android/apps/youtube/uilib/a/h;

    const-class v2, Lcom/google/android/apps/youtube/datalib/innertube/model/a/g;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/u;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/app/fragments/u;-><init>(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ag:Lcom/google/android/apps/youtube/uilib/a/h;

    const-class v2, Lcom/google/android/apps/youtube/datalib/innertube/model/a/f;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/v;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/app/fragments/v;-><init>(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ag:Lcom/google/android/apps/youtube/uilib/a/h;

    const-class v2, Lcom/google/android/apps/youtube/datalib/innertube/model/a/i;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/w;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/app/fragments/w;-><init>(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ag:Lcom/google/android/apps/youtube/uilib/a/h;

    const-class v2, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/x;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/app/fragments/x;-><init>(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ag:Lcom/google/android/apps/youtube/uilib/a/h;

    const-class v2, Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/y;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/app/fragments/y;-><init>(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->b:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ag:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->b:Landroid/widget/ListView;

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/z;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/fragments/z;-><init>(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->bx:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->i:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->i:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/aa;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/fragments/aa;-><init>(Lcom/google/android/apps/youtube/app/fragments/GuideFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->Y:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aP()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "show_channel_store_turorial"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->Y:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aP()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "show_channel_store_turorial"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    return-object v1
.end method

.method public final a()V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->af:Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->af:Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->b(Z)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->af:Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ag:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/uilib/a/h;->notifyDataSetChanged()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->o()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->getNavigationEndpoint()Lcom/google/a/a/a/a/kz;

    move-result-object v1

    move-object v2, v1

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ag:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/uilib/a/h;->getCount()I

    move-result v3

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ag:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v4, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;

    if-nez v4, :cond_3

    instance-of v4, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    if-eqz v4, :cond_3

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->a(Lcom/google/a/a/a/a/kz;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->b(Z)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->af:Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ag:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->notifyDataSetChanged()V

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_4
    move-object v2, v1

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const-string v1, "GuideFragment must be attached to an instance of GuideActivity."

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->Y:Lcom/google/android/apps/youtube/app/ax;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->Y:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->Z:Lcom/google/android/apps/youtube/core/aw;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->Y:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->s()Lcom/google/android/apps/youtube/datalib/innertube/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->aa:Lcom/google/android/apps/youtube/datalib/innertube/j;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->Y:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->m()Lcom/google/android/apps/youtube/datalib/offline/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ab:Lcom/google/android/apps/youtube/datalib/offline/n;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->Y:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ac:Lcom/google/android/apps/youtube/core/identity/l;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->Y:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->a(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/common/c/a;)Lcom/google/android/apps/youtube/app/navigation/AppNavigator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ad:Lcom/google/android/apps/youtube/app/navigation/AppNavigator;

    new-instance v0, Lcom/google/android/apps/youtube/app/d/ae;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->Y:Lcom/google/android/apps/youtube/app/ax;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/app/d/ae;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/ax;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ae:Lcom/google/android/apps/youtube/app/d/ae;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ae:Lcom/google/android/apps/youtube/app/d/ae;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/d/ae;->a(Lcom/google/android/apps/youtube/app/d/ai;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ah:Ljava/util/ArrayList;

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->m()V

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->e(Landroid/os/Bundle;)V

    return-void
.end method

.method public handleChannelSubscribed(Lcom/google/android/apps/youtube/app/ui/p;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const-string v0, "Guide refreshing due to channel subscription "

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a(Z)V

    return-void
.end method

.method public handleChannelUnsubscribed(Lcom/google/android/apps/youtube/app/ui/q;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const-string v0, "Guide refreshing due to channel un-subscription "

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a(Z)V

    return-void
.end method

.method public handleConnectivityChanged(Lcom/google/android/apps/youtube/common/network/a;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a(Z)V

    :cond_0
    return-void
.end method

.method public handlePlaylistAddAction(Lcom/google/android/apps/youtube/app/ui/cp;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const-string v0, "Guide refreshing due to playlist addition"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a(Z)V

    return-void
.end method

.method public handlePlaylistDeleteAction(Lcom/google/android/apps/youtube/app/ui/cq;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const-string v0, "Guide refreshing due to playlist deletion"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a(Z)V

    return-void
.end method

.method public handlePlaylistLikeAction(Lcom/google/android/apps/youtube/app/ui/de;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const-string v0, "Guide refreshing due to playlist like action"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a(Z)V

    return-void
.end method

.method public handleProfileChanged(Lcom/google/android/apps/youtube/core/identity/n;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ae:Lcom/google/android/apps/youtube/app/d/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/d/ae;->a()V

    return-void
.end method

.method public handleSignIn(Lcom/google/android/apps/youtube/core/identity/ai;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a(Z)V

    return-void
.end method

.method public handleSignOut(Lcom/google/android/apps/youtube/core/identity/aj;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a(Z)V

    return-void
.end method

.method public final r()V
    .locals 3

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->r()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->Y:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->Y:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/b/r;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/app/b/r;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ac:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ac:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->d()Lcom/google/android/apps/youtube/core/identity/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/z;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->ac:Lcom/google/android/apps/youtube/core/identity/l;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->Y:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->Y:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/l;->a(Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/common/c/a;)V

    :cond_0
    return-void
.end method

.method public final s()V
    .locals 1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->s()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/GuideFragment;->Y:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    return-void
.end method
