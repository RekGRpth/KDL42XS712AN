.class public final Lcom/google/android/apps/youtube/app/offline/sync/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/util/HashMap;

.field private final b:Lcom/google/android/apps/youtube/core/identity/l;

.field private final c:Lcom/google/android/apps/youtube/core/offline/store/q;

.field private final d:Lcom/google/android/apps/youtube/datalib/innertube/ad;

.field private final e:Lcom/google/android/apps/youtube/common/e/b;

.field private final f:Ljava/util/Set;

.field private final g:Lcom/google/android/apps/youtube/common/d/j;

.field private final h:Lcom/google/android/apps/youtube/core/au;

.field private volatile i:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

.field private j:Lcom/google/android/apps/youtube/app/offline/sync/c;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/datalib/innertube/ad;Lcom/google/android/apps/youtube/common/d/j;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/au;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->a:Ljava/util/HashMap;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->b:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/q;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->c:Lcom/google/android/apps/youtube/core/offline/store/q;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/ad;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->d:Lcom/google/android/apps/youtube/datalib/innertube/ad;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->e:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/d/j;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->g:Lcom/google/android/apps/youtube/common/d/j;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->f:Ljava/util/Set;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/au;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->h:Lcom/google/android/apps/youtube/core/au;

    return-void
.end method

.method private static a(J)I
    .locals 2

    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const v0, 0x7fffffff

    :goto_0
    return v0

    :cond_0
    long-to-int v0, p0

    goto :goto_0
.end method

.method private a(Ljava/util/Collection;)Lcom/google/a/a/a/a/mb;
    .locals 3

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->d:Lcom/google/android/apps/youtube/datalib/innertube/ad;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/ad;->a()Lcom/google/android/apps/youtube/datalib/innertube/af;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/af;->a(Ljava/util/Collection;)Lcom/google/android/apps/youtube/datalib/innertube/af;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->d:Lcom/google/android/apps/youtube/datalib/innertube/ad;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ad;->a(Lcom/google/android/apps/youtube/datalib/innertube/af;)Lcom/google/a/a/a/a/mb;

    move-result-object v0

    const-string v1, "Offline refresh response returned!"

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Offlined video set update count: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/google/a/a/a/a/mb;->c:[Lcom/google/a/a/a/a/mn;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Contains continuation?: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/google/a/a/a/a/mb;->d:Lcom/google/a/a/a/a/mc;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "Failed to reach offline refresh service: "

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(JLcom/google/a/a/a/a/md;Lcom/google/android/apps/youtube/datalib/legacy/model/v;)V
    .locals 3

    invoke-virtual {p4}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->a()Ljava/lang/String;

    move-result-object v0

    iget v1, p3, Lcom/google/a/a/a/a/md;->e:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget v1, p3, Lcom/google/a/a/a/a/md;->e:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->f:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void

    :pswitch_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/app/offline/sync/b;->b(JLcom/google/a/a/a/a/md;Lcom/google/android/apps/youtube/datalib/legacy/model/v;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->i:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->g(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    :try_start_0
    new-instance v1, Lcom/google/a/a/a/a/md;

    invoke-direct {v1}, Lcom/google/a/a/a/a/md;-><init>()V

    invoke-static {p3}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/a/a/a/a/md;->e:I

    invoke-direct {p0, p1, p2, v1, p4}, Lcom/google/android/apps/youtube/app/offline/sync/b;->b(JLcom/google/a/a/a/a/md;Lcom/google/android/apps/youtube/datalib/legacy/model/v;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->f:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "Error parsing the original OfflineState"

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    :try_start_1
    new-instance v1, Lcom/google/a/a/a/a/md;

    invoke-direct {v1}, Lcom/google/a/a/a/a/md;-><init>()V

    invoke-static {p3}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/a/a/a/a/md;->e:I

    invoke-direct {p0, p1, p2, v1, p4}, Lcom/google/android/apps/youtube/app/offline/sync/b;->b(JLcom/google/a/a/a/a/md;Lcom/google/android/apps/youtube/datalib/legacy/model/v;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->i:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->m(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v1, "Error parsing the original OfflineState"

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(Ljava/lang/String;J)V
    .locals 6

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->d:Lcom/google/android/apps/youtube/datalib/innertube/ad;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/ad;->a()Lcom/google/android/apps/youtube/datalib/innertube/af;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/af;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/af;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->d:Lcom/google/android/apps/youtube/datalib/innertube/ad;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ad;->a(Lcom/google/android/apps/youtube/datalib/innertube/af;)Lcom/google/a/a/a/a/mb;

    move-result-object v0

    const-string v1, "Offline refresh continuation response returned!"

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Offlined video set update count: %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v0, Lcom/google/a/a/a/a/mb;->c:[Lcom/google/a/a/a/a/mn;

    array-length v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v1, v0, Lcom/google/a/a/a/a/mb;->c:[Lcom/google/a/a/a/a/mn;

    array-length v1, v1

    if-lez v1, :cond_0

    iget-object v1, v0, Lcom/google/a/a/a/a/mb;->c:[Lcom/google/a/a/a/a/mn;

    iget v0, v0, Lcom/google/a/a/a/a/mb;->f:I

    invoke-direct {p0, v1, v0, p2, p3}, Lcom/google/android/apps/youtube/app/offline/sync/b;->a([Lcom/google/a/a/a/a/mn;IJ)V
    :try_end_0
    .catch Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to reach offline refresh service: "

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private a([Lcom/google/a/a/a/a/mn;IJ)V
    .locals 9

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    array-length v3, p1

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v0, p1, v2

    iget-object v4, v0, Lcom/google/a/a/a/a/mn;->b:Lcom/google/a/a/a/a/md;

    iget-object v5, v0, Lcom/google/a/a/a/a/mn;->c:[Lcom/google/a/a/a/a/mo;

    array-length v5, v5

    if-nez v5, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->i:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iget-object v5, v4, Lcom/google/a/a/a/a/md;->b:Ljava/lang/String;

    invoke-interface {v0, v5}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->d(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    invoke-direct {p0, p3, p4, v4, v0}, Lcom/google/android/apps/youtube/app/offline/sync/b;->a(JLcom/google/a/a/a/a/md;Lcom/google/android/apps/youtube/datalib/legacy/model/v;)V

    goto :goto_1

    :cond_0
    iget-object v5, v0, Lcom/google/a/a/a/a/mn;->c:[Lcom/google/a/a/a/a/mo;

    array-length v6, v5

    move v0, v1

    :goto_2
    if-ge v0, v6, :cond_1

    aget-object v7, v5, v0

    iget-object v7, v7, Lcom/google/a/a/a/a/mo;->b:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->i:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-interface {v8, v7}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    move-result-object v7

    invoke-direct {p0, p3, p4, v4, v7}, Lcom/google/android/apps/youtube/app/offline/sync/b;->a(JLcom/google/a/a/a/a/md;Lcom/google/android/apps/youtube/datalib/legacy/model/v;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->i:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    int-to-long v1, p2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->i:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->f:Ljava/util/Set;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method private b(JLcom/google/a/a/a/a/md;Lcom/google/android/apps/youtube/datalib/legacy/model/v;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->i:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-virtual {p4}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->i()Lcom/google/android/apps/youtube/datalib/legacy/model/w;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    invoke-direct {v2, p3}, Lcom/google/android/apps/youtube/datalib/innertube/model/v;-><init>(Lcom/google/a/a/a/a/md;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/v;)Lcom/google/android/apps/youtube/datalib/legacy/model/w;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->b(J)Lcom/google/android/apps/youtube/datalib/legacy/model/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/v;)Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 15

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->b:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->c:Lcom/google/android/apps/youtube/core/offline/store/q;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->b:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/q;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->i:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->e:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v2

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->i:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->g()Ljava/util/Map;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->i:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->e()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->w()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f()Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->c()J

    move-result-wide v8

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->e()J

    move-result-wide v10

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sub-long v8, v2, v8

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/google/android/apps/youtube/app/offline/sync/b;->a(J)I

    move-result v0

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sub-long v9, v2, v10

    invoke-virtual {v8, v9, v10}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/google/android/apps/youtube/app/offline/sync/b;->a(J)I

    move-result v8

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "Refreshing video %s: Time since last refreshed: %d"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v7, v11, v12

    const/4 v12, 0x1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->d()J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v9, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    new-instance v9, Lcom/google/a/a/a/a/mj;

    invoke-direct {v9}, Lcom/google/a/a/a/a/mj;-><init>()V

    iput-object v7, v9, Lcom/google/a/a/a/a/mj;->b:Ljava/lang/String;

    iput v0, v9, Lcom/google/a/a/a/a/mj;->c:I

    iput v8, v9, Lcom/google/a/a/a/a/mj;->d:I

    invoke-interface {v5, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v7, v9, Lcom/google/a/a/a/a/mj;->e:[Lcom/google/a/a/a/a/ml;

    invoke-interface {v0, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/a/a/a/a/ml;

    iput-object v0, v9, Lcom/google/a/a/a/a/mj;->e:[Lcom/google/a/a/a/a/ml;

    :cond_3
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/mm;

    move-object v1, v0

    :goto_2
    iget-object v0, v1, Lcom/google/a/a/a/a/mm;->c:[Lcom/google/a/a/a/a/mj;

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/google/a/a/a/a/mj;

    const/4 v8, 0x0

    aput-object v9, v7, v8

    invoke-static {v0, v7}, Lcom/google/android/apps/youtube/common/e/c;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/a/a/a/a/mj;

    iput-object v0, v1, Lcom/google/a/a/a/a/mm;->c:[Lcom/google/a/a/a/a/mj;

    goto/16 :goto_1

    :cond_4
    new-instance v0, Lcom/google/a/a/a/a/mm;

    invoke-direct {v0}, Lcom/google/a/a/a/a/mm;-><init>()V

    iput-object v1, v0, Lcom/google/a/a/a/a/mm;->b:Ljava/lang/String;

    invoke-virtual {v4, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    goto :goto_2

    :cond_5
    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/sync/b;->a(Ljava/util/Collection;)Lcom/google/a/a/a/a/mb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/a/a/a/a/mb;->c:[Lcom/google/a/a/a/a/mn;

    iget v4, v0, Lcom/google/a/a/a/a/mb;->f:I

    invoke-direct {p0, v1, v4, v2, v3}, Lcom/google/android/apps/youtube/app/offline/sync/b;->a([Lcom/google/a/a/a/a/mn;IJ)V

    iget-object v1, v0, Lcom/google/a/a/a/a/mb;->d:Lcom/google/a/a/a/a/mc;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/a/a/a/a/mb;->d:Lcom/google/a/a/a/a/mc;

    iget-object v1, v1, Lcom/google/a/a/a/a/mc;->b:Lcom/google/a/a/a/a/ly;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/a/a/a/a/mb;->d:Lcom/google/a/a/a/a/mc;

    iget-object v0, v0, Lcom/google/a/a/a/a/mc;->b:Lcom/google/a/a/a/a/ly;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget v1, v0, Lcom/google/a/a/a/a/ly;->c:I

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->h:Lcom/google/android/apps/youtube/core/au;

    invoke-interface {v4}, Lcom/google/android/apps/youtube/core/au;->Q()I

    move-result v4

    if-le v1, v4, :cond_6

    new-instance v4, Lcom/google/android/apps/youtube/app/offline/sync/c;

    invoke-direct {v4, p0, v0, v2, v3}, Lcom/google/android/apps/youtube/app/offline/sync/c;-><init>(Lcom/google/android/apps/youtube/app/offline/sync/b;Lcom/google/a/a/a/a/ly;J)V

    iput-object v4, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->j:Lcom/google/android/apps/youtube/app/offline/sync/c;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->g:Lcom/google/android/apps/youtube/common/d/j;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v5, v1

    invoke-virtual {v4, v5, v6}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    add-long v1, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/offline/sync/d;->a(J)Lcom/google/android/apps/youtube/a/a/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/d/j;->b(Lcom/google/android/apps/youtube/a/a/g;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v1, "OfflineSyncController: Thread.sleep interrupted: "

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_6
    if-lez v1, :cond_7

    :try_start_1
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v5, v1

    invoke-virtual {v4, v5, v6}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    :cond_7
    iget-object v0, v0, Lcom/google/a/a/a/a/ly;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/youtube/app/offline/sync/b;->a(Ljava/lang/String;J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->j:Lcom/google/android/apps/youtube/app/offline/sync/c;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->e:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->j:Lcom/google/android/apps/youtube/app/offline/sync/c;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/offline/sync/c;->b()J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    iput-object v4, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->j:Lcom/google/android/apps/youtube/app/offline/sync/c;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->h:Lcom/google/android/apps/youtube/core/au;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/au;->R()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/offline/sync/b;->a()V

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->j:Lcom/google/android/apps/youtube/app/offline/sync/c;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/offline/sync/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/youtube/app/offline/sync/b;->a(Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v4, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->j:Lcom/google/android/apps/youtube/app/offline/sync/c;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v4, p0, Lcom/google/android/apps/youtube/app/offline/sync/b;->j:Lcom/google/android/apps/youtube/app/offline/sync/c;

    throw v0
.end method
