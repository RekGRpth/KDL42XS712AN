.class public final Lcom/google/android/apps/youtube/app/ui/presenter/cc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/innertube/j;


# instance fields
.field private A:Lcom/google/android/apps/youtube/uilib/a/j;

.field private B:Lcom/google/android/apps/youtube/uilib/a/j;

.field private C:Lcom/google/android/apps/youtube/uilib/a/j;

.field private D:Lcom/google/android/apps/youtube/uilib/a/j;

.field private E:Lcom/google/android/apps/youtube/uilib/a/j;

.field private F:Lcom/google/android/apps/youtube/app/ui/v;

.field private final a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

.field private final b:Lcom/google/android/apps/youtube/datalib/d/a;

.field private final c:Lcom/google/android/apps/youtube/common/c/a;

.field private final d:Lcom/google/android/apps/youtube/core/client/bj;

.field private final e:Lcom/google/android/apps/youtube/core/identity/l;

.field private final f:Lcom/google/android/apps/youtube/core/identity/as;

.field private final g:Lcom/google/android/apps/youtube/core/identity/aa;

.field private final h:Lcom/google/android/apps/youtube/app/ui/a;

.field private final i:Lcom/google/android/apps/youtube/datalib/innertube/bc;

.field private final j:Lcom/google/android/apps/youtube/datalib/innertube/av;

.field private final k:Ljava/util/concurrent/atomic/AtomicReference;

.field private final l:Lcom/google/android/apps/youtube/core/aw;

.field private final m:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

.field private n:Lcom/google/android/apps/youtube/uilib/a/j;

.field private o:Lcom/google/android/apps/youtube/uilib/a/j;

.field private p:Lcom/google/android/apps/youtube/uilib/a/j;

.field private q:Lcom/google/android/apps/youtube/uilib/a/j;

.field private r:Lcom/google/android/apps/youtube/uilib/a/j;

.field private s:Lcom/google/android/apps/youtube/uilib/a/j;

.field private t:Lcom/google/android/apps/youtube/uilib/a/j;

.field private u:Lcom/google/android/apps/youtube/uilib/a/j;

.field private v:Lcom/google/android/apps/youtube/uilib/a/j;

.field private w:Lcom/google/android/apps/youtube/uilib/a/j;

.field private x:Lcom/google/android/apps/youtube/uilib/a/j;

.field private y:Lcom/google/android/apps/youtube/uilib/a/j;

.field private z:Lcom/google/android/apps/youtube/uilib/a/j;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/as;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/app/ui/a;Lcom/google/android/apps/youtube/datalib/innertube/bc;Lcom/google/android/apps/youtube/datalib/innertube/av;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/d/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->d:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->e:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/as;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->f:Lcom/google/android/apps/youtube/core/identity/as;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/aa;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->g:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->h:Lcom/google/android/apps/youtube/app/ui/a;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->i:Lcom/google/android/apps/youtube/datalib/innertube/bc;

    invoke-static {p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/av;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->j:Lcom/google/android/apps/youtube/datalib/innertube/av;

    invoke-static {p11}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicReference;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {p12}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->l:Lcom/google/android/apps/youtube/core/aw;

    invoke-static {p13}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->m:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    return-void
.end method

.method private a()Lcom/google/android/apps/youtube/uilib/a/j;
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->w:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/ah;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->f()Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/ah;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/app/ui/v;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->w:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->w:Lcom/google/android/apps/youtube/uilib/a/j;

    return-object v0
.end method

.method private b()Lcom/google/android/apps/youtube/uilib/a/j;
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->x:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/ab;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/ui/presenter/ab;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->x:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->x:Lcom/google/android/apps/youtube/uilib/a/j;

    return-object v0
.end method

.method private c()Lcom/google/android/apps/youtube/uilib/a/j;
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->y:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/ad;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/ui/ad;->c(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/ad;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/app/ui/v;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->y:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->y:Lcom/google/android/apps/youtube/uilib/a/j;

    return-object v0
.end method

.method private d()Lcom/google/android/apps/youtube/uilib/a/j;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->A:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/al;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/app/ui/presenter/al;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->A:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->A:Lcom/google/android/apps/youtube/uilib/a/j;

    return-object v0
.end method

.method private e()Lcom/google/android/apps/youtube/uilib/a/j;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->C:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/an;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/presenter/an;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/c/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->C:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->C:Lcom/google/android/apps/youtube/uilib/a/j;

    return-object v0
.end method

.method private f()Lcom/google/android/apps/youtube/app/ui/v;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->F:Lcom/google/android/apps/youtube/app/ui/v;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->c(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->F:Lcom/google/android/apps/youtube/app/ui/v;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->F:Lcom/google/android/apps/youtube/app/ui/v;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/uilib/a/h;)V
    .locals 12

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/model/s;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->o:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/ar;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/youtube/app/ui/presenter/ar;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/datalib/d/a;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->o:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->o:Lcom/google/android/apps/youtube/uilib/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v11, Lcom/google/android/apps/youtube/datalib/innertube/model/at;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->p:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/da;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->j:Lcom/google/android/apps/youtube/datalib/innertube/av;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->e:Lcom/google/android/apps/youtube/core/identity/l;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->f:Lcom/google/android/apps/youtube/core/identity/as;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->g:Lcom/google/android/apps/youtube/core/identity/aa;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->i:Lcom/google/android/apps/youtube/datalib/innertube/bc;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->c:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v9, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->l:Lcom/google/android/apps/youtube/core/aw;

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/youtube/app/ui/presenter/da;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/datalib/innertube/av;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/as;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/datalib/innertube/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/datalib/d/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->p:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->p:Lcom/google/android/apps/youtube/uilib/a/j;

    invoke-virtual {p1, v11, v0}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/model/r;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->u:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v1, :cond_2

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/ap;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/app/ui/presenter/ap;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->u:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->u:Lcom/google/android/apps/youtube/uilib/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/model/as;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->s:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v1, :cond_3

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/cy;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v5, v6}, Lcom/google/android/apps/youtube/app/ui/ad;->e(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/youtube/app/ui/presenter/cy;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/app/ui/v;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->s:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->s:Lcom/google/android/apps/youtube/uilib/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/model/aa;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->t:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v1, :cond_4

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/bj;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v5}, Lcom/google/android/apps/youtube/app/ui/ad;->d(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/youtube/app/ui/presenter/bj;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/app/ui/v;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->t:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->t:Lcom/google/android/apps/youtube/uilib/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v6, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->q:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/cg;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v4, v5}, Lcom/google/android/apps/youtube/app/ui/ad;->f(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->h:Lcom/google/android/apps/youtube/app/ui/a;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/presenter/cg;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->q:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->q:Lcom/google/android/apps/youtube/uilib/a/j;

    invoke-virtual {p1, v6, v0}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/model/k;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->r:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v1, :cond_6

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/aj;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->h:Lcom/google/android/apps/youtube/app/ui/a;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/youtube/app/ui/presenter/aj;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/ui/a;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->r:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->r:Lcom/google/android/apps/youtube/uilib/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/model/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->n:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v1, :cond_7

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/n;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/n;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->n:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->n:Lcom/google/android/apps/youtube/uilib/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->b(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->b(Lcom/google/android/apps/youtube/uilib/a/h;)V

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/uilib/a/h;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->m:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->f()Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;)V

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a()Lcom/google/android/apps/youtube/uilib/a/j;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/model/g;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->B:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/af;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/af;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->B:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->B:Lcom/google/android/apps/youtube/uilib/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->b()Lcom/google/android/apps/youtube/uilib/a/j;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/model/f;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->c()Lcom/google/android/apps/youtube/uilib/a/j;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v0, Lcom/google/android/apps/youtube/uilib/innertube/l;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->d()Lcom/google/android/apps/youtube/uilib/a/j;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v0, Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->e()Lcom/google/android/apps/youtube/uilib/a/j;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    return-void
.end method

.method public final c(Lcom/google/android/apps/youtube/uilib/a/h;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->m:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->f()Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;)V

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ar;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->D:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/ci;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/ci;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->D:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->D:Lcom/google/android/apps/youtube/uilib/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a()Lcom/google/android/apps/youtube/uilib/a/j;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->b()Lcom/google/android/apps/youtube/uilib/a/j;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/model/f;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->c()Lcom/google/android/apps/youtube/uilib/a/j;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v0, Lcom/google/android/apps/youtube/uilib/innertube/u;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->E:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v1, :cond_1

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/cp;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/youtube/app/ui/presenter/cp;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/datalib/d/a;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->E:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->E:Lcom/google/android/apps/youtube/uilib/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    return-void
.end method

.method public final d(Lcom/google/android/apps/youtube/uilib/a/h;)V
    .locals 7

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->z:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/cb;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->b:Lcom/google/android/apps/youtube/datalib/d/a;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-static {v5, v6}, Lcom/google/android/apps/youtube/app/ui/ad;->g(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/youtube/app/ui/presenter/cb;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/app/ui/v;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->z:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->z:Lcom/google/android/apps/youtube/uilib/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v0, Lcom/google/android/apps/youtube/uilib/innertube/l;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->d()Lcom/google/android/apps/youtube/uilib/a/j;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    const-class v0, Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->e()Lcom/google/android/apps/youtube/uilib/a/j;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    return-void
.end method

.method public final e(Lcom/google/android/apps/youtube/uilib/a/h;)V
    .locals 2

    const-class v0, Lcom/google/android/apps/youtube/uilib/a/m;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->v:Lcom/google/android/apps/youtube/uilib/a/j;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/youtube/uilib/a/l;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/uilib/a/l;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->v:Lcom/google/android/apps/youtube/uilib/a/j;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cc;->v:Lcom/google/android/apps/youtube/uilib/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    return-void
.end method
