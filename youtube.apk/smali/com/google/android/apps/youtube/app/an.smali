.class public final Lcom/google/android/apps/youtube/app/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# static fields
.field private static final a:Lcom/google/android/apps/youtube/datalib/legacy/model/be;

.field private static final b:Lcom/google/android/apps/youtube/datalib/legacy/model/be;

.field private static final c:Lcom/google/android/apps/youtube/datalib/legacy/model/be;

.field private static final d:Lcom/google/android/apps/youtube/datalib/legacy/model/be;

.field private static final e:Lcom/google/android/apps/youtube/datalib/legacy/model/be;


# instance fields
.field private final f:Lcom/google/android/apps/youtube/core/identity/l;

.field private final g:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

.field private final h:Lcom/google/android/apps/youtube/core/async/af;

.field private final i:Lcom/google/android/apps/youtube/core/async/af;

.field private final j:Lcom/google/android/apps/youtube/core/async/af;

.field private final k:Ljava/lang/String;

.field private final l:Ljava/util/concurrent/ConcurrentHashMap;

.field private final m:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    sget v1, Lcom/google/android/youtube/p;->cb:I

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/be;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/an;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    sget v1, Lcom/google/android/youtube/p;->ca:I

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/be;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/an;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    sget v1, Lcom/google/android/youtube/p;->bZ:I

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/be;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/an;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    sget v1, Lcom/google/android/youtube/p;->cc:I

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/be;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/an;->d:Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    sget v1, Lcom/google/android/youtube/p;->bY:I

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/be;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/an;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/identity/l;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/an;->f:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-interface {p1}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/an;->g:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-interface {p1}, Lcom/google/android/apps/youtube/core/client/bc;->k()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/an;->h:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {p1}, Lcom/google/android/apps/youtube/core/client/bc;->s()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/an;->i:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {p1}, Lcom/google/android/apps/youtube/core/client/bc;->t()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/an;->j:Lcom/google/android/apps/youtube/core/async/af;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/an;->k:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/an;->l:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v0, "users/([^/]*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/an;->m:Ljava/util/regex/Pattern;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/an;Lcom/google/android/apps/youtube/core/async/GDataRequest;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/an;->m:Ljava/util/regex/Pattern;

    iget-object v1, p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/an;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/an;->l:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/an;Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/an;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 9

    const/4 v5, 0x0

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/an;->g:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->q()Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v6

    sget-object v0, Lcom/google/android/apps/youtube/app/an;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    invoke-virtual {v4, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/an;->g:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    sget-object v1, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_POPULAR:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/an;->k:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;->THIS_WEEK:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v7

    sget-object v0, Lcom/google/android/apps/youtube/app/an;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    invoke-virtual {v4, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/an;->g:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    sget-object v1, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_DISCUSSED:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/an;->k:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;->THIS_WEEK:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v8

    sget-object v0, Lcom/google/android/apps/youtube/app/an;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    invoke-virtual {v4, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/app/ao;

    const v5, 0x7fffffff

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ao;-><init>(Lcom/google/android/apps/youtube/app/an;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/util/HashMap;I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/an;->j:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v1, v6, v0}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/an;->h:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v1, v7, v0}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/an;->h:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v1, v8, v0}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method static synthetic b()Lcom/google/android/apps/youtube/datalib/legacy/model/be;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/an;->d:Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/an;->l:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 9

    const/4 v8, 0x0

    move-object v3, p1

    check-cast v3, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/an;->f:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/an;->g:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    sget-object v1, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;->RECENTLY_FEATURED:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/an;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v8, v2, v8}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v6

    sget-object v0, Lcom/google/android/apps/youtube/app/an;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    invoke-virtual {v4, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/an;->g:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    sget-object v1, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_POPULAR:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/an;->k:Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;->THIS_WEEK:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v0, v1, v8, v2, v5}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v7

    sget-object v0, Lcom/google/android/apps/youtube/app/an;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    invoke-virtual {v4, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/an;->g:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    sget-object v1, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_DISCUSSED:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/an;->k:Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;->THIS_WEEK:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v0, v1, v8, v2, v5}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v8

    sget-object v0, Lcom/google/android/apps/youtube/app/an;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    invoke-virtual {v4, v8, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/app/ao;

    const/4 v5, 0x1

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ao;-><init>(Lcom/google/android/apps/youtube/app/an;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/util/HashMap;I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/an;->h:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v1, v6, v0}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/an;->h:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v1, v7, v0}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/an;->h:Lcom/google/android/apps/youtube/core/async/af;

    invoke-interface {v1, v8, v0}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, v3, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/myfeed/users/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/an;->i:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v1, Lcom/google/android/apps/youtube/app/ap;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/youtube/app/ap;-><init>(Lcom/google/android/apps/youtube/app/an;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v3, v1}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v3, p2}, Lcom/google/android/apps/youtube/app/an;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method
