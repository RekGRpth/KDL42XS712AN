.class public final Lcom/google/android/apps/youtube/app/aw;
.super Lcom/google/android/apps/youtube/core/ax;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Landroid/content/Context;)V
    .locals 6

    const/4 v4, 0x0

    const-string v0, "youtube"

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/core/ax;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/aw;->d:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/aw;->b:Landroid/content/ContentResolver;

    const-string v1, "content://com.google.settings/partner"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "value"

    aput-object v5, v2, v3

    const-string v3, "name=\'youtube_client_id\'"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "value"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v4, "mvapp-android-google"

    :cond_2
    :goto_0
    iput-object v4, p0, Lcom/google/android/apps/youtube/app/aw;->a:Ljava/lang/String;

    return-void

    :cond_3
    const-string v0, "mvapp-android-"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mvapp-android-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private X()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/aw;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/aw;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->g(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0x1644

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Landroid/util/SparseBooleanArray;
    .locals 6

    new-instance v1, Landroid/util/SparseBooleanArray;

    invoke-direct {v1}, Landroid/util/SparseBooleanArray;-><init>()V

    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1

    :catch_0
    move-exception v4

    goto :goto_1
.end method


# virtual methods
.method public final a()I
    .locals 2

    const-string v0, "min_app_version"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 2

    const-string v0, "target_app_version"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final c()Landroid/util/SparseBooleanArray;
    .locals 2

    const-string v0, "blacklisted_app_versions"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;)Landroid/util/SparseBooleanArray;

    move-result-object v0

    return-object v0
.end method

.method public final d()Landroid/util/SparseBooleanArray;
    .locals 2

    const-string v0, "discouraged_app_versions"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;)Landroid/util/SparseBooleanArray;

    move-result-object v0

    return-object v0
.end method

.method public final e()J
    .locals 3

    const-string v0, "time_between_upgrade_prompts_millis"

    const-wide/32 v1, 0x240c8400

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/ax;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/ax;->f()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "UA-20803990-1"

    goto :goto_0
.end method

.method protected final g()I
    .locals 1

    const/16 v0, 0x64

    return v0
.end method

.method public final h()Z
    .locals 2

    const-string v0, "enable_mdx_logs"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 2

    const-string v0, "enable_mdx_private_videos"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 2

    const-string v0, "enable_mdx_live_videos_5_6"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final k()Z
    .locals 2

    const-string v0, "enable_mdx_v3_protocol_5_6"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "enable_exo_proxy:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/aw;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/aw;->X()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "enable_exo_cache:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/aw;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/aw;->X()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final n()Z
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/aw;->X()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    const-string v1, "is_bgol_enabled_5_5"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Z
    .locals 2

    const-string v0, "enable_lact"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final p()Z
    .locals 2

    const-string v0, "enable_more_ads"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final q()I
    .locals 2

    const-string v0, "remote_volume_step_percent"

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 2

    const-string v0, "ads_pings_send_user_auth"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final s()Z
    .locals 2

    const-string v0, "ads_pings_send_visitor_id"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final t()Z
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/aw;->X()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "enable_amodo_only:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/aw;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u()I
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "exo_buffer_segment_size:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/aw;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final v()I
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "exo_video_buffer_segment_count:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/aw;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final w()I
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "exo_audio_buffer_segment_count:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/aw;->d:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/aw;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
