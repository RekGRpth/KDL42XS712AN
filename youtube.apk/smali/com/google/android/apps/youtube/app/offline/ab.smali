.class final Lcom/google/android/apps/youtube/app/offline/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/apps/youtube/common/a/b;

.field final synthetic c:Lcom/google/android/apps/youtube/app/offline/aa;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/offline/aa;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/offline/ab;->c:Lcom/google/android/apps/youtube/app/offline/aa;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/offline/ab;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/offline/ab;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/ab;->c:Lcom/google/android/apps/youtube/app/offline/aa;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/aa;->a(Lcom/google/android/apps/youtube/app/offline/aa;)Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/ab;->c:Lcom/google/android/apps/youtube/app/offline/aa;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/aa;->b(Lcom/google/android/apps/youtube/app/offline/aa;)Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/ab;->c:Lcom/google/android/apps/youtube/app/offline/aa;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/offline/aa;->a(Lcom/google/android/apps/youtube/app/offline/aa;)Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/q;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    :goto_0
    invoke-static {}, Lcom/google/android/apps/youtube/common/a/c;->a()Lcom/google/android/apps/youtube/common/a/c;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/ab;->a:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->c(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/common/a/c;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/ab;->b:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/ab;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/ab;->c:Lcom/google/android/apps/youtube/app/offline/aa;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/aa;->b(Lcom/google/android/apps/youtube/app/offline/aa;)Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/q;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/ab;->b:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/ab;->a:Ljava/lang/String;

    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/ab;->b:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/ab;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_1
.end method
