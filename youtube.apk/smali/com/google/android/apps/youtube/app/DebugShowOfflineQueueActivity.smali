.class public Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;
.super Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"


# instance fields
.field private n:Lcom/google/android/apps/youtube/datalib/offline/i;

.field private o:Landroid/widget/ListView;

.field private p:Landroid/widget/TextView;

.field private q:Lcom/google/android/apps/youtube/uilib/a/h;

.field private r:Landroid/os/AsyncTask;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;)Lcom/google/android/apps/youtube/datalib/offline/i;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->n:Lcom/google/android/apps/youtube/datalib/offline/i;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;Lcom/google/android/apps/youtube/datalib/offline/i;)Ljava/util/List;
    .locals 3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/offline/i;->d()Lcom/google/android/apps/youtube/common/database/e;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/database/e;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/database/e;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/database/e;->a()V

    return-object v1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->p:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;)Lcom/google/android/apps/youtube/uilib/a/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->q:Lcom/google/android/apps/youtube/uilib/a/h;

    return-object v0
.end method


# virtual methods
.method public final d()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->finish()V

    :cond_0
    sget v0, Lcom/google/android/youtube/l;->L:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->ab()Lcom/google/android/apps/youtube/datalib/offline/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->n:Lcom/google/android/apps/youtube/datalib/offline/i;

    new-instance v0, Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/uilib/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->q:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->q:Lcom/google/android/apps/youtube/uilib/a/h;

    const-class v1, Lcom/google/android/apps/youtube/a/a/c;

    new-instance v2, Lcom/google/android/apps/youtube/app/ab;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/ab;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    sget v0, Lcom/google/android/youtube/j;->ch:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->o:Landroid/widget/ListView;

    const v0, 0x1020004    # android.R.id.empty

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->p:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->o:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->q:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/z;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/z;-><init>(Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->r:Landroid/os/AsyncTask;

    return-void
.end method

.method protected onResume()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->r:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Void;

    const/4 v2, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    const-string v1, "Show offline queue"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugShowOfflineQueueActivity;->p:Landroid/widget/TextView;

    const-string v1, "Loading..."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
