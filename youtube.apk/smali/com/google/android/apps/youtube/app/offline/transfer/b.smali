.class public final Lcom/google/android/apps/youtube/app/offline/transfer/b;
.super Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;
.source "SourceFile"


# instance fields
.field private final i:Lcom/google/android/apps/youtube/core/transfer/m;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/offline/store/i;Lcom/google/android/apps/youtube/datalib/innertube/ah;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/datalib/innertube/ag;Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;Lcom/google/android/apps/youtube/core/transfer/m;Lcom/google/android/apps/youtube/app/offline/transfer/c;ILcom/google/android/apps/youtube/core/async/af;Ljava/io/File;)V
    .locals 12

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p9

    move/from16 v9, p10

    move-object/from16 v10, p11

    move-object/from16 v11, p12

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;-><init>(Lcom/google/android/apps/youtube/core/offline/store/i;Lcom/google/android/apps/youtube/datalib/innertube/ah;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/datalib/innertube/ag;Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;Lcom/google/android/apps/youtube/app/offline/transfer/c;ILcom/google/android/apps/youtube/core/async/af;Ljava/io/File;)V

    invoke-static/range {p8 .. p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/m;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->i:Lcom/google/android/apps/youtube/core/transfer/m;

    return-void
.end method


# virtual methods
.method protected final a(J)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->i:Lcom/google/android/apps/youtube/core/transfer/m;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->f:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/apps/youtube/core/transfer/m;->a(Ljava/lang/String;J)V

    return-void
.end method

.method protected final a(JJ)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "offline ad task["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] progress "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->a:Lcom/google/android/apps/youtube/core/offline/store/i;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->g:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->ACTIVE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/offline/store/i;->b(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->i:Lcom/google/android/apps/youtube/core/transfer/m;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->f:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/apps/youtube/core/transfer/m;->b(Ljava/lang/String;J)V

    return-void
.end method

.method protected final a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V
    .locals 0

    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/Exception;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V
    .locals 3

    if-nez p2, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/TransferException;

    invoke-virtual {p3}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->isFatal()Z

    move-result v1

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/youtube/core/transfer/TransferException;-><init>(Ljava/lang/String;Z)V

    :goto_0
    invoke-virtual {p3}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->isFatal()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offline ad task["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->a:Lcom/google/android/apps/youtube/core/offline/store/i;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, p3}, Lcom/google/android/apps/youtube/core/offline/store/i;->b(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->i:Lcom/google/android/apps/youtube/core/transfer/m;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->f:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/youtube/core/transfer/m;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/transfer/TransferException;)V

    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/core/transfer/TransferException;

    invoke-virtual {p3}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->isFatal()Z

    move-result v1

    invoke-direct {v0, p1, p2, v1}, Lcom/google/android/apps/youtube/core/transfer/TransferException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "offline ad task["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected final a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final b()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "offline ad task["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] success"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->a:Lcom/google/android/apps/youtube/core/offline/store/i;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->g:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->COMPLETE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/offline/store/i;->b(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->i:Lcom/google/android/apps/youtube/core/transfer/m;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/b;->f:Ljava/lang/String;

    new-instance v2, Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;-><init>()V

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/transfer/m;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/a;)V

    return-void
.end method
