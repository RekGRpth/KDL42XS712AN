.class final Lcom/google/android/apps/youtube/app/offline/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/identity/ah;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/apps/youtube/app/offline/j;

.field final synthetic c:Lcom/google/android/apps/youtube/app/offline/f;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/offline/f;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/j;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/offline/g;->c:Lcom/google/android/apps/youtube/app/offline/f;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/offline/g;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/offline/g;->b:Lcom/google/android/apps/youtube/app/offline/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/g;->c:Lcom/google/android/apps/youtube/app/offline/f;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/g;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/g;->b:Lcom/google/android/apps/youtube/app/offline/j;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/app/offline/f;->a(Lcom/google/android/apps/youtube/app/offline/f;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/j;)V

    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/g;->c:Lcom/google/android/apps/youtube/app/offline/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/f;->a(Lcom/google/android/apps/youtube/app/offline/f;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/g;->b:Lcom/google/android/apps/youtube/app/offline/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/g;->b:Lcom/google/android/apps/youtube/app/offline/j;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/g;->a:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->CANNOT_ADD:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/offline/j;->a(Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/g;->c:Lcom/google/android/apps/youtube/app/offline/f;

    sget-object v1, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->CANNOT_ADD:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/offline/f;->a(Lcom/google/android/apps/youtube/app/offline/f;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;)V

    return-void
.end method
