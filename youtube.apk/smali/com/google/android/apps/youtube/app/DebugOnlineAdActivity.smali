.class public Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;
.super Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"


# instance fields
.field private n:Lcom/google/android/apps/youtube/core/client/p;

.field private o:Landroid/widget/CheckBox;

.field private p:Landroid/widget/CheckBox;

.field private q:Landroid/widget/Spinner;

.field private r:Landroid/widget/ArrayAdapter;

.field private s:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;)Landroid/widget/CheckBox;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->o:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;)Lcom/google/android/apps/youtube/core/client/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->n:Lcom/google/android/apps/youtube/core/client/p;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->e()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->s:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;)Landroid/widget/CheckBox;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->p:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->p:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->o:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->q:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->p:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->o:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;)Landroid/widget/Spinner;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->q:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;)Landroid/widget/ArrayAdapter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->r:Landroid/widget/ArrayAdapter;

    return-object v0
.end method


# virtual methods
.method public final d()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->finish()V

    :cond_0
    sget v0, Lcom/google/android/youtube/l;->J:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    new-instance v1, Lcom/google/android/apps/youtube/core/client/p;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aP()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/core/client/p;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->n:Lcom/google/android/apps/youtube/core/client/p;

    sget v0, Lcom/google/android/youtube/j;->aT:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->o:Landroid/widget/CheckBox;

    sget v0, Lcom/google/android/youtube/j;->fT:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->p:Landroid/widget/CheckBox;

    sget v0, Lcom/google/android/youtube/j;->e:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->q:Landroid/widget/Spinner;

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x1090008    # android.R.layout.simple_spinner_item

    invoke-static {}, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->values()[Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->r:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->r:Landroid/widget/ArrayAdapter;

    const v1, 0x1090009    # android.R.layout.simple_spinner_dropdown_item

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->q:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->r:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->n:Lcom/google/android/apps/youtube/core/client/p;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/p;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->s:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->o:Landroid/widget/CheckBox;

    new-instance v1, Lcom/google/android/apps/youtube/app/w;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/w;-><init>(Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->p:Landroid/widget/CheckBox;

    new-instance v1, Lcom/google/android/apps/youtube/app/x;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/x;-><init>(Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->q:Landroid/widget/Spinner;

    new-instance v1, Lcom/google/android/apps/youtube/app/y;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/y;-><init>(Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->o:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->n:Lcom/google/android/apps/youtube/core/client/p;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/client/p;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->n:Lcom/google/android/apps/youtube/core/client/p;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/p;->b()Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->p:Landroid/widget/CheckBox;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->q:Landroid/widget/Spinner;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/client/DebugOnlyVmapAdRequester$ForceAdType;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/DebugOnlineAdActivity;->e()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
