.class public Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;
.super Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
.source "SourceFile"


# instance fields
.field private Y:Ljava/util/concurrent/atomic/AtomicReference;

.field private Z:Ljava/lang/String;

.field private a:Lcom/google/android/apps/youtube/app/ax;

.field private aa:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field private ab:Lcom/google/android/apps/youtube/app/d/aj;

.field private ac:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

.field private ad:Lcom/google/android/apps/youtube/app/ui/presenter/cc;

.field private ae:Ljava/lang/String;

.field private af:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

.field private ag:Lcom/google/android/apps/youtube/app/compat/q;

.field private ah:Landroid/app/AlertDialog;

.field private b:Lcom/google/android/apps/youtube/datalib/innertube/d;

.field private d:Lcom/google/android/apps/youtube/core/aw;

.field private e:Lcom/google/android/apps/youtube/app/ui/a;

.field private f:Lcom/google/android/apps/youtube/common/c/a;

.field private g:Lcom/google/android/apps/youtube/core/client/bc;

.field private h:Lcom/google/android/apps/youtube/datalib/innertube/aq;

.field private i:Lcom/google/android/apps/youtube/core/au;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;-><init>()V

    return-void
.end method

.method private L()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->E()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->getNavigationEndpoint()Lcom/google/a/a/a/a/kz;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->b(Lcom/google/a/a/a/a/kz;)[B

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->b:Lcom/google/android/apps/youtube/datalib/innertube/d;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/d;->a()Lcom/google/android/apps/youtube/datalib/innertube/g;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->Z:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/datalib/innertube/g;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/g;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/datalib/innertube/g;->a([B)V

    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/google/a/a/a/a/kz;->c:Lcom/google/a/a/a/a/am;

    if-eqz v3, :cond_0

    iget-object v0, v0, Lcom/google/a/a/a/a/kz;->c:Lcom/google/a/a/a/a/am;

    iget-object v0, v0, Lcom/google/a/a/a/a/am;->c:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/datalib/innertube/g;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/g;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->aa:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/youtube/app/b/q;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/app/b/q;-><init>()V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->b:Lcom/google/android/apps/youtube/datalib/innertube/d;

    new-instance v4, Lcom/google/android/apps/youtube/app/fragments/k;

    invoke-direct {v4, p0, v0, v1}, Lcom/google/android/apps/youtube/app/fragments/k;-><init>(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;Lcom/google/android/apps/youtube/common/c/a;[B)V

    invoke-virtual {v3, v2, v4}, Lcom/google/android/apps/youtube/datalib/innertube/d;->a(Lcom/google/android/apps/youtube/datalib/innertube/g;Lcom/google/android/apps/youtube/datalib/a/l;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;Ljava/lang/Object;[B)Landroid/view/View;
    .locals 17

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->af:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    const/4 v2, 0x0

    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;

    if-eqz v3, :cond_1

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/presenter/o;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/ax;->G()Lcom/google/android/apps/youtube/datalib/innertube/av;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->aQ()Lcom/google/android/apps/youtube/core/identity/as;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/app/ax;->F()Lcom/google/android/apps/youtube/datalib/innertube/bc;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v10}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q()Lcom/google/android/apps/youtube/datalib/d/a;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v11}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->d:Lcom/google/android/apps/youtube/core/aw;

    invoke-direct/range {v2 .. v12}, Lcom/google/android/apps/youtube/app/ui/presenter/o;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/datalib/innertube/av;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/identity/as;Lcom/google/android/apps/youtube/datalib/innertube/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;)V

    check-cast p1, Lcom/google/android/apps/youtube/datalib/innertube/model/c;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/o;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/c;)Landroid/view/View;

    move-result-object v2

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    move-object/from16 v0, p1

    instance-of v3, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->Z()Lcom/google/android/apps/youtube/app/offline/p;

    move-result-object v9

    new-instance v2, Lcom/google/android/apps/youtube/app/offline/f;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/app/ax;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v8

    new-instance v10, Lcom/google/android/apps/youtube/app/ui/bv;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v10, v11, v9}, Lcom/google/android/apps/youtube/app/ui/bv;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/offline/p;)V

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/youtube/app/offline/f;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/app/ui/bv;)V

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/presenter/bk;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/app/ax;->x()Lcom/google/android/apps/youtube/datalib/innertube/v;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v10}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v11}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v12}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q()Lcom/google/android/apps/youtube/datalib/d/a;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v14}, Lcom/google/android/apps/youtube/app/ax;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v15}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v16

    if-eqz v16, :cond_2

    invoke-virtual {v15}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v14, v15}, Lcom/google/android/apps/youtube/core/offline/store/q;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v14

    :goto_1
    move-object v15, v2

    invoke-direct/range {v3 .. v15}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/datalib/innertube/v;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/app/offline/f;)V

    move-object/from16 v2, p1

    check-cast v2, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ab;)Landroid/view/View;

    move-result-object v2

    check-cast p1, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->af:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    goto/16 :goto_0

    :cond_2
    invoke-interface {v14}, Lcom/google/android/apps/youtube/core/offline/store/q;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v14

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;)Lcom/google/android/apps/youtube/datalib/innertube/model/ab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->af:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->i:Lcom/google/android/apps/youtube/core/au;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/au;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->h:Lcom/google/android/apps/youtube/datalib/innertube/aq;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/aq;->a()Lcom/google/android/apps/youtube/datalib/innertube/ar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/ar;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ar;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/ar;->a:[B

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/ar;->a([B)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->h:Lcom/google/android/apps/youtube/datalib/innertube/aq;

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/i;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/youtube/app/fragments/i;-><init>(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/aq;->a(Lcom/google/android/apps/youtube/datalib/innertube/ar;Lcom/google/android/apps/youtube/datalib/a/l;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->g:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/j;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/youtube/app/fragments/j;-><init>(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/core/client/bc;->n(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;Ljava/util/List;Landroid/view/View;)V
    .locals 13

    const/4 v9, 0x0

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->ab:Lcom/google/android/apps/youtube/app/d/aj;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/d/aj;->a()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move v1, v0

    :cond_0
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/google/android/apps/youtube/datalib/innertube/model/an;

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/datalib/innertube/model/an;->c()Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    move-result-object v12

    if-eqz v12, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/am;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/presenter/cv;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/app/ui/presenter/cv;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/am;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/uilib/a/i;Lcom/google/android/apps/youtube/common/c/a;)V

    new-instance v3, Lcom/google/android/apps/youtube/app/adapter/z;

    invoke-direct {v3, v0}, Lcom/google/android/apps/youtube/app/adapter/z;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/am;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v2, Lcom/google/android/youtube/l;->ba:I

    const/4 v4, 0x0

    invoke-static {v0, v2, v4}, Landroid/widget/ListView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    if-eqz v1, :cond_3

    invoke-virtual {v2, p2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    move v10, v9

    :goto_2
    new-instance v0, Lcom/google/android/apps/youtube/uilib/innertube/t;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->b:Lcom/google/android/apps/youtube/datalib/innertube/d;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->ad:Lcom/google/android/apps/youtube/app/ui/presenter/cc;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->d:Lcom/google/android/apps/youtube/core/aw;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/uilib/innertube/t;-><init>(Lcom/google/android/apps/youtube/uilib/innertube/i;Landroid/widget/ListView;Lcom/google/android/apps/youtube/uilib/innertube/p;Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/uilib/innertube/j;Lcom/google/android/apps/youtube/core/aw;)V

    invoke-virtual {v0, v12}, Lcom/google/android/apps/youtube/uilib/innertube/t;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/aj;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->ab:Lcom/google/android/apps/youtube/app/d/aj;

    invoke-virtual {v1, v8, v0}, Lcom/google/android/apps/youtube/app/d/aj;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/an;Lcom/google/android/apps/youtube/uilib/innertube/t;)V

    move v1, v10

    goto :goto_1

    :cond_1
    move v0, v9

    goto :goto_0

    :cond_2
    return-void

    :cond_3
    move v10, v1

    goto :goto_2
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->ae:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;)Ljava/util/concurrent/atomic/AtomicReference;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->Y:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;)Lcom/google/android/apps/youtube/common/c/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->f:Lcom/google/android/apps/youtube/common/c/a;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->d:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->aa:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->L()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 16

    sget v2, Lcom/google/android/youtube/l;->k:I

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->aa:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->aa:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/l;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/youtube/app/fragments/l;-><init>(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;B)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->setOnRetryClickListener(Lcom/google/android/apps/youtube/uilib/innertube/q;)V

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/presenter/cc;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q()Lcom/google/android/apps/youtube/datalib/d/a;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/app/ax;->aQ()Lcom/google/android/apps/youtube/core/identity/as;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->e:Lcom/google/android/apps/youtube/app/ui/a;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v11}, Lcom/google/android/apps/youtube/app/ax;->F()Lcom/google/android/apps/youtube/datalib/innertube/bc;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v12}, Lcom/google/android/apps/youtube/app/ax;->G()Lcom/google/android/apps/youtube/datalib/innertube/av;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->d:Lcom/google/android/apps/youtube/core/aw;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->ac:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-direct/range {v2 .. v15}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/as;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/app/ui/a;Lcom/google/android/apps/youtube/datalib/innertube/bc;Lcom/google/android/apps/youtube/datalib/innertube/av;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->ad:Lcom/google/android/apps/youtube/app/ui/presenter/cc;

    new-instance v3, Lcom/google/android/apps/youtube/app/d/aj;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->aa:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    sget v4, Lcom/google/android/youtube/j;->ft:I

    invoke-virtual {v2, v4}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-direct {v3, v2}, Lcom/google/android/apps/youtube/app/d/aj;-><init>(Lcom/google/android/apps/youtube/app/ui/TabbedView;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->ab:Lcom/google/android/apps/youtube/app/d/aj;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->Z:Ljava/lang/String;

    if-nez v2, :cond_0

    if-eqz p3, :cond_1

    :goto_0
    const-string v2, "browse_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->Z:Ljava/lang/String;

    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->L()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->aa:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    return-object v2

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->h()Landroid/os/Bundle;

    move-result-object p3

    goto :goto_0
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->ae:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->d:Lcom/google/android/apps/youtube/core/aw;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->A()Lcom/google/android/apps/youtube/datalib/innertube/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->b:Lcom/google/android/apps/youtube/datalib/innertube/d;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->C()Lcom/google/android/apps/youtube/datalib/e/b;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/app/ui/a;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->e:Lcom/google/android/apps/youtube/app/ui/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->f:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->g:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->z()Lcom/google/android/apps/youtube/datalib/innertube/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->h:Lcom/google/android/apps/youtube/datalib/innertube/aq;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->b()Lcom/google/android/apps/youtube/app/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->i:Lcom/google/android/apps/youtube/core/au;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->Y:Ljava/util/concurrent/atomic/AtomicReference;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->d:Lcom/google/android/apps/youtube/core/aw;

    sget-object v3, Lcom/google/android/apps/youtube/core/client/WatchFeature;->BROWSE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/client/bc;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->ac:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/j;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Lcom/google/android/apps/youtube/app/compat/j;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->M()Lcom/google/android/apps/youtube/app/compat/o;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/m;->a:I

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/youtube/app/compat/o;->a(ILcom/google/android/apps/youtube/app/compat/j;)V

    sget v0, Lcom/google/android/youtube/j;->cu:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/app/compat/j;->c(I)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->ag:Lcom/google/android/apps/youtube/app/compat/q;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->ag:Lcom/google/android/apps/youtube/app/compat/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/compat/q;->b(Z)Lcom/google/android/apps/youtube/app/compat/q;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/q;)Z
    .locals 7

    const/4 v0, 0x1

    invoke-interface {p1}, Lcom/google/android/apps/youtube/app/compat/q;->e()I

    move-result v1

    sget v2, Lcom/google/android/youtube/j;->cu:I

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->ah:Landroid/app/AlertDialog;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/h;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/fragments/h;-><init>(Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;)V

    new-instance v2, Lcom/google/android/apps/youtube/core/ui/aa;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v2, v3}, Lcom/google/android/apps/youtube/core/ui/aa;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/google/android/youtube/p;->aL:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->af:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->c()Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/core/ui/aa;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x1040013    # android.R.string.yes

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x1040009    # android.R.string.no

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->ah:Landroid/app/AlertDialog;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->ah:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Lcom/google/android/apps/youtube/app/compat/q;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/youtube/app/compat/j;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->b(Lcom/google/android/apps/youtube/app/compat/j;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->ag:Lcom/google/android/apps/youtube/app/compat/q;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->af:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->af:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/app/compat/q;->b(Z)Lcom/google/android/apps/youtube/app/compat/q;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->e(Landroid/os/Bundle;)V

    const-string v0, "browse_id"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->Z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public handleVideoAddedToPlaylistEvent(Lcom/google/android/apps/youtube/datalib/innertube/ao;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->af:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/innertube/ao;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->af:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->L()V

    :cond_0
    return-void
.end method

.method public handleVideoRemovedFromPlaylistEvent(Lcom/google/android/apps/youtube/datalib/innertube/ap;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->af:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/innertube/ap;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->af:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->L()V

    :cond_0
    return-void
.end method

.method public final r()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->r()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->ac:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b()V

    return-void
.end method

.method public final s()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->s()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    return-void
.end method

.method public final t()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->t()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/BrowseFragment;->ac:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a()V

    return-void
.end method
