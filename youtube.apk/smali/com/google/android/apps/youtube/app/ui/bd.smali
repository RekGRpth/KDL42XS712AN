.class public final Lcom/google/android/apps/youtube/app/ui/bd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/core/client/bc;

.field private final c:Lcom/google/android/apps/youtube/core/aw;

.field private final d:Lcom/google/android/apps/youtube/common/c/a;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/c/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bd;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bd;->b:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bd;->c:Lcom/google/android/apps/youtube/core/aw;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bd;->d:Lcom/google/android/apps/youtube/common/c/a;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/bd;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bd;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/bd;)Lcom/google/android/apps/youtube/common/c/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bd;->d:Lcom/google/android/apps/youtube/common/c/a;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/bd;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bd;->c:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/bd;)Lcom/google/android/apps/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bd;->b:Lcom/google/android/apps/youtube/core/client/bc;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/app/ui/bg;)Landroid/app/Dialog;
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bd;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->E:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    sget v0, Lcom/google/android/youtube/j;->cJ:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    sget v1, Lcom/google/android/youtube/j;->bT:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/be;

    invoke-direct {v3, p0, v0, v1, p1}, Lcom/google/android/apps/youtube/app/ui/be;-><init>(Lcom/google/android/apps/youtube/app/ui/bd;Landroid/widget/EditText;Landroid/widget/CheckBox;Lcom/google/android/apps/youtube/app/ui/bg;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/ui/aa;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/bd;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/ui/aa;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/youtube/p;->aB:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/aa;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/bd;->a:Landroid/app/Activity;

    const v2, 0x104000a    # android.R.string.ok

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/bd;->a:Landroid/app/Activity;

    const/high16 v2, 0x1040000    # android.R.string.cancel

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    return-object v0
.end method
