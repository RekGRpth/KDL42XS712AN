.class final Lcom/google/android/apps/youtube/app/ui/ix;
.super Lcom/google/android/apps/youtube/app/ui/FlingDynamics;
.source "SourceFile"


# instance fields
.field final synthetic g:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

.field private h:F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;Landroid/content/Context;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ix;->g:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    const/16 v0, 0x190

    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;-><init>(Landroid/content/Context;I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/ix;->h:F

    return-void
.end method


# virtual methods
.method public final a(II)I
    .locals 6

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/ix;->h:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    int-to-double v0, p1

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/ix;->h:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    int-to-double v2, p2

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/ix;->h:F

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-int p2, v0

    :cond_0
    return p2
.end method

.method public final a(F)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/ix;->h:F

    return-void
.end method

.method public final f(Landroid/view/MotionEvent;)Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;
    .locals 6

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/ix;->f:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    if-ltz v0, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    if-le v1, v0, :cond_5

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/ix;->d:F

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/ix;->e:F

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/ui/ix;->c(Landroid/view/MotionEvent;)I

    move-result v3

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/ui/ix;->d(Landroid/view/MotionEvent;)I

    move-result v4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ix;->g:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;)Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    move-result-object v0

    sget-object v5, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-ne v0, v5, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ix;->g:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;)I

    move-result v0

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/ix;->g:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-static {v5}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;)I

    move-result v5

    if-ne v0, v5, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/youtube/app/ui/ix;->a(II)I

    move-result v5

    if-eqz v0, :cond_3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/ix;->a:I

    mul-int/lit8 v3, v3, 0x2

    if-le v0, v3, :cond_2

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/ix;->h:F

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_0

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/ix;->a:I

    if-ge v0, v3, :cond_2

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;->DISMISS:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/ix;->a:I

    mul-int/lit8 v0, v0, 0x2

    if-le v5, v0, :cond_4

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;->MIN_MAX:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    goto :goto_1

    :cond_3
    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/ix;->a:I

    if-le v0, v3, :cond_4

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;->MIN_MAX:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    goto :goto_1

    :cond_4
    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/ix;->d:F

    iput v2, p0, Lcom/google/android/apps/youtube/app/ui/ix;->e:F

    :goto_2
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;->NONE:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$DragMode;

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/ix;->a()V

    goto :goto_2
.end method
