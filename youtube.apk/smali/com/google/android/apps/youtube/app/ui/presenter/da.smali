.class public final Lcom/google/android/apps/youtube/app/ui/presenter/da;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/j;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

.field private final b:Lcom/google/android/apps/youtube/datalib/innertube/av;

.field private final c:Lcom/google/android/apps/youtube/datalib/d/a;

.field private final d:Lcom/google/android/apps/youtube/core/identity/l;

.field private final e:Lcom/google/android/apps/youtube/core/identity/as;

.field private final f:Lcom/google/android/apps/youtube/core/identity/aa;

.field private final g:Lcom/google/android/apps/youtube/datalib/innertube/bc;

.field private final h:Lcom/google/android/apps/youtube/core/client/bj;

.field private final i:Lcom/google/android/apps/youtube/common/c/a;

.field private final j:Lcom/google/android/apps/youtube/core/aw;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/datalib/innertube/av;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/as;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/datalib/innertube/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/datalib/d/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/d/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->c:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/av;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->b:Lcom/google/android/apps/youtube/datalib/innertube/av;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->d:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/as;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->e:Lcom/google/android/apps/youtube/core/identity/as;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/aa;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->f:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->g:Lcom/google/android/apps/youtube/datalib/innertube/bc;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->h:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->i:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->j:Lcom/google/android/apps/youtube/core/aw;

    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/android/apps/youtube/uilib/a/g;
    .locals 12

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/ja;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v2, Lcom/google/android/apps/youtube/app/c/a;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->g:Lcom/google/android/apps/youtube/datalib/innertube/bc;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->d:Lcom/google/android/apps/youtube/core/identity/l;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->e:Lcom/google/android/apps/youtube/core/identity/as;

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/apps/youtube/app/c/a;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/datalib/innertube/bc;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/as;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->c:Lcom/google/android/apps/youtube/datalib/d/a;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->i:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->j:Lcom/google/android/apps/youtube/core/aw;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/ja;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/c/a;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/cz;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/presenter/p;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-direct {v3, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/p;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->b:Lcom/google/android/apps/youtube/datalib/innertube/av;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->d:Lcom/google/android/apps/youtube/core/identity/l;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->f:Lcom/google/android/apps/youtube/core/identity/aa;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->h:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v9, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->i:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->j:Lcom/google/android/apps/youtube/core/aw;

    iget-object v11, p0, Lcom/google/android/apps/youtube/app/ui/presenter/da;->c:Lcom/google/android/apps/youtube/datalib/d/a;

    move-object v7, v0

    invoke-direct/range {v1 .. v11}, Lcom/google/android/apps/youtube/app/ui/presenter/cz;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/uilib/a/i;Lcom/google/android/apps/youtube/datalib/innertube/av;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/app/ui/ja;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/datalib/d/a;)V

    return-object v1
.end method
