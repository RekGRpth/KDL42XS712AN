.class final Lcom/google/android/apps/youtube/app/ui/cu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/cr;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/ui/cr;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/cu;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cu;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cr;->d(Lcom/google/android/apps/youtube/app/ui/cr;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cu;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cr;->d(Lcom/google/android/apps/youtube/app/ui/cr;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->isPrivate:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cu;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cr;->c(Lcom/google/android/apps/youtube/app/ui/cr;)Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->eN:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cu;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cr;->g(Lcom/google/android/apps/youtube/app/ui/cr;)Lcom/google/android/apps/youtube/app/ui/cl;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cu;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cr;->e(Lcom/google/android/apps/youtube/app/ui/cr;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LikeAction;->REMOVE_LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/cu;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/cr;->f(Lcom/google/android/apps/youtube/app/ui/cr;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/app/ui/cl;->a(Lcom/google/android/apps/youtube/app/ui/LikeAction;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LikeAction;->LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    goto :goto_1
.end method
