.class final enum Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

.field public static final enum PHONE:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

.field public static final enum TABLET:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

    const-string v1, "TABLET"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;->TABLET:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

    const-string v1, "PHONE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;->PHONE:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;->TABLET:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;->PHONE:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;->$VALUES:[Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;->$VALUES:[Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$LayoutMode;

    return-object v0
.end method
