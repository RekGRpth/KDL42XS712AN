.class final Lcom/google/android/apps/youtube/app/ui/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/g;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/ui/g;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/k;->a:Lcom/google/android/apps/youtube/app/ui/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/ui/g;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/k;-><init>(Lcom/google/android/apps/youtube/app/ui/g;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "Error retrieving user thumbnail"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/k;->a:Lcom/google/android/apps/youtube/app/ui/g;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/g;->b(Lcom/google/android/apps/youtube/app/ui/g;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/k;->a:Lcom/google/android/apps/youtube/app/ui/g;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/g;->a(Lcom/google/android/apps/youtube/app/ui/g;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method
