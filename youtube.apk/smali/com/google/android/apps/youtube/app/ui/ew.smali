.class final Lcom/google/android/apps/youtube/app/ui/ew;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/Analytics;

.field final synthetic b:Lcom/google/android/apps/youtube/app/remote/an;

.field final synthetic c:Landroid/app/Activity;

.field final synthetic d:Lcom/google/android/apps/youtube/app/ui/fg;

.field final synthetic e:Lcom/google/android/apps/youtube/core/aw;

.field final synthetic f:Lcom/google/android/apps/youtube/app/ui/ev;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/ui/ev;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/app/remote/an;Landroid/app/Activity;Lcom/google/android/apps/youtube/app/ui/fg;Lcom/google/android/apps/youtube/core/aw;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ew;->f:Lcom/google/android/apps/youtube/app/ui/ev;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/ew;->a:Lcom/google/android/apps/youtube/core/Analytics;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/ew;->b:Lcom/google/android/apps/youtube/app/remote/an;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/ui/ew;->c:Landroid/app/Activity;

    iput-object p5, p0, Lcom/google/android/apps/youtube/app/ui/ew;->d:Lcom/google/android/apps/youtube/app/ui/fg;

    iput-object p6, p0, Lcom/google/android/apps/youtube/app/ui/ew;->e:Lcom/google/android/apps/youtube/core/aw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ew;->a:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "RemoteError"

    const-string v2, "INVALID_PAIRING_CODE"

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ew;->e:Lcom/google/android/apps/youtube/core/aw;

    sget v1, Lcom/google/android/youtube/p;->bp:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/aw;->a(I)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ew;->a:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "RemoteControlPaired"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ew;->b:Lcom/google/android/apps/youtube/app/remote/an;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ew;->c:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/ex;

    invoke-direct {v2, p0, p2}, Lcom/google/android/apps/youtube/app/ui/ex;-><init>(Lcom/google/android/apps/youtube/app/ui/ew;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/google/android/apps/youtube/app/remote/an;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
