.class public Lcom/google/android/apps/youtube/app/m2ts/TextOverlay;
.super Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/av;
.implements Lcom/google/android/apps/youtube/core/player/overlay/ax;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/16 v2, 0x7f

    const/16 v1, 0xff

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;-><init>(Landroid/content/Context;)V

    invoke-static {v2, v0, v0, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/m2ts/TextOverlay;->setBackgroundColor(I)V

    invoke-static {v2, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/m2ts/TextOverlay;->setTextColor(I)V

    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/m2ts/TextOverlay;->setGravity(I)V

    const/4 v0, 0x2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/g;->H:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/m2ts/TextOverlay;->setTextSize(IF)V

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/m2ts/TextOverlay;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public final b()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method public final c()Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 2

    const/4 v1, -0x1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public setMinimized(Z)V
    .locals 0

    return-void
.end method
