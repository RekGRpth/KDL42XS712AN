.class final Lcom/google/android/apps/youtube/app/remote/ay;
.super Lcom/google/android/apps/youtube/app/remote/ba;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/remote/ax;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/remote/ax;Lcom/google/android/apps/youtube/app/remote/RemoteControl;IZ)V
    .locals 6

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/ay;->a:Lcom/google/android/apps/youtube/app/remote/ax;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/remote/ba;-><init>(Lcom/google/android/apps/youtube/app/remote/ax;Lcom/google/android/apps/youtube/app/remote/RemoteControl;IZB)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/remote/ax;Lcom/google/android/apps/youtube/app/remote/RemoteControl;IZB)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/app/remote/ay;-><init>(Lcom/google/android/apps/youtube/app/remote/ax;Lcom/google/android/apps/youtube/app/remote/RemoteControl;IZ)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ay;->a:Lcom/google/android/apps/youtube/app/remote/ax;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/ax;->b(Lcom/google/android/apps/youtube/app/remote/ax;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ay;->a:Lcom/google/android/apps/youtube/app/remote/ax;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/ax;->b(Lcom/google/android/apps/youtube/app/remote/ax;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 8

    const/4 v5, 0x1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget v3, p0, Lcom/google/android/apps/youtube/app/remote/ay;->c:I

    const/4 v0, 0x0

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v0

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/remote/ay;->b:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v7, v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ay;->a:Lcom/google/android/apps/youtube/app/remote/ax;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/ax;->a(Lcom/google/android/apps/youtube/app/remote/ax;)Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v3, v0, :cond_5

    add-int/lit8 v3, v3, -0x1

    move v0, v5

    :goto_1
    move v4, v0

    goto :goto_0

    :cond_1
    if-eqz v4, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ay;->a:Lcom/google/android/apps/youtube/app/remote/ax;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/ax;->b(Lcom/google/android/apps/youtube/app/remote/ax;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    sget v4, Lcom/google/android/youtube/p;->dP:I

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/core/aw;->a(I)V

    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/ay;->d:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ay;->a:Lcom/google/android/apps/youtube/app/remote/ax;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/remote/ay;->b:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/remote/ax;->a(Lcom/google/android/apps/youtube/app/remote/ax;Ljava/util/List;Ljava/util/List;ILcom/google/android/apps/youtube/app/remote/RemoteControl;Z)V

    :cond_3
    :goto_2
    return-void

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ay;->b:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b(Ljava/util/List;)V

    goto :goto_2

    :cond_5
    move v0, v4

    goto :goto_1
.end method
