.class public Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;
.super Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/navigation/e;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/e;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchBackStack;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method protected readEntryFromParcel(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/app/fragments/navigation/a;
    .locals 3

    const-class v0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    const-class v1, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/navigation/f;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/f;-><init>(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;Lcom/google/android/apps/youtube/core/player/PlaybackServiceState;)V

    return-object v2
.end method

.method protected writeEntryToParcel(Lcom/google/android/apps/youtube/app/fragments/navigation/a;Landroid/os/Parcel;I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/fragments/navigation/a;->a:Landroid/os/Parcelable;

    invoke-virtual {p2, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/fragments/navigation/a;->b:Landroid/os/Parcelable;

    invoke-virtual {p2, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
