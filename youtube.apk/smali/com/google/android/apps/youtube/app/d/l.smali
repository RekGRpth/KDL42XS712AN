.class final Lcom/google/android/apps/youtube/app/d/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/d/f;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/d/f;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/d/l;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/d/f;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/d/l;-><init>(Lcom/google/android/apps/youtube/app/d/f;)V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/l;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->g(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/datalib/distiller/model/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/distiller/model/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/l;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->h(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;->LOADING:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;->a(Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/l;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->b(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/uilib/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/l;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->i(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/datalib/distiller/a;

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/distiller/a;->a()Lcom/google/android/apps/youtube/datalib/distiller/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/l;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/d/f;->g(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/datalib/distiller/model/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/distiller/model/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/distiller/c;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/distiller/c;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/distiller/c;->a(I)Lcom/google/android/apps/youtube/datalib/distiller/c;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/l;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/d/f;->g(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/datalib/distiller/model/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/distiller/model/a;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/distiller/c;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/distiller/c;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/l;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/d/f;->g(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/datalib/distiller/model/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/distiller/model/a;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/d/l;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/d/f;->i(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/datalib/distiller/a;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/youtube/app/d/m;

    invoke-direct {v3, p0, v1}, Lcom/google/android/apps/youtube/app/d/m;-><init>(Lcom/google/android/apps/youtube/app/d/l;Ljava/lang/String;)V

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/youtube/datalib/distiller/a;->a(Lcom/google/android/apps/youtube/datalib/distiller/c;Lcom/google/android/apps/youtube/datalib/a/l;)V

    :cond_0
    return-void
.end method
