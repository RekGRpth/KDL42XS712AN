.class final Lcom/google/android/apps/youtube/app/honeycomb/ui/l;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, [Ljava/util/List;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;Landroid/net/Uri;)Lcom/google/android/apps/youtube/app/honeycomb/ui/p;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->b(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->b(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 8

    const/16 v3, 0x8

    const/4 v2, 0x0

    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "nothing to upload"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->c(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->b(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_2
    or-int/2addr v0, v1

    move v1, v0

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_2

    :cond_2
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->d(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Landroid/widget/ImageView;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->d(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->c(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->e(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Landroid/widget/TextView;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->d(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/Long;

    move-result-object v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->e(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/p;->d(Lcom/google/android/apps/youtube/app/honeycomb/ui/p;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    long-to-int v0, v4

    if-lez v0, :cond_6

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->e(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->e(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Landroid/widget/TextView;

    move-result-object v4

    const/4 v5, 0x3

    invoke-static {v0, v5}, Lcom/google/android/apps/youtube/common/e/m;->a(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->f(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->f(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Landroid/view/View;

    move-result-object v0

    if-eqz v1, :cond_8

    :goto_4
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->e(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/l;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->e(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    :cond_8
    move v2, v3

    goto :goto_4
.end method
