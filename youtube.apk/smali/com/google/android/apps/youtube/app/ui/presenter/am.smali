.class public final Lcom/google/android/apps/youtube/app/ui/presenter/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/g;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field private final b:Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

.field private final c:Lcom/google/android/apps/youtube/uilib/a/i;

.field private final d:Lcom/google/android/apps/youtube/common/c/a;

.field private e:Lcom/google/android/apps/youtube/uilib/innertube/o;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/uilib/a/i;Lcom/google/android/apps/youtube/common/c/a;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/a/i;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->c:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->d:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    sget v1, Lcom/google/android/youtube/l;->aj:I

    sget v2, Lcom/google/android/youtube/l;->al:I

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->a:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->bp:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->b:Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->a:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->b:Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->a:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/uilib/innertube/o;)Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->e:Lcom/google/android/apps/youtube/uilib/innertube/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->e:Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/innertube/o;->e()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/o;->e()Ljava/lang/Object;

    move-result-object v1

    if-eq v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->d:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->d:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/o;->e()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->e:Lcom/google/android/apps/youtube/uilib/innertube/o;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->a:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/o;->c()Lcom/google/android/apps/youtube/uilib/innertube/q;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->setOnRetryClickListener(Lcom/google/android/apps/youtube/uilib/innertube/q;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->c:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/o;->b()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/o;->a()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->b:Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/o;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/o;->d()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/youtube/uilib/innertube/b;

    if-eqz v0, :cond_4

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/o;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/innertube/b;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/am;->onContentEvent(Lcom/google/android/apps/youtube/uilib/innertube/b;)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->c:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Lcom/google/android/apps/youtube/uilib/a/f;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->b:Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

    sget v1, Lcom/google/android/youtube/p;->cv:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->setText(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/o;->d()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/youtube/uilib/innertube/d;

    if-eqz v0, :cond_5

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/o;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/innertube/d;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/am;->onLoadingEvent(Lcom/google/android/apps/youtube/uilib/innertube/d;)V

    goto :goto_1

    :cond_5
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/o;->d()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/youtube/uilib/innertube/c;

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/o;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/innertube/c;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/am;->onErrorEvent(Lcom/google/android/apps/youtube/uilib/innertube/c;)V

    goto :goto_1
.end method

.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/uilib/innertube/o;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/am;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/uilib/innertube/o;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onContentEvent(Lcom/google/android/apps/youtube/uilib/innertube/b;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->a:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->b()V

    return-void
.end method

.method public final onErrorEvent(Lcom/google/android/apps/youtube/uilib/innertube/c;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->a:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/uilib/innertube/c;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/uilib/innertube/c;->b()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Ljava/lang/CharSequence;Z)V

    return-void
.end method

.method public final onLoadingEvent(Lcom/google/android/apps/youtube/uilib/innertube/d;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/am;->a:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a()V

    return-void
.end method
