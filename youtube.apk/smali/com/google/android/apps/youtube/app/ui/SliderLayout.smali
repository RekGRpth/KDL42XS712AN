.class public Lcom/google/android/apps/youtube/app/ui/SliderLayout;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# instance fields
.field private final a:I

.field private b:Z

.field private c:I

.field private d:Z

.field private e:I

.field private f:I

.field private final g:[Landroid/view/View;

.field private final h:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

.field private final i:[I

.field private final j:[I

.field private k:I

.field private l:Z

.field private m:Z

.field private n:F

.field private o:I

.field private p:Landroid/widget/Scroller;

.field private q:Lcom/google/android/apps/youtube/app/ui/ft;

.field private r:Z

.field private s:Lcom/google/android/apps/youtube/app/ui/fu;

.field private t:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/high16 v5, -0x40800000    # -1.0f

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/16 v3, 0x28

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a:I

    new-instance v0, Landroid/widget/Scroller;

    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-direct {v0, p1, v3}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Landroid/widget/Scroller;

    iput v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    new-array v0, v4, [Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    new-array v0, v4, [I

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i:[I

    new-array v0, v4, [I

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->j:[I

    new-array v0, v4, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->ALL:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->t:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    sget-object v0, Lcom/google/android/youtube/r;->C:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;->HORIZONTAL:Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;->ordinal()I

    move-result v0

    invoke-virtual {v3, v4, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    sget-object v4, Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;->VERTICAL:Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;->ordinal()I

    move-result v4

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;->VERTICAL:Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->setOrientation(Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;)V

    invoke-virtual {v3, v2, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(II)V

    invoke-virtual {v3, v1, v5}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    const/4 v4, 0x3

    sget-object v5, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->DISPLACE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->ordinal()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c(I)Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    move-result-object v4

    aput-object v4, v0, v2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    const/4 v2, 0x4

    sget-object v4, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->DISPLACE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->ordinal()I

    move-result v4

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c(I)Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;->HORIZONTAL:Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;

    goto :goto_1
.end method

.method private a(II)V
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->f()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i:[I

    aput p2, v2, p1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->j:[I

    rsub-int/lit8 v3, p1, 0x1

    if-gtz p2, :cond_0

    :goto_0
    aput v0, v2, v3

    return-void

    :cond_0
    sub-int/2addr v1, p2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->r:Z

    if-eq v0, p1, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b(Z)V

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->r:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->r:Z

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/SliderLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:Z

    return v0
.end method

.method private b(IZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e()I

    move-result v0

    mul-int/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g(I)V

    if-nez p2, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    if-eq v0, p1, :cond_2

    :cond_1
    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e(I)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->invalidate()V

    return-void
.end method

.method private b(Z)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d(I)Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->RESIZE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    if-eq v2, v3, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d(I)Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->RESIZE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    if-eq v2, v3, :cond_0

    :goto_0
    if-nez v0, :cond_1

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b:Z

    goto :goto_1
.end method

.method private static c(I)Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->RESIZE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->ordinal()I

    move-result v0

    if-ne p0, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->RESIZE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->OCCLUDE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->ordinal()I

    move-result v0

    if-ne p0, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->OCCLUDE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->DISPLACE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    goto :goto_0
.end method

.method private d(I)Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;
    .locals 2

    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "index must be 0 or 1"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    aget-object v0, v0, p1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;->VERTICAL:Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;->HORIZONTAL:Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;

    goto :goto_0
.end method

.method private e()I
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->f()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->j:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->j:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    sub-int/2addr v0, v1

    return v0
.end method

.method private e(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lcom/google/android/apps/youtube/app/ui/ft;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lcom/google/android/apps/youtube/app/ui/ft;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/app/ui/ft;->a(I)V

    :cond_0
    return-void
.end method

.method private f()I
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingRight()I

    move-result v1

    sub-int v1, v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingTop()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v0, v2

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:Z

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private f(I)V
    .locals 4

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:I

    add-int/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_3

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d(I)Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->DISPLACE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e()I

    move-result v2

    mul-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:I

    sub-int/2addr v1, v2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d()Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;->VERTICAL:Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    aget-object v2, v2, v0

    int-to-float v1, v1

    invoke-virtual {v2, v1}, Landroid/view/View;->setTranslationY(F)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    aget-object v2, v2, v0

    int-to-float v1, v1

    invoke-virtual {v2, v1}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->requestLayout()V

    :cond_3
    return-void
.end method

.method private g()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lcom/google/android/apps/youtube/app/ui/ft;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lcom/google/android/apps/youtube/app/ui/ft;

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/youtube/app/ui/ft;->a(II)V

    :cond_0
    return-void
.end method

.method private g(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->requestLayout()V

    return-void
.end method

.method private h()V
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->j:[I

    aget v0, v0, v1

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    aget-object v2, v2, v1

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e()I

    move-result v4

    if-ne v3, v4, :cond_0

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->m:Z

    if-nez v3, :cond_0

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    :cond_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private h(I)V
    .locals 6

    const/4 v2, 0x0

    const/4 v0, 0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:I

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    sub-int v1, v0, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getFocusedChild()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_1

    if-eqz v1, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    aget-object v4, v4, v5

    if-ne v3, v4, :cond_1

    invoke-virtual {v3}, Landroid/view/View;->clearFocus()V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e()I

    move-result v3

    mul-int/2addr v0, v3

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:I

    sub-int v3, v0, v3

    mul-int/lit16 v5, v1, 0xc8

    if-nez v5, :cond_2

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v5

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Landroid/widget/Scroller;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:I

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->invalidate()V

    goto :goto_0
.end method

.method private i(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->m:Z

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b(IZ)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h()V

    return-void
.end method

.method public final a(F)V
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v2, "split must be in the range (0,1)"

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(ZLjava/lang/Object;)V

    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->n:F

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->m:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lcom/google/android/apps/youtube/app/ui/ft;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lcom/google/android/apps/youtube/app/ui/ft;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/ft;->v()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "index must be 0 or 1"

    invoke-static {v4, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    iput-boolean v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->m:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->f()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->n:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:I

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0, v4, v3}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b(IZ)V

    :goto_0
    return-void

    :cond_0
    iput v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    invoke-direct {p0, v4}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    goto :goto_0
.end method

.method public final a(IZ)V
    .locals 3

    const/4 v1, 0x0

    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "index must be 0 or 1"

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->m:Z

    if-eqz v0, :cond_1

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    goto :goto_1

    :cond_2
    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b(IZ)V

    goto :goto_1
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e:I

    if-ne p2, v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->f:I

    if-ne p2, v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    aget-object v0, v0, v2

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->m:Z

    return v0
.end method

.method public final b(I)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "index must be 0 or 1"

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    if-ne v0, p1, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->r:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 4

    const/4 v3, -0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->postInvalidate()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:I

    if-eq v0, v3, :cond_0

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:I

    const/4 v2, 0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:I

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    if-eq v0, v1, :cond_0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e(I)V

    goto :goto_0
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e:I

    if-ne p2, v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    if-ne v1, v0, :cond_0

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->f:I

    if-ne p2, v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    if-nez v1, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 5

    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getChildCount()I

    move-result v0

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "SliderLayout must have 2 child views."

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v0, v2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v0, v1

    move v0, v2

    :goto_1
    if-ge v0, v4, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    aget-object v3, v3, v0

    invoke-virtual {v3, v1}, Landroid/view/View;->setClickable(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    aget-object v0, v0, v1

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->OCCLUDE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    aget-object v0, v0, v2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->bringChildToFront(Landroid/view/View;)V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g()V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->m:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lcom/google/android/apps/youtube/app/ui/ft;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lcom/google/android/apps/youtube/app/ui/ft;

    invoke-interface {v2, p1}, Lcom/google/android/apps/youtube/app/ui/ft;->a(Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->s:Lcom/google/android/apps/youtube/app/ui/fu;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/fu;->a()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    :cond_3
    :goto_1
    :pswitch_0
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->r:Z

    goto :goto_0

    :pswitch_1
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->r:Z

    if-eqz v1, :cond_4

    move v1, v0

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->s:Lcom/google/android/apps/youtube/app/ui/fu;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/app/ui/fu;->f(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(Z)V

    goto :goto_1

    :pswitch_2
    sget-object v2, Lcom/google/android/apps/youtube/app/ui/fs;->a:[I

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->t:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    move v2, v0

    :goto_2
    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->s:Lcom/google/android/apps/youtube/app/ui/fu;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/youtube/app/ui/fu;->b(Landroid/view/MotionEvent;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->isFinished()Z

    move-result v2

    if-nez v2, :cond_9

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(Z)V

    goto :goto_1

    :pswitch_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getLeft()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_5

    move v2, v0

    goto :goto_2

    :cond_5
    move v2, v1

    goto :goto_2

    :pswitch_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getTop()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_6

    move v2, v0

    goto :goto_2

    :cond_6
    move v2, v1

    goto :goto_2

    :pswitch_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getRight()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_7

    move v2, v0

    goto :goto_2

    :cond_7
    move v2, v1

    goto :goto_2

    :pswitch_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getBottom()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_8

    move v2, v0

    goto :goto_2

    :cond_8
    move v2, v1

    goto :goto_2

    :cond_9
    move v0, v1

    goto :goto_3

    :pswitch_7
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->s:Lcom/google/android/apps/youtube/app/ui/fu;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/fu;->a()V

    goto/16 :goto_1

    :pswitch_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->s:Lcom/google/android/apps/youtube/app/ui/fu;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/fu;->e(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_7
        :pswitch_1
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d()Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;->VERTICAL:Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    aget-object v0, v1, v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    aget-object v0, v1, v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    :goto_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->j:[I

    const/4 v1, 0x0

    aget v2, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->j:[I

    const/4 v1, 0x1

    aget v3, v0, v1

    const/4 v0, 0x2

    new-array v4, v0, [I

    const/4 v0, 0x2

    new-array v5, v0, [I

    const/4 v0, 0x2

    new-array v6, v0, [I

    const/4 v0, 0x0

    :goto_1
    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    aput v1, v4, v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    aput v1, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    aget-object v0, v1, v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    aget-object v0, v1, v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0

    :cond_1
    sub-int v0, p4, p2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingRight()I

    move-result v1

    sub-int v1, v0, v1

    sub-int v0, p5, p3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingTop()I

    move-result v7

    sub-int/2addr v0, v7

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v0, v7

    iget-boolean v7, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:Z

    if-eqz v7, :cond_2

    :goto_2
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:Z

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingTop()I

    move-result v1

    :goto_3
    add-int/2addr v0, v1

    iget-boolean v7, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->m:Z

    if-eqz v7, :cond_5

    const/4 v0, 0x0

    aput v1, v6, v0

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    aget v0, v5, v0

    :goto_4
    add-int/2addr v0, v1

    aput v0, v6, v2

    :goto_5
    const/4 v0, 0x0

    :goto_6
    const/4 v1, 0x2

    if-ge v0, v1, :cond_9

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:Z

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    aget-object v1, v1, v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingLeft()I

    move-result v2

    aget v3, v6, v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingLeft()I

    move-result v7

    aget v8, v4, v0

    add-int/2addr v7, v8

    aget v8, v6, v0

    aget v9, v5, v0

    add-int/2addr v8, v9

    invoke-virtual {v1, v2, v3, v7, v8}, Landroid/view/View;->layout(IIII)V

    :goto_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingLeft()I

    move-result v1

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    aget v0, v4, v0

    goto :goto_4

    :cond_5
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    sget-object v8, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->DISPLACE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    if-ne v7, v8, :cond_6

    const/4 v7, 0x0

    iget v8, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:I

    sub-int v8, v1, v8

    aput v8, v6, v7

    :goto_8
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    const/4 v8, 0x1

    aget-object v7, v7, v8

    sget-object v8, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->OCCLUDE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    if-ne v7, v8, :cond_7

    const/4 v0, 0x1

    add-int/2addr v1, v2

    aput v1, v6, v0

    goto :goto_5

    :cond_6
    const/4 v7, 0x0

    aput v1, v6, v7

    goto :goto_8

    :cond_7
    const/4 v1, 0x1

    sub-int/2addr v0, v3

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:I

    sub-int/2addr v0, v2

    aput v0, v6, v1

    goto :goto_5

    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    aget-object v1, v1, v0

    aget v2, v6, v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingTop()I

    move-result v3

    aget v7, v6, v0

    aget v8, v4, v0

    add-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingTop()I

    move-result v8

    aget v9, v5, v0

    add-int/2addr v8, v9

    invoke-virtual {v1, v2, v3, v7, v8}, Landroid/view/View;->layout(IIII)V

    goto :goto_7

    :cond_9
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:Z

    if-eqz v0, :cond_a

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:Z

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_b

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    :cond_a
    :goto_9
    return-void

    :cond_b
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e()I

    move-result v1

    mul-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g(I)V

    goto :goto_9
.end method

.method protected onMeasure(II)V
    .locals 12

    const/4 v11, 0x2

    const/high16 v10, 0x40000000    # 2.0f

    const/4 v9, 0x1

    const/4 v3, 0x0

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->j:[I

    aget v4, v0, v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->j:[I

    aget v5, v0, v9

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingRight()I

    move-result v1

    sub-int v2, v0, v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getPaddingBottom()I

    move-result v1

    sub-int v1, v0, v1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    new-array v6, v11, [I

    iget-boolean v7, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->m:Z

    if-eqz v7, :cond_1

    int-to-float v4, v0

    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->n:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    aput v4, v6, v3

    aget v4, v6, v3

    sub-int/2addr v0, v4

    aput v0, v6, v9

    :goto_1
    move v0, v3

    :goto_2
    if-ge v0, v11, :cond_5

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    aget-object v3, v3, v0

    invoke-static {v2, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    aget v5, v6, v0

    invoke-static {v5, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    aget-object v7, v7, v3

    sget-object v8, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->RESIZE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    if-ne v7, v8, :cond_2

    sub-int v7, v0, v5

    iget v8, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:I

    sub-int/2addr v7, v8

    aput v7, v6, v3

    :goto_4
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    aget-object v7, v7, v9

    sget-object v8, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->RESIZE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    if-ne v7, v8, :cond_3

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:I

    add-int/2addr v0, v5

    aput v0, v6, v9

    goto :goto_1

    :cond_2
    sub-int v7, v0, v5

    aput v7, v6, v3

    goto :goto_4

    :cond_3
    sub-int/2addr v0, v4

    aput v0, v6, v9

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    aget-object v3, v3, v0

    aget v4, v6, v0

    invoke-static {v4, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v1, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    goto :goto_3

    :cond_5
    return-void
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:I

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g:[Landroid/view/View;

    aget-object v0, v1, v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    :goto_1
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    const/4 v2, 0x0

    check-cast p1, Lcom/google/android/apps/youtube/app/ui/DefaultSliderSavedState;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ui/DefaultSliderSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v0, p1, Lcom/google/android/apps/youtube/app/ui/DefaultSliderSavedState;->expandedLayer:I

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    :cond_0
    iput v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    :cond_1
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b(IZ)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/DefaultSliderSavedState;

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/app/ui/DefaultSliderSavedState;-><init>(Landroid/os/Parcelable;)V

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    iput v1, v0, Lcom/google/android/apps/youtube/app/ui/DefaultSliderSavedState;->expandedLayer:I

    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 4

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v1, 0x0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    move v0, v1

    :goto_0
    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i:[I

    aget v2, v2, v0

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:Z

    if-eqz v0, :cond_1

    if-ne p2, p4, :cond_2

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:Z

    if-nez v0, :cond_3

    if-eq p1, p3, :cond_3

    :cond_2
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_4

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->o:I

    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b(IZ)V

    :cond_3
    invoke-static {p1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {p2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->measure(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->requestLayout()V

    return-void

    :cond_4
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->s:Lcom/google/android/apps/youtube/app/ui/fu;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/fu;->a(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v4

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->s:Lcom/google/android/apps/youtube/app/ui/fu;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/fu;->b(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->s:Lcom/google/android/apps/youtube/app/ui/fu;

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/ui/fu;->g:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    iget-boolean v1, v1, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:Z

    if-eqz v1, :cond_2

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/fu;->d(Landroid/view/MotionEvent;)I

    move-result v0

    :goto_1
    if-gez v0, :cond_3

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:I

    if-lez v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:I

    neg-int v1, v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->f(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/fu;->c(Landroid/view/MotionEvent;)I

    move-result v0

    goto :goto_1

    :cond_3
    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:I

    sub-int/2addr v1, v2

    if-lez v1, :cond_0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->f(I)V

    goto :goto_0

    :pswitch_3
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->r:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->s:Lcom/google/android/apps/youtube/app/ui/fu;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/fu;->g(Landroid/view/MotionEvent;)Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;->BACK:Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;

    if-ne v0, v1, :cond_5

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    if-ne v1, v4, :cond_5

    invoke-direct {p0, v3}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    :cond_4
    :goto_2
    invoke-direct {p0, v3}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->s:Lcom/google/android/apps/youtube/app/ui/fu;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/fu;->a()V

    goto :goto_0

    :cond_5
    sget-object v1, Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;->FORWARD:Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;

    if-ne v0, v1, :cond_6

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    if-nez v0, :cond_6

    invoke-direct {p0, v4}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    goto :goto_2

    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c:I

    div-int/lit8 v2, v0, 0x2

    add-int/2addr v1, v2

    div-int v0, v1, v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    goto :goto_2

    :pswitch_4
    invoke-direct {p0, v3}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->s:Lcom/google/android/apps/youtube/app/ui/fu;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/fu;->a()V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->s:Lcom/google/android/apps/youtube/app/ui/fu;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/fu;->e(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->isInTouchMode()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    :cond_0
    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->p:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h(I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setCollapseStrategy(ILcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;)V
    .locals 3

    if-ltz p1, :cond_2

    const/4 v0, 0x2

    if-ge p1, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v1, "index must be 0 or 1"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    aput-object p2, v0, p1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    rsub-int/lit8 v1, p1, 0x1

    aget-object v0, v0, v1

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->OCCLUDE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->OCCLUDE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    if-ne p2, v0, :cond_0

    const-string v0, "Both collapse strategies cannot be OCCLUDE"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->h:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    rsub-int/lit8 v1, p1, 0x1

    sget-object v2, Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;->DISPLACE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$CollapseStrategy;

    aput-object v2, v0, v1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->g()V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->requestLayout()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setExpandedSize(II)V
    .locals 3

    const/4 v1, 0x0

    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "index must be 0 or 1"

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->a(II)V

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->b(IZ)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public setListener(Lcom/google/android/apps/youtube/app/ui/ft;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->q:Lcom/google/android/apps/youtube/app/ui/ft;

    return-void
.end method

.method public setOrientation(Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;)V
    .locals 3

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;->VERTICAL:Lcom/google/android/apps/youtube/app/ui/SliderLayout$Orientation;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x21

    :goto_1
    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->e:I

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->d:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x82

    :goto_2
    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->f:I

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/fu;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/youtube/app/ui/fu;-><init>(Lcom/google/android/apps/youtube/app/ui/SliderLayout;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->s:Lcom/google/android/apps/youtube/app/ui/fu;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->l:Z

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->i(I)V

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->k:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->requestLayout()V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const/16 v0, 0x11

    goto :goto_1

    :cond_3
    const/16 v0, 0x42

    goto :goto_2
.end method

.method public setTouchInterceptArea(Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;)V
    .locals 0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->t:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    return-void
.end method
