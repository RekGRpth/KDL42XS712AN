.class final Lcom/google/android/apps/youtube/app/fragments/bj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/Analytics;

.field final synthetic b:Lcom/google/android/apps/youtube/app/remote/ax;

.field final synthetic c:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

.field final synthetic d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/app/remote/ax;Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/bj;->d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/fragments/bj;->a:Lcom/google/android/apps/youtube/core/Analytics;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/fragments/bj;->b:Lcom/google/android/apps/youtube/app/remote/ax;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/fragments/bj;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bj;->a:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "MdxModalQueue"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bj;->d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->g(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/bj;->d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/bj;->d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->d(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->c(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bj;->b:Lcom/google/android/apps/youtube/app/remote/ax;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/bj;->d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->d(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/ax;->a(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bj;->d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->gF:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bj;->d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->a()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/bj;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/bj;->d:Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->bh:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    goto :goto_1
.end method
