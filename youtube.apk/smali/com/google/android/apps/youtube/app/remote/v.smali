.class final Lcom/google/android/apps/youtube/app/remote/v;
.super Lcom/google/android/apps/youtube/app/remote/ac;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/common/a/b;

.field final synthetic b:Lcom/google/android/apps/youtube/app/remote/t;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/remote/t;Ljava/lang/Void;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/v;->b:Lcom/google/android/apps/youtube/app/remote/t;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/remote/v;->a:Lcom/google/android/apps/youtube/common/a/b;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/apps/youtube/app/remote/ac;-><init>(Lcom/google/android/apps/youtube/app/remote/t;Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 8

    const/4 v1, 0x0

    const/4 v7, 0x0

    check-cast p2, Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/v;->a:Lcom/google/android/apps/youtube/common/a/b;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v7, v1}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/v;->b:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/t;->d(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/v;->b:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/t;->d(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->x()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasScreen()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/ytremote/model/CloudScreen;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/v;->b:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/t;->d(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->x()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/v;->a:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v2, v7, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getAccessType()Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;->PERMANENT:Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/remote/v;->b:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/remote/t;->e(Lcom/google/android/apps/youtube/app/remote/t;)Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    int-to-double v3, v0

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v3, v5

    const-wide/high16 v5, 0x4018000000000000L    # 6.0

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    move-result-wide v3

    double-to-int v0, v3

    add-int/lit8 v3, v0, 0x1

    move v0, v1

    :goto_2
    if-ge v0, v3, :cond_4

    mul-int/lit8 v1, v0, 0x6

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v5, v1, 0x6

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/remote/v;->b:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v5}, Lcom/google/android/apps/youtube/app/remote/t;->e(Lcom/google/android/apps/youtube/app/remote/t;)Ljava/util/Map;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/remote/v;->b:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v6}, Lcom/google/android/apps/youtube/app/remote/t;->f(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/ytremote/backend/a/e;

    move-result-object v6

    invoke-interface {v2, v1, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v6, v1}, Lcom/google/android/apps/ytremote/backend/a/e;->a(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getAccessType()Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    move-result-object v1

    sget-object v4, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;->PERMANENT:Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    if-ne v1, v4, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/v;->b:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/t;->e(Lcom/google/android/apps/youtube/app/remote/t;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/v;->b:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/t;->e(Lcom/google/android/apps/youtube/app/remote/t;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/ytremote/model/LoungeToken;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/ytremote/model/CloudScreen;->withLoungeToken(Lcom/google/android/apps/ytremote/model/LoungeToken;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_6
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/v;->b:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/t;->g(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/ytremote/backend/a/j;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/ytremote/backend/a/j;->a(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    new-instance v4, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-direct {v4, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;-><init>(Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/v;->b:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/t;->d(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/v;->b:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/t;->d(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->x()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasScreen()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/v;->b:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/t;->d(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->x()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/v;->b:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/t;->d(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->x()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/v;->a:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, v7, v3}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
