.class public final Lcom/google/android/apps/youtube/app/offline/a/a;
.super Lcom/google/android/apps/youtube/core/offline/store/b;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/ax;

.field private final b:Lcom/google/android/apps/youtube/common/c/a;

.field private final c:Lcom/google/android/apps/youtube/core/identity/l;

.field private final d:Lcom/google/android/apps/youtube/core/client/ce;

.field private final e:Lcom/google/android/apps/youtube/core/utils/w;

.field private final f:Lcom/google/android/apps/youtube/app/offline/p;

.field private final g:Ljava/util/HashMap;

.field private volatile h:Lcom/google/android/apps/youtube/app/offline/a/f;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ax;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/client/ce;Lcom/google/android/apps/youtube/app/offline/p;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/offline/store/b;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ax;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->b:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/ce;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->d:Lcom/google/android/apps/youtube/core/client/ce;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/p;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->f:Lcom/google/android/apps/youtube/app/offline/p;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ax;->af()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/a/b;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/app/offline/a/b;-><init>(Lcom/google/android/apps/youtube/app/offline/a/a;B)V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/transfer/h;)Lcom/google/android/apps/youtube/core/utils/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->e:Lcom/google/android/apps/youtube/core/utils/w;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->g:Ljava/util/HashMap;

    invoke-virtual {p2, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/offline/a/a;)Lcom/google/android/apps/youtube/app/offline/a/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->h:Lcom/google/android/apps/youtube/app/offline/a/f;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;
    .locals 9

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/offline/a/a;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/a/f;

    :goto_1
    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/a/f;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->a:Lcom/google/android/apps/youtube/app/ax;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->b:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->bd()Lcom/google/android/apps/youtube/core/player/a/l;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->d:Lcom/google/android/apps/youtube/core/client/ce;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->e:Lcom/google/android/apps/youtube/core/utils/w;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->f:Lcom/google/android/apps/youtube/app/offline/p;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->aQ()Lcom/google/android/apps/youtube/core/identity/as;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v7, v8}, Lcom/google/android/apps/youtube/core/identity/as;->a(Lcom/google/android/apps/youtube/core/identity/l;)Landroid/accounts/Account;

    move-result-object v8

    move-object v7, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/app/offline/a/f;-><init>(Lcom/google/android/apps/youtube/app/ax;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/player/a/l;Lcom/google/android/apps/youtube/core/client/ce;Lcom/google/android/apps/youtube/core/utils/w;Lcom/google/android/apps/youtube/app/offline/p;Ljava/lang/String;Landroid/accounts/Account;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->g:Ljava/util/HashMap;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final handleSignInEvent(Lcom/google/android/apps/youtube/core/identity/ai;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/offline/a/a;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/a/f;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->h:Lcom/google/android/apps/youtube/app/offline/a/f;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->h:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->e:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/w;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/k;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/transfer/k;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final handleSignOutEvent(Lcom/google/android/apps/youtube/core/identity/aj;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->e:Lcom/google/android/apps/youtube/core/utils/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/w;->a()Landroid/os/Binder;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/transfer/k;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/transfer/k;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->h:Lcom/google/android/apps/youtube/app/offline/a/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->h:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->b()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->h:Lcom/google/android/apps/youtube/app/offline/a/f;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bp()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/a;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bd()Lcom/google/android/apps/youtube/core/player/a/l;

    move-result-object v0

    invoke-interface {v0, v1, v1}, Lcom/google/android/apps/youtube/core/player/a/l;->a(Lcom/google/android/exoplayer/upstream/cache/a;Lcom/google/android/exoplayer/upstream/cache/a;)V

    return-void
.end method
