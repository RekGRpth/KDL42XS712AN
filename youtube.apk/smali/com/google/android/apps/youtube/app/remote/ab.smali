.class final Lcom/google/android/apps/youtube/app/remote/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/common/a/b;

.field final synthetic b:Lcom/google/android/apps/ytremote/model/CloudScreen;

.field final synthetic c:Lcom/google/android/apps/youtube/app/remote/t;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/remote/t;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/ytremote/model/CloudScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/ab;->c:Lcom/google/android/apps/youtube/app/remote/t;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/remote/ab;->a:Lcom/google/android/apps/youtube/common/a/b;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/remote/ab;->b:Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ab;->c:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/t;->b(Lcom/google/android/apps/youtube/app/remote/t;)Landroid/util/Pair;

    move-result-object v2

    invoke-static {}, Lcom/google/android/apps/youtube/app/remote/t;->a()Landroid/util/Pair;

    move-result-object v0

    if-ne v2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ab;->a:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ab;->b:Lcom/google/android/apps/ytremote/model/CloudScreen;

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Authentication failed."

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ab;->c:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/t;->c(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/ytremote/backend/logic/a;

    move-result-object v3

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v3, v0, v1}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ab;->c:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/t;->g(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/ytremote/backend/a/j;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/ab;->b:Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-static {v3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/ytremote/backend/a/j;->a(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ab;->b:Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ab;->c:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/t;->c(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/ytremote/backend/logic/a;

    move-result-object v4

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/ab;->b:Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-interface {v4, v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    :cond_1
    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ab;->b:Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ab;->b:Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ab;->a:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ab;->b:Lcom/google/android/apps/ytremote/model/CloudScreen;

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "The screen is no longer available"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ab;->a:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ab;->b:Lcom/google/android/apps/ytremote/model/CloudScreen;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/ab;->b:Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method
