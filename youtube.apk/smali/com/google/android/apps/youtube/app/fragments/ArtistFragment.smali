.class public Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;
.super Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/fp;


# instance fields
.field private Y:Ljava/lang/String;

.field private Z:Landroid/widget/TextView;

.field public a:Lcom/google/android/apps/youtube/app/fragments/g;

.field private aa:Landroid/widget/TextView;

.field private ab:Z

.field private ac:Landroid/view/View;

.field private ad:Landroid/view/ViewStub;

.field private ae:Landroid/view/View;

.field private af:Landroid/view/View;

.field private ag:Landroid/widget/TextView;

.field private ah:Landroid/widget/ProgressBar;

.field private ai:Lcom/google/android/apps/youtube/app/ui/v;

.field private aj:Lcom/google/android/apps/youtube/app/ui/dw;

.field private ak:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

.field private al:Z

.field private am:Z

.field private b:Lcom/google/android/apps/youtube/app/am;

.field private d:Lcom/google/android/apps/youtube/core/client/bq;

.field private e:Lcom/google/android/apps/youtube/app/ax;

.field private f:Landroid/widget/ListView;

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;-><init>()V

    return-void
.end method

.method private L()V
    .locals 4

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ae:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ac:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->al:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->am:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ae:Landroid/view/View;

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->al:Z

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->am:Z

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ac:Landroid/view/View;

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->am:Z

    if-eqz v3, :cond_3

    :goto_3
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method private M()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ab:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/youtube/h;->e:I

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->Z:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v2, v0, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    return-void

    :cond_0
    sget v0, Lcom/google/android/youtube/h;->d:I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->Z:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->Y:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;->relatedArtists:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;->relatedArtists:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;->relatedArtists:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->af:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x3

    if-le v0, v2, :cond_1

    sget v0, Lcom/google/android/youtube/l;->g:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->h:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->cH:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ag:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ag:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/c;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/fragments/c;-><init>(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/fragments/d;-><init>(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/g;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v3, Lcom/google/android/youtube/l;->aQ:I

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/google/android/apps/youtube/app/fragments/g;-><init>(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;Landroid/content/Context;ILcom/google/android/apps/youtube/app/adapter/ai;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->a:Lcom/google/android/apps/youtube/app/fragments/g;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->a:Lcom/google/android/apps/youtube/app/fragments/g;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/fragments/g;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->a:Lcom/google/android/apps/youtube/app/fragments/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/g;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->a:Lcom/google/android/apps/youtube/app/fragments/g;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/fragments/e;-><init>(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :cond_0
    return-void

    :cond_1
    sget v0, Lcom/google/android/youtube/l;->m:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->h:Landroid/view/View;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ab:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->aa:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->M()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)Lcom/google/android/apps/youtube/app/ui/dw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->aj:Lcom/google/android/apps/youtube/app/ui/dw;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ab:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ab:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ab:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->aa:Landroid/widget/TextView;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->M()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestLayout()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->aa:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    goto :goto_1
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ag:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->f:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)Lcom/google/android/apps/youtube/app/am;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->b:Lcom/google/android/apps/youtube/app/am;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    const/4 v10, 0x0

    const/16 v9, 0x8

    sget v0, Lcom/google/android/youtube/l;->f:I

    invoke-virtual {p1, v0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->f:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aV()Lcom/google/android/apps/youtube/core/identity/ak;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v8

    sget v0, Lcom/google/android/youtube/l;->h:I

    invoke-virtual {p1, v0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->g:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->g:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->w:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    sget v0, Lcom/google/android/youtube/j;->x:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->Z:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->v:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->aa:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ai:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->g:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->eb:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ac:Landroid/view/View;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/dw;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->b:Lcom/google/android/apps/youtube/app/am;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ai:Lcom/google/android/apps/youtube/app/ui/v;

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/ui/dw;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/app/ui/fp;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->aj:Lcom/google/android/apps/youtube/app/ui/dw;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->aj:Lcom/google/android/apps/youtube/app/ui/dw;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ac:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/dw;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ac:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->g:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fu:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ad:Landroid/view/ViewStub;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ad:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ae:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->aj:Lcom/google/android/apps/youtube/app/ui/dw;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ae:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ai:Lcom/google/android/apps/youtube/app/ui/v;

    const/4 v4, 0x2

    invoke-static {v2, v8, v3, v4}, Lcom/google/android/apps/youtube/app/adapter/ag;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;I)Lcom/google/android/apps/youtube/app/adapter/ai;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/ui/dw;->a(Landroid/view/View;Lcom/google/android/apps/youtube/app/adapter/ai;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ae:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->g:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->dR:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ah:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->g:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->ea:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->af:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->af:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ah:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->d:Lcom/google/android/apps/youtube/core/client/bq;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/a;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/app/fragments/a;-><init>(Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bq;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ai:Lcom/google/android/apps/youtube/app/ui/v;

    sget-object v4, Lcom/google/android/apps/youtube/core/client/WatchFeature;->ARTIST_VIDEOS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v5

    move-object v2, v10

    move-object v3, v7

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ak:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->f:Landroid/widget/ListView;

    return-object v0
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->Y:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->e:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->N()Lcom/google/android/apps/youtube/core/client/bq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->d:Lcom/google/android/apps/youtube/core/client/bq;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->b:Lcom/google/android/apps/youtube/app/am;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->h()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "artist_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->i:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d(Z)V

    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 2

    if-lez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->am:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ah:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->L()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestLayout()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->a:Lcom/google/android/apps/youtube/app/fragments/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->a:Lcom/google/android/apps/youtube/app/fragments/g;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/g;->notifyDataSetChanged()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h_()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ah:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->L()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestLayout()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->a:Lcom/google/android/apps/youtube/app/fragments/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->a:Lcom/google/android/apps/youtube/app/fragments/g;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/g;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->al:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->L()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->r()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->j()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->al:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ak:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->L()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->t()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ArtistFragment;->ak:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a()V

    return-void
.end method
