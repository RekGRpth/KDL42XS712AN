.class public final Lcom/google/android/apps/youtube/app/e/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/async/af;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Lcom/google/android/apps/youtube/core/async/af;

.field private final d:Ljava/util/concurrent/ConcurrentMap;

.field private final e:Lcom/google/android/apps/youtube/core/client/bj;

.field private final f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/client/bj;Ljava/util/concurrent/ConcurrentMap;IIZ)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/e/b;->c:Lcom/google/android/apps/youtube/core/async/af;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/e/b;->e:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/e/b;->d:Ljava/util/concurrent/ConcurrentMap;

    const-string v0, "minDesiredTeasers must be > 0"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    const-string v0, "maxTeasers must be > minDesiredTeasers"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    const/16 v0, 0x8

    iput v0, p0, Lcom/google/android/apps/youtube/app/e/b;->a:I

    const/16 v0, 0x18

    iput v0, p0, Lcom/google/android/apps/youtube/app/e/b;->b:I

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/e/b;->f:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/e/b;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/e/b;->f:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/e/b;)Ljava/util/concurrent/ConcurrentMap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/e/b;->d:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/e/b;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/e/b;->b:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/e/b;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/e/b;->a:I

    return v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/e/b;)Lcom/google/android/apps/youtube/core/async/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/e/b;->c:Lcom/google/android/apps/youtube/core/async/af;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/e/b;)Lcom/google/android/apps/youtube/core/client/bj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/e/b;->e:Lcom/google/android/apps/youtube/core/client/bj;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/common/L;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/e/b;->c:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v1, Lcom/google/android/apps/youtube/app/e/c;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/youtube/app/e/c;-><init>(Lcom/google/android/apps/youtube/app/e/b;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/app/e/b;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
