.class public final Lcom/google/android/apps/youtube/app/ui/ij;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/offline/v;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/app/ui/hj;

.field private c:Lcom/google/a/a/a/a/uv;

.field private d:Ljava/lang/String;

.field private final e:Ljava/util/Map;

.field private final f:Landroid/view/View$OnClickListener;

.field private final g:Landroid/view/View$OnClickListener;

.field private final h:Landroid/view/View$OnClickListener;

.field private final i:Lcom/google/android/apps/youtube/app/ui/ir;

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/ui/hj;Lcom/google/android/apps/youtube/app/ui/ir;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->e:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/hj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->b:Lcom/google/android/apps/youtube/app/ui/hj;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/ij;->i:Lcom/google/android/apps/youtube/app/ui/ir;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/ik;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/ik;-><init>(Lcom/google/android/apps/youtube/app/ui/ij;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->f:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/il;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/il;-><init>(Lcom/google/android/apps/youtube/app/ui/ij;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->g:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/im;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/im;-><init>(Lcom/google/android/apps/youtube/app/ui/ij;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->h:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/ij;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/ij;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->h:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/ij;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->f:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/ij;)Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->g:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/ui/ij;)Lcom/google/android/apps/youtube/app/ui/hj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->b:Lcom/google/android/apps/youtube/app/ui/hj;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/ui/ij;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/ui/ij;)Lcom/google/android/apps/youtube/app/ui/ir;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->i:Lcom/google/android/apps/youtube/app/ui/ir;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->j:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->j:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/is;

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/ij;->j:Z

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/app/ui/is;->b(Z)V

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/ip;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/youtube/app/ui/ip;-><init>(Lcom/google/android/apps/youtube/app/ui/ij;Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ij;->e:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ij;->b:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ip;->a(Lcom/google/android/apps/youtube/app/ui/ip;)Landroid/widget/TextView;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ip;->b(Lcom/google/android/apps/youtube/app/ui/ip;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method public final a(Lcom/google/a/a/a/a/uv;Lcom/google/android/apps/youtube/datalib/legacy/model/x;Ljava/lang/String;Z)V
    .locals 8

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/uv;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->c:Lcom/google/a/a/a/a/uv;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->d:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/google/android/apps/youtube/app/ui/ij;->j:Z

    iget-object v0, p1, Lcom/google/a/a/a/a/uv;->f:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v1

    iget-object v0, p1, Lcom/google/a/a/a/a/uv;->k:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/a/a/a/a/uv;->k:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    if-eqz v1, :cond_1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/CharSequence;

    aput-object v0, v2, v3

    const/4 v0, 0x1

    const-string v3, " \u00b7 "

    aput-object v3, v2, v0

    const/4 v0, 0x2

    aput-object v1, v2, v0

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    move-object v1, v0

    :cond_0
    :goto_0
    iget-object v0, p1, Lcom/google/a/a/a/a/uv;->b:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v2

    iget-object v0, p1, Lcom/google/a/a/a/a/uv;->d:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v3

    iget-object v0, p1, Lcom/google/a/a/a/a/uv;->e:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v4

    iget-object v0, p1, Lcom/google/a/a/a/a/uv;->c:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/is;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/app/ui/is;->a(Ljava/lang/CharSequence;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/ui/is;->b(Ljava/lang/CharSequence;)V

    invoke-interface {v0, v5, v3, v4}, Lcom/google/android/apps/youtube/app/ui/is;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    iget-boolean v7, p0, Lcom/google/android/apps/youtube/app/ui/ij;->j:Z

    invoke-interface {v0, v7}, Lcom/google/android/apps/youtube/app/ui/is;->b(Z)V

    invoke-interface {v0, p4, p2}, Lcom/google/android/apps/youtube/app/ui/is;->a(ZLcom/google/android/apps/youtube/datalib/legacy/model/x;)V

    iget-boolean v7, p1, Lcom/google/a/a/a/a/uv;->i:Z

    invoke-interface {v0, v7}, Lcom/google/android/apps/youtube/app/ui/is;->a(Z)V

    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/ui/LikeAction;)V
    .locals 5

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/LikeAction;->LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    if-ne p1, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->c:Lcom/google/a/a/a/a/uv;

    iget-object v0, v0, Lcom/google/a/a/a/a/uv;->g:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->c:Lcom/google/a/a/a/a/uv;

    iget-object v0, v0, Lcom/google/a/a/a/a/uv;->e:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    move-object v2, v1

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->c:Lcom/google/a/a/a/a/uv;

    iget-object v0, v0, Lcom/google/a/a/a/a/uv;->c:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/is;

    invoke-interface {v0, v3, v2, v1}, Lcom/google/android/apps/youtube/app/ui/is;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_0
    sget-object v1, Lcom/google/android/apps/youtube/app/ui/LikeAction;->DISLIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    if-ne p1, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->c:Lcom/google/a/a/a/a/uv;

    iget-object v0, v0, Lcom/google/a/a/a/a/uv;->d:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->c:Lcom/google/a/a/a/a/uv;

    iget-object v0, v0, Lcom/google/a/a/a/a/uv;->h:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/google/android/apps/youtube/app/ui/LikeAction;->REMOVE_LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    if-ne p1, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->c:Lcom/google/a/a/a/a/uv;

    iget-object v0, v0, Lcom/google/a/a/a/a/uv;->d:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->c:Lcom/google/a/a/a/a/uv;

    iget-object v0, v0, Lcom/google/a/a/a/a/uv;->e:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_2
    return-void

    :cond_3
    move-object v1, v0

    move-object v2, v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/is;

    const/4 v2, 0x1

    invoke-interface {v0, v2, p1}, Lcom/google/android/apps/youtube/app/ui/is;->a(ZLcom/google/android/apps/youtube/datalib/legacy/model/x;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->d:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->ADDING:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/is;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/is;->a()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/in;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/youtube/app/ui/in;-><init>(Lcom/google/android/apps/youtube/app/ui/ij;Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ij;->e:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final c(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->i:Lcom/google/android/apps/youtube/app/ui/ir;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ij;->i:Lcom/google/android/apps/youtube/app/ui/ir;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/ir;->E()V

    :cond_0
    return-void
.end method
