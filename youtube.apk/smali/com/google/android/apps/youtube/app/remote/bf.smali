.class final Lcom/google/android/apps/youtube/app/remote/bf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/remote/aq;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/remote/bb;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/remote/bb;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/bf;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/remote/bb;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/bf;-><init>(Lcom/google/android/apps/youtube/app/remote/bb;)V

    return-void
.end method

.method private b()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bf;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bf;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->b(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bf;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->h()Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bf;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->k()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a(ZZ)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;)V
    .locals 3

    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->STOPPED:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/bd;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bf;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bb;->b(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a(Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bf;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->b(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    move-result-object v1

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->ADVERTISEMENT:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->b(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bf;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->b(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a()V

    return-void

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->PAUSED:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->ERROR:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->PLAYING:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->BUFFERING:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;->ENDED:Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup$PlaybackInfo$PlaybackState;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->ERROR:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne p1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bf;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->b(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->b()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bf;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->b(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bf;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->b(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->b(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/bf;->b()V

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/bf;->b()V

    return-void
.end method

.method public final l_()V
    .locals 0

    return-void
.end method
