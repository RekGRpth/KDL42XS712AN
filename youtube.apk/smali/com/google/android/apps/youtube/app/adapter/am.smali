.class final Lcom/google/android/apps/youtube/app/adapter/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/adapter/al;

.field private final b:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

.field private final c:Lcom/google/android/apps/youtube/common/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/adapter/al;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/adapter/am;->a:Lcom/google/android/apps/youtube/app/adapter/al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/adapter/am;->b:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/adapter/am;->c:Lcom/google/android/apps/youtube/common/a/b;

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/am;->a:Lcom/google/android/apps/youtube/app/adapter/al;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/al;->b(Lcom/google/android/apps/youtube/app/adapter/al;)Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/am;->b:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->ownerUri:Landroid/net/Uri;

    new-instance v2, Lcom/google/android/apps/youtube/app/adapter/an;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/am;->a:Lcom/google/android/apps/youtube/app/adapter/al;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/adapter/am;->b:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/adapter/am;->c:Lcom/google/android/apps/youtube/common/a/b;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/youtube/app/adapter/an;-><init>(Lcom/google/android/apps/youtube/app/adapter/al;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Landroid/net/Uri;

    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/am;->a:Lcom/google/android/apps/youtube/app/adapter/al;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/al;->a(Lcom/google/android/apps/youtube/app/adapter/al;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/am;->b:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/am;->c:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method
