.class final Lcom/google/android/apps/youtube/app/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/g;


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/ListView;

.field private final d:Landroid/widget/TextView;

.field private final e:Lcom/google/android/apps/youtube/uilib/a/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->I:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/m;->a:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m;->a:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/youtube/j;->ge:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/m;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m;->a:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/youtube/j;->i:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/m;->c:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m;->a:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/youtube/j;->aR:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/m;->d:Landroid/widget/TextView;

    new-instance v0, Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/uilib/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/m;->e:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m;->e:Lcom/google/android/apps/youtube/uilib/a/h;

    const-class v1, Lcom/google/android/apps/youtube/app/i;

    new-instance v2, Lcom/google/android/apps/youtube/app/k;

    invoke-direct {v2, p1}, Lcom/google/android/apps/youtube/app/k;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/m;->e:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 2

    check-cast p2, Lcom/google/android/apps/youtube/app/l;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m;->b:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/apps/youtube/app/l;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lcom/google/android/apps/youtube/app/l;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/google/android/apps/youtube/app/l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m;->a:Landroid/view/ViewGroup;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m;->e:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m;->e:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v1, p2, Lcom/google/android/apps/youtube/app/l;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m;->e:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m;->a:Landroid/view/ViewGroup;

    goto :goto_0
.end method
