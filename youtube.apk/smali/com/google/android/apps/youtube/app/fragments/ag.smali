.class final Lcom/google/android/apps/youtube/app/fragments/ag;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/common/a/b;

.field final synthetic b:Lcom/google/android/apps/youtube/core/async/GDataRequest;

.field final synthetic c:Lcom/google/android/apps/youtube/app/fragments/af;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/fragments/af;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/youtube/core/async/GDataRequest;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/ag;->c:Lcom/google/android/apps/youtube/app/fragments/af;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/fragments/ag;->a:Lcom/google/android/apps/youtube/common/a/b;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/fragments/ag;->b:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ag;->a:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ag;->b:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    invoke-interface {v0, v1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    const/4 v4, 0x0

    const/4 v1, 0x0

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;

    if-nez p2, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/ag;->a:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/fragments/ag;->b:Lcom/google/android/apps/youtube/core/async/GDataRequest;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v2, v1

    move v3, v1

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;-><init>(IIILandroid/net/Uri;Landroid/net/Uri;Ljava/util/List;)V

    invoke-interface {v7, v8, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Artist;->artistTape:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/MusicVideo;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/MusicVideo;->videoId:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ag;->c:Lcom/google/android/apps/youtube/app/fragments/af;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/af;->a(Lcom/google/android/apps/youtube/app/fragments/af;)Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->x()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/ah;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/fragments/ah;-><init>(Lcom/google/android/apps/youtube/app/fragments/ag;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
