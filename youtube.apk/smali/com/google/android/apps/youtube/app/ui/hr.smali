.class final Lcom/google/android/apps/youtube/app/ui/hr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/hj;

.field private final b:Lcom/google/android/apps/youtube/app/adapter/ad;

.field private final c:Lcom/google/android/apps/youtube/core/ui/l;

.field private final d:Lcom/google/android/apps/youtube/core/ui/PagedListView;

.field private e:Ljava/lang/String;

.field private f:Landroid/app/Dialog;

.field private g:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/hj;)V
    .locals 8

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/hr;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/ad;

    iget-object v1, p1, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    sget v2, Lcom/google/android/youtube/l;->aH:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/app/adapter/ad;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hr;->b:Lcom/google/android/apps/youtube/app/adapter/ad;

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->d:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hr;->d:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/hs;

    iget-object v2, p1, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/hr;->d:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/hr;->b:Lcom/google/android/apps/youtube/app/adapter/ad;

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/hj;->g(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v5

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v6

    move-object v1, p0

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/app/ui/hs;-><init>(Lcom/google/android/apps/youtube/app/ui/hr;Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/ui/hj;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hr;->c:Lcom/google/android/apps/youtube/core/ui/l;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hr;->d:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/ht;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/ui/ht;-><init>(Lcom/google/android/apps/youtube/app/ui/hr;Lcom/google/android/apps/youtube/app/ui/hj;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/hr;)Lcom/google/android/apps/youtube/app/adapter/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hr;->b:Lcom/google/android/apps/youtube/app/adapter/ad;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/hr;)Landroid/app/Dialog;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hr;->g:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/hr;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hr;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/hr;)Landroid/app/Dialog;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hr;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hr;->f:Landroid/app/Dialog;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hr;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->j(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/app/ui/bd;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/hv;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/ui/hv;-><init>(Lcom/google/android/apps/youtube/app/ui/hr;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/bd;->a(Lcom/google/android/apps/youtube/app/ui/bg;)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hr;->f:Landroid/app/Dialog;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hr;->f:Landroid/app/Dialog;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hr;->c:Lcom/google/android/apps/youtube/core/ui/l;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/hr;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/ui/hj;->e(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->m()Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/l;->a([Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hr;->g:Landroid/app/Dialog;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/core/ui/aa;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hr;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/ui/aa;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/youtube/p;->n:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/aa;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hr;->d:Lcom/google/android/apps/youtube/core/ui/PagedListView;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hr;->g:Landroid/app/Dialog;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hr;->g:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/hr;->e:Ljava/lang/String;

    return-void
.end method
