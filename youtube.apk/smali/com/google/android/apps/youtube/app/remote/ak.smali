.class public final Lcom/google/android/apps/youtube/app/remote/ak;
.super Lcom/google/android/apps/youtube/app/remote/r;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

.field private final c:Lcom/google/android/apps/youtube/app/remote/am;

.field private final d:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

.field private e:Lcom/google/android/apps/youtube/common/a/d;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/youtube/app/remote/am;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)V
    .locals 1

    invoke-direct {p0, p2, p1, p5}, Lcom/google/android/apps/youtube/app/remote/r;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ak;->b:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/remote/ak;->c:Lcom/google/android/apps/youtube/app/remote/am;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/remote/ak;->d:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/ak;Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/ak;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V
    .locals 8

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v2, ""

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ak;->c:Lcom/google/android/apps/youtube/app/remote/am;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/am;->K()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ak;->c:Lcom/google/android/apps/youtube/app/remote/am;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/remote/am;->L()Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v7

    move v3, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ak;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ak;->b:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    int-to-long v3, v3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Ljava/lang/String;JLjava/lang/String;ILcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V

    return-void

    :cond_0
    move-object v7, v5

    move v3, v6

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/remote/r;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ak;->e:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ak;->e:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ak;->e:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Intent;Landroid/support/v7/media/x;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/remote/r;->a(Landroid/content/Intent;Landroid/support/v7/media/x;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 8

    const/4 v2, 0x0

    const/4 v5, -0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ak;->e:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ak;->e:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/remote/ak;->e:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ak;->c:Lcom/google/android/apps/youtube/app/remote/am;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ak;->c:Lcom/google/android/apps/youtube/app/remote/am;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/am;->J()Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/ae;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/al;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/remote/al;-><init>(Lcom/google/android/apps/youtube/app/remote/ak;)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ak;->e:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ak;->d:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    sget-object v2, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a:[B

    const-string v3, ""

    const-string v4, ""

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/remote/ak;->e:Lcom/google/android/apps/youtube/common/a/d;

    move v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/common/a/b;)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/remote/ak;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V

    goto :goto_0
.end method
