.class public final Lcom/google/android/apps/youtube/app/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

.field private final b:Lcom/google/android/apps/youtube/core/async/af;

.field private final c:Lcom/google/android/apps/youtube/common/a/b;

.field private final d:Lcom/google/android/apps/youtube/common/a/b;

.field private e:Ljava/util/List;

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/async/af;Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/a/c;->a:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/a/c;->b:Lcom/google/android/apps/youtube/core/async/af;

    invoke-static {p3, p0}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/a/c;->c:Lcom/google/android/apps/youtube/common/a/b;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/a/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/a/c;->d:Lcom/google/android/apps/youtube/common/a/b;

    return-void
.end method

.method private a(ILcom/google/android/apps/youtube/core/async/GDataRequest;)V
    .locals 5

    const/4 v0, 0x1

    if-gt p1, v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v1, p2, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    add-int/lit8 v2, p1, -0x1

    iput v2, p0, Lcom/google/android/apps/youtube/app/a/c;->h:I

    :goto_0
    if-ge v0, p1, :cond_0

    iget v2, p0, Lcom/google/android/apps/youtube/app/a/c;->g:I

    mul-int/2addr v2, v0

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "start-index"

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/a/c;->a:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->e(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/a/c;->b:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/a/c;->c:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v3, v2, v4}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 4

    const/4 v3, 0x1

    iget v0, p0, Lcom/google/android/apps/youtube/app/a/c;->h:I

    if-nez v0, :cond_0

    iput v3, p0, Lcom/google/android/apps/youtube/app/a/c;->h:I

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "inline"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "start-index"

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/a/c;->b:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/a/c;->c:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/youtube/core/async/af;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error for request "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/a/c;->d:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    iget v0, p0, Lcom/google/android/apps/youtube/app/a/c;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/a/c;->h:I

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    const/4 v3, 0x1

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    iget v0, p0, Lcom/google/android/apps/youtube/app/a/c;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/a/c;->h:I

    iget v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->startIndex:I

    if-ne v0, v3, :cond_1

    iget v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->elementsPerPage:I

    iput v0, p0, Lcom/google/android/apps/youtube/app/a/c;->g:I

    iget v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->totalResults:I

    iput v0, p0, Lcom/google/android/apps/youtube/app/a/c;->f:I

    iget v0, p0, Lcom/google/android/apps/youtube/app/a/c;->f:I

    int-to-double v0, v0

    iget v2, p0, Lcom/google/android/apps/youtube/app/a/c;->g:I

    int-to-double v4, v2

    div-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/a/c;->e:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/a/c;->e:Ljava/util/List;

    iget-object v2, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/youtube/app/a/c;->a(ILcom/google/android/apps/youtube/core/async/GDataRequest;)V

    :goto_0
    iget v0, p0, Lcom/google/android/apps/youtube/app/a/c;->h:I

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    iget v1, p0, Lcom/google/android/apps/youtube/app/a/c;->f:I

    iget v2, p0, Lcom/google/android/apps/youtube/app/a/c;->g:I

    iget-object v4, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->previousUri:Landroid/net/Uri;

    iget-object v5, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->nextUri:Landroid/net/Uri;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/a/c;->e:Ljava/util/List;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;-><init>(IIILandroid/net/Uri;Landroid/net/Uri;Ljava/util/List;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/a/c;->d:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v1, p1, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/a/c;->e:Ljava/util/List;

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
