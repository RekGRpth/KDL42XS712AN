.class public final Lcom/google/android/apps/youtube/app/ui/fq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/view/ViewGroup;

.field protected final b:Landroid/widget/TextView;

.field final synthetic c:Lcom/google/android/apps/youtube/app/ui/fm;

.field private final d:I

.field private final e:[Landroid/view/View;

.field private final f:[Landroid/view/View;

.field private final g:Lcom/google/android/apps/youtube/app/adapter/ai;

.field private h:Ljava/lang/Object;

.field private final i:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/fm;Landroid/view/View;Lcom/google/android/apps/youtube/app/adapter/ai;I)V
    .locals 6

    const/4 v1, 0x0

    const/4 v5, 0x3

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/fq;->c:Lcom/google/android/apps/youtube/app/ui/fm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    check-cast p2, Landroid/view/ViewGroup;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fq;->a:Landroid/view/ViewGroup;

    new-array v0, v5, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fq;->e:[Landroid/view/View;

    new-array v0, v5, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fq;->f:[Landroid/view/View;

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/fq;->a:Landroid/view/ViewGroup;

    invoke-static {}, Lcom/google/android/apps/youtube/app/ui/fm;->a()[I

    move-result-object v3

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/fq;->e:[Landroid/view/View;

    aput-object v2, v3, v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/fq;->f:[Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/fq;->a:Landroid/view/ViewGroup;

    invoke-static {}, Lcom/google/android/apps/youtube/app/ui/fm;->b()[I

    move-result-object v4

    aget v4, v4, v0

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/fq;->d:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fq;->a:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/youtube/j;->cH:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fq;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fq;->b:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/fr;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/ui/fr;-><init>(Lcom/google/android/apps/youtube/app/ui/fq;Lcom/google/android/apps/youtube/app/ui/fm;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/fq;->g:Lcom/google/android/apps/youtube/app/adapter/ai;

    iput p4, p0, Lcom/google/android/apps/youtube/app/ui/fq;->i:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/fq;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fq;->h:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/fq;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/fq;->h:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/fq;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/fq;->d:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/fq;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/fq;->i:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/fq;)[Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fq;->e:[Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/ui/fq;)[Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fq;->f:[Landroid/view/View;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/ui/fq;)Lcom/google/android/apps/youtube/app/adapter/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fq;->g:Lcom/google/android/apps/youtube/app/adapter/ai;

    return-object v0
.end method
