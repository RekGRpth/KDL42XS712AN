.class final Lcom/google/android/apps/youtube/app/offline/q;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Map;

.field private static final b:Landroid/util/SparseArray;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/app/offline/q;->a:Ljava/util/Map;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/app/offline/q;->b:Landroid/util/SparseArray;

    invoke-static {}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->values()[Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    sget-object v4, Lcom/google/android/apps/youtube/app/offline/q;->a:Ljava/util/Map;

    # getter for: Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->formatType:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;
    invoke-static {v3}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->access$000(Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/youtube/app/offline/q;->b:Landroid/util/SparseArray;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getQualityValue()I

    move-result v5

    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/offline/q;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b()Landroid/util/SparseArray;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/offline/q;->b:Landroid/util/SparseArray;

    return-object v0
.end method
