.class public final Lcom/google/android/apps/youtube/app/ui/presenter/aw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/j;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/youtube/core/client/bj;

.field private final c:Lcom/google/android/apps/youtube/common/c/a;

.field private final d:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

.field private final e:Lcom/google/android/apps/youtube/common/network/h;

.field private final f:Lcom/google/android/apps/youtube/app/am;

.field private final g:Lcom/google/android/apps/youtube/app/offline/p;

.field private final h:Lcom/google/android/apps/youtube/app/ui/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/app/ui/v;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;->b:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;->d:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;->e:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/am;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;->f:Lcom/google/android/apps/youtube/app/am;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/p;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;->g:Lcom/google/android/apps/youtube/app/offline/p;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/v;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;->h:Lcom/google/android/apps/youtube/app/ui/v;

    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/android/apps/youtube/uilib/a/g;
    .locals 9

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/at;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;->a:Landroid/content/Context;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/presenter/p;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/google/android/apps/youtube/app/ui/presenter/p;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;->b:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;->d:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;->e:Lcom/google/android/apps/youtube/common/network/h;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;->f:Lcom/google/android/apps/youtube/app/am;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;->g:Lcom/google/android/apps/youtube/app/offline/p;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;->h:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/app/ui/presenter/at;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/uilib/a/i;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/app/ui/v;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/aw;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    return-object v0
.end method
