.class public final Lcom/google/android/apps/youtube/app/adapter/aj;
.super Lcom/google/android/apps/youtube/app/adapter/g;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/youtube/app/adapter/ai;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/adapter/ai;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/adapter/g;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/aj;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/ai;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/aj;->b:Lcom/google/android/apps/youtube/app/adapter/ai;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/aj;)Lcom/google/android/apps/youtube/app/adapter/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/aj;->b:Lcom/google/android/apps/youtube/app/adapter/ai;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/adapter/aj;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/aj;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/youtube/app/adapter/ae;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/ak;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/apps/youtube/app/adapter/ak;-><init>(Lcom/google/android/apps/youtube/app/adapter/aj;Landroid/view/View;Landroid/view/ViewGroup;B)V

    return-object v0
.end method

.method public final a(Ljava/lang/Iterable;)V
    .locals 3

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent;->video:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/aj;->b:Lcom/google/android/apps/youtube/app/adapter/ai;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/ai;->a(Ljava/lang/Iterable;)V

    return-void
.end method
