.class public final Lcom/google/android/apps/youtube/app/ui/id;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/core/aw;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/aw;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/id;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/id;->b:Lcom/google/android/apps/youtube/core/aw;

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 4

    const-string v0, "GData"

    const-string v1, "InvalidEntryException"

    const/4 v2, 0x0

    const-string v3, "Video already in playlist"

    invoke-static {p2, v0, v1, v2, v3}, Lcom/google/android/apps/youtube/core/aw;->a(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/id;->a:Landroid/app/Activity;

    sget v1, Lcom/google/android/youtube/p;->hk:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Error adding to watch later"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/id;->b:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/id;->a:Landroid/app/Activity;

    sget v1, Lcom/google/android/youtube/p;->hj:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    return-void
.end method
