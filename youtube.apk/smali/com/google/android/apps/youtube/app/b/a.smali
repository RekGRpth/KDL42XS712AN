.class public final Lcom/google/android/apps/youtube/app/b/a;
.super Lcom/google/android/apps/youtube/core/client/a/a;
.source "SourceFile"


# direct methods
.method public constructor <init>(IZLjava/lang/String;Lcom/google/a/a/a/a/dt;)V
    .locals 6

    const-string v1, "home"

    move-object v0, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/client/a/a;-><init>(Ljava/lang/String;IZLjava/lang/String;Lcom/google/a/a/a/a/dt;)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/youtube/core/client/a/g;)Z
    .locals 1

    instance-of v0, p1, Lcom/google/android/apps/youtube/app/b/u;

    if-eqz v0, :cond_1

    const-string v0, "home_sign_in"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/b/a;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/core/client/a/a;->a(Lcom/google/android/apps/youtube/core/client/a/g;)Z

    move-result v0

    return v0

    :cond_1
    instance-of v0, p1, Lcom/google/android/apps/youtube/app/b/s;

    if-eqz v0, :cond_2

    const-string v0, "home_signed_in"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/b/a;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lcom/google/android/apps/youtube/app/b/t;

    if-eqz v0, :cond_0

    const-string v0, "home_signed_out"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/b/a;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
