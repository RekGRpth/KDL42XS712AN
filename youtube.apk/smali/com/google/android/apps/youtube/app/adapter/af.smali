.class public Lcom/google/android/apps/youtube/app/adapter/af;
.super Lcom/google/android/apps/youtube/core/a/a;
.source "SourceFile"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:I

.field private final b:Lcom/google/android/apps/youtube/app/adapter/ai;

.field private final d:Landroid/content/Context;

.field private final e:Landroid/view/LayoutInflater;

.field private final f:Ljava/util/List;

.field private final g:Ljava/util/List;

.field private final h:Landroid/util/SparseArray;

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/google/android/apps/youtube/app/adapter/ai;)V
    .locals 2

    const/4 v1, 0x2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/a/a;-><init>()V

    iput p2, p0, Lcom/google/android/apps/youtube/app/adapter/af;->a:I

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/ai;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->b:Lcom/google/android/apps/youtube/app/adapter/ai;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->d:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->e:Landroid/view/LayoutInflater;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->f:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->g:Ljava/util/List;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->h:Landroid/util/SparseArray;

    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/Iterable;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/core/a/a;->a(Ljava/lang/Iterable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->b:Lcom/google/android/apps/youtube/app/adapter/ai;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/ai;->a(Ljava/lang/Iterable;)V

    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->i:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCount()I
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/a/a;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/af;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/af;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/af;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/af;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p1, v1

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/a/a;->getCount()I

    move-result v2

    sub-int/2addr v1, v2

    if-gez v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p1, v0

    invoke-super {p0, v0}, Lcom/google/android/apps/youtube/core/a/a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 3

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/af;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/af;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p1, v1

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/a/a;->getCount()I

    move-result v2

    sub-int/2addr v1, v2

    if-gez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/ax;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/adapter/ax;->a()Landroid/view/View;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p1, v0

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/a/a;->getCount()I

    move-result v1

    sub-int/2addr v0, v1

    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/af;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/ax;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/adapter/ax;->a()Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/af;->h:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v1

    if-ltz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/af;->h:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, v2, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_3

    const/4 p2, 0x0

    move-object v1, p2

    :goto_1
    if-nez v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->e:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/google/android/apps/youtube/app/adapter/af;->a:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->b:Lcom/google/android/apps/youtube/app/adapter/ai;

    invoke-interface {v0, v1, p3}, Lcom/google/android/apps/youtube/app/adapter/ai;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/youtube/app/adapter/ae;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/af;->h:Landroid/util/SparseArray;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v4

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :goto_2
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/af;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, p1, v2}, Lcom/google/android/apps/youtube/app/adapter/ae;->a(ILjava/lang/Object;)Landroid/view/View;

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/ae;

    goto :goto_2

    :cond_3
    move-object v1, p2

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/ax;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/adapter/ax;->b()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/af;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v0, p1, v0

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/a/a;->getCount()I

    move-result v1

    sub-int/2addr v0, v1

    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/af;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/ax;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/adapter/ax;->b()Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
