.class public final Lcom/google/android/apps/youtube/app/d/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/widget/ListView;

.field private final c:Lcom/google/android/apps/youtube/datalib/distiller/a;

.field private final d:Lcom/google/android/apps/youtube/core/client/bc;

.field private final e:Lcom/google/android/apps/youtube/core/identity/ak;

.field private final f:Lcom/google/android/apps/youtube/core/identity/l;

.field private final g:Lcom/google/android/apps/youtube/core/identity/aa;

.field private final h:Lcom/google/android/apps/youtube/app/am;

.field private final i:Lcom/google/android/apps/youtube/core/aw;

.field private final j:Ljava/util/List;

.field private final k:Lcom/google/android/apps/youtube/uilib/a/h;

.field private final l:Lcom/google/android/apps/youtube/app/ui/presenter/v;

.field private final m:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;

.field private final n:Lcom/google/android/apps/youtube/app/d/p;

.field private o:Lcom/google/android/apps/youtube/datalib/distiller/model/a;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/widget/ListView;Lcom/google/android/apps/youtube/datalib/distiller/a;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/aw;)V
    .locals 14

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->a:Landroid/app/Activity;

    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->b:Landroid/widget/ListView;

    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/datalib/distiller/a;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->c:Lcom/google/android/apps/youtube/datalib/distiller/a;

    invoke-static/range {p5 .. p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->d:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static/range {p6 .. p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/identity/ak;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->e:Lcom/google/android/apps/youtube/core/identity/ak;

    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->f:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static/range {p8 .. p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/identity/aa;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->g:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-static/range {p9 .. p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/am;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->h:Lcom/google/android/apps/youtube/app/am;

    invoke-static/range {p10 .. p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/aw;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->i:Lcom/google/android/apps/youtube/core/aw;

    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p4 .. p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/v;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/app/ui/presenter/v;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->l:Lcom/google/android/apps/youtube/app/ui/presenter/v;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->m:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;

    new-instance v1, Lcom/google/android/apps/youtube/app/d/p;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/app/d/p;-><init>(Lcom/google/android/apps/youtube/app/d/f;B)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->n:Lcom/google/android/apps/youtube/app/d/p;

    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0x19

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->j:Ljava/util/List;

    new-instance v4, Lcom/google/android/apps/youtube/app/d/k;

    const/4 v1, 0x0

    invoke-direct {v4, p0, v1}, Lcom/google/android/apps/youtube/app/d/k;-><init>(Lcom/google/android/apps/youtube/app/d/f;B)V

    new-instance v6, Lcom/google/android/apps/youtube/app/d/n;

    const/4 v1, 0x0

    invoke-direct {v6, p0, v1}, Lcom/google/android/apps/youtube/app/d/n;-><init>(Lcom/google/android/apps/youtube/app/d/f;B)V

    new-instance v7, Lcom/google/android/apps/youtube/app/d/s;

    const/4 v1, 0x0

    invoke-direct {v7, p0, v1}, Lcom/google/android/apps/youtube/app/d/s;-><init>(Lcom/google/android/apps/youtube/app/d/f;B)V

    new-instance v1, Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/uilib/a/h;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->k:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/d/f;->k:Lcom/google/android/apps/youtube/uilib/a/h;

    const-class v9, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/ct;

    new-instance v5, Lcom/google/android/apps/youtube/app/d/ac;

    const/4 v2, 0x0

    invoke-direct {v5, p0, v2}, Lcom/google/android/apps/youtube/app/d/ac;-><init>(Lcom/google/android/apps/youtube/app/d/f;B)V

    move-object v2, p1

    move-object/from16 v3, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/youtube/app/ui/presenter/ct;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/presenter/f;Lcom/google/android/apps/youtube/app/ui/presenter/cu;Lcom/google/android/apps/youtube/app/ui/presenter/g;Lcom/google/android/apps/youtube/app/d/r;)V

    invoke-virtual {v8, v9, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->k:Lcom/google/android/apps/youtube/uilib/a/h;

    const-class v2, Lcom/google/android/apps/youtube/datalib/distiller/model/b;

    new-instance v8, Lcom/google/android/apps/youtube/app/ui/presenter/x;

    move-object v9, p1

    move-object/from16 v10, p4

    move-object v11, v4

    move-object v12, v6

    move-object v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/google/android/apps/youtube/app/ui/presenter/x;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/presenter/f;Lcom/google/android/apps/youtube/app/ui/presenter/g;Lcom/google/android/apps/youtube/app/d/r;)V

    invoke-virtual {v1, v2, v8}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->k:Lcom/google/android/apps/youtube/uilib/a/h;

    const-class v2, Lcom/google/android/apps/youtube/app/ui/presenter/v;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/presenter/u;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/d/f;->n:Lcom/google/android/apps/youtube/app/d/p;

    move-object/from16 v0, p4

    invoke-direct {v3, p1, v0, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/u;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/d/o;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->k:Lcom/google/android/apps/youtube/uilib/a/h;

    const-class v2, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/presenter/r;

    new-instance v4, Lcom/google/android/apps/youtube/app/d/l;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/google/android/apps/youtube/app/d/l;-><init>(Lcom/google/android/apps/youtube/app/d/f;B)V

    invoke-direct {v3, p1, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/r;-><init>(Landroid/app/Activity;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->k:Lcom/google/android/apps/youtube/uilib/a/h;

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/app/ui/presenter/v;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->l:Lcom/google/android/apps/youtube/app/ui/presenter/v;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/datalib/distiller/model/c;)Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;
    .locals 1

    instance-of v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/b;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/google/android/apps/youtube/datalib/distiller/model/b;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/distiller/model/b;->a()Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_0
    check-cast p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/d/f;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->a:Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/d/f;Ljava/lang/Runnable;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->l:Lcom/google/android/apps/youtube/app/ui/presenter/v;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/presenter/v;->b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->g:Lcom/google/android/apps/youtube/core/identity/aa;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/apps/youtube/app/d/h;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/youtube/app/d/h;-><init>(Lcom/google/android/apps/youtube/app/d/f;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ah;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/d/f;->a(Lcom/google/android/apps/youtube/core/identity/UserProfile;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/d/f;Lcom/google/android/apps/youtube/core/identity/UserProfile;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/d/f;->a(Lcom/google/android/apps/youtube/core/identity/UserProfile;)Z

    move-result v0

    return v0
.end method

.method private a(Lcom/google/android/apps/youtube/core/identity/UserProfile;)Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->e:Lcom/google/android/apps/youtube/core/identity/ak;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/d/f;->f:Lcom/google/android/apps/youtube/core/identity/l;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/d/f;->d:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {v0, v1, v2, p1, v3}, Lcom/google/android/apps/youtube/app/ui/bm;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/UserProfile;Lcom/google/android/apps/youtube/core/client/bc;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/uilib/a/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->k:Lcom/google/android/apps/youtube/uilib/a/h;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/d/f;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->a:Landroid/app/Activity;

    return-object v0
.end method

.method private c()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->o:Lcom/google/android/apps/youtube/datalib/distiller/model/a;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->o:Lcom/google/android/apps/youtube/datalib/distiller/model/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/distiller/model/a;->a()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->m:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;->NO_COMMENTS:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;->a(Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->j:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->l:Lcom/google/android/apps/youtube/app/ui/presenter/v;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->o:Lcom/google/android/apps/youtube/datalib/distiller/model/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/distiller/model/a;->a()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->o:Lcom/google/android/apps/youtube/datalib/distiller/model/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/distiller/model/a;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/d/f;->j:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->l()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/distiller/model/b;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/d/f;->j:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->o:Lcom/google/android/apps/youtube/datalib/distiller/model/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/distiller/model/a;->c()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->m:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;->NO_MORE_COMMENTS:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;->a(Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->m:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;->CAN_LOAD_MORE:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;->a(Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->j:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->m:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->k:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->k:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->j:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/util/Collection;)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->i:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->d:Lcom/google/android/apps/youtube/core/client/bc;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/app/am;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->h:Lcom/google/android/apps/youtube/app/am;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/datalib/distiller/model/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->o:Lcom/google/android/apps/youtube/datalib/distiller/model/a;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->m:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/datalib/distiller/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->c:Lcom/google/android/apps/youtube/datalib/distiller/a;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/app/d/f;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/d/f;->c()V

    return-void
.end method

.method static synthetic k(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/core/identity/ak;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->e:Lcom/google/android/apps/youtube/core/identity/ak;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/app/d/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->n:Lcom/google/android/apps/youtube/app/d/p;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->f:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->l:Lcom/google/android/apps/youtube/app/ui/presenter/v;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/ui/presenter/v;->b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->k:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->notifyDataSetChanged()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->d:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/apps/youtube/app/d/g;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/d/g;-><init>(Lcom/google/android/apps/youtube/app/d/f;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/client/bc;->a(Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/distiller/model/a;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/d/f;->o:Lcom/google/android/apps/youtube/datalib/distiller/model/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/f;->l:Lcom/google/android/apps/youtube/app/ui/presenter/v;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, v1, Lcom/google/android/apps/youtube/app/ui/presenter/v;->a:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/d/f;->c()V

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/distiller/model/a;->f()Z

    move-result v0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/apps/youtube/uilib/a/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->k:Lcom/google/android/apps/youtube/uilib/a/h;

    return-object v0
.end method

.method public final handleSignInEvent(Lcom/google/android/apps/youtube/core/identity/ai;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/d/f;->a()V

    return-void
.end method

.method public final handleSignOutEvent(Lcom/google/android/apps/youtube/core/identity/aj;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->l:Lcom/google/android/apps/youtube/app/ui/presenter/v;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/ui/presenter/v;->b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/f;->k:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->notifyDataSetChanged()V

    return-void
.end method
