.class public final Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;
.super Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private authenticated:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/navigation/b;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/b;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->authenticated:Z

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;-><init>(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->authenticated:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final isAuthenticated()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->authenticated:Z

    return v0
.end method

.method public final pop()Lcom/google/android/apps/youtube/app/fragments/navigation/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->stack:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/l;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->authenticated:Z

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;->pop()Lcom/google/android/apps/youtube/app/fragments/navigation/a;

    move-result-object v0

    return-object v0
.end method

.method public final popAll()Lcom/google/android/apps/youtube/app/fragments/navigation/a;
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->authenticated:Z

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;->popAll()Lcom/google/android/apps/youtube/app/fragments/navigation/a;

    move-result-object v0

    return-object v0
.end method

.method public final popCheckpoint()Lcom/google/android/apps/youtube/app/fragments/navigation/a;
    .locals 2

    const/4 v1, 0x0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->stack:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/l;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->stack:Lcom/google/android/apps/youtube/common/e/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/l;->poll()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/youtube/app/fragments/navigation/a;

    iget-object v0, v1, Lcom/google/android/apps/youtube/app/fragments/navigation/a;->a:Landroid/os/Parcelable;

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->isCheckPoint()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    return-object v1
.end method

.method public final push(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;Landroid/support/v4/app/Fragment$SavedState;)V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/navigation/a;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/youtube/app/fragments/navigation/a;-><init>(Landroid/os/Parcelable;Landroid/os/Parcelable;)V

    invoke-super {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;->push(Lcom/google/android/apps/youtube/app/fragments/navigation/a;)V

    return-void
.end method

.method protected final readEntryFromParcel(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/app/fragments/navigation/a;
    .locals 3

    const-class v0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    const-class v1, Landroid/support/v4/app/Fragment$SavedState;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment$SavedState;

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/navigation/a;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/a;-><init>(Landroid/os/Parcelable;Landroid/os/Parcelable;)V

    return-object v2
.end method

.method public final setAuthenticated()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->authenticated:Z

    return-void
.end method

.method protected final writeEntryToParcel(Lcom/google/android/apps/youtube/app/fragments/navigation/a;Landroid/os/Parcel;I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/fragments/navigation/a;->a:Landroid/os/Parcelable;

    invoke-virtual {p2, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/fragments/navigation/a;->b:Landroid/os/Parcelable;

    invoke-virtual {p2, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/fragments/navigation/BackStack;->writeToParcel(Landroid/os/Parcel;I)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneBackStack;->authenticated:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
