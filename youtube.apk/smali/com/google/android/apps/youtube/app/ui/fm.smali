.class public abstract Lcom/google/android/apps/youtube/app/ui/fm;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final f:[I

.field private static final g:[I


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Lcom/google/android/apps/youtube/app/am;

.field protected final c:Lcom/google/android/apps/youtube/core/client/bj;

.field protected final d:Lcom/google/android/apps/youtube/app/ui/v;

.field protected final e:Ljava/util/LinkedList;

.field private final h:Lcom/google/android/apps/youtube/app/ui/fp;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v5, [I

    sget v1, Lcom/google/android/youtube/j;->fg:I

    aput v1, v0, v2

    sget v1, Lcom/google/android/youtube/j;->fh:I

    aput v1, v0, v3

    sget v1, Lcom/google/android/youtube/j;->fi:I

    aput v1, v0, v4

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/fm;->f:[I

    new-array v0, v5, [I

    sget v1, Lcom/google/android/youtube/j;->ev:I

    aput v1, v0, v2

    sget v1, Lcom/google/android/youtube/j;->ew:I

    aput v1, v0, v3

    sget v1, Lcom/google/android/youtube/j;->ex:I

    aput v1, v0, v4

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/fm;->g:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/fp;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fm;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/am;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fm;->b:Lcom/google/android/apps/youtube/app/am;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fm;->c:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/v;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fm;->d:Lcom/google/android/apps/youtube/app/ui/v;

    iput-object p5, p0, Lcom/google/android/apps/youtube/app/ui/fm;->h:Lcom/google/android/apps/youtube/app/ui/fp;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fm;->e:Ljava/util/LinkedList;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/fm;)Lcom/google/android/apps/youtube/app/ui/fp;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fm;->h:Lcom/google/android/apps/youtube/app/ui/fp;

    return-object v0
.end method

.method static synthetic a()[I
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/fm;->f:[I

    return-object v0
.end method

.method static synthetic b()[I
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/fm;->g:[I

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fm;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/fq;

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/app/ui/fq;->a(Lcom/google/android/apps/youtube/app/ui/fq;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/youtube/app/ui/fm;->a(Lcom/google/android/apps/youtube/app/ui/fq;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/app/ui/fn;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/fn;-><init>(Lcom/google/android/apps/youtube/app/ui/fm;)V

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Lcom/google/android/apps/youtube/app/ui/fm;->a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fm;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fm;->c:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/fm;->d:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/app/adapter/ag;->c(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/ai;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/apps/youtube/app/ui/fm;->a(Landroid/view/View;Lcom/google/android/apps/youtube/app/adapter/ai;I)V

    return-void
.end method

.method public final a(Landroid/view/View;Lcom/google/android/apps/youtube/app/adapter/ai;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fm;->e:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/fq;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/fm;->a:Landroid/content/Context;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/ui/fq;-><init>(Lcom/google/android/apps/youtube/app/ui/fm;Landroid/view/View;Lcom/google/android/apps/youtube/app/adapter/ai;I)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected final a(Lcom/google/android/apps/youtube/app/ui/fq;Lcom/google/android/apps/youtube/datalib/legacy/model/Page;)V
    .locals 7

    const/4 v2, 0x0

    iget v3, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->totalResults:I

    if-lez v3, :cond_2

    move v1, v2

    :goto_0
    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/fq;->b(Lcom/google/android/apps/youtube/app/ui/fq;)I

    move-result v0

    if-ge v1, v0, :cond_2

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/fq;->c(Lcom/google/android/apps/youtube/app/ui/fq;)I

    move-result v0

    if-le v3, v1, :cond_0

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/fq;->f(Lcom/google/android/apps/youtube/app/ui/fq;)Lcom/google/android/apps/youtube/app/adapter/ai;

    move-result-object v4

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/fq;->d(Lcom/google/android/apps/youtube/app/ui/fq;)[Landroid/view/View;

    move-result-object v5

    aget-object v5, v5, v1

    iget-object v6, p1, Lcom/google/android/apps/youtube/app/ui/fq;->a:Landroid/view/ViewGroup;

    invoke-interface {v4, v5, v6}, Lcom/google/android/apps/youtube/app/adapter/ai;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/youtube/app/adapter/ae;

    move-result-object v4

    invoke-interface {v4, v1, v0}, Lcom/google/android/apps/youtube/app/adapter/ae;->a(ILjava/lang/Object;)Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/fq;->d(Lcom/google/android/apps/youtube/app/ui/fq;)[Landroid/view/View;

    move-result-object v4

    aget-object v4, v4, v1

    new-instance v5, Lcom/google/android/apps/youtube/app/ui/fo;

    invoke-direct {v5, p0, v0}, Lcom/google/android/apps/youtube/app/ui/fo;-><init>(Lcom/google/android/apps/youtube/app/ui/fm;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move v0, v2

    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/fq;->d(Lcom/google/android/apps/youtube/app/ui/fq;)[Landroid/view/View;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/fq;->e(Lcom/google/android/apps/youtube/app/ui/fq;)[Landroid/view/View;

    move-result-object v4

    aget-object v4, v4, v1

    if-eqz v4, :cond_1

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/fq;->e(Lcom/google/android/apps/youtube/app/ui/fq;)[Landroid/view/View;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method protected a(Lcom/google/android/apps/youtube/app/ui/fq;Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method protected abstract a(Ljava/lang/Object;)V
.end method

.method protected abstract a(Ljava/lang/Object;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fm;->b:Lcom/google/android/apps/youtube/app/am;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->RELATED:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/apps/youtube/app/am;->a(Ljava/lang/String;ZLcom/google/android/apps/youtube/core/client/WatchFeature;)V

    return-void
.end method
