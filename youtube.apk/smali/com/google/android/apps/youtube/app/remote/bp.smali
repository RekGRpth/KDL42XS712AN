.class final Lcom/google/android/apps/youtube/app/remote/bp;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:Z

.field private final f:Ljava/lang/String;

.field private final g:I


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Ljava/lang/String;JLjava/lang/String;IZLjava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p8, p0, Lcom/google/android/apps/youtube/app/remote/bp;->f:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bp;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/remote/bp;->b:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/youtube/app/remote/bp;->c:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/apps/youtube/app/remote/bp;->d:J

    iput-boolean p7, p0, Lcom/google/android/apps/youtube/app/remote/bp;->e:Z

    iput p6, p0, Lcom/google/android/apps/youtube/app/remote/bp;->g:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Ljava/lang/String;JLjava/lang/String;IZLjava/lang/String;B)V
    .locals 9

    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-object v5, p5

    move v6, p6

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/app/remote/bp;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Ljava/lang/String;JLjava/lang/String;IZLjava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/bp;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bp;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/remote/bp;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bp;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/remote/bp;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/bp;->e:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/remote/bp;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bp;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/remote/bp;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/app/remote/bp;->d:J

    return-wide v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/remote/bp;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/remote/bp;->g:I

    return v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/remote/bp;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bp;->c:Ljava/lang/String;

    return-object v0
.end method
