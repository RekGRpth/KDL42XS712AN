.class final Lcom/google/android/apps/youtube/app/notification/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/notification/GcmBroadcastReceiver;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/a/a/a/a/kz;

.field private final f:Landroid/content/res/Resources;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/notification/GcmBroadcastReceiver;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/a/a/a/a/kz;Landroid/content/res/Resources;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/notification/a;->a:Lcom/google/android/apps/youtube/app/notification/GcmBroadcastReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/notification/a;->b:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/notification/a;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/notification/a;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/youtube/app/notification/a;->e:Lcom/google/a/a/a/a/kz;

    iput-object p6, p0, Lcom/google/android/apps/youtube/app/notification/a;->f:Landroid/content/res/Resources;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/notification/GcmBroadcastReceiver;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/a/a/a/a/kz;Landroid/content/res/Resources;B)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/youtube/app/notification/a;-><init>(Lcom/google/android/apps/youtube/app/notification/GcmBroadcastReceiver;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/a/a/a/a/kz;Landroid/content/res/Resources;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 5

    const-string v0, "Error retrieving thumbnail image for notification: "

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/notification/a;->f:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/youtube/n;->a:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/notification/a;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/notification/a;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/notification/a;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/notification/a;->e:Lcom/google/a/a/a/a/kz;

    invoke-static {v1, v2, v3, v0, v4}, Lcom/google/android/apps/youtube/app/notification/d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Lcom/google/a/a/a/a/kz;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/notification/a;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/notification/a;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/notification/a;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/notification/a;->e:Lcom/google/a/a/a/a/kz;

    invoke-static {v0, v1, v2, p2, v3}, Lcom/google/android/apps/youtube/app/notification/d;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Lcom/google/a/a/a/a/kz;)V

    return-void
.end method
