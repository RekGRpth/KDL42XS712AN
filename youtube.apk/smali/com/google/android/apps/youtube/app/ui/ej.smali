.class final Lcom/google/android/apps/youtube/app/ui/ej;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic b:Lcom/google/android/apps/youtube/app/ui/ei;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/ui/ei;Landroid/os/Looper;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ej;->b:Lcom/google/android/apps/youtube/app/ui/ei;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/ej;->a:Landroid/app/Activity;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 5

    const/4 v2, 0x1

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ej;->b:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->a(Lcom/google/android/apps/youtube/app/ui/ei;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ej;->b:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->b(Lcom/google/android/apps/youtube/app/ui/ei;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ej;->b:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->c(Lcom/google/android/apps/youtube/app/ui/ei;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {p0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ej;->b:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->d(Lcom/google/android/apps/youtube/app/ui/ei;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ej;->b:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->e(Lcom/google/android/apps/youtube/app/ui/ei;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ej;->b:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->n()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ej;->b:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->n()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->UNSTARTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ej;->b:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->n()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->BUFFERING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v0, v1, :cond_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ej;->a:Landroid/app/Activity;

    sget v1, Lcom/google/android/youtube/p;->bu:I

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/ej;->b:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/ui/ei;->f(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->o()Lcom/google/android/apps/youtube/app/remote/bg;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/youtube/app/remote/bg;->getScreenName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ej;->b:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/ei;->g(Lcom/google/android/apps/youtube/app/ui/ei;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/youtube/p;->K:I

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/ej;->b:Lcom/google/android/apps/youtube/app/ui/ei;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/ui/ei;->h(Lcom/google/android/apps/youtube/app/ui/ei;)Lcom/google/android/apps/youtube/app/ui/RemoteControlView;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlView;->a(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_2
    sget v0, Lcom/google/android/youtube/p;->cx:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
