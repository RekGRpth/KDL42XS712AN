.class final Lcom/google/android/apps/youtube/app/adapter/bc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/adapter/ae;


# instance fields
.field public final a:Landroid/widget/ProgressBar;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field final synthetic d:Lcom/google/android/apps/youtube/app/adapter/bb;

.field private final e:Lcom/google/android/apps/youtube/app/adapter/ae;

.field private final f:Lcom/google/android/apps/youtube/app/adapter/ae;

.field private final g:Landroid/view/View;

.field private final h:Landroid/widget/TextView;

.field private final i:Landroid/widget/TextView;


# direct methods
.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/adapter/bb;Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/apps/youtube/app/adapter/ae;Lcom/google/android/apps/youtube/app/adapter/ae;)V
    .locals 0

    invoke-direct {p0, p1, p2, p4, p5}, Lcom/google/android/apps/youtube/app/adapter/bc;-><init>(Lcom/google/android/apps/youtube/app/adapter/bb;Landroid/view/View;Lcom/google/android/apps/youtube/app/adapter/ae;Lcom/google/android/apps/youtube/app/adapter/ae;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/youtube/app/adapter/bb;Landroid/view/View;Lcom/google/android/apps/youtube/app/adapter/ae;Lcom/google/android/apps/youtube/app/adapter/ae;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->d:Lcom/google/android/apps/youtube/app/adapter/bb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->e:Lcom/google/android/apps/youtube/app/adapter/ae;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->f:Lcom/google/android/apps/youtube/app/adapter/ae;

    sget v0, Lcom/google/android/youtube/j;->fZ:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->g:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->g:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->dU:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->a:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->g:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->eK:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->g:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->dT:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->g:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aP:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->g:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aK:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->i:Landroid/widget/TextView;

    return-void
.end method

.method private a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->a:Landroid/widget/ProgressBar;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/bc;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->c:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/bc;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->b:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/bc;->a(Landroid/view/View;I)V

    return-void
.end method

.method private static a(Landroid/view/View;I)V
    .locals 0

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->h:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/bc;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->i:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/bc;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->g:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->D:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/bc;->a(Landroid/view/View;I)V

    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    check-cast p2, Lcom/google/android/apps/youtube/app/ui/ha;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/app/ui/ha;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/adapter/bc;->a(I)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/adapter/bc;->b(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->e:Lcom/google/android/apps/youtube/app/adapter/ae;

    iget-object v1, p2, Lcom/google/android/apps/youtube/app/ui/ha;->a:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/app/adapter/ae;->a(ILjava/lang/Object;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/adapter/bc;->a(I)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/adapter/bc;->b(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bc;->f:Lcom/google/android/apps/youtube/app/adapter/ae;

    iget-object v1, p2, Lcom/google/android/apps/youtube/app/ui/ha;->b:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/app/adapter/ae;->a(ILjava/lang/Object;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
