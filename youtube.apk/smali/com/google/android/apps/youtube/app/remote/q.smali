.class public abstract Lcom/google/android/apps/youtube/app/remote/q;
.super Landroid/support/v7/media/f;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/remote/bi;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

.field private final b:Ljava/util/Map;

.field private final c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Z)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/v7/media/f;-><init>(Landroid/content/Context;)V

    const-string v0, "youTubeTvScreenMonitor can not be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/q;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    iput-boolean p3, p0, Lcom/google/android/apps/youtube/app/remote/q;->c:Z

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/q;->b:Ljava/util/Map;

    invoke-virtual {p2, p0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/bi;)V

    return-void
.end method

.method protected static c(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/q;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "YouTube MDX"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)Landroid/support/v7/media/c;
.end method

.method public synthetic a(Lcom/google/android/apps/youtube/app/remote/bg;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/remote/q;->b(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    return-void
.end method

.method protected final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/q;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    return-object v0
.end method

.method public final b(Landroid/support/v7/media/e;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "discoveryRequestChanged : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/q;->c(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/q;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/support/v7/media/e;->a()Landroid/support/v7/media/s;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/v7/media/s;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/q;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/q;->g()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/q;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/q;->g()V

    return-void
.end method

.method public final synthetic b(Lcom/google/android/apps/youtube/app/remote/bg;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/q;->g()V

    return-void
.end method

.method protected final d(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)Landroid/support/v7/media/d;
    .locals 5

    const/4 v4, 0x1

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/remote/q;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    new-instance v1, Landroid/support/v7/media/d;

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/remote/q;->c(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreenName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/support/v7/media/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/support/v7/media/d;->a(Landroid/content/IntentFilter;)Landroid/support/v7/media/d;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v7/media/d;->a(I)Landroid/support/v7/media/d;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v7/media/d;->e(I)Landroid/support/v7/media/d;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v7/media/d;->a(Z)Landroid/support/v7/media/d;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/support/v7/media/d;->d(I)Landroid/support/v7/media/d;

    move-result-object v0

    return-object v0
.end method

.method protected abstract f()Ljava/lang/String;
.end method

.method protected final g()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/q;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/q;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/remote/q;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)Landroid/support/v7/media/c;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/remote/q;->b:Ljava/util/Map;

    invoke-virtual {v3}, Landroid/support/v7/media/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/support/v7/media/l;

    invoke-direct {v0}, Landroid/support/v7/media/l;-><init>()V

    invoke-virtual {v0, v1}, Landroid/support/v7/media/l;->a(Ljava/util/Collection;)Landroid/support/v7/media/l;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/l;->a()Landroid/support/v7/media/k;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Publishing routes: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/remote/q;->c(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/remote/q;->a(Landroid/support/v7/media/k;)V

    return-void
.end method
