.class public Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final args:Landroid/os/Bundle;

.field private final fragmentClass:Ljava/lang/Class;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/navigation/c;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/c;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->fragmentClass:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->args:Landroid/os/Bundle;

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Failed to read fragmentClass."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/apps/youtube/app/fragments/navigation/c;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method protected constructor <init>(Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->fragmentClass:Ljava/lang/Class;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->args:Landroid/os/Bundle;

    return-void
.end method

.method private static areBundleNavigationEndpointsEqual(Landroid/os/Bundle;Landroid/os/Bundle;)Z
    .locals 3

    const/4 v0, 0x1

    const-string v1, "navigation_endpoint"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "navigation_endpoint"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v1, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    if-nez v1, :cond_3

    if-eqz v2, :cond_3

    :cond_1
    const/4 v0, 0x0

    :cond_2
    :goto_0
    return v0

    :cond_3
    if-nez v1, :cond_4

    if-eqz v2, :cond_2

    :cond_4
    invoke-static {p0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->getNavigationEndpointFromBundle(Landroid/os/Bundle;)Lcom/google/a/a/a/a/kz;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->getNavigationEndpointFromBundle(Landroid/os/Bundle;)Lcom/google/a/a/a/a/kz;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/a;->a(Lcom/google/a/a/a/a/kz;Lcom/google/a/a/a/a/kz;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private static getNavigationEndpointFromBundle(Landroid/os/Bundle;)Lcom/google/a/a/a/a/kz;
    .locals 2

    const-string v0, "navigation_endpoint"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    new-instance v0, Lcom/google/a/a/a/a/kz;

    invoke-direct {v0}, Lcom/google/a/a/a/a/kz;-><init>()V

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static toComparableArgs(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2

    const-string v0, "navigation_endpoint"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, p0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    const-string v1, "navigation_endpoint"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    move-object p0, v0

    :cond_0
    return-object p0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->fragmentClass:Ljava/lang/Class;

    iget-object v2, p1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->fragmentClass:Ljava/lang/Class;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->args:Landroid/os/Bundle;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->toComparableArgs(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->args:Landroid/os/Bundle;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->toComparableArgs(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Landroid/os/Bundle;Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->args:Landroid/os/Bundle;

    iget-object v2, p1, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->args:Landroid/os/Bundle;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->areBundleNavigationEndpointsEqual(Landroid/os/Bundle;Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public getArgString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->args:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->fragmentClass:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNavigationEndpoint()Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->args:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->getNavigationEndpointFromBundle(Landroid/os/Bundle;)Lcom/google/a/a/a/a/kz;

    move-result-object v0

    return-object v0
.end method

.method public getSearchType()Lcom/google/android/apps/youtube/app/search/SearchType;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->args:Landroid/os/Bundle;

    const-string v1, "search_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/search/SearchType;->fromString(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/search/SearchType;

    move-result-object v0

    return-object v0
.end method

.method public isCheckPoint()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isGuideEntry()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->args:Landroid/os/Bundle;

    const-string v1, "guide_entry"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public mustAuthenticate()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->args:Landroid/os/Bundle;

    const-string v1, "must_authenticate"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final newFragment()Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->fragmentClass:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
    :try_end_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    if-eqz v0, :cond_0

    new-instance v1, Landroid/os/Bundle;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->args:Landroid/os/Bundle;

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->g(Landroid/os/Bundle;)V

    :cond_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method public setNavigationEndpoint(Lcom/google/a/a/a/a/kz;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->args:Landroid/os/Bundle;

    const-string v1, "navigation_endpoint"

    invoke-static {p1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->fragmentClass:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->args:Landroid/os/Bundle;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
