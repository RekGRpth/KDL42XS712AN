.class final Lcom/google/android/apps/youtube/app/remote/br;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/ytremote/backend/browserchannel/u;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/remote/bk;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/remote/bk;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/remote/bk;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/br;-><init>(Lcom/google/android/apps/youtube/app/remote/bk;)V

    return-void
.end method

.method private static a(Lorg/json/JSONObject;Lcom/google/android/apps/ytremote/backend/model/DeviceType;)Lcom/google/android/apps/ytremote/backend/model/c;
    .locals 4

    :try_start_0
    const-string v0, "hasCc"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "hasCc"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    new-instance v1, Lcom/google/android/apps/ytremote/backend/model/d;

    invoke-direct {v1}, Lcom/google/android/apps/ytremote/backend/model/d;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/android/apps/ytremote/backend/model/d;->a(Lcom/google/android/apps/ytremote/backend/model/DeviceType;)Lcom/google/android/apps/ytremote/backend/model/d;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/ytremote/backend/model/d;->a(Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/d;

    move-result-object v1

    const-string v2, "name"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/ytremote/backend/model/d;->b(Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/d;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/ytremote/model/ClientName;

    const-string v3, "clientName"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/ytremote/model/ClientName;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/ytremote/backend/model/d;->a(Lcom/google/android/apps/ytremote/model/ClientName;)Lcom/google/android/apps/ytremote/backend/model/d;

    move-result-object v1

    const-string v2, "user"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/ytremote/backend/model/d;->c(Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/ytremote/backend/model/d;->a(Z)Lcom/google/android/apps/ytremote/backend/model/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/backend/model/d;->a()Lcom/google/android/apps/ytremote/backend/model/c;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Error parsing device object"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Lorg/json/JSONObject;)Ljava/util/Map;
    .locals 6

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/google/android/apps/ytremote/backend/model/DeviceType;->LOUNGE_SCREEN:Lcom/google/android/apps/ytremote/backend/model/DeviceType;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/ytremote/backend/model/DeviceType;->REMOTE_CONTROL:Lcom/google/android/apps/ytremote/backend/model/DeviceType;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    new-instance v4, Lorg/json/JSONArray;

    const-string v0, "devices"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    if-ge v2, v0, :cond_1

    :try_start_1
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    invoke-static {}, Lcom/google/android/apps/youtube/app/remote/bk;->B()Ljava/util/Map;

    move-result-object v0

    const-string v1, "type"

    invoke-virtual {v5, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/backend/model/DeviceType;

    if-eqz v0, :cond_0

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-static {v5, v0}, Lcom/google/android/apps/youtube/app/remote/br;->a(Lorg/json/JSONObject;Lcom/google/android/apps/ytremote/backend/model/DeviceType;)Lcom/google/android/apps/ytremote/backend/model/c;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "Error parsing lounge status message"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v1, "Error parsing lounge status message"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    return-object v3
.end method

.method private b(Lorg/json/JSONObject;)V
    .locals 4

    const-string v0, "currentTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "current_time"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :try_start_0
    const-string v0, "currentTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "currentTime"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-double v0, v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->F(Lcom/google/android/apps/youtube/app/remote/bk;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;D)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const-string v0, "current_time"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-double v0, v0

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->G(Lcom/google/android/apps/youtube/app/remote/bk;)D

    move-result-wide v2

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x409f400000000000L    # 2000.0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->s(Lcom/google/android/apps/youtube/app/remote/bk;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "Error parsing current time"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private c(Lorg/json/JSONObject;)V
    .locals 3

    const-string v0, "state"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_0
    const-string v0, "state"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->valueOf(I)Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->b(Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;)Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->y(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->g(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->m(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->y(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->m(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->y(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->g(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Error receiving state changed message"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private d(Lorg/json/JSONObject;)Z
    .locals 7

    const/4 v2, 0x0

    :try_start_0
    const-string v0, "errors"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "errors"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    new-instance v3, Lorg/json/JSONArray;

    const-string v0, "errors"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    move v1, v2

    move v0, v2

    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v1, v4, :cond_4

    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "error"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "NOT_PLAYABLE"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "PLAYER_ERROR"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_2
    const-string v5, "videoId"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->m(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v5}, Lcom/google/android/apps/youtube/app/remote/bk;->A(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/remote/bk;->s(Lcom/google/android/apps/youtube/app/remote/bk;)Landroid/os/Handler;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeMessages(I)V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Invalid \'errors\' value in request: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lorg/json/JSONArray;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-le v0, v6, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    invoke-static {v1}, Lcom/google/android/apps/ytremote/backend/model/Method;->fromString(Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Method;

    move-result-object v2

    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Invalid method: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ". Ignoring."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-static {}, Lcom/google/android/apps/youtube/app/remote/bk;->A()Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid JSON array: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/bn;->b:[I

    invoke-virtual {v2}, Lcom/google/android/apps/ytremote/backend/model/Method;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/br;->a(Lorg/json/JSONObject;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    sget-object v0, Lcom/google/android/apps/ytremote/backend/model/DeviceType;->REMOTE_CONTROL:Lcom/google/android/apps/ytremote/backend/model/DeviceType;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/util/Set;)Ljava/util/Set;

    sget-object v0, Lcom/google/android/apps/ytremote/backend/model/DeviceType;->LOUNGE_SCREEN:Lcom/google/android/apps/ytremote/backend/model/DeviceType;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->t(Lcom/google/android/apps/youtube/app/remote/bk;)V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/backend/model/c;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-boolean v2, v0, Lcom/google/android/apps/ytremote/backend/model/c;->g:Z

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/remote/bk;->d(Lcom/google/android/apps/youtube/app/remote/bk;Z)Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->u(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->c(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/apps/ytremote/backend/model/c;->e:Lcom/google/android/apps/ytremote/model/ClientName;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->v(Lcom/google/android/apps/youtube/app/remote/bk;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->w(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/at;->b:Lcom/google/android/apps/youtube/app/remote/as;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/as;)V

    goto/16 :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->t(Lcom/google/android/apps/youtube/app/remote/bk;)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->w(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v0, v7}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Z)V

    goto/16 :goto_0

    :pswitch_3
    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/DeviceType;->REMOTE_CONTROL:Lcom/google/android/apps/ytremote/backend/model/DeviceType;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/br;->a(Lorg/json/JSONObject;Lcom/google/android/apps/ytremote/backend/model/DeviceType;)Lcom/google/android/apps/ytremote/backend/model/c;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->x(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_4
    sget-object v1, Lcom/google/android/apps/ytremote/backend/model/DeviceType;->REMOTE_CONTROL:Lcom/google/android/apps/ytremote/backend/model/DeviceType;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/br;->a(Lorg/json/JSONObject;Lcom/google/android/apps/ytremote/backend/model/DeviceType;)Lcom/google/android/apps/ytremote/backend/model/c;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->x(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_5
    const-string v1, "video_id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    :try_start_2
    const-string v1, "video_id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->y(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->b(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->c(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    const-wide/16 v3, 0x0

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;D)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->z(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->z(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/remote/bk;->y(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/remote/bk;->y(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/app/remote/bk;->b(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/remote/bk;->y(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Ljava/lang/String;)V

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->A(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->B(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->B(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->B(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/remote/bk;->b(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/remote/bk;->b(Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/br;->b(Lorg/json/JSONObject;)V

    :goto_3
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/br;->c(Lorg/json/JSONObject;)V

    goto/16 :goto_0

    :catch_1
    move-exception v1

    const-string v2, "Error receiving now playing message"

    invoke-static {v2, v1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v1, v5, v5}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Ljava/lang/String;)V

    goto :goto_3

    :pswitch_6
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->A(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->A(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->y(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_9
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/br;->b(Lorg/json/JSONObject;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/br;->c(Lorg/json/JSONObject;)V

    goto/16 :goto_0

    :pswitch_7
    const-string v1, "videoIds"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    :try_start_3
    const-string v1, "videoIds"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->C(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->C(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/List;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->C(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Ljava/util/List;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_b
    :goto_4
    const-string v1, "videoId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_4
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    const-string v2, "videoId"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->c(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->A(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->b(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->B(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->y(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->B(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->B(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->b(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->A(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Ljava/lang/String;)V

    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->h(Lcom/google/android/apps/youtube/app/remote/bk;)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    const-string v1, "Error receiving playlist modified message"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :catch_3
    move-exception v1

    const-string v2, "Error receiving playlist modified message"

    invoke-static {v2, v1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    :pswitch_8
    const-string v1, "videoId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->y(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->y(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/lang/String;

    move-result-object v1

    :cond_e
    const-string v2, "format"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v6}, Lcom/google/android/apps/youtube/common/e/m;->b(Ljava/lang/String;I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    const-string v4, "languageCode"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "languageName"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "trackName"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v5, v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;->createMdx(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->z(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V

    goto/16 :goto_0

    :pswitch_9
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/br;->d(Lorg/json/JSONObject;)Z

    goto/16 :goto_0

    :pswitch_a
    const-string v1, "volume"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->D(Lcom/google/android/apps/youtube/app/remote/bk;)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/e/m;->a(Ljava/lang/String;F)F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->D(Lcom/google/android/apps/youtube/app/remote/bk;)I

    move-result v1

    if-eq v1, v0, :cond_f

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->E(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->b(Lcom/google/android/apps/youtube/app/remote/bk;I)I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->D(Lcom/google/android/apps/youtube/app/remote/bk;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/remote/bk;->a(I)V

    :cond_f
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/br;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->E(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
