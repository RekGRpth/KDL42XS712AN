.class final Lcom/google/android/apps/youtube/app/ui/cz;
.super Lcom/google/android/apps/youtube/app/d/c;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/cr;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/ui/cr;Landroid/content/Context;Landroid/widget/ImageView;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/cz;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v0}, Lcom/google/android/apps/youtube/app/d/c;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Lcom/google/android/apps/youtube/app/d/e;)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/d/c;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cz;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cr;->o(Lcom/google/android/apps/youtube/app/ui/cr;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final a(Landroid/net/Uri;Landroid/graphics/Bitmap;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/d/c;->a(Landroid/net/Uri;Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cz;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/cr;->n(Lcom/google/android/apps/youtube/app/ui/cr;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cz;->a:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/cr;->c(Lcom/google/android/apps/youtube/app/ui/cr;)Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    move-result-object v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, p2, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/net/Uri;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/cz;->a(Landroid/net/Uri;Landroid/graphics/Bitmap;)V

    return-void
.end method
