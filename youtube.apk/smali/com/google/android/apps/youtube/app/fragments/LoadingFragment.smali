.class public Lcom/google/android/apps/youtube/app/fragments/LoadingFragment;
.super Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final E()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    sget v0, Lcom/google/android/youtube/l;->bh:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/j;->dU:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/ae;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/youtube/app/fragments/ae;-><init>(Lcom/google/android/apps/youtube/app/fragments/LoadingFragment;Landroid/view/View;)V

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-object v0
.end method
