.class public Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;
.super Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private Y:Lcom/google/android/apps/youtube/app/ui/et;

.field private Z:Lcom/google/android/apps/youtube/app/ui/et;

.field private aa:Lcom/google/android/apps/youtube/app/ui/f;

.field private ab:Lcom/google/android/apps/youtube/app/ui/ie;

.field private ac:Lcom/google/android/apps/youtube/core/async/af;

.field private ad:Landroid/view/View;

.field private ae:Z

.field private af:Z

.field private ag:Lcom/google/android/apps/youtube/app/remote/an;

.field private ah:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

.field private ai:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

.field private aj:Landroid/os/Bundle;

.field private b:Lcom/google/android/apps/youtube/app/ax;

.field private d:Lcom/google/android/apps/youtube/core/identity/l;

.field private e:Lcom/google/android/apps/youtube/core/client/bj;

.field private f:Lcom/google/android/apps/youtube/core/aw;

.field private g:Lcom/google/android/apps/youtube/core/client/bc;

.field private h:Lcom/google/android/apps/youtube/core/Analytics;

.field private i:Lcom/google/android/apps/youtube/app/am;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;-><init>()V

    return-void
.end method

.method private P()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ie;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->g:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->o()Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ie;->a([Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    return-void
.end method

.method private Q()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/f;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->g:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->p()Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/f;->a([Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    return-void
.end method

.method private R()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/k;->m:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->Z:Lcom/google/android/apps/youtube/app/ui/et;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->Z:Lcom/google/android/apps/youtube/app/ui/et;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/et;->a(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->Y:Lcom/google/android/apps/youtube/app/ui/et;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->Y:Lcom/google/android/apps/youtube/app/ui/et;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/et;->a(I)V

    :cond_1
    return-void
.end method

.method private S()V
    .locals 6

    const/4 v1, 0x0

    const/4 v5, -0x1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->O()I

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ae:Z

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->L()Landroid/view/ViewGroup;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ad:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ad:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    if-nez v0, :cond_2

    :goto_1
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->af:Z

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_2
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_3

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v3, Lcom/google/android/youtube/l;->bq:I

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ad:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ad:Landroid/view/View;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ad:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ad:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->bs:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->S()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ae:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;)Lcom/google/android/apps/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->h:Lcom/google/android/apps/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;Z)Z
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->af:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;)Lcom/google/android/apps/youtube/app/am;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->i:Lcom/google/android/apps/youtube/app/am;

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/youtube/core/ui/PagedListView;I)Lcom/google/android/apps/youtube/core/a/a;
    .locals 16

    packed-switch p2, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v2, Lcom/google/android/youtube/p;->cU:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->setEmptyText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->e:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {v1, v2, v15}, Lcom/google/android/apps/youtube/app/adapter/ag;->b(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/af;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v1, v14}, Lcom/google/android/apps/youtube/app/ui/et;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/a/a;)Lcom/google/android/apps/youtube/app/ui/et;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->Z:Lcom/google/android/apps/youtube/app/ui/et;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->R()V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/ao;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->Z:Lcom/google/android/apps/youtube/app/ui/et;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ac:Lcom/google/android/apps/youtube/core/async/af;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->f:Lcom/google/android/apps/youtube/core/aw;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->i:Lcom/google/android/apps/youtube/app/am;

    const/4 v10, 0x0

    sget-object v11, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_RIVER_UPLOADS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->h:Lcom/google/android/apps/youtube/core/Analytics;

    sget-object v13, Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;->ChannelUploads:Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/youtube/app/fragments/ao;-><init>(Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;ZLcom/google/android/apps/youtube/app/am;ZLcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ie;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->aj:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ie;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->aj:Landroid/os/Bundle;

    const-string v3, "uploads_helper"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/ie;->a(Landroid/os/Bundle;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->d:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->P()V

    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ag:Lcom/google/android/apps/youtube/app/remote/an;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->Z:Lcom/google/android/apps/youtube/app/ui/et;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->f:Lcom/google/android/apps/youtube/core/aw;

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_RIVER_UPLOADS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->h:Lcom/google/android/apps/youtube/core/Analytics;

    move-object v2, v15

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ah:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ah:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->c()V

    move-object v1, v14

    :goto_1
    return-object v1

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ie;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/ie;->e()V

    goto :goto_0

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v2, Lcom/google/android/youtube/p;->cN:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->setEmptyText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/ui/ad;->b(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->g:Lcom/google/android/apps/youtube/core/client/bc;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->e:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {v1, v2, v3, v10}, Lcom/google/android/apps/youtube/app/adapter/ag;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/af;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v1, v9}, Lcom/google/android/apps/youtube/app/ui/et;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/a/a;)Lcom/google/android/apps/youtube/app/ui/et;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->Y:Lcom/google/android/apps/youtube/app/ui/et;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->R()V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/ap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->Y:Lcom/google/android/apps/youtube/app/ui/et;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->g:Lcom/google/android/apps/youtube/core/client/bc;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->f:Lcom/google/android/apps/youtube/core/aw;

    const/4 v8, 0x1

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/youtube/app/fragments/ap;-><init>(Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/aw;Z)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/f;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->aj:Landroid/os/Bundle;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/f;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->aj:Landroid/os/Bundle;

    const-string v3, "events_helper"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/f;->a(Landroid/os/Bundle;)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->d:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->Q()V

    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ag:Lcom/google/android/apps/youtube/app/remote/an;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->Y:Lcom/google/android/apps/youtube/app/ui/et;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->f:Lcom/google/android/apps/youtube/core/aw;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->h:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {v1, v10, v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/Analytics;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ai:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-object v1, v9

    goto/16 :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/f;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/f;->e()V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->cf:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->a(Landroid/os/Bundle;)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->aj:Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->b:Lcom/google/android/apps/youtube/app/ax;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->d:Lcom/google/android/apps/youtube/core/identity/l;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->e:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->g:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->f:Lcom/google/android/apps/youtube/core/aw;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->h:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->i:Lcom/google/android/apps/youtube/app/am;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ag:Lcom/google/android/apps/youtube/app/remote/an;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->g:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->l()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ac:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->d:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ie;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->P()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/f;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->Q()V

    :cond_1
    return-void
.end method

.method protected final b(I)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->S()V

    return-void
.end method

.method protected final c(I)Ljava/lang/String;
    .locals 2

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->dD:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->dB:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final d()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->d()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->R()V

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->e(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ie;

    if-eqz v0, :cond_0

    const-string v0, "uploads_helper"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ie;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/ie;->d()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/f;

    if-eqz v0, :cond_1

    const-string v0, "events_helper"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->aa:Lcom/google/android/apps/youtube/app/ui/f;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/f;->d()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->aj:Landroid/os/Bundle;

    return-void
.end method

.method protected final j_()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->i:Lcom/google/android/apps/youtube/app/am;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/am;->h()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->R()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->S()V

    return-void
.end method

.method public final r()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->r()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ah:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ah:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ai:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ai:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b()V

    :cond_1
    return-void
.end method

.method public final t()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->t()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ah:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ah:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ai:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MySubscriptionsFragment;->ai:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a()V

    :cond_1
    return-void
.end method
