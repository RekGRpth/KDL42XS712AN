.class public final Lcom/google/android/apps/youtube/app/ui/presenter/ch;
.super Lcom/google/android/apps/youtube/uilib/a/a;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Lcom/google/android/apps/youtube/uilib/a/i;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Lcom/google/android/apps/youtube/app/ui/gr;

.field private final f:Landroid/view/View;

.field private final g:Lcom/google/android/apps/youtube/app/ui/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/uilib/a/i;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/app/ui/v;)V
    .locals 3

    invoke-direct {p0, p4, p2}, Lcom/google/android/apps/youtube/uilib/a/a;-><init>(Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/uilib/a/i;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/a/i;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->b:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/v;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->g:Lcom/google/android/apps/youtube/app/ui/v;

    sget v0, Lcom/google/android/youtube/l;->bf:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->ff:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->d:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/gr;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->a:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {v1, p3, v0}, Lcom/google/android/apps/youtube/app/ui/gr;-><init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->e:Lcom/google/android/apps/youtube/app/ui/gr;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aE:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->a:Landroid/view/View;

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Landroid/view/View;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/ak;)Landroid/view/View;
    .locals 4

    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/uilib/a/a;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/u;)Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->c:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ak;->a()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ak;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->d:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ak;->b()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ak;->d()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->e:Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ak;->d()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/app/ui/gr;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->e:Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ui/gr;->a()Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->g:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->f:Landroid/view/View;

    invoke-static {v2, v3, p2}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/ui/v;Landroid/view/View;Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->f:Landroid/view/View;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ak;->f()Lcom/google/a/a/a/a/kz;

    move-result-object v3

    if-nez v3, :cond_3

    :goto_2
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->b:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Lcom/google/android/apps/youtube/uilib/a/f;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ak;->c()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->d:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ak;->c()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->e:Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ui/gr;->a()Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/u;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/ak;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/ak;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/ak;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/ch;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/ak;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
