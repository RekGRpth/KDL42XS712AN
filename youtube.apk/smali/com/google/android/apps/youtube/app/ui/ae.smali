.class final Lcom/google/android/apps/youtube/app/ui/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/ac;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/ba;

.field final synthetic b:Lcom/google/android/apps/youtube/app/ui/ap;

.field final synthetic c:Lcom/google/android/apps/youtube/core/offline/store/q;

.field final synthetic d:Lcom/google/android/apps/youtube/core/identity/l;

.field final synthetic e:Lcom/google/android/apps/youtube/app/ui/az;

.field final synthetic f:Lcom/google/android/apps/youtube/app/ui/az;

.field final synthetic g:Lcom/google/android/apps/youtube/app/ui/az;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/ui/ba;Lcom/google/android/apps/youtube/app/ui/ap;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/app/ui/az;Lcom/google/android/apps/youtube/app/ui/az;Lcom/google/android/apps/youtube/app/ui/az;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ae;->a:Lcom/google/android/apps/youtube/app/ui/ba;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/ae;->b:Lcom/google/android/apps/youtube/app/ui/ap;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/ae;->c:Lcom/google/android/apps/youtube/core/offline/store/q;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/ui/ae;->d:Lcom/google/android/apps/youtube/core/identity/l;

    iput-object p5, p0, Lcom/google/android/apps/youtube/app/ui/ae;->e:Lcom/google/android/apps/youtube/app/ui/az;

    iput-object p6, p0, Lcom/google/android/apps/youtube/app/ui/ae;->f:Lcom/google/android/apps/youtube/app/ui/az;

    iput-object p7, p0, Lcom/google/android/apps/youtube/app/ui/ae;->g:Lcom/google/android/apps/youtube/app/ui/az;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/app/ui/v;Ljava/lang/Object;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ae;->a:Lcom/google/android/apps/youtube/app/ui/ba;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/app/ui/ba;->c(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/ae;->b:Lcom/google/android/apps/youtube/app/ui/ap;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/ae;->a:Lcom/google/android/apps/youtube/app/ui/ba;

    invoke-virtual {v4, p2}, Lcom/google/android/apps/youtube/app/ui/ba;->a(Ljava/lang/Object;)Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/google/android/apps/youtube/app/ui/ap;->a(Lcom/google/android/apps/youtube/app/ui/ap;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/ae;->c:Lcom/google/android/apps/youtube/core/offline/store/q;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/ae;->d:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {v3, v4}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/identity/l;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/ae;->e:Lcom/google/android/apps/youtube/app/ui/az;

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v3, v1}, Lcom/google/android/apps/youtube/app/ui/az;->a(Lcom/google/android/apps/youtube/app/ui/az;Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ae;->f:Lcom/google/android/apps/youtube/app/ui/az;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/ui/az;->a(Lcom/google/android/apps/youtube/app/ui/az;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ae;->g:Lcom/google/android/apps/youtube/app/ui/az;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ae;->a:Lcom/google/android/apps/youtube/app/ui/ba;

    invoke-virtual {v1, p2}, Lcom/google/android/apps/youtube/app/ui/ba;->d(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/az;->a(Lcom/google/android/apps/youtube/app/ui/az;Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
