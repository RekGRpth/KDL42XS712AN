.class final Lcom/google/android/apps/youtube/app/ui/if;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/ig;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/am;

.field private final b:Z

.field private final c:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field private final d:Lcom/google/android/apps/youtube/core/Analytics;

.field private final e:Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/am;ZLcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/if;->a:Lcom/google/android/apps/youtube/app/am;

    iput-boolean p2, p0, Lcom/google/android/apps/youtube/app/ui/if;->b:Z

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/if;->c:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/ui/if;->d:Lcom/google/android/apps/youtube/core/Analytics;

    iput-object p5, p0, Lcom/google/android/apps/youtube/app/ui/if;->e:Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/if;->d:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/if;->e:Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;

    invoke-interface {v0, v1, p2}, Lcom/google/android/apps/youtube/core/Analytics;->a(Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/if;->a:Lcom/google/android/apps/youtube/app/am;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/if;->b:Z

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/if;->c:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/am;->a(Ljava/lang/String;ZLcom/google/android/apps/youtube/core/client/WatchFeature;)V

    return-void
.end method
