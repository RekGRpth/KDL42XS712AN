.class final Lcom/google/android/apps/youtube/app/d/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/d/r;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/d/f;

.field private b:Landroid/app/AlertDialog;

.field private c:Landroid/app/AlertDialog;

.field private d:Landroid/app/AlertDialog;

.field private e:Lcom/google/android/apps/youtube/app/d/j;

.field private f:Lcom/google/android/apps/youtube/datalib/distiller/model/c;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/d/f;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/d/s;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/d/f;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/d/s;-><init>(Lcom/google/android/apps/youtube/app/d/f;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/d/s;)Lcom/google/android/apps/youtube/app/d/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->e:Lcom/google/android/apps/youtube/app/d/j;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/d/s;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->c:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/s;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/d/f;->c(Lcom/google/android/apps/youtube/app/d/f;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/youtube/p;->fo:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->fm:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->fp:I

    new-instance v2, Lcom/google/android/apps/youtube/app/d/v;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/d/v;-><init>(Lcom/google/android/apps/youtube/app/d/s;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->fn:I

    new-instance v2, Lcom/google/android/apps/youtube/app/d/u;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/d/u;-><init>(Lcom/google/android/apps/youtube/app/d/s;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->c:Landroid/app/AlertDialog;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->a:Lcom/google/android/apps/youtube/app/d/f;

    new-instance v1, Lcom/google/android/apps/youtube/app/d/x;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/d/x;-><init>(Lcom/google/android/apps/youtube/app/d/s;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/d/f;->a(Lcom/google/android/apps/youtube/app/d/f;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/d/s;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->f:Lcom/google/android/apps/youtube/datalib/distiller/model/c;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->a(Lcom/google/android/apps/youtube/datalib/distiller/model/c;)Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->n()Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->PRIVATE:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->a:Lcom/google/android/apps/youtube/app/d/f;

    sget v1, Lcom/google/android/youtube/p;->at:I

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/d/f;->a(Lcom/google/android/apps/youtube/app/d/f;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->l(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/app/d/p;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/s;->f:Lcom/google/android/apps/youtube/datalib/distiller/model/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/d/p;->a(Lcom/google/android/apps/youtube/datalib/distiller/model/c;)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/d/s;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->d:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/s;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/d/f;->c(Lcom/google/android/apps/youtube/app/d/f;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/youtube/p;->aH:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->aF:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->aI:I

    new-instance v2, Lcom/google/android/apps/youtube/app/d/z;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/d/z;-><init>(Lcom/google/android/apps/youtube/app/d/s;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->aG:I

    new-instance v2, Lcom/google/android/apps/youtube/app/d/y;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/d/y;-><init>(Lcom/google/android/apps/youtube/app/d/s;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->d:Landroid/app/AlertDialog;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->a:Lcom/google/android/apps/youtube/app/d/f;

    new-instance v1, Lcom/google/android/apps/youtube/app/d/ab;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/d/ab;-><init>(Lcom/google/android/apps/youtube/app/d/s;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/d/f;->a(Lcom/google/android/apps/youtube/app/d/f;Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/d/s;)Lcom/google/android/apps/youtube/datalib/distiller/model/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->f:Lcom/google/android/apps/youtube/datalib/distiller/model/c;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/d/s;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->c:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/d/s;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->d:Landroid/app/AlertDialog;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/distiller/model/c;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->b:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/d/j;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/s;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/app/d/j;-><init>(Lcom/google/android/apps/youtube/app/d/f;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->e:Lcom/google/android/apps/youtube/app/d/j;

    new-instance v0, Lcom/google/android/apps/youtube/core/ui/aa;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/s;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/d/f;->c(Lcom/google/android/apps/youtube/app/d/f;)Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/ui/aa;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/youtube/p;->an:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/aa;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/s;->e:Lcom/google/android/apps/youtube/app/d/j;

    new-instance v2, Lcom/google/android/apps/youtube/app/d/t;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/d/t;-><init>(Lcom/google/android/apps/youtube/app/d/s;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->b:Landroid/app/AlertDialog;

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/d/s;->f:Lcom/google/android/apps/youtube/datalib/distiller/model/c;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->e:Lcom/google/android/apps/youtube/app/d/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/d/j;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->e:Lcom/google/android/apps/youtube/app/d/j;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/d/f;->a(Lcom/google/android/apps/youtube/datalib/distiller/model/c;)Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->o()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/d/s;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/d/f;->a(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/app/ui/presenter/v;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/youtube/app/ui/presenter/v;->b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/d/s;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/d/f;->a(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/app/ui/presenter/v;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/youtube/app/ui/presenter/v;->b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v2, v2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->plusUserId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/d/s;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/d/f;->a(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/app/ui/presenter/v;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/youtube/app/ui/presenter/v;->b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v2, v2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->plusUserId:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/distiller/model/c;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/d/j;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/s;->b:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void

    :cond_2
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
