.class final Lcom/google/android/apps/youtube/app/b/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/client/a/d;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/b/e;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/b/e;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/b/h;->a:Lcom/google/android/apps/youtube/app/b/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)Ljava/util/List;
    .locals 4

    check-cast p1, Lcom/google/android/apps/youtube/core/player/event/ac;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sget-object v1, Lcom/google/android/apps/youtube/app/b/k;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->c()Lcom/google/android/apps/youtube/core/player/Director$VideoStage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/Director$VideoStage;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v1, Landroid/util/Pair;

    const-string v2, "docid"

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_1
    new-instance v1, Landroid/util/Pair;

    const-string v2, "mod_ad"

    const-string v3, "1"

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_2
    new-instance v1, Landroid/util/Pair;

    const-string v2, "mod_ad_pr"

    const-string v3, "1"

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Landroid/util/Pair;

    const-string v2, "ad_at"

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/player/event/ac;->e()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdFormat()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
