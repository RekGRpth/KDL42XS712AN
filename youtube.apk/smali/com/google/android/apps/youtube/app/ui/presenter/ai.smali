.class public final Lcom/google/android/apps/youtube/app/ui/presenter/ai;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/g;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/youtube/app/ui/a;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/ui/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ai;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ai;->b:Lcom/google/android/apps/youtube/app/ui/a;

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 3

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/k;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ai;->b:Lcom/google/android/apps/youtube/app/ui/a;

    const-string v1, "FORECASTING_PROMOTED_VIDEO"

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/k;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/a;->a(Ljava/lang/String;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ai;->c:Landroid/view/View;

    if-nez v0, :cond_0

    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ai;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ai;->c:Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ai;->c:Landroid/view/View;

    return-object v0
.end method
