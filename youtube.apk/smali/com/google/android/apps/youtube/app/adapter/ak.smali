.class final Lcom/google/android/apps/youtube/app/adapter/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/adapter/ae;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/adapter/aj;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Lcom/google/android/apps/youtube/app/adapter/ae;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/adapter/aj;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/adapter/ak;->a:Lcom/google/android/apps/youtube/app/adapter/aj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lcom/google/android/youtube/j;->fZ:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ak;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ak;->b:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aP:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ak;->c:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/adapter/aj;->a(Lcom/google/android/apps/youtube/app/adapter/aj;)Lcom/google/android/apps/youtube/app/adapter/ai;

    move-result-object v0

    invoke-interface {v0, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/ai;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/youtube/app/adapter/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ak;->d:Lcom/google/android/apps/youtube/app/adapter/ae;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/adapter/aj;Landroid/view/View;Landroid/view/ViewGroup;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/ak;-><init>(Lcom/google/android/apps/youtube/app/adapter/aj;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 3

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ak;->d:Lcom/google/android/apps/youtube/app/adapter/ae;

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent;->video:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/app/adapter/ae;->a(ILjava/lang/Object;)Landroid/view/View;

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent;->video:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ak;->c:Landroid/widget/TextView;

    sget v1, Lcom/google/android/youtube/p;->cl:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ak;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/ak;->a:Lcom/google/android/apps/youtube/app/adapter/aj;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/aj;->b(Lcom/google/android/apps/youtube/app/adapter/aj;)Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/q;->f:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ak;->b:Landroid/view/View;

    return-object v0
.end method
