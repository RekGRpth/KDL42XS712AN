.class final Lcom/google/android/apps/youtube/app/aq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/iu;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/aq;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/aq;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "watch_while_tutorial_views_remaining"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/aq;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/aq;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->b(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/aq;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->b()V

    return-void
.end method

.method public final b()V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/aq;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "watch_while_tutorial_views_remaining"

    const/4 v3, 0x3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/aq;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "watch_while_tutorial_views_remaining"

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/aq;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/aq;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->b(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/aq;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->b()V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method
