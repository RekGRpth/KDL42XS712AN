.class public final Lcom/google/android/apps/youtube/app/ui/l;
.super Lcom/google/android/apps/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final b:Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;

.field private final c:Landroid/app/Activity;

.field private final d:Lcom/google/android/apps/youtube/app/am;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/adapter/br;Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;Landroid/app/Activity;Lcom/google/android/apps/youtube/app/am;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/youtube/core/a/e;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/core/a/l;-><init>([Lcom/google/android/apps/youtube/core/a/e;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/l;->b:Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/am;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/l;->d:Lcom/google/android/apps/youtube/app/am;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/l;->c:Landroid/app/Activity;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/adapter/br;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/adapter/br;->a()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/j;->ae:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {p3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;->toString(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v1, Lcom/google/android/youtube/j;->ad:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;Lcom/google/android/apps/youtube/app/am;)Lcom/google/android/apps/youtube/app/ui/l;
    .locals 4

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->r:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/br;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/app/adapter/br;-><init>(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/l;

    invoke-direct {v0, v1, p1, p0, p2}, Lcom/google/android/apps/youtube/app/ui/l;-><init>(Lcom/google/android/apps/youtube/app/adapter/br;Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;Landroid/app/Activity;Lcom/google/android/apps/youtube/app/am;)V

    return-object v0
.end method


# virtual methods
.method public final b(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/l;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/youtube/app/ui/m;->a:[I

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/l;->b:Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    move-object v0, v1

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/l;->d:Lcom/google/android/apps/youtube/app/am;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/l;->b:Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/youtube/app/am;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;)V

    return-void

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;->MOST_SUBSCRIBED:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;->MOST_VIEWED:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->i(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;->NOTEWORTHY:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$ChannelFeed;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->a()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
