.class public final Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/remote/bh;


# instance fields
.field private final a:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final b:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final c:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final d:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final e:Lcom/google/android/apps/youtube/common/a/b;

.field private final f:Lcom/google/android/apps/youtube/app/remote/bw;

.field private final g:Lcom/google/android/apps/youtube/app/remote/bk;

.field private final h:Landroid/content/SharedPreferences;

.field private final i:Landroid/content/Context;

.field private final j:Ljava/util/Map;

.field private final k:Ljava/util/Map;

.field private final l:Ljava/util/Map;

.field private final m:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private final n:Landroid/os/Handler;

.field private final o:Lcom/google/android/apps/youtube/common/network/h;

.field private final p:Lcom/google/android/apps/ytremote/logic/b;

.field private q:Z

.field private r:Z

.field private final s:Ljava/util/concurrent/Executor;

.field private final t:Lcom/google/android/apps/youtube/app/ac;

.field private final u:Z

.field private final v:Ljava/util/Set;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/app/remote/bw;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/remote/bk;Landroid/content/SharedPreferences;ZLcom/google/android/apps/youtube/app/ac;Landroid/content/Context;Lcom/google/android/apps/youtube/core/identity/l;)V
    .locals 6

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/bw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->f:Lcom/google/android/apps/youtube/app/remote/bw;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->o:Lcom/google/android/apps/youtube/common/network/h;

    const-string v0, "youTubeTvRemoteControl can not be null"

    invoke-static {p4, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/bk;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->g:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->h:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->s:Ljava/util/concurrent/Executor;

    iput-boolean p6, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->u:Z

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ac;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->t:Lcom/google/android/apps/youtube/app/ac;

    iput-object p8, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->i:Landroid/content/Context;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/co;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/remote/co;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->e:Lcom/google/android/apps/youtube/common/a/b;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->j:Ljava/util/Map;

    new-instance v0, Lcom/google/android/apps/ytremote/b/j;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/b/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->p:Lcom/google/android/apps/ytremote/logic/b;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->k:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->l:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->m:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/bx;

    invoke-direct {v0, p0, p5}, Lcom/google/android/apps/youtube/app/remote/bx;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Landroid/content/SharedPreferences;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/cl;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/remote/cl;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;B)V

    invoke-virtual {p4, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/au;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/cm;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/remote/cm;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;B)V

    invoke-virtual {p4, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/aq;)V

    new-instance v0, Lcom/google/android/apps/ytremote/b/c;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/b/c;-><init>()V

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/by;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/app/remote/by;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/ytremote/b/c;)V

    invoke-static {p1, v1}, Lcom/google/android/apps/youtube/core/async/b;->a(Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/async/af;)Lcom/google/android/apps/youtube/core/async/b;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/bz;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/remote/bz;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/remote/bw;Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/core/async/b;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n:Landroid/os/Handler;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.google.android.c2dm.intent.RECEIVE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.google.android.youtube"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor$PushNotificationsBroadcastReceiver;

    invoke-direct {v1, p2, p9, p4}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor$PushNotificationsBroadcastReceiver;-><init>(Lcom/google/android/apps/youtube/app/remote/bw;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/app/remote/bk;)V

    const-string v2, "com.google.android.c2dm.permission.SEND"

    const/4 v3, 0x0

    invoke-virtual {p8, v1, v0, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->v:Ljava/util/Set;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/ytremote/model/SsdpId;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/ytremote/model/SsdpId;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/apps/ytremote/model/SsdpId;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/apps/ytremote/model/SsdpId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/ytremote/model/YouTubeDevice;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized a(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)Ljava/lang/String;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->h:Landroid/content/SharedPreferences;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Landroid/content/SharedPreferences;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->k:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->k:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getDeviceName()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Landroid/content/SharedPreferences;)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->r:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    const-string v0, "dial_device_ids"

    invoke-interface {p1, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "dial_device_ids"

    const-string v1, ""

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    const-string v0, "dial_device_names"

    const-string v2, ""

    invoke-interface {p1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_3

    aget-object v3, v1, v0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v4, Lcom/google/android/apps/ytremote/model/SsdpId;

    invoke-direct {v4, v3}, Lcom/google/android/apps/ytremote/model/SsdpId;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->m:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->k:Ljava/util/Map;

    aget-object v5, v2, v0

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->r:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/cc;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/remote/cc;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;I)V
    .locals 3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/ytremote/model/SsdpId;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Removing duplicate device "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Landroid/content/SharedPreferences;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Landroid/content/SharedPreferences;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->q:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Lcom/google/android/apps/youtube/common/a/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->e:Lcom/google/android/apps/youtube/common/a/b;

    return-object v0
.end method

.method private b(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/cd;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/remote/cd;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private b(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreen()Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreen()Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/ytremote/model/ScreenId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Removing duplicate screen "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->d(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(ILjava/lang/Object;)V

    goto :goto_2
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;I)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method private c()V
    .locals 5

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->o:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->o:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->o:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "Not connected to wifi/eth or network not available. Will mark all devices as unavailable."

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    :goto_1
    return-void

    :cond_4
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/y;->a(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n:Landroid/os/Handler;

    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v3, 0x251c

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const-string v1, "Starting new DIAL search."

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->p:Lcom/google/android/apps/ytremote/logic/b;

    new-instance v2, Lcom/google/android/apps/youtube/app/remote/cj;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/youtube/app/remote/cj;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Ljava/util/Set;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/ytremote/logic/b;->a(Lcom/google/android/apps/ytremote/logic/c;)V

    goto :goto_1
.end method

.method private c(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Removing dial device "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->l:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->u:Z

    if-nez v0, :cond_0

    const-string v0, "true"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->t:Lcom/google/android/apps/youtube/app/ac;

    const-string v2, "enable_mdx_logs"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/app/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "YouTube MDX"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method private d(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Removing cloud screen "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->d(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    return-void
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->l:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->j:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->k:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->m:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->h:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/concurrent/CopyOnWriteArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Lcom/google/android/apps/youtube/app/remote/bk;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->g:Lcom/google/android/apps/youtube/app/remote/bk;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->i:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->d(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->f:Lcom/google/android/apps/youtube/app/remote/bw;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreen()Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/app/remote/cp;

    invoke-direct {v2, p1, p2}, Lcom/google/android/apps/youtube/app/remote/cp;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/app/remote/bw;->a(Lcom/google/android/apps/ytremote/model/ScreenId;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->withNewDeviceName(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-direct {v2, v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;-><init>(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)V

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->s:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/cn;

    invoke-direct {v1, p0, p1, v2, p3}, Lcom/google/android/apps/youtube/app/remote/cn;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreen()Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/ytremote/model/CloudScreen;->withName(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-direct {v2, v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;-><init>(Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->b(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->f:Lcom/google/android/apps/youtube/app/remote/bw;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreen()Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/app/remote/cp;

    invoke-direct {v2, p1, p3}, Lcom/google/android/apps/youtube/app/remote/cp;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1, p2, v2}, Lcom/google/android/apps/youtube/app/remote/bw;->a(Lcom/google/android/apps/ytremote/model/ScreenId;Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/bi;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/ytremote/model/CloudScreen;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->f:Lcom/google/android/apps/youtube/app/remote/bw;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/cg;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/app/remote/cg;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/ytremote/model/CloudScreen;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/app/remote/bw;->a(Lcom/google/android/apps/ytremote/model/CloudScreen;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/ytremote/model/SsdpId;Lcom/google/android/apps/youtube/app/remote/ck;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/apps/ytremote/model/SsdpId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/app/remote/ck;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->j:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->p:Lcom/google/android/apps/ytremote/logic/b;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/ca;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/app/remote/ca;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/ytremote/model/SsdpId;Lcom/google/android/apps/youtube/app/remote/ck;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/ytremote/logic/b;->a(Lcom/google/android/apps/ytremote/logic/c;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n:Landroid/os/Handler;

    const/4 v1, 0x3

    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n:Landroid/os/Handler;

    const-wide/16 v2, 0x251c

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->v:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->v:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->q:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->f:Lcom/google/android/apps/youtube/app/remote/bw;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/ce;

    invoke-direct {v1, p0, p3}, Lcom/google/android/apps/youtube/app/remote/ce;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, p1, p2, v1}, Lcom/google/android/apps/youtube/app/remote/bw;->a(Ljava/lang/String;Lcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final b()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->d:Ljava/util/concurrent/CopyOnWriteArrayList;

    return-object v0
.end method

.method public final b(Lcom/google/android/apps/youtube/app/remote/bi;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->v:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v4, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->q:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->f:Lcom/google/android/apps/youtube/app/remote/bw;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bw;->c(Lcom/google/android/apps/youtube/common/a/b;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->f:Lcom/google/android/apps/youtube/app/remote/bw;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->e:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bw;->a(Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->c()V

    iput-boolean v4, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->q:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n:Landroid/os/Handler;

    const-wide/16 v1, 0x2710

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->v:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method
