.class final Lcom/google/android/apps/youtube/app/remote/cm;
.super Lcom/google/android/apps/youtube/app/remote/av;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/cm;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/av;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/cm;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/cm;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->x()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasScreen()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/cm;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->o(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/cm;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->x()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreen()Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/a;->a(Landroid/content/Context;Lcom/google/android/apps/ytremote/model/ScreenId;)V

    :cond_1
    return-void
.end method
