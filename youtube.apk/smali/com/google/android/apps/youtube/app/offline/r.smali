.class public final Lcom/google/android/apps/youtube/app/offline/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/core/identity/l;

.field private final c:Lcom/google/android/apps/youtube/core/offline/store/q;

.field private final d:Lcom/google/android/apps/youtube/core/identity/aa;

.field private final e:Lcom/google/android/apps/youtube/core/aw;

.field private final f:Lcom/google/android/apps/youtube/common/network/h;

.field private final g:Lcom/google/android/apps/youtube/app/offline/p;

.field private final h:Lcom/google/android/apps/youtube/app/ui/bv;

.field private final i:Lcom/google/android/apps/youtube/app/ui/hh;

.field private final j:Lcom/google/android/apps/youtube/app/offline/y;

.field private final k:Lcom/google/android/apps/youtube/app/offline/w;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/app/ui/bv;Lcom/google/android/apps/youtube/app/ui/hh;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->a:Landroid/app/Activity;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/q;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->c:Lcom/google/android/apps/youtube/core/offline/store/q;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/aa;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->d:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->b:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->e:Lcom/google/android/apps/youtube/core/aw;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->f:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/p;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->g:Lcom/google/android/apps/youtube/app/offline/p;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/bv;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->h:Lcom/google/android/apps/youtube/app/ui/bv;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/hh;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->i:Lcom/google/android/apps/youtube/app/ui/hh;

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/y;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/offline/y;-><init>(Lcom/google/android/apps/youtube/app/offline/r;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->j:Lcom/google/android/apps/youtube/app/offline/y;

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/w;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/offline/w;-><init>(Lcom/google/android/apps/youtube/app/offline/r;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->k:Lcom/google/android/apps/youtube/app/offline/w;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/offline/r;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->e:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method

.method private a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->b:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->c:Lcom/google/android/apps/youtube/core/offline/store/q;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/q;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->c:Lcom/google/android/apps/youtube/core/offline/store/q;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/r;->b:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/q;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/offline/r;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/offline/r;->a(Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/offline/r;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;Lcom/google/android/apps/youtube/app/offline/v;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/offline/r;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;Lcom/google/android/apps/youtube/app/offline/v;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/offline/r;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;Lcom/google/android/apps/youtube/app/offline/v;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/offline/r;->b(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;Lcom/google/android/apps/youtube/app/offline/v;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/offline/r;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/v;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/offline/r;->c(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/v;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;)V
    .locals 3

    sget-object v0, Lcom/google/android/apps/youtube/app/offline/u;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->g:Lcom/google/android/apps/youtube/app/offline/p;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/offline/p;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->f:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->c()Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/youtube/p;->r:I

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/r;->a:Landroid/app/Activity;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    goto :goto_0

    :cond_0
    sget v0, Lcom/google/android/youtube/p;->q:I

    goto :goto_1

    :pswitch_1
    sget v0, Lcom/google/android/youtube/p;->gG:I

    goto :goto_1

    :pswitch_2
    sget v0, Lcom/google/android/youtube/p;->t:I

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;Lcom/google/android/apps/youtube/app/offline/v;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/offline/r;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;->getFormatType()Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    move-result-object v0

    if-eqz p3, :cond_0

    invoke-interface {p3, p1, v0}, Lcom/google/android/apps/youtube/app/offline/v;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;)V

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/r;->a(Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/offline/r;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->a:Landroid/app/Activity;

    return-object v0
.end method

.method private b(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;Lcom/google/android/apps/youtube/app/offline/v;)V
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/offline/r;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->t()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->u()Z

    move-result v0

    :cond_0
    :goto_0
    if-nez v0, :cond_3

    if-eqz p3, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->ALREADY_ADDED:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    invoke-interface {p3, p1, v0}, Lcom/google/android/apps/youtube/app/offline/v;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;)V

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;->ALREADY_ADDED:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/r;->a(Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;)V

    :goto_1
    return-void

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;->a()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->i:Lcom/google/android/apps/youtube/app/ui/hh;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;->c()Lcom/google/a/a/a/a/mh;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/hh;->a(Lcom/google/a/a/a/a/mh;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->g:Lcom/google/android/apps/youtube/app/offline/p;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/app/offline/p;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->h:Lcom/google/android/apps/youtube/app/ui/bv;

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/t;

    invoke-direct {v1, p0, p1, p3}, Lcom/google/android/apps/youtube/app/offline/t;-><init>(Lcom/google/android/apps/youtube/app/offline/r;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/v;)V

    invoke-virtual {v0, p2, v1}, Lcom/google/android/apps/youtube/app/ui/bv;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;Lcom/google/android/apps/youtube/app/ui/bx;)Z

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->g:Lcom/google/android/apps/youtube/app/offline/p;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/offline/p;->c()Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/apps/youtube/app/offline/r;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;Lcom/google/android/apps/youtube/app/offline/v;)V

    goto :goto_1
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/offline/r;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/offline/r;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    return-object v0
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/v;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/offline/r;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;

    move-result-object v0

    if-eqz p3, :cond_0

    invoke-interface {p3, p2, v0}, Lcom/google/android/apps/youtube/app/offline/v;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;)V

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/offline/r;->a(Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface$OfflineAddResult;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/offline/r;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->k:Lcom/google/android/apps/youtube/app/offline/w;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/offline/w;->a(Lcom/google/android/apps/youtube/app/offline/w;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;Lcom/google/android/apps/youtube/app/offline/v;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->b:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->d:Lcom/google/android/apps/youtube/core/identity/aa;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/r;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/apps/youtube/app/offline/s;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/offline/s;-><init>(Lcom/google/android/apps/youtube/app/offline/r;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;Lcom/google/android/apps/youtube/app/offline/v;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ah;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/offline/r;->b(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;Lcom/google/android/apps/youtube/app/offline/v;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/v;)V
    .locals 2

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/offline/r;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->t()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/r;->j:Lcom/google/android/apps/youtube/app/offline/y;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/offline/y;->a(Lcom/google/android/apps/youtube/app/offline/y;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/v;)V

    :cond_1
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/v;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/youtube/app/offline/r;->c(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/v;)V

    return-void
.end method
