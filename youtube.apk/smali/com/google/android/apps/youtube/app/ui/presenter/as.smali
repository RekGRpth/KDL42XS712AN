.class public final Lcom/google/android/apps/youtube/app/ui/presenter/as;
.super Lcom/google/android/apps/youtube/app/ui/presenter/a;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/a;-><init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;)Landroid/view/View;
    .locals 3

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/as;->b()V

    :goto_0
    const/4 v0, 0x0

    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/as;->c()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->c()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->d()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/presenter/as;->a(ZII)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/as;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
