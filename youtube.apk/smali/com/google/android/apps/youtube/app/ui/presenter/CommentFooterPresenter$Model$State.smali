.class public final enum Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

.field public static final enum CAN_LOAD_MORE:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

.field public static final enum LOADING:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

.field public static final enum NO_COMMENTS:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

.field public static final enum NO_MORE_COMMENTS:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    const-string v1, "CAN_LOAD_MORE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;->CAN_LOAD_MORE:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    const-string v1, "NO_MORE_COMMENTS"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;->NO_MORE_COMMENTS:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    const-string v1, "NO_COMMENTS"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;->NO_COMMENTS:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;->LOADING:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;->CAN_LOAD_MORE:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;->NO_MORE_COMMENTS:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;->NO_COMMENTS:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;->LOADING:Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;->$VALUES:[Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;->$VALUES:[Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/app/ui/presenter/CommentFooterPresenter$Model$State;

    return-object v0
.end method
