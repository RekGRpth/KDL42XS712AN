.class public final Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity$ConnectionErrorDialogFragment;
.super Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;
.source "SourceFile"


# instance fields
.field final synthetic Y:Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity$ConnectionErrorDialogFragment;->Y:Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;->a(Landroid/os/Bundle;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity$ConnectionErrorDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/youtube/p;->E:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/google/android/youtube/p;->D:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/google/android/apps/youtube/app/honeycomb/phone/d;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/d;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity$ConnectionErrorDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/google/android/youtube/p;->dA:I

    new-instance v2, Lcom/google/android/apps/youtube/app/honeycomb/phone/e;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/e;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity$ConnectionErrorDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
