.class final Lcom/google/android/apps/youtube/app/ui/hf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/hd;

.field private final b:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/ui/hd;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/hf;->a:Lcom/google/android/apps/youtube/app/ui/hd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/hf;->b:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/ui/hd;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/hf;-><init>(Lcom/google/android/apps/youtube/app/ui/hd;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to fetch video for the transfer of "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hf;->b:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hf;->b:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/hf;->a:Lcom/google/android/apps/youtube/app/ui/hd;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hf;->b:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    invoke-static {v0, p2, v1}, Lcom/google/android/apps/youtube/app/ui/hd;->a(Lcom/google/android/apps/youtube/app/ui/hd;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/hf;->a:Lcom/google/android/apps/youtube/app/ui/hd;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/hf;->b:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/youtube/app/ui/hd;->a(Lcom/google/android/apps/youtube/app/ui/hd;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    return-void
.end method
