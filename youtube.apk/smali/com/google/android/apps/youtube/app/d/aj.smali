.class public final Lcom/google/android/apps/youtube/app/d/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/go;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

.field private final b:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/TabbedView;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/aj;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/aj;->b:Ljava/util/List;

    invoke-virtual {p1, p0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->setOnTabSelectedListener(Lcom/google/android/apps/youtube/app/ui/go;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/aj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/aj;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a()V

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/aj;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/innertube/t;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/innertube/t;->j()V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/an;Lcom/google/android/apps/youtube/uilib/innertube/t;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/aj;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/aj;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/an;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/t;->k()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Ljava/lang/CharSequence;Landroid/view/View;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/an;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/aj;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/aj;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(I)V

    :cond_0
    return-void
.end method
