.class final Lcom/google/android/apps/youtube/app/adapter/bl;
.super Lcom/google/android/apps/youtube/app/adapter/bk;
.source "SourceFile"


# instance fields
.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/view/View;

.field private final i:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/bk;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->c:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->y:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->f:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->c:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aK:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->c:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->eu:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->c:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aP:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->i:Landroid/widget/TextView;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/bl;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-void
.end method


# virtual methods
.method public final a(ILcom/google/android/apps/youtube/datalib/model/gdata/Video;)Landroid/view/View;
    .locals 9

    const/16 v4, 0x8

    const/4 v8, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->f:Landroid/widget/TextView;

    iget-object v3, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->ownerDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->h:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-eq v0, v3, :cond_1

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PROCESSING:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-ne v0, v3, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isLive()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    move v0, v2

    :goto_1
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->g:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->g:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->uploadedDate:Ljava/util/Date;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->b:Landroid/content/res/Resources;

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/core/utils/ag;->a(Ljava/util/Date;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->g:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->b:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/youtube/o;->a:I

    iget-wide v6, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->viewCount:J

    long-to-int v6, v6

    new-array v7, v8, [Ljava/lang/Object;

    if-eqz v0, :cond_4

    :goto_2
    aput-object v0, v7, v1

    iget-wide v0, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->viewCount:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v7, v2

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->b:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/f;->r:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->b:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/f;->s:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_3
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/bk;->a(ILcom/google/android/apps/youtube/datalib/model/gdata/Video;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    const-string v0, ""

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->g:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->b:Landroid/content/res/Resources;

    iget-object v2, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    iget v2, v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->explanationId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->b:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/f;->s:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/bl;->b:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/f;->r:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_3
.end method

.method public final bridge synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/bl;->a(ILcom/google/android/apps/youtube/datalib/model/gdata/Video;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
