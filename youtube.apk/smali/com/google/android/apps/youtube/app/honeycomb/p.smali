.class public final Lcom/google/android/apps/youtube/app/honeycomb/p;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/res/Resources;Landroid/preference/ListPreference;Landroid/preference/ListPreference;Landroid/preference/ListPreference;Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;Landroid/preference/ListPreference;Landroid/preference/ListPreference;Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;Landroid/preference/ListPreference;Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;Landroid/preference/ListPreference;)V
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->getScaleEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesScale;->getScaleValueStrings()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/p;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->getStyleEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesPresetStyle;->getStyleValueStrings()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p2, v0, v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/p;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->getFontEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesFont;->getFontValueStrings()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {p3, v0, v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/p;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getColorEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getColorValueStrings()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p4, v0, v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/p;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getColorValues()[I

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;->a([I)V

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->getOpacityEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->getOpacityValueStrings()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {p5, v0, v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/p;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->getEdgeTypeEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesEdgeType;->getEdgeTypeValueStrings()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p6, v0, v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/p;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getColorEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getColorValueStrings()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p7, v0, v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/p;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getColorValues()[I

    move-result-object v0

    invoke-virtual {p7, v0}, Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;->a([I)V

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getBackgroundColorEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getBackgroundColorValueStrings()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {p8, v0, v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/p;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getBackgroundColorValues()[I

    move-result-object v0

    invoke-virtual {p8, v0}, Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;->a([I)V

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->getOpacityEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->getOpacityValueStrings()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {p9, v0, v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/p;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getBackgroundColorEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getBackgroundColorValueStrings()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p10, v0, v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/p;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesColor;->getBackgroundColorValues()[I

    move-result-object v0

    invoke-virtual {p10, v0}, Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;->a([I)V

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->getOpacityEntryStrings(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/youtube/core/player/overlay/SubtitlesPreferences$SubtitlesOpacity;->getOpacityValueStrings()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {p11, v0, v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/p;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    return-void
.end method

.method private static a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V
    .locals 1

    invoke-virtual {p0, p1}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p2}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p3}, Landroid/preference/ListPreference;->setValueIndex(I)V

    :cond_0
    const-string v0, "%s"

    invoke-virtual {p0, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method
