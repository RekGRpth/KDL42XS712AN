.class public final Lcom/google/android/apps/youtube/app/ui/presenter/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/d/e;
.implements Lcom/google/android/apps/youtube/uilib/a/g;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

.field private final b:Landroid/view/View;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Lcom/google/android/apps/youtube/app/ui/gr;

.field private final g:Lcom/google/android/apps/youtube/app/ui/gr;

.field private final h:Lcom/google/android/apps/youtube/app/ui/fw;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/datalib/innertube/av;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/identity/as;Lcom/google/android/apps/youtube/datalib/innertube/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;)V
    .locals 12

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lcom/google/android/youtube/l;->l:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->b:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->b:Landroid/view/View;

    sget v3, Lcom/google/android/youtube/j;->aj:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->c:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->b:Landroid/view/View;

    sget v3, Lcom/google/android/youtube/j;->ah:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/gr;

    move-object/from16 v0, p7

    invoke-direct {v3, v0, v2}, Lcom/google/android/apps/youtube/app/ui/gr;-><init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/widget/ImageView;)V

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->f:Lcom/google/android/apps/youtube/app/ui/gr;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->b:Landroid/view/View;

    sget v3, Lcom/google/android/youtube/j;->ai:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/gr;

    move-object/from16 v0, p7

    invoke-direct {v3, v0, v2}, Lcom/google/android/apps/youtube/app/ui/gr;-><init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/widget/ImageView;)V

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->g:Lcom/google/android/apps/youtube/app/ui/gr;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->b:Landroid/view/View;

    sget v3, Lcom/google/android/youtube/j;->ap:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->b:Landroid/view/View;

    sget v3, Lcom/google/android/youtube/j;->ao:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->e:Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/ja;

    new-instance v4, Lcom/google/android/apps/youtube/app/c/a;

    move-object/from16 v0, p6

    move-object/from16 v1, p5

    invoke-direct {v4, p1, v0, p3, v1}, Lcom/google/android/apps/youtube/app/c/a;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/datalib/innertube/bc;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/as;)V

    move-object v3, p1

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/youtube/app/ui/ja;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/c/a;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/aw;)V

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/fw;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->b:Landroid/view/View;

    sget v5, Lcom/google/android/youtube/j;->eZ:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object/from16 v7, p4

    move-object v8, v2

    move-object/from16 v9, p10

    move-object/from16 v10, p9

    invoke-direct/range {v3 .. v11}, Lcom/google/android/apps/youtube/app/ui/fw;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/datalib/innertube/av;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/app/ui/ja;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/c/a;Landroid/view/View;)V

    iput-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->h:Lcom/google/android/apps/youtube/app/ui/fw;

    return-void
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->f:Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/gr;->a()Landroid/widget/ImageView;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/h;->am:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/c;)Landroid/view/View;
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->d:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->d()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->e:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->e()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->b()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->f:Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-virtual {v1, v0, p0}, Lcom/google/android/apps/youtube/app/ui/gr;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;Lcom/google/android/apps/youtube/app/d/e;)V

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->g:Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/gr;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->c:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->c()Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->h()Lcom/google/android/apps/youtube/datalib/innertube/model/d;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/d;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    sget v3, Lcom/google/android/youtube/p;->gn:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->d()Ljava/lang/CharSequence;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    const v4, 0x1040013    # android.R.string.yes

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    const v5, 0x1040009    # android.R.string.no

    invoke-virtual {v4, v5}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/d;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/d;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->h:Lcom/google/android/apps/youtube/app/ui/fw;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/fw;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/am;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->b:Landroid/view/View;

    return-object v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/o;->b()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/o;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/c;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/o;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/c;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/presenter/o;->b()V

    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 0

    return-void
.end method

.method public final a(Landroid/widget/ImageView;)V
    .locals 0

    return-void
.end method
