.class final Lcom/google/android/apps/youtube/app/remote/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/remote/aq;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/remote/ae;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/remote/ae;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/ah;->a:Lcom/google/android/apps/youtube/app/remote/ae;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/remote/ae;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/ah;-><init>(Lcom/google/android/apps/youtube/app/remote/ae;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ah;->a:Lcom/google/android/apps/youtube/app/remote/ae;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/ae;->d(Lcom/google/android/apps/youtube/app/remote/ae;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V
    .locals 3

    sget-object v0, Lcom/google/android/apps/youtube/app/remote/ag;->b:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ah;->a:Lcom/google/android/apps/youtube/app/remote/ae;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/ae;->b(Lcom/google/android/apps/youtube/app/remote/ae;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ah;->a:Lcom/google/android/apps/youtube/app/remote/ae;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/remote/ae;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ah;->a:Lcom/google/android/apps/youtube/app/remote/ae;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/ae;->b(Lcom/google/android/apps/youtube/app/remote/ae;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ah;->a:Lcom/google/android/apps/youtube/app/remote/ae;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/ah;->a:Lcom/google/android/apps/youtube/app/remote/ae;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/ae;->c(Lcom/google/android/apps/youtube/app/remote/ae;)Lcom/google/android/apps/ytremote/model/PairingCode;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/youtube/app/remote/ae;->a(Lcom/google/android/apps/youtube/app/remote/ae;Lcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ah;->a:Lcom/google/android/apps/youtube/app/remote/ae;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/ae;->b(Lcom/google/android/apps/youtube/app/remote/ae;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreen()Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ah;->a:Lcom/google/android/apps/youtube/app/remote/ae;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/ae;->d(Lcom/google/android/apps/youtube/app/remote/ae;)V

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 0

    return-void
.end method

.method public final l_()V
    .locals 0

    return-void
.end method
