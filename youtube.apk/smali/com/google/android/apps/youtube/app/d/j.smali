.class final Lcom/google/android/apps/youtube/app/d/j;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/d/f;

.field private final b:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/d/f;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/d/j;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/d/j;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/j;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/j;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/j;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/j;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/d/j;->a(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const/4 v2, 0x0

    check-cast p2, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/j;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->c(Lcom/google/android/apps/youtube/app/d/f;)Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->v:I

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/d/j;->a(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->setText(I)V

    packed-switch v3, :pswitch_data_1

    move v1, v2

    :goto_2
    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    return-object v0

    :pswitch_0
    sget v1, Lcom/google/android/youtube/p;->fl:I

    goto :goto_1

    :pswitch_1
    sget v1, Lcom/google/android/youtube/p;->fk:I

    goto :goto_1

    :pswitch_2
    sget v1, Lcom/google/android/youtube/p;->aE:I

    goto :goto_1

    :pswitch_3
    sget v1, Lcom/google/android/youtube/h;->z:I

    goto :goto_2

    :pswitch_4
    sget v1, Lcom/google/android/youtube/h;->A:I

    goto :goto_2

    :pswitch_5
    sget v1, Lcom/google/android/youtube/h;->y:I

    goto :goto_2

    :cond_0
    move-object v0, p2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
