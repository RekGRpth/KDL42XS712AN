.class public final Lcom/google/android/apps/youtube/app/ui/presenter/cd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/g;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Lcom/google/android/apps/youtube/app/ui/gr;

.field private final f:Landroid/view/View;

.field private final g:Landroid/view/View;

.field private final h:Landroid/view/View;

.field private final i:Landroid/view/View;

.field private final j:Lcom/google/android/apps/youtube/app/ui/gr;

.field private final k:Landroid/widget/TextView;

.field private final l:Landroid/widget/TextView;

.field private final m:Landroid/widget/LinearLayout;

.field private final n:Lcom/google/android/apps/youtube/app/ui/v;

.field private final o:Landroid/content/Context;

.field private final p:Landroid/text/SpannableStringBuilder;

.field private final q:Landroid/text/style/StyleSpan;

.field private final r:Lcom/google/android/apps/youtube/app/ui/a;

.field private final s:Landroid/content/res/Resources;

.field private final t:Landroid/view/View$OnClickListener;

.field private final u:Landroid/view/View$OnClickListener;

.field private v:Lcom/google/android/apps/youtube/datalib/innertube/model/ah;

.field private w:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->o:Landroid/content/Context;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/v;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->n:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->r:Lcom/google/android/apps/youtube/app/ui/a;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->s:Landroid/content/res/Resources;

    sget v0, Lcom/google/android/youtube/l;->aP:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->bo:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aP:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-direct {v1, p2, v0}, Lcom/google/android/apps/youtube/app/ui/gr;-><init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->e:Lcom/google/android/apps/youtube/app/ui/gr;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fA:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->g:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->eu:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aU:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aE:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->i:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->ah:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-direct {v1, p2, v0}, Lcom/google/android/apps/youtube/app/ui/gr;-><init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->j:Lcom/google/android/apps/youtube/app/ui/gr;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->g:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->d:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->l:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->bn:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->m:Landroid/widget/LinearLayout;

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->p:Landroid/text/SpannableStringBuilder;

    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->q:Landroid/text/style/StyleSpan;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/ce;

    invoke-direct {v0, p0, p5, p3}, Lcom/google/android/apps/youtube/app/ui/presenter/ce;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/cd;Lcom/google/android/apps/youtube/app/ui/a;Lcom/google/android/apps/youtube/datalib/d/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->t:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/cf;

    invoke-direct {v0, p0, p3}, Lcom/google/android/apps/youtube/app/ui/presenter/cf;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/cd;Lcom/google/android/apps/youtube/datalib/d/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->u:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/presenter/cd;)Lcom/google/android/apps/youtube/datalib/innertube/model/ah;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ah;

    return-object v0
.end method

.method private static a(Landroid/view/View;I)V
    .locals 0

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 11

    const/4 v3, 0x2

    const/4 v10, -0x1

    const/4 v7, 0x0

    const/16 v9, 0x8

    const/4 v8, 0x0

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ah;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->r:Lcom/google/android/apps/youtube/app/ui/a;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->m()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/a;->a(Ljava/lang/String;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->o:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/l;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v2, v3

    :goto_0
    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->w:I

    if-eq v2, v0, :cond_0

    iput v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->w:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->h:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->m:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->g:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->s:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/youtube/g;->K:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->g:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->s:Landroid/content/res/Resources;

    sget v6, Lcom/google/android/youtube/g;->L:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->s:Landroid/content/res/Resources;

    sget v7, Lcom/google/android/youtube/g;->J:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->setPadding(IIII)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->h:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->h:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->h:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->h:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v2, v3, v8, v4, v5}, Landroid/view/View;->setPadding(IIII)V

    iput v8, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->s:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/youtube/k;->l:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iput v8, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->s:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/k;->k:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->f:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->b:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->d()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->k:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->d()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->i()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->c:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->i()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_3
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->f()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->p:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->p:Landroid/text/SpannableStringBuilder;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->g()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->p:Landroid/text/SpannableStringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->q:Landroid/text/style/StyleSpan;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->p:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v8, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->p:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ah;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->j()Lcom/google/a/a/a/a/kz;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ah;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->k()Lcom/google/a/a/a/a/vb;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->l:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->l:Landroid/widget/TextView;

    sget v1, Lcom/google/android/youtube/h;->c:I

    invoke-virtual {v0, v8, v8, v1, v8}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    :cond_1
    :goto_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->u:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->l:Landroid/widget/TextView;

    invoke-static {v0, v8}, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a(Landroid/view/View;I)V

    :goto_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->e:Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->b()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/gr;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->j:Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->h()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/gr;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->i:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->n:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->i:Landroid/view/View;

    invoke-static {v0, v1, p2}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/ui/v;Landroid/view/View;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->s:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/youtube/g;->P:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a:Landroid/view/View;

    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->s:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move v2, v0

    goto/16 :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->m:Landroid/widget/LinearLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->g:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->g:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-virtual {v2, v8, v3, v8, v8}, Landroid/view/View;->setPadding(IIII)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->h:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->h:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->s:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/youtube/g;->I:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->h:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->h:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->setPadding(IIII)V

    iput v10, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iput v10, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    iput v7, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->f:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ah;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->l()Lcom/google/a/a/a/a/w;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->l:Landroid/widget/TextView;

    sget v1, Lcom/google/android/youtube/p;->cj:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v8, v8, v8, v8}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    goto/16 :goto_5

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->l:Landroid/widget/TextView;

    invoke-static {v0, v9}, Lcom/google/android/apps/youtube/app/ui/presenter/cd;->a(Landroid/view/View;I)V

    goto/16 :goto_6
.end method
