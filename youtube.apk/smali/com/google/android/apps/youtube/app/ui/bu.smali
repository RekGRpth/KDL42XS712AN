.class public abstract Lcom/google/android/apps/youtube/app/ui/bu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/am;

.field private b:Ljava/lang/String;

.field protected final f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

.field protected final g:Lcom/google/android/apps/youtube/core/Analytics;

.field protected final h:Lcom/google/android/apps/youtube/app/compat/o;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/am;Ljava/lang/String;Lcom/google/android/apps/youtube/app/compat/o;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/bu;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/bu;->a:Lcom/google/android/apps/youtube/app/am;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->D()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bu;->g:Lcom/google/android/apps/youtube/core/Analytics;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/bu;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/ui/bu;->h:Lcom/google/android/apps/youtube/app/compat/o;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/bu;->b:Ljava/lang/String;

    return-void
.end method

.method public a(Lcom/google/android/apps/youtube/app/compat/j;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bu;->h:Lcom/google/android/apps/youtube/app/compat/o;

    sget v1, Lcom/google/android/youtube/m;->c:I

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/youtube/app/compat/o;->a(ILcom/google/android/apps/youtube/app/compat/j;)V

    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/q;)Z
    .locals 5

    const/4 v1, 0x1

    invoke-interface {p1}, Lcom/google/android/apps/youtube/app/compat/q;->e()I

    move-result v0

    sget v2, Lcom/google/android/youtube/j;->cx:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bu;->a:Lcom/google/android/apps/youtube/app/am;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/am;->c()V

    move v0, v1

    :goto_0
    return v0

    :cond_0
    sget v2, Lcom/google/android/youtube/j;->cy:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bu;->g:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v2, "Search"

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/bu;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_1
    sget v2, Lcom/google/android/youtube/j;->cB:I

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bu;->g:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v2, "UploadFromMenu"

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/bu;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bu;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/m;->a(Landroid/app/Activity;)V

    move v0, v1

    goto :goto_0

    :cond_2
    sget v2, Lcom/google/android/youtube/j;->cz:I

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bu;->a:Lcom/google/android/apps/youtube/app/am;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/am;->e()V

    move v0, v1

    goto :goto_0

    :cond_3
    sget v2, Lcom/google/android/youtube/j;->cw:I

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bu;->b:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bu;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    sget v2, Lcom/google/android/youtube/p;->gw:I

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/bu;->b:Ljava/lang/String;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/youtube/core/utils/m;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/bu;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/core/utils/m;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bu;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    sget v2, Lcom/google/android/youtube/p;->gy:I

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/core/utils/m;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    sget v2, Lcom/google/android/youtube/j;->cv:I

    if-ne v0, v2, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bu;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bu;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/e;->a(Landroid/app/Activity;)V

    :goto_2
    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bu;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/bu;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/bu;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/youtube/p;->gx:I

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/youtube/core/utils/m;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/core/utils/m;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    goto :goto_2

    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
