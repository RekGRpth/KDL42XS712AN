.class final Lcom/google/android/apps/youtube/app/ui/presenter/bw;
.super Lcom/google/android/apps/youtube/app/d/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/presenter/bk;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bw;->a:Lcom/google/android/apps/youtube/app/ui/presenter/bk;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/d/b;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/ui/presenter/bk;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/presenter/bw;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bw;->a:Lcom/google/android/apps/youtube/app/ui/presenter/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->i(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bw;->a:Lcom/google/android/apps/youtube/app/ui/presenter/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->h(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    move-result-object v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, p1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public final a(Landroid/widget/ImageView;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/d/b;->a(Landroid/widget/ImageView;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bw;->a:Lcom/google/android/apps/youtube/app/ui/presenter/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->j(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
