.class public final enum Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

.field public static final enum DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

.field public static final enum MAXIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

.field public static final enum MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;


# instance fields
.field public final intValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    const-string v1, "DISMISSED"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    const-string v1, "MAXIMIZED"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MAXIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    const-string v1, "MINIMIZED"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MAXIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->$VALUES:[Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->intValue:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->$VALUES:[Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    return-object v0
.end method
