.class public Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/youtube/app/ui/r;


# instance fields
.field private a:Lcom/google/android/apps/youtube/app/ui/ClingView;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/TextView;

.field private d:Lcom/google/android/apps/youtube/app/ui/iu;

.field private e:Landroid/view/View;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->h:Landroid/graphics/Rect;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;I)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->setVisibility(I)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->d:Lcom/google/android/apps/youtube/app/ui/iu;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/iu;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->d:Lcom/google/android/apps/youtube/app/ui/iu;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/iu;->b()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->h:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->requestLayout()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->b:Landroid/view/View;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->a(Z)V

    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    sget v0, Lcom/google/android/youtube/j;->cV:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->b:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->av:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/ClingView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->a:Lcom/google/android/apps/youtube/app/ui/ClingView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->a:Lcom/google/android/apps/youtube/app/ui/ClingView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/ui/ClingView;->setHighlightBoundsListener(Lcom/google/android/apps/youtube/app/ui/r;)V

    sget v0, Lcom/google/android/youtube/j;->fw:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->c:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->e:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->aZ:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->f:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/youtube/j;->aN:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->g:Landroid/widget/ImageView;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->h:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->h:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->f:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->h:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->f:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->f:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->f:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->f:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->h:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->g:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->h:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->h:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->g:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->g:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->g:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->g:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/widget/ImageView;->layout(IIII)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->a:Lcom/google/android/apps/youtube/app/ui/ClingView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/ClingView;->a()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->a(Z)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setDismissListener(Lcom/google/android/apps/youtube/app/ui/iu;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->d:Lcom/google/android/apps/youtube/app/ui/iu;

    return-void
.end method

.method public setTargetView(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->a:Lcom/google/android/apps/youtube/app/ui/ClingView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/ClingView;->setViewToCling(Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->postInvalidate()V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setVisibility(I)V
    .locals 6

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->getVisibility()I

    move-result v0

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    new-instance v5, Landroid/view/animation/AlphaAnimation;

    if-eqz v0, :cond_3

    move v4, v3

    :goto_2
    if-eqz v0, :cond_4

    :goto_3
    invoke-direct {v5, v4, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    if-eqz v0, :cond_5

    const-wide/16 v2, 0x3e8

    :goto_4
    invoke-virtual {v5, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/it;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/youtube/app/ui/it;-><init>(Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;I)V

    invoke-virtual {v5, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    if-eqz v0, :cond_1

    invoke-super {p0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :cond_1
    invoke-virtual {p0, v5}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v4, v2

    goto :goto_2

    :cond_4
    move v2, v3

    goto :goto_3

    :cond_5
    const-wide/16 v2, 0x1f4

    goto :goto_4
.end method
