.class final Lcom/google/android/apps/youtube/app/adapter/bm;
.super Lcom/google/android/apps/youtube/app/adapter/bk;
.source "SourceFile"


# instance fields
.field private final f:Landroid/widget/TextView;

.field private final g:I


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;I)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/bk;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bm;->c:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->z:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bm;->f:Landroid/widget/TextView;

    iput p4, p0, Lcom/google/android/apps/youtube/app/adapter/bm;->g:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;IB)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/app/adapter/bm;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;I)V

    return-void
.end method


# virtual methods
.method public final a(ILcom/google/android/apps/youtube/datalib/model/gdata/Video;)Landroid/view/View;
    .locals 8

    iget v0, p0, Lcom/google/android/apps/youtube/app/adapter/bm;->g:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bm;->d:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/apps/youtube/app/adapter/bm;->g:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLines(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/bm;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/bm;->b:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/o;->b:I

    iget-wide v3, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->viewCount:J

    long-to-int v3, v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->ownerDisplayName:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-wide v6, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->viewCount:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/bk;->a(ILcom/google/android/apps/youtube/datalib/model/gdata/Video;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/bm;->a(ILcom/google/android/apps/youtube/datalib/model/gdata/Video;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
