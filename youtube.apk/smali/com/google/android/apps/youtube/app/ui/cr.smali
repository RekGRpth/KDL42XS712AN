.class public final Lcom/google/android/apps/youtube/app/ui/cr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

.field private final b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

.field private final c:Lcom/google/android/apps/youtube/core/client/bc;

.field private final d:Lcom/google/android/apps/youtube/core/client/bj;

.field private final e:Lcom/google/android/apps/youtube/app/ui/db;

.field private final f:Landroid/widget/ImageView;

.field private final g:Landroid/widget/ImageView;

.field private final h:Landroid/view/View;

.field private final i:Landroid/widget/TextView;

.field private final j:Landroid/widget/TextView;

.field private final k:Landroid/widget/TextView;

.field private final l:Landroid/view/View;

.field private final m:Landroid/widget/ImageView;

.field private final n:Landroid/widget/ImageView;

.field private final o:Landroid/widget/ImageView;

.field private final p:Lcom/google/android/apps/youtube/app/ui/presenter/as;

.field private final q:Lcom/google/android/apps/youtube/app/ui/cl;

.field private final r:Lcom/google/android/apps/youtube/app/offline/f;

.field private s:Ljava/lang/String;

.field private t:Landroid/net/Uri;

.field private u:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

.field private v:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

.field private w:Z

.field private x:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/datalib/innertube/v;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/cl;Lcom/google/android/apps/youtube/app/offline/f;ILandroid/view/ViewGroup;Lcom/google/android/apps/youtube/app/ui/db;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->c:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->d:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/cl;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->q:Lcom/google/android/apps/youtube/app/ui/cl;

    iput-object p7, p0, Lcom/google/android/apps/youtube/app/ui/cr;->r:Lcom/google/android/apps/youtube/app/offline/f;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/db;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->e:Lcom/google/android/apps/youtube/app/ui/db;

    sget v0, Lcom/google/android/youtube/j;->dE:I

    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->f:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/youtube/j;->fA:I

    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->h:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->h:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->g:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/youtube/j;->dH:I

    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->i:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->dD:I

    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->j:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->dG:I

    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->k:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->dm:I

    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->l:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->bK:I

    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->m:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/youtube/j;->ce:I

    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->n:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/youtube/j;->eI:I

    invoke-virtual {p9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->o:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->l:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/cs;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/ui/cs;-><init>(Lcom/google/android/apps/youtube/app/ui/cr;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->m:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/ct;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/ui/ct;-><init>(Lcom/google/android/apps/youtube/app/ui/cr;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->n:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/cu;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/ui/cu;-><init>(Lcom/google/android/apps/youtube/app/ui/cr;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->o:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/cv;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/ui/cv;-><init>(Lcom/google/android/apps/youtube/app/ui/cr;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/ui/cr;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->r:Lcom/google/android/apps/youtube/app/offline/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/cw;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/cw;-><init>(Lcom/google/android/apps/youtube/app/ui/cr;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/as;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/cx;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/youtube/app/ui/cx;-><init>(Lcom/google/android/apps/youtube/app/ui/cr;Lcom/google/android/apps/youtube/app/offline/j;)V

    invoke-direct {v1, p9, v2}, Lcom/google/android/apps/youtube/app/ui/presenter/as;-><init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cr;->p:Lcom/google/android/apps/youtube/app/ui/presenter/as;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->p:Lcom/google/android/apps/youtube/app/ui/presenter/as;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/cr;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/cr;->t:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/cr;)Lcom/google/android/apps/youtube/app/ui/db;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->e:Lcom/google/android/apps/youtube/app/ui/db;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/cr;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/cr;->v:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    return-object p1
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V
    .locals 8

    const/4 v7, 0x1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/cr;->u:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->i:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->j:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->author:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->k:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cr;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/o;->j:I

    iget v3, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->hqThumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->hqThumbnailUri:Landroid/net/Uri;

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cr;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/cr;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/cz;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/cr;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/cr;->g:Landroid/widget/ImageView;

    invoke-direct {v3, p0, v4, v5}, Lcom/google/android/apps/youtube/app/ui/cz;-><init>(Lcom/google/android/apps/youtube/app/ui/cr;Landroid/content/Context;Landroid/widget/ImageView;)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setEnabled(Z)V

    return-void

    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->thumbnailUri:Landroid/net/Uri;

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->p:Lcom/google/android/apps/youtube/app/ui/presenter/as;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->p:Lcom/google/android/apps/youtube/app/ui/presenter/as;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/presenter/as;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;)Landroid/view/View;

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/cr;->x:Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cr;->n:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/youtube/h;->ac:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void

    :cond_0
    sget v0, Lcom/google/android/youtube/h;->ab:I

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/cr;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->u:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/cr;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/cr;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/cr;)Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/cr;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->v:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/ui/cr;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->x:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/ui/cr;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->s:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/ui/cr;)Lcom/google/android/apps/youtube/app/ui/cl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->q:Lcom/google/android/apps/youtube/app/ui/cl;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/app/ui/cr;)Lcom/google/android/apps/youtube/app/ui/presenter/as;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->p:Lcom/google/android/apps/youtube/app/ui/presenter/as;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/app/ui/cr;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/app/ui/cr;)Lcom/google/android/apps/youtube/app/offline/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->r:Lcom/google/android/apps/youtube/app/offline/f;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/apps/youtube/app/ui/cr;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->t:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/apps/youtube/app/ui/cr;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->n:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/apps/youtube/app/ui/cr;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->w:Z

    return v0
.end method

.method static synthetic n(Lcom/google/android/apps/youtube/app/ui/cr;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->f:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/apps/youtube/app/ui/cr;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->h:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/cr;->s:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->w:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->b:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/cr;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/cr;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/da;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/app/ui/da;-><init>(Lcom/google/android/apps/youtube/app/ui/cr;B)V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cr;->c:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v1, p1, v0}, Lcom/google/android/apps/youtube/core/client/bc;->d(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method public final handleOfflinePlaylistAddEvent(Lcom/google/android/apps/youtube/app/offline/a/q;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/q;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/cr;->s:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/cr;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;)V

    :cond_0
    return-void
.end method

.method public final handleOfflinePlaylistAddFailedEvent(Lcom/google/android/apps/youtube/app/offline/a/r;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/r;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cr;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/cr;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;)V

    :cond_0
    return-void
.end method

.method public final handleOfflinePlaylistDeleteEvent(Lcom/google/android/apps/youtube/app/offline/a/s;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/s;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cr;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/cr;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;)V

    :cond_0
    return-void
.end method

.method public final handleOfflinePlaylistProgressEvent(Lcom/google/android/apps/youtube/app/offline/a/t;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/t;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/cr;->s:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/cr;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;)V

    :cond_0
    return-void
.end method

.method public final handleOfflinePlaylistSyncEvent(Lcom/google/android/apps/youtube/app/offline/a/u;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/u;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/cr;->w:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/cr;->s:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/cr;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V

    :cond_0
    return-void
.end method

.method public final handlePlaylistLikeActionEvent(Lcom/google/android/apps/youtube/app/ui/de;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->u:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cr;->u:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ui/de;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ui/de;->b()Lcom/google/android/apps/youtube/app/ui/LikeAction;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/LikeAction;->LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/cr;->a(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
