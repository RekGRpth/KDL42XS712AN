.class public final Lcom/google/android/apps/youtube/app/honeycomb/Shell;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Z

.field private static b:Z

.field private static c:Z

.field private static d:J

.field private static e:J


# direct methods
.method static synthetic a()J
    .locals 2

    sget-wide v0, Lcom/google/android/apps/youtube/app/honeycomb/Shell;->d:J

    return-wide v0
.end method

.method static synthetic a(J)J
    .locals 2

    const-wide v0, 0x7fffffffffffffffL

    sput-wide v0, Lcom/google/android/apps/youtube/app/honeycomb/Shell;->d:J

    return-wide v0
.end method

.method public static a(Lcom/google/android/apps/youtube/app/YouTubeApplication;Lcom/google/android/apps/youtube/app/aw;Landroid/content/SharedPreferences;)V
    .locals 10

    const/4 v1, 0x0

    const/4 v2, 0x1

    sget-boolean v0, Lcom/google/android/apps/youtube/app/honeycomb/Shell;->a:Z

    if-nez v0, :cond_3

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->g(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/aw;->a()I

    move-result v4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/aw;->b()I

    move-result v5

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/aw;->c()Landroid/util/SparseBooleanArray;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/aw;->d()Landroid/util/SparseBooleanArray;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/aw;->e()J

    move-result-wide v7

    sput-wide v7, Lcom/google/android/apps/youtube/app/honeycomb/Shell;->e:J

    const-string v7, "upgrade_prompt_shown_millis"

    const-wide/16 v8, 0x0

    invoke-interface {p2, v7, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v7

    sput-wide v7, Lcom/google/android/apps/youtube/app/honeycomb/Shell;->d:J

    if-lt v3, v4, :cond_0

    invoke-virtual {v0, v3}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/youtube/app/honeycomb/Shell;->b:Z

    if-lt v3, v5, :cond_1

    invoke-virtual {v6, v3}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    sput-boolean v1, Lcom/google/android/apps/youtube/app/honeycomb/Shell;->c:Z

    sput-boolean v2, Lcom/google/android/apps/youtube/app/honeycomb/Shell;->a:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "App version = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Min app version = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Target app version = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Prompt shown ago = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    sget-wide v3, Lcom/google/android/apps/youtube/app/honeycomb/Shell;->d:J

    sub-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method static synthetic b()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/youtube/app/honeycomb/Shell;->b:Z

    return v0
.end method

.method static synthetic c()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/youtube/app/honeycomb/Shell;->c:Z

    return v0
.end method

.method static synthetic d()J
    .locals 2

    sget-wide v0, Lcom/google/android/apps/youtube/app/honeycomb/Shell;->e:J

    return-wide v0
.end method
