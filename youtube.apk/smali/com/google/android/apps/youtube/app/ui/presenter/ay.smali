.class public final Lcom/google/android/apps/youtube/app/ui/presenter/ay;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/youtube/uilib/a/g;


# instance fields
.field private A:Landroid/widget/ImageView;

.field protected final a:Landroid/view/View;

.field protected final b:Landroid/widget/TextView;

.field protected final c:Landroid/widget/TextView;

.field protected final d:Landroid/widget/TextView;

.field protected final e:Landroid/widget/TextView;

.field protected final f:Landroid/view/View;

.field protected final g:Landroid/view/View;

.field protected final h:Landroid/widget/ImageView;

.field protected final i:Lcom/google/android/apps/youtube/app/ui/presenter/az;

.field protected final j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

.field protected final k:Landroid/view/View;

.field private final l:Landroid/content/Context;

.field private final m:Landroid/content/res/Resources;

.field private final n:Lcom/google/android/apps/youtube/common/network/h;

.field private final o:Lcom/google/android/apps/youtube/app/am;

.field private final p:Lcom/google/android/apps/youtube/core/player/w;

.field private final q:Lcom/google/android/apps/youtube/app/ui/hh;

.field private final r:Lcom/google/android/apps/youtube/app/offline/r;

.field private final s:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

.field private final t:Lcom/google/android/apps/youtube/core/client/bj;

.field private final u:Lcom/google/android/apps/youtube/app/offline/p;

.field private final v:Lcom/google/android/apps/youtube/uilib/a/i;

.field private final w:Ljava/lang/String;

.field private x:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

.field private y:I

.field private z:Lcom/google/android/apps/youtube/app/ui/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/uilib/a/i;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/app/ui/hh;Lcom/google/android/apps/youtube/app/offline/r;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/offline/p;Ljava/lang/String;Lcom/google/android/apps/youtube/app/ui/v;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->l:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/a/i;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->v:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->m:Landroid/content/res/Resources;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->n:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/am;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->o:Lcom/google/android/apps/youtube/app/am;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/w;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->p:Lcom/google/android/apps/youtube/core/player/w;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/hh;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->q:Lcom/google/android/apps/youtube/app/ui/hh;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/r;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->r:Lcom/google/android/apps/youtube/app/offline/r;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->s:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->t:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/p;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->u:Lcom/google/android/apps/youtube/app/offline/p;

    iput-object p11, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->w:Ljava/lang/String;

    invoke-static {p12}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/v;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->z:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->ax:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->b:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aP:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->y:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aK:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fB:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->f:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fA:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->g:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->g:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->h:Landroid/widget/ImageView;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/az;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/presenter/az;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/ay;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->i:Lcom/google/android/apps/youtube/app/ui/presenter/az;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->cQ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->aE:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->k:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->a:Landroid/view/View;

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Landroid/view/View;)V

    invoke-interface {p2, p0}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/presenter/ay;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->w:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)V
    .locals 10

    const v3, 0x3e4ccccd    # 0.2f

    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->A:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->A:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->x:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-eq v0, v2, :cond_1

    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->t()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->x:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->m:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/youtube/f;->s:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->m:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/youtube/f;->r:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->setProgressVisible(Z)V

    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    sget v2, Lcom/google/android/youtube/p;->dr:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    sget v1, Lcom/google/android/youtube/h;->aa:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->setIcon(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->l:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->u()Z

    move-result v0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    sget v1, Lcom/google/android/youtube/h;->W:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->setIcon(I)V

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->v()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->x:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->h:Landroid/widget/ImageView;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->b:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->m:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/youtube/f;->r:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v0, v3, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->m:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/youtube/f;->s:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->uploadedDate:Ljava/util/Date;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->m:Landroid/content/res/Resources;

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/core/utils/ag;->a(Ljava/util/Date;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->m:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/youtube/o;->a:I

    iget-wide v6, v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->viewCount:J

    long-to-int v6, v6

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    if-eqz v0, :cond_6

    :goto_2
    aput-object v0, v7, v8

    iget-wide v8, v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->viewCount:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v7, v1

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_6
    const-string v0, ""

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->x:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->m:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/youtube/f;->s:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->m:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/f;->r:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->o()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->setProgress(I)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->m()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->l:Landroid/content/Context;

    sget v5, Lcom/google/android/youtube/p;->dp:I

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {v4, v5, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    sget v2, Lcom/google/android/youtube/h;->X:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->setIcon(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v1, v8}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->setProgressVisible(Z)V

    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->n:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    sget v2, Lcom/google/android/youtube/p;->dy:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->c()V

    goto :goto_3

    :cond_9
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->u:Lcom/google/android/apps/youtube/app/offline/p;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/offline/p;->d()Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->n:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/common/network/h;->c()Z

    move-result v3

    if-nez v3, :cond_a

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    sget v2, Lcom/google/android/youtube/p;->dz:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->c()V

    goto :goto_3

    :cond_a
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->l()Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->l:Landroid/content/Context;

    sget v5, Lcom/google/android/youtube/p;->dx:I

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {v4, v5, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->c()V

    goto :goto_3

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->l:Landroid/content/Context;

    sget v4, Lcom/google/android/youtube/p;->df:I

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {v3, v4, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->m:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/youtube/f;->i:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->a()V

    goto/16 :goto_3
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/presenter/ay;)Lcom/google/android/apps/youtube/app/offline/r;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->r:Lcom/google/android/apps/youtube/app/offline/r;

    return-object v0
.end method

.method private handleConnectivityChangedEvent(Lcom/google/android/apps/youtube/common/network/a;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->s:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->x:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)V

    :cond_0
    return-void
.end method

.method private handleOfflineVideoCompleteEvent(Lcom/google/android/apps/youtube/app/offline/a/ac;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->x:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/youtube/app/offline/a/ac;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/ac;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)V

    :cond_0
    return-void
.end method

.method private handleOfflineVideoStatusUpdateEvent(Lcom/google/android/apps/youtube/app/offline/a/ae;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->x:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/youtube/app/offline/a/ae;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/ae;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 8

    const/16 v7, 0x8

    const/4 v6, 0x2

    const/4 v1, 0x0

    check-cast p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    sget v3, Lcom/google/android/youtube/k;->g:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->x:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/uilib/a/f;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->y:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->b:Landroid/widget/TextView;

    iget-object v2, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->c:Landroid/widget/TextView;

    iget v2, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->duration:I

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/e/m;->a(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->d:Landroid/widget/TextView;

    iget-object v2, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->ownerDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->l:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->t:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v3, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->hqThumbnailUri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->h:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->i:Lcom/google/android/apps/youtube/app/ui/presenter/az;

    invoke-static {v0, v2, v3, v4, v5}, Lcom/google/android/apps/youtube/app/d/a;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Landroid/net/Uri;Landroid/widget/ImageView;Lcom/google/android/apps/youtube/app/d/e;)V

    iget-object v2, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->s:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/x;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->z:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->k:Landroid/view/View;

    invoke-static {v0, v1, p2}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/ui/v;Landroid/view/View;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->v:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Lcom/google/android/apps/youtube/uilib/a/f;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->h:Landroid/widget/ImageView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0, v7}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->A:Landroid/widget/ImageView;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->g:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->fN:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->A:Landroid/widget/ImageView;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->A:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->m:Landroid/content/res/Resources;

    iget-object v2, p2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    iget v2, v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->explanationId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->m:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/f;->s:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->m:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/f;->r:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->x:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->x:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->x:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->s:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->t()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->q()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->g()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->p:Lcom/google/android/apps/youtube/core/player/w;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/presenter/bg;

    invoke-direct {v3, p0, v0, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/bg;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/ay;Ljava/lang/String;B)V

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/apps/youtube/core/player/w;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/w;Lcom/google/android/apps/youtube/core/player/y;Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->u()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->r:Lcom/google/android/apps/youtube/app/offline/r;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->w:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/apps/youtube/app/offline/r;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/v;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f()Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->f()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->q:Lcom/google/android/apps/youtube/app/ui/hh;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/hh;->a()V

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->b()Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->j()Lcom/google/a/a/a/a/lz;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->q:Lcom/google/android/apps/youtube/app/ui/hh;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/hh;->a(Lcom/google/a/a/a/a/lz;)V

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->v()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->w:Ljava/lang/String;

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->o:Lcom/google/android/apps/youtube/app/am;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/app/am;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->o:Lcom/google/android/apps/youtube/app/am;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->w:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->y:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/app/am;->a(Ljava/lang/String;I)V

    goto :goto_0

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->r:Lcom/google/android/apps/youtube/app/offline/r;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ay;->w:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/apps/youtube/app/offline/r;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/v;)V

    goto/16 :goto_0
.end method
