.class public final Lcom/google/android/apps/youtube/app/adapter/av;
.super Lcom/google/android/apps/youtube/app/adapter/a;
.source "SourceFile"


# instance fields
.field private final a:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;)V
    .locals 2

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/a;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/h;->aI:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/av;->a:Landroid/graphics/Bitmap;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->subjectUri:Landroid/net/Uri;

    return-object v0
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_RECOMMENDED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/av;->a:Landroid/graphics/Bitmap;

    invoke-interface {p3, v0, v1}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/a;->a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method
