.class public final Lcom/google/android/apps/youtube/app/ui/cl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/core/Analytics;

.field private final c:Lcom/google/android/apps/youtube/core/identity/l;

.field private final d:Lcom/google/android/apps/youtube/core/identity/aa;

.field private final e:Lcom/google/android/apps/youtube/core/aw;

.field private final f:Lcom/google/android/apps/youtube/datalib/innertube/v;

.field private final g:Lcom/google/android/apps/youtube/common/c/a;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/datalib/innertube/v;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/c/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cl;->a:Landroid/app/Activity;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/v;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cl;->f:Lcom/google/android/apps/youtube/datalib/innertube/v;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cl;->b:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cl;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/aa;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cl;->d:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cl;->e:Lcom/google/android/apps/youtube/core/aw;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cl;->g:Lcom/google/android/apps/youtube/common/c/a;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/cl;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cl;->e:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method

.method private a(Lcom/google/android/apps/youtube/app/ui/LikeAction;Ljava/lang/String;[B)V
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/cn;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/cn;-><init>(Lcom/google/android/apps/youtube/app/ui/cl;Lcom/google/android/apps/youtube/app/ui/LikeAction;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/co;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ui/LikeAction;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cl;->f:Lcom/google/android/apps/youtube/datalib/innertube/v;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/v;->a()Lcom/google/android/apps/youtube/datalib/innertube/z;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/apps/youtube/datalib/innertube/z;->a([B)V

    invoke-virtual {v1, p2}, Lcom/google/android/apps/youtube/datalib/innertube/z;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/x;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/cl;->f:Lcom/google/android/apps/youtube/datalib/innertube/v;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/v;->a(Lcom/google/android/apps/youtube/datalib/innertube/z;Lcom/google/android/apps/youtube/datalib/a/l;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cl;->f:Lcom/google/android/apps/youtube/datalib/innertube/v;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/v;->b()Lcom/google/android/apps/youtube/datalib/innertube/y;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/apps/youtube/datalib/innertube/y;->a([B)V

    invoke-virtual {v1, p2}, Lcom/google/android/apps/youtube/datalib/innertube/y;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/x;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/cl;->f:Lcom/google/android/apps/youtube/datalib/innertube/v;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/v;->a(Lcom/google/android/apps/youtube/datalib/innertube/y;Lcom/google/android/apps/youtube/datalib/a/l;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cl;->f:Lcom/google/android/apps/youtube/datalib/innertube/v;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/v;->c()Lcom/google/android/apps/youtube/datalib/innertube/aa;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/apps/youtube/datalib/innertube/aa;->a([B)V

    invoke-virtual {v1, p2}, Lcom/google/android/apps/youtube/datalib/innertube/aa;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/x;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/cl;->f:Lcom/google/android/apps/youtube/datalib/innertube/v;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/v;->a(Lcom/google/android/apps/youtube/datalib/innertube/aa;Lcom/google/android/apps/youtube/datalib/a/l;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/cl;Lcom/google/android/apps/youtube/app/ui/LikeAction;Ljava/lang/String;[B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/ui/cl;->a(Lcom/google/android/apps/youtube/app/ui/LikeAction;Ljava/lang/String;[B)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/cl;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cl;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/cl;)Lcom/google/android/apps/youtube/common/c/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cl;->g:Lcom/google/android/apps/youtube/common/c/a;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/app/ui/LikeAction;Ljava/lang/String;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cl;->b:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ui/LikeAction;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cl;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/z;->a:[B

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/youtube/app/ui/cl;->a(Lcom/google/android/apps/youtube/app/ui/LikeAction;Ljava/lang/String;[B)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cl;->d:Lcom/google/android/apps/youtube/core/identity/aa;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/cl;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/cm;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/cm;-><init>(Lcom/google/android/apps/youtube/app/ui/cl;Lcom/google/android/apps/youtube/app/ui/LikeAction;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ah;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cl;->b:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v1, "Share Playlist"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/cl;->a:Landroid/app/Activity;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/youtube/core/utils/m;->b(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
