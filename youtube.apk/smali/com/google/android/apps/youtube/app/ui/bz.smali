.class public final Lcom/google/android/apps/youtube/app/ui/bz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/core/client/bc;

.field private final c:Lcom/google/android/apps/youtube/datalib/innertube/v;

.field private final d:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

.field private final e:Lcom/google/android/apps/youtube/common/c/a;

.field private final f:Lcom/google/android/apps/youtube/app/am;

.field private final g:Lcom/google/android/apps/youtube/core/client/bj;

.field private final h:Lcom/google/android/apps/youtube/common/network/h;

.field private final i:Lcom/google/android/apps/youtube/app/offline/p;

.field private final j:Lcom/google/android/apps/youtube/core/player/w;

.field private final k:Ljava/lang/String;

.field private final l:Lcom/google/android/apps/youtube/app/ui/cl;

.field private final m:Lcom/google/android/apps/youtube/app/offline/r;

.field private final n:Lcom/google/android/apps/youtube/app/offline/f;

.field private final o:Lcom/google/android/apps/youtube/app/ui/hh;

.field private p:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field private q:Landroid/widget/ListView;

.field private r:Lcom/google/android/apps/youtube/uilib/a/h;

.field private s:Lcom/google/android/apps/youtube/common/a/d;

.field private final t:Ljava/util/Set;

.field private u:Lcom/google/android/apps/youtube/app/ui/cr;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/datalib/innertube/v;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/app/ui/cl;Lcom/google/android/apps/youtube/app/offline/r;Lcom/google/android/apps/youtube/app/offline/f;Lcom/google/android/apps/youtube/app/ui/hh;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->a:Landroid/app/Activity;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->b:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/v;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->c:Lcom/google/android/apps/youtube/datalib/innertube/v;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/am;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->f:Lcom/google/android/apps/youtube/app/am;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->d:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->e:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->g:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->h:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/p;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->i:Lcom/google/android/apps/youtube/app/offline/p;

    invoke-static {p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/w;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->j:Lcom/google/android/apps/youtube/core/player/w;

    invoke-static {p11}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/cl;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->l:Lcom/google/android/apps/youtube/app/ui/cl;

    invoke-static {p12}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/r;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->m:Lcom/google/android/apps/youtube/app/offline/r;

    invoke-static {p13}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/f;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->n:Lcom/google/android/apps/youtube/app/offline/f;

    invoke-static {p14}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/hh;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->o:Lcom/google/android/apps/youtube/app/ui/hh;

    invoke-static/range {p15 .. p15}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->k:Ljava/lang/String;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->t:Ljava/util/Set;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/bz;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/bz;)Lcom/google/android/apps/youtube/app/am;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->f:Lcom/google/android/apps/youtube/app/am;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/bz;)Lcom/google/android/apps/youtube/uilib/a/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->r:Lcom/google/android/apps/youtube/uilib/a/h;

    return-object v0
.end method

.method private c()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->s:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->s:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->s:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->p:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a()V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/cb;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/cb;-><init>(Lcom/google/android/apps/youtube/app/ui/bz;B)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->s:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->d:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/bz;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/bz;->a:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/bz;->s:Lcom/google/android/apps/youtube/common/a/d;

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/bz;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/ui/bz;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->p:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/ui/bz;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->t:Ljava/util/Set;

    return-object v0
.end method

.method private handleOfflinePlaylistSyncEvent(Lcom/google/android/apps/youtube/app/offline/a/u;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->k:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/youtube/app/offline/a/u;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/bz;->c()V

    :cond_0
    return-void
.end method

.method private handleOfflineVideoDeleteEvent(Lcom/google/android/apps/youtube/app/offline/a/ad;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->t:Ljava/util/Set;

    iget-object v1, p1, Lcom/google/android/apps/youtube/app/offline/a/ad;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/bz;->c()V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/bz;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->u:Lcom/google/android/apps/youtube/app/ui/cr;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/bz;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/cr;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->e:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->e:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/bz;->u:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/View;)V
    .locals 15

    sget v1, Lcom/google/android/youtube/j;->cm:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/bz;->p:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    sget v1, Lcom/google/android/youtube/j;->gf:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/bz;->q:Landroid/widget/ListView;

    sget v1, Lcom/google/android/youtube/l;->aJ:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/bz;->q:Landroid/widget/ListView;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/bz;->q:Landroid/widget/ListView;

    invoke-virtual {v1, v10}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/cr;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/bz;->a:Landroid/app/Activity;

    check-cast v2, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/bz;->d:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/bz;->c:Lcom/google/android/apps/youtube/datalib/innertube/v;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/bz;->b:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/bz;->g:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/bz;->l:Lcom/google/android/apps/youtube/app/ui/cl;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/bz;->n:Lcom/google/android/apps/youtube/app/offline/f;

    const/4 v9, 0x2

    new-instance v11, Lcom/google/android/apps/youtube/app/ui/ca;

    invoke-direct {v11, p0}, Lcom/google/android/apps/youtube/app/ui/ca;-><init>(Lcom/google/android/apps/youtube/app/ui/bz;)V

    invoke-direct/range {v1 .. v11}, Lcom/google/android/apps/youtube/app/ui/cr;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/datalib/innertube/v;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/cl;Lcom/google/android/apps/youtube/app/offline/f;ILandroid/view/ViewGroup;Lcom/google/android/apps/youtube/app/ui/db;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/bz;->u:Lcom/google/android/apps/youtube/app/ui/cr;

    new-instance v1, Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/uilib/a/h;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/bz;->r:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v13, p0, Lcom/google/android/apps/youtube/app/ui/bz;->r:Lcom/google/android/apps/youtube/uilib/a/h;

    const-class v14, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/ba;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/bz;->a:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/bz;->e:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/bz;->h:Lcom/google/android/apps/youtube/common/network/h;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/bz;->f:Lcom/google/android/apps/youtube/app/am;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/ui/bz;->j:Lcom/google/android/apps/youtube/core/player/w;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/ui/bz;->o:Lcom/google/android/apps/youtube/app/ui/hh;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/ui/bz;->m:Lcom/google/android/apps/youtube/app/offline/r;

    iget-object v9, p0, Lcom/google/android/apps/youtube/app/ui/bz;->d:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/ui/bz;->g:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v11, p0, Lcom/google/android/apps/youtube/app/ui/bz;->i:Lcom/google/android/apps/youtube/app/offline/p;

    iget-object v12, p0, Lcom/google/android/apps/youtube/app/ui/bz;->k:Ljava/lang/String;

    invoke-direct/range {v1 .. v12}, Lcom/google/android/apps/youtube/app/ui/presenter/ba;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/app/ui/hh;Lcom/google/android/apps/youtube/app/offline/r;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/offline/p;Ljava/lang/String;)V

    invoke-virtual {v13, v14, v1}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/bz;->q:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/bz;->r:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->s:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->s:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->s:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->s:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->e:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bz;->e:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/bz;->u:Lcom/google/android/apps/youtube/app/ui/cr;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    return-void
.end method
