.class public final Lcom/google/android/apps/youtube/app/ui/presenter/bk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/g;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

.field private final b:Lcom/google/android/apps/youtube/app/ui/cl;

.field private final c:Landroid/view/View;

.field private final d:Lcom/google/android/apps/youtube/app/ui/gr;

.field private final e:Landroid/widget/ImageView;

.field private final f:Landroid/view/View;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/TextView;

.field private final i:Landroid/widget/TextView;

.field private final j:Landroid/view/View;

.field private final k:Landroid/widget/ImageView;

.field private final l:Landroid/widget/ImageView;

.field private final m:Landroid/widget/ImageView;

.field private final n:Landroid/widget/ImageView;

.field private final o:Lcom/google/android/apps/youtube/app/ui/presenter/bw;

.field private final p:Lcom/google/android/apps/youtube/core/client/bc;

.field private final q:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

.field private final r:Lcom/google/android/apps/youtube/common/a/b;

.field private s:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

.field private final t:Lcom/google/android/apps/youtube/app/ui/presenter/as;

.field private final u:Lcom/google/android/apps/youtube/app/offline/f;

.field private v:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

.field private w:Lcom/google/a/a/a/a/kz;

.field private x:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/datalib/innertube/v;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/app/offline/f;)V
    .locals 9

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static/range {p10 .. p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p8 .. p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->p:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static/range {p11 .. p11}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->q:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->u:Lcom/google/android/apps/youtube/app/offline/f;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/l;->aJ:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->c:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->c:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->dE:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->c:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->fA:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->f:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/gr;

    move-object/from16 v0, p9

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/youtube/app/ui/gr;-><init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/widget/ImageView;)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->d:Lcom/google/android/apps/youtube/app/ui/gr;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->c:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->dH:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->c:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->dD:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->h:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->c:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->dG:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->c:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->dm:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->j:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->c:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->bK:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->k:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->c:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->ce:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->l:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->c:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->eI:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->m:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->c:Landroid/view/View;

    sget v2, Lcom/google/android/youtube/j;->dQ:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->n:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/bw;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/app/ui/presenter/bw;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/bk;B)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->o:Lcom/google/android/apps/youtube/app/ui/presenter/bw;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/cl;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/youtube/datalib/innertube/v;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/youtube/core/aw;

    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/youtube/common/c/a;

    move-object v2, p1

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/youtube/app/ui/cl;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/datalib/innertube/v;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/c/a;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->b:Lcom/google/android/apps/youtube/app/ui/cl;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->j:Landroid/view/View;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/presenter/bl;

    move-object/from16 v0, p10

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/bl;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/bk;Lcom/google/android/apps/youtube/datalib/d/a;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->k:Landroid/widget/ImageView;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/presenter/bm;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/youtube/app/ui/presenter/bm;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/bk;Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->l:Landroid/widget/ImageView;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/presenter/bn;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/ui/presenter/bn;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->m:Landroid/widget/ImageView;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/presenter/bo;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/ui/presenter/bo;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/p;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->u:Lcom/google/android/apps/youtube/app/offline/f;

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/bp;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/ui/presenter/bp;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/bk;Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;)V

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/presenter/as;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->c:Landroid/view/View;

    new-instance v4, Lcom/google/android/apps/youtube/app/ui/presenter/bq;

    move-object/from16 v0, p11

    invoke-direct {v4, p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/presenter/bq;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/bk;Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;Lcom/google/android/apps/youtube/app/offline/j;)V

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/as;-><init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->t:Lcom/google/android/apps/youtube/app/ui/presenter/as;

    :goto_0
    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/bs;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/app/ui/presenter/bs;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/bk;B)V

    invoke-static {p1, v1}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->r:Lcom/google/android/apps/youtube/common/a/b;

    move-object/from16 v0, p7

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->t:Lcom/google/android/apps/youtube/app/ui/presenter/as;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->w:Lcom/google/a/a/a/a/kz;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/presenter/bk;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->s:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    return-object p1
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->t:Lcom/google/android/apps/youtube/app/ui/presenter/as;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->t:Lcom/google/android/apps/youtube/app/ui/presenter/as;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/presenter/as;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;)Landroid/view/View;

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->l:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    sget v0, Lcom/google/android/youtube/h;->ac:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void

    :cond_0
    sget v0, Lcom/google/android/youtube/h;->ab:I

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Lcom/google/android/apps/youtube/datalib/innertube/model/ab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->x:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Lcom/google/android/apps/youtube/app/ui/cl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->b:Lcom/google/android/apps/youtube/app/ui/cl;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Lcom/google/android/apps/youtube/app/ui/presenter/as;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->t:Lcom/google/android/apps/youtube/app/ui/presenter/as;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Lcom/google/android/apps/youtube/app/offline/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->u:Lcom/google/android/apps/youtube/app/offline/f;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->s:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->e:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/apps/youtube/app/ui/presenter/bk;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->f:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/ab;)Landroid/view/View;
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->s:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->q:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->p:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->r:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v2, v0, v3}, Lcom/google/android/apps/youtube/core/client/bc;->d(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;)V

    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->h:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->h()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->f()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->l:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->i()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->j()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->x:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->x:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->m:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->k()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->n:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->m()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->d()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->d:Lcom/google/android/apps/youtube/app/ui/gr;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->o:Lcom/google/android/apps/youtube/app/ui/presenter/bw;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/app/ui/gr;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;Lcom/google/android/apps/youtube/app/d/e;)V

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->b()Lcom/google/a/a/a/a/kz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->w:Lcom/google/a/a/a/a/kz;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->c:Landroid/view/View;

    return-object v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ab;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final handleOfflinePlaylistAddEvent(Lcom/google/android/apps/youtube/app/offline/a/q;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/q;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;)V

    :cond_0
    return-void
.end method

.method public final handleOfflinePlaylistAddFailedEvent(Lcom/google/android/apps/youtube/app/offline/a/r;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/apps/youtube/app/offline/a/r;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;)V

    :cond_0
    return-void
.end method

.method public final handleOfflinePlaylistDeleteEvent(Lcom/google/android/apps/youtube/app/offline/a/s;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/apps/youtube/app/offline/a/s;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;)V

    :cond_0
    return-void
.end method

.method public final handleOfflinePlaylistProgressEvent(Lcom/google/android/apps/youtube/app/offline/a/t;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p1, Lcom/google/android/apps/youtube/app/offline/a/t;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->v:Lcom/google/android/apps/youtube/datalib/innertube/model/ab;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;)V

    :cond_0
    return-void
.end method

.method public final handlePlaylistLikeAction(Lcom/google/android/apps/youtube/app/ui/de;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ui/de;->b()Lcom/google/android/apps/youtube/app/ui/LikeAction;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/LikeAction;->LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->x:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->x:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/presenter/bk;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
