.class public final Lcom/google/android/apps/youtube/app/adapter/ao;
.super Lcom/google/android/apps/youtube/app/adapter/g;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/adapter/g;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ao;->a:Landroid/content/res/Resources;

    iput-boolean p2, p0, Lcom/google/android/apps/youtube/app/adapter/ao;->b:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/ao;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/ao;->b:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/adapter/ao;)Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ao;->a:Landroid/content/res/Resources;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/youtube/app/adapter/ae;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/ap;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/ap;-><init>(Lcom/google/android/apps/youtube/app/adapter/ao;Landroid/view/View;Landroid/view/ViewGroup;)V

    return-object v0
.end method
