.class public Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;
.super Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/ig;


# instance fields
.field private Y:Lcom/google/android/apps/youtube/app/ui/ie;

.field private Z:Lcom/google/android/apps/youtube/app/adapter/af;

.field private a:Lcom/google/android/apps/youtube/app/ax;

.field private aa:Lcom/google/android/apps/youtube/app/ui/v;

.field private ab:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

.field private ac:Lcom/google/android/apps/youtube/app/remote/an;

.field private b:Landroid/content/res/Resources;

.field private d:Lcom/google/android/apps/youtube/core/async/af;

.field private e:Lcom/google/android/apps/youtube/core/client/bc;

.field private f:Lcom/google/android/apps/youtube/core/client/bj;

.field private g:Lcom/google/android/apps/youtube/core/identity/l;

.field private h:Lcom/google/android/apps/youtube/core/aw;

.field private i:Lcom/google/android/apps/youtube/app/ui/et;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;-><init>()V

    return-void
.end method

.method private L()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->b:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/k;->m:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/et;->a(I)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;)Lcom/google/android/apps/youtube/app/adapter/af;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->Z:Lcom/google/android/apps/youtube/app/adapter/af;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/aj;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/fragments/aj;-><init>(Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->e:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->editUri:Landroid/net/Uri;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/youtube/core/client/bc;->b(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    sget v0, Lcom/google/android/youtube/l;->ao:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->aa:Lcom/google/android/apps/youtube/app/ui/v;

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/ai;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/fragments/ai;-><init>(Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->aa:Lcom/google/android/apps/youtube/app/ui/v;

    sget v2, Lcom/google/android/youtube/p;->eT:I

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/app/ui/v;->a(ILcom/google/android/apps/youtube/app/ui/ab;)I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->f:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->aa:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/app/adapter/ag;->b(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->Z:Lcom/google/android/apps/youtube/app/adapter/af;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->Z:Lcom/google/android/apps/youtube/app/adapter/af;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/et;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/a/a;)Lcom/google/android/apps/youtube/app/ui/et;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/ie;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v2, Lcom/google/android/youtube/j;->bf:I

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/core/ui/PagedView;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->d:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    const/4 v6, 0x1

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/app/ui/ie;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;ZLcom/google/android/apps/youtube/app/ui/ig;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->Y:Lcom/google/android/apps/youtube/app/ui/ie;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->ac:Lcom/google/android/apps/youtube/app/remote/an;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->aa:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    sget-object v4, Lcom/google/android/apps/youtube/core/client/WatchFeature;->MY_FAVORITES:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->ab:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->Y:Lcom/google/android/apps/youtube/app/ui/ie;

    const-string v1, "favorites_helper"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ie;->a(Landroid/os/Bundle;)V

    :cond_0
    return-object v8
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 1

    sget v0, Lcom/google/android/youtube/p;->ad:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->j()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->b:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->g:Lcom/google/android/apps/youtube/core/identity/l;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->e:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->f:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->e:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->v()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->d:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->ac:Lcom/google/android/apps/youtube/app/remote/an;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/youtube/core/client/WatchFeature;->MY_FAVORITES:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/am;->a(Ljava/lang/String;ZLcom/google/android/apps/youtube/core/client/WatchFeature;)V

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_your_channel"

    return-object v0
.end method

.method public final d()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->d()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->L()V

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->e(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->Y:Lcom/google/android/apps/youtube/app/ui/ie;

    if-eqz v0, :cond_0

    const-string v0, "favorites_helper"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->Y:Lcom/google/android/apps/youtube/app/ui/ie;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/ie;->d()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->ab:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->aa:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/v;->b()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->L()V

    return-void
.end method

.method public final r()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->r()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->g:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->Y:Lcom/google/android/apps/youtube/app/ui/ie;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/apps/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->e:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->j()Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ie;->a([Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyFavoritesFragment;->ab:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b()V

    return-void
.end method
