.class final Lcom/google/android/apps/youtube/app/ui/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/g;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/ui/g;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/i;->a:Lcom/google/android/apps/youtube/app/ui/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/ui/g;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/i;-><init>(Lcom/google/android/apps/youtube/app/ui/g;)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "Error retrieving user banner"

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/i;->a:Lcom/google/android/apps/youtube/app/ui/g;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/g;->c(Lcom/google/android/apps/youtube/app/ui/g;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/i;->a:Lcom/google/android/apps/youtube/app/ui/g;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/g;->a(Lcom/google/android/apps/youtube/app/ui/g;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/i;->a:Lcom/google/android/apps/youtube/app/ui/g;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/g;->d(Lcom/google/android/apps/youtube/app/ui/g;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/i;->a:Lcom/google/android/apps/youtube/app/ui/g;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/g;->e(Lcom/google/android/apps/youtube/app/ui/g;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/i;->a:Lcom/google/android/apps/youtube/app/ui/g;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/g;->f(Lcom/google/android/apps/youtube/app/ui/g;)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/i;->a:Lcom/google/android/apps/youtube/app/ui/g;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/g;->f(Lcom/google/android/apps/youtube/app/ui/g;)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/i;->a:Lcom/google/android/apps/youtube/app/ui/g;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/g;->g(Lcom/google/android/apps/youtube/app/ui/g;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/i;->a:Lcom/google/android/apps/youtube/app/ui/g;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/g;->c(Lcom/google/android/apps/youtube/app/ui/g;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/i;->a:Lcom/google/android/apps/youtube/app/ui/g;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/g;->f(Lcom/google/android/apps/youtube/app/ui/g;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/i;->a:Lcom/google/android/apps/youtube/app/ui/g;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/g;->b(Lcom/google/android/apps/youtube/app/ui/g;Z)Z

    :cond_0
    return-void
.end method
