.class public final Lcom/google/android/apps/youtube/app/notification/d;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Lcom/google/a/a/a/a/kz;)V
    .locals 5

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p4, Lcom/google/a/a/a/a/kz;->i:Lcom/google/a/a/a/a/wb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    sget-object v2, Lcom/google/android/apps/youtube/core/client/WatchFeature;->VIDEO_NOTIFICATION:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-direct {v1, p4, v2}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Lcom/google/a/a/a/a/kz;Lcom/google/android/apps/youtube/core/client/WatchFeature;)V

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-direct {v2, v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;-><init>(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    const-string v1, "watch"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v1

    const-wide v3, 0x41dfffffffc00000L    # 2.147483647E9

    mul-double/2addr v1, v3

    double-to-int v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/support/v4/app/al;

    invoke-direct {v1, p0}, Landroid/support/v4/app/al;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v4/app/al;->b(Z)Landroid/support/v4/app/al;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/al;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/al;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v4/app/al;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/al;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/al;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/al;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/h;->ae:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/al;->a(I)Landroid/support/v4/app/al;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/support/v4/app/al;->a(Landroid/graphics/Bitmap;)Landroid/support/v4/app/al;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/support/v4/app/al;->b(I)Landroid/support/v4/app/al;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/al;->a()Landroid/app/Notification;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const/16 v2, 0x3ea

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method
