.class public abstract Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field protected c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static I()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static J()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static K()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static b(Lcom/google/android/apps/youtube/app/compat/SupportActionBar;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Z)V

    return-void
.end method


# virtual methods
.method public E()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(Lcom/google/android/apps/youtube/app/fragments/PaneFragment;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    return-object v0
.end method

.method protected final F()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Lcom/google/android/apps/youtube/app/compat/SupportActionBar;)V

    :cond_0
    return-void
.end method

.method public final G()Lcom/google/android/apps/youtube/app/YouTubeApplication;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    return-object v0
.end method

.method public final H()Lcom/google/android/apps/youtube/app/ax;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/CharSequence;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->a(Landroid/app/Activity;)V

    check-cast p1, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    return-void
.end method

.method public a(Lcom/google/android/apps/youtube/app/compat/SupportActionBar;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/youtube/app/compat/j;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O()Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v1

    if-eqz v0, :cond_0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/youtube/app/compat/q;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Lcom/google/android/apps/youtube/app/compat/j;)V
    .locals 0

    return-void
.end method

.method public r()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->r()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O()Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->ICONIFIED:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Lcom/google/android/apps/youtube/app/compat/SupportActionBar;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->n()V

    return-void
.end method

.method public s()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->s()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O()Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->d()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Z)V

    return-void
.end method
