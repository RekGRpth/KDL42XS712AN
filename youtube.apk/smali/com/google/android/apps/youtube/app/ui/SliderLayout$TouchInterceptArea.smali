.class public final enum Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

.field public static final enum ALL:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

.field public static final enum BOTTOM_EDGE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

.field public static final enum LEFT_EDGE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

.field public static final enum RIGHT_EDGE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

.field public static final enum TOP_EDGE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    const-string v1, "LEFT_EDGE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->LEFT_EDGE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    const-string v1, "TOP_EDGE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->TOP_EDGE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    const-string v1, "RIGHT_EDGE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->RIGHT_EDGE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    const-string v1, "BOTTOM_EDGE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->BOTTOM_EDGE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->ALL:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->LEFT_EDGE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->TOP_EDGE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->RIGHT_EDGE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->BOTTOM_EDGE:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->ALL:Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->$VALUES:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->$VALUES:[Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/app/ui/SliderLayout$TouchInterceptArea;

    return-object v0
.end method
