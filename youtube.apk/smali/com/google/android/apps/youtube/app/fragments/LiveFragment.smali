.class public Lcom/google/android/apps/youtube/app/fragments/LiveFragment;
.super Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;
.source "SourceFile"


# instance fields
.field private Y:Lcom/google/android/apps/youtube/core/ui/l;

.field private Z:Lcom/google/android/apps/youtube/core/ui/l;

.field private aa:Landroid/os/Bundle;

.field private b:Lcom/google/android/apps/youtube/app/ax;

.field private d:Lcom/google/android/apps/youtube/core/client/bj;

.field private e:Lcom/google/android/apps/youtube/core/aw;

.field private f:Lcom/google/android/apps/youtube/common/network/h;

.field private g:Lcom/google/android/apps/youtube/core/client/bc;

.field private h:Lcom/google/android/apps/youtube/app/ui/et;

.field private i:Lcom/google/android/apps/youtube/app/ui/et;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;-><init>()V

    return-void
.end method

.method private P()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/k;->m:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->h:Lcom/google/android/apps/youtube/app/ui/et;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->h:Lcom/google/android/apps/youtube/app/ui/et;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/et;->a(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/et;->a(I)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/youtube/core/ui/PagedListView;I)Lcom/google/android/apps/youtube/core/a/a;
    .locals 11

    const/4 v10, 0x1

    const/4 v9, 0x0

    packed-switch p2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->g:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->f:Lcom/google/android/apps/youtube/common/network/h;

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/app/adapter/ag;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/af;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0, v8}, Lcom/google/android/apps/youtube/app/ui/et;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/a/a;)Lcom/google/android/apps/youtube/app/ui/et;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->h:Lcom/google/android/apps/youtube/app/ui/et;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->P()V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/br;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->h:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->g:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->e:Lcom/google/android/apps/youtube/core/aw;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v6

    new-instance v7, Lcom/google/android/apps/youtube/app/fragments/ac;

    invoke-direct {v7, p0}, Lcom/google/android/apps/youtube/app/fragments/ac;-><init>(Lcom/google/android/apps/youtube/app/fragments/LiveFragment;)V

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/app/ui/br;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/app/ui/et;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/common/fromguava/d;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->Y:Lcom/google/android/apps/youtube/core/ui/l;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->aa:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->Y:Lcom/google/android/apps/youtube/core/ui/l;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->aa:Landroid/os/Bundle;

    const-string v2, "live_now_helper"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/l;->a(Landroid/os/Bundle;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->Y:Lcom/google/android/apps/youtube/core/ui/l;

    new-array v1, v10, [Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->g:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->h()Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v9

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/l;->a([Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    move-object v0, v8

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->g:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->f:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/apps/youtube/app/adapter/ag;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/af;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0, v8}, Lcom/google/android/apps/youtube/app/ui/et;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/a/a;)Lcom/google/android/apps/youtube/app/ui/et;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->P()V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/br;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->i:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->g:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->e:Lcom/google/android/apps/youtube/core/aw;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v6

    new-instance v7, Lcom/google/android/apps/youtube/app/fragments/ad;

    invoke-direct {v7, p0}, Lcom/google/android/apps/youtube/app/fragments/ad;-><init>(Lcom/google/android/apps/youtube/app/fragments/LiveFragment;)V

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/app/ui/br;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/app/ui/et;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/common/fromguava/d;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->Z:Lcom/google/android/apps/youtube/core/ui/l;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->aa:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->Z:Lcom/google/android/apps/youtube/core/ui/l;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->aa:Landroid/os/Bundle;

    const-string v2, "recent_helper"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/l;->a(Landroid/os/Bundle;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->Z:Lcom/google/android/apps/youtube/core/ui/l;

    new-array v1, v10, [Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->g:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->i()Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v9

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/l;->a([Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    move-object v0, v8

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->cm:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->a(Landroid/os/Bundle;)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->aa:Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->b:Lcom/google/android/apps/youtube/app/ax;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->f:Lcom/google/android/apps/youtube/common/network/h;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->g:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->e:Lcom/google/android/apps/youtube/core/aw;

    return-void
.end method

.method protected final c(I)Ljava/lang/String;
    .locals 2

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->cq:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->eR:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final d()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->d()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->P()V

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->e(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->Y:Lcom/google/android/apps/youtube/core/ui/l;

    if-eqz v0, :cond_0

    const-string v0, "live_now_helper"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->Y:Lcom/google/android/apps/youtube/core/ui/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/ui/l;->d()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->Z:Lcom/google/android/apps/youtube/core/ui/l;

    if-eqz v0, :cond_1

    const-string v0, "recent_helper"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->Z:Lcom/google/android/apps/youtube/core/ui/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/ui/l;->d()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->aa:Landroid/os/Bundle;

    return-void
.end method

.method protected final j_()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/TabbedMultiFeedFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/LiveFragment;->P()V

    return-void
.end method
