.class public Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;
.super Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
.source "SourceFile"


# instance fields
.field private Y:Lcom/google/android/apps/youtube/core/Analytics;

.field private Z:Lcom/google/android/apps/youtube/core/a/i;

.field private a:Lcom/google/android/apps/youtube/core/identity/l;

.field private aa:Landroid/widget/ListView;

.field private ab:Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline;

.field private b:Landroid/view/View;

.field private d:Lcom/google/android/apps/youtube/app/ui/gj;

.field private e:Lcom/google/android/apps/youtube/core/client/bc;

.field private f:Lcom/google/android/apps/youtube/core/client/bj;

.field private g:Lcom/google/android/apps/youtube/common/c/a;

.field private h:Lcom/google/android/apps/youtube/app/am;

.field private i:Lcom/google/android/apps/youtube/core/aw;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;-><init>()V

    return-void
.end method

.method private L()V
    .locals 11

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->Y:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->e:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->a:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->g:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->h:Lcom/google/android/apps/youtube/app/am;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->i:Lcom/google/android/apps/youtube/core/aw;

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->f:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v9, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->d:Lcom/google/android/apps/youtube/app/ui/gj;

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->b:Landroid/view/View;

    invoke-static/range {v0 .. v10}, Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/app/YouTubeApplication;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/gj;Landroid/view/View;)Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->Z:Lcom/google/android/apps/youtube/core/a/i;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/a/i;->b(Lcom/google/android/apps/youtube/core/a/e;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->aa:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->Z:Lcom/google/android/apps/youtube/core/a/i;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    sget v1, Lcom/google/android/youtube/l;->t:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->q()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p1, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->b:Landroid/view/View;

    new-instance v0, Lcom/google/android/apps/youtube/core/a/i;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/core/a/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->Z:Lcom/google/android/apps/youtube/core/a/i;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->b:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->ac:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->aa:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->b:Landroid/view/View;

    return-object v0
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->ce:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline;->a(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline;->a(Landroid/net/Uri;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->d:Lcom/google/android/apps/youtube/app/ui/gj;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->e:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->a:Lcom/google/android/apps/youtube/core/identity/l;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->h:Lcom/google/android/apps/youtube/app/am;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->f:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->g:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->i:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->Y:Lcom/google/android/apps/youtube/core/Analytics;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Subscription;)V

    :cond_0
    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->ab:Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline;->b(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->d()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->L()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreFragment;->L()V

    return-void
.end method
