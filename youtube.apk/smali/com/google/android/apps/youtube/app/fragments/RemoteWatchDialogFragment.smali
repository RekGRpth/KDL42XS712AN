.class public Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;
.super Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;
.source "SourceFile"


# static fields
.field private static final Y:Ljava/util/regex/Pattern;

.field private static final Z:Ljava/util/regex/Pattern;


# instance fields
.field private aA:I

.field private aB:Ljava/util/List;

.field private aC:Lcom/google/android/apps/youtube/core/client/WatchFeature;

.field private aD:Lcom/google/android/apps/youtube/core/aw;

.field private aE:Landroid/app/Activity;

.field private aa:Landroid/view/View;

.field private ab:Landroid/view/View;

.field private ac:Landroid/view/View;

.field private ad:Landroid/view/View;

.field private ae:Landroid/view/View;

.field private af:Landroid/widget/TextView;

.field private ag:Landroid/view/View;

.field private ah:Landroid/view/View;

.field private ai:Landroid/widget/TextView;

.field private aj:Landroid/widget/TextView;

.field private ak:Landroid/widget/TextView;

.field private al:Landroid/widget/TextView;

.field private am:Landroid/widget/TextView;

.field private an:Landroid/widget/TextView;

.field private ao:Landroid/widget/ImageView;

.field private ap:Landroid/view/View;

.field private aq:Landroid/view/View;

.field private ar:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

.field private as:Lcom/google/android/apps/youtube/core/client/bc;

.field private at:Lcom/google/android/apps/youtube/common/a/d;

.field private au:Lcom/google/android/apps/youtube/common/a/d;

.field private av:Lcom/google/android/apps/youtube/app/fragments/bl;

.field private aw:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

.field private ax:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

.field private ay:Ljava/lang/String;

.field private az:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "(PL|RD).*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->Y:Ljava/util/regex/Pattern;

    const-string v0, "RD.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->Z:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;-><init>()V

    return-void
.end method

.method private E()V
    .locals 10

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v8

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/ax;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v2

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v3

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v5

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v6

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->b()Lcom/google/android/apps/youtube/app/aw;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/aw;->k()Z

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/app/remote/ax;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/identity/l;Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aC:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/ax;->a(Lcom/google/android/apps/youtube/core/client/WatchFeature;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ax:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ad:Landroid/view/View;

    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ag:Landroid/view/View;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/bi;

    invoke-direct {v3, p0, v8, v0, v1}, Lcom/google/android/apps/youtube/app/fragments/bi;-><init>(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/app/remote/ax;Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ah:Landroid/view/View;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/bj;

    invoke-direct {v3, p0, v8, v0, v1}, Lcom/google/android/apps/youtube/app/fragments/bj;-><init>(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/app/remote/ax;Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->af:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->af:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private F()Lcom/google/android/apps/youtube/app/remote/RemoteControl;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->p()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/an;->e()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spannable;
    .locals 5

    const/16 v3, 0x33

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " \u00b7 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-static {v3, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x3

    add-int/lit8 v3, v3, -0x1

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aj:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ao:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;)V
    .locals 11

    const/16 v1, 0x8

    const/4 v10, 0x1

    const/4 v9, 0x0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ax:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aa:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ab:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ac:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->F()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->E()V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getVideo()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ay:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->d(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aj:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/p;->hb:I

    new-array v4, v10, [Ljava/lang/Object;

    iget-wide v5, v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->viewCount:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-virtual {v1, v2, v4}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->ownerDisplayName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->al:Landroid/widget/TextView;

    iget-object v4, v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->ownerDisplayName:Ljava/lang/String;

    invoke-static {v4, v1}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-wide v1, v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->likesCount:J

    iget-wide v4, v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->dislikesCount:J

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->am:Landroid/widget/TextView;

    const-string v7, "%1$,d"

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->an:Landroid/widget/TextView;

    const-string v2, "%1$,d"

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v6, v9

    invoke-static {v2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->uploadedDate:Ljava/util/Date;

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    iget-object v2, v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->uploadedDate:Ljava/util/Date;

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    iget-object v2, v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->description:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->description:Ljava/lang/String;

    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ak:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->i()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v1

    iget-object v0, v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->hqThumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_5

    iget-object v0, v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->hqThumbnailUri:Landroid/net/Uri;

    :goto_2
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/bh;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/app/fragments/bh;-><init>(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)V

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/youtube/core/client/bj;->b(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_2
    return-void

    :cond_3
    const-string v1, ""

    goto :goto_0

    :cond_4
    const-string v2, ""

    goto :goto_1

    :cond_5
    iget-object v0, v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->mqThumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_6

    iget-object v0, v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->mqThumbnailUri:Landroid/net/Uri;

    goto :goto_2

    :cond_6
    iget-object v0, v3, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->defaultThumbnailUri:Landroid/net/Uri;

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/bk;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ae:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ax:Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->E()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->a()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ae:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ab:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->ek:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/be;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/app/fragments/be;-><init>(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->al:Landroid/widget/TextView;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 8

    const/16 v2, 0x8

    const/4 v5, -0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aa:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ab:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ac:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/bg;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/fragments/bg;-><init>(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->at:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ar:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->az:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getClickTrackingParams()[B

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->az:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlayerParams()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->at:Lcom/google/android/apps/youtube/common/a/d;

    invoke-static {v1, v6}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v7

    move-object v1, p1

    move v6, v5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;Ljava/lang/String;)Z
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)V
    .locals 3

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aa:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ab:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ac:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;Ljava/lang/String;)Z
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static c(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->Y:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ay:Ljava/lang/String;

    return-object v0
.end method

.method private static d(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->Z:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aA:I

    return v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aB:Ljava/util/List;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->F()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    sget v0, Lcom/google/android/youtube/l;->aU:I

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    sget v0, Lcom/google/android/youtube/j;->cj:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aa:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->aV:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ab:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->aB:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ac:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->aA:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ae:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->ed:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ad:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->dp:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->af:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->dk:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ag:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->do:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ai:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->dV:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ah:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aj:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->aH:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ak:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->db:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->al:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->cg:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->am:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->aM:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->an:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ao:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/youtube/j;->ck:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ap:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->cf:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aq:Landroid/view/View;

    return-object v1
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;->a(Landroid/app/Activity;)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aE:Landroid/app/Activity;

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;->a(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aE:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->ap()Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ar:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->as:Lcom/google/android/apps/youtube/core/client/bc;

    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;->c(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    return-object v0
.end method

.method public final d()V
    .locals 5

    const/16 v4, 0x8

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;->d()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->h()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "watch"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->F()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aw:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aw:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/bl;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/youtube/app/fragments/bl;-><init>(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->av:Lcom/google/android/apps/youtube/app/fragments/bl;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aw:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->av:Lcom/google/android/apps/youtube/app/fragments/bl;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->a(Lcom/google/android/apps/youtube/app/remote/aq;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->A()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aD:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->h()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "watch"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->getPlaybackStartDescriptor()Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistIndex()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aA:I

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->getPlaybackStartDescriptor()Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->az:Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->getPlaybackStartDescriptor()Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ay:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->getPlaybackStartDescriptor()Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoIds()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aB:Ljava/util/List;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->getPlaybackStartDescriptor()Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getWatchFeature()Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aC:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->h()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "watch"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->getPlaybackStartDescriptor()Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aB:Ljava/util/List;

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aA:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aB:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aA:I

    if-ltz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aB:Ljava/util/List;

    iget v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aA:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->a(Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ay:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/bf;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/fragments/bf;-><init>(Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->au:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->as:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ay:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->au:Lcom/google/android/apps/youtube/common/a/d;

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->d(Ljava/lang/String;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ay:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ai:Landroid/widget/TextView;

    sget v2, Lcom/google/android/youtube/p;->dL:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_2
    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ak:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aq:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->j()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/g;->ab:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->j()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/g;->aa:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ap:Landroid/view/View;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v0, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ac:Landroid/view/View;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v0, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ab:Landroid/view/View;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Nothing to play. Parsed videoIds are "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aB:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and playlistStartPosition is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aA:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aD:Lcom/google/android/apps/youtube/core/aw;

    sget v1, Lcom/google/android/youtube/p;->bn:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/aw;->a(I)V

    goto/16 :goto_1

    :cond_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aB:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aB:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput v2, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aA:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    const-string v0, "No video_id. No idea what to play."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aD:Lcom/google/android/apps/youtube/core/aw;

    sget v1, Lcom/google/android/youtube/p;->bn:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/aw;->a(I)V

    goto/16 :goto_1

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->ai:Landroid/widget/TextView;

    sget v2, Lcom/google/android/youtube/p;->dM:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2
.end method

.method public final e()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->at:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->at:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->at:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->au:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->au:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->au:Lcom/google/android/apps/youtube/common/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aw:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aw:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->av:Lcom/google/android/apps/youtube/app/fragments/bl;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b(Lcom/google/android/apps/youtube/app/remote/aq;)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->aw:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;->av:Lcom/google/android/apps/youtube/app/fragments/bl;

    :cond_2
    return-void
.end method
