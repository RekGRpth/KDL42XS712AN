.class final Lcom/google/android/apps/youtube/app/d/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/presenter/cu;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/d/f;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/d/f;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/d/ac;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/d/f;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/d/ac;-><init>(Lcom/google/android/apps/youtube/app/d/f;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;)V
    .locals 3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ac;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->i(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/datalib/distiller/a;

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/distiller/a;->b()Lcom/google/android/apps/youtube/datalib/distiller/d;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/distiller/d;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/distiller/d;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/distiller/d;->a(I)Lcom/google/android/apps/youtube/datalib/distiller/d;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/distiller/d;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/distiller/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/d/ac;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/d/f;->i(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/datalib/distiller/a;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/app/d/ad;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/youtube/app/d/ad;-><init>(Lcom/google/android/apps/youtube/app/d/ac;Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/distiller/a;->a(Lcom/google/android/apps/youtube/datalib/distiller/d;Lcom/google/android/apps/youtube/datalib/a/l;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ac;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->b(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/uilib/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->notifyDataSetChanged()V

    goto :goto_0
.end method
