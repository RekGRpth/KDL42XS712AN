.class public Lcom/google/android/apps/youtube/app/fragments/SearchFragment;
.super Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/innertube/q;


# instance fields
.field private Y:Landroid/widget/ListView;

.field private Z:Lcom/google/android/apps/youtube/uilib/innertube/t;

.field private a:Lcom/google/android/apps/youtube/app/ax;

.field private aa:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field private ab:Lcom/google/android/apps/youtube/core/aw;

.field private ac:Lcom/google/android/apps/youtube/app/ui/bi;

.field private ad:Landroid/os/Handler;

.field private ae:Lcom/google/android/apps/youtube/app/fragments/bq;

.field private af:Ljava/lang/Runnable;

.field private ag:Lcom/google/android/apps/youtube/app/search/SearchType;

.field private ah:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

.field private ai:Landroid/widget/Spinner;

.field private aj:Landroid/widget/Spinner;

.field private ak:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

.field private al:Z

.field private b:Lcom/google/android/apps/youtube/datalib/innertube/SearchService;

.field private d:Landroid/provider/SearchRecentSuggestions;

.field private e:Landroid/content/SharedPreferences;

.field private f:Lcom/google/android/apps/youtube/core/Analytics;

.field private g:Lcom/google/android/apps/youtube/app/ui/a;

.field private h:Ljava/lang/String;

.field private i:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;-><init>()V

    sget-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->DEFAULT_SEARCH_TYPE:Lcom/google/android/apps/youtube/app/search/SearchType;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ag:Lcom/google/android/apps/youtube/app/search/SearchType;

    sget-object v0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;->ALL_TIME:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ah:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    return-void
.end method

.method private L()V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->al:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ac:Lcom/google/android/apps/youtube/app/ui/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/bi;->a()Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ah:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->E()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->getNavigationEndpoint()Lcom/google/a/a/a/a/kz;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->b(Lcom/google/a/a/a/a/kz;)[B

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->b:Lcom/google/android/apps/youtube/datalib/innertube/SearchService;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/SearchService;->a()Lcom/google/android/apps/youtube/datalib/innertube/au;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->h:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/datalib/innertube/au;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/au;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aW()Lcom/google/android/apps/youtube/core/utils/v;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/datalib/innertube/au;->a(Lcom/google/android/apps/youtube/core/utils/v;)Lcom/google/android/apps/youtube/datalib/innertube/au;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ag:Lcom/google/android/apps/youtube/app/search/SearchType;

    sget-object v3, Lcom/google/android/apps/youtube/app/fragments/bp;->a:[I

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/search/SearchType;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;->RESULT_TYPE_ANY:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/datalib/innertube/au;->a(Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;)Lcom/google/android/apps/youtube/datalib/innertube/au;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ah:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    sget-object v3, Lcom/google/android/apps/youtube/app/fragments/bp;->b:[I

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->UPLOAD_DATE_ANY:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/datalib/innertube/au;->a(Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;)Lcom/google/android/apps/youtube/datalib/innertube/au;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/datalib/innertube/au;->a([B)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->aa:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->b:Lcom/google/android/apps/youtube/datalib/innertube/SearchService;

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/bo;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/fragments/bo;-><init>(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)V

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/youtube/datalib/innertube/SearchService;->a(Lcom/google/android/apps/youtube/datalib/innertube/au;Lcom/google/android/apps/youtube/datalib/a/l;)V

    return-void

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;->RESULT_TYPE_CHANNEL:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;->RESULT_TYPE_PLAYLIST:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->UPLOAD_DATE_TODAY:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    goto :goto_1

    :pswitch_3
    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->UPLOAD_DATE_THIS_WEEK:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    goto :goto_1

    :pswitch_4
    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;->UPLOAD_DATE_THIS_MONTH:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$UploadDateRestrictType;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;Lcom/google/android/apps/youtube/app/search/SearchType;)Lcom/google/android/apps/youtube/app/search/SearchType;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ag:Lcom/google/android/apps/youtube/app/search/SearchType;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ah:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->h:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->aa:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    return-object v0
.end method

.method private final b(Lcom/google/android/apps/youtube/app/search/SearchType;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)V
    .locals 3

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->h:Ljava/lang/String;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->h:Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->i:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->e:Landroid/content/SharedPreferences;

    const-string v1, "no_search_history"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->d:Landroid/provider/SearchRecentSuggestions;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->h:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/provider/SearchRecentSuggestions;->saveRecentQuery(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ag:Lcom/google/android/apps/youtube/app/search/SearchType;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ai:Landroid/widget/Spinner;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ai:Landroid/widget/Spinner;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/search/SearchType;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_2
    if-eqz p3, :cond_3

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ah:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->m()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->L()V

    :cond_4
    :goto_1
    return-void

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->h:Ljava/lang/String;

    goto :goto_0

    :cond_6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->al:Z

    goto :goto_1
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)Lcom/google/android/apps/youtube/uilib/innertube/t;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->Z:Lcom/google/android/apps/youtube/uilib/innertube/t;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ab:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)Lcom/google/android/apps/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->f:Lcom/google/android/apps/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->L()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)Lcom/google/android/apps/youtube/app/search/SearchType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ag:Lcom/google/android/apps/youtube/app/search/SearchType;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->af:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ad:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 17

    sget v2, Lcom/google/android/youtube/l;->bs:I

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v16

    sget v2, Lcom/google/android/youtube/j;->cm:I

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->aa:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->aa:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->setOnRetryClickListener(Lcom/google/android/apps/youtube/uilib/innertube/q;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->aa:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    sget v3, Lcom/google/android/youtube/j;->ei:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->Y:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ab:Lcom/google/android/apps/youtube/core/aw;

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->SEARCH:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v7

    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/client/bc;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ak:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/presenter/cc;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q()Lcom/google/android/apps/youtube/datalib/d/a;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/app/ax;->aQ()Lcom/google/android/apps/youtube/core/identity/as;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->g:Lcom/google/android/apps/youtube/app/ui/a;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v11}, Lcom/google/android/apps/youtube/app/ax;->F()Lcom/google/android/apps/youtube/datalib/innertube/bc;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v12}, Lcom/google/android/apps/youtube/app/ax;->G()Lcom/google/android/apps/youtube/datalib/innertube/av;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ab:Lcom/google/android/apps/youtube/core/aw;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ak:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-direct/range {v2 .. v15}, Lcom/google/android/apps/youtube/app/ui/presenter/cc;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/as;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/app/ui/a;Lcom/google/android/apps/youtube/datalib/innertube/bc;Lcom/google/android/apps/youtube/datalib/innertube/av;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;)V

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/presenter/am;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v5, Lcom/google/android/apps/youtube/app/ui/presenter/cv;

    invoke-direct {v5}, Lcom/google/android/apps/youtube/app/ui/presenter/cv;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/apps/youtube/app/ui/presenter/am;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/uilib/a/i;Lcom/google/android/apps/youtube/common/c/a;)V

    new-instance v6, Lcom/google/android/apps/youtube/app/adapter/z;

    invoke-direct {v6, v3}, Lcom/google/android/apps/youtube/app/adapter/z;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/am;)V

    new-instance v3, Lcom/google/android/apps/youtube/uilib/innertube/t;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->Y:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->b:Lcom/google/android/apps/youtube/datalib/innertube/SearchService;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ab:Lcom/google/android/apps/youtube/core/aw;

    move-object v9, v2

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/youtube/uilib/innertube/t;-><init>(Lcom/google/android/apps/youtube/uilib/innertube/i;Landroid/widget/ListView;Lcom/google/android/apps/youtube/uilib/innertube/p;Lcom/google/android/apps/youtube/datalib/innertube/i;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/uilib/innertube/j;Lcom/google/android/apps/youtube/core/aw;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->Z:Lcom/google/android/apps/youtube/uilib/innertube/t;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->h:Ljava/lang/String;

    if-nez v2, :cond_0

    if-eqz p3, :cond_2

    :goto_0
    const-string v2, "search_query"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v2, "search_type"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/search/SearchType;->fromString(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/search/SearchType;

    move-result-object v4

    const-string v2, "time_filter"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v3, v2}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->b(Lcom/google/android/apps/youtube/app/search/SearchType;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)V

    :cond_0
    sget v2, Lcom/google/android/youtube/j;->es:I

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ai:Landroid/widget/Spinner;

    new-instance v4, Landroid/widget/ArrayAdapter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v3, 0x1090008    # android.R.layout.simple_spinner_item

    invoke-direct {v4, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    const v2, 0x1090009    # android.R.layout.simple_spinner_dropdown_item

    invoke-virtual {v4, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-static {}, Lcom/google/android/apps/youtube/app/search/SearchType;->values()[Lcom/google/android/apps/youtube/app/search/SearchType;

    move-result-object v5

    array-length v6, v5

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_3

    aget-object v2, v5, v3

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/search/SearchType;->getDescriptionStringResourceId()I

    move-result v2

    sget v7, Lcom/google/android/youtube/p;->hf:I

    if-ne v2, v7, :cond_1

    sget v2, Lcom/google/android/youtube/p;->v:I

    :cond_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->h()Landroid/os/Bundle;

    move-result-object p3

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ai:Landroid/widget/Spinner;

    invoke-virtual {v2, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ai:Landroid/widget/Spinner;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/br;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/youtube/app/fragments/br;-><init>(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;B)V

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ai:Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ag:Lcom/google/android/apps/youtube/app/search/SearchType;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/search/SearchType;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    sget v2, Lcom/google/android/youtube/j;->fE:I

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->aj:Landroid/widget/Spinner;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/bs;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/youtube/app/fragments/bs;-><init>(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;B)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ah:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->aj:Landroid/widget/Spinner;

    sget v6, Lcom/google/android/youtube/l;->W:I

    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/android/apps/youtube/app/ui/bi;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/ui/bk;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;Landroid/widget/Spinner;I)Lcom/google/android/apps/youtube/app/ui/bi;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ac:Lcom/google/android/apps/youtube/app/ui/bi;

    sget v2, Lcom/google/android/youtube/l;->b:I

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->i:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->i:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->i:Landroid/widget/TextView;

    new-instance v3, Lcom/google/android/apps/youtube/app/fragments/bn;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/apps/youtube/app/fragments/bn;-><init>(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v16
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ab:Lcom/google/android/apps/youtube/core/aw;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->v()Lcom/google/android/apps/youtube/datalib/innertube/SearchService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->b:Lcom/google/android/apps/youtube/datalib/innertube/SearchService;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->j()Landroid/provider/SearchRecentSuggestions;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->d:Landroid/provider/SearchRecentSuggestions;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aP()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->e:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->f:Lcom/google/android/apps/youtube/core/Analytics;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->C()Lcom/google/android/apps/youtube/datalib/e/b;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/app/ui/a;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->g:Lcom/google/android/apps/youtube/app/ui/a;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ad:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/bq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/fragments/bq;-><init>(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ae:Lcom/google/android/apps/youtube/app/fragments/bq;

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/bm;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/fragments/bm;-><init>(Lcom/google/android/apps/youtube/app/fragments/SearchFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->af:Ljava/lang/Runnable;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/SupportActionBar;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Lcom/google/android/apps/youtube/app/compat/SupportActionBar;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->i:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Landroid/view/View;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/search/SearchType;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O()Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->d()V

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->b(Lcom/google/android/apps/youtube/app/search/SearchType;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)V

    return-void
.end method

.method public final d()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->d()V

    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->e(Landroid/os/Bundle;)V

    const-string v0, "search_query"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "search_type"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ag:Lcom/google/android/apps/youtube/app/search/SearchType;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/search/SearchType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "time_filter"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ah:Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    return-void
.end method

.method public final k_()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->L()V

    return-void
.end method

.method public final r()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->r()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O()Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ae:Lcom/google/android/apps/youtube/app/fragments/bq;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/f;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ak:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b()V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->al:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->L()V

    :cond_0
    return-void
.end method

.method public final s()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->s()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O()Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ae:Lcom/google/android/apps/youtube/app/fragments/bq;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->b(Lcom/google/android/apps/youtube/app/honeycomb/ui/f;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ad:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->af:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final t()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->t()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/SearchFragment;->ak:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a()V

    return-void
.end method
