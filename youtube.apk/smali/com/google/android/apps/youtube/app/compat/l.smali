.class public final Lcom/google/android/apps/youtube/app/compat/l;
.super Landroid/app/Dialog;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Ljava/util/List;

.field private final c:Landroid/widget/LinearLayout;

.field private final d:Landroid/widget/ListView;

.field private final e:Lcom/google/android/apps/youtube/app/compat/n;


# direct methods
.method private constructor <init>(Landroid/app/Activity;)V
    .locals 3

    sget v0, Lcom/google/android/youtube/q;->i:I

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/compat/l;->a:Landroid/app/Activity;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/l;->b:Ljava/util/List;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->bj:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/l;->c:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/l;->c:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/youtube/j;->cX:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/l;->d:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/l;->d:Landroid/widget/ListView;

    new-instance v1, Lcom/google/android/apps/youtube/app/compat/m;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/compat/m;-><init>(Lcom/google/android/apps/youtube/app/compat/l;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/n;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/l;->b:Ljava/util/List;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/youtube/app/compat/n;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/l;->e:Lcom/google/android/apps/youtube/app/compat/n;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/l;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/l;->e:Lcom/google/android/apps/youtube/app/compat/n;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/l;->c:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/compat/l;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public static a(Landroid/app/Activity;)Lcom/google/android/apps/youtube/app/compat/l;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/l;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/compat/l;-><init>(Landroid/app/Activity;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/compat/l;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/l;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/compat/l;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/l;->a:Landroid/app/Activity;

    return-object v0
.end method

.method private b()V
    .locals 7

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/compat/l;->getWindow()Landroid/view/Window;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/compat/l;->a:Landroid/app/Activity;

    const-string v0, "window"

    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v6, :cond_0

    iget v0, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    :goto_0
    int-to-double v2, v0

    const-wide v4, 0x3fe999999999999aL    # 0.8

    mul-double/2addr v2, v4

    double-to-int v0, v2

    const/4 v2, -0x2

    invoke-virtual {v1, v0, v2}, Landroid/view/Window;->setLayout(II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/l;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v6, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/compat/l;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x50

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/l;->d:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    :goto_1
    return-void

    :cond_0
    iget v0, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/compat/l;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x53

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/l;->d:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/l;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/util/DisplayMetrics;I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/compat/l;->b()V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/j;)V
    .locals 8

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/compat/l;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/l;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/compat/l;->b:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/compat/l;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x5

    :goto_0
    move v1, v2

    move v3, v2

    :goto_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/compat/j;->d()I

    move-result v6

    if-ge v1, v6, :cond_2

    invoke-virtual {p1, v1}, Lcom/google/android/apps/youtube/app/compat/j;->d(I)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/apps/youtube/app/compat/q;->h()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Lcom/google/android/apps/youtube/app/compat/q;->i()I

    move-result v6

    and-int/lit8 v6, v6, 0x2

    if-eqz v6, :cond_0

    add-int/lit8 v3, v3, 0x1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    :goto_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/compat/j;->d()I

    move-result v1

    if-ge v2, v1, :cond_6

    invoke-virtual {p1, v2}, Lcom/google/android/apps/youtube/app/compat/j;->d(I)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/compat/q;->h()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/compat/q;->i()I

    move-result v6

    and-int/lit8 v6, v6, 0x2

    if-nez v6, :cond_3

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/compat/q;->i()I

    move-result v6

    and-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_5

    if-ge v3, v0, :cond_5

    :cond_3
    add-int/lit8 v3, v3, 0x1

    :cond_4
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_6
    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/l;->e:Lcom/google/android/apps/youtube/app/compat/n;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/compat/n;->notifyDataSetChanged()V

    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/16 v0, 0x52

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/compat/l;->dismiss()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
