.class final Lcom/google/android/apps/youtube/app/ui/ef;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/ab;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

.field final synthetic b:Lcom/google/android/apps/youtube/app/ui/ee;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/ui/ee;Lcom/google/android/apps/youtube/app/remote/RemoteControl;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ef;->b:Lcom/google/android/apps/youtube/app/ui/ee;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/ef;->a:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 5

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_UPLOADED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CHANNEL_ACTIVITY_UPLOAD:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    :goto_0
    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->targetVideo:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ef;->a:Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->targetVideo:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/ef;->b:Lcom/google/android/apps/youtube/app/ui/ee;

    iget-object v3, v3, Lcom/google/android/apps/youtube/app/ui/ee;->a:Lcom/google/android/apps/youtube/core/aw;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/ef;->b:Lcom/google/android/apps/youtube/app/ui/ee;

    iget-object v4, v4, Lcom/google/android/apps/youtube/app/ui/ee;->b:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-static {v1, v2, v0, v3, v4}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/Analytics;)V

    :cond_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CHANNEL_ACTIVITY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    goto :goto_0
.end method
