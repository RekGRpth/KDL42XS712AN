.class public Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;
.super Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
.source "SourceFile"


# instance fields
.field private Y:Lcom/google/android/apps/youtube/app/ui/et;

.field private Z:Lcom/google/android/apps/youtube/app/ui/ie;

.field private a:Lcom/google/android/apps/youtube/app/ax;

.field private aa:Lcom/google/android/apps/youtube/app/ui/v;

.field private ab:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

.field private ac:Lcom/google/android/apps/youtube/app/remote/an;

.field private b:Lcom/google/android/apps/youtube/core/client/bc;

.field private d:Lcom/google/android/apps/youtube/core/client/bj;

.field private e:Lcom/google/android/apps/youtube/core/client/bq;

.field private f:Lcom/google/android/apps/youtube/core/aw;

.field private g:Lcom/google/android/apps/youtube/core/Analytics;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;-><init>()V

    return-void
.end method

.method private L()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->Y:Lcom/google/android/apps/youtube/app/ui/et;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->Y:Lcom/google/android/apps/youtube/app/ui/et;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/et;->a(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    const/4 v6, 0x0

    sget v0, Lcom/google/android/youtube/l;->aR:I

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/core/ui/PagedListView;

    sget v0, Lcom/google/android/youtube/l;->an:I

    invoke-virtual {p1, v0, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->a(Landroid/view/View;)V

    sget v0, Lcom/google/android/youtube/l;->m:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/ui/PagedListView;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->aa:Lcom/google/android/apps/youtube/app/ui/v;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->aa:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->aa:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/youtube/app/adapter/ag;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/af;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/ui/et;->b(Landroid/content/Context;Lcom/google/android/apps/youtube/core/a/a;)Lcom/google/android/apps/youtube/app/ui/et;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->Y:Lcom/google/android/apps/youtube/app/ui/et;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/ie;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->Y:Lcom/google/android/apps/youtube/app/ui/et;

    new-instance v4, Lcom/google/android/apps/youtube/app/fragments/af;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->b:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->e:Lcom/google/android/apps/youtube/core/client/bq;

    invoke-direct {v4, v5, v7}, Lcom/google/android/apps/youtube/app/fragments/af;-><init>(Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bq;)V

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->f:Lcom/google/android/apps/youtube/core/aw;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v7

    sget-object v9, Lcom/google/android/apps/youtube/core/client/WatchFeature;->SEARCH:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->g:Lcom/google/android/apps/youtube/core/Analytics;

    sget-object v11, Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;->RelatedVideos:Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;

    move v8, v6

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/youtube/app/ui/ie;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;ZLcom/google/android/apps/youtube/app/am;ZLcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->Z:Lcom/google/android/apps/youtube/app/ui/ie;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->ac:Lcom/google/android/apps/youtube/app/remote/an;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->aa:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->Y:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->f:Lcom/google/android/apps/youtube/core/aw;

    sget-object v7, Lcom/google/android/apps/youtube/core/client/WatchFeature;->ARTIST_VIDEOS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->D()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v8

    invoke-static/range {v3 .. v8}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->ab:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->Z:Lcom/google/android/apps/youtube/app/ui/ie;

    const-string v1, "videos_helper"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ie;->a(Landroid/os/Bundle;)V

    :cond_0
    return-object v2
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    sget v1, Lcom/google/android/youtube/p;->B:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->h:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->b:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->d:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->N()Lcom/google/android/apps/youtube/core/client/bq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->e:Lcom/google/android/apps/youtube/core/client/bq;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->f:Lcom/google/android/apps/youtube/core/aw;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->g:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->ac:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->h()Landroid/os/Bundle;

    move-result-object v1

    const-string v0, "artist_name"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->h:Ljava/lang/String;

    const-string v0, "artist_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->i:Ljava/lang/String;

    return-void
.end method

.method public final d()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->d()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->L()V

    return-void
.end method

.method public final e()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->Z:Lcom/google/android/apps/youtube/app/ui/ie;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->Z:Lcom/google/android/apps/youtube/app/ui/ie;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/ie;->f()V

    :cond_0
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->e(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->Z:Lcom/google/android/apps/youtube/app/ui/ie;

    if-eqz v0, :cond_0

    const-string v0, "videos_helper"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->Z:Lcom/google/android/apps/youtube/app/ui/ie;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/ie;->d()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->f()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->ab:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->L()V

    return-void
.end method

.method public final r()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->r()V

    const-string v0, "bs://%s/"

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->i:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->Z:Lcom/google/android/apps/youtube/app/ui/ie;

    new-array v2, v4, [Lcom/google/android/apps/youtube/core/async/GDataRequest;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/async/GDataRequest;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/ie;->a([Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MoreFromArtistFragment;->ab:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b()V

    return-void
.end method
