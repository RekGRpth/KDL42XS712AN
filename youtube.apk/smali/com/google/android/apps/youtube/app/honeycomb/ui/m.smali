.class final Lcom/google/android/apps/youtube/app/honeycomb/ui/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/compat/s;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/m;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/app/compat/q;)Z
    .locals 8

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/m;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->g(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v1

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/m;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->a(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;Z)Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/m;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->g(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/p;->fF:I

    invoke-interface {v1, v2}, Lcom/google/android/apps/youtube/app/compat/q;->c(I)Lcom/google/android/apps/youtube/app/compat/q;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/m;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->i(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v1

    const-string v2, "UploadStarted"

    const/4 v3, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/m;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v6}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->h(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    long-to-int v4, v4

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/m;->a:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->j(Lcom/google/android/apps/youtube/app/honeycomb/ui/j;)V

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
