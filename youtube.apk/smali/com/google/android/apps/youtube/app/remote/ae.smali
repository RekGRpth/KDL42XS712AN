.class final Lcom/google/android/apps/youtube/app/remote/ae;
.super Lcom/google/android/apps/youtube/app/remote/r;
.source "SourceFile"


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/concurrent/Executor;

.field private final d:Lcom/google/android/apps/ytremote/model/PairingCode;

.field private final e:Lcom/google/android/apps/ytremote/backend/logic/c;

.field private final f:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

.field private final g:Lcom/google/android/apps/youtube/app/remote/aq;

.field private final h:Lcom/google/android/apps/youtube/app/remote/ai;

.field private i:Landroid/app/PendingIntent;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/ytremote/model/PairingCode;Landroid/content/Context;Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Ljava/util/concurrent/Executor;Lcom/google/android/apps/ytremote/backend/logic/c;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p4, p5}, Lcom/google/android/apps/youtube/app/remote/r;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->f:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/PairingCode;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->d:Lcom/google/android/apps/ytremote/model/PairingCode;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->b:Landroid/content/Context;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->c:Ljava/util/concurrent/Executor;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/backend/logic/c;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->e:Lcom/google/android/apps/ytremote/backend/logic/c;

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/ah;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/remote/ah;-><init>(Lcom/google/android/apps/youtube/app/remote/ae;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->g:Lcom/google/android/apps/youtube/app/remote/aq;

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/ai;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/remote/ai;-><init>(Lcom/google/android/apps/youtube/app/remote/ae;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->h:Lcom/google/android/apps/youtube/app/remote/ai;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/ae;)Lcom/google/android/apps/ytremote/backend/logic/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->e:Lcom/google/android/apps/ytremote/backend/logic/c;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/ae;Lcom/google/android/apps/ytremote/model/PairingCode;Landroid/net/Uri;)V
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.youtube.action.mrp_pairing_code_registered"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "pairingCode"

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/PairingCode;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz v0, :cond_0

    const-string v2, "stopApplicationUri"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/ae;Lcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/ytremote/model/CloudScreen;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/af;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/app/remote/af;-><init>(Lcom/google/android/apps/youtube/app/remote/ae;Lcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not register pairing code"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/remote/ae;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->f:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    return-object v0
.end method

.method private c(I)Landroid/os/Bundle;
    .locals 3

    new-instance v0, Landroid/support/v7/media/b;

    invoke-direct {v0, p1}, Landroid/support/v7/media/b;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ae;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->t()D

    move-result-wide v1

    double-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/media/b;->b(J)Landroid/support/v7/media/b;

    move-result-object v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/media/b;->a(J)Landroid/support/v7/media/b;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/b;->a()Landroid/support/v7/media/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/a;->a()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/remote/ae;)Lcom/google/android/apps/ytremote/model/PairingCode;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->d:Lcom/google/android/apps/ytremote/model/PairingCode;

    return-object v0
.end method

.method private d()I
    .locals 3

    const/4 v0, 0x3

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/ag;->a:[I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/ae;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->n()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x7

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_6
    const/16 v0, 0x439

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/remote/ae;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->i:Landroid/app/PendingIntent;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.media.intent.extra.ITEM_ID"

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/ae;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.media.intent.extra.ITEM_STATUS"

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/ae;->d()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/remote/ae;->c(I)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ae;->i:Landroid/app/PendingIntent;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/ae;->b:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->i:Landroid/app/PendingIntent;
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not send status update"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/remote/ae;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->b:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;Landroid/support/v7/media/x;)Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    const-string v2, "android.media.intent.action.GET_STATUS"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/ae;->d()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/remote/ae;->c(I)Landroid/os/Bundle;

    move-result-object v0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "android.media.intent.extra.ITEM_ID"

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/ae;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/remote/bk;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Landroid/support/v7/media/x;->a(Landroid/os/Bundle;)V

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v2, "com.google.android.apps.youtube.app.remote.action.WATCH_STATUS"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v0, "android.media.intent.extra.ITEM_STATUS_UPDATE_RECEIVER"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->i:Landroid/app/PendingIntent;

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/ae;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->p()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/ae;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/e;->n()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->ADVERTISEMENT:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v2, v3, :cond_4

    move v2, v1

    :goto_1
    if-eqz v2, :cond_5

    :cond_3
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/ae;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/youtube/p;->bn:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2, v0}, Landroid/support/v7/media/x;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    move v0, v1

    goto :goto_0

    :cond_4
    move v2, v0

    goto :goto_1

    :cond_5
    const-string v2, "android.media.intent.action.RESUME"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->e()V

    move v0, v1

    goto :goto_0

    :cond_6
    const-string v2, "android.media.intent.action.PAUSE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->f()V

    move v0, v1

    goto :goto_0

    :cond_7
    const-string v2, "android.media.intent.action.SEEK"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "android.media.intent.extra.ITEM_POSITION"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    long-to-int v0, v2

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/ae;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->c(I)V

    move v0, v1

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 8

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ae;->f:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    const-string v2, ""

    const-wide/16 v3, 0x0

    const/4 v6, 0x0

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Ljava/lang/String;JLjava/lang/String;ILcom/google/android/apps/youtube/datalib/legacy/model/SubtitleTrack;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ae;->g:Lcom/google/android/apps/youtube/app/remote/aq;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/aq;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ae;->h:Lcom/google/android/apps/youtube/app/remote/ai;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/au;)V

    return-void
.end method

.method public final c()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/remote/r;->c()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ae;->g:Lcom/google/android/apps/youtube/app/remote/aq;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->b(Lcom/google/android/apps/youtube/app/remote/aq;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ae;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/ae;->h:Lcom/google/android/apps/youtube/app/remote/ai;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->b(Lcom/google/android/apps/youtube/app/remote/au;)V

    return-void
.end method
