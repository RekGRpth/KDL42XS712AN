.class final Lcom/google/android/apps/youtube/app/offline/a/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

.field final synthetic b:Lcom/google/android/apps/youtube/app/offline/a/x;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/offline/a/x;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/offline/a/z;->b:Lcom/google/android/apps/youtube/app/offline/a/x;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/offline/a/z;->a:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/z;->a:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/p;->c(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/z;->b:Lcom/google/android/apps/youtube/app/offline/a/x;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/offline/a/x;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/core/offline/store/i;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/z;->a:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/z;->a:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->COMPLETED:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/z;->b:Lcom/google/android/apps/youtube/app/offline/a/x;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/offline/a/x;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->v(Ljava/lang/String;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/z;->b:Lcom/google/android/apps/youtube/app/offline/a/x;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/offline/a/x;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/offline/a/f;->d(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/app/offline/a/af;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/offline/a/af;->c(Lcom/google/android/apps/youtube/app/offline/a/af;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/a/ag;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/z;->a:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/app/offline/a/ag;->a(Lcom/google/android/apps/youtube/app/offline/a/ag;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/ag;->b(Lcom/google/android/apps/youtube/app/offline/a/ag;)I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/z;->b:Lcom/google/android/apps/youtube/app/offline/a/x;

    iget-object v2, v2, Lcom/google/android/apps/youtube/app/offline/a/x;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/offline/a/f;->d(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/app/offline/a/af;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/a/ag;->c(Lcom/google/android/apps/youtube/app/offline/a/ag;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/app/offline/a/af;->b(Lcom/google/android/apps/youtube/app/offline/a/af;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/z;->a:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->RUNNING:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/z;->b:Lcom/google/android/apps/youtube/app/offline/a/x;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/offline/a/x;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/offline/a/f;->f(Lcom/google/android/apps/youtube/app/offline/a/f;)Lcom/google/android/apps/youtube/app/offline/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/offline/c;->a(Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/z;->b:Lcom/google/android/apps/youtube/app/offline/a/x;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/offline/a/x;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->u(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    return-void
.end method
