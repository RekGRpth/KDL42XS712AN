.class final Lcom/google/android/apps/youtube/app/remote/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/common/a/b;

.field final synthetic b:Lcom/google/android/apps/youtube/app/remote/t;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/remote/t;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/z;->b:Lcom/google/android/apps/youtube/app/remote/t;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/remote/z;->a:Lcom/google/android/apps/youtube/common/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/z;->b:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/t;->b(Lcom/google/android/apps/youtube/app/remote/t;)Landroid/util/Pair;

    move-result-object v3

    invoke-static {}, Lcom/google/android/apps/youtube/app/remote/t;->a()Landroid/util/Pair;

    move-result-object v0

    if-ne v3, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/z;->a:Lcom/google/android/apps/youtube/common/a/b;

    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Authentication failed."

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v7, v1}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/z;->b:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/t;->c(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/ytremote/backend/logic/a;

    move-result-object v2

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v0, v1}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-string v1, "SIGNED_OUT_USER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/z;->b:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/t;->c(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/ytremote/backend/logic/a;

    move-result-object v0

    const-string v1, "SIGNED_OUT_USER"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/CloudScreen;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/z;->b:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/t;->c(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/ytremote/backend/logic/a;

    move-result-object v6

    iget-object v1, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-interface {v6, v1, v2, v0}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/z;->a:Lcom/google/android/apps/youtube/common/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/z;->a:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, v7, v4}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method
