.class final Lcom/google/android/apps/youtube/app/ui/ap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/hp;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

.field private final b:Lcom/google/android/apps/youtube/app/offline/r;

.field private c:Ljava/lang/String;

.field private d:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/offline/r;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ap;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/r;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ap;->b:Lcom/google/android/apps/youtube/app/offline/r;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/offline/r;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/ap;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/offline/r;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/ap;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ap;->c:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/ap;->d:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ap;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ap;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ap;->b:Lcom/google/android/apps/youtube/app/offline/r;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/ap;->d:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/youtube/app/offline/r;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;Lcom/google/android/apps/youtube/app/offline/v;)V

    goto :goto_0
.end method
