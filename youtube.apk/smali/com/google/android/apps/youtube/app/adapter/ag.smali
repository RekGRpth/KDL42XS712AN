.class public final Lcom/google/android/apps/youtube/app/adapter/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/af;
    .locals 3

    invoke-static {p0, p2}, Lcom/google/android/apps/youtube/app/adapter/ag;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/aa;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/app/adapter/aa;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/adapter/ai;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/av;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/av;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;)V

    new-instance v2, Lcom/google/android/apps/youtube/app/adapter/v;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/app/adapter/v;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/x;

    invoke-direct {v1, p3}, Lcom/google/android/apps/youtube/app/adapter/x;-><init>(Lcom/google/android/apps/youtube/app/ui/v;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/af;

    sget v2, Lcom/google/android/youtube/l;->c:I

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/apps/youtube/app/adapter/af;-><init>(Landroid/content/Context;ILcom/google/android/apps/youtube/app/adapter/ai;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/af;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/youtube/app/adapter/bg;->a(Landroid/content/Context;)Lcom/google/android/apps/youtube/app/adapter/bg;

    move-result-object v0

    invoke-static {p0, p2, p3, p1}, Lcom/google/android/apps/youtube/app/adapter/al;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/core/client/bc;)Lcom/google/android/apps/youtube/app/adapter/al;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/app/adapter/v;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/app/adapter/v;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    if-eqz p4, :cond_0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/x;

    invoke-direct {v1, p4}, Lcom/google/android/apps/youtube/app/adapter/x;-><init>(Lcom/google/android/apps/youtube/app/ui/v;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    :cond_0
    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/aj;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/app/adapter/aj;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/adapter/ai;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/af;

    sget v2, Lcom/google/android/youtube/l;->bx:I

    invoke-direct {v0, p0, v2, v1}, Lcom/google/android/apps/youtube/app/adapter/af;-><init>(Landroid/content/Context;ILcom/google/android/apps/youtube/app/adapter/ai;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/af;
    .locals 3

    invoke-static {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/ag;->c(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/ai;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/af;

    sget v2, Lcom/google/android/youtube/l;->Q:I

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/apps/youtube/app/adapter/af;-><init>(Landroid/content/Context;ILcom/google/android/apps/youtube/app/adapter/ai;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;Z)Lcom/google/android/apps/youtube/app/adapter/af;
    .locals 4

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/ao;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/app/adapter/ao;-><init>(Landroid/content/Context;Z)V

    sget-object v1, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->LARGE:Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p0, p1, v1}, Lcom/google/android/apps/youtube/app/adapter/aq;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/apps/youtube/app/adapter/aq;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/app/adapter/x;

    invoke-direct {v2, p2}, Lcom/google/android/apps/youtube/app/adapter/x;-><init>(Lcom/google/android/apps/youtube/app/ui/v;)V

    new-instance v3, Lcom/google/android/apps/youtube/app/adapter/v;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/app/adapter/v;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/af;

    sget v2, Lcom/google/android/youtube/l;->P:I

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/apps/youtube/app/adapter/af;-><init>(Landroid/content/Context;ILcom/google/android/apps/youtube/app/adapter/ai;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/af;
    .locals 3

    invoke-static {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ag;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/v;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/app/adapter/v;-><init>()V

    new-instance v2, Lcom/google/android/apps/youtube/app/adapter/aa;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/youtube/app/adapter/aa;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/adapter/ai;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/x;

    invoke-direct {v1, p3}, Lcom/google/android/apps/youtube/app/adapter/x;-><init>(Lcom/google/android/apps/youtube/app/ui/v;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/av;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/youtube/app/adapter/av;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/client/bj;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/af;

    sget v2, Lcom/google/android/youtube/l;->c:I

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/apps/youtube/app/adapter/af;-><init>(Landroid/content/Context;ILcom/google/android/apps/youtube/app/adapter/ai;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;I)Lcom/google/android/apps/youtube/app/adapter/ai;
    .locals 4

    const/4 v0, 0x2

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/bj;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/app/adapter/bj;-><init>(Landroid/content/Context;I)V

    sget-object v0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->SMALL:Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/youtube/app/adapter/bq;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/apps/youtube/app/adapter/bq;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/youtube/app/adapter/x;

    invoke-direct {v2, p2}, Lcom/google/android/apps/youtube/app/adapter/x;-><init>(Lcom/google/android/apps/youtube/app/ui/v;)V

    new-instance v3, Lcom/google/android/apps/youtube/app/adapter/v;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/app/adapter/v;-><init>()V

    invoke-virtual {v3, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/youtube/app/adapter/d;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/app/adapter/d;-><init>()V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/ba;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/youtube/app/adapter/bg;->b(Landroid/content/Context;)Lcom/google/android/apps/youtube/app/adapter/bg;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->LARGE:Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p0, p1, v1}, Lcom/google/android/apps/youtube/app/adapter/bq;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/apps/youtube/app/adapter/bq;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/app/adapter/v;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/app/adapter/v;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/d;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/app/adapter/d;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/x;

    invoke-direct {v1, p2}, Lcom/google/android/apps/youtube/app/adapter/x;-><init>(Lcom/google/android/apps/youtube/app/ui/v;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/ay;

    invoke-direct {v1, p0, p3}, Lcom/google/android/apps/youtube/app/adapter/ay;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/ui/v;)V

    new-instance v2, Lcom/google/android/apps/youtube/app/adapter/bb;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/youtube/app/adapter/bb;-><init>(Lcom/google/android/apps/youtube/app/adapter/ai;Lcom/google/android/apps/youtube/app/adapter/ay;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/ba;

    sget v1, Lcom/google/android/youtube/l;->bu:I

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/youtube/app/adapter/ba;-><init>(Landroid/content/Context;ILcom/google/android/apps/youtube/app/adapter/bb;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/app/YouTubeApplication;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/gj;Landroid/app/Activity;)Lcom/google/android/apps/youtube/app/adapter/u;
    .locals 13

    new-instance v12, Lcom/google/android/apps/youtube/app/adapter/ah;

    sget v1, Lcom/google/android/youtube/j;->ah:I

    move-object/from16 v0, p8

    invoke-direct {v12, p0, v1, v0}, Lcom/google/android/apps/youtube/app/adapter/ah;-><init>(Landroid/content/Context;ILcom/google/android/apps/youtube/core/client/bj;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v1 .. v11}, Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/app/YouTubeApplication;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/ui/gj;Landroid/app/Activity;)V

    new-instance v2, Lcom/google/android/apps/youtube/app/adapter/v;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/app/adapter/v;-><init>()V

    invoke-virtual {v2, v12}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/youtube/app/adapter/u;

    sget v4, Lcom/google/android/youtube/l;->s:I

    invoke-direct {v3, p0, v4, v2, v1}, Lcom/google/android/apps/youtube/app/adapter/u;-><init>(Landroid/content/Context;ILcom/google/android/apps/youtube/app/adapter/ai;Lcom/google/android/apps/youtube/app/adapter/ChannelStoreItemRendererFactory;)V

    return-object v3
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;)Lcom/google/android/apps/youtube/app/adapter/v;
    .locals 4

    invoke-static {p0}, Lcom/google/android/apps/youtube/app/adapter/bg;->a(Landroid/content/Context;)Lcom/google/android/apps/youtube/app/adapter/bg;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->LARGE:Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p0, p1, v1}, Lcom/google/android/apps/youtube/app/adapter/bq;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/apps/youtube/app/adapter/bq;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/app/adapter/d;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/app/adapter/d;-><init>()V

    new-instance v3, Lcom/google/android/apps/youtube/app/adapter/v;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/app/adapter/v;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/af;
    .locals 3

    invoke-static {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ag;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/x;

    invoke-direct {v1, p2}, Lcom/google/android/apps/youtube/app/adapter/x;-><init>(Lcom/google/android/apps/youtube/app/ui/v;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/af;

    sget v2, Lcom/google/android/youtube/l;->bx:I

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/apps/youtube/app/adapter/af;-><init>(Landroid/content/Context;ILcom/google/android/apps/youtube/app/adapter/ai;)V

    return-object v1
.end method

.method public static c(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/ai;
    .locals 4

    invoke-static {p0}, Lcom/google/android/apps/youtube/app/adapter/bg;->b(Landroid/content/Context;)Lcom/google/android/apps/youtube/app/adapter/bg;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->SMALL:Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p0, p1, v1}, Lcom/google/android/apps/youtube/app/adapter/bq;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/apps/youtube/app/adapter/bq;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/app/adapter/x;

    invoke-direct {v2, p2}, Lcom/google/android/apps/youtube/app/adapter/x;-><init>(Lcom/google/android/apps/youtube/app/ui/v;)V

    new-instance v3, Lcom/google/android/apps/youtube/app/adapter/v;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/app/adapter/v;-><init>()V

    invoke-virtual {v3, v0}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/youtube/app/adapter/d;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/app/adapter/d;-><init>()V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    return-object v0
.end method

.method public static d(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/v;)Lcom/google/android/apps/youtube/app/adapter/af;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/youtube/app/adapter/bg;->a(Landroid/content/Context;)Lcom/google/android/apps/youtube/app/adapter/bg;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->LARGE:Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    invoke-static {p0, p1, v1}, Lcom/google/android/apps/youtube/app/adapter/bq;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)Lcom/google/android/apps/youtube/app/adapter/bq;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/app/adapter/v;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/app/adapter/v;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/d;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/app/adapter/d;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/x;

    invoke-direct {v1, p2}, Lcom/google/android/apps/youtube/app/adapter/x;-><init>(Lcom/google/android/apps/youtube/app/ui/v;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/adapter/v;->a(Lcom/google/android/apps/youtube/app/adapter/ai;)Lcom/google/android/apps/youtube/app/adapter/v;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/adapter/af;

    sget v2, Lcom/google/android/youtube/l;->bx:I

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/apps/youtube/app/adapter/af;-><init>(Landroid/content/Context;ILcom/google/android/apps/youtube/app/adapter/ai;)V

    return-object v1
.end method
