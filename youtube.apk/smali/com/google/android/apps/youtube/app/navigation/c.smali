.class public final Lcom/google/android/apps/youtube/app/navigation/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/navigation/b;


# static fields
.field static final a:Ljava/util/Set;


# instance fields
.field private final b:Lcom/google/android/apps/youtube/app/GuideActivity;

.field private final c:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/navigation/c;->a:Ljava/util/Set;

    const-string v1, "Autos"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/youtube/app/navigation/c;->a:Ljava/util/Set;

    const-string v1, "Comedy"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/youtube/app/navigation/c;->a:Ljava/util/Set;

    const-string v1, "Education"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/youtube/app/navigation/c;->a:Ljava/util/Set;

    const-string v1, "Entertainment"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/youtube/app/navigation/c;->a:Ljava/util/Set;

    const-string v1, "Film"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/youtube/app/navigation/c;->a:Ljava/util/Set;

    const-string v1, "Games"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/youtube/app/navigation/c;->a:Ljava/util/Set;

    const-string v1, "Music"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/youtube/app/navigation/c;->a:Ljava/util/Set;

    const-string v1, "News"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/youtube/app/navigation/c;->a:Ljava/util/Set;

    const-string v1, "Nonprofit"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/youtube/app/navigation/c;->a:Ljava/util/Set;

    const-string v1, "People"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/youtube/app/navigation/c;->a:Ljava/util/Set;

    const-string v1, "Animals"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/youtube/app/navigation/c;->a:Ljava/util/Set;

    const-string v1, "Tech"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/youtube/app/navigation/c;->a:Ljava/util/Set;

    const-string v1, "Sports"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/youtube/app/navigation/c;->a:Ljava/util/Set;

    const-string v1, "Howto"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/apps/youtube/app/navigation/c;->a:Ljava/util/Set;

    const-string v1, "Travel"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/GuideActivity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/navigation/c;->b:Lcom/google/android/apps/youtube/app/GuideActivity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/navigation/c;->c:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    return-void
.end method

.method public static a(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/a/a/a/a/kz;)Lcom/google/android/apps/youtube/app/navigation/c;
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->r:Lcom/google/a/a/a/a/aa;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/aa;

    iget-object v0, v0, Lcom/google/a/a/a/a/aa;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->d(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->setNavigationEndpoint(Lcom/google/a/a/a/a/kz;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/navigation/c;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/app/navigation/c;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    return-object v1
.end method

.method public static a(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/a/a/a/a/kz;Z)Lcom/google/android/apps/youtube/app/navigation/c;
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->o:Lcom/google/a/a/a/a/jx;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/jx;

    iget-object v0, v0, Lcom/google/a/a/a/a/jx;->b:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(Ljava/lang/String;Z)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->setNavigationEndpoint(Lcom/google/a/a/a/a/kz;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/navigation/c;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/app/navigation/c;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    return-object v1
.end method

.method public static b(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/a/a/a/a/kz;)Lcom/google/android/apps/youtube/app/navigation/c;
    .locals 4

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->m:Lcom/google/a/a/a/a/bj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/bj;

    iget-object v0, v0, Lcom/google/a/a/a/a/bj;->b:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/youtube/app/navigation/c;->a:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->setNavigationEndpoint(Lcom/google/a/a/a/a/kz;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/navigation/c;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/app/navigation/c;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    return-object v1

    :cond_0
    new-instance v1, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown category name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static b(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/a/a/a/a/kz;Z)Lcom/google/android/apps/youtube/app/navigation/c;
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->c:Lcom/google/a/a/a/a/am;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/am;

    iget-object v0, v0, Lcom/google/a/a/a/a/am;->b:Ljava/lang/String;

    invoke-static {v0, p2}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->b(Ljava/lang/String;Z)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->setNavigationEndpoint(Lcom/google/a/a/a/a/kz;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/navigation/c;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/app/navigation/c;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    return-object v1
.end method

.method public static c(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/a/a/a/a/kz;)Lcom/google/android/apps/youtube/app/navigation/c;
    .locals 4

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->p:Lcom/google/a/a/a/a/jz;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/jz;

    iget-object v0, v0, Lcom/google/a/a/a/a/jz;->b:Ljava/lang/String;

    const-string v1, "uploads"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->e()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    :goto_0
    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->setNavigationEndpoint(Lcom/google/a/a/a/a/kz;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/navigation/c;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/app/navigation/c;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    return-object v1

    :cond_0
    const-string v1, "favorites"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->f()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v1, "watch_later"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->h()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v1, "history"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->d()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v1, "playlists"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->g()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    goto :goto_0

    :cond_4
    const-string v1, "purchases"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v0, "Purchases V2 User Feed requested but not supported"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v1, "Purchases V2 User Feed requested but not supported"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    const-string v1, "subscriptions"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->b()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    goto :goto_0

    :cond_6
    const-string v1, "live"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->c()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    goto :goto_0

    :cond_7
    new-instance v1, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown V2 User Feed requested: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static d(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/a/a/a/a/kz;)Lcom/google/android/apps/youtube/app/navigation/c;
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->g:Lcom/google/a/a/a/a/qf;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/qf;

    iget-object v0, v0, Lcom/google/a/a/a/a/qf;->b:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/youtube/app/search/SearchType;->DEFAULT_SEARCH_TYPE:Lcom/google/android/apps/youtube/app/search/SearchType;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(Lcom/google/android/apps/youtube/app/search/SearchType;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->setNavigationEndpoint(Lcom/google/a/a/a/a/kz;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/navigation/c;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/app/navigation/c;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/navigation/c;->b:Lcom/google/android/apps/youtube/app/GuideActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/navigation/c;->c:Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    return-void
.end method
