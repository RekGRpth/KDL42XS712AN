.class final Lcom/google/android/apps/youtube/app/ui/be;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/EditText;

.field final synthetic b:Landroid/widget/CheckBox;

.field final synthetic c:Lcom/google/android/apps/youtube/app/ui/bg;

.field final synthetic d:Lcom/google/android/apps/youtube/app/ui/bd;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/ui/bd;Landroid/widget/EditText;Landroid/widget/CheckBox;Lcom/google/android/apps/youtube/app/ui/bg;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/be;->d:Lcom/google/android/apps/youtube/app/ui/bd;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/be;->a:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/be;->b:Landroid/widget/CheckBox;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/ui/be;->c:Lcom/google/android/apps/youtube/app/ui/bg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/be;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/be;->d:Lcom/google/android/apps/youtube/app/ui/bd;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ui/bd;->d(Lcom/google/android/apps/youtube/app/ui/bd;)Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/be;->b:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/be;->d:Lcom/google/android/apps/youtube/app/ui/bd;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/ui/bd;->a(Lcom/google/android/apps/youtube/app/ui/bd;)Landroid/app/Activity;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/youtube/app/ui/bf;

    invoke-direct {v4, p0}, Lcom/google/android/apps/youtube/app/ui/bf;-><init>(Lcom/google/android/apps/youtube/app/ui/be;)V

    invoke-static {v3, v4}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/youtube/core/client/bc;->a(Ljava/lang/String;ZLcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_0
.end method
