.class public Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;
.super Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;
.source "SourceFile"


# static fields
.field private static final Y:Ljava/lang/String;


# instance fields
.field private Z:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

.field private aa:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".playlist"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->Y:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;)Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->aa:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->aa:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->Z:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->aa:Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->Z:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;->a(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->Z:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->Z:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->Y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->Z:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/an;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/fragments/an;-><init>(Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;)V

    new-instance v2, Lcom/google/android/apps/youtube/core/ui/aa;

    invoke-direct {v2, v0}, Lcom/google/android/apps/youtube/core/ui/aa;-><init>(Landroid/content/Context;)V

    const-string v0, ""

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/core/ui/aa;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x1040013    # android.R.string.yes

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x1040009    # android.R.string.no

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;->e(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->Z:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->Y:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->Z:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    return-void
.end method

.method public final r()V
    .locals 5

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;->r()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->Z:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->b()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    sget v1, Lcom/google/android/youtube/p;->aL:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/MyPlaylistsFragment$DeletePlaylistDialogFragment;->Z:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v4, v4, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->title:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
