.class public final Lcom/google/android/apps/youtube/app/ak;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/apps/youtube/datalib/config/b;

.field public static final b:[B

.field public static final c:[B

.field public static final d:Landroid/util/SparseArray;

.field private static final e:[B

.field private static final f:[B

.field private static final g:[B


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v1, 0x20

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/app/al;

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/app/al;-><init>(B)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ak;->a:Lcom/google/android/apps/youtube/datalib/config/b;

    const/16 v0, 0x62

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/youtube/app/ak;->b:[B

    const/16 v0, 0x18

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/youtube/app/ak;->c:[B

    new-array v0, v1, [B

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/apps/youtube/app/ak;->e:[B

    new-array v0, v1, [B

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/apps/youtube/app/ak;->f:[B

    const/16 v0, 0x21

    new-array v0, v0, [B

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/apps/youtube/app/ak;->g:[B

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/app/ak;->d:Landroid/util/SparseArray;

    sget-object v1, Lcom/google/android/apps/youtube/app/ak;->e:[B

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/ak;->d:Landroid/util/SparseArray;

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/youtube/app/ak;->f:[B

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/ak;->d:Landroid/util/SparseArray;

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/youtube/app/ak;->g:[B

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void

    :array_0
    .array-data 1
        0x41t
        0x49t
        0x33t
        0x39t
        0x73t
        0x69t
        0x34t
        0x6ft
        0x4bt
        0x74t
        0x4ct
        0x4dt
        0x61t
        0x64t
        0x5at
        0x43t
        0x55t
        0x76t
        0x4et
        0x70t
        0x7at
        0x64t
        0x68t
        0x52t
        0x74t
        0x5ft
        0x38t
        0x42t
        0x62t
        0x73t
        0x70t
        0x4ct
        0x33t
        0x67t
        0x4bt
        0x52t
        0x4ft
        0x7at
        0x79t
        0x53t
        0x6bt
        0x4bt
        0x4at
        0x50t
        0x6at
        0x64t
        0x42t
        0x53t
        0x54t
        0x59t
        0x67t
        0x46t
        0x49t
        0x4bt
        0x47t
        0x61t
        0x54t
        0x4et
        0x51t
        0x6ct
        0x68t
        0x44t
        0x63t
        0x61t
        0x6ft
        0x56t
        0x37t
        0x68t
        0x78t
        0x5at
        0x6at
        0x4ft
        0x39t
        0x61t
        0x42t
        0x6at
        0x6ft
        0x43t
        0x5ft
        0x33t
        0x37t
        0x6et
        0x69t
        0x73t
        0x70t
        0x52t
        0x4bt
        0x79t
        0x73t
        0x69t
        0x5ft
        0x59t
        0x66t
        0x52t
        0x4at
        0x76t
        0x33t
        0x77t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x74t
        0x6ct
        0x39t
        0x52t
        0x53t
        0x67t
        0x45t
        0x31t
        0x64t
        0x53t
        0x30t
        0x62t
        0x4ct
        0x4at
        0x42t
        0x64t
        0x30t
        0x43t
        0x39t
        0x7at
        0x62t
        0x41t
        0x3dt
        0x3dt
    .end array-data

    :array_2
    .array-data 1
        0x6t
        -0x32t
        0x27t
        0x62t
        -0x68t
        0x6ct
        0x6ft
        -0x65t
        0x2dt
        0x10t
        0x9t
        -0x3ct
        -0x29t
        -0x11t
        -0x70t
        0x56t
        0x51t
        -0x73t
        -0x5et
        -0x3dt
        -0x55t
        -0x3t
        -0x1at
        0x1dt
        0x42t
        -0x19t
        -0x48t
        0x5t
        -0x6et
        -0x5dt
        -0x30t
        0x66t
    .end array-data

    :array_3
    .array-data 1
        0x6t
        -0x32t
        0x27t
        0x62t
        -0x68t
        0x6ct
        0x6ft
        -0x65t
        0x2dt
        0x10t
        0x9t
        -0x3ct
        -0x29t
        -0x11t
        -0x70t
        0x56t
        0x51t
        -0x73t
        -0x5et
        -0x3dt
        -0x55t
        -0x3t
        -0x1at
        0x1dt
        0x42t
        -0x19t
        -0x48t
        0x5t
        -0x6et
        -0x5dt
        -0x30t
        0x66t
    .end array-data

    :array_4
    .array-data 1
        -0x11t
        -0x37t
        0x3ct
        -0x3at
        0x1t
        0x4t
        -0x5bt
        -0x1ft
        -0x41t
        -0x43t
        -0x6at
        0x1bt
        0x7ct
        -0x5bt
        0x6bt
        0x6ft
        0xat
        -0x45t
        -0x73t
        -0x39t
        0x4ft
        0x4at
        -0x73t
        0x46t
        -0x7bt
        0x57t
        -0x69t
        -0x47t
        0x65t
        0x48t
        0xat
        -0x4t
        -0x3at
    .end array-data
.end method
