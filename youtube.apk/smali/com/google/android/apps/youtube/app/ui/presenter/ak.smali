.class public final Lcom/google/android/apps/youtube/app/ui/presenter/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/g;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

.field private final b:Lcom/google/android/apps/youtube/uilib/a/i;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/uilib/a/i;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/a/i;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ak;->b:Lcom/google/android/apps/youtube/uilib/a/i;

    sget v0, Lcom/google/android/youtube/l;->aZ:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ak;->a:Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ak;->a:Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 2

    check-cast p2, Lcom/google/android/apps/youtube/uilib/innertube/l;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ak;->a:Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/l;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/ak;->b:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Lcom/google/android/apps/youtube/uilib/a/f;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
