.class public final Lcom/google/android/apps/youtube/app/navigation/AppNavigator;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/d/a;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/GuideActivity;

.field private final b:Lcom/google/android/apps/youtube/common/c/a;

.field private final c:Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/common/c/a;)V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;->FROM_OTHER:Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/GuideActivity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->a:Lcom/google/android/apps/youtube/app/GuideActivity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->b:Lcom/google/android/apps/youtube/common/c/a;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->c:Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;

    return-void
.end method

.method public static a(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/common/c/a;)Lcom/google/android/apps/youtube/app/navigation/AppNavigator;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;

    sget-object v1, Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;->FROM_GUIDE:Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;)V

    return-object v0
.end method

.method public static b(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/common/c/a;)Lcom/google/android/apps/youtube/app/navigation/AppNavigator;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;

    sget-object v1, Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;->FROM_WATCH:Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;)V

    return-object v0
.end method

.method public static b(Lcom/google/a/a/a/a/kz;)[B
    .locals 1

    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/a/a/a/a/kz;->b:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/a/a/a/a/kz;->b:[B

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/b;->a:[B

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/a/a/a/a/kz;)V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->a:Lcom/google/android/apps/youtube/app/GuideActivity;

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->c:Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;

    sget-object v3, Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;->FROM_GUIDE:Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;

    if-ne v2, v3, :cond_0

    move v3, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->c:Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;

    sget-object v4, Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;->FROM_WATCH:Lcom/google/android/apps/youtube/app/navigation/AppNavigator$Origin;

    if-eq v2, v4, :cond_1

    move v2, v1

    :goto_1
    iget-object v4, p1, Lcom/google/a/a/a/a/kz;->n:Lcom/google/a/a/a/a/y;

    if-eqz v4, :cond_2

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v2, "Settings not supported"

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->a:Lcom/google/android/apps/youtube/app/GuideActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/youtube/core/utils/ah;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    :goto_2
    return-void

    :cond_0
    move v3, v0

    goto :goto_0

    :cond_1
    move v2, v0

    goto :goto_1

    :cond_2
    :try_start_1
    iget-object v4, p1, Lcom/google/a/a/a/a/kz;->r:Lcom/google/a/a/a/a/aa;

    if-eqz v4, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->a:Lcom/google/android/apps/youtube/app/GuideActivity;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/navigation/c;->a(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/a/a/a/a/kz;)Lcom/google/android/apps/youtube/app/navigation/c;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/navigation/b;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->b:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v2, Lcom/google/android/apps/youtube/app/navigation/a;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v3}, Lcom/google/android/apps/youtube/app/navigation/a;-><init>(Lcom/google/a/a/a/a/kz;B)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    goto :goto_2

    :cond_3
    iget-object v4, p1, Lcom/google/a/a/a/a/kz;->c:Lcom/google/a/a/a/a/am;

    if-eqz v4, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->a:Lcom/google/android/apps/youtube/app/GuideActivity;

    invoke-static {v0, p1, v3}, Lcom/google/android/apps/youtube/app/navigation/c;->b(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/a/a/a/a/kz;Z)Lcom/google/android/apps/youtube/app/navigation/c;

    move-result-object v0

    goto :goto_3

    :cond_4
    iget-object v4, p1, Lcom/google/a/a/a/a/kz;->q:Lcom/google/a/a/a/a/bi;

    if-eqz v4, :cond_5

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v2, "Capture not supported"

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iget-object v4, p1, Lcom/google/a/a/a/a/kz;->m:Lcom/google/a/a/a/a/bj;

    if-eqz v4, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->a:Lcom/google/android/apps/youtube/app/GuideActivity;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/navigation/c;->b(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/a/a/a/a/kz;)Lcom/google/android/apps/youtube/app/navigation/c;

    move-result-object v0

    goto :goto_3

    :cond_6
    iget-object v4, p1, Lcom/google/a/a/a/a/kz;->d:Lcom/google/a/a/a/a/dr;

    if-eqz v4, :cond_7

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v2, "Create Channel not supported"

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    iget-object v4, p1, Lcom/google/a/a/a/a/kz;->e:Lcom/google/a/a/a/a/fw;

    if-eqz v4, :cond_8

    invoke-static {}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->setNavigationEndpoint(Lcom/google/a/a/a/a/kz;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/navigation/c;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->a:Lcom/google/android/apps/youtube/app/GuideActivity;

    invoke-direct {v0, v3, v2}, Lcom/google/android/apps/youtube/app/navigation/c;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    goto :goto_3

    :cond_8
    iget-object v4, p1, Lcom/google/a/a/a/a/kz;->f:Lcom/google/a/a/a/a/hm;

    if-eqz v4, :cond_9

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v2, "Capture not supported"

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    iget-object v4, p1, Lcom/google/a/a/a/a/kz;->o:Lcom/google/a/a/a/a/jx;

    if-eqz v4, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->a:Lcom/google/android/apps/youtube/app/GuideActivity;

    invoke-static {v0, p1, v3}, Lcom/google/android/apps/youtube/app/navigation/c;->a(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/a/a/a/a/kz;Z)Lcom/google/android/apps/youtube/app/navigation/c;

    move-result-object v0

    goto :goto_3

    :cond_a
    iget-object v3, p1, Lcom/google/a/a/a/a/kz;->t:Lcom/google/a/a/a/a/jy;

    if-eqz v3, :cond_b

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v2, "MobileV2Playlist not supported"

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    iget-object v3, p1, Lcom/google/a/a/a/a/kz;->p:Lcom/google/a/a/a/a/jz;

    if-eqz v3, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->a:Lcom/google/android/apps/youtube/app/GuideActivity;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/navigation/c;->c(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/a/a/a/a/kz;)Lcom/google/android/apps/youtube/app/navigation/c;

    move-result-object v0

    goto :goto_3

    :cond_c
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->a:Lcom/google/android/apps/youtube/app/GuideActivity;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/utils/p;->a(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_d

    iget-object v3, p1, Lcom/google/a/a/a/a/kz;->s:Lcom/google/a/a/a/a/ls;

    if-eqz v3, :cond_d

    invoke-static {}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->i()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;->setNavigationEndpoint(Lcom/google/a/a/a/a/kz;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/navigation/c;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->a:Lcom/google/android/apps/youtube/app/GuideActivity;

    invoke-direct {v0, v3, v2}, Lcom/google/android/apps/youtube/app/navigation/c;-><init>(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    goto/16 :goto_3

    :cond_d
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->a:Lcom/google/android/apps/youtube/app/GuideActivity;

    invoke-static {v3}, Lcom/google/android/apps/youtube/core/utils/p;->a(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_f

    iget-object v3, p1, Lcom/google/a/a/a/a/kz;->w:Lcom/google/a/a/a/a/me;

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->a:Lcom/google/android/apps/youtube/app/GuideActivity;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->OFFLINE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-direct {v4, p1, v5}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Lcom/google/a/a/a/a/kz;Lcom/google/android/apps/youtube/core/client/WatchFeature;)V

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->setOffline(Z)V

    new-instance v5, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-direct {v5, v4}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;-><init>(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    if-nez v2, :cond_e

    move v0, v1

    :cond_e
    invoke-virtual {v5, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->setKeepHistory(Z)V

    new-instance v0, Lcom/google/android/apps/youtube/app/navigation/e;

    invoke-direct {v0, v3, v5}, Lcom/google/android/apps/youtube/app/navigation/e;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    goto/16 :goto_3

    :cond_f
    iget-object v3, p1, Lcom/google/a/a/a/a/kz;->j:Lcom/google/a/a/a/a/pm;

    if-eqz v3, :cond_10

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v2, "Purchases not supported"

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    iget-object v3, p1, Lcom/google/a/a/a/a/kz;->g:Lcom/google/a/a/a/a/qf;

    if-eqz v3, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->a:Lcom/google/android/apps/youtube/app/GuideActivity;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/navigation/c;->d(Lcom/google/android/apps/youtube/app/GuideActivity;Lcom/google/a/a/a/a/kz;)Lcom/google/android/apps/youtube/app/navigation/c;

    move-result-object v0

    goto/16 :goto_3

    :cond_11
    iget-object v3, p1, Lcom/google/a/a/a/a/kz;->l:Lcom/google/a/a/a/a/ri;

    if-eqz v3, :cond_12

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v2, "Sign in not supported"

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    iget-object v3, p1, Lcom/google/a/a/a/a/kz;->h:Lcom/google/a/a/a/a/sj;

    if-eqz v3, :cond_13

    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v2, "Subscription Manager not supported"

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_13
    iget-object v3, p1, Lcom/google/a/a/a/a/kz;->k:Lcom/google/a/a/a/a/tu;

    if-eqz v3, :cond_14

    iget-object v0, p1, Lcom/google/a/a/a/a/kz;->k:Lcom/google/a/a/a/a/tu;

    iget-object v0, v0, Lcom/google/a/a/a/a/tu;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/youtube/app/navigation/d;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->a:Lcom/google/android/apps/youtube/app/GuideActivity;

    invoke-direct {v0, v3, v2}, Lcom/google/android/apps/youtube/app/navigation/d;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    goto/16 :goto_3

    :cond_14
    iget-object v3, p1, Lcom/google/a/a/a/a/kz;->i:Lcom/google/a/a/a/a/wb;

    if-nez v3, :cond_15

    iget-object v3, p1, Lcom/google/a/a/a/a/kz;->u:Lcom/google/a/a/a/a/wl;

    if-eqz v3, :cond_17

    :cond_15
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/navigation/AppNavigator;->a:Lcom/google/android/apps/youtube/app/GuideActivity;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->NO_FEATURE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-direct {v4, p1, v5}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Lcom/google/a/a/a/a/kz;Lcom/google/android/apps/youtube/core/client/WatchFeature;)V

    new-instance v5, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-direct {v5, v4}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;-><init>(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    if-nez v2, :cond_16

    move v0, v1

    :cond_16
    invoke-virtual {v5, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->setKeepHistory(Z)V

    new-instance v0, Lcom/google/android/apps/youtube/app/navigation/e;

    invoke-direct {v0, v3, v5}, Lcom/google/android/apps/youtube/app/navigation/e;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    goto/16 :goto_3

    :cond_17
    new-instance v0, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;

    const-string v2, "Unknown NavigationData encountered"

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcom/google/android/apps/youtube/core/navigation/UnknownNavigationException; {:try_start_1 .. :try_end_1} :catch_0
.end method
