.class public final Lcom/google/android/apps/youtube/app/adapter/aa;
.super Lcom/google/android/apps/youtube/app/adapter/g;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Lcom/google/android/apps/youtube/app/adapter/ai;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/adapter/ai;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/adapter/g;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/aa;->a:Landroid/content/res/Resources;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/adapter/ai;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/aa;->b:Lcom/google/android/apps/youtube/app/adapter/ai;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/aa;)Lcom/google/android/apps/youtube/app/adapter/ai;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/aa;->b:Lcom/google/android/apps/youtube/app/adapter/ai;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/adapter/aa;)Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/aa;->a:Landroid/content/res/Resources;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/apps/youtube/app/adapter/ae;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/ac;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/apps/youtube/app/adapter/ac;-><init>(Lcom/google/android/apps/youtube/app/adapter/aa;Landroid/view/View;Landroid/view/ViewGroup;B)V

    return-object v0
.end method
