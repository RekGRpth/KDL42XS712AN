.class public final Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;
.super Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;
.source "SourceFile"


# instance fields
.field private Y:Lcom/google/android/apps/youtube/app/honeycomb/phone/x;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;)Lcom/google/android/apps/youtube/app/honeycomb/phone/x;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;->Y:Lcom/google/android/apps/youtube/app/honeycomb/phone/x;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/app/honeycomb/phone/x;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;->Y:Lcom/google/android/apps/youtube/app/honeycomb/phone/x;

    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    const/4 v6, 0x0

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->aV:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;->h()Landroid/os/Bundle;

    move-result-object v3

    const-string v1, "YouTubeScreen"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    sget v4, Lcom/google/android/youtube/p;->fi:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreenName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v4, Lcom/google/android/youtube/p;->dA:I

    new-instance v5, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;

    invoke-direct {v5, p0, v0, v3, v6}, Lcom/google/android/apps/youtube/app/honeycomb/phone/z;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;Landroid/widget/EditText;Landroid/os/Bundle;B)V

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v3, Lcom/google/android/youtube/p;->K:I

    new-instance v4, Lcom/google/android/apps/youtube/app/honeycomb/phone/w;

    invoke-direct {v4, p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/w;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;Landroid/widget/EditText;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/app/honeycomb/phone/aa;

    invoke-direct {v2, p0, v6}, Lcom/google/android/apps/youtube/app/honeycomb/phone/aa;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;B)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    return-object v1
.end method

.method public final d()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;->d()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$RenameScreenDialogFragment;->b()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method
