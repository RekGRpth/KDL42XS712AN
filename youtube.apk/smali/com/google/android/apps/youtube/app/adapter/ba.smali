.class public final Lcom/google/android/apps/youtube/app/adapter/ba;
.super Lcom/google/android/apps/youtube/app/adapter/af;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/adapter/bb;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/google/android/apps/youtube/app/adapter/bb;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/af;-><init>(Landroid/content/Context;ILcom/google/android/apps/youtube/app/adapter/ai;)V

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/adapter/ba;->a:Lcom/google/android/apps/youtube/app/adapter/bb;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Z
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ba;->a:Lcom/google/android/apps/youtube/app/adapter/bb;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/adapter/bb;->a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/ha;->a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Lcom/google/android/apps/youtube/app/ui/ha;

    move-result-object v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/adapter/ba;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/adapter/ba;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/ha;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/ui/ha;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1, v3}, Lcom/google/android/apps/youtube/app/adapter/ba;->b(ILjava/lang/Object;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return v2
.end method
