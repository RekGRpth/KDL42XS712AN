.class Lcom/google/android/apps/youtube/app/honeycomb/ui/a;
.super Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field protected a:Lcom/google/android/apps/youtube/app/compat/q;

.field protected b:Landroid/widget/SearchView;

.field protected c:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

.field protected d:Z

.field protected final e:Ljava/util/List;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/am;Ljava/lang/String;Lcom/google/android/apps/youtube/app/compat/o;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/am;Ljava/lang/String;Lcom/google/android/apps/youtube/app/compat/o;)V

    sget-object v0, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->ICONIFIED:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->e:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/ui/a;Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/app/am;->c(Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x10102eb    # android.R.attr.actionBarSize

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return v1
.end method

.method public final a(Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->invalidateOptionsMenu()V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/honeycomb/ui/f;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->a:Lcom/google/android/apps/youtube/app/compat/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->g:Lcom/google/android/apps/youtube/core/Analytics;

    const-string v2, "Search"

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/SearchView;->setSubmitButtonEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setIconified(Z)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/youtube/app/compat/j;)Z
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/apps/youtube/app/compat/j;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v3, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-ne v0, v3, :cond_0

    sget v0, Lcom/google/android/youtube/j;->cy:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/app/compat/j;->f(I)V

    :goto_0
    return v1

    :cond_0
    sget v0, Lcom/google/android/youtube/j;->cy:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/youtube/app/compat/j;->c(I)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->a:Lcom/google/android/apps/youtube/app/compat/q;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->a:Lcom/google/android/apps/youtube/app/compat/q;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/compat/q;->d()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    const-string v3, "search"

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->f:Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setQueryRefinementEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    new-instance v3, Lcom/google/android/apps/youtube/app/honeycomb/ui/b;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/b;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/ui/a;)V

    invoke-virtual {v0, v3}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    new-instance v3, Lcom/google/android/apps/youtube/app/honeycomb/ui/c;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/c;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/ui/a;)V

    invoke-virtual {v0, v3}, Landroid/widget/SearchView;->setOnSuggestionListener(Landroid/widget/SearchView$OnSuggestionListener;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v3, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->CUSTOM:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->a:Lcom/google/android/apps/youtube/app/compat/q;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/compat/q;->c()Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setFocusable(Z)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v4, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->ICONIFIED:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->a:Lcom/google/android/apps/youtube/app/compat/q;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v3, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->ICONIFIED:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-ne v0, v3, :cond_3

    const/16 v0, 0xa

    :goto_2
    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/app/compat/q;->b(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->hasFocus()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->d:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->setOnQueryTextFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    const/4 v0, 0x2

    goto :goto_2
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    const-string v1, ""

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/app/honeycomb/ui/f;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final c()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->isIconified()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->a:Lcom/google/android/apps/youtube/app/compat/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setIconified(Z)V

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v1, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->ICONIFIED:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->d()V

    :cond_0
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->c:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v1, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->a(Ljava/lang/String;Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    if-ne p1, v0, :cond_1

    iput-boolean p2, p0, Lcom/google/android/apps/youtube/app/honeycomb/ui/a;->d:Z

    :cond_1
    return-void
.end method
