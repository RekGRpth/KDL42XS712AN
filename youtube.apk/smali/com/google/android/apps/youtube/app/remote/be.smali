.class final Lcom/google/android/apps/youtube/app/remote/be;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/notification/j;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/remote/bb;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/remote/bb;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/remote/bb;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/be;-><init>(Lcom/google/android/apps/youtube/app/remote/bb;)V

    return-void
.end method

.method private i()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->n()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->ADVERTISEMENT:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->b(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->h()Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->k()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->a(ZZ)V

    :cond_0
    return-void
.end method

.method public final a(J)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    long-to-int v1, p1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->c(I)V

    return-void
.end method

.method public final b()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/be;->i()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->e()V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/be;->i()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->f()V

    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->n()Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->PLAYING:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->f()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/remote/be;->i()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->e()V

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->i()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->c(I)V

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->m()V

    :cond_0
    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->g()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->b(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/notification/PlaybackControllerGroup;->b()V

    return-void
.end method

.method public final h()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/be;->a:Lcom/google/android/apps/youtube/app/remote/bb;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bb;->a(Lcom/google/android/apps/youtube/app/remote/bb;)Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->c(I)V

    return-void
.end method
