.class public Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;
.super Lcom/google/android/apps/youtube/core/transfer/TransferService;
.source "SourceFile"


# instance fields
.field private b:Ljava/security/Key;

.field private c:Lcom/google/android/apps/youtube/datalib/innertube/ag;

.field private d:Lcom/google/android/apps/youtube/common/cache/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/transfer/h;)Lcom/google/android/apps/youtube/core/utils/w;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/apps/youtube/core/transfer/h;)Lcom/google/android/apps/youtube/core/utils/w;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/BaseApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/BaseApplication;->y()Lcom/google/android/apps/youtube/core/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/a;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->a:Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/transfer/TransfersExecutor;->a(Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;Lcom/google/android/apps/youtube/core/transfer/m;)Lcom/google/android/apps/youtube/core/transfer/l;
    .locals 14

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v1

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->o()Lcom/google/android/apps/youtube/datalib/innertube/ah;

    move-result-object v10

    new-instance v7, Lcom/google/android/apps/youtube/common/e/n;

    invoke-direct {v7}, Lcom/google/android/apps/youtube/common/e/n;-><init>()V

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v2

    const/4 v0, 0x0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/core/offline/store/q;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->d()Lcom/google/android/apps/youtube/core/offline/store/i;

    move-result-object v0

    move-object v8, v0

    :goto_0
    new-instance v0, Lcom/google/android/apps/youtube/app/offline/transfer/c;

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->bb()Lcom/google/android/apps/youtube/common/fromguava/e;

    move-result-object v1

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->bl()Lcom/google/android/exoplayer/upstream/cache/a;

    move-result-object v2

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->bm()Ljava/io/File;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->b:Ljava/security/Key;

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->be()Lcom/google/android/apps/youtube/medialib/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/medialib/a;->c()Lcom/google/android/apps/youtube/common/fromguava/e;

    move-result-object v5

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->aG()Lcom/google/android/apps/youtube/common/e/n;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/offline/transfer/c;-><init>(Lcom/google/android/apps/youtube/common/fromguava/e;Lcom/google/android/exoplayer/upstream/cache/a;Ljava/io/File;Ljava/security/Key;Lcom/google/android/apps/youtube/common/fromguava/e;Lcom/google/android/apps/youtube/common/e/b;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v2, "stream_quality"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;I)I

    move-result v11

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->aL()Lorg/apache/http/client/HttpClient;

    move-result-object v1

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->aJ()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-static {v1, v2, v7}, Lcom/google/android/apps/youtube/core/async/ak;->a(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/common/e/b;)Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v12

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->bc()Ljava/io/File;

    move-result-object v13

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/p;->a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/youtube/app/offline/transfer/e;

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->aa()Lcom/google/android/apps/youtube/core/player/w;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->c:Lcom/google/android/apps/youtube/datalib/innertube/ag;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->d:Lcom/google/android/apps/youtube/common/cache/a;

    move-object v2, v8

    move-object v3, v10

    move-object v8, p1

    move-object/from16 v9, p2

    move-object v10, v0

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/youtube/app/offline/transfer/e;-><init>(Lcom/google/android/apps/youtube/core/offline/store/i;Lcom/google/android/apps/youtube/datalib/innertube/ah;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/datalib/innertube/ag;Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;Lcom/google/android/apps/youtube/core/transfer/m;Lcom/google/android/apps/youtube/app/offline/transfer/c;ILcom/google/android/apps/youtube/core/async/af;Ljava/io/File;)V

    :goto_1
    return-object v1

    :cond_0
    new-instance v1, Lcom/google/android/apps/youtube/app/offline/transfer/b;

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->aa()Lcom/google/android/apps/youtube/core/player/w;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->c:Lcom/google/android/apps/youtube/datalib/innertube/ag;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->d:Lcom/google/android/apps/youtube/common/cache/a;

    move-object v2, v8

    move-object v3, v10

    move-object v8, p1

    move-object/from16 v9, p2

    move-object v10, v0

    invoke-direct/range {v1 .. v13}, Lcom/google/android/apps/youtube/app/offline/transfer/b;-><init>(Lcom/google/android/apps/youtube/core/offline/store/i;Lcom/google/android/apps/youtube/datalib/innertube/ah;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/datalib/innertube/ag;Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;Lcom/google/android/apps/youtube/core/transfer/m;Lcom/google/android/apps/youtube/app/offline/transfer/c;ILcom/google/android/apps/youtube/core/async/af;Ljava/io/File;)V

    goto :goto_1

    :cond_1
    move-object v8, v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Ljava/lang/Runnable;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final b()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method protected final c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final d()Ljava/lang/String;
    .locals 1

    const-string v0, "bgol_tasks.db"

    return-object v0
.end method

.method protected final e()Ljava/lang/String;
    .locals 1

    const-string v0, "offline_policy_string"

    return-object v0
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final g()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final h()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final i()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    const-string v0, "Creating OfflineService..."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/BaseApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/BaseApplication;->y()Lcom/google/android/apps/youtube/core/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/a;->aP()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/utils/h;->a(Landroid/content/SharedPreferences;)Ljava/security/Key;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->b:Ljava/security/Key;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/BaseApplication;->x()Lcom/google/android/apps/youtube/datalib/innertube/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->c:Lcom/google/android/apps/youtube/datalib/innertube/ag;

    new-instance v0, Lcom/google/android/apps/youtube/common/cache/b;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/common/cache/b;-><init>(I)V

    new-instance v1, Lcom/google/android/apps/youtube/common/e/n;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/common/e/n;-><init>()V

    const-wide/32 v2, 0x36ee80

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/common/cache/n;->a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/common/e/b;J)Lcom/google/android/apps/youtube/common/cache/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->d:Lcom/google/android/apps/youtube/common/cache/a;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    const-string v0, "Destroying OfflineService..."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    invoke-super {p0}, Lcom/google/android/apps/youtube/core/transfer/TransferService;->onDestroy()V

    return-void
.end method
