.class public Lcom/google/android/apps/youtube/app/ui/FlingDynamics;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:I

.field protected b:F

.field protected c:F

.field protected d:F

.field protected e:F

.field protected f:I

.field private final g:I

.field private final h:I

.field private i:Landroid/view/VelocityTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/16 v0, 0xc8

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->f:I

    const/16 v0, 0xc8

    if-lt p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "snapVelocity cannot be less than 200"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->a:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->g:I

    iput p2, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->h:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Orientation;Z)Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;
    .locals 4

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->f:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->i:Landroid/view/VelocityTracker;

    const/16 v2, 0x3e8

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->g:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/bl;->a:[I

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Orientation;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot assess fling for ANY orientation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->c:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    sub-float v0, v1, v0

    float-to-int v1, v0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->i:Landroid/view/VelocityTracker;

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->f:I

    invoke-virtual {v0, v2}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v0

    float-to-int v0, v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->i:Landroid/view/VelocityTracker;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->i:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->i:Landroid/view/VelocityTracker;

    :cond_0
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const/16 v2, 0x14

    if-le v1, v2, :cond_1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->h:I

    if-gt v1, v2, :cond_2

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;->NONE:Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;

    :goto_1
    return-object v0

    :pswitch_1
    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->b:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    sub-float v0, v1, v0

    float-to-int v1, v0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->i:Landroid/view/VelocityTracker;

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->f:I

    invoke-virtual {v0, v2}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0

    :cond_2
    if-lez v0, :cond_3

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;->BACK:Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;->FORWARD:Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Fling;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->f:I

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->i:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->i:Landroid/view/VelocityTracker;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->i:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    return-void
.end method

.method public final a(Landroid/view/MotionEvent;Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Orientation;)Z
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Orientation;->VERTICAL:Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Orientation;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Orientation;->ANY:Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Orientation;

    if-ne p2, v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    sget-object v3, Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Orientation;->HORIZONTAL:Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Orientation;

    if-eq p2, v3, :cond_1

    sget-object v3, Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Orientation;->ANY:Lcom/google/android/apps/youtube/app/ui/FlingDynamics$Orientation;

    if-ne p2, v3, :cond_3

    :cond_1
    move v3, v2

    :goto_1
    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->f:I

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v4

    if-ltz v4, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    if-le v5, v4, :cond_6

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->e:F

    sub-float v0, v6, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v0, v0

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->a:I

    if-le v0, v4, :cond_4

    move v0, v2

    :goto_2
    or-int/lit8 v4, v0, 0x0

    :goto_3
    if-eqz v3, :cond_8

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->d:F

    sub-float v0, v5, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v0, v0

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->a:I

    if-le v0, v3, :cond_5

    move v0, v2

    :goto_4
    or-int/2addr v0, v4

    :goto_5
    if-eqz v0, :cond_7

    iput v5, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->d:F

    iput v6, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->e:F

    :goto_6
    return v2

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v3, v1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_4

    :cond_6
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->f:I

    :cond_7
    move v2, v1

    goto :goto_6

    :cond_8
    move v0, v4

    goto :goto_5

    :cond_9
    move v4, v1

    goto :goto_3
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->d:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->e:F

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->d:F

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->b:F

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->e:F

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->c:F

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->f:I

    return-void
.end method

.method public final c(Landroid/view/MotionEvent;)I
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->f:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->d:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->d:F

    goto :goto_0
.end method

.method public final d(Landroid/view/MotionEvent;)I
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->f:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->e:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->e:F

    goto :goto_0
.end method

.method public final e(Landroid/view/MotionEvent;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const v1, 0xff00

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->f:I

    if-ne v1, v2, :cond_0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->d:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->e:F

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->d:F

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->b:F

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->e:F

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->c:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->f:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->i:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/FlingDynamics;->i:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
