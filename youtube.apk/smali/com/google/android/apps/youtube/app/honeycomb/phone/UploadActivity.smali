.class public Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;
.super Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/honeycomb/ui/n;


# instance fields
.field private n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

.field private o:Lcom/google/android/apps/youtube/core/identity/o;

.field private p:Lcom/google/android/apps/youtube/core/identity/l;

.field private q:Lcom/google/android/apps/youtube/core/identity/aa;

.field private r:Lcom/google/android/apps/youtube/core/identity/as;

.field private s:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

.field private t:Lcom/google/android/apps/youtube/core/aw;

.field private u:Ljava/util/concurrent/Executor;

.field private v:Z

.field private w:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method private a(Landroid/content/DialogInterface$OnClickListener;)V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/youtube/p;->fK:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->fM:I

    invoke-virtual {v0, v1, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->fL:I

    new-instance v2, Lcom/google/android/apps/youtube/app/honeycomb/phone/aj;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/aj;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/app/honeycomb/phone/ai;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ai;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->i()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->t:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->g()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V
    .locals 0

    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->v:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->w:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->s:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->r:Lcom/google/android/apps/youtube/core/identity/as;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->p:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/core/identity/as;->a(Lcom/google/android/apps/youtube/core/identity/l;)Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->a(Landroid/content/Intent;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->w:Z

    :cond_0
    return-void
.end method

.method private i()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->o:Lcom/google/android/apps/youtube/core/identity/o;

    new-instance v1, Lcom/google/android/apps/youtube/app/honeycomb/phone/ak;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ak;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;B)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/youtube/core/identity/o;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/v;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/app/compat/j;)Z
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/apps/youtube/app/compat/j;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->s:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->a(Lcom/google/android/apps/youtube/app/compat/j;)Z

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method protected final a_(I)Landroid/app/Dialog;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->s:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->a(I)Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->a_(I)Landroid/app/Dialog;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_upload"

    return-object v0
.end method

.method public final e()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->finish()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/am;->d()V

    return-void
.end method

.method protected final e_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final f()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->finish()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/am;->d()V

    return-void
.end method

.method public final g()Z
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ag;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ag;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->a(Landroid/content/DialogInterface$OnClickListener;)V

    const/4 v0, 0x1

    return v0
.end method

.method public handleSignOutEvent(Lcom/google/android/apps/youtube/core/identity/aj;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->finish()V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ah;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ah;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->a(Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->bt:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->setContentView(Landroid/view/View;)V

    sget v0, Lcom/google/android/youtube/p;->gq:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->setTitle(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->k()Lcom/google/android/apps/youtube/core/identity/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->o:Lcom/google/android/apps/youtube/core/identity/o;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->p:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->q:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->aQ()Lcom/google/android/apps/youtube/core/identity/as;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->r:Lcom/google/android/apps/youtube/core/identity/as;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->t:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->aI()Ljava/util/concurrent/Executor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->u:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->t:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->M()Lcom/google/android/apps/youtube/app/compat/o;

    move-result-object v6

    move-object v1, p0

    move-object v4, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/app/honeycomb/ui/n;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/compat/o;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->s:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->E()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->E()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onStart()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->v:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->p:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->q:Lcom/google/android/apps/youtube/core/identity/aa;

    new-instance v1, Lcom/google/android/apps/youtube/app/honeycomb/phone/af;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/af;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ah;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->i()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->v:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->s:Lcom/google/android/apps/youtube/app/honeycomb/ui/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/j;->a()V

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/UploadActivity;->w:Z

    :cond_0
    return-void
.end method
