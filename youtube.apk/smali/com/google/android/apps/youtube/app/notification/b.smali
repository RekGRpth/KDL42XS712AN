.class public final Lcom/google/android/apps/youtube/app/notification/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/a/a;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/content/SharedPreferences;

.field private final d:Lcom/google/android/apps/youtube/datalib/innertube/ab;

.field private final e:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/a/a;Ljava/lang/String;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/datalib/innertube/ab;Ljava/util/concurrent/Executor;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/a/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/notification/b;->a:Lcom/google/android/gms/a/a;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/notification/b;->b:Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/notification/b;->c:Landroid/content/SharedPreferences;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/ab;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/notification/b;->d:Lcom/google/android/apps/youtube/datalib/innertube/ab;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/notification/b;->e:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/notification/b;)V
    .locals 5

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/notification/b;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/google/android/apps/youtube/common/e/d;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/common/e/d;-><init>()V

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/notification/b;->a:Lcom/google/android/gms/a/a;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/notification/b;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Lcom/google/android/gms/a/a;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/notification/b;->c:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "gcm_registration_id"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/notification/b;->b()V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v2, "Could not register with GCM: "

    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/common/e/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/notification/b;->b()V

    goto :goto_1
.end method

.method private b()V
    .locals 4

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/notification/b;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "Could not register with InnerTube because no GCM registration ID was found."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/google/android/apps/youtube/common/e/d;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/common/e/d;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/notification/b;->d:Lcom/google/android/apps/youtube/datalib/innertube/ab;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/ab;->a()Lcom/google/android/apps/youtube/datalib/innertube/ac;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ac;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ac;

    move-result-object v2

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/notification/b;->d:Lcom/google/android/apps/youtube/datalib/innertube/ab;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/ab;->a(Lcom/google/android/apps/youtube/datalib/innertube/ac;)Lcom/google/a/a/a/a/ra;
    :try_end_0
    .catch Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "Could not register for notifications with InnerTube: "

    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/common/e/d;->a()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v3, "Could not register for notifications with InnerTube: "

    invoke-static {v3, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/common/e/d;->a()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0
.end method

.method private c()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/notification/b;->c:Landroid/content/SharedPreferences;

    const-string v1, "gcm_registration_id"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/notification/b;->e:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/app/notification/c;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/notification/c;-><init>(Lcom/google/android/apps/youtube/app/notification/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final onSignIn(Lcom/google/android/apps/youtube/core/identity/ai;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/notification/b;->a()V

    return-void
.end method

.method public final onSignOut(Lcom/google/android/apps/youtube/core/identity/aj;)V
    .locals 0
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/notification/b;->a()V

    return-void
.end method
