.class final Lcom/google/android/apps/youtube/app/ui/x;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/w;

.field private final b:Ljava/lang/Integer;

.field private final c:Lcom/google/android/apps/youtube/app/ui/ab;

.field private final d:Lcom/google/android/apps/youtube/app/ui/aa;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/w;Ljava/lang/Integer;Lcom/google/android/apps/youtube/app/ui/aa;Lcom/google/android/apps/youtube/app/ui/ab;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/x;->a:Lcom/google/android/apps/youtube/app/ui/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/x;->b:Ljava/lang/Integer;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/ui/x;->c:Lcom/google/android/apps/youtube/app/ui/ab;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/x;->d:Lcom/google/android/apps/youtube/app/ui/aa;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/w;Ljava/lang/Integer;Ljava/lang/String;ILcom/google/android/apps/youtube/app/ui/ab;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/x;->a:Lcom/google/android/apps/youtube/app/ui/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/x;->b:Ljava/lang/Integer;

    iput-object p5, p0, Lcom/google/android/apps/youtube/app/ui/x;->c:Lcom/google/android/apps/youtube/app/ui/ab;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/x;->e:Z

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/y;

    invoke-direct {v0, p0, p1, p3, p4}, Lcom/google/android/apps/youtube/app/ui/y;-><init>(Lcom/google/android/apps/youtube/app/ui/x;Lcom/google/android/apps/youtube/app/ui/w;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/x;->d:Lcom/google/android/apps/youtube/app/ui/aa;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/x;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/x;->e:Z

    return v0
.end method


# virtual methods
.method public final a()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/x;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/x;->e:Z

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/x;->d:Lcom/google/android/apps/youtube/app/ui/aa;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/aa;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/x;->d:Lcom/google/android/apps/youtube/app/ui/aa;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/aa;->c()I

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/x;->d:Lcom/google/android/apps/youtube/app/ui/aa;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/aa;->a()Z

    move-result v0

    return v0
.end method

.method public final e()Lcom/google/android/apps/youtube/app/ui/ab;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/x;->c:Lcom/google/android/apps/youtube/app/ui/ab;

    return-object v0
.end method
