.class final Lcom/google/android/apps/youtube/app/ap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/an;

.field private final b:Lcom/google/android/apps/youtube/common/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/an;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ap;->a:Lcom/google/android/apps/youtube/app/an;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/a/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ap;->b:Lcom/google/android/apps/youtube/common/a/b;

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ap;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ap;->a:Lcom/google/android/apps/youtube/app/an;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/an;->a(Lcom/google/android/apps/youtube/app/an;Lcom/google/android/apps/youtube/core/async/GDataRequest;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ap;->a:Lcom/google/android/apps/youtube/app/an;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ap;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/youtube/app/an;->a(Lcom/google/android/apps/youtube/app/an;Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ap;->a:Lcom/google/android/apps/youtube/app/an;

    invoke-static {v3}, Lcom/google/android/apps/youtube/app/an;->a(Lcom/google/android/apps/youtube/app/an;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/apps/youtube/app/an;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/be;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->nextUri:Landroid/net/Uri;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ap;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "/myfeed/users/"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    iget v1, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->totalResults:I

    iget v2, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->elementsPerPage:I

    iget v3, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->startIndex:I

    iget-object v4, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->previousUri:Landroid/net/Uri;

    iget-object v6, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;->entries:Ljava/util/List;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;-><init>(IIILandroid/net/Uri;Landroid/net/Uri;Ljava/util/List;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ap;->b:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v1, p1, v0}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method
