.class public Lcom/google/android/apps/youtube/app/WatchWhileActivity;
.super Lcom/google/android/apps/youtube/app/GuideActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/compat/e;
.implements Lcom/google/android/apps/youtube/app/ui/eq;
.implements Lcom/google/android/apps/youtube/app/ui/iw;
.implements Lcom/google/android/apps/youtube/core/ui/h;
.implements Lcom/google/android/apps/youtube/core/utils/j;
.implements Lcom/google/android/apps/youtube/core/utils/n;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Lcom/google/android/apps/youtube/core/utils/DeviceOrientationHelper;

.field private D:Lcom/google/android/apps/youtube/core/utils/HdmiReceiver;

.field private E:Lcom/google/android/apps/youtube/app/ar;

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:I

.field private L:I

.field private M:Landroid/graphics/drawable/Drawable;

.field private N:F

.field private O:Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

.field private P:Z

.field private Q:Z

.field private R:Landroid/content/SharedPreferences;

.field private S:I

.field private T:Lcom/google/android/apps/youtube/app/ui/hh;

.field private x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

.field private y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

.field private z:Lcom/google/android/apps/youtube/core/ui/g;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Q:Z

    return-void
.end method

.method private T()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O()Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3fa00000    # 1.25f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a()V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->B:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->U()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e(Z)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private U()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->o:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private V()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->B:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->U()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private W()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c()Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MAXIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private X()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c()Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Y()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->c()Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Z()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->v:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->G:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->F:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->w:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->I()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->z:Lcom/google/android/apps/youtube/core/ui/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/g;->c(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->a()V

    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->R:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private a(F)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/ah;->a(F)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L:I

    if-eq v1, v0, :cond_0

    iput v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P:Z

    return p1
.end method

.method private aa()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->setRequestedOrientation(I)V

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    return-object v0
.end method

.method private b(F)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->q()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    const/high16 v0, 0x40a00000    # 5.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    iget v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S:I

    if-eq v0, v1, :cond_0

    iput v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S:I

    iget v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S:I

    rsub-int/lit8 v0, v0, 0x5

    add-int/lit8 v0, v0, 0x1

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x4018000000000000L    # 6.0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v0, v1, v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(F)V

    :cond_0
    return-void
.end method

.method private handleVideoControlsVisibilityEvent(Lcom/google/android/apps/youtube/core/player/event/aa;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-boolean v0, p1, Lcom/google/android/apps/youtube/core/player/event/aa;->a:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->G:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->G:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->v:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->z:Lcom/google/android/apps/youtube/core/ui/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/g;->c(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Z()V

    goto :goto_0
.end method


# virtual methods
.method public final A()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->A()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Z()V

    return-void
.end method

.method public final C()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "PlayerMinimizeManual"

    const-string v2, "PlayerCarat"

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->v:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g()V

    goto :goto_0
.end method

.method public final D()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->I:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->finish()V

    :cond_0
    return-void
.end method

.method public final E()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->b(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->a(Z)V

    return-void
.end method

.method public final F()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->u:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r:Lcom/google/android/apps/youtube/app/ui/SliderLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/SliderLayout;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->v:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->G()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "PlayerMaximizeManual"

    const-string v2, "PlayerClick"

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->V()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->f()V

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e(Z)V

    goto :goto_0
.end method

.method public final G()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->u:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h()V

    :cond_0
    return-void
.end method

.method public final H()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->D:Lcom/google/android/apps/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/HdmiReceiver;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->C:Lcom/google/android/apps/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/DeviceOrientationHelper;->disable()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aa()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->setRequestedOrientation(I)V

    goto :goto_0
.end method

.method public final I()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->p()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/an;->h()V

    return-void
.end method

.method public final J()Lcom/google/android/apps/youtube/app/ui/hh;
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->T:Lcom/google/android/apps/youtube/app/ui/hh;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/hh;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q()Lcom/google/android/apps/youtube/datalib/d/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->as()Lcom/google/android/apps/youtube/datalib/innertube/t;

    move-result-object v3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/ui/hh;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/datalib/d/a;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/datalib/innertube/t;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->T:Lcom/google/android/apps/youtube/app/ui/hh;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->T:Lcom/google/android/apps/youtube/app/ui/hh;

    return-object v0
.end method

.method public final a(IF)V
    .locals 7

    const/4 v6, 0x1

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v4, 0x3e800000    # 0.25f

    const/high16 v3, 0x3f400000    # 0.75f

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    iget v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K:I

    if-ge p1, v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->b(Z)V

    cmpg-float v0, p2, v3

    if-gez v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(F)V

    cmpl-float v0, p2, v5

    if-lez v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N:F

    const/high16 v1, 0x40400000    # 3.0f

    sub-float/2addr v1, p2

    mul-float/2addr v1, v0

    :cond_0
    :goto_2
    iput v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N:F

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->b(F)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->a(Z)V

    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b()V

    goto :goto_0

    :cond_2
    cmpg-float v0, p2, v1

    if-gez v0, :cond_4

    sub-float v0, p2, v3

    div-float/2addr v0, v4

    goto :goto_1

    :cond_3
    cmpl-float v0, p2, v1

    if-lez v0, :cond_0

    sub-float v0, v5, p2

    mul-float/2addr v0, v3

    add-float v1, v4, v0

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final a(II)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(II)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->u:Z

    if-nez v0, :cond_1

    sub-int v0, p2, p1

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->setHorizontalDisplacement(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;Z)V
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->WATCH:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Z

    :cond_0
    return-void
.end method

.method protected final a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;I)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W()Z

    move-result v0

    if-eqz v0, :cond_0

    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "PlayerMinimizeManual"

    const-string v2, "Browse"

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g()V

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;I)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;Z)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;Z)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;)V
    .locals 7

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->b()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W()Z

    move-result v2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X()Z

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v4

    iget-boolean v5, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->v:Z

    if-nez v5, :cond_7

    if-eqz v2, :cond_6

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->V()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e(Z)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-direct {p0, v6}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->b(F)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Z)V

    :cond_1
    :goto_1
    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->v:Z

    if-nez v2, :cond_2

    if-eqz v3, :cond_2

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->I:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aa()V

    :cond_2
    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Q:Z

    if-nez v2, :cond_3

    if-eqz v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->O()Lcom/google/android/apps/youtube/core/player/PlayerView;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->setTargetView(Landroid/view/View;Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->b(Z)V

    :cond_3
    sget-object v2, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->MINIMIZED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-eq p1, v2, :cond_4

    sget-object v2, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;->DISMISSED:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout$WatchState;

    if-ne p1, v2, :cond_5

    :cond_4
    move v0, v1

    :cond_5
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Q:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->S()Lcom/google/android/apps/youtube/app/honeycomb/ui/g;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/g;->c(Z)V

    return-void

    :cond_6
    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b()V

    invoke-direct {p0, v6}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(F)V

    goto :goto_0

    :cond_7
    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a()V

    if-nez v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e(Z)V

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->F()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->B()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/an;->e()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->p()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-interface {v2}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->g()V

    goto :goto_1
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/j;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/GuideActivity;->a(Lcom/google/android/apps/youtube/app/compat/j;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final b(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->mustAuthenticate()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->m()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/youtube/app/b/v;

    invoke-direct {v3}, Lcom/google/android/apps/youtube/app/b/v;-><init>()V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/k;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->p()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/an;->e()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->r()Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->getPlaybackStartDescriptor()Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    move-result-object v4

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->shouldSkipRemoteDialog()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v3, v4}, Lcom/google/android/apps/youtube/core/player/ae;->b(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->isFromRemoteQueue()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getVideoId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->getPlaylistIndex()I

    move-result v4

    invoke-interface {v0, v3, v5, v4}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i()V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->shouldFinishOnEnded()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->I:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Z

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->I:Z

    or-int/2addr v0, v3

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->I:Z

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->shouldForceFullscreen()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_2
    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J:Z

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->v:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J:Z

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->V()Z

    move-result v0

    if-nez v0, :cond_7

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a()V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e(Z)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->setRequestedOrientation(I)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J:Z

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;Z)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->D()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "MdxModalShow"

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/app/fragments/RemoteWatchDialogFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "watch"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/DialogFragment;->g(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c()Landroid/support/v4/app/l;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(Landroid/support/v4/app/l;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->w:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->a()V

    :cond_8
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->g(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    goto/16 :goto_0

    :cond_9
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b()V

    :cond_a
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->X()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->noAnimation()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->b(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->E:Lcom/google/android/apps/youtube/app/ar;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ar;->sendEmptyMessage(I)Z

    goto :goto_2
.end method

.method public final b(Z)V
    .locals 0

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/app/compat/j;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O()Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    const-string v1, "yt_watch"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->t:Lcom/google/android/apps/youtube/app/compat/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->t:Lcom/google/android/apps/youtube/app/compat/q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/compat/q;->b(Z)Lcom/google/android/apps/youtube/app/compat/q;

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/GuideActivity;->b(Lcom/google/android/apps/youtube/app/compat/j;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->C:Lcom/google/android/apps/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/DeviceOrientationHelper;->disable()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "accelerometer_rotation"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->aa()V

    :cond_0
    return-void
.end method

.method protected final e()I
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->A:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->B:Z

    sget v0, Lcom/google/android/youtube/l;->bo:I

    :goto_0
    return v0

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->A:Z

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->B:Z

    sget v0, Lcom/google/android/youtube/l;->aB:I

    goto :goto_0
.end method

.method protected final f()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k()Z

    move-result v0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->f()V

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->p:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->d()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e(Z)V

    :cond_0
    return-void
.end method

.method public final f_()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->v:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->G()V

    :cond_0
    return-void
.end method

.method public finish()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->F()V

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->finish()V

    return-void
.end method

.method public final g(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->F:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Z()V

    return-void
.end method

.method public final h(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->z:Lcom/google/android/apps/youtube/core/ui/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/g;->c(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Z()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->v:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->E()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->finish()V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->v:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e(Z)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "PlayerMinimizeManual"

    const-string v2, "Back"

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->g()V

    goto :goto_0

    :cond_4
    invoke-super {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/GuideActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->T()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/GuideActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    sget v0, Lcom/google/android/youtube/j;->gr:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->setListener(Lcom/google/android/apps/youtube/app/ui/iw;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->q:Landroid/support/v4/app/l;

    sget v1, Lcom/google/android/youtube/j;->dx:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/l;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    new-instance v0, Lcom/google/android/apps/youtube/core/ui/g;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->O()Lcom/google/android/apps/youtube/core/player/PlayerView;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/android/apps/youtube/core/ui/g;-><init>(Landroid/view/Window;Landroid/app/ActionBar;Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;Lcom/google/android/apps/youtube/core/ui/h;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->z:Lcom/google/android/apps/youtube/core/ui/g;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->z:Lcom/google/android/apps/youtube/core/ui/g;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/youtube/core/ui/g;->a(Z)V

    new-instance v0, Lcom/google/android/apps/youtube/core/utils/DeviceOrientationHelper;

    invoke-direct {v0, p0, p0}, Lcom/google/android/apps/youtube/core/utils/DeviceOrientationHelper;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/utils/j;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->C:Lcom/google/android/apps/youtube/core/utils/DeviceOrientationHelper;

    new-instance v0, Lcom/google/android/apps/youtube/app/ar;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ar;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->E:Lcom/google/android/apps/youtube/app/ar;

    new-instance v0, Lcom/google/android/apps/youtube/core/utils/HdmiReceiver;

    invoke-direct {v0, p0, p0}, Lcom/google/android/apps/youtube/core/utils/HdmiReceiver;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/utils/n;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->D:Lcom/google/android/apps/youtube/core/utils/HdmiReceiver;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/b;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/honeycomb/d;)V

    if-eqz p1, :cond_0

    const-string v0, "finish_on_back"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Z

    const-string v0, "finish_on_watch_ended"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->I:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->B()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->R:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->R:Landroid/content/SharedPreferences;

    const-string v1, "watch_while_tutorial_views_remaining"

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_1

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/youtube/l;->bM:I

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/j;->gp:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O:Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

    new-instance v1, Lcom/google/android/apps/youtube/app/aq;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/aq;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->setDismissListener(Lcom/google/android/apps/youtube/app/ui/iu;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P:Z

    :cond_1
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->z:Lcom/google/android/apps/youtube/core/ui/g;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/g;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->C:Lcom/google/android/apps/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/DeviceOrientationHelper;->disable()V

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :goto_0
    if-nez v1, :cond_0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/GuideActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    :goto_0
    if-nez v1, :cond_0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/app/GuideActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->E()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->b(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b(Lcom/google/android/apps/youtube/app/compat/e;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->C:Lcom/google/android/apps/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/DeviceOrientationHelper;->disable()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->E:Lcom/google/android/apps/youtube/app/ar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ar;->removeMessages(I)V

    return-void
.end method

.method protected onResume()V
    .locals 7

    const/4 v6, 0x1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->n:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->T()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Lcom/google/android/apps/youtube/app/compat/e;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->C:Lcom/google/android/apps/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/DeviceOrientationHelper;->enable()V

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(F)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->g(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->B()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/an;->e()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v3}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_1

    invoke-interface {v3}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Y()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->p()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->r()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->q()I

    move-result v3

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->REMOTE_QUEUE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Ljava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;-><init>(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    invoke-virtual {v1, v6}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->setSkipRemoteDialog(Z)V

    invoke-virtual {v1, v6}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->setNoAnimation(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->b(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/GuideActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "finish_on_back"

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->H:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "finish_on_watch_ended"

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->I:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->D:Lcom/google/android/apps/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/HdmiReceiver;->a()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->z:Lcom/google/android/apps/youtube/core/ui/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/g;->c(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->D:Lcom/google/android/apps/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/HdmiReceiver;->b()V

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->onStop()V

    return-void
.end method

.method public final p()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final t()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->t()V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->u:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method protected final u()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->u()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public final v()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->v()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->setHorizontalDisplacement(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method protected final w()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->O()Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a()I

    move-result v0

    sget v1, Lcom/google/android/youtube/j;->di:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2, v0, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    sget v1, Lcom/google/android/youtube/j;->bv:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2, v0, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method

.method protected final x()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->z:Lcom/google/android/apps/youtube/core/ui/g;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->v:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/g;->b(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->v:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->setFullscreen(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->U()Z

    move-result v1

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->v:Z

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->Z()V

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->A:Z

    if-nez v1, :cond_0

    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->setRequestedOrientation(I)V

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a()V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->C:Lcom/google/android/apps/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/utils/DeviceOrientationHelper;->enable()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->y:Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->v:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/fragments/PlayerFragment;->c(Z)V

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->x()V

    return-void

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->z:Lcom/google/android/apps/youtube/core/ui/g;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/core/ui/g;->c(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->W()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b()V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->V()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->P()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "PlayerMinimizeManual"

    const-string v2, "ExitFullscreen"

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->x:Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/WatchWhileLayout;->b()V

    goto :goto_0
.end method

.method protected final y()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->M:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/h;->a:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->M:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->M:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->M:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->L:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    return-void
.end method

.method public final z()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/GuideActivity;->z()V

    return-void
.end method
