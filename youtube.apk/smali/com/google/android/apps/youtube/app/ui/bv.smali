.class public final Lcom/google/android/apps/youtube/app/ui/bv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/app/offline/p;

.field private c:Landroid/app/AlertDialog;

.field private d:Ljava/util/LinkedList;

.field private e:Landroid/widget/CheckBox;

.field private f:Landroid/view/View$OnClickListener;

.field private g:Lcom/google/android/apps/youtube/app/ui/bx;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/offline/p;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/p;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->b:Lcom/google/android/apps/youtube/app/offline/p;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/bv;)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/by;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/by;->e(Lcom/google/android/apps/youtube/app/ui/by;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/by;->a(Lcom/google/android/apps/youtube/app/ui/by;)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->c:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->a:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->aw:I

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->d:Ljava/util/LinkedList;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/by;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;->SD:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    sget v3, Lcom/google/android/youtube/j;->er:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/by;-><init>(Lcom/google/android/apps/youtube/app/ui/bv;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;Landroid/view/View;B)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/bv;->a(Lcom/google/android/apps/youtube/app/ui/by;)V

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/by;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;->HD:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    sget v3, Lcom/google/android/youtube/j;->bB:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/by;-><init>(Lcom/google/android/apps/youtube/app/ui/bv;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;Landroid/view/View;B)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/bv;->a(Lcom/google/android/apps/youtube/app/ui/by;)V

    sget v0, Lcom/google/android/youtube/j;->ec:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->e:Landroid/widget/CheckBox;

    new-instance v0, Lcom/google/android/apps/youtube/core/ui/aa;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/bv;->a:Landroid/app/Activity;

    invoke-direct {v0, v2}, Lcom/google/android/apps/youtube/core/ui/aa;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/youtube/p;->cz:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/core/ui/aa;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/google/android/youtube/p;->dA:I

    invoke-virtual {v0, v2, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/google/android/youtube/p;->K:I

    invoke-virtual {v0, v2, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->c:Landroid/app/AlertDialog;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->b:Lcom/google/android/apps/youtube/app/offline/p;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/offline/p;->c()Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/bv;->a(Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/by;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/by;->a(Lcom/google/android/apps/youtube/app/ui/by;)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    move-result-object v1

    if-ne p1, v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/by;->a(Lcom/google/android/apps/youtube/app/ui/by;Z)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/bv;Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/bv;->a(Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/app/ui/by;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/by;->a(Lcom/google/android/apps/youtube/app/ui/by;)Lcom/google/android/apps/youtube/app/offline/OfflineStreamQuality;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/ui/by;->b(Lcom/google/android/apps/youtube/app/ui/by;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/bv;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->a:Landroid/app/Activity;

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->c:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->f:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/bw;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/bw;-><init>(Lcom/google/android/apps/youtube/app/ui/bv;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->f:Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->c:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/bv;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/bv;)Landroid/widget/CheckBox;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->e:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/bv;)Lcom/google/android/apps/youtube/app/offline/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->b:Lcom/google/android/apps/youtube/app/offline/p;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/ui/bv;)Lcom/google/android/apps/youtube/app/ui/bx;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->g:Lcom/google/android/apps/youtube/app/ui/bx;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/ui/bv;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->c:Landroid/app/AlertDialog;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;Lcom/google/android/apps/youtube/app/ui/bx;)Z
    .locals 7

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;->b()Ljava/util/Map;

    move-result-object v5

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/bx;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->g:Lcom/google/android/apps/youtube/app/ui/bx;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/bv;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/by;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/by;->c(Lcom/google/android/apps/youtube/app/ui/by;)Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;

    if-eqz v1, :cond_0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/ui/by;->a(Lcom/google/android/apps/youtube/app/ui/by;Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/by;->b(Lcom/google/android/apps/youtube/app/ui/by;)V

    goto :goto_0

    :cond_1
    if-lez v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/bv;->b()V

    move v3, v4

    :cond_2
    :goto_1
    return v3

    :cond_3
    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/bx;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->g:Lcom/google/android/apps/youtube/app/ui/bx;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/bv;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bv;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/by;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/by;->d(Lcom/google/android/apps/youtube/app/ui/by;)V

    goto :goto_2

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/bv;->b()V

    move v3, v4

    goto :goto_1
.end method
