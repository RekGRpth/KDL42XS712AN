.class final Lcom/google/android/apps/youtube/app/bz;
.super Lcom/google/android/apps/youtube/common/e/f;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ax;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/ax;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/bz;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/common/e/f;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 4

    new-instance v1, Lcom/google/android/apps/youtube/core/client/i;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/bz;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aG()Lcom/google/android/apps/youtube/common/e/n;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/bz;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/ax;->aP()Landroid/content/SharedPreferences;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/bz;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/youtube/core/client/i;-><init>(Lcom/google/android/apps/youtube/common/e/b;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/common/network/h;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/bz;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ax;->v(Lcom/google/android/apps/youtube/app/ax;)Lcom/google/android/apps/youtube/common/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/utils/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/client/i;->a(Lcom/google/android/apps/youtube/core/utils/a;)Lcom/google/android/apps/youtube/core/client/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/bz;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/ax;->u(Lcom/google/android/apps/youtube/app/ax;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/client/i;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/i;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/bz;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ax;->t(Lcom/google/android/apps/youtube/app/ax;)Lcom/google/android/apps/youtube/common/e/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/f;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/ax;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/client/i;->a(Lcom/google/android/apps/youtube/core/client/ax;)Lcom/google/android/apps/youtube/core/client/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/bz;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->m()Lcom/google/android/apps/youtube/datalib/offline/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/client/i;->a(Lcom/google/android/apps/youtube/datalib/offline/n;)Lcom/google/android/apps/youtube/core/client/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/bz;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->aa()Lcom/google/android/apps/youtube/core/player/w;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/client/i;->a(Lcom/google/android/apps/youtube/core/player/w;)Lcom/google/android/apps/youtube/core/client/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/bz;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->ah()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/client/i;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/bz;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->L()Lcom/google/android/apps/youtube/core/client/cf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/client/i;->a(Lcom/google/android/apps/youtube/core/client/cf;)Lcom/google/android/apps/youtube/core/client/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/bz;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->b()Lcom/google/android/apps/youtube/app/aw;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/aw;->p()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/client/i;->a(Z)Lcom/google/android/apps/youtube/core/client/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/bz;->a:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->ad()Lcom/google/android/apps/youtube/core/player/ad;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/client/i;->a(Lcom/google/android/apps/youtube/core/player/ad;)Lcom/google/android/apps/youtube/core/client/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/i;->a()Lcom/google/android/apps/youtube/core/client/h;

    move-result-object v0

    return-object v0
.end method
