.class public final Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/remote/ap;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/remote/an;

.field private b:Lcom/google/android/apps/youtube/app/ui/v;

.field private c:Landroid/widget/BaseAdapter;

.field private final d:Lcom/google/android/apps/youtube/app/ui/eh;

.field private final e:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;

.field private f:I

.field private g:Z


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;Lcom/google/android/apps/youtube/app/ui/eh;Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/an;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a:Lcom/google/android/apps/youtube/app/remote/an;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b:Lcom/google/android/apps/youtube/app/ui/v;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->c:Landroid/widget/BaseAdapter;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->d:Lcom/google/android/apps/youtube/app/ui/eh;

    iput-object p5, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->e:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;

    invoke-virtual {p1, p0}, Lcom/google/android/apps/youtube/app/remote/an;->a(Lcom/google/android/apps/youtube/app/remote/ap;)V

    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/client/bc;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;
    .locals 9

    const/4 v8, 0x0

    new-instance v7, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/ea;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p3

    move-object v4, p2

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/ea;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/bc;)V

    sget-object v6, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;->ADD:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;

    move-object v1, v7

    move-object v2, p1

    move-object v3, v8

    move-object v4, v8

    move-object v5, v0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;-><init>(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;Lcom/google/android/apps/youtube/app/ui/eh;Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;)V

    return-object v7
.end method

.method public static a(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/Analytics;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    new-instance v4, Lcom/google/android/apps/youtube/app/ui/ee;

    invoke-direct {v4, p3, p4}, Lcom/google/android/apps/youtube/app/ui/ee;-><init>(Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/Analytics;)V

    sget-object v5, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;->ADD:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;-><init>(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;Lcom/google/android/apps/youtube/app/ui/eh;Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    new-instance v4, Lcom/google/android/apps/youtube/app/ui/dy;

    invoke-direct {v4, p4, p3, p5}, Lcom/google/android/apps/youtube/app/ui/dy;-><init>(Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/Analytics;)V

    sget-object v5, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;->ADD:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;-><init>(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;Lcom/google/android/apps/youtube/app/ui/eh;Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/client/bc;)Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;
    .locals 6

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;

    new-instance v4, Lcom/google/android/apps/youtube/app/ui/ec;

    invoke-direct {v4, p3}, Lcom/google/android/apps/youtube/app/ui/ec;-><init>(Lcom/google/android/apps/youtube/core/Analytics;)V

    sget-object v5, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;->REMOVE:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;

    move-object v1, p0

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;-><init>(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;Lcom/google/android/apps/youtube/app/ui/eh;Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;)V

    return-object v0
.end method

.method private a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b:Lcom/google/android/apps/youtube/app/ui/v;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_3

    move v0, v1

    :goto_1
    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->g:Z

    if-nez v3, :cond_5

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->e:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;

    sget-object v3, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;->ADD:Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController$Action;

    if-ne v0, v3, :cond_4

    sget v0, Lcom/google/android/youtube/p;->cB:I

    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->d:Lcom/google/android/apps/youtube/app/ui/eh;

    invoke-interface {v4, p1}, Lcom/google/android/apps/youtube/app/ui/eh;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;)Lcom/google/android/apps/youtube/app/ui/ab;

    move-result-object v4

    invoke-virtual {v3, v2, v0, v4}, Lcom/google/android/apps/youtube/app/ui/v;->a(IILcom/google/android/apps/youtube/app/ui/ab;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->f:I

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->g:Z

    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b:Lcom/google/android/apps/youtube/app/ui/v;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ui/v;->a()I

    move-result v3

    if-lez v3, :cond_6

    :goto_4
    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/v;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->c:Landroid/widget/BaseAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->c:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    sget v0, Lcom/google/android/youtube/p;->eZ:I

    goto :goto_2

    :cond_5
    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->g:Z

    if-eqz v3, :cond_2

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b:Lcom/google/android/apps/youtube/app/ui/v;

    iget v3, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->f:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/ui/v;->c(I)V

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->g:Z

    goto :goto_3

    :cond_6
    move v1, v2

    goto :goto_4
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Lcom/google/android/apps/youtube/core/client/WatchFeature;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/Analytics;)V
    .locals 1

    invoke-interface {p0, p1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AddToTvQueueFromOverflowMenu"

    invoke-interface {p4, v0}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-interface {p0, v0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget v0, Lcom/google/android/youtube/p;->bn:I

    invoke-virtual {p3, v0}, Lcom/google/android/apps/youtube/core/aw;->a(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;Ljava/lang/String;Lcom/google/android/apps/youtube/core/Analytics;)V
    .locals 1

    invoke-interface {p0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->p()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->g()V

    :cond_0
    invoke-interface {p0, p1}, Lcom/google/android/apps/youtube/app/remote/RemoteControl;->d(Ljava/lang/String;)V

    const-string v0, "RemoteQueueDelete"

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/core/Analytics;->b(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/remote/an;->b(Lcom/google/android/apps/youtube/app/remote/ap;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/ui/v;Landroid/widget/BaseAdapter;)V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b:Lcom/google/android/apps/youtube/app/ui/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b:Lcom/google/android/apps/youtube/app/ui/v;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->f:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/v;->c(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->g:Z

    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/v;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->b:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->c:Landroid/widget/BaseAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->c()V

    return-void
.end method

.method public final b()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->c()V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/an;->e()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/RemoteControlContextualMenuController;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl;)V

    return-void
.end method
