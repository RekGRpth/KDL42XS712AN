.class final Lcom/google/android/apps/youtube/app/remote/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/common/a/b;

.field final synthetic b:Lcom/google/android/apps/ytremote/model/ScreenId;

.field final synthetic c:Lcom/google/android/apps/youtube/app/remote/t;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/remote/t;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/ytremote/model/ScreenId;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/y;->c:Lcom/google/android/apps/youtube/app/remote/t;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/remote/y;->a:Lcom/google/android/apps/youtube/common/a/b;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/remote/y;->b:Lcom/google/android/apps/ytremote/model/ScreenId;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/y;->c:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/t;->b(Lcom/google/android/apps/youtube/app/remote/t;)Landroid/util/Pair;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/youtube/app/remote/t;->a()Landroid/util/Pair;

    move-result-object v0

    if-ne v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/y;->a:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/y;->b:Lcom/google/android/apps/ytremote/model/ScreenId;

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Authentication failed."

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/y;->c:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/t;->c(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/ytremote/backend/logic/a;

    move-result-object v2

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/y;->b:Lcom/google/android/apps/ytremote/model/ScreenId;

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/y;->a:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/y;->b:Lcom/google/android/apps/ytremote/model/ScreenId;

    new-instance v3, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-direct {v3, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;-><init>(Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    const-string v0, "A screen which did not exist in the combined screen storage was removed!"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/y;->a:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/y;->b:Lcom/google/android/apps/ytremote/model/ScreenId;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method
