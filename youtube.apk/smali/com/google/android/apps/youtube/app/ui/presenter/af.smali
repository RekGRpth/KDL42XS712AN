.class public final Lcom/google/android/apps/youtube/app/ui/presenter/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/j;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/youtube/core/client/bj;

.field private final c:Lcom/google/android/apps/youtube/datalib/d/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/datalib/d/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/af;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/af;->b:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/d/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/af;->c:Lcom/google/android/apps/youtube/datalib/d/a;

    return-void
.end method


# virtual methods
.method public final synthetic a()Lcom/google/android/apps/youtube/uilib/a/g;
    .locals 5

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/ae;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/af;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/af;->b:Lcom/google/android/apps/youtube/core/client/bj;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/presenter/p;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/af;->a:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/p;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/presenter/af;->c:Lcom/google/android/apps/youtube/datalib/d/a;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/app/ui/presenter/ae;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/uilib/a/i;Lcom/google/android/apps/youtube/datalib/d/a;)V

    return-object v0
.end method
