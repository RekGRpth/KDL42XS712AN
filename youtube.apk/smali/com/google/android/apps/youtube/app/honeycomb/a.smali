.class public final Lcom/google/android/apps/youtube/app/honeycomb/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/am;


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->i()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->b:Ljava/lang/Class;

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->b:Ljava/lang/Class;

    if-eqz v0, :cond_0

    const-string v0, "ancestor_classname"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->b:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    const-class v2, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "pane"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    const-class v2, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "watch"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private i()Ljava/lang/Class;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "ancestor_classname"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :try_start_0
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->b:Ljava/lang/Class;

    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    return-void
.end method

.method public final a(Landroid/net/Uri;IZLcom/google/android/apps/youtube/core/client/WatchFeature;)V
    .locals 6

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/q;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/utils/q;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    const-string v1, ""

    iget-object v2, v2, Lcom/google/android/apps/youtube/core/utils/q;->a:Ljava/lang/String;

    move v4, v3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Ljava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;-><init>(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    invoke-virtual {v1, p3}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->setMustAuthenticate(Z)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid playlist uri: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/net/Uri;Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    iget v1, p2, Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;->stringId:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    return-void
.end method

.method public final a(Lcom/google/a/a/a/a/qu;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(Lcom/google/a/a/a/a/qu;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    const-string v1, ""

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->OFFLINE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-object v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Ljava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->setOffline(Z)V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;-><init>(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-static {p1, p2}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;IZLcom/google/android/apps/youtube/core/client/WatchFeature;Z)V
    .locals 6

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    move-object v1, p2

    move-object v2, p1

    move v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Ljava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;-><init>(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    invoke-virtual {v1, v4}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->setMustAuthenticate(Z)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->setSkipRemoteDialog(Z)V

    invoke-virtual {v1, p6}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->setNoAnimation(Z)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    return-void
.end method

.method public final a(Ljava/lang/String;ZLcom/google/android/apps/youtube/core/client/WatchFeature;)V
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    const-string v2, ""

    const/4 v3, -0x1

    const/4 v4, 0x0

    move-object v1, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Ljava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;-><init>(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    invoke-virtual {v1, p2}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;->setMustAuthenticate(Z)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->b:Ljava/lang/Class;

    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->b(Landroid/net/Uri;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    return-void
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenPairingActivity;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/app/search/SearchType;->DEFAULT_SEARCH_TYPE:Lcom/google/android/apps/youtube/app/search/SearchType;

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a(Lcom/google/android/apps/youtube/app/search/SearchType;Ljava/lang/String;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    invoke-static {}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->e()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    const-string v2, ""

    const/4 v3, -0x1

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->OFFLINE:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Ljava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;->setOffline(Z)V

    new-instance v1, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;-><init>(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;)V

    return-void
.end method

.method public final e()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    const-class v2, Lcom/google/android/apps/youtube/app/honeycomb/SettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->d(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    return-void
.end method

.method public final f()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    const-class v2, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final g()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/a;->a:Landroid/app/Activity;

    const-class v2, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public final h()V
    .locals 1

    invoke-static {}, Lcom/google/android/apps/youtube/app/fragments/navigation/d;->a()Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/a;->a(Lcom/google/android/apps/youtube/app/fragments/navigation/PaneDescriptor;)V

    return-void
.end method
