.class final Lcom/google/android/apps/youtube/app/fragments/o;
.super Lcom/google/android/apps/youtube/app/ui/f;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/aw;Z)V
    .locals 7

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/fragments/o;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/ui/f;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/aw;Z)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Event;I)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->targetIsVideo()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->targetVideo:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/o;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->a(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;->ChannelActivity:Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;

    invoke-interface {v0, v1, p2}, Lcom/google/android/apps/youtube/core/Analytics;->a(Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;I)V

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_UPLOADED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CHANNEL_ACTIVITY_UPLOAD:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/o;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->b(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/app/am;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->target:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/apps/youtube/app/am;->a(Ljava/lang/String;ZLcom/google/android/apps/youtube/core/client/WatchFeature;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/core/client/WatchFeature;->GUIDE_CHANNEL_ACTIVITY:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/o;->a:Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;->b(Lcom/google/android/apps/youtube/app/fragments/ChannelFragment;)Lcom/google/android/apps/youtube/app/am;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->target:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/am;->a(Ljava/lang/String;)V

    goto :goto_1
.end method
