.class final Lcom/google/android/apps/youtube/app/ui/ia;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/a/l;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/ui/hj;

.field private final b:Lcom/google/android/apps/youtube/app/ui/v;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/hj;Lcom/google/android/apps/youtube/app/ui/v;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/ia;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/ia;->b:Lcom/google/android/apps/youtube/app/ui/v;

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/VolleyError;)V
    .locals 2

    const-string v0, "Error deleting from playlist"

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ia;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/hj;->a(Lcom/google/android/apps/youtube/app/ui/hj;)Lcom/google/android/apps/youtube/core/aw;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/aw;->c(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ia;->b:Lcom/google/android/apps/youtube/app/ui/v;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/v;->a(Z)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ia;->a:Lcom/google/android/apps/youtube/app/ui/hj;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/hj;->a:Landroid/app/Activity;

    sget v1, Lcom/google/android/youtube/p;->eY:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->a(Landroid/content/Context;II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/ia;->b:Lcom/google/android/apps/youtube/app/ui/v;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/v;->a(Z)V

    return-void
.end method
