.class final Lcom/google/android/apps/youtube/app/p;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/p;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/p;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)V

    return-void
.end method

.method private varargs a([Ljava/lang/Long;)Ljava/lang/Void;
    .locals 9

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/p;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->b(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->d()Lcom/google/android/apps/youtube/core/offline/store/i;

    move-result-object v2

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/p;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->b(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->e()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/core/offline/store/i;->r(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAdBreakId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lcom/google/android/apps/youtube/core/offline/store/i;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v7

    if-eqz v7, :cond_1

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAdBreakId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v7

    invoke-virtual {v7, v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v7

    invoke-virtual {v2, v8, v1, v7}, Lcom/google/android/apps/youtube/core/offline/store/i;->b(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Long;

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/p;->a([Ljava/lang/Long;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/p;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    const-string v1, "All offline ad expiration times have been changed!"

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/core/utils/ah;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    new-instance v0, Lcom/google/android/apps/youtube/app/h;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/p;->a:Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/app/h;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;B)V

    new-array v1, v2, [Ljava/lang/Void;

    const/4 v2, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/h;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
