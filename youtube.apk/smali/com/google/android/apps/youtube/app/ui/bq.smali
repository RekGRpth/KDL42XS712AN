.class final Lcom/google/android/apps/youtube/app/ui/bq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/core/client/bc;

.field final synthetic b:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/core/client/bc;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/bq;->a:Lcom/google/android/apps/youtube/core/client/bc;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/bq;->b:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bq;->a:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/client/bc;->i()V

    invoke-static {}, Lcom/google/android/apps/youtube/app/ui/bm;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "feature"

    const-string v2, "android"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "app_version"

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/bq;->b:Landroid/app/Activity;

    invoke-static {v2}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->g(Landroid/content/Context;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/bq;->b:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
