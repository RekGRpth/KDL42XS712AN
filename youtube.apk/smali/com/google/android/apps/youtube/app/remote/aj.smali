.class public final Lcom/google/android/apps/youtube/app/remote/aj;
.super Lcom/google/android/apps/youtube/app/remote/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/remote/aw;
.implements Lcom/google/android/apps/youtube/app/remote/bi;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

.field private final b:Lcom/google/android/apps/youtube/app/remote/bk;

.field private final c:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

.field private d:Lcom/google/android/apps/youtube/app/remote/am;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/app/ac;Z)V
    .locals 3

    if-nez p6, :cond_0

    const-string v0, "true"

    const-string v1, "enable_mdx_logs"

    const-string v2, ""

    invoke-virtual {p5, v1, v2}, Lcom/google/android/apps/youtube/app/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/apps/youtube/app/remote/q;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Z)V

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/aj;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/bk;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/aj;->b:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/aj;->c:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    invoke-virtual {p4, p0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/aw;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)Landroid/support/v7/media/c;
    .locals 3

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/remote/aj;->d(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)Landroid/support/v7/media/d;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "screen"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/media/d;->a(Landroid/os/Bundle;)Landroid/support/v7/media/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/aj;->b:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bg;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/aj;->b:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->s()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/media/d;->c(I)Landroid/support/v7/media/d;

    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/media/d;->a()Landroid/support/v7/media/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Landroid/support/v7/media/j;
    .locals 6

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/remote/aj;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/app/remote/ak;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/aj;->b:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/aj;->d:Lcom/google/android/apps/youtube/app/remote/am;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/remote/aj;->c:Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/remote/aj;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/remote/ak;-><init>(Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/youtube/app/remote/am;Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/remote/q;->g()V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/remote/am;)V
    .locals 1

    const-string v0, "provider cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/am;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/aj;->d:Lcom/google/android/apps/youtube/app/remote/am;

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/app/remote/am;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/aj;->d:Lcom/google/android/apps/youtube/app/remote/am;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/aj;->d:Lcom/google/android/apps/youtube/app/remote/am;

    :cond_0
    return-void
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    const-string v0, "MDX_MEDIA_ROUTE_CONTROL_CATEGORY"

    return-object v0
.end method
