.class public abstract Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;
.super Lcom/google/android/apps/youtube/app/adapter/k;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/client/bj;

.field private final b:Lcom/google/android/apps/youtube/common/network/h;

.field private final c:Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V
    .locals 6

    const/16 v5, 0xa0

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget v0, Lcom/google/android/youtube/j;->fy:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/app/adapter/k;-><init>(Landroid/content/Context;I)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->a:Lcom/google/android/apps/youtube/core/client/bj;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->b:Lcom/google/android/apps/youtube/common/network/h;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->c:Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->d:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/google/android/youtube/e;->d:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->e:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-boolean v4, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->d:Z

    iget v0, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    if-le v0, v5, :cond_1

    move v0, v1

    :goto_0
    and-int/2addr v0, v4

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->d:Z

    iget-boolean v4, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->e:Z

    iget v0, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    if-le v0, v5, :cond_2

    move v0, v1

    :goto_1
    and-int/2addr v0, v4

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->e:Z

    :cond_0
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_4

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->e:Z

    const/16 v4, 0x30

    if-lt v0, v4, :cond_3

    :goto_2
    and-int v0, v3, v1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->e:Z

    :goto_3
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2

    :cond_4
    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->d:Z

    const/16 v4, 0x18

    if-lt v0, v4, :cond_5

    :goto_4
    and-int v0, v3, v1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->d:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->e:Z

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_4
.end method


# virtual methods
.method protected abstract a(Ljava/lang/Object;)Landroid/net/Uri;
.end method

.method protected final a(Landroid/graphics/Matrix;Landroid/widget/ImageView;Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 9

    const/4 v0, 0x0

    const/high16 v7, 0x3f000000    # 0.5f

    invoke-virtual {p1}, Landroid/graphics/Matrix;->reset()V

    invoke-virtual {p3}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {p3}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {p2}, Landroid/widget/ImageView;->getWidth()I

    move-result v2

    invoke-virtual {p2}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v2, v4

    invoke-virtual {p2}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v4

    sub-int v4, v2, v4

    invoke-virtual {p2}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    invoke-virtual {p2}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v5

    sub-int/2addr v2, v5

    invoke-virtual {p2}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v5

    sub-int v5, v2, v5

    mul-int v2, v1, v5

    mul-int v6, v4, v3

    if-le v2, v6, :cond_0

    int-to-float v2, v5

    int-to-float v3, v3

    div-float/2addr v2, v3

    int-to-float v3, v4

    int-to-float v1, v1

    mul-float/2addr v1, v2

    sub-float v1, v3, v1

    mul-float/2addr v1, v7

    :goto_0
    invoke-virtual {p1, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    add-float/2addr v1, v7

    float-to-int v1, v1

    int-to-float v1, v1

    add-float/2addr v0, v7

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return-void

    :cond_0
    int-to-float v2, v4

    int-to-float v1, v1

    div-float/2addr v2, v1

    int-to-float v1, v5

    int-to-float v3, v3

    mul-float/2addr v3, v2

    sub-float/2addr v1, v3

    const v3, 0x3eb33333    # 0.35f

    mul-float/2addr v1, v3

    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_0
.end method

.method protected a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->a:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method protected a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 6

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->d(Ljava/lang/Object;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->c(Ljava/lang/Object;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->b(Ljava/lang/Object;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->a(Ljava/lang/Object;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v1, :cond_2

    :goto_0
    if-eqz v0, :cond_3

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->e:Z

    if-eqz v3, :cond_3

    :goto_1
    if-eqz v0, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->c:Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    sget-object v3, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;->LARGE:Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    if-ne v2, v3, :cond_4

    :cond_0
    :goto_2
    if-eqz v0, :cond_1

    invoke-virtual {p0, v0, p3}, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_1
    return-void

    :cond_2
    move-object v1, v3

    goto :goto_0

    :cond_3
    move-object v0, v2

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->c:Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;

    if-nez v2, :cond_6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->g:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xea60

    cmp-long v2, v2, v4

    if-lez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->b:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/common/network/h;->h()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->f:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->g:J

    :cond_5
    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->f:Z

    if-eqz v2, :cond_6

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory;->d:Z

    if-nez v2, :cond_0

    :cond_6
    move-object v0, v1

    goto :goto_2
.end method

.method protected abstract b(Ljava/lang/Object;)Landroid/net/Uri;
.end method

.method protected abstract c(Ljava/lang/Object;)Landroid/net/Uri;
.end method

.method protected abstract d(Ljava/lang/Object;)Landroid/net/Uri;
.end method
