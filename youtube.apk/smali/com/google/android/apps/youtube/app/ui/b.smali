.class public final Lcom/google/android/apps/youtube/app/ui/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/u;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/core/client/bj;

.field private final c:Landroid/view/View;

.field private final d:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final e:Landroid/widget/ImageView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/TextView;

.field private final i:Lcom/google/android/apps/youtube/common/a/a;

.field private final j:Lcom/google/android/apps/youtube/common/a/a;

.field private final k:Landroid/widget/RatingBar;

.field private final l:Landroid/widget/ImageView;

.field private m:Lcom/google/android/apps/youtube/common/a/d;

.field private n:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

.field private o:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;

.field private p:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bj;Landroid/view/View;Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/b;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->b:Lcom/google/android/apps/youtube/core/client/bj;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/b;->c:Landroid/view/View;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/ui/b;->d:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/e;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/youtube/app/ui/e;-><init>(Lcom/google/android/apps/youtube/app/ui/b;B)V

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->i:Lcom/google/android/apps/youtube/common/a/a;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/d;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/youtube/app/ui/d;-><init>(Lcom/google/android/apps/youtube/app/ui/b;B)V

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/a/a;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->j:Lcom/google/android/apps/youtube/common/a/a;

    sget v0, Lcom/google/android/youtube/j;->u:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->e:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/youtube/j;->r:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->f:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->dW:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->k:Landroid/widget/RatingBar;

    sget v0, Lcom/google/android/youtube/j;->dX:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->l:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/youtube/j;->s:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->h:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->b:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->g:Landroid/widget/TextView;

    const/16 v0, 0x8

    invoke-virtual {p3, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->g:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/c;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/youtube/app/ui/c;-><init>(Lcom/google/android/apps/youtube/app/ui/b;B)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->n:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->n:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getInfoCardApp()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getRatingImageUri()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->n:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getInfoCardApp()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->hasRating()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->j:Lcom/google/android/apps/youtube/common/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->m:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->b:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/b;->n:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getInfoCardApp()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getRatingImageUri()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/b;->m:Lcom/google/android/apps/youtube/common/a/d;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/b;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->n:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->o:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->d:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/b;->n:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/b;->o:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->a:Landroid/app/Activity;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/b;->o:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getLinkUrl()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/b;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/b;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/b;Landroid/graphics/Bitmap;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/b;->l:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private e()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->p:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->n:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->d:Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/b;->n:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->c:Landroid/view/View;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Z
    .locals 7

    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getInfoCards()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getInfoCards()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getType()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getInfoCardApp()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    move-result-object v4

    if-eqz v4, :cond_0

    move-object v2, v0

    :goto_0
    if-nez v2, :cond_2

    move v0, v1

    :goto_1
    return v0

    :cond_1
    move-object v2, v3

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getActions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getType()I

    move-result v5

    const/4 v6, 0x4

    if-ne v5, v6, :cond_3

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->o:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/b;->g:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    iput-object v2, p0, Lcom/google/android/apps/youtube/app/ui/b;->n:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->f:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getInfoCardApp()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getAppName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/ui/b;->h:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getInfoCardApp()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getPrice()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, ""

    :goto_2
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getInfoCardApp()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->hasRating()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->l:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->k:Landroid/widget/RatingBar;

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->k:Landroid/widget/RatingBar;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/b;->n:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getInfoCardApp()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getRating()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setRating(F)V

    :cond_5
    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getInfoCardApp()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getIcon()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->i:Lcom/google/android/apps/youtube/common/a/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/a/d;->a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->m:Lcom/google/android/apps/youtube/common/a/d;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->b:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getInfoCardApp()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getIcon()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/b;->m:Lcom/google/android/apps/youtube/common/a/d;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    :goto_3
    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_6
    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getInfoCardApp()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getPrice()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_7
    invoke-direct {p0, v3}, Lcom/google/android/apps/youtube/app/ui/b;->a(Landroid/graphics/Bitmap;)V

    goto :goto_3
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->p:Z

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/b;->e()V

    return-void
.end method

.method public final c()V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->p:Z

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/ui/b;->n:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/ui/b;->o:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->m:Lcom/google/android/apps/youtube/common/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->m:Lcom/google/android/apps/youtube/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/a/d;->a()V

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/ui/b;->m:Lcom/google/android/apps/youtube/common/a/d;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->k:Landroid/widget/RatingBar;

    invoke-virtual {v0, v3}, Landroid/widget/RatingBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->l:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/b;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final d()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/b;->e()V

    return-void
.end method
