.class public final Lcom/google/android/apps/youtube/app/honeycomb/widget/c;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 5

    const/4 v4, 0x0

    invoke-static {p0}, Lcom/google/android/apps/youtube/app/honeycomb/widget/c;->c(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/j;->gs:I

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v1, Lcom/google/android/youtube/j;->gu:I

    invoke-virtual {v0, v1, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v1, Lcom/google/android/youtube/j;->gx:I

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v3, 0x4000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v2

    invoke-static {p0, v4, v2, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setPendingIntentTemplate(ILandroid/app/PendingIntent;)V

    return-object v0
.end method

.method public static b(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 4

    invoke-static {p0}, Lcom/google/android/apps/youtube/app/honeycomb/widget/c;->c(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/j;->gs:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v2, Lcom/google/android/youtube/j;->gu:I

    const/16 v3, 0x8

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v2, Lcom/google/android/youtube/j;->gt:I

    sget v3, Lcom/google/android/youtube/p;->bs:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    sget v2, Lcom/google/android/youtube/j;->gv:I

    sget v3, Lcom/google/android/youtube/p;->fZ:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    sget v1, Lcom/google/android/youtube/j;->gs:I

    invoke-static {p0}, Lcom/google/android/apps/youtube/app/honeycomb/widget/a;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    return-object v0
.end method

.method private static c(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 3

    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/l;->bN:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    sget v1, Lcom/google/android/youtube/j;->gx:I

    sget v2, Lcom/google/android/youtube/j;->gy:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setEmptyView(II)V

    return-object v0
.end method
