.class final Lcom/google/android/apps/youtube/app/offline/a/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/offline/a/f;

.field private final b:Lcom/google/android/apps/youtube/app/offline/a/af;

.field private final c:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

.field private final d:Ljava/util/HashSet;

.field private volatile e:Lcom/google/android/apps/youtube/datalib/legacy/model/s;

.field private volatile f:I

.field private volatile g:I

.field private volatile h:Z

.field private volatile i:Z


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/offline/a/f;Lcom/google/android/apps/youtube/app/offline/a/af;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/offline/a/af;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->b:Lcom/google/android/apps/youtube/app/offline/a/af;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    new-instance v0, Ljava/util/HashSet;

    iget v1, p3, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->d:Ljava/util/HashSet;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/offline/a/f;Lcom/google/android/apps/youtube/app/offline/a/af;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/offline/a/ag;-><init>(Lcom/google/android/apps/youtube/app/offline/a/f;Lcom/google/android/apps/youtube/app/offline/a/af;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V

    return-void
.end method

.method private declared-synchronized a()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/offline/a/ag;)Lcom/google/android/apps/youtube/datalib/legacy/model/s;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/offline/a/ag;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/offline/a/ag;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/ag;->a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/offline/a/ag;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/ag;->a(Ljava/lang/String;)V

    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 8

    const-wide/16 v3, 0x0

    const/4 v1, 0x2

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/p;->c(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x0

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->d:Ljava/util/HashSet;

    invoke-virtual {v6, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->d:Ljava/util/HashSet;

    invoke-virtual {v0, v5}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->b:Lcom/google/android/apps/youtube/app/offline/a/af;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v6, v6, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-static {v0, v5, v6}, Lcom/google/android/apps/youtube/app/offline/a/af;->b(Lcom/google/android/apps/youtube/app/offline/a/af;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v5, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->FAILED:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-ne v0, v5, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->i:Z

    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/p;->b(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v5, v5, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v5, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->RUNNING:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-ne v0, v5, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->h:Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    if-lez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->d:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v5

    sub-int v5, v0, v5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    if-ne v5, v0, :cond_4

    const/16 v0, 0x64

    iput v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->f:I

    iput v5, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->g:I

    move v0, v1

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->h:Z

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->PENDING:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-ne v2, v3, :cond_2

    move v0, v1

    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/offline/a/ag;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_4
    mul-int/lit8 v0, v5, 0x64

    :try_start_1
    iget-object v6, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget v6, v6, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    div-int/2addr v0, v6

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->a()Z

    move-result v6

    if-eqz v6, :cond_6

    iget-wide v6, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->f:J

    cmp-long v6, v6, v3

    if-eqz v6, :cond_5

    iget-wide v3, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->e:J

    const-wide/16 v6, 0x64

    mul-long/2addr v3, v6

    iget-wide v6, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->f:J

    div-long/2addr v3, v6

    :cond_5
    long-to-int v3, v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget v4, v4, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    div-int/2addr v3, v4

    add-int/2addr v0, v3

    :cond_6
    const/16 v3, 0x63

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget v3, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->f:I

    if-gt v0, v3, :cond_7

    iget v3, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->g:I

    if-le v5, v3, :cond_8

    :cond_7
    iput v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->f:I

    iput v5, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->g:I

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->h:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_8

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_8
    move v0, v2

    goto :goto_0
.end method

.method private declared-synchronized a(Ljava/lang/String;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->b:Lcom/google/android/apps/youtube/app/offline/a/af;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/youtube/app/offline/a/af;->a(Lcom/google/android/apps/youtube/app/offline/a/af;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/offline/a/ag;)I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/offline/a/ag;->a()I

    move-result v0

    return v0
.end method

.method private b()Lcom/google/android/apps/youtube/datalib/legacy/model/s;
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/offline/a/ag;->a()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->f:I

    iget-boolean v4, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->i:Z

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;IIZ)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/offline/a/ag;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/offline/a/ag;->b(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    return-void
.end method

.method private declared-synchronized b(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/p;->c(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->d:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->b:Lcom/google/android/apps/youtube/app/offline/a/af;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v2, v2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/app/offline/a/af;->b(Lcom/google/android/apps/youtube/app/offline/a/af;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/core/utils/p;->b(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x2

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget v1, v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget v1, v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->d:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->g:I

    iget v1, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->g:I

    mul-int/lit8 v1, v1, 0x64

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget v2, v2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    div-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->f:I

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->e:Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->a:Lcom/google/android/apps/youtube/app/offline/a/f;

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/offline/a/ag;->b()Lcom/google/android/apps/youtube/datalib/legacy/model/s;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/youtube/app/offline/a/f;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/s;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/offline/a/ag;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/a/ag;->c:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    return-object v0
.end method
