.class public final Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$ConfirmRemoveDialogFragment;
.super Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;
.source "SourceFile"


# instance fields
.field private Y:Lcom/google/android/apps/youtube/app/honeycomb/phone/u;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/YouTubeDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$ConfirmRemoveDialogFragment;)Lcom/google/android/apps/youtube/app/honeycomb/phone/u;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$ConfirmRemoveDialogFragment;->Y:Lcom/google/android/apps/youtube/app/honeycomb/phone/u;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/app/honeycomb/phone/u;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$ConfirmRemoveDialogFragment;->Y:Lcom/google/android/apps/youtube/app/honeycomb/phone/u;

    return-void
.end method

.method public final c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    sget v0, Lcom/google/android/youtube/p;->ff:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$ConfirmRemoveDialogFragment;->h()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "screenName"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$ConfirmRemoveDialogFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$ConfirmRemoveDialogFragment;->i()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/google/android/youtube/p;->fe:I

    new-instance v3, Lcom/google/android/apps/youtube/app/honeycomb/phone/t;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/t;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$ConfirmRemoveDialogFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/google/android/youtube/p;->K:I

    new-instance v3, Lcom/google/android/apps/youtube/app/honeycomb/phone/s;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/s;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity$ConfirmRemoveDialogFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
