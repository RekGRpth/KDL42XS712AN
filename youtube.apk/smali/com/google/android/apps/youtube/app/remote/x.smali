.class final Lcom/google/android/apps/youtube/app/remote/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/common/a/b;

.field final synthetic b:Lcom/google/android/apps/ytremote/model/ScreenId;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/google/android/apps/youtube/app/remote/t;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/remote/t;Lcom/google/android/apps/youtube/common/a/b;Lcom/google/android/apps/ytremote/model/ScreenId;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/x;->d:Lcom/google/android/apps/youtube/app/remote/t;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/remote/x;->a:Lcom/google/android/apps/youtube/common/a/b;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/remote/x;->b:Lcom/google/android/apps/ytremote/model/ScreenId;

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/remote/x;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/x;->d:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/t;->b(Lcom/google/android/apps/youtube/app/remote/t;)Landroid/util/Pair;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/youtube/app/remote/t;->a()Landroid/util/Pair;

    move-result-object v0

    if-ne v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/x;->a:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/x;->b:Lcom/google/android/apps/ytremote/model/ScreenId;

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Authentication failed."

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/x;->d:Lcom/google/android/apps/youtube/app/remote/t;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/t;->c(Lcom/google/android/apps/youtube/app/remote/t;)Lcom/google/android/apps/ytremote/backend/logic/a;

    move-result-object v2

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/x;->b:Lcom/google/android/apps/ytremote/model/ScreenId;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/remote/x;->c:Ljava/lang/String;

    invoke-interface {v2, v0, v1, v3, v4}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/x;->a:Lcom/google/android/apps/youtube/common/a/b;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/x;->b:Lcom/google/android/apps/ytremote/model/ScreenId;

    new-instance v3, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-direct {v3, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;-><init>(Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method
