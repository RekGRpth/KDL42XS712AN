.class final Lcom/google/android/apps/youtube/app/d/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/a/l;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

.field final synthetic b:Lcom/google/android/apps/youtube/app/d/ac;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/d/ac;Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/d/ad;->b:Lcom/google/android/apps/youtube/app/d/ac;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/d/ad;->a:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/VolleyError;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ad;->a:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ad;->b:Lcom/google/android/apps/youtube/app/d/ac;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/d/ac;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->b(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/uilib/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->notifyDataSetChanged()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 2

    check-cast p1, Lorg/json/JSONObject;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ad;->a:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->a(Lorg/json/JSONObject;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ad;->b:Lcom/google/android/apps/youtube/app/d/ac;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/d/ac;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->j(Lcom/google/android/apps/youtube/app/d/f;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ad;->a:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/d/ad;->b:Lcom/google/android/apps/youtube/app/d/ac;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/d/ac;->a:Lcom/google/android/apps/youtube/app/d/f;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/d/f;->b(Lcom/google/android/apps/youtube/app/d/f;)Lcom/google/android/apps/youtube/uilib/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/uilib/a/h;->notifyDataSetChanged()V

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/android/volley/ParseError;

    invoke-direct {v1, v0}, Lcom/android/volley/ParseError;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/d/ad;->a(Lcom/android/volley/VolleyError;)V

    goto :goto_0
.end method
