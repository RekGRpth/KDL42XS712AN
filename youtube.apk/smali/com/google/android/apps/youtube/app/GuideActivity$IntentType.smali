.class public final enum Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

.field public static final enum BROWSE:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

.field public static final enum NONE:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

.field public static final enum SEARCH:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

.field public static final enum WATCH:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->NONE:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    new-instance v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    const-string v1, "WATCH"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->WATCH:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    new-instance v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    const-string v1, "SEARCH"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->SEARCH:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    new-instance v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    const-string v1, "BROWSE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->BROWSE:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    sget-object v1, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->NONE:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->WATCH:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->SEARCH:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->BROWSE:Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->$VALUES:[Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->$VALUES:[Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/app/GuideActivity$IntentType;

    return-object v0
.end method
