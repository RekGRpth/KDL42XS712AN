.class final Lcom/google/android/apps/youtube/app/adapter/ap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/adapter/ae;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/adapter/ao;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/view/View;

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/adapter/ao;Landroid/view/View;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->a:Lcom/google/android/apps/youtube/app/adapter/ao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lcom/google/android/youtube/j;->dF:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->e:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->e:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->e:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->y:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->e:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fV:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->f:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->g:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->h:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->i:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/adapter/ao;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/ap;-><init>(Lcom/google/android/apps/youtube/app/adapter/ao;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 7

    const/4 v1, 0x0

    check-cast p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->b:Landroid/widget/TextView;

    iget-object v2, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->title:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->b:Landroid/widget/TextView;

    iget v3, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->f:I

    iget v4, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->g:I

    iget v5, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->h:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->a:Lcom/google/android/apps/youtube/app/adapter/ao;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ao;->a(Lcom/google/android/apps/youtube/app/adapter/ao;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v2, v3, v4, v5, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->a:Lcom/google/android/apps/youtube/app/adapter/ao;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ao;->a(Lcom/google/android/apps/youtube/app/adapter/ao;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->c:Landroid/widget/TextView;

    iget-object v2, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->author:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->a:Lcom/google/android/apps/youtube/app/adapter/ao;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ao;->a(Lcom/google/android/apps/youtube/app/adapter/ao;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->a:Lcom/google/android/apps/youtube/app/adapter/ao;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/adapter/ao;->b(Lcom/google/android/apps/youtube/app/adapter/ao;)Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/youtube/o;->l:I

    iget v4, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p2, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->e:Landroid/view/View;

    return-object v0

    :cond_3
    iget v0, p0, Lcom/google/android/apps/youtube/app/adapter/ap;->i:I

    goto :goto_0

    :cond_4
    const/16 v0, 0x8

    goto :goto_1
.end method
