.class public final enum Lcom/google/android/apps/youtube/app/ui/LikeAction;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/app/ui/LikeAction;

.field public static final enum DISLIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

.field public static final enum LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

.field public static final enum REMOVE_LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;


# instance fields
.field private final successToastStringId:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/LikeAction;

    const-string v1, "LIKE"

    sget v2, Lcom/google/android/youtube/p;->eM:I

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/youtube/app/ui/LikeAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/LikeAction;->LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/LikeAction;

    const-string v1, "DISLIKE"

    sget v2, Lcom/google/android/youtube/p;->eK:I

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/youtube/app/ui/LikeAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/LikeAction;->DISLIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/LikeAction;

    const-string v1, "REMOVE_LIKE"

    sget v2, Lcom/google/android/youtube/p;->eP:I

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/youtube/app/ui/LikeAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/LikeAction;->REMOVE_LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/youtube/app/ui/LikeAction;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/LikeAction;->LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/LikeAction;->DISLIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/LikeAction;->REMOVE_LIKE:Lcom/google/android/apps/youtube/app/ui/LikeAction;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/youtube/app/ui/LikeAction;->$VALUES:[Lcom/google/android/apps/youtube/app/ui/LikeAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/app/ui/LikeAction;->successToastStringId:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/ui/LikeAction;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/app/ui/LikeAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/LikeAction;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/app/ui/LikeAction;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/ui/LikeAction;->$VALUES:[Lcom/google/android/apps/youtube/app/ui/LikeAction;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/app/ui/LikeAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/app/ui/LikeAction;

    return-object v0
.end method


# virtual methods
.method public final getSuccessToastStringId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/LikeAction;->successToastStringId:I

    return v0
.end method
