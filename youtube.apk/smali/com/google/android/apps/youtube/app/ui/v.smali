.class public final Lcom/google/android/apps/youtube/app/ui/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/content/res/Resources;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:Landroid/view/View;

.field private final e:Landroid/app/Dialog;

.field private final f:Lcom/google/android/apps/youtube/app/ui/w;

.field private g:Lcom/google/android/apps/youtube/app/ui/ac;

.field private final h:I

.field private final i:I

.field private j:I

.field private k:Z

.field private l:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v3, p0, Lcom/google/android/apps/youtube/app/ui/v;->k:Z

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->a:Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/v;->h:I

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->i:I

    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->c:Landroid/view/LayoutInflater;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->b:Landroid/content/res/Resources;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/w;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/w;-><init>(Lcom/google/android/apps/youtube/app/ui/v;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->f:Lcom/google/android/apps/youtube/app/ui/w;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->c:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/youtube/l;->C:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->d:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->d:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->bU:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/v;->f:Lcom/google/android/apps/youtube/app/ui/w;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v0, Landroid/app/Dialog;

    sget v1, Lcom/google/android/youtube/q;->b:I

    invoke-direct {v0, p1, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->e:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->e:Landroid/app/Dialog;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/v;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->e:Landroid/app/Dialog;

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/g;->G:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    const v1, 0x1030002    # android.R.style.Animation_Dialog

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    const/16 v1, 0x300

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/v;->e:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method private a(IIILcom/google/android/apps/youtube/app/ui/ab;)I
    .locals 3

    if-lez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, -0x1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/v;->f:Lcom/google/android/apps/youtube/app/ui/w;

    invoke-static {v2, p1, v0, v1, p4}, Lcom/google/android/apps/youtube/app/ui/w;->a(Lcom/google/android/apps/youtube/app/ui/w;ILjava/lang/String;ILcom/google/android/apps/youtube/app/ui/ab;)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/v;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->d:Landroid/view/View;

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/v;->h:I

    iget v2, p0, Lcom/google/android/apps/youtube/app/ui/v;->i:I

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->j:I

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/v;)Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->c:Landroid/view/LayoutInflater;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->f:Lcom/google/android/apps/youtube/app/ui/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/w;->getCount()I

    move-result v0

    return v0
.end method

.method public final a(IILcom/google/android/apps/youtube/app/ui/ab;)I
    .locals 2

    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, v0, p2, v1, p3}, Lcom/google/android/apps/youtube/app/ui/v;->a(IIILcom/google/android/apps/youtube/app/ui/ab;)I

    move-result v0

    return v0
.end method

.method public final a(ILcom/google/android/apps/youtube/app/ui/ab;)I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->f:Lcom/google/android/apps/youtube/app/ui/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/w;->a()I

    move-result v0

    const/4 v1, -0x1

    invoke-direct {p0, v0, p1, v1, p2}, Lcom/google/android/apps/youtube/app/ui/v;->a(IIILcom/google/android/apps/youtube/app/ui/ab;)I

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/apps/youtube/app/ui/aa;Lcom/google/android/apps/youtube/app/ui/ab;)I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->f:Lcom/google/android/apps/youtube/app/ui/w;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/v;->f:Lcom/google/android/apps/youtube/app/ui/w;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/w;->a()I

    move-result v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/apps/youtube/app/ui/w;->a(ILcom/google/android/apps/youtube/app/ui/aa;Lcom/google/android/apps/youtube/app/ui/ab;)I

    move-result v0

    return v0
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->f:Lcom/google/android/apps/youtube/app/ui/w;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/w;->b(I)V

    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/ui/ac;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/v;->g:Lcom/google/android/apps/youtube/app/ui/ac;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/v;->k:Z

    return-void
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->l:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method public final b(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->f:Lcom/google/android/apps/youtube/app/ui/w;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/w;->c(I)V

    return-void
.end method

.method public final c(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->f:Lcom/google/android/apps/youtube/app/ui/w;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/w;->d(I)V

    return-void
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->k:Z

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->g:Lcom/google/android/apps/youtube/app/ui/ac;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->g:Lcom/google/android/apps/youtube/app/ui/ac;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/ac;->a(Lcom/google/android/apps/youtube/app/ui/v;Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->f:Lcom/google/android/apps/youtube/app/ui/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/w;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p1, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/v;->e:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v3, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    iget v4, p0, Lcom/google/android/apps/youtube/app/ui/v;->j:I

    add-int/2addr v3, v4

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    if-le v3, v0, :cond_2

    iget v0, v1, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/google/android/apps/youtube/app/ui/v;->j:I

    sub-int/2addr v0, v1

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/v;->l:Landroid/view/View;

    :cond_1
    return-void

    :cond_2
    iget v0, v1, Landroid/graphics/Rect;->bottom:I

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    goto :goto_0
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/v;->f:Lcom/google/android/apps/youtube/app/ui/w;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/youtube/app/ui/w;->a(I)Lcom/google/android/apps/youtube/app/ui/ab;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/v;->l:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/v;->l:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/ui/ab;->a(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/v;->b()V

    :cond_0
    return-void
.end method
