.class final Lcom/google/android/apps/youtube/app/honeycomb/phone/c;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x2710

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;)Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "MDXPushNotificationPairingSuccessful"

    const-string v2, "NavigateToPostPairing"

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->d(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;)Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->c(Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;)Lcom/google/android/apps/youtube/app/honeycomb/phone/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->b(Lcom/google/android/apps/youtube/app/remote/aq;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->n:Lcom/google/android/apps/youtube/app/am;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->n:Lcom/google/android/apps/youtube/app/am;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/am;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/honeycomb/phone/c;->a:Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;->n:Lcom/google/android/apps/youtube/app/am;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/am;->g()V

    :cond_0
    return-void
.end method
