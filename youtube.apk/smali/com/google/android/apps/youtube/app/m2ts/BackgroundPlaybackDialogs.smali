.class public final Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/content/SharedPreferences;

.field private final c:Lcom/google/android/apps/youtube/app/ui/hh;

.field private d:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;

.field private e:J

.field private f:Lcom/google/a/a/a/a/ai;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/app/ui/hh;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->b:Landroid/content/SharedPreferences;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/hh;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->c:Lcom/google/android/apps/youtube/app/ui/hh;

    sget-object v0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;->NONE:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->d:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->b:Landroid/content/SharedPreferences;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;->BACKGROUND_SUCCESS:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->d:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->e:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->f:Lcom/google/a/a/a/a/ai;

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "background_dialog_type"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->d:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->f:Lcom/google/a/a/a/a/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->f:Lcom/google/a/a/a/a/ai;

    invoke-static {v0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v0

    :goto_0
    const-string v1, "background_failed_message"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v0, "background_start_time"

    iget-wide v1, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->e:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/a/a/a/a/ai;)V
    .locals 2

    sget-object v0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;->BACKGROUND_FAILED:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->d:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->e:J

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->f:Lcom/google/a/a/a/a/ai;

    return-void
.end method

.method public final b()V
    .locals 2

    sget-object v0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;->NONE:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->d:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->e:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->f:Lcom/google/a/a/a/a/ai;

    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "background_dialog_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;->values()[Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;->values()[Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;

    move-result-object v1

    aget-object v0, v1, v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->d:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;

    :cond_1
    const-string v0, "background_failed_message"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_2

    :try_start_0
    new-instance v1, Lcom/google/a/a/a/a/ai;

    invoke-direct {v1}, Lcom/google/a/a/a/a/ai;-><init>()V

    invoke-static {v1, v0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->f:Lcom/google/a/a/a/a/ai;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    const-string v0, "background_start_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->e:J

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final c()V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->e:J

    sub-long/2addr v3, v5

    const-wide/16 v5, 0x7530

    cmp-long v0, v3, v5

    if-gez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/app/m2ts/b;->a:[I

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->d:Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs$DialogType;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->b:Landroid/content/SharedPreferences;

    const-string v3, "show_background_feature_dialog"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/app/m2ts/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/m2ts/a;-><init>(Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->a:Landroid/app/Activity;

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/google/android/youtube/p;->F:I

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v3, Lcom/google/android/youtube/p;->G:I

    invoke-virtual {v1, v3, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->dA:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "show_background_feature_dialog"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->f:Lcom/google/a/a/a/a/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->c:Lcom/google/android/apps/youtube/app/ui/hh;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/m2ts/BackgroundPlaybackDialogs;->f:Lcom/google/a/a/a/a/ai;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/hh;->a(Lcom/google/a/a/a/a/ai;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
