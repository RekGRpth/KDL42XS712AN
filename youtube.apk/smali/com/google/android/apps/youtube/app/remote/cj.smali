.class final Lcom/google/android/apps/youtube/app/remote/cj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/ytremote/logic/c;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

.field private final b:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Ljava/util/Set;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/cj;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/remote/cj;->b:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)V
    .locals 4

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/cj;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v1, p1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/ytremote/model/YouTubeDevice;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->withNewDeviceName(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;-><init>(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/cj;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/cj;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->x()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->hasAppStatus()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getAppStatus()Lcom/google/android/apps/ytremote/model/AppStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/AppStatus;->getStatus()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/cj;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The app status for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is STOPPED. Will remove the route!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/cj;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->e(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/cj;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/cj;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->g(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/cj;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/ytremote/model/SsdpId;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->hasAppStatus()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->hasAppStatus()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getAppStatus()Lcom/google/android/apps/ytremote/model/AppStatus;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getAppStatus()Lcom/google/android/apps/ytremote/model/AppStatus;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/apps/ytremote/model/AppStatus;->appInfoEquals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/cj;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/cj;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->n(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;)Lcom/google/android/apps/youtube/app/remote/bk;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->x()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/cj;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;->a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/cj;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
