.class public Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/graphics/drawable/Drawable;

.field private b:Z

.field private c:Z

.field private d:I

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v0, Lcom/google/android/youtube/r;->B:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v4, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->b:Z

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->d:I

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->e:I

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->f:I

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->g:I

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setSelectorDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :cond_0
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    sget v3, Lcom/google/android/youtube/d;->a:I

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setSelectorDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->b:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->b:Z

    if-eqz v2, :cond_3

    move v2, v1

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->getWidth()I

    move-result v4

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->b:Z

    if-eqz v3, :cond_4

    move v3, v1

    :goto_2
    sub-int/2addr v4, v3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->getHeight()I

    move-result v5

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->b:Z

    if-eqz v3, :cond_5

    move v3, v1

    :goto_3
    sub-int v3, v5, v3

    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->d:I

    add-int/2addr v0, v5

    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->e:I

    add-int/2addr v2, v5

    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->f:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->g:I

    sub-int/2addr v3, v5

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, v0, v2, v4, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->c:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->getPaddingLeft()I

    move-result v0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->getPaddingTop()I

    move-result v2

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->getPaddingRight()I

    move-result v3

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->getPaddingBottom()I

    move-result v3

    goto :goto_3
.end method

.method protected drawableStateChanged()V
    .locals 2

    invoke-super {p0}, Landroid/widget/FrameLayout;->drawableStateChanged()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->invalidate()V

    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->c:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->invalidate()V

    return-void
.end method

.method public setSelector(I)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->setSelectorDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSelectorDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->a:Landroid/graphics/drawable/Drawable;

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->a:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->a:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->c:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->invalidate()V

    goto :goto_0
.end method

.method public setSelectorMargins(IIII)V
    .locals 1

    iput p1, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->d:I

    iput p2, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->e:I

    iput p3, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->f:I

    iput p4, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->g:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->c:Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/SelectorOnTopFrameLayout;->invalidate()V

    :cond_0
    return-void
.end method
