.class public Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;
.super Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"


# instance fields
.field private A:Landroid/widget/Button;

.field private B:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

.field private C:Lcom/google/android/apps/youtube/common/e/b;

.field private n:Landroid/view/View;

.field private o:Landroid/widget/TextView;

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/ListView;

.field private r:Landroid/widget/TextView;

.field private s:Lcom/google/android/apps/youtube/uilib/a/h;

.field private t:Landroid/widget/Button;

.field private u:Landroid/widget/Button;

.field private v:Landroid/widget/Button;

.field private w:Landroid/widget/Button;

.field private x:Lcom/google/android/apps/youtube/core/client/h;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Lcom/google/android/apps/youtube/common/e/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->C:Lcom/google/android/apps/youtube/common/e/b;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;J)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->x:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/core/client/h;->a(J)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->e()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->B:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Ljava/util/List;
    .locals 11

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->C:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->B:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->g()Ljava/util/Map;

    move-result-object v6

    new-instance v7, Ljava/util/TreeMap;

    invoke-direct {v7}, Ljava/util/TreeMap;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->B:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;->e()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/youtube/app/l;

    invoke-direct {v3, v0, v2}, Lcom/google/android/apps/youtube/app/l;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/x;Ljava/util/List;)V

    invoke-virtual {v7, v1, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/a/a/a/a/ml;

    iget-object v10, v1, Lcom/google/a/a/a/a/ml;->b:[Lcom/google/a/a/a/a/mk;

    array-length v10, v10

    if-lez v10, :cond_1

    new-instance v10, Lcom/google/android/apps/youtube/app/i;

    invoke-direct {v10, v1, v4, v5}, Lcom/google/android/apps/youtube/app/i;-><init>(Lcom/google/a/a/a/a/ml;J)V

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->b()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/google/android/apps/youtube/app/l;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v1, v2

    :goto_2
    invoke-direct {v10, v0, v1}, Lcom/google/android/apps/youtube/app/l;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/x;Ljava/util/List;)V

    invoke-virtual {v7, v9, v10}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    move-object v1, v3

    goto :goto_2

    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->r:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->q:Landroid/widget/ListView;

    return-object v0
.end method

.method private e()V
    .locals 7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->x:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/client/h;->g()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->C:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v2

    sub-long v0, v2, v0

    const-wide/16 v2, 0x9

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v5, 0x1

    invoke-virtual {v4, v5, v6}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    div-long/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->y:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ">="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "min"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)Lcom/google/android/apps/youtube/uilib/a/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->s:Lcom/google/android/apps/youtube/uilib/a/h;

    return-object v0
.end method


# virtual methods
.method public final d()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->finish()V

    :cond_0
    sget v0, Lcom/google/android/youtube/l;->F:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->setContentView(I)V

    sget v0, Lcom/google/android/youtube/j;->cT:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->n:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->o:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->cM:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->p:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->aG()Lcom/google/android/apps/youtube/common/e/n;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->C:Lcom/google/android/apps/youtube/common/e/b;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->o:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Signed in as "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/core/offline/store/q;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->B:Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    :goto_0
    new-instance v0, Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/uilib/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->s:Lcom/google/android/apps/youtube/uilib/a/h;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->s:Lcom/google/android/apps/youtube/uilib/a/h;

    const-class v2, Lcom/google/android/apps/youtube/app/l;

    new-instance v3, Lcom/google/android/apps/youtube/app/n;

    invoke-direct {v3, p0}, Lcom/google/android/apps/youtube/app/n;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/uilib/a/h;->a(Ljava/lang/Class;Lcom/google/android/apps/youtube/uilib/a/j;)V

    sget v0, Lcom/google/android/youtube/j;->ch:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->q:Landroid/widget/ListView;

    const v0, 0x1020004    # android.R.id.empty

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->r:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->q:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->s:Lcom/google/android/apps/youtube/uilib/a/h;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    sget v0, Lcom/google/android/youtube/j;->bJ:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->t:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->t:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/apps/youtube/app/a;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/a;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->eh:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->u:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->u:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/apps/youtube/app/b;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/b;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->bd:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->v:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->v:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/apps/youtube/app/c;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/c;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->bb:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->w:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->w:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/apps/youtube/app/d;

    invoke-direct {v2, p0}, Lcom/google/android/apps/youtube/app/d;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->H()Lcom/google/android/apps/youtube/core/client/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->x:Lcom/google/android/apps/youtube/core/client/h;

    sget v0, Lcom/google/android/youtube/j;->bZ:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->y:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->bX:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->z:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->z:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/youtube/app/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/e;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/youtube/j;->bY:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->A:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->A:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/youtube/app/f;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/app/f;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->e()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->n:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->p:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    new-instance v0, Lcom/google/android/apps/youtube/app/h;

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/youtube/app/h;-><init>(Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;B)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Void;

    const/4 v2, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/h;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/p;->dV:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/DebugOfflineAdActivity;->r:Landroid/widget/TextView;

    const-string v1, "Loading..."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
