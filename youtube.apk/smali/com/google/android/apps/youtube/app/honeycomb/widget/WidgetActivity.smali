.class public Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->D()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/l;->b(Landroid/content/Context;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google.android.youtube.action.widget_play"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;->Widget:Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;

    invoke-interface {v0, v2, v4}, Lcom/google/android/apps/youtube/core/Analytics;->a(Lcom/google/android/apps/youtube/core/Analytics$VideoCategory;I)V

    const-string v0, "video_id"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v6

    new-instance v0, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;

    const-string v2, ""

    const/4 v3, -0x1

    sget-object v5, Lcom/google/android/apps/youtube/core/client/WatchFeature;->WIDGET:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;-><init>(Ljava/lang/String;Ljava/lang/String;IILcom/google/android/apps/youtube/core/client/WatchFeature;)V

    const-string v1, "watch"

    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    invoke-direct {v2, v0}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;-><init>(Lcom/google/android/apps/youtube/core/player/model/PlaybackStartDescriptor;)V

    invoke-virtual {v6, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v6}, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/widget/WidgetActivity;->finish()V

    return-void

    :cond_0
    const-string v0, "missing a widget launch action"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_0
.end method
