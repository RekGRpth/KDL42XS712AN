.class public final Lcom/google/android/apps/youtube/app/ui/ad;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;)Lcom/google/android/apps/youtube/app/ui/v;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/v;-><init>(Landroid/app/Activity;)V

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Z)V

    invoke-static {}, Lcom/google/android/apps/youtube/app/ui/au;->a()Lcom/google/android/apps/youtube/app/ui/au;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/at;)Lcom/google/android/apps/youtube/app/ui/v;

    return-object v0
.end method

.method private static a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/at;)Lcom/google/android/apps/youtube/app/ui/v;
    .locals 11

    const/4 v10, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->Z()Lcom/google/android/apps/youtube/app/offline/p;

    move-result-object v7

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/youtube/app/offline/f;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v6

    new-instance v8, Lcom/google/android/apps/youtube/app/ui/bv;

    invoke-direct {v8, p0, v7}, Lcom/google/android/apps/youtube/app/ui/bv;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/offline/p;)V

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/app/offline/f;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/app/ui/bv;)V

    new-instance v8, Lcom/google/android/apps/youtube/app/ui/az;

    sget v1, Lcom/google/android/youtube/p;->cz:I

    invoke-direct {v8, p0, v1, v10}, Lcom/google/android/apps/youtube/app/ui/az;-><init>(Landroid/content/Context;IB)V

    new-instance v9, Lcom/google/android/apps/youtube/app/ui/az;

    sget v1, Lcom/google/android/youtube/p;->fa:I

    invoke-direct {v9, p0, v1, v10}, Lcom/google/android/apps/youtube/app/ui/az;-><init>(Landroid/content/Context;IB)V

    new-instance v4, Lcom/google/android/apps/youtube/app/ui/ao;

    move-object v5, p2

    move-object v6, v3

    move-object v7, v2

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/youtube/app/ui/ao;-><init>(Lcom/google/android/apps/youtube/app/ui/at;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/app/ui/az;Lcom/google/android/apps/youtube/app/ui/az;)V

    invoke-virtual {p1, v4}, Lcom/google/android/apps/youtube/app/ui/v;->a(Lcom/google/android/apps/youtube/app/ui/ac;)V

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/p;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/af;

    invoke-direct {v1, p2, p0, v0}, Lcom/google/android/apps/youtube/app/ui/af;-><init>(Lcom/google/android/apps/youtube/app/ui/at;Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/offline/f;)V

    invoke-virtual {p1, v8, v1}, Lcom/google/android/apps/youtube/app/ui/v;->a(Lcom/google/android/apps/youtube/app/ui/aa;Lcom/google/android/apps/youtube/app/ui/ab;)I

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/ag;

    invoke-direct {v1, p2, v0}, Lcom/google/android/apps/youtube/app/ui/ag;-><init>(Lcom/google/android/apps/youtube/app/ui/at;Lcom/google/android/apps/youtube/app/offline/f;)V

    invoke-virtual {p1, v9, v1}, Lcom/google/android/apps/youtube/app/ui/v;->a(Lcom/google/android/apps/youtube/app/ui/aa;Lcom/google/android/apps/youtube/app/ui/ab;)I

    :cond_0
    return-object p1
.end method

.method public static a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/v;-><init>(Landroid/app/Activity;)V

    invoke-static {}, Lcom/google/android/apps/youtube/app/ui/bb;->a()Lcom/google/android/apps/youtube/app/ui/bb;

    move-result-object v1

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/ba;)Lcom/google/android/apps/youtube/app/ui/v;

    return-object v0
.end method

.method private static a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/ba;)Lcom/google/android/apps/youtube/app/ui/v;
    .locals 24

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v15

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->Z()Lcom/google/android/apps/youtube/app/offline/p;

    move-result-object v9

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v4

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->q()Lcom/google/android/apps/youtube/core/offline/store/q;

    move-result-object v5

    new-instance v2, Lcom/google/android/apps/youtube/app/offline/r;

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v6

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v7

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v8

    new-instance v10, Lcom/google/android/apps/youtube/app/ui/bv;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v9}, Lcom/google/android/apps/youtube/app/ui/bv;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/app/offline/p;)V

    move-object/from16 v3, p0

    check-cast v3, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->J()Lcom/google/android/apps/youtube/app/ui/hh;

    move-result-object v11

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v11}, Lcom/google/android/apps/youtube/app/offline/r;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/offline/p;Lcom/google/android/apps/youtube/app/ui/bv;Lcom/google/android/apps/youtube/app/ui/hh;)V

    new-instance v18, Lcom/google/android/apps/youtube/app/ui/ap;

    const/4 v3, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/ui/ap;-><init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/offline/r;B)V

    new-instance v6, Lcom/google/android/apps/youtube/app/ui/hj;

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v8

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->x()Lcom/google/android/apps/youtube/datalib/innertube/v;

    move-result-object v9

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->y()Lcom/google/android/apps/youtube/datalib/innertube/al;

    move-result-object v10

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->aV()Lcom/google/android/apps/youtube/core/identity/ak;

    move-result-object v11

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->k()Lcom/google/android/apps/youtube/core/identity/o;

    move-result-object v12

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->bk()Lcom/google/android/apps/youtube/core/identity/aa;

    move-result-object v14

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v16

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v17

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->ai()Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v19

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->aI()Ljava/util/concurrent/Executor;

    move-result-object v20

    move-object/from16 v7, p0

    move-object v13, v4

    invoke-direct/range {v6 .. v20}, Lcom/google/android/apps/youtube/app/ui/hj;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/datalib/innertube/v;Lcom/google/android/apps/youtube/datalib/innertube/al;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/core/identity/o;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/app/ui/hp;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/concurrent/Executor;)V

    new-instance v21, Lcom/google/android/apps/youtube/app/ui/az;

    sget v3, Lcom/google/android/youtube/p;->cz:I

    const/4 v7, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3, v7}, Lcom/google/android/apps/youtube/app/ui/az;-><init>(Landroid/content/Context;IB)V

    new-instance v22, Lcom/google/android/apps/youtube/app/ui/az;

    sget v3, Lcom/google/android/youtube/p;->fa:I

    const/4 v7, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3, v7}, Lcom/google/android/apps/youtube/app/ui/az;-><init>(Landroid/content/Context;IB)V

    new-instance v23, Lcom/google/android/apps/youtube/app/ui/az;

    sget v3, Lcom/google/android/youtube/p;->eX:I

    const/4 v7, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3, v7}, Lcom/google/android/apps/youtube/app/ui/az;-><init>(Landroid/content/Context;IB)V

    new-instance v16, Lcom/google/android/apps/youtube/app/ui/ae;

    move-object/from16 v17, p3

    move-object/from16 v19, v5

    move-object/from16 v20, v4

    invoke-direct/range {v16 .. v23}, Lcom/google/android/apps/youtube/app/ui/ae;-><init>(Lcom/google/android/apps/youtube/app/ui/ba;Lcom/google/android/apps/youtube/app/ui/ap;Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/app/ui/az;Lcom/google/android/apps/youtube/app/ui/az;Lcom/google/android/apps/youtube/app/ui/az;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/v;->a(Lcom/google/android/apps/youtube/app/ui/ac;)V

    sget v3, Lcom/google/android/youtube/p;->cy:I

    new-instance v4, Lcom/google/android/apps/youtube/app/ui/ah;

    move-object/from16 v0, p3

    invoke-direct {v4, v6, v0}, Lcom/google/android/apps/youtube/app/ui/ah;-><init>(Lcom/google/android/apps/youtube/app/ui/hj;Lcom/google/android/apps/youtube/app/ui/ba;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/youtube/app/ui/v;->a(ILcom/google/android/apps/youtube/app/ui/ab;)I

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/dv;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/youtube/app/ui/dv;-><init>(Landroid/content/Context;Ljava/util/concurrent/atomic/AtomicReference;)V

    new-instance v4, Lcom/google/android/apps/youtube/app/ui/ai;

    move-object/from16 v0, p3

    invoke-direct {v4, v6, v0, v3}, Lcom/google/android/apps/youtube/app/ui/ai;-><init>(Lcom/google/android/apps/youtube/app/ui/hj;Lcom/google/android/apps/youtube/app/ui/ba;Lcom/google/android/apps/youtube/app/ui/dv;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/youtube/app/ui/v;->a(Lcom/google/android/apps/youtube/app/ui/aa;Lcom/google/android/apps/youtube/app/ui/ab;)I

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/youtube/core/utils/p;->a(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/aj;

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/youtube/app/ui/aj;-><init>(Lcom/google/android/apps/youtube/app/ui/ba;Lcom/google/android/apps/youtube/app/ui/ap;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/youtube/app/ui/v;->a(Lcom/google/android/apps/youtube/app/ui/aa;Lcom/google/android/apps/youtube/app/ui/ab;)I

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/ak;

    move-object/from16 v0, p3

    invoke-direct {v3, v0, v2}, Lcom/google/android/apps/youtube/app/ui/ak;-><init>(Lcom/google/android/apps/youtube/app/ui/ba;Lcom/google/android/apps/youtube/app/offline/r;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/youtube/app/ui/v;->a(Lcom/google/android/apps/youtube/app/ui/aa;Lcom/google/android/apps/youtube/app/ui/ab;)I

    :cond_0
    sget v2, Lcom/google/android/youtube/p;->cD:I

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/al;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v3, v15, v0, v1}, Lcom/google/android/apps/youtube/app/ui/al;-><init>(Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/ui/ba;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/youtube/app/ui/v;->a(ILcom/google/android/apps/youtube/app/ui/ab;)I

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/am;

    move-object/from16 v0, p3

    move-object/from16 v1, p2

    invoke-direct {v2, v6, v0, v1}, Lcom/google/android/apps/youtube/app/ui/am;-><init>(Lcom/google/android/apps/youtube/app/ui/hj;Lcom/google/android/apps/youtube/app/ui/ba;Lcom/google/android/apps/youtube/app/ui/v;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/v;->a(Lcom/google/android/apps/youtube/app/ui/aa;Lcom/google/android/apps/youtube/app/ui/ab;)I

    return-object p2
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/core/offline/store/q;Lcom/google/android/apps/youtube/core/identity/l;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;
    .locals 1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/apps/youtube/core/offline/store/q;->a()Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/core/identity/l;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/google/android/apps/youtube/core/offline/store/q;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/offline/store/OfflineStoreInterface;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/f;)V
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/youtube/core/utils/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, Lcom/google/android/apps/youtube/app/offline/f;->a(Ljava/lang/String;Lcom/google/android/apps/youtube/app/offline/j;)V

    :cond_0
    return-void
.end method

.method public static final a(Lcom/google/android/apps/youtube/app/ui/v;Landroid/view/View;Ljava/lang/Object;)V
    .locals 2

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/v;->a()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/v;->a(Landroid/view/View;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/ui/v;->c()Z

    move-result v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    if-eqz v0, :cond_0

    :goto_1
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v1, 0x8

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private static a(Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Z)V
    .locals 8

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->W()Lcom/google/android/apps/youtube/app/remote/an;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v6

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->b()Lcom/google/android/apps/youtube/app/aw;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/aw;->k()Z

    move-result v7

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/app/remote/ax;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/identity/l;Z)V

    sget v1, Lcom/google/android/youtube/p;->dL:I

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/an;

    invoke-direct {v2, v3, v5, p2, v0}, Lcom/google/android/apps/youtube/app/ui/an;-><init>(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/am;ZLcom/google/android/apps/youtube/app/remote/ax;)V

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/v;->a(ILcom/google/android/apps/youtube/app/ui/ab;)I

    return-void
.end method

.method public static b(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;)Lcom/google/android/apps/youtube/app/ui/v;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/v;-><init>(Landroid/app/Activity;)V

    const/4 v1, 0x1

    invoke-static {v0, p0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Z)V

    invoke-static {}, Lcom/google/android/apps/youtube/app/ui/au;->a()Lcom/google/android/apps/youtube/app/ui/au;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/at;)Lcom/google/android/apps/youtube/app/ui/v;

    return-object v0
.end method

.method public static b(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/v;-><init>(Landroid/app/Activity;)V

    invoke-static {}, Lcom/google/android/apps/youtube/app/ui/as;->a()Lcom/google/android/apps/youtube/app/ui/as;

    move-result-object v1

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/ba;)Lcom/google/android/apps/youtube/app/ui/v;

    return-object v0
.end method

.method public static c(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;)Lcom/google/android/apps/youtube/app/ui/v;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/v;-><init>(Landroid/app/Activity;)V

    invoke-static {}, Lcom/google/android/apps/youtube/app/ui/aq;->a()Lcom/google/android/apps/youtube/app/ui/aq;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/at;)Lcom/google/android/apps/youtube/app/ui/v;

    return-object v0
.end method

.method public static c(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/v;-><init>(Landroid/app/Activity;)V

    invoke-static {}, Lcom/google/android/apps/youtube/app/ui/ar;->a()Lcom/google/android/apps/youtube/app/ui/ar;

    move-result-object v1

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/ba;)Lcom/google/android/apps/youtube/app/ui/v;

    return-object v0
.end method

.method public static d(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;)Lcom/google/android/apps/youtube/app/ui/v;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/v;-><init>(Landroid/app/Activity;)V

    invoke-static {}, Lcom/google/android/apps/youtube/app/ui/av;->a()Lcom/google/android/apps/youtube/app/ui/av;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/at;)Lcom/google/android/apps/youtube/app/ui/v;

    return-object v0
.end method

.method public static d(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/v;-><init>(Landroid/app/Activity;)V

    invoke-static {}, Lcom/google/android/apps/youtube/app/ui/aw;->a()Lcom/google/android/apps/youtube/app/ui/aw;

    move-result-object v1

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/ba;)Lcom/google/android/apps/youtube/app/ui/v;

    return-object v0
.end method

.method public static e(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/v;-><init>(Landroid/app/Activity;)V

    invoke-static {}, Lcom/google/android/apps/youtube/app/ui/bc;->a()Lcom/google/android/apps/youtube/app/ui/bc;

    move-result-object v1

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/ba;)Lcom/google/android/apps/youtube/app/ui/v;

    return-object v0
.end method

.method public static f(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/v;-><init>(Landroid/app/Activity;)V

    invoke-static {}, Lcom/google/android/apps/youtube/app/ui/ay;->a()Lcom/google/android/apps/youtube/app/ui/ay;

    move-result-object v1

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/ba;)Lcom/google/android/apps/youtube/app/ui/v;

    return-object v0
.end method

.method public static g(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;)Lcom/google/android/apps/youtube/app/ui/v;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/ui/v;-><init>(Landroid/app/Activity;)V

    invoke-static {}, Lcom/google/android/apps/youtube/app/ui/ax;->a()Lcom/google/android/apps/youtube/app/ui/ax;

    move-result-object v1

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/apps/youtube/app/ui/ad;->a(Lcom/google/android/apps/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/util/concurrent/atomic/AtomicReference;Lcom/google/android/apps/youtube/app/ui/v;Lcom/google/android/apps/youtube/app/ui/ba;)Lcom/google/android/apps/youtube/app/ui/v;

    return-object v0
.end method
