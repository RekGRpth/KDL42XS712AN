.class final Lcom/google/android/apps/youtube/app/remote/bu;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/remote/bk;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/remote/bk;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/bu;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/remote/bk;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/bu;-><init>(Lcom/google/android/apps/youtube/app/remote/bk;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bu;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Recieved intent "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    const-string v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bu;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->OFFLINE:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bu;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->SLEEP:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bu;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->s(Lcom/google/android/apps/youtube/app/remote/bk;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bu;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->d()V

    goto :goto_0
.end method
