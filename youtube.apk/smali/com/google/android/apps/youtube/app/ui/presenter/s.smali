.class public final Lcom/google/android/apps/youtube/app/ui/presenter/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/g;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/ImageView;

.field private final c:Landroid/widget/EditText;

.field private final d:Landroid/app/Activity;

.field private final e:Lcom/google/android/apps/youtube/core/client/bj;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/d/o;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/s;->d:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/s;->e:Lcom/google/android/apps/youtube/core/client/bj;

    sget v0, Lcom/google/android/youtube/l;->S:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/s;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/s;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->A:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/s;->b:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/s;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->o:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/s;->c:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/s;->c:Landroid/widget/EditText;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/presenter/t;

    invoke-direct {v1, p0, p3}, Lcom/google/android/apps/youtube/app/ui/presenter/t;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/s;Lcom/google/android/apps/youtube/app/d/o;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 4

    check-cast p2, Lcom/google/android/apps/youtube/app/ui/presenter/v;

    iget-boolean v0, p2, Lcom/google/android/apps/youtube/app/ui/presenter/v;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/s;->c:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/s;->c:Landroid/widget/EditText;

    sget v1, Lcom/google/android/youtube/p;->ar:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    :goto_0
    iget-object v0, p2, Lcom/google/android/apps/youtube/app/ui/presenter/v;->b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/s;->b:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/youtube/h;->am:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/s;->a:Landroid/view/View;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/s;->c:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/s;->c:Landroid/widget/EditText;

    sget v1, Lcom/google/android/youtube/p;->av:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/s;->d:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/s;->e:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v2, p2, Lcom/google/android/apps/youtube/app/ui/presenter/v;->b:Lcom/google/android/apps/youtube/core/identity/UserProfile;

    iget-object v2, v2, Lcom/google/android/apps/youtube/core/identity/UserProfile;->thumbnailUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/ui/presenter/s;->b:Landroid/widget/ImageView;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/app/d/a;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Landroid/net/Uri;Landroid/widget/ImageView;)V

    goto :goto_1
.end method
