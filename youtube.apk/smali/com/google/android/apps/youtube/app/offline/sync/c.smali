.class final Lcom/google/android/apps/youtube/app/offline/sync/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/offline/sync/b;

.field private final b:Ljava/lang/String;

.field private final c:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/offline/sync/b;Lcom/google/a/a/a/a/ly;J)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/offline/sync/c;->a:Lcom/google/android/apps/youtube/app/offline/sync/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p2, Lcom/google/a/a/a/a/ly;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/c;->b:Ljava/lang/String;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v1, p2, Lcom/google/a/a/a/a/ly;->d:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    add-long/2addr v0, p3

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/c;->c:J

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/app/offline/sync/c;->c:J

    return-wide v0
.end method
