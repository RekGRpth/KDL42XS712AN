.class final Lcom/google/android/apps/youtube/app/ui/az;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/aa;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Z

.field private c:I


# direct methods
.method private constructor <init>(Landroid/content/Context;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/az;->a:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/az;->b:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/app/ui/az;->c:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;IB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/az;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/az;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/app/ui/az;->b:Z

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/ui/az;->b:Z

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/az;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/ui/az;->c:I

    return v0
.end method
