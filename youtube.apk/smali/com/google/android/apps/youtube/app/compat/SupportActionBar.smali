.class public final Lcom/google/android/apps/youtube/app/compat/SupportActionBar;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[I


# instance fields
.field private final b:Landroid/app/ActionBar;

.field private final c:Ljava/util/List;

.field private final d:Lcom/google/android/apps/youtube/app/compat/g;

.field private e:Lcom/google/android/apps/youtube/app/compat/d;

.field private final f:Landroid/app/Activity;

.field private g:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:Lcom/google/android/apps/youtube/app/compat/h;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101030b    # android.R.attr.homeAsUpIndicator

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 3

    const/16 v2, 0x8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->f:Landroid/app/Activity;

    sget-object v0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->NONE:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->g:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActionBar;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->c:Ljava/util/List;

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/g;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/app/compat/g;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->d:Lcom/google/android/apps/youtube/app/compat/g;

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b(Landroid/app/Activity;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/h;->u:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->i:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->i:Landroid/graphics/drawable/Drawable;

    new-instance v1, Lcom/google/android/apps/youtube/app/compat/h;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/app/compat/h;-><init>(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x3eaaaaab

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/compat/h;->b(F)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->j:Lcom/google/android/apps/youtube/app/compat/h;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, v2, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    return-void
.end method

.method public static a(Landroid/app/Activity;)Lcom/google/android/apps/youtube/app/compat/SupportActionBar;
    .locals 1

    instance-of v0, p0, Lcom/google/android/apps/youtube/app/compat/i;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/google/android/apps/youtube/app/compat/i;

    invoke-interface {p0}, Lcom/google/android/apps/youtube/app/compat/i;->K()Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;-><init>(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/drawable/Drawable;I)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->d:Lcom/google/android/apps/youtube/app/compat/g;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/compat/g;->a:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->d:Lcom/google/android/apps/youtube/app/compat/g;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/compat/g;->a:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->d:Lcom/google/android/apps/youtube/app/compat/g;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/compat/g;->b:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Couldn\'t set home-as-up indicator via JB-MR2 API"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->d:Lcom/google/android/apps/youtube/app/compat/g;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/compat/g;->c:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->d:Lcom/google/android/apps/youtube/app/compat/g;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/compat/g;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    const-string v0, "Couldn\'t set home-as-up indicator"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static b(Landroid/app/Activity;)Landroid/graphics/drawable/Drawable;
    .locals 2

    sget-object v0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a:[I

    invoke-virtual {p0, v0}, Landroid/app/Activity;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-object v1
.end method

.method private c(I)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->d:Lcom/google/android/apps/youtube/app/compat/g;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/compat/g;->a:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->d:Lcom/google/android/apps/youtube/app/compat/g;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/compat/g;->b:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Couldn\'t set content description via JB-MR2 API"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static f()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public static g()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->e:Lcom/google/android/apps/youtube/app/compat/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->e:Lcom/google/android/apps/youtube/app/compat/d;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/compat/d;->f(Z)V

    :cond_0
    return-void
.end method

.method public final a(F)V
    .locals 4

    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v2, 0x3f000000    # 0.5f

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->j:Lcom/google/android/apps/youtube/app/compat/h;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/compat/h;->a()F

    move-result v0

    cmpl-float v1, p1, v2

    if-lez v1, :cond_0

    const/4 v1, 0x0

    sub-float v2, p1, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    mul-float/2addr v1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->j:Lcom/google/android/apps/youtube/app/compat/h;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/app/compat/h;->a(F)V

    return-void

    :cond_0
    mul-float v1, p1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(I)V

    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;Z)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->g:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->g:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    sget-object v0, Lcom/google/android/apps/youtube/app/compat/c;->a:[I

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->g:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, p2}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, v3, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, v2, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->h:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Landroid/graphics/drawable/Drawable;I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, v2, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->j:Lcom/google/android/apps/youtube/app/compat/h;

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->a(Landroid/graphics/drawable/Drawable;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/d;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->e:Lcom/google/android/apps/youtube/app/compat/d;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/compat/e;)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/apps/youtube/app/compat/f;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/app/compat/f;-><init>(Lcom/google/android/apps/youtube/app/compat/e;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->e:Lcom/google/android/apps/youtube/app/compat/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->e:Lcom/google/android/apps/youtube/app/compat/d;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/app/compat/d;->f(Z)V

    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/app/compat/e;)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/compat/f;

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/compat/f;->a:Lcom/google/android/apps/youtube/app/compat/e;

    if-ne v2, p1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b:Landroid/app/ActionBar;

    invoke-virtual {v2, v0}, Landroid/app/ActionBar;->removeOnMenuVisibilityListener(Landroid/app/ActionBar$OnMenuVisibilityListener;)V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->f:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->b(Landroid/app/Activity;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->h:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/h;->u:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->i:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->j:Lcom/google/android/apps/youtube/app/compat/h;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/compat/h;->a(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->g:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    sget-object v1, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->DRAWER_TOGGLE:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    if-ne v0, v1, :cond_0

    sget v0, Lcom/google/android/youtube/p;->e:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->c(I)V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->j:Lcom/google/android/apps/youtube/app/compat/h;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/compat/h;->a(F)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->g:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    sget-object v1, Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;->DRAWER_TOGGLE:Lcom/google/android/apps/youtube/app/compat/SupportActionBar$HomeAction;

    if-ne v0, v1, :cond_0

    sget v0, Lcom/google/android/youtube/p;->f:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/compat/SupportActionBar;->c(I)V

    :cond_0
    return-void
.end method
