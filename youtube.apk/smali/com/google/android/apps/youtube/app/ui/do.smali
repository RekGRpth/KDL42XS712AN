.class public final Lcom/google/android/apps/youtube/app/ui/do;
.super Lcom/google/android/apps/youtube/core/ui/l;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/a/a;

.field private final b:Lcom/google/android/apps/youtube/app/ui/et;

.field private final g:Lcom/google/android/apps/youtube/app/ui/dq;

.field private h:Lcom/google/android/apps/youtube/app/ui/dr;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/am;)V
    .locals 7

    new-instance v6, Lcom/google/android/apps/youtube/app/ui/dp;

    invoke-direct {v6, p6}, Lcom/google/android/apps/youtube/app/ui/dp;-><init>(Lcom/google/android/apps/youtube/app/am;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/app/ui/do;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/ui/dq;)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/app/ui/dq;)V
    .locals 1

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/youtube/core/ui/l;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;)V

    const-string v0, "onPlaylistClickListener can\'t be null"

    invoke-static {p6, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/dq;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/do;->g:Lcom/google/android/apps/youtube/app/ui/dq;

    instance-of v0, p3, Lcom/google/android/apps/youtube/app/ui/et;

    if-eqz v0, :cond_0

    move-object v0, p3

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/et;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/youtube/app/ui/et;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    check-cast p3, Lcom/google/android/apps/youtube/app/ui/et;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/do;->b:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/do;->b:Lcom/google/android/apps/youtube/app/ui/et;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/et;->b()Lcom/google/android/apps/youtube/core/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/do;->a:Lcom/google/android/apps/youtube/core/a/a;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/do;->b:Lcom/google/android/apps/youtube/app/ui/et;

    iput-object p3, p0, Lcom/google/android/apps/youtube/app/ui/do;->a:Lcom/google/android/apps/youtube/core/a/a;

    invoke-interface {p2, p0}, Lcom/google/android/apps/youtube/core/ui/PagedView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V
    .locals 1

    instance-of v0, p2, Lcom/google/android/apps/youtube/core/async/GDataResponseException;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/youtube/core/async/GDataResponseException;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/async/GDataResponseException;->containsYouTubeSignupRequiredError()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->build()Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/youtube/app/ui/do;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Lcom/google/android/apps/youtube/datalib/legacy/model/Page;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/core/ui/l;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/youtube/core/async/GDataRequest;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/do;->a(Lcom/google/android/apps/youtube/core/async/GDataRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v1

    if-ge p3, v1, :cond_0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/do;->g:Lcom/google/android/apps/youtube/app/ui/dq;

    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/app/ui/dq;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/do;->a:Lcom/google/android/apps/youtube/core/a/a;

    goto :goto_0
.end method

.method public final onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    :goto_0
    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/do;->h:Lcom/google/android/apps/youtube/app/ui/dr;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/ui/dr;->a()Z

    move-result v0

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/do;->a:Lcom/google/android/apps/youtube/core/a/a;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
