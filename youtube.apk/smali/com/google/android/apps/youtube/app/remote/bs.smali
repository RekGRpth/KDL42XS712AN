.class final Lcom/google/android/apps/youtube/app/remote/bs;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/remote/bk;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/remote/bk;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/remote/bk;Landroid/os/Looper;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/remote/bs;-><init>(Lcom/google/android/apps/youtube/app/remote/bk;Landroid/os/Looper;)V

    return-void
.end method

.method private a(Landroid/content/Context;Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->d(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->d(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->d(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->a(Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->k(Lcom/google/android/apps/youtube/app/remote/bk;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->l(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/app/remote/bo;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->b(Lcom/google/android/apps/youtube/app/remote/bk;Z)Z

    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/bs;Lcom/google/android/apps/ytremote/model/CloudScreen;Lcom/google/android/apps/youtube/app/remote/bp;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/remote/bs;->a(Lcom/google/android/apps/ytremote/model/CloudScreen;Lcom/google/android/apps/youtube/app/remote/bp;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/remote/bs;Lcom/google/android/apps/ytremote/model/YouTubeDevice;Lcom/google/android/apps/ytremote/model/CloudScreen;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->o(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->p(Lcom/google/android/apps/youtube/app/remote/bk;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/SsdpId;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/ytremote/model/ScreenId;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private a(Lcom/google/android/apps/ytremote/model/CloudScreen;Lcom/google/android/apps/youtube/app/remote/bp;)V
    .locals 8

    const-wide/16 v6, 0x3e8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->c(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->m(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/ytremote/model/CloudScreen;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->k(Lcom/google/android/apps/youtube/app/remote/bk;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->b(Lcom/google/android/apps/youtube/app/remote/bk;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->l(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/app/remote/bo;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/youtube/app/remote/bk;->z()Landroid/content/IntentFilter;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_1
    new-instance v0, Lcom/google/android/apps/ytremote/backend/model/b;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/backend/model/b;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getLoungeToken()Lcom/google/android/apps/ytremote/model/LoungeToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/ytremote/backend/model/b;->a(Lcom/google/android/apps/ytremote/model/LoungeToken;)Lcom/google/android/apps/ytremote/backend/model/b;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/apps/youtube/app/remote/bp;->b(Lcom/google/android/apps/youtube/app/remote/bp;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/ytremote/backend/model/Method;->SET_PLAYLIST:Lcom/google/android/apps/ytremote/backend/model/Method;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/ytremote/backend/model/b;->a(Lcom/google/android/apps/ytremote/backend/model/Method;)Lcom/google/android/apps/ytremote/backend/model/b;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->n(Lcom/google/android/apps/youtube/app/remote/bk;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/backend/model/Params;-><init>()V

    const-string v3, "videoId"

    invoke-static {p2}, Lcom/google/android/apps/youtube/app/remote/bp;->b(Lcom/google/android/apps/youtube/app/remote/bp;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v3, "currentIndex"

    invoke-static {p2}, Lcom/google/android/apps/youtube/app/remote/bp;->f(Lcom/google/android/apps/youtube/app/remote/bp;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v3, "listId"

    invoke-static {p2}, Lcom/google/android/apps/youtube/app/remote/bp;->g(Lcom/google/android/apps/youtube/app/remote/bp;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v3, "currentTime"

    invoke-static {p2}, Lcom/google/android/apps/youtube/app/remote/bp;->e(Lcom/google/android/apps/youtube/app/remote/bp;)J

    move-result-wide v4

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/ytremote/backend/model/b;->a(Lcom/google/android/apps/ytremote/backend/model/Params;)Lcom/google/android/apps/ytremote/backend/model/b;

    :cond_2
    invoke-static {p2}, Lcom/google/android/apps/youtube/app/remote/bp;->c(Lcom/google/android/apps/youtube/app/remote/bp;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/ytremote/backend/model/b;->a(Z)V

    :cond_3
    invoke-static {p2}, Lcom/google/android/apps/youtube/app/remote/bp;->d(Lcom/google/android/apps/youtube/app/remote/bp;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Bearer "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/app/remote/bp;->d(Lcom/google/android/apps/youtube/app/remote/bp;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/ytremote/backend/model/b;->a(Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/b;

    :cond_4
    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/backend/model/b;->a()Lcom/google/android/apps/ytremote/backend/model/a;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Connecting to "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " with "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/backend/model/a;->f()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/backend/model/a;->b()Lcom/google/android/apps/ytremote/backend/model/Method;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/backend/model/a;->g()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/backend/model/a;->c()Lcom/google/android/apps/ytremote/backend/model/Params;

    move-result-object v0

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->d(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->a(Lcom/google/android/apps/ytremote/backend/model/a;)Ljava/util/concurrent/CountDownLatch;

    return-void

    :cond_5
    new-instance v0, Lcom/google/android/apps/ytremote/backend/model/Params;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/backend/model/Params;-><init>()V

    const-string v3, "videoId"

    invoke-static {p2}, Lcom/google/android/apps/youtube/app/remote/bp;->b(Lcom/google/android/apps/youtube/app/remote/bp;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v3, "currentTime"

    invoke-static {p2}, Lcom/google/android/apps/youtube/app/remote/bp;->e(Lcom/google/android/apps/youtube/app/remote/bp;)J

    move-result-wide v4

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v3, "videoSources"

    const-string v4, "XX"

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    const-string v3, "videoIds"

    invoke-static {p2}, Lcom/google/android/apps/youtube/app/remote/bp;->b(Lcom/google/android/apps/youtube/app/remote/bp;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/ytremote/backend/model/Params;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/ytremote/backend/model/Params;

    goto/16 :goto_0

    :cond_6
    const-string v0, "{}"

    goto :goto_1

    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "no message."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 10

    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    const-string v1, "Remote going to sleep ..."

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->b(Lcom/google/android/apps/youtube/app/remote/bk;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/youtube/app/remote/bs;->a(Landroid/content/Context;Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->c(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    const-string v1, "We should reconnect, but we lost the cloud screen."

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    const-string v1, "Remote waking up ..."

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->d(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/ytremote/backend/model/b;

    invoke-direct {v1}, Lcom/google/android/apps/ytremote/backend/model/b;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v2}, Lcom/google/android/apps/youtube/app/remote/bk;->c(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getLoungeToken()Lcom/google/android/apps/ytremote/model/LoungeToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/ytremote/backend/model/b;->a(Lcom/google/android/apps/ytremote/model/LoungeToken;)Lcom/google/android/apps/ytremote/backend/model/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/backend/model/b;->a()Lcom/google/android/apps/ytremote/backend/model/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->a(Lcom/google/android/apps/ytremote/backend/model/a;)Ljava/util/concurrent/CountDownLatch;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->d(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->a()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->d(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    const-string v3, "Connecting to a new screen. Will disconnect the previous browser channel connection."

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->d(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/backend/browserchannel/k;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/ytremote/backend/browserchannel/k;->a(Z)V

    :cond_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v7, v0

    check-cast v7, Lcom/google/android/apps/youtube/app/remote/bp;

    invoke-static {v7}, Lcom/google/android/apps/youtube/app/remote/bp;->a(Lcom/google/android/apps/youtube/app/remote/bp;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v0, "Connecting to "

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/bp;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bp;->a(Lcom/google/android/apps/youtube/app/remote/bp;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreenName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasDevice()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v9

    if-eqz v9, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Found associated cloud screen "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for device "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->j(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/logic/a;

    move-result-object v0

    invoke-virtual {v8}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getAppUri()Landroid/net/Uri;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/google/android/apps/ytremote/logic/a;->a(Landroid/net/Uri;)Lcom/google/android/apps/ytremote/model/AppStatus;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Application status for device is "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/AppStatus;->getStatus()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/AppStatus;->getStatus()I

    move-result v0

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->q(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/backend/logic/d;

    move-result-object v0

    new-array v3, v1, [Lcom/google/android/apps/ytremote/model/CloudScreen;

    aput-object v9, v3, v2

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/google/android/apps/ytremote/backend/logic/d;->a(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {v0, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    const-string v1, "Screen appears to be online. Will not send a launch request."

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    invoke-direct {p0, v9, v7}, Lcom/google/android/apps/youtube/app/remote/bs;->a(Lcom/google/android/apps/ytremote/model/CloudScreen;Lcom/google/android/apps/youtube/app/remote/bp;)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->c(Lcom/google/android/apps/youtube/app/remote/bk;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sending launch request for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v8}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getAppUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->e(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/logic/d;

    move-result-object v0

    invoke-virtual {v8}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getAppUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v7}, Lcom/google/android/apps/youtube/app/remote/bp;->b(Lcom/google/android/apps/youtube/app/remote/bp;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v7}, Lcom/google/android/apps/youtube/app/remote/bp;->e(Lcom/google/android/apps/youtube/app/remote/bp;)J

    move-result-wide v3

    new-instance v6, Lcom/google/android/apps/youtube/app/remote/bt;

    invoke-direct {v6, p0, v8, v7}, Lcom/google/android/apps/youtube/app/remote/bt;-><init>(Lcom/google/android/apps/youtube/app/remote/bs;Lcom/google/android/apps/ytremote/model/YouTubeDevice;Lcom/google/android/apps/youtube/app/remote/bp;)V

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/ytremote/logic/d;->a(Landroid/net/Uri;Ljava/lang/String;JLcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/ytremote/logic/e;)V

    if-eqz v9, :cond_0

    invoke-direct {p0, v9, v7}, Lcom/google/android/apps/youtube/app/remote/bs;->a(Lcom/google/android/apps/ytremote/model/CloudScreen;Lcom/google/android/apps/youtube/app/remote/bp;)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreen()Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v0

    invoke-direct {p0, v0, v7}, Lcom/google/android/apps/youtube/app/remote/bs;->a(Lcom/google/android/apps/ytremote/model/CloudScreen;Lcom/google/android/apps/youtube/app/remote/bp;)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/bq;

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/remote/bq;->a:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Disconnecting from "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getScreenName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    iget-boolean v3, v0, Lcom/google/android/apps/youtube/app/remote/bq;->b:Z

    if-eqz v3, :cond_7

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasDevice()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->i(Lcom/google/android/apps/youtube/app/remote/bk;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getAppStatus()Lcom/google/android/apps/ytremote/model/AppStatus;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getAppStatus()Lcom/google/android/apps/ytremote/model/AppStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/AppStatus;->isStopAllowed()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->f(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->f(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Sending stop request to "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Ljava/lang/String;)V

    if-eqz v0, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/remote/bk;->e(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/logic/d;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/ytremote/logic/d;->a(Landroid/net/Uri;)V

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->e(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/logic/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/ytremote/logic/d;->a()V

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->hasDevice()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->f(Lcom/google/android/apps/youtube/app/remote/bk;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->b(Lcom/google/android/apps/youtube/app/remote/bk;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/youtube/app/remote/bs;->a(Landroid/content/Context;Z)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;->getDevice()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getAppUri()Landroid/net/Uri;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/remote/bk;->j(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/ytremote/logic/a;

    move-result-object v4

    invoke-interface {v4, v0}, Lcom/google/android/apps/ytremote/logic/a;->a(Landroid/net/Uri;)Lcom/google/android/apps/ytremote/model/AppStatus;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-virtual {v4}, Lcom/google/android/apps/ytremote/model/AppStatus;->getStatus()I

    move-result v6

    if-ne v6, v1, :cond_a

    invoke-virtual {v4}, Lcom/google/android/apps/ytremote/model/AppStatus;->getRunningPathSegment()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_2

    :cond_a
    move-object v0, v5

    goto :goto_2

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->b()Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;->CONNECTED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->g(Lcom/google/android/apps/youtube/app/remote/bk;)Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;->UNCONFIRMED:Lcom/google/android/apps/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/remote/bk;->h(Lcom/google/android/apps/youtube/app/remote/bk;)V

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/bs;->a:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Lcom/google/android/apps/youtube/app/remote/bk;Z)Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
