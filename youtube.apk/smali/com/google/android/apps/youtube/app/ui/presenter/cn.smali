.class public final Lcom/google/android/apps/youtube/app/ui/presenter/cn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/uilib/a/g;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

.field private final b:Lcom/google/android/apps/youtube/uilib/a/i;

.field private final c:Landroid/view/View$OnClickListener;

.field private d:Lcom/google/a/a/a/a/kz;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/uilib/a/i;Lcom/google/android/apps/youtube/datalib/d/a;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/uilib/a/i;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cn;->b:Lcom/google/android/apps/youtube/uilib/a/i;

    sget v0, Lcom/google/android/youtube/l;->bp:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cn;->a:Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/co;

    invoke-direct {v0, p0, p3}, Lcom/google/android/apps/youtube/app/ui/presenter/co;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/cn;Lcom/google/android/apps/youtube/datalib/d/a;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cn;->c:Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cn;->a:Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/presenter/cn;)Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cn;->d:Lcom/google/a/a/a/a/kz;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 2

    check-cast p2, Lcom/google/android/apps/youtube/uilib/innertube/u;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/u;->c()Lcom/google/a/a/a/a/kz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cn;->d:Lcom/google/a/a/a/a/kz;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cn;->b:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/u;->b()Landroid/view/View$OnClickListener;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cn;->c:Landroid/view/View$OnClickListener;

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/u;->a()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cn;->a:Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/u;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cn;->b:Lcom/google/android/apps/youtube/uilib/a/i;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/uilib/a/i;->a(Lcom/google/android/apps/youtube/uilib/a/f;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/uilib/innertube/u;->b()Landroid/view/View$OnClickListener;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/cn;->a:Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;

    sget v1, Lcom/google/android/youtube/p;->cv:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/YouTubeTextView;->setText(I)V

    goto :goto_1
.end method
