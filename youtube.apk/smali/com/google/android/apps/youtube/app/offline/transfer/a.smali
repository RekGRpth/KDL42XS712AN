.class final Lcom/google/android/apps/youtube/app/offline/transfer/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/offline/transfer/d;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;

.field private b:J

.field private c:J

.field private d:I

.field private e:J


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/a;->a:Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/a;->d:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/offline/transfer/a;-><init>(Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/offline/transfer/a;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/a;->c:J

    return-wide p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/offline/transfer/a;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/youtube/app/offline/transfer/a;->b:J

    return-wide p1
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;J)V
    .locals 8

    iget-wide v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/a;->b:J

    add-long/2addr v0, p2

    long-to-double v2, v0

    iget-wide v4, p0, Lcom/google/android/apps/youtube/app/offline/transfer/a;->c:J

    long-to-double v4, v4

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    div-double/2addr v4, v6

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    iget v3, p0, Lcom/google/android/apps/youtube/app/offline/transfer/a;->d:I

    if-ltz v3, :cond_0

    iget v3, p0, Lcom/google/android/apps/youtube/app/offline/transfer/a;->d:I

    sub-int v3, v2, v3

    if-gtz v3, :cond_0

    iget-wide v3, p0, Lcom/google/android/apps/youtube/app/offline/transfer/a;->e:J

    sub-long v3, v0, v3

    const-wide/32 v5, 0x400000

    cmp-long v3, v3, v5

    if-gez v3, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getContentLength()J

    move-result-wide v3

    cmp-long v3, p2, v3

    if-nez v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/transfer/a;->a:Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;

    iget-object v3, v3, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a:Lcom/google/android/apps/youtube/core/offline/store/i;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/offline/transfer/a;->a:Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;

    iget-object v4, v4, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->g:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v5

    invoke-virtual {v3, v4, v5, p2, p3}, Lcom/google/android/apps/youtube/core/offline/store/i;->a(Ljava/lang/String;IJ)Z

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/offline/transfer/a;->a:Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;

    iget-wide v4, p0, Lcom/google/android/apps/youtube/app/offline/transfer/a;->b:J

    add-long/2addr v4, p2

    iget-wide v6, p0, Lcom/google/android/apps/youtube/app/offline/transfer/a;->c:J

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/google/android/apps/youtube/app/offline/transfer/AbstractOfflineTransferTask;->a(JJ)V

    iput v2, p0, Lcom/google/android/apps/youtube/app/offline/transfer/a;->d:I

    iput-wide v0, p0, Lcom/google/android/apps/youtube/app/offline/transfer/a;->e:J

    :cond_1
    return-void
.end method
