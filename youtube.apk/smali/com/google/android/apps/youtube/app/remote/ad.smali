.class public final Lcom/google/android/apps/youtube/app/remote/ad;
.super Lcom/google/android/apps/youtube/app/remote/q;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Map;

.field private final b:Ljava/util/concurrent/Executor;

.field private final c:Lcom/google/android/apps/youtube/app/remote/bk;

.field private final d:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

.field private final e:Landroid/content/Context;

.field private f:Lcom/google/android/apps/ytremote/backend/logic/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/bk;Ljava/util/concurrent/Executor;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/youtube/app/remote/q;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Z)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ad;->e:Landroid/content/Context;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/bk;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ad;->c:Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ad;->b:Ljava/util/concurrent/Executor;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ad;->d:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ad;->a:Ljava/util/Map;

    return-void
.end method

.method private c(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/PairingCode;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ad;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ad;->a:Ljava/util/Map;

    new-instance v1, Lcom/google/android/apps/ytremote/model/PairingCode;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/ytremote/model/PairingCode;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ad;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/PairingCode;

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)Landroid/support/v7/media/c;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "pairingCode"

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/remote/ad;->c(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/app/remote/ad;->c(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/PairingCode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/ytremote/model/PairingCode;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/remote/ad;->d(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)Landroid/support/v7/media/d;

    move-result-object v1

    const-string v2, "YouTube"

    invoke-virtual {v1, v2}, Landroid/support/v7/media/d;->a(Ljava/lang/String;)Landroid/support/v7/media/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v7/media/d;->a(Landroid/os/Bundle;)Landroid/support/v7/media/d;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/media/d;->a()Landroid/support/v7/media/c;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Landroid/support/v7/media/j;
    .locals 8

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/ae;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/remote/ad;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/remote/ad;->c(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/PairingCode;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/remote/ad;->e:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/remote/ad;->c:Lcom/google/android/apps/youtube/app/remote/bk;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/remote/ad;->d:Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/remote/ad;->b:Ljava/util/concurrent/Executor;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/remote/ad;->f:Lcom/google/android/apps/ytremote/backend/logic/c;

    if-nez v7, :cond_0

    new-instance v7, Lcom/google/android/apps/ytremote/backend/a/i;

    invoke-direct {v7}, Lcom/google/android/apps/ytremote/backend/a/i;-><init>()V

    iput-object v7, p0, Lcom/google/android/apps/youtube/app/remote/ad;->f:Lcom/google/android/apps/ytremote/backend/logic/c;

    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/youtube/app/remote/ad;->f:Lcom/google/android/apps/ytremote/backend/logic/c;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/app/remote/ae;-><init>(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;Lcom/google/android/apps/ytremote/model/PairingCode;Landroid/content/Context;Lcom/google/android/apps/youtube/app/remote/bk;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Ljava/util/concurrent/Executor;Lcom/google/android/apps/ytremote/backend/logic/c;)V

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/apps/youtube/app/remote/bg;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/app/remote/ad;->b(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    return-void
.end method

.method public final b(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/remote/ad;->a:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/remote/ad;->c(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/remote/q;->b(Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V

    return-void
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    const-string v0, "EXTERNAL_MDX_MEDIA_ROUTE_CONTROL_CATEGORY"

    return-object v0
.end method
