.class final Lcom/google/android/apps/youtube/app/fragments/navigation/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Lcom/google/android/apps/youtube/a/a/i;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/i;-><init>()V

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/common/e/j;->b(Landroid/os/Parcel;Lcom/google/protobuf/micro/c;)Lcom/google/protobuf/micro/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/i;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;-><init>(Lcom/google/android/apps/youtube/a/a/i;Lcom/google/android/apps/youtube/app/fragments/navigation/g;)V
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v2

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/app/fragments/navigation/g;->a(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/apps/youtube/app/fragments/navigation/WatchDescriptor;

    return-object v0
.end method
