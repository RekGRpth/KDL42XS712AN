.class public final Lcom/google/android/apps/youtube/app/adapter/al;
.super Lcom/google/android/apps/youtube/app/adapter/bq;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/client/bc;

.field private final b:Lcom/google/android/apps/youtube/core/client/bj;

.field private final c:Ljava/util/Map;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;Lcom/google/android/apps/youtube/core/client/bc;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/youtube/app/adapter/bq;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/al;->a:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/bj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/al;->b:Lcom/google/android/apps/youtube/core/client/bj;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/al;->c:Ljava/util/Map;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/core/client/bc;)Lcom/google/android/apps/youtube/app/adapter/al;
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/al;

    const/4 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/app/adapter/al;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;Lcom/google/android/apps/youtube/core/client/bc;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/al;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/al;->c:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/adapter/al;)Lcom/google/android/apps/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/al;->a:Lcom/google/android/apps/youtube/core/client/bc;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/adapter/al;)Lcom/google/android/apps/youtube/core/client/bj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/al;->b:Lcom/google/android/apps/youtube/core/client/bj;

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Landroid/view/View;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/al;->c:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->mqThumbnailUri:Landroid/net/Uri;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->mqThumbnailUri:Landroid/net/Uri;

    :cond_0
    :goto_0
    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-eq v1, v2, :cond_3

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PROCESSING:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-ne v1, v2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isLive()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    const/4 v1, 0x0

    invoke-interface {p3, v0, v1}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_1
    return-void

    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->defaultThumbnailUri:Landroid/net/Uri;

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/al;->b:Lcom/google/android/apps/youtube/core/client/bj;

    new-instance v2, Lcom/google/android/apps/youtube/app/adapter/am;

    invoke-direct {v2, p0, p1, p3}, Lcom/google/android/apps/youtube/app/adapter/am;-><init>(Lcom/google/android/apps/youtube/app/adapter/al;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/youtube/core/client/bj;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/al;->a:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->ownerUri:Landroid/net/Uri;

    new-instance v2, Lcom/google/android/apps/youtube/app/adapter/an;

    invoke-direct {v2, p0, p1, p3}, Lcom/google/android/apps/youtube/app/adapter/an;-><init>(Lcom/google/android/apps/youtube/app/adapter/al;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/core/client/bc;->a(Landroid/net/Uri;Lcom/google/android/apps/youtube/common/a/b;)V

    goto :goto_1
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    check-cast p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/adapter/al;->a(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Landroid/view/View;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
