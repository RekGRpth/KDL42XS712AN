.class final Lcom/google/android/apps/youtube/app/adapter/az;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/adapter/ae;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/adapter/ay;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/ProgressBar;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/TextView;

.field private final i:Landroid/view/View;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/app/adapter/ay;Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/adapter/az;->a:Lcom/google/android/apps/youtube/app/adapter/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/adapter/az;->b:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->c:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->d:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/youtube/j;->y:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->e:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->dU:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->f:Landroid/widget/ProgressBar;

    sget v0, Lcom/google/android/youtube/j;->eK:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->g:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->dT:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->h:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/j;->aE:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->i:Landroid/view/View;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/app/adapter/ay;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/adapter/az;-><init>(Lcom/google/android/apps/youtube/app/adapter/ay;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 4

    check-cast p2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->a:Lcom/google/android/apps/youtube/app/adapter/ay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ay;->a(Lcom/google/android/apps/youtube/app/adapter/ay;)Ljava/util/WeakHashMap;

    move-result-object v0

    invoke-virtual {v0, p2, p0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->a:Lcom/google/android/apps/youtube/app/adapter/ay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ay;->b(Lcom/google/android/apps/youtube/app/adapter/ay;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/az;->i:Landroid/view/View;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/apps/youtube/app/ui/v;->a(Landroid/view/View;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v1, "username"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->e:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v2, "username"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->c:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v2, "upload_title"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->g:Lcom/google/android/apps/youtube/datalib/model/transfer/a;

    const-string v1, "upload_file_thumbnail"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/transfer/a;->c(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/az;->d:Landroid/widget/ImageView;

    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {v0, v2, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->RUNNING:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/az;->a:Lcom/google/android/apps/youtube/app/adapter/ay;

    invoke-static {v1}, Lcom/google/android/apps/youtube/app/adapter/ay;->c(Lcom/google/android/apps/youtube/app/adapter/ay;)Landroid/content/Context;

    move-result-object v1

    iget-wide v2, p2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->f:J

    invoke-static {v1, v2, v3}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_0
    invoke-virtual {p0, p2}, Lcom/google/android/apps/youtube/app/adapter/az;->a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->b:Landroid/view/View;

    return-object v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method final a(Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/high16 v2, 0x42c80000    # 100.0f

    iget-wide v3, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->e:J

    long-to-float v3, v3

    mul-float/2addr v2, v3

    iget-wide v3, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->f:J

    long-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->ceil(F)F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/az;->f:Landroid/widget/ProgressBar;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v4, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->FAILED:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/az;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/az;->h:Landroid/widget/TextView;

    if-eqz v3, :cond_6

    iget-object v3, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v4, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->RUNNING:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-ne v3, v4, :cond_4

    iget-wide v3, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->f:J

    iget-wide v5, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->e:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/az;->h:Landroid/widget/TextView;

    sget v2, Lcom/google/android/youtube/p;->gu:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/az;->i:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/adapter/az;->i:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->a:Lcom/google/android/apps/youtube/app/adapter/ay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/adapter/ay;->b(Lcom/google/android/apps/youtube/app/adapter/ay;)Lcom/google/android/apps/youtube/app/ui/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/v;->b()V

    :cond_1
    return-void

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/az;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/youtube/app/adapter/az;->h:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/adapter/az;->a:Lcom/google/android/apps/youtube/app/adapter/ay;

    invoke-static {v4}, Lcom/google/android/apps/youtube/app/adapter/ay;->c(Lcom/google/android/apps/youtube/app/adapter/ay;)Landroid/content/Context;

    move-result-object v4

    sget v5, Lcom/google/android/youtube/p;->dK:I

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v0, v1

    goto :goto_1

    :cond_4
    iget-object v0, p1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer;->c:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->FAILED:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->h:Landroid/widget/TextView;

    sget v2, Lcom/google/android/youtube/p;->gr:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    move v0, v1

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/az;->h:Landroid/widget/TextView;

    sget v2, Lcom/google/android/youtube/p;->gt:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_6
    move v0, v1

    goto :goto_1
.end method
