.class final Lcom/google/android/apps/youtube/app/compat/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/Menu;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/app/compat/j;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/compat/j;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/compat/j;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    return-void
.end method


# virtual methods
.method public final add(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/compat/j;->a(I)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/compat/q;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIII)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/app/compat/j;->a(IIII)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/compat/q;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/app/compat/j;->a(IIILjava/lang/CharSequence;)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/compat/q;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/compat/j;->a(Ljava/lang/CharSequence;)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/compat/q;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 9

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/youtube/app/compat/j;->a(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I

    move-result v0

    return v0
.end method

.method public final addSubMenu(I)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/compat/j;->b(I)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/app/compat/j;->b(IIII)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/app/compat/j;->b(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/compat/j;->b(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final clear()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/compat/j;->a()V

    return-void
.end method

.method public final close()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/compat/j;->b()V

    return-void
.end method

.method public final findItem(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/compat/j;->c(I)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/compat/q;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final getItem(I)Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/compat/j;->d(I)Lcom/google/android/apps/youtube/app/compat/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/compat/q;->a()Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final hasVisibleItems()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/compat/j;->c()Z

    move-result v0

    return v0
.end method

.method public final isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/compat/j;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public final performIdentifierAction(II)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/compat/j;->a(II)Z

    move-result v0

    return v0
.end method

.method public final performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/compat/j;->a(ILandroid/view/KeyEvent;I)Z

    move-result v0

    return v0
.end method

.method public final removeGroup(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/compat/j;->e(I)V

    return-void
.end method

.method public final removeItem(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/compat/j;->f(I)V

    return-void
.end method

.method public final setGroupCheckable(IZZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/app/compat/j;->a(IZZ)V

    return-void
.end method

.method public final setGroupEnabled(IZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/compat/j;->a(IZ)V

    return-void
.end method

.method public final setGroupVisible(IZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/youtube/app/compat/j;->b(IZ)V

    return-void
.end method

.method public final setQwertyMode(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/compat/j;->a(Z)V

    return-void
.end method

.method public final size()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/compat/p;->a:Lcom/google/android/apps/youtube/app/compat/j;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/compat/j;->d()I

    move-result v0

    return v0
.end method
