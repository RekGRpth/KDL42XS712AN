.class public final Lcom/google/android/apps/youtube/app/ui/presenter/a/b;
.super Lcom/google/android/apps/youtube/uilib/a/a;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Lcom/google/android/apps/youtube/app/ui/gr;

.field private final e:Lcom/google/android/apps/youtube/app/d/e;

.field private final f:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/d/a;Landroid/app/Activity;Lcom/google/android/apps/youtube/core/client/bj;Landroid/view/ViewGroup;)V
    .locals 4

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/uilib/a/a;-><init>(Lcom/google/android/apps/youtube/datalib/d/a;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/l;->ab:I

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fF:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->ff:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/j;->fy:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-direct {v1, p3, v0}, Lcom/google/android/apps/youtube/app/ui/gr;-><init>(Lcom/google/android/apps/youtube/core/client/bj;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->d:Lcom/google/android/apps/youtube/app/ui/gr;

    new-instance v0, Lcom/google/android/apps/youtube/app/ui/presenter/a/c;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/youtube/app/ui/presenter/a/c;-><init>(Lcom/google/android/apps/youtube/app/ui/presenter/a/b;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->e:Lcom/google/android/apps/youtube/app/d/e;

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p2}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Lcom/google/android/youtube/d;->a:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->f:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;)Landroid/view/View;
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/uilib/a/a;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/u;)Landroid/view/View;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->d:Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->d()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->e:Lcom/google/android/apps/youtube/app/d/e;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/gr;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;Lcom/google/android/apps/youtube/app/d/e;)V

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->d()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->d:Lcom/google/android/apps/youtube/app/ui/gr;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/gr;->a()Landroid/widget/ImageView;

    move-result-object v0

    sget v1, Lcom/google/android/youtube/f;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->a:Landroid/view/View;

    sget v1, Lcom/google/android/youtube/f;->f:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->a:Landroid/view/View;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/presenter/a/b;)Lcom/google/android/apps/youtube/app/ui/gr;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->d:Lcom/google/android/apps/youtube/app/ui/gr;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/u;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/android/apps/youtube/uilib/a/f;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/app/ui/presenter/a/b;->a(Lcom/google/android/apps/youtube/uilib/a/f;Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
