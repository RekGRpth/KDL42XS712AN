.class public final Lcom/google/android/apps/youtube/app/remote/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/apps/youtube/app/remote/bw;Landroid/os/Bundle;)V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/b;

    invoke-direct {v0, p1, p0}, Lcom/google/android/apps/youtube/app/remote/b;-><init>(Lcom/google/android/apps/youtube/app/remote/bw;Landroid/content/Context;)V

    invoke-static {p2, p1, v0}, Lcom/google/android/apps/youtube/app/remote/a;->a(Landroid/os/Bundle;Lcom/google/android/apps/youtube/app/remote/bw;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method

.method static synthetic a(Landroid/content/Context;Lcom/google/android/apps/ytremote/model/CloudScreen;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;)V
    .locals 9

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    sget v4, Lcom/google/android/youtube/h;->M:I

    if-eqz v1, :cond_2

    sget v2, Lcom/google/android/youtube/p;->cY:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    :goto_1
    if-eqz v1, :cond_3

    sget v2, Lcom/google/android/youtube/p;->cY:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_2
    sget v5, Lcom/google/android/youtube/p;->cX:I

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v6

    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/google/android/apps/youtube/app/honeycomb/phone/AutomaticPairingActivity;

    invoke-direct {v7, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v8, "cloud_screen"

    invoke-virtual {v7, v8, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    if-eqz v1, :cond_0

    const-string v1, "yt_tv_screen"

    invoke-virtual {v7, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {v6}, Lcom/google/android/apps/ytremote/model/ScreenId;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v6}, Lcom/google/android/apps/ytremote/model/ScreenId;->hashCode()I

    move-result v1

    const/high16 v8, 0x8000000

    invoke-static {p0, v1, v7, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    new-instance v8, Landroid/support/v4/app/al;

    invoke-direct {v8, p0}, Landroid/support/v4/app/al;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8, v4}, Landroid/support/v4/app/al;->a(I)Landroid/support/v4/app/al;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/support/v4/app/al;->d(Ljava/lang/CharSequence;)Landroid/support/v4/app/al;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/support/v4/app/al;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/al;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/support/v4/app/al;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/al;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/support/v4/app/al;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/al;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/al;->a()Landroid/app/Notification;

    move-result-object v2

    const/16 v3, 0x10

    iput v3, v2, Landroid/app/Notification;->flags:I

    invoke-virtual {v6}, Lcom/google/android/apps/ytremote/model/ScreenId;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v1, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    :cond_2
    sget v2, Lcom/google/android/youtube/p;->cZ:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    goto :goto_1

    :cond_3
    sget v2, Lcom/google/android/youtube/p;->cZ:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/ytremote/model/ScreenId;)V
    .locals 3

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/ScreenId;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/ScreenId;->hashCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    return-void
.end method

.method private static a(Landroid/os/Bundle;Lcom/google/android/apps/youtube/app/remote/bw;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    const-string v0, "tv_name"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "pairing_code"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Lcom/google/android/apps/ytremote/model/PairingCode;

    invoke-direct {v1, v0}, Lcom/google/android/apps/ytremote/model/PairingCode;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v1, p2}, Lcom/google/android/apps/youtube/app/remote/bw;->a(Lcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/youtube/common/a/b;)V

    :cond_1
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/apps/youtube/app/remote/bw;Landroid/os/Bundle;)V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/app/remote/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/app/remote/d;-><init>(Landroid/content/Context;)V

    invoke-static {p2, p1, v0}, Lcom/google/android/apps/youtube/app/remote/a;->a(Landroid/os/Bundle;Lcom/google/android/apps/youtube/app/remote/bw;Lcom/google/android/apps/youtube/common/a/b;)V

    return-void
.end method
