.class final Lcom/google/android/apps/youtube/app/ui/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/app/ui/ab;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/app/remote/an;

.field final synthetic b:Lcom/google/android/apps/youtube/app/am;

.field final synthetic c:Z

.field final synthetic d:Lcom/google/android/apps/youtube/app/remote/ax;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/app/remote/an;Lcom/google/android/apps/youtube/app/am;ZLcom/google/android/apps/youtube/app/remote/ax;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/an;->a:Lcom/google/android/apps/youtube/app/remote/an;

    iput-object p2, p0, Lcom/google/android/apps/youtube/app/ui/an;->b:Lcom/google/android/apps/youtube/app/am;

    iput-boolean p3, p0, Lcom/google/android/apps/youtube/app/ui/an;->c:Z

    iput-object p4, p0, Lcom/google/android/apps/youtube/app/ui/an;->d:Lcom/google/android/apps/youtube/app/remote/ax;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 5

    const/4 v4, 0x0

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/an;->a:Lcom/google/android/apps/youtube/app/remote/an;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/remote/an;->e()Lcom/google/android/apps/youtube/app/remote/RemoteControl;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/an;->b:Lcom/google/android/apps/youtube/app/am;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->contentUri:Landroid/net/Uri;

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/app/ui/an;->c:Z

    sget-object v3, Lcom/google/android/apps/youtube/core/client/WatchFeature;->PLAYLISTS:Lcom/google/android/apps/youtube/core/client/WatchFeature;

    invoke-interface {v0, v1, v4, v2, v3}, Lcom/google/android/apps/youtube/app/am;->a(Landroid/net/Uri;IZLcom/google/android/apps/youtube/core/client/WatchFeature;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/an;->d:Lcom/google/android/apps/youtube/app/remote/ax;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/apps/youtube/app/remote/ax;->a(Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0
.end method
