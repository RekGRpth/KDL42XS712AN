.class final Lcom/google/android/apps/youtube/app/remote/bv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    if-eqz v0, :cond_1

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    const-class v0, Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;-><init>(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)V

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    const-class v0, Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;-><init>(Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    move-object v0, v1

    goto :goto_1
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreen;

    return-object v0
.end method
