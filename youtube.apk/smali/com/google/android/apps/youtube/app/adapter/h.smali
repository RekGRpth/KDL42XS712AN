.class public abstract Lcom/google/android/apps/youtube/app/adapter/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lcom/google/android/apps/youtube/app/adapter/ae;


# static fields
.field private static a:Landroid/os/Handler;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Landroid/widget/ImageView;

.field private final d:Landroid/view/animation/Animation;

.field private final e:I

.field private f:Lcom/google/android/apps/youtube/app/adapter/i;

.field private g:Z

.field private h:Lcom/google/android/apps/youtube/app/adapter/j;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->g:Z

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->b:Landroid/content/Context;

    const/high16 v0, 0x10a0000    # android.R.anim.fade_in

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->d:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->d:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10e0000    # android.R.integer.config_shortAnimTime

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->e:I

    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->c:Landroid/widget/ImageView;

    sget-object v0, Lcom/google/android/apps/youtube/app/adapter/h;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/apps/youtube/app/adapter/h;->a:Landroid/os/Handler;

    :cond_0
    return-void
.end method

.method static synthetic a()Landroid/os/Handler;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/app/adapter/h;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/h;)Lcom/google/android/apps/youtube/app/adapter/i;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->f:Lcom/google/android/apps/youtube/app/adapter/i;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/h;Lcom/google/android/apps/youtube/app/adapter/i;)Lcom/google/android/apps/youtube/app/adapter/i;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->f:Lcom/google/android/apps/youtube/app/adapter/i;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/adapter/h;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->g:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/adapter/h;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->g:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/adapter/h;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/adapter/h;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->c:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/app/adapter/h;)Lcom/google/android/apps/youtube/app/adapter/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->h:Lcom/google/android/apps/youtube/app/adapter/j;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/youtube/app/adapter/h;)Landroid/view/animation/Animation;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->d:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/apps/youtube/app/adapter/h;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->e:I

    return v0
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/app/adapter/i;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/apps/youtube/app/adapter/i;-><init>(Lcom/google/android/apps/youtube/app/adapter/h;Lcom/google/android/apps/youtube/app/adapter/h;B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->f:Lcom/google/android/apps/youtube/app/adapter/i;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->c:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/apps/youtube/app/adapter/h;->a:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/adapter/h;->f:Lcom/google/android/apps/youtube/app/adapter/i;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/a/e;->a(Landroid/os/Handler;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/e;

    move-result-object v1

    invoke-virtual {p0, p2, v0, v1}, Lcom/google/android/apps/youtube/app/adapter/h;->a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/apps/youtube/common/a/b;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->f:Lcom/google/android/apps/youtube/app/adapter/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->f:Lcom/google/android/apps/youtube/app/adapter/i;

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/app/adapter/i;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->f:Lcom/google/android/apps/youtube/app/adapter/i;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/youtube/app/adapter/i;->a:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->c:Landroid/widget/ImageView;

    return-object v0
.end method

.method protected a(Landroid/graphics/Matrix;Landroid/widget/ImageView;Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/app/adapter/j;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/adapter/h;->h:Lcom/google/android/apps/youtube/app/adapter/j;

    return-void
.end method

.method protected abstract a(Ljava/lang/Object;Landroid/view/View;Lcom/google/android/apps/youtube/common/a/b;)V
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->h:Lcom/google/android/apps/youtube/app/adapter/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/adapter/h;->h:Lcom/google/android/apps/youtube/app/adapter/j;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/app/adapter/j;->b()V

    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method
