.class public final Lcom/google/android/apps/youtube/app/ui/fw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/apps/youtube/datalib/innertube/av;

.field private final c:Lcom/google/android/apps/youtube/core/identity/l;

.field private final d:Lcom/google/android/apps/youtube/core/identity/aa;

.field private final e:Lcom/google/android/apps/youtube/app/ui/ja;

.field private final f:Lcom/google/android/apps/youtube/common/c/a;

.field private final g:Lcom/google/android/apps/youtube/core/aw;

.field private final h:Landroid/view/View;

.field private final i:Landroid/widget/ProgressBar;

.field private final j:Landroid/widget/TextView;

.field private k:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

.field private l:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/datalib/innertube/av;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/aa;Lcom/google/android/apps/youtube/app/ui/ja;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/common/c/a;Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->a:Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/av;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->b:Lcom/google/android/apps/youtube/datalib/innertube/av;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/aa;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->d:Lcom/google/android/apps/youtube/core/identity/aa;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/ja;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->e:Lcom/google/android/apps/youtube/app/ui/ja;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->f:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/aw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->g:Lcom/google/android/apps/youtube/core/aw;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->h:Landroid/view/View;

    sget v0, Lcom/google/android/youtube/j;->fb:I

    invoke-virtual {p8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->i:Landroid/widget/ProgressBar;

    sget v0, Lcom/google/android/youtube/j;->fc:I

    invoke-virtual {p8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->j:Landroid/widget/TextView;

    invoke-virtual {p7, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    invoke-virtual {p8, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/fw;)Lcom/google/android/apps/youtube/common/c/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->f:Lcom/google/android/apps/youtube/common/c/a;

    return-object v0
.end method

.method private a()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->j:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/app/ui/fw;Lcom/google/android/apps/youtube/datalib/innertube/model/am;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/fw;->c(Lcom/google/android/apps/youtube/datalib/innertube/model/am;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/fw;)Lcom/google/android/apps/youtube/datalib/innertube/model/am;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->k:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    return-object v0
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->k:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fw;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->k:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/youtube/p;->fO:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->h:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->i:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->j:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_1
    sget v0, Lcom/google/android/youtube/p;->fN:I

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/app/ui/fw;Lcom/google/android/apps/youtube/datalib/innertube/model/am;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/fw;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/am;)V

    return-void
.end method

.method private b(Lcom/google/android/apps/youtube/datalib/innertube/model/am;)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->h()Lcom/google/android/apps/youtube/datalib/innertube/model/d;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->h()Lcom/google/android/apps/youtube/datalib/innertube/model/d;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/fw;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/d;->a()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/d;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/d;->c()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/d;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, -0x1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/d;->d()Ljava/lang/CharSequence;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/fx;

    invoke-direct {v3, p0, p1}, Lcom/google/android/apps/youtube/app/ui/fx;-><init>(Lcom/google/android/apps/youtube/app/ui/fw;Lcom/google/android/apps/youtube/datalib/innertube/model/am;)V

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    :cond_0
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/app/ui/fw;->c(Lcom/google/android/apps/youtube/datalib/innertube/model/am;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->i()Lcom/google/android/apps/youtube/datalib/innertube/model/bb;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->e:Lcom/google/android/apps/youtube/app/ui/ja;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->i()Lcom/google/android/apps/youtube/datalib/innertube/model/bb;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/ja;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/bb;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->g()Lcom/google/a/a/a/a/ee;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->g()Lcom/google/a/a/a/a/ee;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fw;->l:Landroid/app/AlertDialog;

    if-nez v1, :cond_5

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/fw;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/fw;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/youtube/p;->dA:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fw;->l:Landroid/app/AlertDialog;

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fw;->l:Landroid/app/AlertDialog;

    iget-object v2, v0, Lcom/google/a/a/a/a/ee;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fw;->l:Landroid/app/AlertDialog;

    iget-object v0, v0, Lcom/google/a/a/a/a/ee;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->l:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/fw;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->b:Lcom/google/android/apps/youtube/datalib/innertube/av;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/av;->a()Lcom/google/android/apps/youtube/datalib/innertube/aw;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/aw;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/aw;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/aw;->a:[B

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/aw;->a([B)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fw;->b:Lcom/google/android/apps/youtube/datalib/innertube/av;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/fy;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/youtube/app/ui/fy;-><init>(Lcom/google/android/apps/youtube/app/ui/fw;Lcom/google/android/apps/youtube/datalib/innertube/model/am;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/av;->a(Lcom/google/android/apps/youtube/datalib/innertube/aw;Lcom/google/android/apps/youtube/datalib/a/l;)V

    goto/16 :goto_0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/app/ui/fw;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/fw;->b()V

    return-void
.end method

.method private c(Lcom/google/android/apps/youtube/datalib/innertube/model/am;)V
    .locals 3

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/fw;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->b:Lcom/google/android/apps/youtube/datalib/innertube/av;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/av;->b()Lcom/google/android/apps/youtube/datalib/innertube/ax;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/ax;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ax;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/ax;->a:[B

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/ax;->a([B)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fw;->b:Lcom/google/android/apps/youtube/datalib/innertube/av;

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/fz;

    invoke-direct {v2, p0, p1}, Lcom/google/android/apps/youtube/app/ui/fz;-><init>(Lcom/google/android/apps/youtube/app/ui/fw;Lcom/google/android/apps/youtube/datalib/innertube/model/am;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/av;->a(Lcom/google/android/apps/youtube/datalib/innertube/ax;Lcom/google/android/apps/youtube/datalib/a/l;)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/app/ui/fw;)Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->g:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/am;)V
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-object p1, p0, Lcom/google/android/apps/youtube/app/ui/fw;->k:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->h:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->f()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->c()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->e()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->j:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->b()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/fw;->b()V

    goto :goto_1
.end method

.method public final handleChannelSubscribedEvent(Lcom/google/android/apps/youtube/app/ui/p;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->k:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ui/p;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fw;->k:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->k:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->a(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/fw;->b()V

    :cond_0
    return-void
.end method

.method public final handleChannelUnsubscribedEvent(Lcom/google/android/apps/youtube/app/ui/q;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->k:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/ui/q;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fw;->k:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->k:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->a(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/ui/fw;->b()V

    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->k:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/ui/fw;->k:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fw;->c:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/ui/fw;->d:Lcom/google/android/apps/youtube/core/identity/aa;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/ui/fw;->a:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/apps/youtube/app/ui/ga;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/youtube/app/ui/ga;-><init>(Lcom/google/android/apps/youtube/app/ui/fw;Lcom/google/android/apps/youtube/datalib/innertube/model/am;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/youtube/core/identity/aa;->a(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/identity/ah;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/app/ui/fw;->b(Lcom/google/android/apps/youtube/datalib/innertube/model/am;)V

    goto :goto_0
.end method
