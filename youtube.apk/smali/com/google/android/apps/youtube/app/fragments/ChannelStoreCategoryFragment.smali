.class public final Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;
.super Lcom/google/android/apps/youtube/app/fragments/PaneFragment;
.source "SourceFile"


# instance fields
.field private Y:Lcom/google/android/apps/youtube/app/ui/et;

.field private Z:Lcom/google/android/apps/youtube/core/ui/l;

.field private a:Landroid/content/res/Resources;

.field private aa:Landroid/net/Uri;

.field private ab:Ljava/lang/String;

.field private ac:Z

.field private b:Lcom/google/android/apps/youtube/core/identity/l;

.field private d:Lcom/google/android/apps/youtube/core/client/bc;

.field private e:Lcom/google/android/apps/youtube/core/async/af;

.field private f:Lcom/google/android/apps/youtube/core/client/bj;

.field private g:Lcom/google/android/apps/youtube/common/c/a;

.field private h:Lcom/google/android/apps/youtube/core/aw;

.field private i:Lcom/google/android/apps/youtube/core/Analytics;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;-><init>()V

    return-void
.end method

.method private L()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->Y:Lcom/google/android/apps/youtube/app/ui/et;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->Y:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->a:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/youtube/k;->b:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/et;->a(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 14

    sget v1, Lcom/google/android/youtube/l;->q:I

    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-virtual {p1, v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    sget v1, Lcom/google/android/youtube/j;->as:I

    invoke-virtual {v13, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/google/android/apps/youtube/core/ui/PagedListView;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v5

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->i:Lcom/google/android/apps/youtube/core/Analytics;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->d:Lcom/google/android/apps/youtube/core/client/bc;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->b:Lcom/google/android/apps/youtube/core/identity/l;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->g:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v7, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->N()Lcom/google/android/apps/youtube/app/am;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    iget-object v9, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->f:Lcom/google/android/apps/youtube/core/client/bj;

    iget-object v10, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v11, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/youtube/app/adapter/ag;->a(Landroid/content/Context;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/app/YouTubeApplication;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/app/am;Lcom/google/android/apps/youtube/core/aw;Lcom/google/android/apps/youtube/core/client/bj;Lcom/google/android/apps/youtube/app/ui/gj;Landroid/app/Activity;)Lcom/google/android/apps/youtube/app/adapter/u;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v2, v3, v1}, Lcom/google/android/apps/youtube/app/ui/et;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/core/a/a;)V

    iput-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->Y:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->Y:Lcom/google/android/apps/youtube/app/ui/et;

    sget v2, Lcom/google/android/youtube/g;->x:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/et;->b(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->Y:Lcom/google/android/apps/youtube/app/ui/et;

    sget v2, Lcom/google/android/youtube/g;->x:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/et;->c(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->Y:Lcom/google/android/apps/youtube/app/ui/et;

    sget v2, Lcom/google/android/youtube/g;->u:I

    sget v3, Lcom/google/android/youtube/g;->w:I

    sget v4, Lcom/google/android/youtube/g;->v:I

    sget v5, Lcom/google/android/youtube/g;->t:I

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/apps/youtube/app/ui/et;->a(IIII)V

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->ac:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->d:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/client/bc;->g()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v1

    :goto_0
    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->e:Lcom/google/android/apps/youtube/core/async/af;

    new-instance v1, Lcom/google/android/apps/youtube/core/ui/l;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v4, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->Y:Lcom/google/android/apps/youtube/app/ui/et;

    iget-object v5, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->e:Lcom/google/android/apps/youtube/core/async/af;

    iget-object v6, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    move-object v3, v12

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/youtube/core/ui/l;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/core/ui/PagedView;Lcom/google/android/apps/youtube/core/a/a;Lcom/google/android/apps/youtube/core/async/af;Lcom/google/android/apps/youtube/core/aw;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->Z:Lcom/google/android/apps/youtube/core/ui/l;

    if-eqz p3, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->Z:Lcom/google/android/apps/youtube/core/ui/l;

    const-string v2, "category_helper"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/core/ui/l;->a(Landroid/os/Bundle;)V

    :cond_0
    return-object v13

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->d:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/core/client/bc;->f()Lcom/google/android/apps/youtube/core/async/af;

    move-result-object v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->ab:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->j()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->a:Landroid/content/res/Resources;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->b:Lcom/google/android/apps/youtube/core/identity/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->d:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aw()Lcom/google/android/apps/youtube/core/client/bj;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->f:Lcom/google/android/apps/youtube/core/client/bj;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->bf()Lcom/google/android/apps/youtube/common/c/a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->g:Lcom/google/android/apps/youtube/common/c/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aO()Lcom/google/android/apps/youtube/core/aw;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->h:Lcom/google/android/apps/youtube/core/aw;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->ba()Lcom/google/android/apps/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->i:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->h()Landroid/os/Bundle;

    move-result-object v1

    const-string v0, "channel_feed_uri"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    const-string v2, "channelFeedUri cannot be null"

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->aa:Landroid/net/Uri;

    const-string v0, "category"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "category cannot be null"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->ab:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->ab:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;->RECOMMENDED:Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->a:Landroid/content/res/Resources;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/ChannelStoreOutline$Category;->toString(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->ac:Z

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_channel"

    return-object v0
.end method

.method public final d()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->d()V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->L()V

    return-void
.end method

.method public final e()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->e()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->Z:Lcom/google/android/apps/youtube/core/ui/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->Z:Lcom/google/android/apps/youtube/core/ui/l;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/ui/l;->f()V

    :cond_0
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->e(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->Z:Lcom/google/android/apps/youtube/core/ui/l;

    if-eqz v0, :cond_0

    const-string v0, "category_helper"

    iget-object v1, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->Z:Lcom/google/android/apps/youtube/core/ui/l;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/ui/l;->d()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->L()V

    return-void
.end method

.method public final r()V
    .locals 5

    const/4 v1, 0x1

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/google/android/apps/youtube/app/fragments/PaneFragment;->r()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->G()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->ac:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->Z:Lcom/google/android/apps/youtube/core/ui/l;

    new-array v1, v1, [Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->d:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    const/16 v3, 0x18

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->e(I)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/l;->a([Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->Z:Lcom/google/android/apps/youtube/core/ui/l;

    new-array v1, v1, [Lcom/google/android/apps/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->d:Lcom/google/android/apps/youtube/core/client/bc;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/core/client/bc;->a()Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/app/fragments/ChannelStoreCategoryFragment;->aa:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;->d(Landroid/net/Uri;)Lcom/google/android/apps/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/core/ui/l;->a([Lcom/google/android/apps/youtube/core/async/GDataRequest;)V

    goto :goto_0
.end method
