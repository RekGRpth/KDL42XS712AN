.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/bb;
.super Lcom/google/android/apps/youtube/datalib/legacy/a/b;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/a/b;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/a/b;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-void
.end method

.method private static e(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;
    .locals 2

    invoke-static {p0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    :try_start_0
    new-instance v1, Lcom/google/android/apps/youtube/a/a/e;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/a/a/e;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/a/a/e;->b([B)Lcom/google/protobuf/micro/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/e;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;-><init>(Lcom/google/android/apps/youtube/a/a/e;)V

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;-><init>()V

    goto :goto_0
.end method

.method private static f(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;
    .locals 2

    invoke-static {p0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    :try_start_0
    new-instance v1, Lcom/google/android/apps/youtube/a/a/f;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/a/a/f;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/a/a/f;->b([B)Lcom/google/protobuf/micro/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/f;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;-><init>(Lcom/google/android/apps/youtube/a/a/f;)V

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;-><init>()V

    goto :goto_0
.end method

.method private static g(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/google/a/a/a/a/ne;
    .locals 2

    invoke-static {p0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    :try_start_0
    new-instance v1, Lcom/google/a/a/a/a/ne;

    invoke-direct {v1}, Lcom/google/a/a/a/a/ne;-><init>()V

    invoke-static {v1, v0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/ne;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Lorg/json/JSONException;

    const-string v1, "Invalid protobuf"

    invoke-direct {v0, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final a(Lorg/json/JSONObject;I)Lcom/google/android/apps/youtube/datalib/legacy/a/a;
    .locals 59

    const/4 v3, 0x1

    move/from16 v0, p2

    if-eq v0, v3, :cond_0

    new-instance v3, Lorg/json/JSONException;

    const-string v4, "Unsupported version"

    invoke-direct {v3, v4}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    new-instance v3, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    const-string v4, "impressionUris"

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    const-string v5, "adVideoId"

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "originalVideoId"

    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "adBreakId"

    move-object/from16 v0, p1

    invoke-static {v0, v7}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "title"

    move-object/from16 v0, p1

    invoke-static {v0, v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "adOwnerName"

    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "adOwnerUri"

    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->c(Lorg/json/JSONObject;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    const-string v11, "vastAdId"

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "vastAdSystem"

    move-object/from16 v0, p1

    invoke-static {v0, v12}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v13, "billingPartner"

    const-class v14, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-object/from16 v0, p1

    invoke-static {v0, v13, v14}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    const-string v14, "adFormat"

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v15, "duration"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v15

    const/16 v16, 0x0

    const-string v17, "playbackTracking"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->e(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    move-result-object v17

    const-string v18, "playerConfig"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->f(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    move-result-object v18

    const-string v19, "clickthroughUri"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->c(Lorg/json/JSONObject;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    const-string v20, "startPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v20

    const-string v21, "firstQuartilePingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v21

    const-string v22, "midpointPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v22

    const-string v23, "thirdQuartilePingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v23

    const-string v24, "progressPings"

    const-class v25, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v24

    const-string v25, "skipPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v25

    const-string v26, "skipShownPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v26

    const-string v27, "engagedViewPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v27

    const-string v28, "completePingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v28

    const-string v29, "closePingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v29

    const-string v30, "pausePingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v30

    const-string v31, "resumePingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v31

    const-string v32, "mutePingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v32

    const-string v33, "fullscreenPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v33

    const-string v34, "clickthroughPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v34

    const-string v35, "videoTitleClickedPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v35

    const-string v36, "errorPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v36

    const-string v37, "exclusionReasonPingUris"

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v37

    const-string v38, "videoAdTrackingTemplateUri"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->c(Lorg/json/JSONObject;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v38

    const-string v39, "adSenseBaseConversionUri"

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->c(Lorg/json/JSONObject;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v39

    const-string v40, "shouldPingVssOnEngaged"

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v40

    const-string v41, "fallbackHint"

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v41

    const-string v42, "expirationTimeMillis"

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v42

    const-string v44, "assetFrequencyCap"

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v44

    const-string v45, "isPublicVideo"

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v45

    const-string v46, "showCtaAnnotations"

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v46

    const-string v47, "adAnnotations"

    move-object/from16 v0, p1

    move-object/from16 v1, v47

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->g(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/google/a/a/a/a/ne;

    move-result-object v47

    const-string v48, "adInfoCards"

    move-object/from16 v0, p1

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v49

    if-eqz v49, :cond_2

    const/16 v48, 0x0

    :cond_1
    const-string v49, "requestTimeMilliseconds"

    move-object/from16 v0, p1

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v49

    const-string v51, "offlineShouldCountPlayback"

    move-object/from16 v0, p1

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v51

    const-string v52, "shouldAllowQueuedOfflinePings"

    move-object/from16 v0, p1

    move-object/from16 v1, v52

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v52

    const-string v53, "adWrapperUri"

    move-object/from16 v0, p1

    move-object/from16 v1, v53

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->c(Lorg/json/JSONObject;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v53

    const-string v54, "prefetchedAd"

    move-object/from16 v0, p1

    move-object/from16 v1, v54

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/a/a;

    move-result-object v54

    check-cast v54, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    const-string v55, "parentWrapper"

    move-object/from16 v0, p1

    move-object/from16 v1, v55

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/a/a;

    move-result-object v55

    check-cast v55, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    const/16 v56, 0x0

    const-string v57, "infoCards"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v57

    if-eqz v57, :cond_3

    const/16 v57, 0x0

    :goto_0
    const-string v58, "survey"

    move-object/from16 v0, p1

    move-object/from16 v1, v58

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/a/a;

    move-result-object v58

    check-cast v58, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    invoke-direct/range {v3 .. v58}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;Ljava/lang/String;ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Landroid/net/Uri;Landroid/net/Uri;ZZJIZZLcom/google/a/a/a/a/ne;Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;JZZLandroid/net/Uri;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;)V

    return-object v3

    :cond_2
    const/16 v49, 0x2

    invoke-static/range {v48 .. v49}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v48

    invoke-static/range {v48 .. v48}, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->fromByteArray([B)Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    move-result-object v48

    if-nez v48, :cond_1

    new-instance v3, Lorg/json/JSONException;

    const-string v4, "Invalid info card byte array"

    invoke-direct {v3, v4}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_3
    const-string v57, "infoCards"

    move-object/from16 v0, p1

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v57

    invoke-static/range {v57 .. v57}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v57

    goto :goto_0
.end method

.method protected final a(Lorg/json/JSONObject;)V
    .locals 4

    const/4 v3, 0x2

    const-string v0, "impressionUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getImpressionUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "adVideoId"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "originalVideoId"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "adBreakId"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdBreakId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "title"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "adOwnerName"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    # getter for: Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerName:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->access$000(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "adOwnerUri"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    # getter for: Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->access$100(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "vastAdId"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "vastAdSystem"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdSystem()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "billingPartner"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getBillingPartner()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Enum;)V

    const-string v0, "adFormat"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdFormat()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "duration"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getDuration()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "playbackTracking"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    # getter for: Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playbackTracking:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;
    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->access$200(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->toPlaybackTrackingProto()Lcom/google/android/apps/youtube/a/a/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/a/a/e;->s()[B

    move-result-object v1

    invoke-static {v1, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "playerConfig"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    # getter for: Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playerConfig:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;
    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->access$300(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->toPlayerConfigProto()Lcom/google/android/apps/youtube/a/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/a/a/f;->s()[B

    move-result-object v1

    invoke-static {v1, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "clickthroughUri"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClickthroughUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "startPingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getStartPingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "firstQuartilePingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getFirstQuartilePingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "midpointPingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getMidpointPingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "thirdQuartilePingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getThirdQuartilePingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "progressPings"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getProgressPings()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "skipPingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSkipPingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "skipShownPingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSkipShownPingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "engagedViewPingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getEngagedViewPingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "completePingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getCompletePingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "closePingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClosePingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "pausePingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPausePingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "resumePingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getResumePingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "mutePingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getMutePingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "fullscreenPingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getFullscreenPingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "clickthroughPingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClickTrackingPingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "videoTitleClickedPingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoTitleClickedPingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "errorPingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getErrorPingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "exclusionReasonPingUris"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExclusionReasonPingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "videoAdTrackingTemplateUri"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoAdTrackingTemplateUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "adSenseBaseConversionUri"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdSenseBaseConversionUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "shouldPingVssOnEngaged"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldPingVssOnEngaged()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "fallbackHint"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowAdsFallback()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "expirationTimeMillis"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExpirationTimeMillis()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "assetFrequencyCap"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAssetFrequencyCap()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "isPublicVideo"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isPublicVideo()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "showCtaAnnotations"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldShowCtaAnnotations()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adAnnotations:Lcom/google/a/a/a/a/ne;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adAnnotations:Lcom/google/a/a/a/a/ne;

    invoke-static {v0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v0

    const-string v1, "adAnnotations"

    invoke-static {v0, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adInfoCards:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adInfoCards:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->toByteArray()[B

    move-result-object v0

    const-string v1, "adInfoCards"

    invoke-static {v0, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    :cond_1
    const-string v0, "requestTimeMilliseconds"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getRequestTimeMills()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "offlineShouldCountPlayback"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isOfflineShouldCountPlayback()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "shouldAllowQueuedOfflinePings"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    # invokes: Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowQueuedOfflinePings()Z
    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->access$400(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "adWrapperUri"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdWrapperUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "prefetchedAd"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPrefetchedAd()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/a/a;)V

    const-string v0, "parentWrapper"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/a/a;)V

    const-string v0, "infoCards"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getInfoCards()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "survey"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSurvey()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;->a(Lorg/json/JSONObject;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/a/a;)V

    return-void
.end method
