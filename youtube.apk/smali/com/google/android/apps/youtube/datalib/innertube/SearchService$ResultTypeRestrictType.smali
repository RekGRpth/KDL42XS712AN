.class public final enum Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

.field public static final enum RESULT_TYPE_ANY:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

.field public static final enum RESULT_TYPE_CHANNEL:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

.field public static final enum RESULT_TYPE_PLAYLIST:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;


# instance fields
.field private final type:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    const-string v1, "RESULT_TYPE_ANY"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;->RESULT_TYPE_ANY:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    const-string v1, "RESULT_TYPE_CHANNEL"

    invoke-direct {v0, v1, v4, v3}, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;->RESULT_TYPE_CHANNEL:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    const-string v1, "RESULT_TYPE_PLAYLIST"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;->RESULT_TYPE_PLAYLIST:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    new-array v0, v5, [Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;->RESULT_TYPE_ANY:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;->RESULT_TYPE_CHANNEL:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;->RESULT_TYPE_PLAYLIST:Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;->$VALUES:[Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;->type:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;->$VALUES:[Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;

    return-object v0
.end method


# virtual methods
.method public final getType()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService$ResultTypeRestrictType;->type:I

    return v0
.end method
