.class public final Lcom/google/android/apps/youtube/datalib/e/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/net/Uri;

.field private c:Z

.field private d:J

.field private e:I

.field private f:Lcom/google/android/apps/youtube/datalib/e/d;


# direct methods
.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/e/f;->c:Z

    sget-object v0, Lcom/google/android/apps/youtube/datalib/e/d;->b:Lcom/google/android/apps/youtube/datalib/e/d;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/f;->f:Lcom/google/android/apps/youtube/datalib/e/d;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/f;->a:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/apps/youtube/datalib/e/f;->e:I

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/youtube/datalib/e/f;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/f;->b:Landroid/net/Uri;

    return-object v0
.end method

.method public final a(J)Lcom/google/android/apps/youtube/datalib/e/f;
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/youtube/datalib/e/f;->d:J

    return-object p0
.end method

.method public final a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/e/f;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/f;->b:Landroid/net/Uri;

    return-object p0
.end method

.method public final a(Z)Lcom/google/android/apps/youtube/datalib/e/f;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/e/f;->c:Z

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/e/d;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/e/f;->f:Lcom/google/android/apps/youtube/datalib/e/d;

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/f;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/e/f;->d:J

    return-wide v0
.end method

.method public final d()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/e/f;->e:I

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/f;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/e/f;->c:Z

    return v0
.end method

.method public final g()Lcom/google/android/apps/youtube/datalib/e/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/f;->f:Lcom/google/android/apps/youtube/datalib/e/d;

    return-object v0
.end method
