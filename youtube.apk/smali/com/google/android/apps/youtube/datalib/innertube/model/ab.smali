.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/model/u;


# instance fields
.field private final a:Lcom/google/a/a/a/a/oi;

.field private b:Ljava/lang/CharSequence;

.field private c:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

.field private d:Ljava/lang/CharSequence;

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/oi;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/oi;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget-object v0, v0, Lcom/google/a/a/a/a/oi;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget-object v0, v0, Lcom/google/a/a/a/a/oi;->c:Lcom/google/a/a/a/a/kz;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->b:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget-object v0, v0, Lcom/google/a/a/a/a/oi;->d:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget-object v0, v0, Lcom/google/a/a/a/a/oi;->d:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->b:Ljava/lang/CharSequence;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->b:Ljava/lang/CharSequence;

    return-object v0

    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->b:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final d()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget-object v1, v1, Lcom/google/a/a/a/a/oi;->e:Lcom/google/a/a/a/a/sx;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;-><init>(Lcom/google/a/a/a/a/sx;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    return-object v0
.end method

.method public final e()Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget-object v0, v0, Lcom/google/a/a/a/a/oi;->r:Lcom/google/a/a/a/a/kz;

    return-object v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->d:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget-object v0, v0, Lcom/google/a/a/a/a/oi;->f:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->d:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final g()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->e:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget-object v0, v0, Lcom/google/a/a/a/a/oi;->g:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->e:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final h()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->f:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget-object v0, v0, Lcom/google/a/a/a/a/oi;->h:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->f:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final i()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget-object v0, v0, Lcom/google/a/a/a/a/oi;->k:Lcom/google/a/a/a/a/ja;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget-object v0, v0, Lcom/google/a/a/a/a/oi;->k:Lcom/google/a/a/a/a/ja;

    iget-boolean v0, v0, Lcom/google/a/a/a/a/ja;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget-object v0, v0, Lcom/google/a/a/a/a/oi;->k:Lcom/google/a/a/a/a/ja;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget-object v0, v0, Lcom/google/a/a/a/a/oi;->k:Lcom/google/a/a/a/a/ja;

    iget-boolean v0, v0, Lcom/google/a/a/a/a/ja;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget-object v0, v0, Lcom/google/a/a/a/a/oi;->l:Lcom/google/a/a/a/a/rb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget-object v0, v0, Lcom/google/a/a/a/a/oi;->l:Lcom/google/a/a/a/a/rb;

    iget-boolean v0, v0, Lcom/google/a/a/a/a/rb;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget-object v0, v0, Lcom/google/a/a/a/a/oi;->s:Lcom/google/a/a/a/a/el;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget-object v0, v0, Lcom/google/a/a/a/a/oi;->s:Lcom/google/a/a/a/a/el;

    iget-boolean v0, v0, Lcom/google/a/a/a/a/el;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    iget v0, v0, Lcom/google/a/a/a/a/oi;->q:I

    return v0
.end method

.method public final n()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ab;->a:Lcom/google/a/a/a/a/oi;

    invoke-static {v0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v0

    return-object v0
.end method
