.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/legacy/model/r;
.implements Ljava/io/Serializable;


# instance fields
.field private author:Ljava/lang/String;

.field private paidContent:Z

.field private subscriberCount:I

.field private summary:Ljava/lang/String;

.field private title:Ljava/lang/String;

.field private updated:Ljava/util/Date;

.field private userProfileUri:Landroid/net/Uri;

.field private videoCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->title:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->summary:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->author:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->userProfileUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->updated:Ljava/util/Date;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->videoCount:I

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->subscriberCount:I

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->paidContent:Z

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->build()Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->summary:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->author:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->userProfileUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->updated:Ljava/util/Date;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->videoCount:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->subscriberCount:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->paidContent:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    return-void
.end method


# virtual methods
.method public final author(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->author:Ljava/lang/String;

    return-object p0
.end method

.method public final build()Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;
    .locals 9

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->title:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->summary:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->author:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->userProfileUri:Landroid/net/Uri;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->updated:Ljava/util/Date;

    iget v6, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->videoCount:I

    iget v7, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->subscriberCount:I

    iget-boolean v8, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->paidContent:Z

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/util/Date;IIZ)V

    return-object v0
.end method

.method public final bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->build()Lcom/google/android/apps/youtube/datalib/legacy/model/Channel;

    move-result-object v0

    return-object v0
.end method

.method public final paidContent(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->paidContent:Z

    return-object p0
.end method

.method public final subscriberCount(I)Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->subscriberCount:I

    return-object p0
.end method

.method public final summary(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->summary:Ljava/lang/String;

    return-object p0
.end method

.method public final title(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->title:Ljava/lang/String;

    return-object p0
.end method

.method public final updated(Ljava/util/Date;)Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->updated:Ljava/util/Date;

    return-object p0
.end method

.method public final userProfileUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->userProfileUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final videoCount(I)Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Channel$Builder;->videoCount:I

    return-object p0
.end method
