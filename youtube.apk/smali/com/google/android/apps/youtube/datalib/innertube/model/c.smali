.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/a/a/a/a/ba;

.field private b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

.field private c:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

.field private d:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

.field private e:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/ba;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/ba;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->a:Lcom/google/a/a/a/a/ba;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->a:Lcom/google/a/a/a/a/ba;

    iget-object v1, v1, Lcom/google/a/a/a/a/ba;->h:Lcom/google/a/a/a/a/sx;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;-><init>(Lcom/google/a/a/a/a/sx;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->a:Lcom/google/a/a/a/a/ba;

    iget-object v1, v1, Lcom/google/a/a/a/a/ba;->g:Lcom/google/a/a/a/a/sx;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;-><init>(Lcom/google/a/a/a/a/sx;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/youtube/datalib/innertube/model/am;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->d:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->a:Lcom/google/a/a/a/a/ba;

    iget-object v0, v0, Lcom/google/a/a/a/a/ba;->n:Lcom/google/a/a/a/a/bc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->a:Lcom/google/a/a/a/a/ba;

    iget-object v0, v0, Lcom/google/a/a/a/a/ba;->n:Lcom/google/a/a/a/a/bc;

    iget-object v0, v0, Lcom/google/a/a/a/a/bc;->c:Lcom/google/a/a/a/a/sd;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->a:Lcom/google/a/a/a/a/ba;

    iget-object v1, v1, Lcom/google/a/a/a/a/ba;->n:Lcom/google/a/a/a/a/bc;

    iget-object v1, v1, Lcom/google/a/a/a/a/bc;->c:Lcom/google/a/a/a/a/sd;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/am;-><init>(Lcom/google/a/a/a/a/sd;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->d:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->d:Lcom/google/android/apps/youtube/datalib/innertube/model/am;

    return-object v0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->a:Lcom/google/a/a/a/a/ba;

    iget-object v0, v0, Lcom/google/a/a/a/a/ba;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->a:Lcom/google/a/a/a/a/ba;

    iget-object v0, v0, Lcom/google/a/a/a/a/ba;->e:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final e()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->e:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->a:Lcom/google/a/a/a/a/ba;

    iget-object v0, v0, Lcom/google/a/a/a/a/ba;->r:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->e:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/c;->e:Ljava/lang/CharSequence;

    return-object v0
.end method
