.class public Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/legacy/model/r;
.implements Ljava/io/Serializable;


# instance fields
.field protected elementsPerPage:I

.field protected entries:Ljava/util/List;

.field protected nextUri:Landroid/net/Uri;

.field protected previousUri:Landroid/net/Uri;

.field protected startIndex:I

.field protected totalResults:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->entries:Ljava/util/List;

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->totalResults:I

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->elementsPerPage:I

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->startIndex:I

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->previousUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->nextUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->entries:Ljava/util/List;

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->build()Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->totalResults:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->elementsPerPage:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->startIndex:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->previousUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->nextUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->entries:Ljava/util/List;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public addEntries(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->entries:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public addEntry(Ljava/lang/Object;)Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->entries:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public build()Lcom/google/android/apps/youtube/datalib/legacy/model/Page;
    .locals 7

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->totalResults:I

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->elementsPerPage:I

    iget v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->startIndex:I

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->previousUri:Landroid/net/Uri;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->nextUri:Landroid/net/Uri;

    iget-object v6, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->entries:Ljava/util/List;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page;-><init>(IIILandroid/net/Uri;Landroid/net/Uri;Ljava/util/List;)V

    return-object v0
.end method

.method public bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->build()Lcom/google/android/apps/youtube/datalib/legacy/model/Page;

    move-result-object v0

    return-object v0
.end method

.method public elementsPerPage(I)Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->elementsPerPage:I

    return-object p0
.end method

.method public entries(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->entries:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public nextUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->nextUri:Landroid/net/Uri;

    return-object p0
.end method

.method public previousUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->previousUri:Landroid/net/Uri;

    return-object p0
.end method

.method public startIndex(I)Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->startIndex:I

    return-object p0
.end method

.method public totalResults(I)Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Page$Builder;->totalResults:I

    return-object p0
.end method
