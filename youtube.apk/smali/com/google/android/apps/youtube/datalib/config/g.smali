.class public final Lcom/google/android/apps/youtube/datalib/config/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/config/a;


# instance fields
.field private final b:Landroid/content/SharedPreferences;

.field private final c:Landroid/util/SparseArray;

.field private final d:Lcom/google/android/apps/youtube/datalib/config/e;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/config/e;Landroid/content/SharedPreferences;Landroid/util/SparseArray;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/config/e;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/config/g;->d:Lcom/google/android/apps/youtube/datalib/config/e;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/config/g;->b:Landroid/content/SharedPreferences;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/config/g;->c:Landroid/util/SparseArray;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/config/g;->d:Lcom/google/android/apps/youtube/datalib/config/e;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/config/e;->a()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/config/g;->d:Lcom/google/android/apps/youtube/datalib/config/e;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/config/e;->b()Z

    move-result v0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/config/g;->b:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;->getInnerTubeApiSelection(Landroid/content/SharedPreferences;)Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;->getEncodedPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    const-string v0, "deviceregistration/v1/devices"

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    const-string v0, "plus/v1whitelisted"

    return-object v0
.end method

.method public final f()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/config/g;->b:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;->getApiaryHostSelection(Landroid/content/SharedPreferences;)Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;->getApiaryBaseUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final g()[B
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/config/g;->b:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;->getApiaryHostSelection(Landroid/content/SharedPreferences;)Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/datalib/config/h;->a:[I

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled case: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/config/g;->c:Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/config/g;->c:Landroid/util/SparseArray;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
