.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/a/a/a/a/fh;

.field private b:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/fh;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/fh;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/j;->a:Lcom/google/a/a/a/a/fh;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/j;->b:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/j;->a:Lcom/google/a/a/a/a/fh;

    iget-object v0, v0, Lcom/google/a/a/a/a/fh;->b:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/j;->a:Lcom/google/a/a/a/a/fh;

    iget-object v0, v0, Lcom/google/a/a/a/a/fh;->b:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/j;->b:Ljava/lang/CharSequence;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/j;->b:Ljava/lang/CharSequence;

    return-object v0

    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/j;->b:Ljava/lang/CharSequence;

    goto :goto_0
.end method
