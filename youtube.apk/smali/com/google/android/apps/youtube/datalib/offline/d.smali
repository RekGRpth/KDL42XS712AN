.class final Lcom/google/android/apps/youtube/datalib/offline/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/volley/VolleyError;

.field final synthetic b:Lcom/google/android/apps/youtube/datalib/offline/c;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/datalib/offline/c;Lcom/android/volley/VolleyError;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/offline/d;->b:Lcom/google/android/apps/youtube/datalib/offline/c;

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/offline/d;->a:Lcom/android/volley/VolleyError;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/d;->b:Lcom/google/android/apps/youtube/datalib/offline/c;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/offline/c;->b:Lcom/google/android/apps/youtube/datalib/offline/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/d;->b:Lcom/google/android/apps/youtube/datalib/offline/c;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/offline/c;->a:Lcom/google/android/apps/youtube/a/a/c;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/offline/d;->a:Lcom/android/volley/VolleyError;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/offline/a;->a(Lcom/google/android/apps/youtube/datalib/offline/a;Lcom/google/android/apps/youtube/a/a/c;Lcom/android/volley/VolleyError;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/d;->b:Lcom/google/android/apps/youtube/datalib/offline/c;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/offline/c;->a:Lcom/google/android/apps/youtube/a/a/c;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/c;->m()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/d;->b:Lcom/google/android/apps/youtube/datalib/offline/c;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/offline/c;->a:Lcom/google/android/apps/youtube/a/a/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/a/a/c;->c(I)Lcom/google/android/apps/youtube/a/a/c;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/d;->b:Lcom/google/android/apps/youtube/datalib/offline/c;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/offline/c;->a:Lcom/google/android/apps/youtube/a/a/c;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/d;->b:Lcom/google/android/apps/youtube/datalib/offline/c;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/offline/c;->b:Lcom/google/android/apps/youtube/datalib/offline/a;

    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/offline/a;->d(Lcom/google/android/apps/youtube/datalib/offline/a;)Lcom/google/android/apps/youtube/common/e/b;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/a/a/c;->c(J)Lcom/google/android/apps/youtube/a/a/c;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/d;->b:Lcom/google/android/apps/youtube/datalib/offline/c;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/offline/c;->b:Lcom/google/android/apps/youtube/datalib/offline/a;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/d;->b:Lcom/google/android/apps/youtube/datalib/offline/c;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/offline/c;->a:Lcom/google/android/apps/youtube/a/a/c;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/offline/a;->a(Lcom/google/android/apps/youtube/datalib/offline/a;Lcom/google/android/apps/youtube/a/a/c;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/d;->b:Lcom/google/android/apps/youtube/datalib/offline/c;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/offline/c;->b:Lcom/google/android/apps/youtube/datalib/offline/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/offline/a;->a(Lcom/google/android/apps/youtube/datalib/offline/a;)Lcom/google/android/apps/youtube/datalib/offline/m;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/d;->b:Lcom/google/android/apps/youtube/datalib/offline/c;

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/offline/c;->a:Lcom/google/android/apps/youtube/a/a/c;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/a/a/c;->k()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/offline/m;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method
