.class public Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final ad:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

.field private final adBreak:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/a;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/a;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;->adBreak:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;->ad:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;->adBreak:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;->adBreak:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;->ad:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;->ad:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getAd()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;->ad:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-object v0
.end method

.method public getAdBreak()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;->adBreak:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;->adBreak:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/AdPair;->ad:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
