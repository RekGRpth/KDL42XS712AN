.class public final Lcom/google/android/apps/youtube/datalib/innertube/an;
.super Lcom/google/android/apps/youtube/datalib/innertube/b;
.source "SourceFile"


# instance fields
.field private c:Ljava/lang/String;

.field private final d:Ljava/util/List;

.field private e:I


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/b;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/an;->d:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/datalib/innertube/an;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/an;->d:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/datalib/innertube/an;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/an;->c:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/an;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/an;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/an;
    .locals 2

    new-instance v0, Lcom/google/a/a/a/a/oc;

    invoke-direct {v0}, Lcom/google/a/a/a/a/oc;-><init>()V

    const/4 v1, 0x1

    iput v1, v0, Lcom/google/a/a/a/a/oc;->e:I

    iput-object p1, v0, Lcom/google/a/a/a/a/oc;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/an;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/an;
    .locals 2

    new-instance v0, Lcom/google/a/a/a/a/oc;

    invoke-direct {v0}, Lcom/google/a/a/a/a/oc;-><init>()V

    const/4 v1, 0x2

    iput v1, v0, Lcom/google/a/a/a/a/oc;->e:I

    iput-object p1, v0, Lcom/google/a/a/a/a/oc;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/an;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method protected final c()V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/an;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/an;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    const-string v0, "browse/edit_playlist"

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/nano/c;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/an;->b()V

    new-instance v0, Lcom/google/a/a/a/a/od;

    invoke-direct {v0}, Lcom/google/a/a/a/a/od;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/an;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/od;->d:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/an;->e:I

    iput v1, v0, Lcom/google/a/a/a/a/od;->e:I

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/an;->d()Lcom/google/a/a/a/a/ii;

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/od;->b:Lcom/google/a/a/a/a/ii;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/an;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/a/a/a/a/oc;

    iput-object v1, v0, Lcom/google/a/a/a/a/od;->c:[Lcom/google/a/a/a/a/oc;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/an;->d:Ljava/util/List;

    iget-object v2, v0, Lcom/google/a/a/a/a/od;->c:[Lcom/google/a/a/a/a/oc;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    return-object v0
.end method
