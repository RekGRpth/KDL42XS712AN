.class public final enum Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

.field public static final enum MID_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

.field public static final enum POST_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

.field public static final enum PRE_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;


# instance fields
.field private val:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    const-string v1, "PRE_ROLL"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;->PRE_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    const-string v1, "MID_ROLL"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;->MID_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    const-string v1, "POST_ROLL"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;->POST_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    new-array v0, v5, [Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;->PRE_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;->MID_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;->POST_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;->$VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;->val:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;->$VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    return-object v0
.end method


# virtual methods
.method public final value()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;->val:I

    return v0
.end method
