.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/g;
.super Lcom/google/android/apps/youtube/datalib/legacy/a/b;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/a/b;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/a/b;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/g;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final a(Lorg/json/JSONObject;I)Lcom/google/android/apps/youtube/datalib/legacy/a/a;
    .locals 6

    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    new-instance v0, Lorg/json/JSONException;

    const-string v1, "Unsupported version"

    invoke-direct {v0, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    const-string v1, "type"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "actions"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/g;->a(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v2

    const-string v3, "events"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/g;->a(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v3

    const-string v4, "app"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;-><init>(ILjava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;Lcom/google/android/apps/youtube/datalib/legacy/model/e;)V

    return-object v0
.end method

.method protected final a(Lorg/json/JSONObject;)V
    .locals 2

    const-string v0, "type"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/g;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getType()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "actions"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/g;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getActions()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/g;->a(Ljava/util/List;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "events"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/g;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getEvents()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/g;->a(Ljava/util/List;)Lorg/json/JSONArray;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "app"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/g;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getInfoCardApp()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method
