.class public final enum Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

.field public static final enum C:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

.field public static final enum CMT:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

.field public static final enum CONN:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

.field public static final enum CPN:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

.field public static final enum MS:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

.field public static final enum NO_OP:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    const-string v1, "MS"

    const/4 v2, -0x2

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->MS:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    const-string v1, "NO_OP"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->NO_OP:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    const-string v1, "C"

    invoke-direct {v0, v1, v5, v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->C:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    const-string v1, "CPN"

    invoke-direct {v0, v1, v7, v5}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->CPN:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    const-string v1, "CONN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->CONN:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    const-string v1, "CMT"

    const/4 v2, 0x5

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->CMT:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->MS:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->NO_OP:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->C:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->CPN:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->CONN:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->CMT:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->$VALUES:[Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->value:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->value:I

    return v0
.end method

.method public static fromValue(I)Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;
    .locals 5

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->values()[Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    iget v4, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->value:I

    if-ne v4, p0, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->NO_OP:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->$VALUES:[Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    return-object v0
.end method
