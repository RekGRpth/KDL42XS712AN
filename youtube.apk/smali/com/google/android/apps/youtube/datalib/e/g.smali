.class public Lcom/google/android/apps/youtube/datalib/e/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/offline/SendingStrategy;


# instance fields
.field protected final a:Lcom/google/android/apps/youtube/common/e/b;

.field private final b:Lcom/android/volley/l;

.field private final c:Lcom/google/android/apps/youtube/datalib/config/c;

.field private final d:Lcom/google/android/apps/youtube/datalib/offline/m;


# direct methods
.method public constructor <init>(Lcom/android/volley/l;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/datalib/config/c;Lcom/google/android/apps/youtube/datalib/offline/m;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/volley/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/g;->b:Lcom/android/volley/l;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/g;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/config/c;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/g;->c:Lcom/google/android/apps/youtube/datalib/config/c;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/offline/m;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/g;->d:Lcom/google/android/apps/youtube/datalib/offline/m;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const v0, 0x323467f

    return v0
.end method

.method public final a(Lcom/google/android/apps/youtube/a/a/c;Lcom/google/android/apps/youtube/datalib/a/l;)V
    .locals 6

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/c;->g()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/offline/SendingStrategy$PermanentVolleyError;

    const-string v1, "malformed request proto"

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/offline/SendingStrategy$PermanentVolleyError;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/datalib/a/l;->a(Lcom/android/volley/VolleyError;)V

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/datalib/e/h;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/e/g;->a:Lcom/google/android/apps/youtube/common/e/b;

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/e/g;->c:Lcom/google/android/apps/youtube/datalib/config/c;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/e/g;->d:Lcom/google/android/apps/youtube/datalib/offline/m;

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/e/h;-><init>(Lcom/google/android/apps/youtube/a/a/c;Lcom/google/android/apps/youtube/datalib/a/l;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/datalib/config/c;Lcom/google/android/apps/youtube/datalib/offline/m;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/e/g;->b:Lcom/android/volley/l;

    invoke-virtual {v1, v0}, Lcom/android/volley/l;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method

.method public a(Lcom/google/android/apps/youtube/a/a/c;)Z
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/g;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/e/g;->c:Lcom/google/android/apps/youtube/datalib/config/c;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/datalib/config/c;->b()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/c;->i()J

    move-result-wide v2

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
