.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/model/u;


# instance fields
.field private final a:Lcom/google/a/a/a/a/op;

.field private b:Ljava/lang/CharSequence;

.field private c:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

.field private d:Ljava/lang/CharSequence;

.field private e:Lcom/google/a/a/a/a/kz;

.field private f:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/op;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/op;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->a:Lcom/google/a/a/a/a/op;

    iget-object v0, p1, Lcom/google/a/a/a/a/op;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p1, Lcom/google/a/a/a/a/op;->c:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v0, p1, Lcom/google/a/a/a/a/op;->e:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/op;->m:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/op;->i:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->a:Lcom/google/a/a/a/a/op;

    iget-object v0, v0, Lcom/google/a/a/a/a/op;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->b:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->a:Lcom/google/a/a/a/a/op;

    iget-object v0, v0, Lcom/google/a/a/a/a/op;->c:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->a:Lcom/google/a/a/a/a/op;

    iget-object v1, v1, Lcom/google/a/a/a/a/op;->d:[Lcom/google/a/a/a/a/sx;

    array-length v1, v1

    if-lez v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->a:Lcom/google/a/a/a/a/op;

    iget-object v0, v0, Lcom/google/a/a/a/a/op;->d:[Lcom/google/a/a/a/a/sx;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :cond_0
    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;-><init>(Lcom/google/a/a/a/a/sx;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    return-object v0
.end method

.method public final d()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->a:Lcom/google/a/a/a/a/op;

    iget-wide v0, v0, Lcom/google/a/a/a/a/op;->e:J

    return-wide v0
.end method

.method public final e()Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->e:Lcom/google/a/a/a/a/kz;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->a:Lcom/google/a/a/a/a/op;

    iget-object v0, v0, Lcom/google/a/a/a/a/op;->f:Lcom/google/a/a/a/a/kz;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->e:Lcom/google/a/a/a/a/kz;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->e:Lcom/google/a/a/a/a/kz;

    return-object v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->d:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->a:Lcom/google/a/a/a/a/op;

    iget-object v0, v0, Lcom/google/a/a/a/a/op;->m:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->d:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final g()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->f:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->a:Lcom/google/a/a/a/a/op;

    iget-object v0, v0, Lcom/google/a/a/a/a/op;->h:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->f:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ae;->f:Ljava/lang/CharSequence;

    return-object v0
.end method
