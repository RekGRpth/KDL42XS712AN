.class public Lcom/google/android/apps/youtube/datalib/innertube/model/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/model/u;


# instance fields
.field private final a:Lcom/google/a/a/a/a/ow;

.field private b:Ljava/lang/CharSequence;

.field private c:Ljava/lang/CharSequence;

.field private d:Ljava/lang/CharSequence;

.field private e:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

.field private f:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

.field private final g:Ljava/lang/String;

.field private final h:Z

.field private final i:Lcom/google/a/a/a/a/kj;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/ow;Ljava/lang/String;Z)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/ow;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->a:Lcom/google/a/a/a/a/ow;

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->g:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->h:Z

    const/4 v0, 0x0

    iget-object v2, p1, Lcom/google/a/a/a/a/ow;->j:[Lcom/google/a/a/a/a/ak;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    iget-object v5, v4, Lcom/google/a/a/a/a/ak;->h:Lcom/google/a/a/a/a/kj;

    if-eqz v5, :cond_1

    iget-object v0, v4, Lcom/google/a/a/a/a/ak;->h:Lcom/google/a/a/a/a/kj;

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->i:Lcom/google/a/a/a/a/kj;

    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->a:Lcom/google/a/a/a/a/ow;

    iget-object v0, v0, Lcom/google/a/a/a/a/ow;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->a:Lcom/google/a/a/a/a/ow;

    iget-object v0, v0, Lcom/google/a/a/a/a/ow;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->e:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->a:Lcom/google/a/a/a/a/ow;

    iget-object v1, v1, Lcom/google/a/a/a/a/ow;->c:Lcom/google/a/a/a/a/sx;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;-><init>(Lcom/google/a/a/a/a/sx;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->e:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->e:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    return-object v0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->b:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->a:Lcom/google/a/a/a/a/ow;

    iget-object v0, v0, Lcom/google/a/a/a/a/ow;->d:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final e()Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->a:Lcom/google/a/a/a/a/ow;

    iget-object v0, v0, Lcom/google/a/a/a/a/ow;->h:Lcom/google/a/a/a/a/kz;

    return-object v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->d:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->a:Lcom/google/a/a/a/a/ow;

    iget-object v0, v0, Lcom/google/a/a/a/a/ow;->f:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->d:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final g()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->a:Lcom/google/a/a/a/a/ow;

    iget-object v0, v0, Lcom/google/a/a/a/a/ow;->g:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->c:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->h:Z

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->f:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->a:Lcom/google/a/a/a/a/ow;

    iget-object v0, v0, Lcom/google/a/a/a/a/ow;->p:Lcom/google/a/a/a/a/ov;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->a:Lcom/google/a/a/a/a/ow;

    iget-object v0, v0, Lcom/google/a/a/a/a/ow;->p:Lcom/google/a/a/a/a/ov;

    iget-object v0, v0, Lcom/google/a/a/a/a/ov;->b:Lcom/google/a/a/a/a/mf;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->a:Lcom/google/a/a/a/a/ow;

    iget-object v1, v1, Lcom/google/a/a/a/a/ow;->p:Lcom/google/a/a/a/a/ov;

    iget-object v1, v1, Lcom/google/a/a/a/a/ov;->b:Lcom/google/a/a/a/a/mf;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;-><init>(Lcom/google/a/a/a/a/mf;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->f:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/af;->f:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

    return-object v0
.end method
