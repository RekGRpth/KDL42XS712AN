.class public Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final AUDIO_ONLY_STREAM_ITAG:I = 0x8c

.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final NO_EXPIRATION_TIME:J = 0x7fffffffffffffffL


# instance fields
.field private final adaptiveFormatStreams:Ljava/util/List;

.field private final allFormatStreams:Ljava/util/List;

.field private final expirationInElapsedTimeMillis:J

.field private final hlsStream:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

.field private final isLive:Z

.field private final progressiveFormatStreams:Ljava/util/List;

.field private final streamingData:Lcom/google/a/a/a/a/sb;

.field private final videoDurationMillis:J

.field private final videoId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/l;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/l;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/a/a/a/a/sb;Ljava/lang/String;JJZ)V
    .locals 6

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->streamingData:Lcom/google/a/a/a/a/sb;

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->videoId:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->videoDurationMillis:J

    iput-boolean p7, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isLive:Z

    iget-wide v1, p1, Lcom/google/a/a/a/a/sb;->b:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p1, Lcom/google/a/a/a/a/sb;->b:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v1

    add-long/2addr v1, p5

    iput-wide v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->expirationInElapsedTimeMillis:J

    :goto_0
    iget-object v1, p1, Lcom/google/a/a/a/a/sb;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p1, Lcom/google/a/a/a/a/sb;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1, p2, p3, p4}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->forHls(Landroid/net/Uri;Ljava/lang/String;J)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->hlsStream:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    :goto_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->allFormatStreams:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->progressiveFormatStreams:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->adaptiveFormatStreams:Ljava/util/List;

    iget-object v2, p1, Lcom/google/a/a/a/a/sb;->c:[Lcom/google/a/a/a/a/fj;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    iget-boolean v5, v4, Lcom/google/a/a/a/a/fj;->s:Z

    if-nez v5, :cond_0

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-direct {v5, v4, p2, p3, p4}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;-><init>(Lcom/google/a/a/a/a/fj;Ljava/lang/String;J)V

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->allFormatStreams:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->progressiveFormatStreams:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_1
    const-wide v1, 0x7fffffffffffffffL

    iput-wide v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->expirationInElapsedTimeMillis:J

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->hlsStream:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    goto :goto_1

    :cond_3
    iget-object v1, p1, Lcom/google/a/a/a/a/sb;->d:[Lcom/google/a/a/a/a/fj;

    array-length v2, v1

    :goto_3
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    iget-boolean v4, v3, Lcom/google/a/a/a/a/fj;->s:Z

    if-nez v4, :cond_4

    new-instance v4, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-direct {v4, v3, p2, p3, p4}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;-><init>(Lcom/google/a/a/a/a/fj;Ljava/lang/String;J)V

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->allFormatStreams:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->adaptiveFormatStreams:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    return-void
.end method

.method public static create(Lcom/google/a/a/a/a/sb;Ljava/lang/String;JJZ)Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;
    .locals 8

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;-><init>(Lcom/google/a/a/a/a/sb;Ljava/lang/String;JJZ)V

    return-object v0
.end method

.method private getOfflineSupportedMimeTypes(Z)Ljava/util/Set;
    .locals 1

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/f;->a:Ljava/util/Set;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/g;->a:Ljava/util/Set;

    goto :goto_0
.end method

.method private sortAndFilter(Ljava/util/List;Ljava/util/Set;)Ljava/util/List;
    .locals 4

    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getSimpleMimeType()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {p2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/k;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/k;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;)V

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v1, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getVideoId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getHlsManifestUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getHlsManifestUri()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isLive()Z

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isLive()Z

    move-result v2

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getVideoDurationMillis()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getVideoDurationMillis()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getAllFormatStreams()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getAllFormatStreams()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isLive:Z

    iget-boolean v2, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isLive:Z

    if-ne v0, v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->expirationInElapsedTimeMillis:J

    iget-wide v4, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->expirationInElapsedTimeMillis:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getAllFormatStreams()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getAllFormatStreams()Ljava/util/List;

    move-result-object v3

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getAllFormatStreams()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method public getAdaptiveFormatStreams()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->adaptiveFormatStreams:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAllFormatStreams()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->allFormatStreams:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAudioOnlyStream()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 1

    const/16 v0, 0x8c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getFormatStreamByItag(I)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v0

    return-object v0
.end method

.method public getExpirationInElapsedTimeMillis()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->expirationInElapsedTimeMillis:J

    return-wide v0
.end method

.method public getExpiredForSeconds(J)I
    .locals 4

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isExpired(J)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->expirationInElapsedTimeMillis:J

    sub-long v1, p1, v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method public getFormatStreamByItag(I)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->allFormatStreams:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v2

    if-ne v2, p1, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHlsManifestUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->streamingData:Lcom/google/a/a/a/a/sb;

    iget-object v0, v0, Lcom/google/a/a/a/a/sb;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->streamingData:Lcom/google/a/a/a/a/sb;

    iget-object v0, v0, Lcom/google/a/a/a/a/sb;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHlsStream()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->hlsStream:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    return-object v0
.end method

.method public getOfflineVideoStream(ILcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 3

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;->isExoPlayerEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->adaptiveFormatStreams:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getOfflineSupportedMimeTypes(Z)Ljava/util/Set;

    move-result-object v1

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getAdaptiveFormatStreams()Ljava/util/List;

    move-result-object v0

    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->sortAndFilter(Ljava/util/List;Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getHeight()I

    move-result v2

    if-gt v2, p1, :cond_0

    :goto_2
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getProgressiveFormatStreams()Ljava/util/List;

    move-result-object v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public getProgressiveFormatStreams()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->progressiveFormatStreams:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getVideoDurationMillis()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->videoDurationMillis:J

    return-wide v0
.end method

.method public getVideoId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->videoId:Ljava/lang/String;

    return-object v0
.end method

.method public hasAudioOnlyStream()Z
    .locals 1

    const/16 v0, 0x8c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->hasStreamForItag(I)Z

    move-result v0

    return v0
.end method

.method public hasStreamForItag(I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getFormatStreamByItag(I)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    return v0
.end method

.method public isAudioOnly()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->allFormatStreams:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->hasAudioOnlyStream()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExpired(J)Z
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->expirationInElapsedTimeMillis:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLive()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isLive:Z

    return v0
.end method

.method public isOffline()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->allFormatStreams:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->isLocal()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->streamingData:Lcom/google/a/a/a/a/sb;

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/e/j;->a(Landroid/os/Parcel;Lcom/google/protobuf/nano/c;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->videoDurationMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->expirationInElapsedTimeMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->isLive:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
