.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/a/a/a/a/mf;

.field private final b:Z

.field private c:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/mf;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/mf;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;->a:Lcom/google/a/a/a/a/mf;

    iget-boolean v0, p1, Lcom/google/a/a/a/a/mf;->b:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;->b:Z

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;->b:Z

    return v0
.end method

.method public final b()Ljava/util/Map;
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;->c:Ljava/util/Map;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;->c:Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;->a:Lcom/google/a/a/a/a/mf;

    iget-object v2, v0, Lcom/google/a/a/a/a/mf;->d:[Lcom/google/a/a/a/a/mg;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;

    invoke-direct {v5, v4, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;-><init>(Lcom/google/a/a/a/a/mg;B)V

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;->d()Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;->c:Ljava/util/Map;

    invoke-interface {v6, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;->c:Ljava/util/Map;

    return-object v0
.end method

.method public final c()Lcom/google/a/a/a/a/mh;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;->a:Lcom/google/a/a/a/a/mf;

    iget-object v0, v0, Lcom/google/a/a/a/a/mf;->c:Lcom/google/a/a/a/a/mh;

    return-object v0
.end method
