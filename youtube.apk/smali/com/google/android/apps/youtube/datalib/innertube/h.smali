.class public final Lcom/google/android/apps/youtube/datalib/innertube/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/a/f;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/pm/PackageManager;

.field private final c:Lcom/google/android/apps/youtube/core/au;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/PackageManager;Lcom/google/android/apps/youtube/core/au;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/h;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageManager;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/h;->b:Landroid/content/pm/PackageManager;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/au;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/h;->c:Lcom/google/android/apps/youtube/core/au;

    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/h;->b:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/h;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const-string v1, "\\."

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    aget-object v3, v1, v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    const/4 v3, 0x1

    aget-object v1, v1, v3

    aput-object v1, v0, v2

    const-string v1, "."

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "PackageManager did not find our package name!"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Lcom/google/a/a/a/a/ii;)V
    .locals 4

    iget-object v0, p1, Lcom/google/a/a/a/a/ii;->b:Lcom/google/a/a/a/a/ct;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/a/a/a/a/ii;->b:Lcom/google/a/a/a/a/ct;

    :goto_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/ct;->n:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/h;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/ct;->o:Ljava/lang/String;

    const/4 v1, 0x3

    iput v1, v0, Lcom/google/a/a/a/a/ct;->j:I

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/innertube/h;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/ct;->k:Ljava/lang/String;

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/ct;->m:Ljava/lang/String;

    const-string v1, "Android"

    iput-object v1, v0, Lcom/google/a/a/a/a/ct;->l:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/ct;->h:Ljava/lang/String;

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/ct;->i:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/a/a/a/a/ct;->e:[I

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/h;->c:Lcom/google/android/apps/youtube/core/au;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/h;->a:Landroid/content/Context;

    invoke-interface {v2, v3}, Lcom/google/android/apps/youtube/core/au;->a(Landroid/content/Context;)[I

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/e/c;->a([I[I)[I

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/ct;->e:[I

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/h;->c:Lcom/google/android/apps/youtube/core/au;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/h;->a:Landroid/content/Context;

    invoke-interface {v1, v2}, Lcom/google/android/apps/youtube/core/au;->b(Landroid/content/Context;)Lcom/google/a/a/a/a/bg;

    move-result-object v1

    iput-object v1, p1, Lcom/google/a/a/a/a/ii;->d:Lcom/google/a/a/a/a/bg;

    iput-object v0, p1, Lcom/google/a/a/a/a/ii;->b:Lcom/google/a/a/a/a/ct;

    return-void

    :cond_0
    new-instance v0, Lcom/google/a/a/a/a/ct;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ct;-><init>()V

    goto :goto_0
.end method
