.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/legacy/model/r;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/google/android/apps/youtube/datalib/innertube/model/v;

.field private c:J

.field private d:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/legacy/model/v;
    .locals 7

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    iget-wide v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->c:J

    iget-wide v5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->d:J

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;-><init>(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/innertube/model/v;JJ)V

    return-object v0
.end method

.method public final a(J)Lcom/google/android/apps/youtube/datalib/legacy/model/w;
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->c:J

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/v;)Lcom/google/android/apps/youtube/datalib/legacy/model/w;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/w;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->a:Ljava/lang/String;

    return-object p0
.end method

.method public final b(J)Lcom/google/android/apps/youtube/datalib/legacy/model/w;
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->d:J

    return-object p0
.end method

.method public final synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/w;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    move-result-object v0

    return-object v0
.end method
