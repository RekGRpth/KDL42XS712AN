.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/x;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/net/Uri;

.field private final d:Z

.field private final e:J

.field private final f:Lcom/google/android/apps/youtube/datalib/legacy/model/v;

.field private final g:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

.field private final h:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

.field private final i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

.field private final j:J

.field private final k:J

.field private final l:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;ZJLcom/google/android/apps/youtube/datalib/legacy/model/v;Lcom/google/android/apps/youtube/datalib/innertube/model/w;Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;JJZ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->c:Landroid/net/Uri;

    iput-boolean p4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->d:Z

    iput-wide p5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->e:J

    iput-object p7, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    iput-object p8, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    iput-object p9, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    iput-object p10, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    iput-wide p11, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->j:J

    iput-wide p13, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->k:J

    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->l:Z

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/youtube/p;->dt:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->r()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    sget v0, Lcom/google/android/youtube/p;->dh:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->b()Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->c()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->b()Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->s()Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lcom/google/android/youtube/p;->dj:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->NETWORK_READ_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    if-ne v0, v1, :cond_5

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_6

    sget v0, Lcom/google/android/youtube/p;->dl:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->l:Z

    if-nez v0, :cond_7

    sget v0, Lcom/google/android/youtube/p;->dk:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_7
    sget v0, Lcom/google/android/youtube/p;->di:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->c:Landroid/net/Uri;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->d:Z

    return v0
.end method

.method public final e()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->e:J

    return-wide v0
.end method

.method public final f()Lcom/google/android/apps/youtube/datalib/legacy/model/v;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/youtube/datalib/innertube/model/w;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    return-object v0
.end method

.method public final i()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->j:J

    return-wide v0
.end method

.method public final j()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->k:J

    return-wide v0
.end method

.method public final k()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->ACTIVE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->i:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;->PENDING:Lcom/google/android/apps/youtube/datalib/model/transfer/Transfer$Status;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->PAUSED:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->COMPLETE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()I
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->k:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->j:J

    const-wide/16 v2, 0x64

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->k:J

    div-long/2addr v0, v2

    long-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final q()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->DISK_WRITE_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->k()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->m()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->r()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->q()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->l:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u()Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->k()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->r()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->m()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->h:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->CANNOT_OFFLINE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->n()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final v()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->r()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/x;->f:Lcom/google/android/apps/youtube/datalib/legacy/model/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/v;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
