.class public final Lcom/google/android/apps/youtube/datalib/distiller/d;
.super Lcom/google/android/apps/youtube/datalib/distiller/i;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/distiller/i;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/d;->a:I

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/youtube/datalib/distiller/d;
    .locals 1

    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/d;->a:I

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/distiller/d;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/distiller/d;->b:Ljava/lang/String;

    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "activities/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/distiller/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/comments"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/distiller/d;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/distiller/d;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final b()Ljava/util/Map;
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/distiller/d;->a:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const-string v1, "maxResults"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/google/android/apps/youtube/datalib/distiller/d;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/distiller/d;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, "pageToken"

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/distiller/d;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const-string v1, "sortOrder"

    const-string v2, "descending"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method protected final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/d;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/d;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/d;->a:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/d;->a:I

    const/16 v1, 0x1f4

    if-gt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
