.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/ao;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:I

.field private final c:I


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/sy;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/sy;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->a:Landroid/net/Uri;

    iget v0, p1, Lcom/google/a/a/a/a/sy;->c:I

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->b:I

    iget v0, p1, Lcom/google/a/a/a/a/sy;->d:I

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->c:I

    return-void

    :cond_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "https"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->a:Landroid/net/Uri;

    return-object v0
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->b:I

    return v0
.end method

.method public final c()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->c:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;

    if-eqz v2, :cond_4

    check-cast p1, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->a:Landroid/net/Uri;

    if-nez v2, :cond_2

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->a:Landroid/net/Uri;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->a:Landroid/net/Uri;

    iget-object v3, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->b:I

    iget v3, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->c:I

    iget v3, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->c:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->a:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->b:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->c:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    goto :goto_0
.end method
