.class public final Lcom/google/android/apps/youtube/datalib/innertube/o;
.super Lcom/google/android/apps/youtube/datalib/innertube/b;
.source "SourceFile"


# instance fields
.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/b;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    const/4 v0, 0x0

    new-array v0, v0, [B

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/datalib/innertube/o;->a([B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/o;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/o;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/o;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/o;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/o;->d:Ljava/lang/String;

    return-object p0
.end method

.method protected final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/o;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/o;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    const-string v0, "player/heartbeat"

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/nano/c;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/o;->b()V

    new-instance v0, Lcom/google/a/a/a/a/gs;

    invoke-direct {v0}, Lcom/google/a/a/a/a/gs;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/o;->d()Lcom/google/a/a/a/a/ii;

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/gs;->b:Lcom/google/a/a/a/a/ii;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/o;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/gs;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/o;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/gs;->d:Ljava/lang/String;

    return-object v0
.end method
