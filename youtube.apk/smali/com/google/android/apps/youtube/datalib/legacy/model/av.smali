.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/av;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static b:Ljava/util/Random;


# instance fields
.field private final a:Ljava/util/List;


# direct methods
.method private constructor <init>(Ljava/util/List;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/av;->a:Ljava/util/List;

    return-void
.end method

.method public static a(ILjava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/av;
    .locals 3

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    if-ne p0, v0, :cond_2

    invoke-static {v1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    :cond_1
    :goto_1
    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/av;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/av;-><init>(Ljava/util/List;)V

    return-object v0

    :cond_2
    if-nez p0, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/av;->b:Ljava/util/Random;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/av;->b:Ljava/util/Random;

    invoke-static {v1, v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;Ljava/util/Random;)V

    goto :goto_1

    :cond_3
    invoke-static {v1}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/av;->a:Ljava/util/List;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/av;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/av;->a:Ljava/util/List;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/av;->a:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
