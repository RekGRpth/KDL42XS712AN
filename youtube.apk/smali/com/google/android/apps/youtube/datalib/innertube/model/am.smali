.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/am;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/a/a/a/a/sd;

.field private final b:Lcom/google/a/a/a/a/se;

.field private final c:Lcom/google/a/a/a/a/tn;

.field private final d:Ljava/lang/String;

.field private e:Lcom/google/android/apps/youtube/datalib/innertube/model/bb;

.field private f:Lcom/google/android/apps/youtube/datalib/innertube/model/d;

.field private g:Ljava/lang/CharSequence;

.field private h:Z


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/sd;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/sd;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->a:Lcom/google/a/a/a/a/sd;

    iget-object v0, p1, Lcom/google/a/a/a/a/sd;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->d:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/a/a/a/a/sd;->f:Lcom/google/a/a/a/a/se;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->b:Lcom/google/a/a/a/a/se;

    iget-object v0, p1, Lcom/google/a/a/a/a/sd;->l:Lcom/google/a/a/a/a/tn;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->c:Lcom/google/a/a/a/a/tn;

    iget-boolean v0, p1, Lcom/google/a/a/a/a/sd;->d:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->h:Z

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/d;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->f:Lcom/google/android/apps/youtube/datalib/innertube/model/d;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->h:Z

    return-void
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->g:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->a:Lcom/google/a/a/a/a/sd;

    iget-object v0, v0, Lcom/google/a/a/a/a/sd;->b:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->a:Lcom/google/a/a/a/a/sd;

    iget-object v0, v0, Lcom/google/a/a/a/a/sd;->b:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->g:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->h:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->a:Lcom/google/a/a/a/a/sd;

    iget-boolean v0, v0, Lcom/google/a/a/a/a/sd;->e:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->b:Lcom/google/a/a/a/a/se;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->c:Lcom/google/a/a/a/a/tn;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->f:Lcom/google/android/apps/youtube/datalib/innertube/model/d;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/google/a/a/a/a/ee;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->b:Lcom/google/a/a/a/a/se;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->b:Lcom/google/a/a/a/a/se;

    iget-object v0, v0, Lcom/google/a/a/a/a/se;->b:Lcom/google/a/a/a/a/ee;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/android/apps/youtube/datalib/innertube/model/d;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->f:Lcom/google/android/apps/youtube/datalib/innertube/model/d;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->c:Lcom/google/a/a/a/a/tn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->c:Lcom/google/a/a/a/a/tn;

    iget-object v0, v0, Lcom/google/a/a/a/a/tn;->b:Lcom/google/a/a/a/a/mr;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/d;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->c:Lcom/google/a/a/a/a/tn;

    iget-object v1, v1, Lcom/google/a/a/a/a/tn;->b:Lcom/google/a/a/a/a/mr;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/d;-><init>(Lcom/google/a/a/a/a/mr;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->f:Lcom/google/android/apps/youtube/datalib/innertube/model/d;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->f:Lcom/google/android/apps/youtube/datalib/innertube/model/d;

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/youtube/datalib/innertube/model/bb;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->e:Lcom/google/android/apps/youtube/datalib/innertube/model/bb;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->b:Lcom/google/a/a/a/a/se;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->b:Lcom/google/a/a/a/a/se;

    iget-object v0, v0, Lcom/google/a/a/a/a/se;->d:Lcom/google/a/a/a/a/xg;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/bb;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->b:Lcom/google/a/a/a/a/se;

    iget-object v1, v1, Lcom/google/a/a/a/a/se;->d:Lcom/google/a/a/a/a/xg;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/bb;-><init>(Lcom/google/a/a/a/a/xg;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->e:Lcom/google/android/apps/youtube/datalib/innertube/model/bb;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/am;->e:Lcom/google/android/apps/youtube/datalib/innertube/model/bb;

    return-object v0
.end method
