.class public final Lcom/google/android/apps/youtube/datalib/innertube/SearchService;
.super Lcom/google/android/apps/youtube/datalib/innertube/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/i;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/r;Lcom/google/android/apps/youtube/datalib/innertube/p;Lcom/android/volley/l;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/datalib/innertube/a;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/r;Lcom/google/android/apps/youtube/datalib/innertube/p;Lcom/android/volley/l;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/au;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/au;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService;->b:Lcom/google/android/apps/youtube/datalib/innertube/p;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/au;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;B)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/au;Lcom/google/android/apps/youtube/datalib/a/l;)V
    .locals 4

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/as;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/youtube/datalib/innertube/as;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/SearchService;Lcom/google/android/apps/youtube/datalib/a/l;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService;->c:Lcom/android/volley/l;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/SearchService;->a:Lcom/google/android/apps/youtube/datalib/innertube/r;

    const-class v3, Lcom/google/a/a/a/a/qm;

    invoke-virtual {v2, p1, v3, v0}, Lcom/google/android/apps/youtube/datalib/innertube/r;->a(Lcom/google/android/apps/youtube/datalib/innertube/s;Ljava/lang/Class;Lcom/google/android/apps/youtube/datalib/a/l;)Lcom/google/android/apps/youtube/datalib/innertube/q;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/volley/l;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/a/l;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/SearchService;->a()Lcom/google/android/apps/youtube/datalib/innertube/au;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/au;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/au;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/at;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/youtube/datalib/innertube/at;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/SearchService;Lcom/google/android/apps/youtube/datalib/a/l;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/SearchService;->a(Lcom/google/android/apps/youtube/datalib/innertube/au;Lcom/google/android/apps/youtube/datalib/a/l;)V

    return-void
.end method
