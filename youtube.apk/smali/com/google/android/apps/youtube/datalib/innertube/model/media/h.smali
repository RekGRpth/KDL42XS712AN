.class final Lcom/google/android/apps/youtube/datalib/innertube/model/media/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;
    .locals 2

    :try_start_0
    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    new-instance v0, Lcom/google/android/apps/youtube/a/a/f;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/f;-><init>()V

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/common/e/j;->b(Landroid/os/Parcel;Lcom/google/protobuf/micro/c;)Lcom/google/protobuf/micro/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/f;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;-><init>(Lcom/google/android/apps/youtube/a/a/f;)V
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/h;->a(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    return-object v0
.end method
