.class public final Lcom/google/android/apps/youtube/datalib/e/a;
.super Lcom/google/android/apps/youtube/datalib/a/r;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/offline/l;


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/android/apps/youtube/common/e/b;

.field private final e:Z

.field private final f:Ljava/lang/String;

.field private final g:I

.field private final h:J

.field private final i:Lcom/google/android/apps/youtube/datalib/e/d;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;IJLcom/google/android/apps/youtube/datalib/e/d;Lcom/android/volley/n;Ljava/util/List;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/datalib/config/c;)V
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p8}, Lcom/google/android/apps/youtube/datalib/a/r;-><init>(ILjava/lang/String;Lcom/android/volley/n;)V

    new-instance v0, Lcom/android/volley/d;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {p12 .. p12}, Lcom/google/android/apps/youtube/datalib/config/c;->e()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    long-to-int v1, v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/android/volley/d;-><init>(IIF)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/datalib/e/a;->a(Lcom/android/volley/q;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/datalib/e/a;->a(Z)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/a;->c:Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/a;->f:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/apps/youtube/datalib/e/a;->g:I

    iput-wide p5, p0, Lcom/google/android/apps/youtube/datalib/e/a;->h:J

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/e/d;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/a;->i:Lcom/google/android/apps/youtube/datalib/e/d;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/a;->a:Ljava/util/List;

    invoke-static {p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/a;->b:Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

    invoke-static {p11}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/a;->d:Lcom/google/android/apps/youtube/common/e/b;

    iput-boolean p2, p0, Lcom/google/android/apps/youtube/datalib/e/a;->e:Z

    return-void
.end method


# virtual methods
.method protected final a(Lcom/android/volley/j;)Lcom/android/volley/m;
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0, v0}, Lcom/android/volley/m;->a(Ljava/lang/Object;Lcom/android/volley/b;)Lcom/android/volley/m;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/google/android/apps/youtube/a/a/c;
    .locals 5

    new-instance v2, Lcom/google/android/apps/youtube/a/a/c;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/a/a/c;-><init>()V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/a/a/c;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/c;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/a;->f:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/a/a/c;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/c;

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/e/a;->h:J

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/youtube/a/a/c;->b(J)Lcom/google/android/apps/youtube/a/a/c;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/a;->d:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/youtube/a/a/c;->a(J)Lcom/google/android/apps/youtube/a/a/c;

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/e/a;->g:I

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/a/a/c;->a(I)Lcom/google/android/apps/youtube/a/a/c;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/e/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/a/a/c;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/c;

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/a/a/c;->b(I)Lcom/google/android/apps/youtube/a/a/c;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/e/a;->h()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v4, Lcom/google/android/apps/youtube/a/a/b;

    invoke-direct {v4}, Lcom/google/android/apps/youtube/a/a/b;-><init>()V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/youtube/a/a/b;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/b;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/a/a/b;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/b;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/a/a/c;->a(Lcom/google/android/apps/youtube/a/a/b;)Lcom/google/android/apps/youtube/a/a/c;

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/e/a;->t_()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/a/a/c;->b(I)Lcom/google/android/apps/youtube/a/a/c;

    return-object v2
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/a;->i:Lcom/google/android/apps/youtube/datalib/e/d;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/e/d;->a()V

    return-void
.end method

.method public final b(Lcom/android/volley/VolleyError;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/a;->i:Lcom/google/android/apps/youtube/datalib/e/d;

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/a/b;->a(Lcom/android/volley/VolleyError;)I

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/e/d;->b()V

    return-void
.end method

.method public final h()Ljava/util/Map;
    .locals 4

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/e/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/a/e;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/datalib/a/e;->a(Ljava/util/Map;)V

    goto :goto_0

    :cond_0
    :try_start_0
    const-string v0, "X-GData-Device"

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/e/a;->b:Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/e/a;->c:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis$DeviceRegistrationException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-object v1

    :catch_0
    move-exception v0

    goto :goto_1
.end method
