.class public Lcom/google/android/apps/youtube/datalib/innertube/model/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/model/u;


# instance fields
.field private a:Lcom/google/a/a/a/a/db;

.field private b:Ljava/lang/CharSequence;

.field private c:Ljava/lang/CharSequence;

.field private d:Ljava/lang/CharSequence;

.field private e:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/db;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/db;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->a:Lcom/google/a/a/a/a/db;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->b:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->a:Lcom/google/a/a/a/a/db;

    iget-object v0, v0, Lcom/google/a/a/a/a/db;->j:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->a:Lcom/google/a/a/a/a/db;

    iget-object v0, v0, Lcom/google/a/a/a/a/db;->f:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->c:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->d:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->a:Lcom/google/a/a/a/a/db;

    iget-object v0, v0, Lcom/google/a/a/a/a/db;->e:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->d:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->e:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->a:Lcom/google/a/a/a/a/db;

    iget-object v1, v1, Lcom/google/a/a/a/a/db;->c:Lcom/google/a/a/a/a/sx;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;-><init>(Lcom/google/a/a/a/a/sx;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->e:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->e:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    return-object v0
.end method

.method public final e()Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/e;->a:Lcom/google/a/a/a/a/db;

    iget-object v0, v0, Lcom/google/a/a/a/a/db;->g:Lcom/google/a/a/a/a/kz;

    return-object v0
.end method
