.class public final Lcom/google/android/apps/youtube/datalib/innertube/m;
.super Lcom/google/android/apps/youtube/datalib/innertube/a;
.source "SourceFile"


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/innertube/a;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/r;Lcom/google/android/apps/youtube/datalib/innertube/p;Lcom/android/volley/l;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/datalib/innertube/a;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/r;Lcom/google/android/apps/youtube/datalib/innertube/p;Lcom/android/volley/l;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/o;)Lcom/google/a/a/a/a/gt;
    .locals 5

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/a/k;->a()Lcom/google/android/apps/youtube/datalib/a/k;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/n;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/youtube/datalib/innertube/n;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/m;Lcom/google/android/apps/youtube/datalib/a/l;)V

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/m;->c:Lcom/android/volley/l;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/m;->a:Lcom/google/android/apps/youtube/datalib/innertube/r;

    const-class v4, Lcom/google/a/a/a/a/gt;

    invoke-virtual {v3, p1, v4, v1}, Lcom/google/android/apps/youtube/datalib/innertube/r;->a(Lcom/google/android/apps/youtube/datalib/innertube/s;Ljava/lang/Class;Lcom/google/android/apps/youtube/datalib/a/l;)Lcom/google/android/apps/youtube/datalib/innertube/q;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/android/volley/l;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/a/k;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/gt;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/o;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/o;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/m;->b:Lcom/google/android/apps/youtube/datalib/innertube/p;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/o;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;B)V

    return-object v0
.end method
