.class public Lcom/google/android/apps/youtube/datalib/legacy/model/bj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/legacy/model/r;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

.field private c:I

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Ljava/util/List;

.field private i:Ljava/util/List;

.field private j:Ljava/util/List;

.field private k:Ljava/util/List;

.field private l:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

.field private m:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;->PRE_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    iput v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->c:I

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->d:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->e:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->f:Z

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->g:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->h:Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->i:Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->j:Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->k:Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->l:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->m:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/datalib/legacy/model/bj;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->i:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/datalib/legacy/model/bj;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->j:Ljava/util/List;

    return-object p1
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/datalib/legacy/model/bj;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->k:Ljava/util/List;

    return-object p1
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;
    .locals 15

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a:Ljava/lang/String;

    :goto_0
    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->c:I

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->d:Z

    iget-boolean v4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->e:Z

    iget-boolean v5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->f:Z

    iget-object v7, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->g:Ljava/lang/String;

    if-nez v7, :cond_1

    const-string v7, ""

    :goto_1
    iget-object v8, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->h:Ljava/util/List;

    iget-object v9, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->i:Ljava/util/List;

    iget-object v10, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->j:Ljava/util/List;

    iget-object v11, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->k:Ljava/util/List;

    iget-object v12, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->l:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    iget-boolean v13, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->m:Z

    const/4 v14, 0x0

    invoke-direct/range {v0 .. v14}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;IZZZLjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;ZLcom/google/android/apps/youtube/datalib/legacy/model/bh;)V

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "_INTERNAL_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->g:Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(I)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->c:I

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->l:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->h:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->h:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$TrackingEventType;Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/bi;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$TrackingEventType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-object p0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->i:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->i:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->i:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->j:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->j:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->j:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->k:Ljava/util/List;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->k:Ljava/util/List;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->k:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->h:Ljava/util/List;

    return-object p0
.end method

.method public final a(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->d:Z

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->g:Ljava/lang/String;

    return-object p0
.end method

.method public final b(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->e:Z

    return-object p0
.end method

.method public synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    move-result-object v0

    return-object v0
.end method

.method public final c(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->f:Z

    return-object p0
.end method

.method public final d(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->m:Z

    return-object p0
.end method
