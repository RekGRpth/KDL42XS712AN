.class public final Lcom/google/android/apps/youtube/datalib/offline/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/offline/j;


# instance fields
.field private final b:Lcom/google/android/apps/youtube/common/database/d;

.field private final c:Landroid/util/SparseArray;

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:Lcom/google/android/apps/youtube/datalib/config/c;

.field private final f:Lcom/google/android/apps/youtube/datalib/offline/m;

.field private final g:Lcom/google/android/apps/youtube/common/e/b;

.field private final h:Lcom/google/android/apps/youtube/common/network/h;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/common/database/d;Ljava/util/Set;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/datalib/config/c;Lcom/google/android/apps/youtube/datalib/offline/m;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/common/network/h;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/database/d;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->d:Ljava/util/concurrent/Executor;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/config/c;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->e:Lcom/google/android/apps/youtube/datalib/config/c;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/offline/m;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->f:Lcom/google/android/apps/youtube/datalib/offline/m;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->g:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->h:Lcom/google/android/apps/youtube/common/network/h;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->c:Landroid/util/SparseArray;

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/offline/SendingStrategy;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->c:Landroid/util/SparseArray;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/offline/SendingStrategy;->a()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/datalib/offline/a;)Lcom/google/android/apps/youtube/datalib/offline/m;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->f:Lcom/google/android/apps/youtube/datalib/offline/m;

    return-object v0
.end method

.method private a(Lcom/google/android/apps/youtube/a/a/c;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/offline/e;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/datalib/offline/e;-><init>(Lcom/google/android/apps/youtube/datalib/offline/a;Lcom/google/android/apps/youtube/a/a/c;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/datalib/offline/a;Lcom/google/android/apps/youtube/a/a/c;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/offline/a;->a(Lcom/google/android/apps/youtube/a/a/c;)V

    return-void
.end method

.method private static a(Ljava/util/Map;Ljava/lang/String;)V
    .locals 1

    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/datalib/offline/a;Lcom/google/android/apps/youtube/a/a/c;Lcom/android/volley/VolleyError;)Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/c;->m()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->e:Lcom/google/android/apps/youtube/datalib/config/c;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/datalib/config/c;->f()I

    move-result v3

    if-ge v2, v3, :cond_6

    const/4 v2, 0x0

    instance-of v3, p2, Lcom/google/android/apps/youtube/datalib/offline/SendingStrategy$PermanentVolleyError;

    if-eqz v3, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_6

    :goto_1
    return v0

    :cond_0
    instance-of v3, p2, Lcom/android/volley/NetworkError;

    if-eqz v3, :cond_3

    check-cast p2, Lcom/android/volley/NetworkError;

    iget-object v2, p2, Lcom/android/volley/NetworkError;->networkResponse:Lcom/android/volley/j;

    :cond_1
    :goto_2
    if-eqz v2, :cond_5

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Status code of errored request is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v2, Lcom/android/volley/j;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget v3, v2, Lcom/android/volley/j;->a:I

    const/16 v4, 0x190

    if-eq v3, v4, :cond_2

    iget v2, v2, Lcom/android/volley/j;->a:I

    const/16 v3, 0x193

    if-ne v2, v3, :cond_4

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    instance-of v3, p2, Lcom/android/volley/ServerError;

    if-eqz v3, :cond_1

    check-cast p2, Lcom/android/volley/ServerError;

    iget-object v2, p2, Lcom/android/volley/ServerError;->networkResponse:Lcom/android/volley/j;

    goto :goto_2

    :cond_4
    move v2, v1

    goto :goto_0

    :cond_5
    const-string v2, "Network response was not present, request is retryable."

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    move v2, v1

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/datalib/offline/a;)Lcom/google/android/apps/youtube/common/database/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->b:Lcom/google/android/apps/youtube/common/database/d;

    return-object v0
.end method

.method private b()Ljava/util/List;
    .locals 6

    const/4 v0, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/database/d;->d()Lcom/google/android/apps/youtube/common/database/e;

    move-result-object v4

    move v1, v0

    move v2, v0

    :goto_0
    invoke-interface {v4}, Lcom/google/android/apps/youtube/common/database/e;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Lcom/google/android/apps/youtube/common/database/e;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/c;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->e:Lcom/google/android/apps/youtube/datalib/config/c;

    invoke-interface {v5}, Lcom/google/android/apps/youtube/datalib/config/c;->c()I

    move-result v5

    if-ge v2, v5, :cond_0

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    move v0, v1

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {v4}, Lcom/google/android/apps/youtube/common/database/e;->a()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->f:Lcom/google/android/apps/youtube/datalib/offline/m;

    sub-int v1, v2, v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/offline/m;->b(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/database/d;->a()V

    :try_start_0
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/c;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/c;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/common/database/d;->a(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/database/d;->b()V

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/database/d;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/database/d;->b()V

    return-object v3
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/datalib/offline/a;)Lcom/google/android/apps/youtube/common/network/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->h:Lcom/google/android/apps/youtube/common/network/h;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/datalib/offline/a;)Lcom/google/android/apps/youtube/common/e/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->g:Lcom/google/android/apps/youtube/common/e/b;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/youtube/datalib/offline/a;)Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->d:Ljava/util/concurrent/Executor;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 15

    const/4 v4, 0x1

    const/4 v3, 0x0

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/database/d;->d()Lcom/google/android/apps/youtube/common/database/e;

    move-result-object v10

    move v2, v4

    move v5, v3

    :goto_0
    invoke-interface {v10}, Lcom/google/android/apps/youtube/common/database/e;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v10}, Lcom/google/android/apps/youtube/common/database/e;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/a/a/c;

    add-int/lit8 v6, v5, 0x1

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->f:Lcom/google/android/apps/youtube/datalib/offline/m;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v11, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->g:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v11}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v11

    invoke-virtual {v5, v11, v12}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v11

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/a/a/c;->i()J

    move-result-wide v13

    invoke-virtual {v5, v13, v14}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v13

    sub-long/2addr v11, v13

    invoke-virtual {v2, v11, v12}, Lcom/google/android/apps/youtube/datalib/offline/m;->a(J)V

    move v5, v3

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/a/a/c;->b()I

    move-result v11

    invoke-virtual {v2, v11}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/datalib/offline/SendingStrategy;

    if-eqz v2, :cond_0

    invoke-interface {v2, v1}, Lcom/google/android/apps/youtube/datalib/offline/SendingStrategy;->a(Lcom/google/android/apps/youtube/a/a/c;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/youtube/a/a/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/a/a/c;->k()Ljava/lang/String;

    move-result-object v1

    invoke-static {v9, v1}, Lcom/google/android/apps/youtube/datalib/offline/a;->a(Ljava/util/Map;Ljava/lang/String;)V

    move v2, v5

    move v5, v6

    goto :goto_0

    :cond_1
    new-instance v2, Lcom/google/android/apps/youtube/datalib/offline/f;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/a/a/c;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/a/a/c;->k()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v11, v1}, Lcom/google/android/apps/youtube/datalib/offline/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v5

    move v5, v6

    goto :goto_0

    :cond_2
    invoke-interface {v10}, Lcom/google/android/apps/youtube/common/database/e;->a()V

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->e:Lcom/google/android/apps/youtube/datalib/config/c;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/datalib/config/c;->a()I

    move-result v2

    if-le v1, v2, :cond_3

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->e:Lcom/google/android/apps/youtube/datalib/config/c;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/datalib/config/c;->a()I

    move-result v2

    sub-int v6, v1, v2

    move v2, v3

    :goto_2
    if-ge v2, v6, :cond_3

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/datalib/offline/f;

    iget-object v10, v1, Lcom/google/android/apps/youtube/datalib/offline/f;->a:Ljava/lang/String;

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, v1, Lcom/google/android/apps/youtube/datalib/offline/f;->b:Ljava/lang/String;

    invoke-static {v9, v1}, Lcom/google/android/apps/youtube/datalib/offline/a;->a(Ljava/util/Map;Ljava/lang/String;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->f:Lcom/google/android/apps/youtube/datalib/offline/m;

    invoke-virtual {v1, v9}, Lcom/google/android/apps/youtube/datalib/offline/m;->a(Ljava/util/Map;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->f:Lcom/google/android/apps/youtube/datalib/offline/m;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/youtube/datalib/offline/m;->a(I)V

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/database/d;->a()V

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-interface {v5, v1}, Lcom/google/android/apps/youtube/common/database/d;->a(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_4
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/database/d;->c()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->b:Lcom/google/android/apps/youtube/common/database/d;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/database/d;->b()V

    :cond_5
    move v1, v3

    :goto_4
    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->c:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->c:Landroid/util/SparseArray;

    invoke-virtual {v5, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/offline/a;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/youtube/a/a/c;

    move-object v2, v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->e:Lcom/google/android/apps/youtube/datalib/config/c;

    invoke-interface {v6}, Lcom/google/android/apps/youtube/datalib/config/c;->g()I

    move-result v6

    int-to-long v6, v6

    invoke-static {v6, v7}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/a/a/c;->m()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/math/BigInteger;->pow(I)Ljava/math/BigInteger;

    move-result-object v6

    invoke-virtual {v6}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/a/a/c;->n()J

    move-result-wide v8

    add-long/2addr v6, v8

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->g:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-lez v1, :cond_7

    move v1, v4

    :goto_6
    if-eqz v1, :cond_8

    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/datalib/offline/a;->a(Lcom/google/android/apps/youtube/a/a/c;)V

    goto :goto_5

    :cond_7
    move v1, v3

    goto :goto_6

    :cond_8
    new-instance v6, Lcom/google/android/apps/youtube/datalib/offline/c;

    invoke-direct {v6, p0, v2}, Lcom/google/android/apps/youtube/datalib/offline/c;-><init>(Lcom/google/android/apps/youtube/datalib/offline/a;Lcom/google/android/apps/youtube/a/a/c;)V

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/a/a/c;->b()I

    move-result v1

    iget-object v7, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->c:Landroid/util/SparseArray;

    invoke-virtual {v7, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/datalib/offline/SendingStrategy;

    invoke-interface {v1, v2, v6}, Lcom/google/android/apps/youtube/datalib/offline/SendingStrategy;->a(Lcom/google/android/apps/youtube/a/a/c;Lcom/google/android/apps/youtube/datalib/a/l;)V

    goto :goto_5

    :cond_9
    move v1, v3

    :goto_7
    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->c:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->c:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_a
    monitor-exit p0

    return-void

    :cond_b
    move v5, v2

    goto/16 :goto_1
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/youtube/datalib/offline/l;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/a;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/offline/b;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/datalib/offline/b;-><init>(Lcom/google/android/apps/youtube/datalib/offline/a;Lcom/google/android/apps/youtube/datalib/offline/l;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
