.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/CharSequence;

.field private final b:Ljava/lang/CharSequence;

.field private final c:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;


# direct methods
.method private constructor <init>(Lcom/google/a/a/a/a/mg;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/mg;->b:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;->a:Ljava/lang/CharSequence;

    iget-object v0, p1, Lcom/google/a/a/a/a/mg;->c:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;->b:Ljava/lang/CharSequence;

    iget v0, p1, Lcom/google/a/a/a/a/mg;->d:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid offlineability.format.format_type: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Lcom/google/a/a/a/a/mg;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;->AMODO_ONLY:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;->SD:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;->HD:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method synthetic constructor <init>(Lcom/google/a/a/a/a/mg;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;-><init>(Lcom/google/a/a/a/a/mg;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;->a:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;->b:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format;->c:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability$Format$Type;

    return-object v0
.end method
