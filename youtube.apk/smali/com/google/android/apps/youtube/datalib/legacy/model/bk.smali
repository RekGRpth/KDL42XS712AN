.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/bk;
.super Lcom/google/android/apps/youtube/datalib/legacy/a/b;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/a/b;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/a/b;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final a(Lorg/json/JSONObject;I)Lcom/google/android/apps/youtube/datalib/legacy/a/a;
    .locals 16

    const/4 v1, 0x1

    move/from16 v0, p2

    if-eq v0, v1, :cond_0

    new-instance v1, Lorg/json/JSONException;

    const-string v2, "Unsupported version"

    invoke-direct {v1, v2}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    const-string v2, "offsetType"

    const-class v3, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    const-string v3, "offsetValue"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    const-string v4, "isLinearAdAllowed"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    const-string v5, "isNonlinearAdAllowed"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    const-string v6, "isDisplayAdAllowed"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    const-string v7, "adBreakId"

    move-object/from16 v0, p1

    invoke-static {v0, v7}, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "originalVideoId"

    move-object/from16 v0, p1

    invoke-static {v0, v8}, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "ads"

    const-class v10, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-object/from16 v0, p1

    invoke-static {v0, v9, v10}, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v9

    const-string v10, "startEvents"

    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    const-string v11, "endEvents"

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    const-string v12, "errorEvents"

    move-object/from16 v0, p1

    invoke-static {v0, v12}, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v12

    const-string v13, "trackingDecoration"

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->b(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/a/a;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    const-string v14, "isForOffline"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v14

    const/4 v15, 0x0

    invoke-direct/range {v1 .. v15}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;IZZZLjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;ZLcom/google/android/apps/youtube/datalib/legacy/model/bh;)V

    return-object v1
.end method

.method protected final a(Lorg/json/JSONObject;)V
    .locals 2

    const-string v0, "offsetType"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOffsetType()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Enum;)V

    const-string v0, "offsetValue"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOffsetValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v0, "isLinearAdAllowed"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isLinearAdAllowed()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "isNonlinearAdAllowed"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isNonlinearAdAllowed()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "isDisplayAdAllowed"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isDisplayAdAllowed()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "adBreakId"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAdBreakId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "originalVideoId"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "ads"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAds()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "startEvents"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getBreakStartPingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "endEvents"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getBreakEndPingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "errorEvents"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getErrorPingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->b(Lorg/json/JSONObject;Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "trackingDecoration"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getTrackingDecoration()Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a(Lorg/json/JSONObject;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/a/a;)V

    const-string v0, "isForOffline"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isForOffline()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    return-void
.end method
