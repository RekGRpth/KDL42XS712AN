.class public final Lcom/google/android/apps/youtube/datalib/distiller/h;
.super Lcom/android/volley/toolbox/p;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/datalib/distiller/i;

.field private final b:Lcom/google/android/apps/youtube/core/identity/ak;

.field private final c:Lcom/google/android/apps/youtube/datalib/c/f;

.field private final d:Ljava/util/List;

.field private final e:Lcom/google/android/apps/youtube/datalib/config/a;

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILcom/google/android/apps/youtube/datalib/distiller/i;Lcom/google/android/apps/youtube/datalib/a/l;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/datalib/c/f;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/config/a;Ljava/lang/String;)V
    .locals 6

    const-string v2, ""

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/distiller/i;->e()Lorg/json/JSONObject;

    move-result-object v3

    move-object v0, p0

    move v1, p1

    move-object v4, p3

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/android/volley/toolbox/p;-><init>(ILjava/lang/String;Lorg/json/JSONObject;Lcom/android/volley/o;Lcom/android/volley/n;)V

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->a:Lcom/google/android/apps/youtube/datalib/distiller/i;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/ak;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->b:Lcom/google/android/apps/youtube/core/identity/ak;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/c/f;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->c:Lcom/google/android/apps/youtube/datalib/c/f;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->d:Ljava/util/List;

    invoke-static {p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/config/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->e:Lcom/google/android/apps/youtube/datalib/config/a;

    iput-object p8, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->f:Ljava/lang/String;

    return-void
.end method

.method private a(Lorg/json/JSONObject;)V
    .locals 6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    :try_start_0
    const-string v0, "curl "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/distiller/h;->h()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "-H \""

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\" "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Lcom/android/volley/AuthFailureError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "Curl command line not available"

    invoke-static {v2, v0}, Lcom/google/android/apps/youtube/common/L;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    const-string v0, "-H \"Content-Type:application/json\" "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "-d \""

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, "\" "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/distiller/h;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "Curl commandline"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/android/volley/VolleyError;)Lcom/android/volley/VolleyError;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->e:Lcom/google/android/apps/youtube/datalib/config/a;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/config/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->a:Lcom/google/android/apps/youtube/datalib/distiller/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/distiller/i;->e()Lorg/json/JSONObject;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/datalib/distiller/h;->a(Lorg/json/JSONObject;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/android/volley/toolbox/p;->a(Lcom/android/volley/VolleyError;)Lcom/android/volley/VolleyError;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/android/volley/j;)Lcom/android/volley/m;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->e:Lcom/google/android/apps/youtube/datalib/config/a;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/config/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->a:Lcom/google/android/apps/youtube/datalib/distiller/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/distiller/i;->e()Lorg/json/JSONObject;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/datalib/distiller/h;->a(Lorg/json/JSONObject;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/android/volley/toolbox/p;->a(Lcom/android/volley/j;)Lcom/android/volley/m;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x5f

    const/16 v2, 0x2d

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->e:Lcom/google/android/apps/youtube/datalib/config/a;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/datalib/config/a;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->e:Lcom/google/android/apps/youtube/datalib/config/a;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/datalib/config/a;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->a:Lcom/google/android/apps/youtube/datalib/distiller/i;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/distiller/i;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "key"

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "firstPartyProperty"

    const-string v3, "youTube"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "language"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->a:Lcom/google/android/apps/youtube/datalib/distiller/i;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/distiller/i;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->b:Lcom/google/android/apps/youtube/core/identity/ak;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/identity/ak;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "onBehalfOf"

    invoke-virtual {v2, v1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/util/Map;
    .locals 4

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->c:Lcom/google/android/apps/youtube/datalib/c/f;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/distiller/h;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/distiller/h;->l()[B

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/c/f;->a(Ljava/util/Map;Ljava/lang/String;[B)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/h;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/a/e;

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/datalib/a/e;->a(Ljava/util/Map;)V

    goto :goto_0

    :cond_0
    return-object v1
.end method
