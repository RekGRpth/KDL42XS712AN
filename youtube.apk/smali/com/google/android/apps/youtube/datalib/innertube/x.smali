.class public abstract Lcom/google/android/apps/youtube/datalib/innertube/x;
.super Lcom/google/android/apps/youtube/datalib/innertube/b;
.source "SourceFile"


# instance fields
.field protected c:Ljava/lang/String;

.field protected d:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/b;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/x;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/x;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/x;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/x;->c:Ljava/lang/String;

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/x;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/x;->d:Ljava/lang/String;

    return-object p0
.end method

.method protected final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/x;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/x;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final g()Lcom/google/a/a/a/a/jd;
    .locals 2

    new-instance v0, Lcom/google/a/a/a/a/jd;

    invoke-direct {v0}, Lcom/google/a/a/a/a/jd;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/x;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/jd;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/x;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/jd;->b:Ljava/lang/String;

    return-object v0
.end method
