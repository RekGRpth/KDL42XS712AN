.class public final Lcom/google/android/apps/youtube/datalib/innertube/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/a/a/a/a/kz;Lcom/google/a/a/a/a/kz;Z)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    if-eqz p0, :cond_2

    if-nez p1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    if-nez p0, :cond_3

    if-nez p1, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/a/a/a/a/kz;->r:Lcom/google/a/a/a/a/aa;

    if-eqz v2, :cond_4

    iget-object v2, p1, Lcom/google/a/a/a/a/kz;->r:Lcom/google/a/a/a/a/aa;

    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/google/a/a/a/a/kz;->r:Lcom/google/a/a/a/a/aa;

    iget-object v0, v0, Lcom/google/a/a/a/a/aa;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/a/a/a/a/kz;->r:Lcom/google/a/a/a/a/aa;

    iget-object v1, v1, Lcom/google/a/a/a/a/aa;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/a/a/a/a/kz;->c:Lcom/google/a/a/a/a/am;

    if-eqz v2, :cond_6

    iget-object v2, p1, Lcom/google/a/a/a/a/kz;->c:Lcom/google/a/a/a/a/am;

    if-eqz v2, :cond_6

    if-eqz p2, :cond_5

    iget-object v1, p0, Lcom/google/a/a/a/a/kz;->c:Lcom/google/a/a/a/a/am;

    iget-object v1, v1, Lcom/google/a/a/a/a/am;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/a/a/a/a/kz;->c:Lcom/google/a/a/a/a/am;

    iget-object v2, v2, Lcom/google/a/a/a/a/am;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_5
    iget-object v0, p0, Lcom/google/a/a/a/a/kz;->c:Lcom/google/a/a/a/a/am;

    iget-object v0, v0, Lcom/google/a/a/a/a/am;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/a/a/a/a/kz;->c:Lcom/google/a/a/a/a/am;

    iget-object v1, v1, Lcom/google/a/a/a/a/am;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/a/a/a/a/kz;->m:Lcom/google/a/a/a/a/bj;

    if-eqz v2, :cond_7

    iget-object v2, p1, Lcom/google/a/a/a/a/kz;->m:Lcom/google/a/a/a/a/bj;

    if-eqz v2, :cond_7

    iget-object v0, p0, Lcom/google/a/a/a/a/kz;->m:Lcom/google/a/a/a/a/bj;

    iget-object v0, v0, Lcom/google/a/a/a/a/bj;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/a/a/a/a/kz;->m:Lcom/google/a/a/a/a/bj;

    iget-object v1, v1, Lcom/google/a/a/a/a/bj;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lcom/google/a/a/a/a/kz;->e:Lcom/google/a/a/a/a/fw;

    if-eqz v2, :cond_8

    iget-object v2, p1, Lcom/google/a/a/a/a/kz;->e:Lcom/google/a/a/a/a/fw;

    if-eqz v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lcom/google/a/a/a/a/kz;->p:Lcom/google/a/a/a/a/jz;

    if-eqz v2, :cond_9

    iget-object v2, p1, Lcom/google/a/a/a/a/kz;->p:Lcom/google/a/a/a/a/jz;

    if-eqz v2, :cond_9

    iget-object v0, p0, Lcom/google/a/a/a/a/kz;->p:Lcom/google/a/a/a/a/jz;

    iget-object v0, v0, Lcom/google/a/a/a/a/jz;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/a/a/a/a/kz;->p:Lcom/google/a/a/a/a/jz;

    iget-object v1, v1, Lcom/google/a/a/a/a/jz;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0

    :cond_9
    iget-object v2, p0, Lcom/google/a/a/a/a/kz;->s:Lcom/google/a/a/a/a/ls;

    if-eqz v2, :cond_a

    iget-object v2, p1, Lcom/google/a/a/a/a/kz;->s:Lcom/google/a/a/a/a/ls;

    if-eqz v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Lcom/google/a/a/a/a/kz;->g:Lcom/google/a/a/a/a/qf;

    if-eqz v2, :cond_c

    iget-object v2, p1, Lcom/google/a/a/a/a/kz;->g:Lcom/google/a/a/a/a/qf;

    if-eqz v2, :cond_c

    if-eqz p2, :cond_b

    iget-object v1, p0, Lcom/google/a/a/a/a/kz;->g:Lcom/google/a/a/a/a/qf;

    iget-object v1, v1, Lcom/google/a/a/a/a/qf;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/a/a/a/a/kz;->g:Lcom/google/a/a/a/a/qf;

    iget-object v2, v2, Lcom/google/a/a/a/a/qf;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_b
    iget-object v0, p0, Lcom/google/a/a/a/a/kz;->g:Lcom/google/a/a/a/a/qf;

    iget-object v0, v0, Lcom/google/a/a/a/a/qf;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/a/a/a/a/kz;->g:Lcom/google/a/a/a/a/qf;

    iget-object v1, v1, Lcom/google/a/a/a/a/qf;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto/16 :goto_0

    :cond_c
    iget-object v2, p0, Lcom/google/a/a/a/a/kz;->k:Lcom/google/a/a/a/a/tu;

    if-eqz v2, :cond_d

    iget-object v2, p1, Lcom/google/a/a/a/a/kz;->k:Lcom/google/a/a/a/a/tu;

    if-eqz v2, :cond_d

    iget-object v0, p0, Lcom/google/a/a/a/a/kz;->k:Lcom/google/a/a/a/a/tu;

    iget-object v0, v0, Lcom/google/a/a/a/a/tu;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/a/a/a/a/kz;->k:Lcom/google/a/a/a/a/tu;

    iget-object v1, v1, Lcom/google/a/a/a/a/tu;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    goto/16 :goto_0

    :cond_d
    iget-object v2, p0, Lcom/google/a/a/a/a/kz;->i:Lcom/google/a/a/a/a/wb;

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/google/a/a/a/a/kz;->i:Lcom/google/a/a/a/a/wb;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/a/a/a/a/kz;->i:Lcom/google/a/a/a/a/wb;

    iget-object v3, p1, Lcom/google/a/a/a/a/kz;->i:Lcom/google/a/a/a/a/wb;

    if-eqz p2, :cond_e

    iget-object v4, v2, Lcom/google/a/a/a/a/wb;->e:Ljava/lang/String;

    iget-object v5, v3, Lcom/google/a/a/a/a/wb;->e:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v2, Lcom/google/a/a/a/a/wb;->f:Ljava/lang/String;

    iget-object v5, v3, Lcom/google/a/a/a/a/wb;->f:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v2, Lcom/google/a/a/a/a/wb;->j:Ljava/lang/String;

    iget-object v5, v3, Lcom/google/a/a/a/a/wb;->j:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, v2, Lcom/google/a/a/a/a/wb;->i:F

    iget v5, v3, Lcom/google/a/a/a/a/wb;->i:F

    invoke-static {v4, v5}, Ljava/lang/Float;->compare(FF)I

    move-result v4

    if-nez v4, :cond_1

    iget v4, v2, Lcom/google/a/a/a/a/wb;->h:F

    iget v5, v3, Lcom/google/a/a/a/a/wb;->h:F

    invoke-static {v4, v5}, Ljava/lang/Float;->compare(FF)I

    move-result v4

    if-nez v4, :cond_1

    :cond_e
    iget-boolean v4, v2, Lcom/google/a/a/a/a/wb;->g:Z

    iget-boolean v5, v3, Lcom/google/a/a/a/a/wb;->g:Z

    if-ne v4, v5, :cond_1

    iget v4, v2, Lcom/google/a/a/a/a/wb;->d:I

    iget v5, v3, Lcom/google/a/a/a/a/wb;->d:I

    if-ne v4, v5, :cond_1

    iget-object v4, v2, Lcom/google/a/a/a/a/wb;->b:Ljava/lang/String;

    iget-object v5, v3, Lcom/google/a/a/a/a/wb;->b:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v2, v2, Lcom/google/a/a/a/a/wb;->c:Ljava/lang/String;

    iget-object v3, v3, Lcom/google/a/a/a/a/wb;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto/16 :goto_0
.end method
