.class public Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/apps/youtube/datalib/legacy/a/a;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final appName:Ljava/lang/String;

.field private final hasRating:Z

.field private final icon:Landroid/net/Uri;

.field private final price:Ljava/lang/String;

.field private final rating:F

.field private final ratingImageUri:Landroid/net/Uri;

.field private final reviewCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/k;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/k;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;ZFLandroid/net/Uri;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->appName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->icon:Landroid/net/Uri;

    iput-object p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->price:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->hasRating:Z

    iput p5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->rating:F

    iput-object p6, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->ratingImageUri:Landroid/net/Uri;

    iput p7, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->reviewCount:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getAppName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getAppName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getIcon()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getIcon()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getPrice()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getPrice()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getRating()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getRating()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getRatingImageUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getRatingImageUri()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getReviewCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getReviewCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->appName:Ljava/lang/String;

    return-object v0
.end method

.method public bridge synthetic getConverter()Lcom/google/android/apps/youtube/datalib/legacy/a/b;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getConverter()Lcom/google/android/apps/youtube/datalib/legacy/model/m;

    move-result-object v0

    return-object v0
.end method

.method public getConverter()Lcom/google/android/apps/youtube/datalib/legacy/model/m;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/m;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/m;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;)V

    return-object v0
.end method

.method public getIcon()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->icon:Landroid/net/Uri;

    return-object v0
.end method

.method public getPrice()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->price:Ljava/lang/String;

    return-object v0
.end method

.method public getRating()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->rating:F

    return v0
.end method

.method public getRatingImageUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->ratingImageUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getReviewCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->reviewCount:I

    return v0
.end method

.method public hasRating()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->hasRating:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getAppName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getIcon()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getPrice()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->hasRating()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getRating()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getRatingImageUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getReviewCount()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method
