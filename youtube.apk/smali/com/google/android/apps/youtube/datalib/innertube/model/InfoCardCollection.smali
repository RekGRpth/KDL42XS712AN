.class public Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final infoCardCollectionProto:Lcom/google/a/a/a/a/hv;

.field private infoCards:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/n;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/n;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/a/a/a/a/hv;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/hv;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->infoCardCollectionProto:Lcom/google/a/a/a/a/hv;

    return-void
.end method

.method public static fromByteArray([B)Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;
    .locals 2

    :try_start_0
    new-instance v0, Lcom/google/a/a/a/a/hv;

    invoke-direct {v0}, Lcom/google/a/a/a/a/hv;-><init>()V

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/hv;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;-><init>(Lcom/google/a/a/a/a/hv;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getInfoCards()Ljava/util/List;
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->infoCards:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->infoCards:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->infoCardCollectionProto:Lcom/google/a/a/a/a/hv;

    iget-object v1, v0, Lcom/google/a/a/a/a/hv;->b:[Lcom/google/a/a/a/a/ic;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->infoCards:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/m;

    iget-object v3, v3, Lcom/google/a/a/a/a/ic;->b:Lcom/google/a/a/a/a/ia;

    invoke-direct {v5, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;-><init>(Lcom/google/a/a/a/a/ia;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->infoCards:Ljava/util/List;

    return-object v0
.end method

.method public toByteArray()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->infoCardCollectionProto:Lcom/google/a/a/a/a/hv;

    invoke-static {v0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;->infoCardCollectionProto:Lcom/google/a/a/a/a/hv;

    invoke-static {v0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method
