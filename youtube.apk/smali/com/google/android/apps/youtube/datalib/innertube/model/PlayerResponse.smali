.class public Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private captionsTracksUri:Landroid/net/Uri;

.field private infoCardCollectionRenderer:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

.field private offlineState:Lcom/google/android/apps/youtube/datalib/innertube/model/v;

.field private playabilityStatus:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

.field private playbackTracking:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

.field private playerConfig:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

.field private final playerResponseProto:Lcom/google/a/a/a/a/nw;

.field private final streamingDataTimestampElapsedMillis:J

.field private videoStreamingData:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/y;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/y;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/a/a/a/a/nw;J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/nw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iput-wide p2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->streamingDataTimestampElapsedMillis:J

    return-void
.end method

.method public static fromBlob([BJ)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    new-instance v2, Lcom/google/a/a/a/a/nw;

    invoke-direct {v2}, Lcom/google/a/a/a/a/nw;-><init>()V

    invoke-static {v2, p0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;-><init>(Lcom/google/a/a/a/a/nw;J)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private getFormatsSparseArrayByItag([Lcom/google/a/a/a/a/fj;)Landroid/util/SparseArray;
    .locals 4

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    aget-object v2, p1, v0

    iget v3, v2, Lcom/google/a/a/a/a/fj;->b:I

    invoke-virtual {v1, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static getVideoIdFromProto(Lcom/google/a/a/a/a/nw;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    iget-object v0, v0, Lcom/google/a/a/a/a/uh;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private toValuesArray(Landroid/util/SparseArray;)[Lcom/google/a/a/a/a/fj;
    .locals 3

    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    new-array v2, v0, [Lcom/google/a/a/a/a/fj;

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/fj;

    aput-object v0, v2, v1

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object v2
.end method


# virtual methods
.method public cloneAndMergeOfflineStreams(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    .locals 7

    const-wide/16 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->d:Lcom/google/a/a/a/a/sb;

    if-eqz v0, :cond_0

    iget-wide v5, v0, Lcom/google/a/a/a/a/sb;->b:J

    :cond_0
    iget-wide v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->streamingDataTimestampElapsedMillis:J

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->cloneAndMergeOfflineStreams(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;JJ)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    return-object v0
.end method

.method public cloneAndMergeOfflineStreams(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;JJ)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    .locals 6

    const-wide/16 v3, 0x0

    new-instance v0, Lcom/google/a/a/a/a/nw;

    invoke-direct {v0}, Lcom/google/a/a/a/a/nw;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    invoke-static {v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    iget-object v1, v0, Lcom/google/a/a/a/a/nw;->d:Lcom/google/a/a/a/a/sb;

    if-eqz v1, :cond_2

    cmp-long v2, p5, v3

    if-lez v2, :cond_3

    iput-wide p5, v1, Lcom/google/a/a/a/a/sb;->b:J

    :goto_0
    iget-object v2, v1, Lcom/google/a/a/a/a/sb;->d:[Lcom/google/a/a/a/a/fj;

    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getFormatsSparseArrayByItag([Lcom/google/a/a/a/a/fj;)Landroid/util/SparseArray;

    move-result-object v2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->isAdaptive()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getFormatStreamProto()Lcom/google/a/a/a/a/fj;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    :goto_1
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v3

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getFormatStreamProto()Lcom/google/a/a/a/a/fj;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->toValuesArray(Landroid/util/SparseArray;)[Lcom/google/a/a/a/a/fj;

    move-result-object v2

    iput-object v2, v1, Lcom/google/a/a/a/a/sb;->d:[Lcom/google/a/a/a/a/fj;

    :cond_2
    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-direct {v1, v0, p3, p4}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;-><init>(Lcom/google/a/a/a/a/nw;J)V

    return-object v1

    :cond_3
    iput-wide v3, v1, Lcom/google/a/a/a/a/sb;->b:J

    goto :goto_0

    :cond_4
    iget-object v3, v1, Lcom/google/a/a/a/a/sb;->c:[Lcom/google/a/a/a/a/fj;

    invoke-direct {p0, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getFormatsSparseArrayByItag([Lcom/google/a/a/a/a/fj;)Landroid/util/SparseArray;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getFormatStreamProto()Lcom/google/a/a/a/a/fj;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-direct {p0, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->toValuesArray(Landroid/util/SparseArray;)[Lcom/google/a/a/a/a/fj;

    move-result-object v3

    iput-object v3, v1, Lcom/google/a/a/a/a/sb;->c:[Lcom/google/a/a/a/a/fj;

    goto :goto_1
.end method

.method public cloneAndReplaceOfflineState(Lcom/google/android/apps/youtube/datalib/innertube/model/v;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    .locals 4

    new-instance v0, Lcom/google/a/a/a/a/nw;

    invoke-direct {v0}, Lcom/google/a/a/a/a/nw;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    invoke-static {v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/v;->a()Lcom/google/a/a/a/a/md;

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/nw;->l:Lcom/google/a/a/a/a/md;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    iget-wide v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->streamingDataTimestampElapsedMillis:J

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;-><init>(Lcom/google/a/a/a/a/nw;J)V

    return-object v1
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public getCaptionTracksUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->captionsTracksUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->i:Lcom/google/a/a/a/a/bh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->i:Lcom/google/a/a/a/a/bh;

    iget-object v0, v0, Lcom/google/a/a/a/a/bh;->b:Lcom/google/a/a/a/a/ng;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->i:Lcom/google/a/a/a/a/bh;

    iget-object v0, v0, Lcom/google/a/a/a/a/bh;->b:Lcom/google/a/a/a/a/ng;

    iget-object v0, v0, Lcom/google/a/a/a/a/ng;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->captionsTracksUri:Landroid/net/Uri;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->captionsTracksUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getExpandedAnnotation()Lcom/google/a/a/a/a/ne;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->k:Lcom/google/a/a/a/a/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->k:Lcom/google/a/a/a/a/v;

    iget-object v0, v0, Lcom/google/a/a/a/a/v;->b:Lcom/google/a/a/a/a/ne;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHeartbeatParams()Lcom/google/a/a/a/a/gr;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->f:Lcom/google/a/a/a/a/gr;

    return-object v0
.end method

.method public getInfoCardCollection()Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->infoCardCollectionRenderer:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->r:Lcom/google/a/a/a/a/ig;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->r:Lcom/google/a/a/a/a/ig;

    iget-object v0, v0, Lcom/google/a/a/a/a/ig;->b:Lcom/google/a/a/a/a/hv;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v1, v1, Lcom/google/a/a/a/a/nw;->r:Lcom/google/a/a/a/a/ig;

    iget-object v1, v1, Lcom/google/a/a/a/a/ig;->b:Lcom/google/a/a/a/a/hv;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;-><init>(Lcom/google/a/a/a/a/hv;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->infoCardCollectionRenderer:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->infoCardCollectionRenderer:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    return-object v0
.end method

.method public getInstreamAdPlayerResponseMap()Ljava/util/Map;
    .locals 8

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v2, v0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    iget-object v5, v4, Lcom/google/a/a/a/a/nc;->e:Lcom/google/a/a/a/a/nk;

    if-eqz v5, :cond_0

    iget-object v5, v4, Lcom/google/a/a/a/a/nc;->e:Lcom/google/a/a/a/a/nk;

    iget-object v5, v5, Lcom/google/a/a/a/a/nk;->b:Lcom/google/a/a/a/a/nw;

    if-eqz v5, :cond_0

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    iget-object v4, v4, Lcom/google/a/a/a/a/nc;->e:Lcom/google/a/a/a/a/nk;

    iget-object v4, v4, Lcom/google/a/a/a/a/nk;->b:Lcom/google/a/a/a/a/nw;

    iget-wide v6, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->streamingDataTimestampElapsedMillis:J

    invoke-direct {v5, v4, v6, v7}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;-><init>(Lcom/google/a/a/a/a/nw;J)V

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_2
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_1
.end method

.method public getLengthSeconds()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    iget-wide v0, v0, Lcom/google/a/a/a/a/uh;->d:J

    long-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOfflineState()Lcom/google/android/apps/youtube/datalib/innertube/model/v;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->offlineState:Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->l:Lcom/google/a/a/a/a/md;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v1, v1, Lcom/google/a/a/a/a/nw;->l:Lcom/google/a/a/a/a/md;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/v;-><init>(Lcom/google/a/a/a/a/md;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->offlineState:Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->offlineState:Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    return-object v0
.end method

.method public getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playabilityStatus:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->c:Lcom/google/a/a/a/a/mw;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v1, v1, Lcom/google/a/a/a/a/nw;->c:Lcom/google/a/a/a/a/mw;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->isForOffline()Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;-><init>(Lcom/google/a/a/a/a/mw;Z)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playabilityStatus:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playabilityStatus:Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    return-object v0
.end method

.method public getPlaybackTracking()Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playbackTracking:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v1, v1, Lcom/google/a/a/a/a/nw;->h:Lcom/google/a/a/a/a/nb;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;-><init>(Lcom/google/a/a/a/a/nb;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playbackTracking:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playbackTracking:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    return-object v0
.end method

.method public getPlayerConfig(Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerConfig:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->m:Lcom/google/a/a/a/a/nh;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v1, v1, Lcom/google/a/a/a/a/nw;->m:Lcom/google/a/a/a/a/nh;

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;-><init>(Lcom/google/a/a/a/a/nh;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerConfig:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerConfig:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    new-instance v1, Lcom/google/a/a/a/a/nh;

    invoke-direct {v1}, Lcom/google/a/a/a/a/nh;-><init>()V

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;-><init>(Lcom/google/a/a/a/a/nh;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig$ExoPlayerActivationType;)V

    goto :goto_0
.end method

.method public getThumbnailDetails()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    iget-object v0, v0, Lcom/google/a/a/a/a/uh;->l:Lcom/google/a/a/a/a/sx;

    :goto_0
    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;-><init>(Lcom/google/a/a/a/a/sx;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    iget-object v0, v0, Lcom/google/a/a/a/a/uh;->c:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getVastProto()Lcom/google/a/a/a/a/tz;
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v1, v0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    iget-object v4, v3, Lcom/google/a/a/a/a/nc;->f:Lcom/google/a/a/a/a/ua;

    if-eqz v4, :cond_0

    iget-object v0, v3, Lcom/google/a/a/a/a/nc;->f:Lcom/google/a/a/a/a/ua;

    iget-object v0, v0, Lcom/google/a/a/a/a/ua;->b:Lcom/google/a/a/a/a/tz;

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getVideoId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoIdFromProto(Lcom/google/a/a/a/a/nw;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVideoStreamingData()Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;
    .locals 7

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->videoStreamingData:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->d:Lcom/google/a/a/a/a/sb;

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v2, v2, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    iget-wide v0, v0, Lcom/google/a/a/a/a/uh;->d:J

    move-wide v2, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->d:Lcom/google/a/a/a/a/sb;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iget-wide v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->streamingDataTimestampElapsedMillis:J

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->isLive()Z

    move-result v6

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->create(Lcom/google/a/a/a/a/sb;Ljava/lang/String;JJZ)Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->videoStreamingData:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->videoStreamingData:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    return-object v0

    :cond_1
    move-wide v2, v0

    goto :goto_0
.end method

.method public getVmapXml()Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v1, v0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    iget-object v4, v3, Lcom/google/a/a/a/a/nc;->b:Lcom/google/a/a/a/a/vh;

    if-eqz v4, :cond_0

    iget-object v0, v3, Lcom/google/a/a/a/a/nc;->b:Lcom/google/a/a/a/a/vh;

    iget-object v0, v0, Lcom/google/a/a/a/a/vh;->b:Ljava/lang/String;

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x13

    mul-int/lit8 v1, v0, 0x13

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getPlayabilityStatus()Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public isForOffline()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->getOfflineState()Lcom/google/android/apps/youtube/datalib/innertube/model/v;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLive()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    iget-object v0, v0, Lcom/google/a/a/a/a/nw;->j:Lcom/google/a/a/a/a/uh;

    iget-boolean v0, v0, Lcom/google/a/a/a/a/uh;->e:Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sanitizeForOffline()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    .locals 4

    new-instance v0, Lcom/google/a/a/a/a/nw;

    invoke-direct {v0}, Lcom/google/a/a/a/a/nw;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    invoke-static {v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    iget-object v1, v0, Lcom/google/a/a/a/a/nw;->d:Lcom/google/a/a/a/a/sb;

    if-eqz v1, :cond_0

    sget-object v2, Lcom/google/a/a/a/a/fj;->a:[Lcom/google/a/a/a/a/fj;

    iput-object v2, v1, Lcom/google/a/a/a/a/sb;->d:[Lcom/google/a/a/a/a/fj;

    sget-object v2, Lcom/google/a/a/a/a/fj;->a:[Lcom/google/a/a/a/a/fj;

    iput-object v2, v1, Lcom/google/a/a/a/a/sb;->c:[Lcom/google/a/a/a/a/fj;

    :cond_0
    sget-object v1, Lcom/google/a/a/a/a/nc;->a:[Lcom/google/a/a/a/a/nc;

    iput-object v1, v0, Lcom/google/a/a/a/a/nw;->g:[Lcom/google/a/a/a/a/nc;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    iget-wide v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->streamingDataTimestampElapsedMillis:J

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;-><init>(Lcom/google/a/a/a/a/nw;J)V

    return-object v1
.end method

.method public toBlob()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    invoke-static {v0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->playerResponseProto:Lcom/google/a/a/a/a/nw;

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/e/j;->a(Landroid/os/Parcel;Lcom/google/protobuf/nano/c;)V

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->streamingDataTimestampElapsedMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
