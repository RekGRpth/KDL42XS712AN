.class public Lcom/google/android/apps/youtube/datalib/innertube/model/al;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/a/a/a/a/ry;

.field private b:Ljava/lang/CharSequence;

.field private c:Ljava/lang/CharSequence;

.field private d:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/ry;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/ry;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->a:Lcom/google/a/a/a/a/ry;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->b:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->a:Lcom/google/a/a/a/a/ry;

    iget-object v0, v0, Lcom/google/a/a/a/a/ry;->b:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->a:Lcom/google/a/a/a/a/ry;

    iget-object v0, v0, Lcom/google/a/a/a/a/ry;->b:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->a:Lcom/google/a/a/a/a/ry;

    iget-object v0, v0, Lcom/google/a/a/a/a/ry;->c:[Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->a:Lcom/google/a/a/a/a/ry;

    iget-object v2, v0, Lcom/google/a/a/a/a/ry;->c:[Lcom/google/a/a/a/a/fk;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    invoke-static {v4}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->c:Ljava/lang/CharSequence;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    iput-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->c:Ljava/lang/CharSequence;

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/CharSequence;

    iget-object v6, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->c:Ljava/lang/CharSequence;

    aput-object v6, v5, v1

    const/4 v6, 0x1

    const-string v7, "line.separator"

    invoke-static {v7}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    aput-object v4, v5, v6

    invoke-static {v5}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->c:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->d:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->a:Lcom/google/a/a/a/a/ry;

    iget-object v0, v0, Lcom/google/a/a/a/a/ry;->e:Lcom/google/a/a/a/a/lq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->a:Lcom/google/a/a/a/a/ry;

    iget-object v0, v0, Lcom/google/a/a/a/a/ry;->e:Lcom/google/a/a/a/a/lq;

    iget-object v0, v0, Lcom/google/a/a/a/a/lq;->b:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->a:Lcom/google/a/a/a/a/ry;

    iget-object v0, v0, Lcom/google/a/a/a/a/ry;->e:Lcom/google/a/a/a/a/lq;

    iget-object v0, v0, Lcom/google/a/a/a/a/lq;->b:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->d:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Lcom/google/a/a/a/a/lq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/al;->a:Lcom/google/a/a/a/a/ry;

    iget-object v0, v0, Lcom/google/a/a/a/a/ry;->e:Lcom/google/a/a/a/a/lq;

    return-object v0
.end method
