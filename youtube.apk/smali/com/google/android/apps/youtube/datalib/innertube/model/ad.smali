.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/model/u;


# instance fields
.field private final a:Lcom/google/a/a/a/a/oo;

.field private final b:Lcom/google/android/apps/youtube/datalib/innertube/model/t;

.field private final c:F

.field private final d:Z

.field private e:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/oo;FLjava/lang/String;)V
    .locals 6

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/oo;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->a:Lcom/google/a/a/a/a/oo;

    iput p2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->c:F

    if-eqz p3, :cond_1

    const-string v0, "RQ"

    invoke-virtual {p3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->d:Z

    const/4 v0, 0x0

    iget-object v2, p1, Lcom/google/a/a/a/a/oo;->m:[Lcom/google/a/a/a/a/ak;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    iget-object v5, v4, Lcom/google/a/a/a/a/ak;->h:Lcom/google/a/a/a/a/kj;

    if-eqz v5, :cond_2

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/t;

    iget-object v1, v4, Lcom/google/a/a/a/a/ak;->h:Lcom/google/a/a/a/a/kj;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/t;-><init>(Lcom/google/a/a/a/a/kj;)V

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/t;

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a()Landroid/text/Spanned;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->a:Lcom/google/a/a/a/a/oo;

    iget-object v0, v0, Lcom/google/a/a/a/a/oo;->b:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public final b()Landroid/text/Spanned;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->a:Lcom/google/a/a/a/a/oo;

    iget-object v0, v0, Lcom/google/a/a/a/a/oo;->l:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public final c()Landroid/text/Spanned;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->a:Lcom/google/a/a/a/a/oo;

    iget-object v0, v0, Lcom/google/a/a/a/a/oo;->e:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public final d()Landroid/text/Spanned;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->a:Lcom/google/a/a/a/a/oo;

    iget-object v0, v0, Lcom/google/a/a/a/a/oo;->f:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->a:Lcom/google/a/a/a/a/oo;

    iget-object v0, v0, Lcom/google/a/a/a/a/oo;->h:Lcom/google/a/a/a/a/kz;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->a:Lcom/google/a/a/a/a/oo;

    iget-object v0, v0, Lcom/google/a/a/a/a/oo;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->d:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->a:Lcom/google/a/a/a/a/oo;

    iget-object v0, v0, Lcom/google/a/a/a/a/oo;->j:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Landroid/text/Spanned;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->a:Lcom/google/a/a/a/a/oo;

    iget-object v0, v0, Lcom/google/a/a/a/a/oo;->j:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public final j()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->a:Lcom/google/a/a/a/a/oo;

    iget-boolean v0, v0, Lcom/google/a/a/a/a/oo;->g:Z

    return v0
.end method

.method public final k()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->e:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->a:Lcom/google/a/a/a/a/oo;

    iget-object v1, v1, Lcom/google/a/a/a/a/oo;->d:Lcom/google/a/a/a/a/sx;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;-><init>(Lcom/google/a/a/a/a/sx;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->e:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->e:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    return-object v0
.end method

.method public final l()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;->c:F

    return v0
.end method
