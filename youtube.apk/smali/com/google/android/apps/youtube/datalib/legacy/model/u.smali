.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

.field private b:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

.field private c:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/legacy/model/t;Lcom/google/android/apps/youtube/datalib/legacy/model/t;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    goto :goto_0

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    goto :goto_0

    :cond_1
    return-void
.end method

.method private h()Ljava/util/List;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->c:Ljava/util/List;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_1
    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->c:Ljava/util/List;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->c:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/legacy/model/t;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/youtube/datalib/legacy/model/t;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final f()J
    .locals 6

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->h()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-wide v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->e()J

    move-result-wide v4

    add-long v0, v1, v4

    move-wide v1, v0

    goto :goto_0

    :cond_0
    return-wide v1
.end method

.method public final g()J
    .locals 6

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/u;->h()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-wide v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->f()J

    move-result-wide v4

    add-long v0, v1, v4

    move-wide v1, v0

    goto :goto_0

    :cond_0
    return-wide v1
.end method
