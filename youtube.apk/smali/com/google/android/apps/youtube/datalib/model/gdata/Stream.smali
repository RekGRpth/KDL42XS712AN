.class public Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final FORMAT_100_OVER_HTTP:I = 0x18

.field private static final FORMAT_101_OVER_HTTP:I = 0x19

.field private static final FORMAT_102_OVER_HTTP:I = 0x1a

.field private static final FORMAT_113_OVER_HTTP:I = 0x10

.field private static final FORMAT_114_OVER_HTTP:I = 0xf

.field private static final FORMAT_119_OVER_HTTP:I = 0x14

.field private static final FORMAT_159_OVER_HTTP:I = 0x20

.field private static final FORMAT_17_OVER_HTTP:I = 0x2

.field private static final FORMAT_180_OVER_HTTP:I = 0x23

.field private static final FORMAT_186_OVER_HTTP:I = 0x22

.field private static final FORMAT_18_OVER_HTTP:I = 0x3

.field private static final FORMAT_193_OVER_HTTP:I = 0x26

.field private static final FORMAT_22_OVER_HTTP:I = 0x8

.field private static final FORMAT_36_OVER_HTTP:I = 0x9

.field private static final FORMAT_37_OVER_HTTP:I = 0x1e

.field private static final FORMAT_5_OVER_HTTP:I = 0x7

.field private static final FORMAT_62_OVER_HTTP:I = 0xe

.field private static final FORMAT_64_OVER_HTTP:I = 0x1f

.field private static final FORMAT_80_OVER_HTTP:I = 0xb

.field private static final FORMAT_81_OVER_HTTP:I = 0xc

.field private static final FORMAT_82_OVER_HTTP:I = 0x15

.field private static final FORMAT_83_OVER_HTTP:I = 0x16

.field private static final FORMAT_84_OVER_HTTP:I = 0x17

.field private static final FORMAT_88_OVER_HTTP:I = 0xd

.field private static final FORMAT_HLS:I = 0x1c

.field private static final GDATA_TO_ITAG:Landroid/util/SparseIntArray;

.field public static final ITAG_DESCRIPTORS:Landroid/util/SparseArray;

.field private static final ITAG_HLS:I = 0x5d

.field public static final ITAG_UNKNOWN:I


# instance fields
.field private final height:I

.field public final is3D:Z

.field public final isEncrypted:Z

.field private final itag:I

.field public final mimeType:Ljava/lang/String;

.field public final sizeInBytes:J

.field public final uri:Landroid/net/Uri;

.field public final videoId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/16 v11, 0x1e0

    const/16 v10, 0x168

    const/16 v9, 0x2d0

    const/4 v8, 0x1

    const/4 v4, 0x0

    new-instance v6, Landroid/util/SparseArray;

    invoke-direct {v6}, Landroid/util/SparseArray;-><init>()V

    sput-object v6, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/4 v7, 0x5

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/4 v1, 0x7

    const/16 v2, 0x1aa

    const/16 v3, 0xf0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v6, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v7, 0x11

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/4 v1, 0x2

    const/16 v2, 0xb0

    const/16 v3, 0x90

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v6, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v7, 0x12

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/4 v1, 0x3

    const/16 v2, 0x280

    move v3, v10

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v6, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v7, 0x16

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v1, 0x8

    const/16 v2, 0x500

    move v3, v9

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v6, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v7, 0x24

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v1, 0x9

    const/16 v2, 0x140

    const/16 v3, 0xf0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v6, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v7, 0x25

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v1, 0x1e

    const/16 v2, 0x780

    const/16 v3, 0x438

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v1, 0x3e

    new-instance v2, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v3, 0xe

    move v5, v11

    move v6, v4

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v1, 0x40

    new-instance v2, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v3, 0x1f

    const/16 v5, 0x438

    move v6, v4

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v1, 0x50

    new-instance v2, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v3, 0xb

    const/16 v5, 0x195

    move v6, v4

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v1, 0x51

    new-instance v2, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v3, 0xc

    move v5, v10

    move v6, v4

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v6, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v7, 0x52

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v1, 0x15

    const/16 v2, 0x280

    move v3, v10

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v6, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v7, 0x53

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v1, 0x16

    const/16 v2, 0x356

    move v3, v11

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v6, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v7, 0x54

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v1, 0x17

    const/16 v2, 0x500

    move v3, v9

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v1, 0x58

    new-instance v2, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v3, 0xd

    move v5, v9

    move v6, v4

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v6, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v7, 0x64

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v1, 0x18

    const/16 v2, 0x280

    move v3, v10

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v6, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v7, 0x65

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v1, 0x19

    const/16 v2, 0x356

    move v3, v11

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v6, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v7, 0x66

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v1, 0x1a

    const/16 v2, 0x500

    move v3, v9

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v1, 0x71

    new-instance v2, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v3, 0x10

    move v5, v9

    move v6, v8

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v1, 0x72

    new-instance v2, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v3, 0xf

    move v5, v11

    move v6, v8

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v1, 0x77

    new-instance v2, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v3, 0x14

    const/16 v5, 0xf0

    move v6, v4

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v1, 0x9f

    new-instance v2, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v3, 0x20

    const/16 v5, 0x438

    move v6, v8

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v1, 0xb4

    new-instance v2, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v3, 0x23

    const/16 v5, 0x438

    move v6, v8

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v1, 0xba

    new-instance v2, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v3, 0x22

    move v5, v9

    move v6, v8

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v1, 0xc1

    new-instance v2, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v3, 0x26

    const/16 v5, 0x195

    move v6, v4

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    const/16 v1, 0x5d

    new-instance v2, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    const/16 v3, 0x1c

    move v5, v4

    move v6, v4

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/b;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->GDATA_TO_ITAG:Landroid/util/SparseIntArray;

    :goto_0
    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v4, v0, :cond_0

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->GDATA_TO_ITAG:Landroid/util/SparseIntArray;

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    iget v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/b;->c:I

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseIntArray;->put(II)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/a;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/a;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILandroid/net/Uri;Ljava/lang/String;ILjava/lang/String;)V
    .locals 8

    const-wide/16 v6, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;-><init>(ILandroid/net/Uri;Ljava/lang/String;ILjava/lang/String;J)V

    return-void
.end method

.method private constructor <init>(ILandroid/net/Uri;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->itag:I

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p3, :cond_0

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/e/o;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v3, "dnc"

    const-string v4, "1"

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/o;->a()Landroid/net/Uri;

    move-result-object p2

    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->uri:Landroid/net/Uri;

    iput-object p3, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->videoId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->mimeType:Ljava/lang/String;

    const-wide/16 v3, 0x0

    cmp-long v0, p6, v3

    if-ltz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    iput-wide p6, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->sizeInBytes:J

    const-string v0, "video/wvm"

    invoke-virtual {v0, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->isEncrypted:Z

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->ITAG_DESCRIPTORS:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/b;

    if-ltz p4, :cond_3

    move v3, v1

    :goto_1
    invoke-static {v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    if-eqz v0, :cond_1

    if-nez p4, :cond_1

    iget p4, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/b;->b:I

    :cond_1
    iput p4, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->height:I

    if-eqz v0, :cond_4

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/datalib/model/gdata/b;->e:Z

    if-eqz v0, :cond_4

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->is3D:Z

    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v3, v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method synthetic constructor <init>(ILandroid/net/Uri;Ljava/lang/String;ILjava/lang/String;JLcom/google/android/apps/youtube/datalib/model/gdata/a;)V
    .locals 0

    invoke-direct/range {p0 .. p7}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;-><init>(ILandroid/net/Uri;Ljava/lang/String;ILjava/lang/String;J)V

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;)V
    .locals 8

    const/4 v3, 0x0

    const/4 v1, 0x0

    const-wide/16 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    move v4, v1

    move-object v5, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;-><init>(ILandroid/net/Uri;Ljava/lang/String;ILjava/lang/String;J)V

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 8

    const/4 v1, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, v1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;-><init>(ILandroid/net/Uri;Ljava/lang/String;ILjava/lang/String;J)V

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;I)V
    .locals 8

    const/4 v1, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;-><init>(ILandroid/net/Uri;Ljava/lang/String;ILjava/lang/String;J)V

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;ILjava/lang/String;)V
    .locals 8

    const/4 v1, 0x0

    const-wide/16 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;-><init>(ILandroid/net/Uri;Ljava/lang/String;ILjava/lang/String;J)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 8

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;-><init>(ILandroid/net/Uri;Ljava/lang/String;ILjava/lang/String;J)V

    return-void
.end method

.method static synthetic access$000()Landroid/util/SparseIntArray;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->GDATA_TO_ITAG:Landroid/util/SparseIntArray;

    return-object v0
.end method

.method public static getFirstAvailableFormat(Ljava/util/List;Ljava/util/Map;)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;
    .locals 2

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "builder required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->buildUpon()Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public buildUpon()Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->uri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->videoId(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->height:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->height(I)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->sizeInBytes:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->sizeInBytes(J)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->mimeType(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->itag:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;->itag(I)Lcom/google/android/apps/youtube/datalib/model/gdata/Stream$Builder;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->itag:I

    iget v3, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->itag:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->height:I

    iget v3, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->height:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->uri:Landroid/net/Uri;

    iget-object v3, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->videoId:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->videoId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->is3D:Z

    iget-boolean v3, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->is3D:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->isEncrypted:Z

    iget-boolean v3, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->isEncrypted:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->mimeType:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->mimeType:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->height:I

    return v0
.end method

.method public getItag()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->itag:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    return v0
.end method

.method public isHD()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->height:I

    const/16 v1, 0x2d0

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHls()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->itag:I

    const/16 v1, 0x5d

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->itag:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->sizeInBytes:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->itag:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->uri:Landroid/net/Uri;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->height:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->mimeType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Stream;->sizeInBytes:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
