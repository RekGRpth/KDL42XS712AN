.class public final Lcom/google/android/apps/youtube/datalib/innertube/ah;
.super Lcom/google/android/apps/youtube/datalib/innertube/a;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/android/apps/youtube/common/e/b;


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/innertube/a;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ah;->d:Lcom/google/android/apps/youtube/common/e/b;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/r;Lcom/google/android/apps/youtube/datalib/innertube/p;Lcom/android/volley/l;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/datalib/innertube/a;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/r;Lcom/google/android/apps/youtube/datalib/innertube/p;Lcom/android/volley/l;)V

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ah;->d:Lcom/google/android/apps/youtube/common/e/b;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/ak;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ah;->b:Lcom/google/android/apps/youtube/datalib/innertube/p;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/ak;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/ak;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/a/k;->a()Lcom/google/android/apps/youtube/datalib/a/k;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/ah;->a(Lcom/google/android/apps/youtube/datalib/innertube/ak;Lcom/google/android/apps/youtube/datalib/a/l;)V

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/a/k;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/ak;Lcom/google/android/apps/youtube/datalib/a/l;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ah;->d:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v0

    new-instance v2, Lcom/google/android/apps/youtube/datalib/innertube/ai;

    invoke-direct {v2, p0, p2, v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/ai;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/ah;Lcom/google/android/apps/youtube/datalib/a/l;J)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ah;->c:Lcom/android/volley/l;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ah;->a:Lcom/google/android/apps/youtube/datalib/innertube/r;

    const-class v3, Lcom/google/a/a/a/a/nw;

    invoke-virtual {v1, p1, v3, v2}, Lcom/google/android/apps/youtube/datalib/innertube/r;->a(Lcom/google/android/apps/youtube/datalib/innertube/s;Ljava/lang/Class;Lcom/google/android/apps/youtube/datalib/a/l;)Lcom/google/android/apps/youtube/datalib/innertube/q;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/volley/l;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method
