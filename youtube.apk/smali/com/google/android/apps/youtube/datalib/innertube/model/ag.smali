.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/a/a/a/a/os;

.field private b:Ljava/util/List;

.field private c:Lcom/google/a/a/a/a/dp;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/os;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/os;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;->a:Lcom/google/a/a/a/a/os;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 8

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;->b:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;->a:Lcom/google/a/a/a/a/os;

    iget-object v1, v1, Lcom/google/a/a/a/a/os;->b:[Lcom/google/a/a/a/a/ou;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;->b:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;->a:Lcom/google/a/a/a/a/os;

    iget-object v1, v0, Lcom/google/a/a/a/a/os;->b:[Lcom/google/a/a/a/a/ou;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    iget-object v4, v3, Lcom/google/a/a/a/a/ou;->c:Lcom/google/a/a/a/a/ow;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/af;

    iget-object v3, v3, Lcom/google/a/a/a/a/ou;->c:Lcom/google/a/a/a/a/ow;

    iget-object v6, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;->a:Lcom/google/a/a/a/a/os;

    iget-object v6, v6, Lcom/google/a/a/a/a/os;->c:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;->a:Lcom/google/a/a/a/a/os;

    iget-boolean v7, v7, Lcom/google/a/a/a/a/os;->d:Z

    invoke-direct {v5, v3, v6, v7}, Lcom/google/android/apps/youtube/datalib/innertube/model/af;-><init>(Lcom/google/a/a/a/a/ow;Ljava/lang/String;Z)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;->b:Ljava/util/List;

    return-object v0
.end method

.method public final b()Lcom/google/a/a/a/a/dp;
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;->c:Lcom/google/a/a/a/a/dp;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/a/a/a/a/dp;

    invoke-direct {v0}, Lcom/google/a/a/a/a/dp;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;->c:Lcom/google/a/a/a/a/dp;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;->a:Lcom/google/a/a/a/a/os;

    iget-object v1, v0, Lcom/google/a/a/a/a/os;->e:[Lcom/google/a/a/a/a/ot;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    iget-object v4, v3, Lcom/google/a/a/a/a/ot;->b:Lcom/google/a/a/a/a/li;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;->c:Lcom/google/a/a/a/a/dp;

    iget-object v3, v3, Lcom/google/a/a/a/a/ot;->b:Lcom/google/a/a/a/a/li;

    iput-object v3, v4, Lcom/google/a/a/a/a/dp;->c:Lcom/google/a/a/a/a/li;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ag;->c:Lcom/google/a/a/a/a/dp;

    return-object v0
.end method
