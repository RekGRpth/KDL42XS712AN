.class public Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final DEFAULT_QOE_BASE_URL:Ljava/lang/String; = "https://s.youtube.com/api/stats/qoe"

.field public static final DEFAULT_VSS2_DELAYPLAY_BASE_URL:Ljava/lang/String; = "https://s.youtube.com/api/stats/delayplay"

.field public static final DEFAULT_VSS2_PLAYBACK_BASE_URL:Ljava/lang/String; = "https://s.youtube.com/api/stats/playback"

.field public static final DEFAULT_VSS2_WATCHTIME_BASE_URL:Ljava/lang/String; = "https://s.youtube.com/api/stats/watchtime"

.field private static final PTRACKING_CLIENT_PARAMS:Ljava/util/Set;

.field private static final REMARKETING_CLIENT_PARAMS:Ljava/util/Set;


# instance fields
.field private final playbackTrackingUrls:Ljava/util/List;

.field private final qoeTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

.field private final vss2DelayplayTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

.field private final vss2PlaybackTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

.field private final vss2WatchtimeTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->PTRACKING_CLIENT_PARAMS:Ljava/util/Set;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->CPN:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->REMARKETING_CLIENT_PARAMS:Ljava/util/Set;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->MS:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/x;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/x;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;-><init>(Lcom/google/a/a/a/a/nb;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/a/a/a/a/nb;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/google/a/a/a/a/nb;->b:Lcom/google/a/a/a/a/tb;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/a/a/a/a/nb;->b:Lcom/google/a/a/a/a/tb;

    iget-object v0, v0, Lcom/google/a/a/a/a/tb;->b:Ljava/lang/String;

    :goto_0
    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->NO_CLIENT_PARAMS:Ljava/util/Set;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2PlaybackTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    if-eqz p1, :cond_3

    iget-object v0, p1, Lcom/google/a/a/a/a/nb;->c:Lcom/google/a/a/a/a/tb;

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    iget-object v1, p1, Lcom/google/a/a/a/a/nb;->c:Lcom/google/a/a/a/a/tb;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->NO_CLIENT_PARAMS:Ljava/util/Set;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Lcom/google/a/a/a/a/tb;Ljava/util/Set;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2DelayplayTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    :goto_1
    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    if-eqz p1, :cond_4

    iget-object v0, p1, Lcom/google/a/a/a/a/nb;->d:Lcom/google/a/a/a/a/tb;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/google/a/a/a/a/nb;->d:Lcom/google/a/a/a/a/tb;

    iget-object v0, v0, Lcom/google/a/a/a/a/tb;->b:Ljava/lang/String;

    :goto_2
    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->NO_CLIENT_PARAMS:Ljava/util/Set;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2WatchtimeTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    if-eqz p1, :cond_5

    iget-object v0, p1, Lcom/google/a/a/a/a/nb;->f:Lcom/google/a/a/a/a/tb;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/google/a/a/a/a/nb;->f:Lcom/google/a/a/a/a/tb;

    iget-object v0, v0, Lcom/google/a/a/a/a/tb;->b:Ljava/lang/String;

    :goto_3
    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->NO_CLIENT_PARAMS:Ljava/util/Set;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->qoeTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->playbackTrackingUrls:Ljava/util/List;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/a/a/a/a/nb;->e:Lcom/google/a/a/a/a/tb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->playbackTrackingUrls:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    iget-object v2, p1, Lcom/google/a/a/a/a/nb;->e:Lcom/google/a/a/a/a/tb;

    iget-object v2, v2, Lcom/google/a/a/a/a/tb;->b:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->PTRACKING_CLIENT_PARAMS:Ljava/util/Set;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Ljava/lang/String;Ljava/util/Set;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/google/a/a/a/a/nb;->g:Lcom/google/a/a/a/a/tb;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->playbackTrackingUrls:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    iget-object v2, p1, Lcom/google/a/a/a/a/nb;->g:Lcom/google/a/a/a/a/tb;

    iget-object v2, v2, Lcom/google/a/a/a/a/tb;->b:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->REMARKETING_CLIENT_PARAMS:Ljava/util/Set;

    iget-object v4, p1, Lcom/google/a/a/a/a/nb;->g:Lcom/google/a/a/a/a/tb;

    iget v4, v4, Lcom/google/a/a/a/a/tb;->c:I

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Ljava/lang/String;Ljava/util/Set;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void

    :cond_2
    const-string v0, "https://s.youtube.com/api/stats/playback"

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    const-string v1, "https://s.youtube.com/api/stats/delayplay"

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->NO_CLIENT_PARAMS:Ljava/util/Set;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2DelayplayTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    goto :goto_1

    :cond_4
    const-string v0, "https://s.youtube.com/api/stats/watchtime"

    goto :goto_2

    :cond_5
    const-string v0, "https://s.youtube.com/api/stats/qoe"

    goto :goto_3
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/a/a/e;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/e;->b()Lcom/google/android/apps/youtube/a/a/h;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Lcom/google/android/apps/youtube/a/a/h;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2PlaybackTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/e;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/e;->d()Lcom/google/android/apps/youtube/a/a/h;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Lcom/google/android/apps/youtube/a/a/h;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2DelayplayTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    :goto_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/e;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/e;->h()Lcom/google/android/apps/youtube/a/a/h;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Lcom/google/android/apps/youtube/a/a/h;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2WatchtimeTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    :goto_2
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/e;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/e;->j()Lcom/google/android/apps/youtube/a/a/h;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Lcom/google/android/apps/youtube/a/a/h;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->qoeTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    :goto_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->playbackTrackingUrls:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/e;->k()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/h;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->playbackTrackingUrls:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-direct {v3, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Lcom/google/android/apps/youtube/a/a/h;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    const-string v1, "https://s.youtube.com/api/stats/playback"

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->NO_CLIENT_PARAMS:Ljava/util/Set;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2PlaybackTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    const-string v1, "https://s.youtube.com/api/stats/delayplay"

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->NO_CLIENT_PARAMS:Ljava/util/Set;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2DelayplayTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    goto :goto_1

    :cond_2
    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    const-string v1, "https://s.youtube.com/api/stats/watchtime"

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->NO_CLIENT_PARAMS:Ljava/util/Set;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2WatchtimeTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    goto :goto_2

    :cond_3
    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    const-string v1, "https://s.youtube.com/api/stats/qoe"

    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->NO_CLIENT_PARAMS:Ljava/util/Set;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->qoeTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    goto :goto_3

    :cond_4
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2PlaybackTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2PlaybackTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2DelayplayTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2DelayplayTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2WatchtimeTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2WatchtimeTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->qoeTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->qoeTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->playbackTrackingUrls:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->playbackTrackingUrls:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getPlaybackTrackingUrls()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->playbackTrackingUrls:Ljava/util/List;

    return-object v0
.end method

.method public getQoeTrackingUrl()Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->qoeTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    return-object v0
.end method

.method public getVss2DelayplayTrackingUrl()Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2DelayplayTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    return-object v0
.end method

.method public getVss2PlaybackTrackingUrl()Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2PlaybackTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    return-object v0
.end method

.method public getVss2WatchtimeTrackingUrl()Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2WatchtimeTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    return-object v0
.end method

.method public toPlaybackTrackingProto()Lcom/google/android/apps/youtube/a/a/e;
    .locals 3

    new-instance v1, Lcom/google/android/apps/youtube/a/a/e;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/a/a/e;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2PlaybackTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->toTrackingUrlProto()Lcom/google/android/apps/youtube/a/a/h;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/a/a/e;->a(Lcom/google/android/apps/youtube/a/a/h;)Lcom/google/android/apps/youtube/a/a/e;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2DelayplayTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->toTrackingUrlProto()Lcom/google/android/apps/youtube/a/a/h;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/a/a/e;->b(Lcom/google/android/apps/youtube/a/a/h;)Lcom/google/android/apps/youtube/a/a/e;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->vss2WatchtimeTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->toTrackingUrlProto()Lcom/google/android/apps/youtube/a/a/h;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/a/a/e;->c(Lcom/google/android/apps/youtube/a/a/h;)Lcom/google/android/apps/youtube/a/a/e;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->qoeTrackingUrl:Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->toTrackingUrlProto()Lcom/google/android/apps/youtube/a/a/h;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/a/a/e;->d(Lcom/google/android/apps/youtube/a/a/h;)Lcom/google/android/apps/youtube/a/a/e;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->playbackTrackingUrls:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->toTrackingUrlProto()Lcom/google/android/apps/youtube/a/a/h;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/a/a/e;->e(Lcom/google/android/apps/youtube/a/a/h;)Lcom/google/android/apps/youtube/a/a/e;

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;->toPlaybackTrackingProto()Lcom/google/android/apps/youtube/a/a/e;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/e/j;->a(Landroid/os/Parcel;Lcom/google/protobuf/micro/c;)V

    return-void
.end method
