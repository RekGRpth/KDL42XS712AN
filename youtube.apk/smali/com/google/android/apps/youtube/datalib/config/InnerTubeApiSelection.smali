.class public final enum Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

.field public static final DEFAULT_ORDINAL:I = 0x0

.field public static final PREFERENCES_KEY:Ljava/lang/String; = "InnerTubeApiSelection"

.field public static final enum PRERELEASE:Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

.field public static final enum V1:Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

.field public static final enum V1RELEASE_ONLY_WORKS_IN_STAGING:Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;


# instance fields
.field private final encodedPathName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    const-string v1, "V1"

    const-string v2, "youtubei/v1"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;->V1:Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    const-string v1, "V1RELEASE_ONLY_WORKS_IN_STAGING"

    const-string v2, "youtubei/v1release"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;->V1RELEASE_ONLY_WORKS_IN_STAGING:Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    const-string v1, "PRERELEASE"

    const-string v2, "youtubei/vi"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;->PRERELEASE:Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;->V1:Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;->V1RELEASE_ONLY_WORKS_IN_STAGING:Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;->PRERELEASE:Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;->$VALUES:[Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;->encodedPathName:Ljava/lang/String;

    return-void
.end method

.method public static getDefaultIndex()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method static getInnerTubeApiSelection(Landroid/content/SharedPreferences;)Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;
    .locals 4

    const/4 v3, 0x0

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "InnerTubeApiSelection"

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;->values()[Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    move-result-object v1

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bogus value in shared preferences for key InnerTubeApiSelection value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " returning default value."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;->values()[Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    move-result-object v0

    aget-object v0, v0, v3

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;->$VALUES:[Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;

    return-object v0
.end method


# virtual methods
.method final getEncodedPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/config/InnerTubeApiSelection;->encodedPathName:Ljava/lang/String;

    return-object v0
.end method
