.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/m;
.super Lcom/google/android/apps/youtube/datalib/legacy/a/b;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/a/b;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/a/b;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/m;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final a(Lorg/json/JSONObject;I)Lcom/google/android/apps/youtube/datalib/legacy/a/a;
    .locals 8

    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    new-instance v0, Lorg/json/JSONException;

    const-string v1, "Unsupported version"

    invoke-direct {v0, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    const-string v1, "name"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "icon"

    invoke-static {p1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/m;->c(Lorg/json/JSONObject;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "price"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "rating"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    const-string v5, "rating"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v5

    double-to-float v5, v5

    const-string v6, "ratingImage"

    invoke-static {p1, v6}, Lcom/google/android/apps/youtube/datalib/legacy/model/m;->c(Lorg/json/JSONObject;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const-string v7, "reviews"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;ZFLandroid/net/Uri;I)V

    return-object v0
.end method

.method protected final a(Lorg/json/JSONObject;)V
    .locals 3

    const-string v0, "name"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/m;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getAppName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "icon"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/m;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getIcon()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "price"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/m;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getPrice()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "hasRating"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/m;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->hasRating()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "rating"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/m;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getRating()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    const-string v0, "ratingImage"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/m;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getRatingImageUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "reviews"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/m;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;->getReviewCount()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    return-void
.end method
