.class public final enum Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

.field public static final DEFAULT_ORDINAL:I = 0x0

.field public static final PREFERENCES_KEY:Ljava/lang/String; = "ApiaryHostSelection"

.field public static final enum PRODUCTION:Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

.field public static final enum STAGING:Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;


# instance fields
.field private final apiaryBaseUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

    const-string v1, "PRODUCTION"

    const-string v2, "https://www.googleapis.com"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;->PRODUCTION:Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

    const-string v1, "STAGING"

    const-string v2, "https://www-googleapis-staging.sandbox.google.com"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;->STAGING:Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;->PRODUCTION:Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;->STAGING:Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;->$VALUES:[Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;->apiaryBaseUri:Landroid/net/Uri;

    return-void
.end method

.method static getApiaryHostSelection(Landroid/content/SharedPreferences;)Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;
    .locals 4

    const/4 v3, 0x0

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "ApiaryHostSelection"

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;->values()[Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

    move-result-object v1

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bogus value in shared preferences for key ApiaryHostSelection value "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " returning default value."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->b(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;->values()[Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

    move-result-object v0

    aget-object v0, v0, v3

    goto :goto_0
.end method

.method public static getDefaultIndex()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;->$VALUES:[Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;

    return-object v0
.end method


# virtual methods
.method final getApiaryBaseUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/config/ApiaryHostSelection;->apiaryBaseUri:Landroid/net/Uri;

    return-object v0
.end method
