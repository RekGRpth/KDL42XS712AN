.class public final Lcom/google/android/apps/youtube/datalib/offline/h;
.super Lcom/google/android/apps/youtube/common/d/c;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/datalib/offline/j;

.field private final b:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/offline/j;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/youtube/common/d/c;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/h;->b:Ljava/util/Set;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/offline/j;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/h;->a:Lcom/google/android/apps/youtube/datalib/offline/j;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/h;->b:Ljava/util/Set;

    sget-object v1, Lcom/google/android/apps/youtube/common/d/g;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    const-string v0, "Network change detected, dispatch offline http requests."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/h;->a:Lcom/google/android/apps/youtube/datalib/offline/j;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/offline/j;->a()V

    return-void
.end method

.method protected final b()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/h;->b:Ljava/util/Set;

    return-object v0
.end method
