.class public final enum Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

.field public static final enum DOMAIN:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

.field public static final enum EXTENDED_CIRCLES:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

.field public static final enum OTHER:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

.field public static final enum PRIVATE:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

.field public static final enum PUBLIC:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

.field public static final enum SQUARE:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->OTHER:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    const-string v1, "PRIVATE"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->PRIVATE:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    const-string v1, "PUBLIC"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->PUBLIC:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    const-string v1, "EXTENDED_CIRCLES"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->EXTENDED_CIRCLES:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    const-string v1, "SQUARE"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->SQUARE:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    const-string v1, "DOMAIN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->DOMAIN:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->OTHER:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->PRIVATE:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->PUBLIC:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->EXTENDED_CIRCLES:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->SQUARE:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->DOMAIN:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->$VALUES:[Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->$VALUES:[Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    return-object v0
.end method
