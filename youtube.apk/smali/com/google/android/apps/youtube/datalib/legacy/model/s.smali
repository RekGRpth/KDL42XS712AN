.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/s;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

.field private final b:I

.field private final c:I

.field private final d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/s;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;IIZ)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;IIZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iput p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->b:I

    iput p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->c:I

    iput-boolean p4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->d:Z

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;)Z
    .locals 2

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->updated:Ljava/util/Date;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->updated:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    return-object v0
.end method

.method public final c()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final d()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    return v0
.end method

.method public final e()Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->c:I

    return v0
.end method

.method public final g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/s;->d:Z

    return v0
.end method
