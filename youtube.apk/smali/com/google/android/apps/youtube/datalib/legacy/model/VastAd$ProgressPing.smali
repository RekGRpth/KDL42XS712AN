.class public Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/apps/youtube/datalib/legacy/a/a;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final isPercentageOffset:Z

.field private final offset:I

.field private final pingUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/bc;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bc;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IZLandroid/net/Uri;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->offset:I

    iput-boolean p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->isPercentageOffset:Z

    iput-object p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->pingUri:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getOffset()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getOffset()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->isPercentageOffset()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->isPercentageOffset()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getPingUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getPingUri()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic getConverter()Lcom/google/android/apps/youtube/datalib/legacy/a/b;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getConverter()Lcom/google/android/apps/youtube/datalib/legacy/model/bd;

    move-result-object v0

    return-object v0
.end method

.method public getConverter()Lcom/google/android/apps/youtube/datalib/legacy/model/bd;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/bd;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bd;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;)V

    return-object v0
.end method

.method public getOffset()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->offset:I

    return v0
.end method

.method public getPingUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->pingUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getTimeOffsetMilliseconds(I)I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->isPercentageOffset()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getOffset()I

    move-result v0

    mul-int/2addr v0, p1

    mul-int/lit16 v0, v0, 0x3e8

    div-int/lit8 v0, v0, 0x64

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getOffset()I

    move-result v0

    goto :goto_0
.end method

.method public isPercentageOffset()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->isPercentageOffset:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getOffset()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->isPercentageOffset()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->getPingUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method
