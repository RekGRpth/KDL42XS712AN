.class public Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;
.super Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/model/a/d;


# instance fields
.field private final g:Ljava/util/List;

.field private h:Z

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/fy;)V
    .locals 6

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/fy;->b:Lcom/google/a/a/a/a/fz;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/fy;->d:Lcom/google/a/a/a/a/fz;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    iget-object v0, p1, Lcom/google/a/a/a/a/fy;->b:Lcom/google/a/a/a/a/fz;

    iget-object v0, v0, Lcom/google/a/a/a/a/fz;->b:Lcom/google/a/a/a/a/gc;

    iget-object v0, v0, Lcom/google/a/a/a/a/gc;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;->j:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/a/a/a/a/fy;->d:Lcom/google/a/a/a/a/fz;

    iget-object v0, v0, Lcom/google/a/a/a/a/fz;->b:Lcom/google/a/a/a/a/gc;

    iget-object v0, v0, Lcom/google/a/a/a/a/gc;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;->i:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/a/a/a/a/fy;->c:[Lcom/google/a/a/a/a/fz;

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    iget-object v5, v4, Lcom/google/a/a/a/a/fz;->b:Lcom/google/a/a/a/a/gc;

    if-eqz v5, :cond_0

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    iget-object v4, v4, Lcom/google/a/a/a/a/fz;->b:Lcom/google/a/a/a/a/gc;

    invoke-direct {v5, v4, p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;-><init>(Lcom/google/a/a/a/a/gc;Lcom/google/android/apps/youtube/datalib/innertube/model/a/d;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;->g:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;->g:Ljava/util/List;

    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;->h:Z

    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;->i:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;->j:Ljava/lang/String;

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;->h:Z

    return v0
.end method
