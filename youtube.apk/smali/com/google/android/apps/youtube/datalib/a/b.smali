.class public final Lcom/google/android/apps/youtube/datalib/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/android/volley/n;

.field public static final b:Lcom/android/volley/n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/a/c;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/a/c;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/a/b;->a:Lcom/android/volley/n;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/a/d;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/a/d;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/a/b;->b:Lcom/android/volley/n;

    return-void
.end method

.method public static a(Lcom/android/volley/VolleyError;)I
    .locals 2

    const/4 v0, 0x0

    instance-of v1, p0, Lcom/android/volley/NetworkError;

    if-eqz v1, :cond_1

    check-cast p0, Lcom/android/volley/NetworkError;

    iget-object v1, p0, Lcom/android/volley/NetworkError;->networkResponse:Lcom/android/volley/j;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/volley/NetworkError;->networkResponse:Lcom/android/volley/j;

    iget v0, v0, Lcom/android/volley/j;->a:I

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v1, p0, Lcom/android/volley/ServerError;

    if-eqz v1, :cond_0

    check-cast p0, Lcom/android/volley/ServerError;

    iget-object v1, p0, Lcom/android/volley/ServerError;->networkResponse:Lcom/android/volley/j;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/android/volley/ServerError;->networkResponse:Lcom/android/volley/j;

    iget v0, v0, Lcom/android/volley/j;->a:I

    goto :goto_0
.end method
