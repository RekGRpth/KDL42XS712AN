.class public Lcom/google/android/apps/youtube/datalib/legacy/model/au;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/a/a/a/c;

.field private b:Ljava/util/List;

.field private c:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/a/a/a/c;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    :try_start_0
    new-instance v0, Lcom/google/android/apps/youtube/a/a/a/c;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/a/c;-><init>()V

    invoke-static {p1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/a/c;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->b:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/google/android/apps/youtube/a/a/a/c;->f:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->c:Ljava/util/List;

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/apps/youtube/a/a/a/c;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/youtube/a/a/a/c;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/youtube/a/a/a/c;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/youtube/a/a/a/c;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    const/16 v1, 0xf

    iput v1, v0, Lcom/google/android/apps/youtube/a/a/a/c;->h:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    const/4 v1, 0x2

    iput v1, v0, Lcom/google/android/apps/youtube/a/a/a/c;->e:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->b:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->c:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;
    .locals 7

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->c:Ljava/util/List;

    new-array v4, v1, [Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/apps/youtube/a/a/a/c;->f:[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    iget-object v0, v0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    array-length v0, v0

    if-gtz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    iget v0, v0, Lcom/google/android/apps/youtube/a/a/a/c;->e:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->b:Ljava/util/List;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/av;->a(ILjava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/av;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/av;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v2, v1

    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/av;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    :goto_2
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    iget-object v2, v0, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/av;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/e/c;->b(Ljava/util/List;)[I

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    :cond_3
    :try_start_0
    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;

    new-instance v0, Lcom/google/android/apps/youtube/a/a/a/c;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/a/c;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    invoke-static {v2}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/a/c;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;-><init>(Lcom/google/android/apps/youtube/a/a/a/c;Lcom/google/android/apps/youtube/datalib/legacy/model/at;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :goto_3
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v3

    goto :goto_3
.end method

.method public final a(I)Lcom/google/android/apps/youtube/datalib/legacy/model/au;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    iput p1, v0, Lcom/google/android/apps/youtube/a/a/a/c;->i:I

    return-object p0
.end method

.method public final a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/au;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->c:Ljava/util/List;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;)Lcom/google/android/apps/youtube/datalib/legacy/model/au;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->getValue()I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/youtube/a/a/a/c;->e:I

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/au;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    iget-object v0, v0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    array-length v0, v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    const/4 v1, 0x0

    new-array v1, v1, [I

    iput-object v1, v0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final b(I)Lcom/google/android/apps/youtube/datalib/legacy/model/au;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    iput p1, v0, Lcom/google/android/apps/youtube/a/a/a/c;->h:I

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/au;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;->a:Lcom/google/android/apps/youtube/a/a/a/c;

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    iput-object p1, v0, Lcom/google/android/apps/youtube/a/a/a/c;->g:Ljava/lang/String;

    return-object p0
.end method
