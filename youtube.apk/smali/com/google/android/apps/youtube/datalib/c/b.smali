.class final Lcom/google/android/apps/youtube/datalib/c/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:[B

.field private c:Lcom/google/android/apps/youtube/datalib/c/c;

.field private final d:Ljava/security/Key;


# direct methods
.method constructor <init>(Ljava/lang/String;[B)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/b;->a:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/b;->b:[B

    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    const-string v1, "HmacSHA1"

    invoke-direct {v0, p2, v1}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/b;->d:Ljava/security/Key;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/c/c;

    invoke-direct {v0, p2}, Lcom/google/android/apps/youtube/datalib/c/c;-><init>([B)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/b;->c:Lcom/google/android/apps/youtube/datalib/c/c;

    return-void
.end method

.method private a([BI)Ljava/lang/String;
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/b;->d:Ljava/security/Key;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/datalib/c/b;->a(Ljava/security/Key;[B)[B

    move-result-object v0

    const/16 v1, 0x14

    if-eq p2, v1, :cond_0

    invoke-static {v0, v3, p2}, Lcom/google/android/apps/youtube/common/e/a;->a([BII)[B

    move-result-object v0

    move-object v1, v0

    :goto_0
    new-array v2, v5, [[B

    new-array v0, v4, [B

    aput-byte v3, v0, v3

    aput-object v0, v2, v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/b;->c:Lcom/google/android/apps/youtube/datalib/c/c;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/c/c;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    aput-object v0, v2, v4

    const/4 v0, 0x2

    aput-object v1, v2, v0

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/e/a;->a([[B)[B

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    invoke-static {v0, v5}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    return-object v1

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method static a(Ljava/security/Key;[B)[B
    .locals 4

    sget-object v1, Lcom/google/android/apps/youtube/datalib/c/d;->a:Ljavax/crypto/Mac;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/youtube/datalib/c/d;->a:Ljavax/crypto/Mac;

    invoke-virtual {v0, p0}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/c/d;->a:Ljavax/crypto/Mac;

    invoke-virtual {v0, p1}, Ljavax/crypto/Mac;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    monitor-exit v1

    return-object v0

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Fatal error: hmac key is invalid."

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method final a(Ljava/util/Map;Ljava/lang/String;[B)V
    .locals 5

    const-string v0, "device_id=%s,data=%s,content=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/c/b;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    array-length v4, v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v3, v4}, Lcom/google/android/apps/youtube/common/e/a;->b([BI)[B

    move-result-object v3

    const/4 v4, 0x4

    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/youtube/datalib/c/b;->a([BI)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const/16 v3, 0x14

    invoke-direct {p0, p3, v3}, Lcom/google/android/apps/youtube/datalib/c/b;->a([BI)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "X-Goog-Device-Auth"

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method final a()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/b;->b:[B

    return-object v0
.end method

.method final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/b;->a:Ljava/lang/String;

    return-object v0
.end method
