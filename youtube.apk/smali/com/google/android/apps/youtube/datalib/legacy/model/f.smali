.class public Lcom/google/android/apps/youtube/datalib/legacy/model/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Ljava/util/ArrayList;

.field private c:Ljava/util/ArrayList;

.field private d:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/f;->a:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/f;->b:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/f;->c:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/f;->a:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/f;->b:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/f;->c:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/f;->d:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;-><init>(ILjava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;Lcom/google/android/apps/youtube/datalib/legacy/model/e;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;)Lcom/google/android/apps/youtube/datalib/legacy/model/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/f;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;)Lcom/google/android/apps/youtube/datalib/legacy/model/f;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/f;->d:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardTrackingEvent;)Lcom/google/android/apps/youtube/datalib/legacy/model/f;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method
