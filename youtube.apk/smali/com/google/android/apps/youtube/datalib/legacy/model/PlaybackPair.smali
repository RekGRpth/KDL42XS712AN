.class public Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final playerResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

.field private final video:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/y;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/y;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->playerResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->video:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->playerResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->video:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->getVideo()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public getPlayerResponse()Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->playerResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    return-object v0
.end method

.method public getVideo()Lcom/google/android/apps/youtube/datalib/model/gdata/Video;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->video:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->playerResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->playerResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;->hashCode()I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->video:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->video:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->hashCode()I

    :cond_1
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->playerResponse:Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/PlaybackPair;->video:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
