.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final author:Ljava/lang/String;

.field public final contentUri:Landroid/net/Uri;

.field public final editUri:Landroid/net/Uri;

.field public final hqThumbnailUri:Landroid/net/Uri;

.field public final id:Ljava/lang/String;

.field public final isPrivate:Z

.field public final postUri:Landroid/net/Uri;

.field public final sdThumbnailUri:Landroid/net/Uri;

.field public final size:I

.field public final summary:Ljava/lang/String;

.field public final thumbnailUri:Landroid/net/Uri;

.field public final title:Ljava/lang/String;

.field public final updated:Ljava/util/Date;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;IZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->title:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->summary:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->author:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->updated:Ljava/util/Date;

    iput-object p6, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->contentUri:Landroid/net/Uri;

    iput-object p7, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->editUri:Landroid/net/Uri;

    iput-object p8, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->postUri:Landroid/net/Uri;

    iput-object p9, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->thumbnailUri:Landroid/net/Uri;

    iput-object p10, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->hqThumbnailUri:Landroid/net/Uri;

    iput-object p11, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->sdThumbnailUri:Landroid/net/Uri;

    iput p12, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    iput-boolean p13, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->isPrivate:Z

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "builder required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->id(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->title(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->summary:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->summary(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->author:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->author(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->updated:Ljava/util/Date;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->updated(Ljava/util/Date;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->contentUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->contentUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->editUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->editUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->postUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->postUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->thumbnailUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->thumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->hqThumbnailUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->hqThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->sdThumbnailUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->sdThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->size:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->size(I)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->isPrivate:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;->isPrivate(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->id:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;->title:Ljava/lang/String;

    return-object v0
.end method
