.class public final enum Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

.field public static final enum RANDOMIZE:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

.field public static final enum RANDOMLY_REVERSE:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

.field public static final enum SORTED:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

.field public static final enum UNKNOWN:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    const-string v1, "RANDOMIZE"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->RANDOMIZE:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    const-string v1, "RANDOMLY_REVERSE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->RANDOMLY_REVERSE:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    const-string v1, "SORTED"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->SORTED:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    const-string v1, "UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->UNKNOWN:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->RANDOMIZE:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->RANDOMLY_REVERSE:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->SORTED:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->UNKNOWN:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->$VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;
    .locals 5

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->values()[Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    iget v4, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->value:I

    if-ne v4, p0, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->UNKNOWN:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->$VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$RandomizeOptions;->value:I

    return v0
.end method
