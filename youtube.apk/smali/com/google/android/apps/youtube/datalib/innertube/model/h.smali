.class public Lcom/google/android/apps/youtube/datalib/innertube/model/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/model/u;


# instance fields
.field private a:Lcom/google/a/a/a/a/dj;

.field private b:Ljava/lang/CharSequence;

.field private c:Ljava/lang/CharSequence;

.field private d:Ljava/lang/CharSequence;

.field private e:Ljava/lang/CharSequence;

.field private f:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

.field private g:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

.field private final h:Lcom/google/android/apps/youtube/datalib/innertube/model/t;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/dj;)V
    .locals 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/dj;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->a:Lcom/google/a/a/a/a/dj;

    const/4 v0, 0x0

    iget-object v2, p1, Lcom/google/a/a/a/a/dj;->m:[Lcom/google/a/a/a/a/ak;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    iget-object v5, v4, Lcom/google/a/a/a/a/ak;->h:Lcom/google/a/a/a/a/kj;

    if-eqz v5, :cond_1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/t;

    iget-object v1, v4, Lcom/google/a/a/a/a/ak;->h:Lcom/google/a/a/a/a/kj;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/t;-><init>(Lcom/google/a/a/a/a/kj;)V

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->h:Lcom/google/android/apps/youtube/datalib/innertube/model/t;

    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->a:Lcom/google/a/a/a/a/dj;

    iget-object v0, v0, Lcom/google/a/a/a/a/dj;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->b:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->a:Lcom/google/a/a/a/a/dj;

    iget-object v0, v0, Lcom/google/a/a/a/a/dj;->d:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->a:Lcom/google/a/a/a/a/dj;

    iget-object v0, v0, Lcom/google/a/a/a/a/dj;->f:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->a:Lcom/google/a/a/a/a/dj;

    iget-object v0, v0, Lcom/google/a/a/a/a/dj;->f:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->c:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->a:Lcom/google/a/a/a/a/dj;

    iget-object v0, v0, Lcom/google/a/a/a/a/dj;->g:Lcom/google/a/a/a/a/fk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->a:Lcom/google/a/a/a/a/dj;

    iget-object v0, v0, Lcom/google/a/a/a/a/dj;->g:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->c:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->c:Ljava/lang/CharSequence;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, " \u00b7 "

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object v0, v1, v2

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->c:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->d:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->a:Lcom/google/a/a/a/a/dj;

    iget-object v0, v0, Lcom/google/a/a/a/a/dj;->h:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->d:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final e()Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->a:Lcom/google/a/a/a/a/dj;

    iget-object v0, v0, Lcom/google/a/a/a/a/dj;->i:Lcom/google/a/a/a/a/kz;

    return-object v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->e:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->a:Lcom/google/a/a/a/a/dj;

    iget-object v0, v0, Lcom/google/a/a/a/a/dj;->k:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->e:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->f:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->a:Lcom/google/a/a/a/a/dj;

    iget-object v1, v1, Lcom/google/a/a/a/a/dj;->c:Lcom/google/a/a/a/a/sx;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;-><init>(Lcom/google/a/a/a/a/sx;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->f:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->f:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->a:Lcom/google/a/a/a/a/dj;

    iget-object v0, v0, Lcom/google/a/a/a/a/dj;->p:Lcom/google/a/a/a/a/dk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->a:Lcom/google/a/a/a/a/dj;

    iget-object v0, v0, Lcom/google/a/a/a/a/dj;->p:Lcom/google/a/a/a/a/dk;

    iget-object v0, v0, Lcom/google/a/a/a/a/dk;->b:Lcom/google/a/a/a/a/mf;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->a:Lcom/google/a/a/a/a/dj;

    iget-object v1, v1, Lcom/google/a/a/a/a/dj;->p:Lcom/google/a/a/a/a/dk;

    iget-object v1, v1, Lcom/google/a/a/a/a/dk;->b:Lcom/google/a/a/a/a/mf;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;-><init>(Lcom/google/a/a/a/a/mf;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/h;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/Offlineability;

    return-object v0
.end method
