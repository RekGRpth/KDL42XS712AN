.class final Lcom/google/android/apps/youtube/datalib/innertube/model/media/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/k;->a:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    check-cast p1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    check-cast p2, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getHeight()I

    move-result v0

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getHeight()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getHeight()I

    move-result v0

    invoke-virtual {p2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getHeight()I

    move-result v1

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method
