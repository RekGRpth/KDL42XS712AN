.class public final Lcom/google/android/apps/youtube/datalib/innertube/ab;
.super Lcom/google/android/apps/youtube/datalib/innertube/a;
.source "SourceFile"


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/innertube/a;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/r;Lcom/google/android/apps/youtube/datalib/innertube/p;Lcom/android/volley/l;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/datalib/innertube/a;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/r;Lcom/google/android/apps/youtube/datalib/innertube/p;Lcom/android/volley/l;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/ac;)Lcom/google/a/a/a/a/ra;
    .locals 4

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/a/k;->a()Lcom/google/android/apps/youtube/datalib/a/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ab;->c:Lcom/android/volley/l;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ab;->a:Lcom/google/android/apps/youtube/datalib/innertube/r;

    const-class v3, Lcom/google/a/a/a/a/ra;

    invoke-virtual {v2, p1, v3, v0}, Lcom/google/android/apps/youtube/datalib/innertube/r;->a(Lcom/google/android/apps/youtube/datalib/innertube/s;Ljava/lang/Class;Lcom/google/android/apps/youtube/datalib/a/l;)Lcom/google/android/apps/youtube/datalib/innertube/q;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/volley/l;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/a/k;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/ra;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/InnerTubeServiceException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/ac;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/ac;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ab;->b:Lcom/google/android/apps/youtube/datalib/innertube/p;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/ac;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;B)V

    return-object v0
.end method
