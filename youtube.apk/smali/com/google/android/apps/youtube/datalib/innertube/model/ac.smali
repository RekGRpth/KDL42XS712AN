.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/a/a/a/a/ol;

.field private final b:Ljava/util/List;

.field private final c:I

.field private final d:[B


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/ol;[B)V
    .locals 6

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/ol;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->a:Lcom/google/a/a/a/a/ol;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->d:[B

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->b:Ljava/util/List;

    iget-object v0, p1, Lcom/google/a/a/a/a/ol;->c:[Lcom/google/a/a/a/a/om;

    array-length v0, v0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->c:I

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->c:I

    if-ge v0, v1, :cond_2

    iget-object v1, p1, Lcom/google/a/a/a/a/ol;->c:[Lcom/google/a/a/a/a/om;

    aget-object v1, v1, v0

    iget-object v3, v1, Lcom/google/a/a/a/a/om;->b:Lcom/google/a/a/a/a/oo;

    if-eqz v3, :cond_0

    new-instance v4, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;

    iget-boolean v1, p1, Lcom/google/a/a/a/a/ol;->h:Z

    iget v5, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->c:I

    if-eqz v1, :cond_1

    sub-int v1, v5, v0

    int-to-float v1, v1

    const/high16 v5, 0x40a00000    # 5.0f

    div-float/2addr v1, v5

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    const/4 v5, 0x0

    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    :goto_1
    iget-object v5, p1, Lcom/google/a/a/a/a/ol;->e:Ljava/lang/String;

    invoke-direct {v4, v3, v1, v5}, Lcom/google/android/apps/youtube/datalib/innertube/model/ad;-><init>(Lcom/google/a/a/a/a/oo;FLjava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->b:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->b:Ljava/util/List;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->a:Lcom/google/a/a/a/a/ol;

    iget-object v0, v0, Lcom/google/a/a/a/a/ol;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->a:Lcom/google/a/a/a/a/ol;

    iget v0, v0, Lcom/google/a/a/a/a/ol;->d:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->a:Lcom/google/a/a/a/a/ol;

    iget v0, v0, Lcom/google/a/a/a/a/ol;->d:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->a:Lcom/google/a/a/a/a/ol;

    iget-object v0, v0, Lcom/google/a/a/a/a/ol;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->a:Lcom/google/a/a/a/a/ol;

    iget-object v0, v0, Lcom/google/a/a/a/a/ol;->g:Lcom/google/a/a/a/a/fk;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->a:Lcom/google/a/a/a/a/ol;

    iget-object v0, v0, Lcom/google/a/a/a/a/ol;->g:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->a:Lcom/google/a/a/a/a/ol;

    iget v0, v0, Lcom/google/a/a/a/a/ol;->f:I

    return v0
.end method

.method public final g()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->a:Lcom/google/a/a/a/a/ol;

    iget-boolean v0, v0, Lcom/google/a/a/a/a/ol;->h:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->a:Lcom/google/a/a/a/a/ol;

    iget-boolean v0, v0, Lcom/google/a/a/a/a/ol;->i:Z

    return v0
.end method

.method public final i()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;->a:Lcom/google/a/a/a/a/ol;

    iget v0, v0, Lcom/google/a/a/a/a/ol;->j:I

    return v0
.end method
