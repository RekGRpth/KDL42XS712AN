.class public Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/legacy/model/r;


# instance fields
.field private A:Ljava/util/List;

.field private B:Ljava/util/List;

.field private C:Ljava/util/List;

.field private D:Ljava/util/List;

.field private E:Ljava/util/List;

.field private F:Ljava/util/List;

.field private G:Ljava/util/List;

.field private H:Ljava/util/List;

.field private I:Ljava/util/List;

.field private J:Ljava/util/List;

.field private K:Landroid/net/Uri;

.field private L:Landroid/net/Uri;

.field private M:J

.field private N:I

.field private O:Z

.field private P:Z

.field private Q:Z

.field private R:Z

.field private S:Z

.field private T:Lcom/google/a/a/a/a/ne;

.field private U:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

.field private V:J

.field private W:Z

.field private X:Z

.field private Y:Landroid/net/Uri;

.field private Z:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

.field public final a:I

.field private aa:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

.field private ab:Ljava/util/List;

.field private ac:J

.field private ad:Ljava/util/List;

.field private ae:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

.field private af:Ljava/lang/String;

.field private b:Ljava/util/List;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Landroid/net/Uri;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

.field private l:Ljava/lang/String;

.field private m:I

.field private n:Lcom/google/a/a/a/a/sb;

.field private o:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

.field private p:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

.field private q:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

.field private r:Landroid/net/Uri;

.field private s:Ljava/util/List;

.field private t:Ljava/util/List;

.field private u:Ljava/util/List;

.field private v:Ljava/util/List;

.field private w:Ljava/util/List;

.field private x:Ljava/util/List;

.field private y:Ljava/util/List;

.field private z:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->P:Z

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;->UNKNOWN:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->N:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->X:Z

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->Q:Z

    return-object p0
.end method

.method public final a(I)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->m:I

    return-object p0
.end method

.method public final a(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->ac:J

    return-object p0
.end method

.method public final a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Lcom/google/a/a/a/a/fj;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->n:Lcom/google/a/a/a/a/sb;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/a/a/a/a/sb;

    invoke-direct {v0}, Lcom/google/a/a/a/a/sb;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->n:Lcom/google/a/a/a/a/sb;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->n:Lcom/google/a/a/a/a/sb;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->n:Lcom/google/a/a/a/a/sb;

    iget-object v0, v0, Lcom/google/a/a/a/a/sb;->c:[Lcom/google/a/a/a/a/fj;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/a/a/a/a/fj;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/common/e/c;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/a/a/a/a/fj;

    iput-object v0, v1, Lcom/google/a/a/a/a/sb;->c:[Lcom/google/a/a/a/a/fj;

    return-object p0
.end method

.method public final a(Lcom/google/a/a/a/a/ne;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->T:Lcom/google/a/a/a/a/ne;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->U:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->p:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->q:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->o:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->ae:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->w:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->w:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->w:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->aa:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->e:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b:Ljava/util/List;

    return-object p0
.end method

.method public final a(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->O:Z

    return-object p0
.end method

.method public final b(I)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->N:I

    return-object p0
.end method

.method public final b(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->M:J

    return-object p0
.end method

.method public final b(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->s:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->s:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->s:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final b(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->Z:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final b(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->s:Ljava/util/List;

    return-object p0
.end method

.method public final b(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->P:Z

    return-object p0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->af:Ljava/lang/String;

    return-object v0
.end method

.method public synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    return-object v0
.end method

.method public final c(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->V:J

    return-object p0
.end method

.method public final c(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->t:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->t:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->t:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->t:Ljava/util/List;

    return-object p0
.end method

.method public final c(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->R:Z

    return-object p0
.end method

.method public final c()Z
    .locals 2

    const-string v0, "402"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 58

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->Q:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->s:Ljava/util/List;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->s:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->s:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/e/p;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "eid\\d=\\d+"

    const-string v4, "eid1=5"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/e/p;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->f(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->o:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->n:Lcom/google/a/a/a/a/sb;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->n:Lcom/google/a/a/a/a/sb;

    iget-object v2, v2, Lcom/google/a/a/a/a/sb;->c:[Lcom/google/a/a/a/a/fj;

    array-length v2, v2

    if-lez v2, :cond_1

    new-instance v2, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->n:Lcom/google/a/a/a/a/sb;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->m:I

    int-to-long v5, v5

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->ac:J

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;-><init>(Lcom/google/a/a/a/a/sb;Ljava/lang/String;JJZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->o:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->o:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->o:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getAllFormatStreams()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->o:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getAllFormatStreams()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isYouTubeHostedUri(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "modules"

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->S:Z

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->p:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    if-nez v2, :cond_4

    new-instance v2, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->p:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->q:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    if-nez v2, :cond_5

    new-instance v2, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->q:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    :cond_5
    new-instance v2, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->h:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->k:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->m:I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->o:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->p:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->q:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->r:Landroid/net/Uri;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->s:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->t:Ljava/util/List;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->u:Ljava/util/List;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->v:Ljava/util/List;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->w:Ljava/util/List;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->x:Ljava/util/List;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->y:Ljava/util/List;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->z:Ljava/util/List;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->A:Ljava/util/List;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->B:Ljava/util/List;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->C:Ljava/util/List;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->D:Ljava/util/List;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->E:Ljava/util/List;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->F:Ljava/util/List;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->G:Ljava/util/List;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->H:Ljava/util/List;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->I:Ljava/util/List;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->J:Ljava/util/List;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->K:Landroid/net/Uri;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->L:Landroid/net/Uri;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->O:Z

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->P:Z

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->M:J

    move-wide/from16 v41, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->N:I

    move/from16 v43, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->R:Z

    move/from16 v44, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->S:Z

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->T:Lcom/google/a/a/a/a/ne;

    move-object/from16 v46, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->U:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    move-object/from16 v47, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->V:J

    move-wide/from16 v48, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->W:Z

    move/from16 v50, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->X:Z

    move/from16 v51, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->Y:Landroid/net/Uri;

    move-object/from16 v52, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->Z:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-object/from16 v53, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->aa:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-object/from16 v54, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->ab:Ljava/util/List;

    move-object/from16 v55, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->ad:Ljava/util/List;

    move-object/from16 v56, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->ae:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-object/from16 v57, v0

    invoke-direct/range {v2 .. v57}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;Ljava/lang/String;ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Landroid/net/Uri;Landroid/net/Uri;ZZJIZZLcom/google/a/a/a/a/ne;Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;JZZLandroid/net/Uri;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;)V

    return-object v2
.end method

.method public final d(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->u:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->u:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->u:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->f:Ljava/lang/String;

    return-object p0
.end method

.method public final d(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->u:Ljava/util/List;

    return-object p0
.end method

.method public final d(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->S:Z

    return-object p0
.end method

.method public final e(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->v:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->v:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->v:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final e(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->g:Ljava/lang/String;

    return-object p0
.end method

.method public final e(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->v:Ljava/util/List;

    return-object p0
.end method

.method public final e(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->W:Z

    return-object p0
.end method

.method public final f(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->x:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->x:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->x:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final f(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->i:Ljava/lang/String;

    return-object p0
.end method

.method public final f(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->w:Ljava/util/List;

    return-object p0
.end method

.method public final f(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->X:Z

    return-object p0
.end method

.method public final g(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->y:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->y:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->y:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final g(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->j:Ljava/lang/String;

    return-object p0
.end method

.method public final g(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->x:Ljava/util/List;

    return-object p0
.end method

.method public final h(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->z:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->z:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->z:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final h(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->l:Ljava/lang/String;

    return-object p0
.end method

.method public final h(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->y:Ljava/util/List;

    return-object p0
.end method

.method public final i(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->A:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->A:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->A:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final i(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->af:Ljava/lang/String;

    return-object p0
.end method

.method public final i(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->z:Ljava/util/List;

    return-object p0
.end method

.method public final j(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->B:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->B:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->B:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final j(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->A:Ljava/util/List;

    return-object p0
.end method

.method public final k(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->C:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->C:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->C:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final k(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->B:Ljava/util/List;

    return-object p0
.end method

.method public final l(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->D:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->D:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->D:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final l(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->C:Ljava/util/List;

    return-object p0
.end method

.method public final m(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->E:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->E:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->E:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final m(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->D:Ljava/util/List;

    return-object p0
.end method

.method public final n(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->F:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->F:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->F:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final n(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->E:Ljava/util/List;

    return-object p0
.end method

.method public final o(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->G:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->G:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->G:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final o(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->F:Ljava/util/List;

    return-object p0
.end method

.method public final p(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->H:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->H:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->H:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final p(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->G:Ljava/util/List;

    return-object p0
.end method

.method public final q(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->I:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->I:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->I:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final q(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->H:Ljava/util/List;

    return-object p0
.end method

.method public final r(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->J:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->J:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->J:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final r(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->I:Ljava/util/List;

    return-object p0
.end method

.method public final s(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->h:Landroid/net/Uri;

    return-object p0
.end method

.method public final s(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->J:Ljava/util/List;

    return-object p0
.end method

.method public final t(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->r:Landroid/net/Uri;

    return-object p0
.end method

.method public final t(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->ab:Ljava/util/List;

    return-object p0
.end method

.method public final u(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->K:Landroid/net/Uri;

    return-object p0
.end method

.method public final u(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->ad:Ljava/util/List;

    return-object p0
.end method

.method public final v(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->L:Landroid/net/Uri;

    return-object p0
.end method

.method public final w(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->Y:Landroid/net/Uri;

    return-object p0
.end method
