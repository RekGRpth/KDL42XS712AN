.class public Lcom/google/android/apps/youtube/datalib/offline/o;
.super Lcom/google/android/apps/youtube/common/d/h;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/apps/youtube/datalib/offline/j;

.field private final c:Lcom/google/android/apps/youtube/common/network/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/datalib/offline/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/datalib/offline/o;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/a/a/g;Lcom/google/android/apps/youtube/datalib/offline/j;Lcom/google/android/apps/youtube/common/network/h;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/common/d/h;-><init>(Lcom/google/android/apps/youtube/a/a/g;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/offline/j;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/o;->b:Lcom/google/android/apps/youtube/datalib/offline/j;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/o;->c:Lcom/google/android/apps/youtube/common/network/h;

    return-void
.end method

.method public static a(J)Lcom/google/android/apps/youtube/a/a/g;
    .locals 4

    new-instance v0, Lcom/google/android/apps/youtube/a/a/g;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/g;-><init>()V

    sget-object v1, Lcom/google/android/apps/youtube/datalib/offline/o;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/g;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/g;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    add-long/2addr v1, p0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/a/a/g;->a(J)Lcom/google/android/apps/youtube/a/a/g;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x258

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/a/a/g;->b(J)Lcom/google/android/apps/youtube/a/a/g;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/offline/o;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/o;->c:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Flushing offline queue."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/o;->b:Lcom/google/android/apps/youtube/datalib/offline/j;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/offline/j;->a()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Not flushing offline queue because we have no network."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
