.class public Lcom/google/android/apps/youtube/datalib/offline/q;
.super Lcom/google/android/apps/youtube/common/d/h;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/apps/youtube/datalib/e/b;

.field private final c:Lcom/google/android/apps/youtube/datalib/offline/m;

.field private final d:Lcom/google/android/apps/youtube/common/d/j;

.field private final e:Lcom/google/android/apps/youtube/common/e/b;

.field private final f:Lcom/google/android/apps/youtube/datalib/config/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/datalib/offline/q;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/datalib/offline/q;->a:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/apps/youtube/a/a/g;Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/datalib/offline/m;Lcom/google/android/apps/youtube/common/d/j;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/datalib/config/c;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/common/d/h;-><init>(Lcom/google/android/apps/youtube/a/a/g;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/q;->b:Lcom/google/android/apps/youtube/datalib/e/b;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/offline/m;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/q;->c:Lcom/google/android/apps/youtube/datalib/offline/m;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/d/j;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/q;->d:Lcom/google/android/apps/youtube/common/d/j;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/q;->e:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/config/c;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/q;->f:Lcom/google/android/apps/youtube/datalib/config/c;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/datalib/offline/q;)Lcom/google/android/apps/youtube/datalib/offline/m;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/q;->c:Lcom/google/android/apps/youtube/datalib/offline/m;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/q;->b:Lcom/google/android/apps/youtube/datalib/e/b;

    const-string v0, "delayed_request"

    const v1, 0x323467f

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/e/b;->a(Ljava/lang/String;I)Lcom/google/android/apps/youtube/datalib/e/f;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/e/f;->a(Z)Lcom/google/android/apps/youtube/datalib/e/f;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/q;->c:Lcom/google/android/apps/youtube/datalib/offline/m;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/offline/m;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/e/f;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/e/f;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/offline/r;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/datalib/offline/r;-><init>(Lcom/google/android/apps/youtube/datalib/offline/q;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/e/f;->a(Lcom/google/android/apps/youtube/datalib/e/d;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/q;->b:Lcom/google/android/apps/youtube/datalib/e/b;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/a/b;->b:Lcom/android/volley/n;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/e/b;->a(Lcom/google/android/apps/youtube/datalib/e/f;Lcom/android/volley/n;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/q;->e:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x1e

    sub-long/2addr v0, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/offline/q;->f:Lcom/google/android/apps/youtube/datalib/config/c;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/datalib/config/c;->d()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/offline/q;->d:Lcom/google/android/apps/youtube/common/d/j;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/offline/q;->f:Lcom/google/android/apps/youtube/datalib/config/c;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/youtube/datalib/offline/s;->a(JLcom/google/android/apps/youtube/datalib/config/c;)Lcom/google/android/apps/youtube/a/a/g;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/common/d/j;->d(Lcom/google/android/apps/youtube/a/a/g;)V

    return-void
.end method
