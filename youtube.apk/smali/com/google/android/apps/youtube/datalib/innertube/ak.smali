.class public final Lcom/google/android/apps/youtube/datalib/innertube/ak;
.super Lcom/google/android/apps/youtube/datalib/innertube/b;
.source "SourceFile"


# instance fields
.field private A:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:J

.field private n:I

.field private o:I

.field private p:I

.field private q:Z

.field private r:Ljava/lang/String;

.field private s:Z

.field private t:I

.field private u:Ljava/lang/String;

.field private v:I

.field private w:I

.field private x:I

.field private y:Ljava/lang/String;

.field private z:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/b;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    iput v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->f:I

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->g:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->h:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->i:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->j:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->k:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->l:Ljava/lang/String;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->m:J

    iput v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->n:I

    iput v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->o:I

    iput v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->p:I

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->q:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->r:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->s:Z

    iput v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->t:I

    iput v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->v:I

    iput v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->w:I

    iput v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->x:I

    iput v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->z:I

    iput v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->A:I

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->f:I

    return-object p0
.end method

.method public final a(J)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->m:J

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Z)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->h:Z

    return-object p0
.end method

.method public final a()Ljava/util/Map;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->b:Ljava/util/Map;

    if-nez v0, :cond_1

    invoke-super {p0}, Lcom/google/android/apps/youtube/datalib/innertube/b;->a()Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->u:Ljava/lang/String;

    if-nez v0, :cond_0

    const/16 v0, 0x9

    invoke-static {v0}, Lcom/google/android/apps/youtube/core/utils/Util;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->u:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->b:Ljava/util/Map;

    const-string v1, "id"

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->b:Ljava/util/Map;

    const-string v1, "t"

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->u:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->b:Ljava/util/Map;

    return-object v0
.end method

.method public final b(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->n:I

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final b(Z)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->g:Z

    return-object p0
.end method

.method public final c(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->o:I

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->e:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Z)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->i:Z

    return-object p0
.end method

.method protected final c()V
    .locals 2

    const/4 v1, -0x1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->A:I

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->j:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->o:I

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->t:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->i:Z

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->y:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public final d(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->t:I

    return-object p0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->k:Ljava/lang/String;

    return-object p0
.end method

.method public final d(Z)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->j:Z

    return-object p0
.end method

.method public final e(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->v:I

    return-object p0
.end method

.method public final e(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->l:Ljava/lang/String;

    return-object p0
.end method

.method public final e(Z)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->q:Z

    return-object p0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    const-string v0, "player"

    return-object v0
.end method

.method public final f(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 1

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->w:I

    return-object p0
.end method

.method public final f(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->y:Ljava/lang/String;

    return-object p0
.end method

.method public final f(Z)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->s:Z

    return-object p0
.end method

.method public final synthetic f()Lcom/google/protobuf/nano/c;
    .locals 7

    const/4 v6, -0x1

    new-instance v0, Lcom/google/a/a/a/a/nv;

    invoke-direct {v0}, Lcom/google/a/a/a/a/nv;-><init>()V

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->h:Z

    iput-boolean v1, v0, Lcom/google/a/a/a/a/nv;->f:Z

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->g:Z

    iput-boolean v1, v0, Lcom/google/a/a/a/a/nv;->d:Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/nv;->c:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->i:Z

    iput-boolean v1, v0, Lcom/google/a/a/a/a/nv;->g:Z

    new-instance v1, Lcom/google/a/a/a/a/mx;

    invoke-direct {v1}, Lcom/google/a/a/a/a/mx;-><init>()V

    iput-object v1, v0, Lcom/google/a/a/a/a/nv;->e:Lcom/google/a/a/a/a/mx;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/ak;->d()Lcom/google/a/a/a/a/ii;

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/nv;->b:Lcom/google/a/a/a/a/ii;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/nv;->k:Ljava/lang/String;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/nv;->h:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->f:I

    if-ltz v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->f:I

    iput v1, v0, Lcom/google/a/a/a/a/nv;->i:I

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->j:Z

    if-nez v1, :cond_9

    new-instance v1, Lcom/google/a/a/a/a/do;

    invoke-direct {v1}, Lcom/google/a/a/a/a/do;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->k:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/a/a/a/a/do;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->l:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->l:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/a/a/a/a/do;->c:Ljava/lang/String;

    :cond_2
    iget-wide v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->m:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->m:J

    iput-wide v2, v1, Lcom/google/a/a/a/a/do;->e:J

    :cond_3
    iget v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->n:I

    if-eq v2, v6, :cond_4

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->n:I

    iput v2, v1, Lcom/google/a/a/a/a/do;->d:I

    :cond_4
    iget v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->p:I

    if-eq v2, v6, :cond_5

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->p:I

    iput v2, v1, Lcom/google/a/a/a/a/do;->f:I

    :cond_5
    iget v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->t:I

    if-eq v2, v6, :cond_6

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->t:I

    iput v2, v1, Lcom/google/a/a/a/a/do;->h:I

    :cond_6
    iget-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->q:Z

    iput-boolean v2, v1, Lcom/google/a/a/a/a/do;->k:Z

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->r:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/a/a/a/a/do;->l:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->s:Z

    iput-boolean v2, v1, Lcom/google/a/a/a/a/do;->j:Z

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->o:I

    iput v2, v1, Lcom/google/a/a/a/a/do;->g:I

    iget-object v2, v0, Lcom/google/a/a/a/a/nv;->e:Lcom/google/a/a/a/a/mx;

    iput-object v1, v2, Lcom/google/a/a/a/a/mx;->b:Lcom/google/a/a/a/a/do;

    :goto_0
    iget v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->z:I

    if-eq v1, v6, :cond_7

    new-instance v1, Lcom/google/a/a/a/a/qa;

    invoke-direct {v1}, Lcom/google/a/a/a/a/qa;-><init>()V

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->z:I

    iput v2, v1, Lcom/google/a/a/a/a/qa;->b:I

    iget-object v2, v0, Lcom/google/a/a/a/a/nv;->e:Lcom/google/a/a/a/a/mx;

    iput-object v1, v2, Lcom/google/a/a/a/a/mx;->e:Lcom/google/a/a/a/a/qa;

    :cond_7
    iget v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->A:I

    if-eq v1, v6, :cond_8

    new-instance v1, Lcom/google/a/a/a/a/ct;

    invoke-direct {v1}, Lcom/google/a/a/a/a/ct;-><init>()V

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->A:I

    iput v2, v1, Lcom/google/a/a/a/a/ct;->j:I

    iget-object v2, v0, Lcom/google/a/a/a/a/nv;->b:Lcom/google/a/a/a/a/ii;

    iput-object v1, v2, Lcom/google/a/a/a/a/ii;->h:Lcom/google/a/a/a/a/ct;

    :cond_8
    return-object v0

    :cond_9
    new-instance v1, Lcom/google/a/a/a/a/m;

    invoke-direct {v1}, Lcom/google/a/a/a/a/m;-><init>()V

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->v:I

    iput v2, v1, Lcom/google/a/a/a/a/m;->b:I

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->w:I

    iput v2, v1, Lcom/google/a/a/a/a/m;->c:I

    iget v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->x:I

    iput v2, v1, Lcom/google/a/a/a/a/m;->d:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->y:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/a/a/a/a/m;->e:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/a/a/a/a/nv;->e:Lcom/google/a/a/a/a/mx;

    iput-object v1, v2, Lcom/google/a/a/a/a/mx;->c:Lcom/google/a/a/a/a/m;

    goto :goto_0
.end method

.method public final g(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->x:I

    return-object p0
.end method

.method public final g(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->r:Ljava/lang/String;

    return-object p0
.end method

.method public final h(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->p:I

    return-object p0
.end method

.method public final i(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->z:I

    return-object p0
.end method

.method public final j(I)Lcom/google/android/apps/youtube/datalib/innertube/ak;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/ak;->A:I

    return-object p0
.end method
