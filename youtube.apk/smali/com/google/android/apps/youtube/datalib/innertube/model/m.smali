.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/a/a/a/a/ia;

.field private b:Lcom/google/a/a/a/a/rn;

.field private c:Lcom/google/a/a/a/a/rm;

.field private d:Lcom/google/a/a/a/a/rk;

.field private e:Lcom/google/a/a/a/a/rl;

.field private f:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/ia;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->a:Lcom/google/a/a/a/a/ia;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/a/a/a/a/rn;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->b:Lcom/google/a/a/a/a/rn;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->a:Lcom/google/a/a/a/a/ia;

    iget-object v0, v0, Lcom/google/a/a/a/a/ia;->b:Lcom/google/a/a/a/a/id;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->a:Lcom/google/a/a/a/a/ia;

    iget-object v0, v0, Lcom/google/a/a/a/a/ia;->b:Lcom/google/a/a/a/a/id;

    iget-object v0, v0, Lcom/google/a/a/a/a/id;->b:Lcom/google/a/a/a/a/rn;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->b:Lcom/google/a/a/a/a/rn;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->b:Lcom/google/a/a/a/a/rn;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/a/a/a/a/rm;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->c:Lcom/google/a/a/a/a/rm;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->a:Lcom/google/a/a/a/a/ia;

    iget-object v0, v0, Lcom/google/a/a/a/a/ia;->c:Lcom/google/a/a/a/a/hw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->a:Lcom/google/a/a/a/a/ia;

    iget-object v0, v0, Lcom/google/a/a/a/a/ia;->c:Lcom/google/a/a/a/a/hw;

    iget-object v0, v0, Lcom/google/a/a/a/a/hw;->b:Lcom/google/a/a/a/a/rm;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->c:Lcom/google/a/a/a/a/rm;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->c:Lcom/google/a/a/a/a/rm;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/a/a/a/a/rk;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->d:Lcom/google/a/a/a/a/rk;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->b()Lcom/google/a/a/a/a/rm;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->b()Lcom/google/a/a/a/a/rm;

    move-result-object v0

    iget-object v0, v0, Lcom/google/a/a/a/a/rm;->e:Lcom/google/a/a/a/a/ht;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->b()Lcom/google/a/a/a/a/rm;

    move-result-object v0

    iget-object v0, v0, Lcom/google/a/a/a/a/rm;->e:Lcom/google/a/a/a/a/ht;

    iget-object v0, v0, Lcom/google/a/a/a/a/ht;->b:Lcom/google/a/a/a/a/rk;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->d:Lcom/google/a/a/a/a/rk;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->d:Lcom/google/a/a/a/a/rk;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/google/a/a/a/a/rl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->e:Lcom/google/a/a/a/a/rl;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->b()Lcom/google/a/a/a/a/rm;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->b()Lcom/google/a/a/a/a/rm;

    move-result-object v0

    iget-object v0, v0, Lcom/google/a/a/a/a/rm;->f:Lcom/google/a/a/a/a/hu;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->b()Lcom/google/a/a/a/a/rm;

    move-result-object v0

    iget-object v0, v0, Lcom/google/a/a/a/a/rm;->f:Lcom/google/a/a/a/a/hu;

    iget-object v0, v0, Lcom/google/a/a/a/a/hu;->b:Lcom/google/a/a/a/a/rl;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->e:Lcom/google/a/a/a/a/rl;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->e:Lcom/google/a/a/a/a/rl;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->f:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->a:Lcom/google/a/a/a/a/ia;

    iget-object v0, v0, Lcom/google/a/a/a/a/ia;->d:[Lcom/google/a/a/a/a/hx;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->f:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/m;->f:Ljava/util/List;

    return-object v0
.end method
