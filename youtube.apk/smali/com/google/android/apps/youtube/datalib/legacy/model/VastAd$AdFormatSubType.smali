.class public final enum Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

.field public static final enum NONE:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

.field public static final enum SKIPPABLE:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

.field public static final enum SURVEY:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;


# instance fields
.field public final subType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    const-string v1, "NONE"

    const-string v2, "0"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;->NONE:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    const-string v1, "SKIPPABLE"

    const-string v2, "1"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;->SKIPPABLE:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    const-string v1, "SURVEY"

    const-string v2, "3"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;->SURVEY:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;->NONE:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;->SKIPPABLE:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;->SURVEY:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;->$VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;->subType:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;->$VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    return-object v0
.end method
