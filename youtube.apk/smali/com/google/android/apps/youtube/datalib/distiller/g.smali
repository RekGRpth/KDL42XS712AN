.class public abstract Lcom/google/android/apps/youtube/datalib/distiller/g;
.super Lcom/google/android/apps/youtube/datalib/distiller/i;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/datalib/distiller/model/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/distiller/i;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/distiller/model/c;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/distiller/model/c;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/g;->a:Lcom/google/android/apps/youtube/datalib/distiller/model/c;

    return-void
.end method

.method protected final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/g;->a:Lcom/google/android/apps/youtube/datalib/distiller/model/c;

    instance-of v0, v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/g;->a:Lcom/google/android/apps/youtube/datalib/distiller/model/c;

    instance-of v0, v0, Lcom/google/android/apps/youtube/datalib/distiller/model/b;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/g;->a:Lcom/google/android/apps/youtube/datalib/distiller/model/c;

    instance-of v0, v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "activities/"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/g;->a:Lcom/google/android/apps/youtube/datalib/distiller/model/c;

    check-cast v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "comments/"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/g;->a:Lcom/google/android/apps/youtube/datalib/distiller/model/c;

    check-cast v0, Lcom/google/android/apps/youtube/datalib/distiller/model/b;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/distiller/model/b;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
