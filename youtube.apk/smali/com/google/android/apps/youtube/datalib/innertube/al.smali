.class public final Lcom/google/android/apps/youtube/datalib/innertube/al;
.super Lcom/google/android/apps/youtube/datalib/innertube/a;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/android/apps/youtube/common/c/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/r;Lcom/google/android/apps/youtube/datalib/innertube/p;Lcom/android/volley/l;Lcom/google/android/apps/youtube/common/c/a;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/datalib/innertube/a;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/r;Lcom/google/android/apps/youtube/datalib/innertube/p;Lcom/android/volley/l;)V

    iput-object p4, p0, Lcom/google/android/apps/youtube/datalib/innertube/al;->d:Lcom/google/android/apps/youtube/common/c/a;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/datalib/innertube/al;Lcom/google/android/apps/youtube/datalib/innertube/an;)V
    .locals 6

    const/4 v5, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/innertube/an;->a(Lcom/google/android/apps/youtube/datalib/innertube/an;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/oc;

    iget v2, v0, Lcom/google/a/a/a/a/oc;->e:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/al;->d:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v3, Lcom/google/android/apps/youtube/datalib/innertube/ao;

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/innertube/an;->b(Lcom/google/android/apps/youtube/datalib/innertube/an;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, v0, Lcom/google/a/a/a/a/oc;->b:Ljava/lang/String;

    invoke-direct {v3, v4, v0, v5}, Lcom/google/android/apps/youtube/datalib/innertube/ao;-><init>(Ljava/lang/String;Ljava/lang/String;B)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget v2, v0, Lcom/google/a/a/a/a/oc;->e:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/al;->d:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v3, Lcom/google/android/apps/youtube/datalib/innertube/ap;

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/innertube/an;->b(Lcom/google/android/apps/youtube/datalib/innertube/an;)Ljava/lang/String;

    move-result-object v4

    iget-object v0, v0, Lcom/google/a/a/a/a/oc;->c:Ljava/lang/String;

    invoke-direct {v3, v4, v0, v5}, Lcom/google/android/apps/youtube/datalib/innertube/ap;-><init>(Ljava/lang/String;Ljava/lang/String;B)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/common/c/a;->c(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/an;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/an;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/al;->b:Lcom/google/android/apps/youtube/datalib/innertube/p;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/an;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/an;Lcom/google/android/apps/youtube/datalib/a/l;)V
    .locals 4

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/am;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/apps/youtube/datalib/innertube/am;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/al;Lcom/google/android/apps/youtube/datalib/a/l;Lcom/google/android/apps/youtube/datalib/innertube/an;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/al;->c:Lcom/android/volley/l;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/al;->a:Lcom/google/android/apps/youtube/datalib/innertube/r;

    const-class v3, Lcom/google/a/a/a/a/oe;

    invoke-virtual {v2, p1, v3, v0}, Lcom/google/android/apps/youtube/datalib/innertube/r;->a(Lcom/google/android/apps/youtube/datalib/innertube/s;Ljava/lang/Class;Lcom/google/android/apps/youtube/datalib/a/l;)Lcom/google/android/apps/youtube/datalib/innertube/q;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/volley/l;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method
