.class public Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final DASH_PERIOD_START_MILLIS:I = 0x0

.field private static final HLS_ITAG:I = 0x5d

.field private static final HLS_MIME_TYPE:Ljava/lang/String; = "application/x-mpegURL"

.field private static final UNKNOWN_ITAG:I = 0x0

.field private static final UNKNOWN_MIME_TYPE:Ljava/lang/String; = "unknown"

.field private static final WIDEVINE_MIME_TYPE:Ljava/lang/String; = "video/wvm"


# instance fields
.field private final formatStreamProto:Lcom/google/a/a/a/a/fj;

.field private uri:Landroid/net/Uri;

.field private final videoDurationMillis:J

.field private final videoId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/a;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/a;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/a/a/a/a/fj;Ljava/lang/String;J)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoId:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoDurationMillis:J

    iget-object v0, p1, Lcom/google/a/a/a/a/fj;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->uri:Landroid/net/Uri;

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->uri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/o;->a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    const-string v1, "dnc"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/common/e/o;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/common/e/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/e/o;->a()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->uri:Landroid/net/Uri;

    :cond_0
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->uri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoDurationMillis:J

    return-wide v0
.end method

.method public static forHls(Landroid/net/Uri;Ljava/lang/String;J)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 2

    new-instance v0, Lcom/google/a/a/a/a/fj;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fj;-><init>()V

    const/16 v1, 0x5d

    iput v1, v0, Lcom/google/a/a/a/a/fj;->b:I

    const-string v1, "application/x-mpegURL"

    iput-object v1, v0, Lcom/google/a/a/a/a/fj;->f:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/fj;->c:Ljava/lang/String;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-direct {v1, v0, p1, p2, p3}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;-><init>(Lcom/google/a/a/a/a/fj;Ljava/lang/String;J)V

    return-object v1
.end method

.method public static forPreload(Landroid/net/Uri;Ljava/lang/String;J)Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 4

    new-instance v0, Lcom/google/a/a/a/a/fj;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fj;-><init>()V

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/a/a/a/a/fj;->b:I

    const-string v1, "unknown"

    iput-object v1, v0, Lcom/google/a/a/a/a/fj;->f:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/fj;->c:Ljava/lang/String;

    iput-wide p2, v0, Lcom/google/a/a/a/a/fj;->m:J

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    const-wide/16 v2, 0x0

    invoke-direct {v1, v0, p1, v2, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;-><init>(Lcom/google/a/a/a/a/fj;Ljava/lang/String;J)V

    return-object v1
.end method


# virtual methods
.method public buildUpon()Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;B)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    iget-wide v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoDurationMillis:J

    iget-wide v4, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoDurationMillis:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoId:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    iget-object v3, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;Lcom/google/protobuf/nano/c;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public getContentLength()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    iget-wide v0, v0, Lcom/google/a/a/a/a/fj;->m:J

    return-wide v0
.end method

.method public getFormatStreamProto()Lcom/google/a/a/a/a/fj;
    .locals 2

    :try_start_0
    new-instance v0, Lcom/google/a/a/a/a/fj;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fj;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    invoke-static {v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/fj;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public getHeight()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    iget v0, v0, Lcom/google/a/a/a/a/fj;->i:I

    return v0
.end method

.method public getItag()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    iget v0, v0, Lcom/google/a/a/a/a/fj;->b:I

    return v0
.end method

.method public getLastModified()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    iget-wide v0, v0, Lcom/google/a/a/a/a/fj;->l:J

    return-wide v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    iget-object v0, v0, Lcom/google/a/a/a/a/fj;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getRepresentation(Ljava/lang/String;)Lcom/google/android/exoplayer/b/a/a;
    .locals 23

    new-instance v2, Lcom/google/android/exoplayer/a/m;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    iget-object v4, v4, Lcom/google/a/a/a/a/fj;->f:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getWidth()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getHeight()I

    move-result v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    iget v9, v9, Lcom/google/a/a/a/a/fj;->g:I

    div-int/lit8 v9, v9, 0x8

    invoke-direct/range {v2 .. v9}, Lcom/google/android/exoplayer/a/m;-><init>(ILjava/lang/String;IIIII)V

    new-instance v3, Lcom/google/android/exoplayer/b/a/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    iget-wide v5, v5, Lcom/google/a/a/a/a/fj;->l:J

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getUriWithCpn(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getContentLength()J

    move-result-wide v9

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    iget-object v7, v7, Lcom/google/a/a/a/a/fj;->j:Lcom/google/a/a/a/a/pp;

    iget-wide v11, v7, Lcom/google/a/a/a/a/pp;->d:J

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    iget-object v7, v7, Lcom/google/a/a/a/a/fj;->j:Lcom/google/a/a/a/a/pp;

    iget-wide v13, v7, Lcom/google/a/a/a/a/pp;->e:J

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    iget-object v7, v7, Lcom/google/a/a/a/a/fj;->k:Lcom/google/a/a/a/a/pp;

    iget-wide v15, v7, Lcom/google/a/a/a/a/pp;->d:J

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    iget-object v7, v7, Lcom/google/a/a/a/a/fj;->k:Lcom/google/a/a/a/a/pp;

    iget-wide v0, v7, Lcom/google/a/a/a/a/pp;->e:J

    move-wide/from16 v17, v0

    const-wide/16 v19, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoDurationMillis:J

    move-wide/from16 v21, v0

    move-object v7, v2

    invoke-direct/range {v3 .. v22}, Lcom/google/android/exoplayer/b/a/a;-><init>(Ljava/lang/String;JLcom/google/android/exoplayer/a/m;Landroid/net/Uri;JJJJJJJ)V

    return-object v3
.end method

.method public getSimpleMimeType()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getMimeType()Ljava/lang/String;

    move-result-object v0

    const-string v1, ";"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->uri:Landroid/net/Uri;

    return-object v0
.end method

.method public getUriWithCpn(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->uri:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "cpn"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getVideoDurationMillis()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoDurationMillis:J

    return-wide v0
.end method

.method public getVideoId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoId:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    iget v0, v0, Lcom/google/a/a/a/a/fj;->h:I

    return v0
.end method

.method public hashCode()I
    .locals 6

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoDurationMillis:J

    iget-wide v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoDurationMillis:J

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoId:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    invoke-static {v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    goto :goto_1
.end method

.method public is3D()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    iget-boolean v0, v0, Lcom/google/a/a/a/a/fj;->r:Z

    return v0
.end method

.method public isAdaptive()Z
    .locals 2

    sget-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/d;->a:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isFile()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->uri:Landroid/net/Uri;

    const-string v1, "file"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isHD()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getHeight()I

    move-result v0

    const/16 v1, 0x2d0

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHls()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v0

    const/16 v1, 0x5d

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLocal()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->uri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->b(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public isWidevine()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getMimeType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "video/wvm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    const-string v0, "v:{%s} t:{%d} i:{%d} s:{%dx%d} m:{%s} u:{%s}"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoDurationMillis:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getWidth()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getHeight()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getMimeType()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->uri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->formatStreamProto:Lcom/google/a/a/a/a/fj;

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/e/j;->a(Landroid/os/Parcel;Lcom/google/protobuf/nano/c;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoDurationMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
