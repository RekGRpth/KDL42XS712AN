.class public Lcom/google/android/apps/youtube/datalib/legacy/model/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:F

.field private e:I

.field private f:Landroid/net/Uri;

.field private g:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;
    .locals 8

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->f:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->b:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->c:Z

    iget v5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->d:F

    iget-object v6, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->g:Landroid/net/Uri;

    iget v7, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->e:I

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;ZFLandroid/net/Uri;I)V

    return-object v0
.end method

.method public final a(F)Lcom/google/android/apps/youtube/datalib/legacy/model/l;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->c:Z

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->d:F

    return-object p0
.end method

.method public final a(I)Lcom/google/android/apps/youtube/datalib/legacy/model/l;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->e:I

    return-object p0
.end method

.method public final a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/l;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->g:Landroid/net/Uri;

    return-object p0
.end method

.method public final b(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/l;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/l;->f:Landroid/net/Uri;

    return-object p0
.end method
