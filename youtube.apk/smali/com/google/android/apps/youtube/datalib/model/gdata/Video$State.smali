.class public final enum Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum ACCOUNT_SUSPENDED:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum BLOCKED_BY_OWNER:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum BLOCKED_FOR_CLIENT_APP:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum CANT_PROCESS:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum COPYRIGHT:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum COUNTRY_RESTRICTED:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum DELETED:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum DUPLICATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum EMPTY:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum INAPPROPRIATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum INVALID_FORMAT:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum NOT_AVAILABLE_ON_MOBILE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum PRIVATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum PROCESSING:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum TERMS_OF_USE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum TOO_SMALL:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum UNSUPPORTED_CODEC:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public static final enum VIDEO_TOO_LONG:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;


# instance fields
.field public final explanationId:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "PLAYABLE"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "PROCESSING"

    sget v2, Lcom/google/android/youtube/p;->eI:I

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PROCESSING:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "DELETED"

    sget v2, Lcom/google/android/youtube/p;->hd:I

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->DELETED:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "COUNTRY_RESTRICTED"

    sget v2, Lcom/google/android/youtube/p;->gP:I

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->COUNTRY_RESTRICTED:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "NOT_AVAILABLE_ON_MOBILE"

    sget v2, Lcom/google/android/youtube/p;->gQ:I

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->NOT_AVAILABLE_ON_MOBILE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "PRIVATE"

    const/4 v2, 0x5

    sget v3, Lcom/google/android/youtube/p;->gM:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PRIVATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "BLOCKED_FOR_CLIENT_APP"

    const/4 v2, 0x6

    sget v3, Lcom/google/android/youtube/p;->J:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->BLOCKED_FOR_CLIENT_APP:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "COPYRIGHT"

    const/4 v2, 0x7

    sget v3, Lcom/google/android/youtube/p;->gJ:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->COPYRIGHT:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "INAPPROPRIATE"

    const/16 v2, 0x8

    sget v3, Lcom/google/android/youtube/p;->he:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->INAPPROPRIATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "DUPLICATE"

    const/16 v2, 0x9

    sget v3, Lcom/google/android/youtube/p;->ba:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->DUPLICATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "TERMS_OF_USE"

    const/16 v2, 0xa

    sget v3, Lcom/google/android/youtube/p;->hc:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->TERMS_OF_USE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "ACCOUNT_SUSPENDED"

    const/16 v2, 0xb

    sget v3, Lcom/google/android/youtube/p;->gv:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->ACCOUNT_SUSPENDED:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "VIDEO_TOO_LONG"

    const/16 v2, 0xc

    sget v3, Lcom/google/android/youtube/p;->gZ:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->VIDEO_TOO_LONG:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "BLOCKED_BY_OWNER"

    const/16 v2, 0xd

    sget v3, Lcom/google/android/youtube/p;->I:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->BLOCKED_BY_OWNER:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "CANT_PROCESS"

    const/16 v2, 0xe

    sget v3, Lcom/google/android/youtube/p;->gH:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->CANT_PROCESS:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "INVALID_FORMAT"

    const/16 v2, 0xf

    sget v3, Lcom/google/android/youtube/p;->gL:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->INVALID_FORMAT:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "UNSUPPORTED_CODEC"

    const/16 v2, 0x10

    sget v3, Lcom/google/android/youtube/p;->ha:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->UNSUPPORTED_CODEC:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "EMPTY"

    const/16 v2, 0x11

    sget v3, Lcom/google/android/youtube/p;->gK:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->EMPTY:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const-string v1, "TOO_SMALL"

    const/16 v2, 0x12

    sget v3, Lcom/google/android/youtube/p;->gN:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->TOO_SMALL:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    const/16 v0, 0x13

    new-array v0, v0, [Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PROCESSING:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->DELETED:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->COUNTRY_RESTRICTED:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->NOT_AVAILABLE_ON_MOBILE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PRIVATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->BLOCKED_FOR_CLIENT_APP:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->COPYRIGHT:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->INAPPROPRIATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->DUPLICATE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->TERMS_OF_USE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->ACCOUNT_SUSPENDED:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->VIDEO_TOO_LONG:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->BLOCKED_BY_OWNER:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->CANT_PROCESS:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->INVALID_FORMAT:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->UNSUPPORTED_CODEC:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->EMPTY:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->TOO_SMALL:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->$VALUES:[Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->explanationId:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->$VALUES:[Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    return-object v0
.end method
