.class public final Lcom/google/android/apps/youtube/datalib/innertube/g;
.super Lcom/google/android/apps/youtube/datalib/innertube/b;
.source "SourceFile"


# instance fields
.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/b;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/g;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/g;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/g;->e:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/g;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/g;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/g;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/g;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/g;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/g;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/g;->e:Ljava/lang/String;

    return-object p0
.end method

.method protected final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/g;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/g;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/g;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    const-string v0, "browse"

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/nano/c;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/g;->b()V

    new-instance v0, Lcom/google/a/a/a/a/aq;

    invoke-direct {v0}, Lcom/google/a/a/a/a/aq;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/g;->d()Lcom/google/a/a/a/a/ii;

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/aq;->b:Lcom/google/a/a/a/a/ii;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/g;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/aq;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/g;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/aq;->f:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/g;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/aq;->d:Ljava/lang/String;

    return-object v0
.end method
