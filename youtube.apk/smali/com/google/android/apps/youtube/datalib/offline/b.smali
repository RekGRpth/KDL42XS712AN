.class final Lcom/google/android/apps/youtube/datalib/offline/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/datalib/offline/l;

.field final synthetic b:Lcom/google/android/apps/youtube/datalib/offline/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/datalib/offline/a;Lcom/google/android/apps/youtube/datalib/offline/l;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/offline/b;->b:Lcom/google/android/apps/youtube/datalib/offline/a;

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/offline/b;->a:Lcom/google/android/apps/youtube/datalib/offline/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/b;->b:Lcom/google/android/apps/youtube/datalib/offline/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/offline/a;->a(Lcom/google/android/apps/youtube/datalib/offline/a;)Lcom/google/android/apps/youtube/datalib/offline/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/offline/m;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/b;->a:Lcom/google/android/apps/youtube/datalib/offline/l;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/offline/l;->a()Lcom/google/android/apps/youtube/a/a/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/b;->b:Lcom/google/android/apps/youtube/datalib/offline/a;

    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/offline/a;->b(Lcom/google/android/apps/youtube/datalib/offline/a;)Lcom/google/android/apps/youtube/common/database/d;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/a/a/c;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/youtube/common/database/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/b;->b:Lcom/google/android/apps/youtube/datalib/offline/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/offline/a;->c(Lcom/google/android/apps/youtube/datalib/offline/a;)Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Dispatching stored offline request immediately."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/b;->b:Lcom/google/android/apps/youtube/datalib/offline/a;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/offline/a;->a()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Queuing stored offline request."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
