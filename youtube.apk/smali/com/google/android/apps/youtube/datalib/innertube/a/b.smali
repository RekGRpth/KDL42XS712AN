.class public final Lcom/google/android/apps/youtube/datalib/innertube/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;
    .locals 9

    const/4 v1, 0x0

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    iget-object v7, p0, Lcom/google/a/a/a/a/fk;->b:[Lcom/google/a/a/a/a/sc;

    array-length v8, v7

    move v4, v1

    move v0, v1

    move v6, v1

    :goto_1
    if-ge v4, v8, :cond_4

    aget-object v3, v7, v4

    iget-object v5, v3, Lcom/google/a/a/a/a/sc;->b:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v0

    iget-object v0, v3, Lcom/google/a/a/a/a/sc;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-boolean v0, v3, Lcom/google/a/a/a/a/sc;->c:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    iget-boolean v3, v3, Lcom/google/a/a/a/a/sc;->d:Z

    if-eqz v3, :cond_3

    const/4 v3, 0x2

    :goto_3
    or-int/2addr v0, v3

    if-eqz v0, :cond_1

    new-instance v3, Landroid/text/style/StyleSpan;

    invoke-direct {v3, v0}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v0, 0x21

    invoke-virtual {v2, v3, v6, v5, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v6, v5

    move v0, v5

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    move v3, v1

    goto :goto_3

    :cond_4
    move-object v0, v2

    goto :goto_0
.end method

.method public static a(J)Lcom/google/a/a/a/a/fk;
    .locals 4

    new-instance v0, Lcom/google/a/a/a/a/fk;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fk;-><init>()V

    new-instance v1, Lcom/google/a/a/a/a/sc;

    invoke-direct {v1}, Lcom/google/a/a/a/a/sc;-><init>()V

    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/a/a/a/a/sc;->b:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/a/a/a/a/sc;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    iput-object v2, v0, Lcom/google/a/a/a/a/fk;->b:[Lcom/google/a/a/a/a/sc;

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/youtube/datalib/innertube/model/ap;II)Lcom/google/android/apps/youtube/datalib/innertube/model/ao;
    .locals 6

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-ltz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    if-ltz p2, :cond_2

    :goto_1
    invoke-static {v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;->a()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    return-object v3

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->b()I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ao;->c()I

    move-result v5

    sub-int v5, p2, v5

    mul-int/2addr v2, v2

    mul-int/2addr v5, v5

    add-int/2addr v2, v5

    if-eqz v3, :cond_4

    if-ge v2, v1, :cond_5

    :cond_4
    move-object v1, v0

    move v0, v2

    :goto_3
    move-object v3, v1

    move v1, v0

    goto :goto_2

    :cond_5
    move v0, v1

    move-object v1, v3

    goto :goto_3
.end method
