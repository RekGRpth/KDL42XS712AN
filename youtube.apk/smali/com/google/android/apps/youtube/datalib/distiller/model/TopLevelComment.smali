.class public Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;
.super Lcom/google/android/apps/youtube/datalib/distiller/model/c;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Z

.field private final d:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

.field private e:Ljava/util/List;

.field private f:I

.field private g:Ljava/lang/String;

.field private final h:Z

.field private final i:Z

.field private j:Z


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 5

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/distiller/model/c;-><init>(Lorg/json/JSONObject;)V

    const-string v0, "id"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->a:Ljava/lang/String;

    const-string v0, "object"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "statusForViewer"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "canComment"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->c:Z

    const-string v1, "replies"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "totalItems"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->f:I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->e:Ljava/util/List;

    const-string v1, "items"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "items"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->e:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/youtube/datalib/distiller/model/b;

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    invoke-direct {v3, v4, p0}, Lcom/google/android/apps/youtube/datalib/distiller/model/b;-><init>(Lorg/json/JSONObject;Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v0, "provider"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "provider"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "Youtube"

    const-string v2, "title"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->h:Z

    :goto_1
    const-string v0, "access"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v0, "description"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "description"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->b(Lorg/json/JSONObject;)Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->d:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    const-string v0, "verb"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "post"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->i:Z

    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->h:Z

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private static b(Lorg/json/JSONObject;)Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;
    .locals 4

    const-string v0, "items"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "items"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_7

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "type"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->PRIVATE:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    :goto_1
    return-object v0

    :cond_0
    const-string v3, "type"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "private"

    invoke-static {v3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "limited"

    invoke-static {v3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->PRIVATE:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    goto :goto_1

    :cond_2
    const-string v3, "public"

    invoke-static {v3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->PUBLIC:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    goto :goto_1

    :cond_3
    const-string v3, "extendedCircles"

    invoke-static {v3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->EXTENDED_CIRCLES:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    goto :goto_1

    :cond_4
    const-string v3, "domain"

    invoke-static {v3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    sget-object v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->DOMAIN:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    goto :goto_1

    :cond_5
    const-string v3, "square"

    invoke-static {v3, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    sget-object v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->SQUARE:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    goto :goto_1

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_7
    sget-object v0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;->OTHER:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    goto :goto_1
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;)V
    .locals 5

    const-string v0, "items"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    new-instance v3, Lcom/google/android/apps/youtube/datalib/distiller/model/b;

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    invoke-direct {v3, v4, p0}, Lcom/google/android/apps/youtube/datalib/distiller/model/b;-><init>(Lorg/json/JSONObject;Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->e:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->e:Ljava/util/List;

    const-string v0, "nextPageToken"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "nextPageToken"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->g:Ljava/lang/String;

    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->f:I

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->j:Z

    return-void
.end method

.method public final a()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->g:Ljava/lang/String;

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->f:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gt v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final m()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->j:Z

    return v0
.end method

.method public final n()Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->d:Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment$AclType;

    return-object v0
.end method

.method public final o()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->c:Z

    return v0
.end method

.method public final p()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->i:Z

    return v0
.end method

.method public final q()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/model/TopLevelComment;->h:Z

    return v0
.end method
