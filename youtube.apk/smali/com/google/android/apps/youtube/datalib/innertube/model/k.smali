.class public Lcom/google/android/apps/youtube/datalib/innertube/model/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/a/a/a/a/pk;

.field private b:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/pk;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/pk;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/k;->a:Lcom/google/a/a/a/a/pk;

    return-void
.end method

.method public static a(Lcom/google/a/a/a/a/pk;)Z
    .locals 1

    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/a/a/a/a/pk;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/k;->b:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/k;->a:Lcom/google/a/a/a/a/pk;

    iget-object v0, v0, Lcom/google/a/a/a/a/pk;->l:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/k;->b:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/k;->b:Ljava/util/List;

    return-object v0
.end method
