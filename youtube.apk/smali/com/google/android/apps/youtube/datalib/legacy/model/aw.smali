.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/aw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/common/e/b;

.field private final b:Ljava/util/TimeZone;

.field private c:J

.field private d:J

.field private e:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 1

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;-><init>(Lcom/google/android/apps/youtube/common/e/b;Ljava/util/TimeZone;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/youtube/common/e/b;Ljava/util/TimeZone;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TimeZone;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->b:Ljava/util/TimeZone;

    return-void
.end method

.method private a(Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 7

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getApiContext()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "p"

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getApiContext()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "m.d-"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-wide v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->d:J

    iget-wide v5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->c:J

    sub-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v0, "m.v"

    const-string v3, "2"

    invoke-virtual {v2, v0, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-wide v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->d:J

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->b:Ljava/util/TimeZone;

    iget-wide v5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->d:J

    invoke-virtual {v0, v5, v6}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    int-to-long v5, v0

    add-long/2addr v3, v5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "m.lt-"

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    long-to-float v3, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-long v3, v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getType()Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->MULTI_SELECT:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    if-eq v0, v3, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getType()Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->SINGLE_ANSWERS:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    if-ne v0, v3, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "r.o-"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getAnswerOrdering()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->e:Ljava/util/List;

    if-eqz v0, :cond_3

    const-string v0, "t"

    const-string v3, "a"

    invoke-virtual {v2, v0, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "r.r-"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getAnswers()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "m.f-"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v0, "t"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "a"

    if-ne v0, v1, :cond_5

    const-string v0, "t"

    const-string v1, "pa"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_4
    :goto_1
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0

    :cond_5
    const-string v0, "t"

    const-string v1, "nr"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;)Ljava/util/List;
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->getQuestions()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getBeaconUris()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->c:J

    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->d:J

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->e:Ljava/util/List;

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->a:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->d:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/aw;->e:Ljava/util/List;

    return-void
.end method
