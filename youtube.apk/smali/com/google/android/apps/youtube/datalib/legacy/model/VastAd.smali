.class public Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/apps/youtube/datalib/legacy/a/a;


# static fields
.field private static final ADSENSE_CONVERSION_LABEL:Ljava/lang/String; = "label"

.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final EMPTY_AD:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

.field private static final MSEC_IN_SECOND:I = 0x3e8

.field public static final SURVEY_AD_SYSTEM:Ljava/lang/String; = "402"

.field public static final UNSPECIFIED_FREQUENCY_CAP:I = -0x1

.field public static final VAST_VERSION_2:I = 0x2

.field public static final VAST_VERSION_3:I = 0x3

.field private static final YOUTUBE_ANNOTATION_FRAGMENT_FLAG:Ljava/lang/String; = "modules"


# instance fields
.field public final adAnnotations:Lcom/google/a/a/a/a/ne;

.field private final adBreakId:Ljava/lang/String;

.field private final adFormat:Ljava/lang/String;

.field private final adIds:Ljava/lang/String;

.field public final adInfoCards:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

.field private final adOwnerName:Ljava/lang/String;

.field private final adOwnerUri:Landroid/net/Uri;

.field private final adSenseBaseConversionUri:Landroid/net/Uri;

.field private final adSystems:Ljava/lang/String;

.field private final adVideoId:Ljava/lang/String;

.field private final adWrapperUri:Landroid/net/Uri;

.field private final assetFrequencyCap:I

.field private final billingPartner:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

.field private final clickTrackingPingUris:Ljava/util/List;

.field private final clickthroughUri:Landroid/net/Uri;

.field private final closePingUris:Ljava/util/List;

.field private final completePingUris:Ljava/util/List;

.field private final duration:I

.field private final engagedViewPingUris:Ljava/util/List;

.field private final errorPingUris:Ljava/util/List;

.field private final exclusionReasonPingUris:Ljava/util/List;

.field private final expirationTimeMillis:J

.field private final firstQuartilePingUris:Ljava/util/List;

.field private final fullscreenPingUris:Ljava/util/List;

.field private final impressionUris:Ljava/util/List;

.field private final infoCards:Ljava/util/List;

.field private final isPublicVideo:Z

.field private final isVastWrapper:Z

.field private final midpointPingUris:Ljava/util/List;

.field private final mutePingUris:Ljava/util/List;

.field private final offlineAdFormatExclusionReasons:Ljava/util/List;

.field private final offlineShouldCountPlayback:Z

.field private final originalVideoId:Ljava/lang/String;

.field private final parentWrapper:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

.field private final pausePingUris:Ljava/util/List;

.field private final playbackTracking:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

.field private final playerConfig:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

.field private final prefetchedAd:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

.field private final progressPings:Ljava/util/List;

.field private final requestTimeMillis:J

.field private final requestTimes:Ljava/lang/String;

.field private final resumePingUris:Ljava/util/List;

.field private final shouldAllowAdsFallback:Z

.field private final shouldAllowQueuedOfflinePings:Z

.field private final shouldPingVssOnEngaged:Z

.field private final showCtaAnnotations:Z

.field private final skipPingUris:Ljava/util/List;

.field private final skipShownPingUris:Ljava/util/List;

.field private final startPingUris:Ljava/util/List;

.field private final survey:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

.field private final thirdQuartilePingUris:Ljava/util/List;

.field private final title:Ljava/lang/String;

.field private final vastAdId:Ljava/lang/String;

.field private final vastAdSystem:Ljava/lang/String;

.field private final videoAdTrackingTemplateUri:Landroid/net/Uri;

.field private final videoStreamingData:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

.field private final videoTitleClickedPingUris:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->EMPTY_AD:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/az;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/az;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->impressionUris:Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adVideoId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->originalVideoId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adBreakId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->title:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerName:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerUri:Landroid/net/Uri;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->vastAdId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->vastAdSystem:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;->UNKNOWN:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->billingPartner:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adFormat:Ljava/lang/String;

    iput v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->duration:I

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->videoStreamingData:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playbackTracking:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playerConfig:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->startPingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->firstQuartilePingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->midpointPingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->thirdQuartilePingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->progressPings:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->skipPingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->skipShownPingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->engagedViewPingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->completePingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->closePingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->pausePingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->resumePingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->mutePingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->fullscreenPingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->clickTrackingPingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->videoTitleClickedPingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->errorPingUris:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->exclusionReasonPingUris:Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->videoAdTrackingTemplateUri:Landroid/net/Uri;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adSenseBaseConversionUri:Landroid/net/Uri;

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldPingVssOnEngaged:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowAdsFallback:Z

    iput-wide v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->expirationTimeMillis:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->assetFrequencyCap:I

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isPublicVideo:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->showCtaAnnotations:Z

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adAnnotations:Lcom/google/a/a/a/a/ne;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adInfoCards:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    iput-wide v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->requestTimeMillis:J

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->offlineShouldCountPlayback:Z

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowQueuedOfflinePings:Z

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adWrapperUri:Landroid/net/Uri;

    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isVastWrapper:Z

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->prefetchedAd:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->parentWrapper:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->computedRequestTimes()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->requestTimes:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->computedAdSystems()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adSystems:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->computedAdIds()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adIds:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->offlineAdFormatExclusionReasons:Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->infoCards:Ljava/util/List;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->survey:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 57

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    const-class v1, Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Landroid/net/Uri;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    const-class v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v1, v11}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v13

    const-class v1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v14

    check-cast v14, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    const-class v1, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    const-class v1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v16

    check-cast v16, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    const-class v1, Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v17

    check-cast v17, Landroid/net/Uri;

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v18

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v19

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v20

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v21

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readProgressPingsList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v22

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v23

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v24

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v25

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v26

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v27

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v28

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v29

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v30

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v31

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v32

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v33

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v34

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v35

    const-class v1, Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v36

    check-cast v36, Landroid/net/Uri;

    const-class v1, Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v37

    check-cast v37, Landroid/net/Uri;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/16 v38, 0x1

    move/from16 v0, v38

    if-ne v1, v0, :cond_0

    const/16 v38, 0x1

    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/16 v39, 0x1

    move/from16 v0, v39

    if-ne v1, v0, :cond_1

    const/16 v39, 0x1

    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v40

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v42

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/16 v43, 0x1

    move/from16 v0, v43

    if-ne v1, v0, :cond_2

    const/16 v43, 0x1

    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/16 v44, 0x1

    move/from16 v0, v44

    if-ne v1, v0, :cond_3

    const/16 v44, 0x1

    :goto_3
    new-instance v1, Lcom/google/a/a/a/a/ne;

    invoke-direct {v1}, Lcom/google/a/a/a/a/ne;-><init>()V

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/e/j;->b(Landroid/os/Parcel;Lcom/google/protobuf/nano/c;)Lcom/google/protobuf/nano/c;

    move-result-object v45

    check-cast v45, Lcom/google/a/a/a/a/ne;

    const-class v1, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v46

    check-cast v46, Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v47

    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/16 v49, 0x1

    move/from16 v0, v49

    if-ne v1, v0, :cond_4

    const/16 v49, 0x1

    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/16 v50, 0x1

    move/from16 v0, v50

    if-ne v1, v0, :cond_5

    const/16 v50, 0x1

    :goto_5
    const-class v1, Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v51

    check-cast v51, Landroid/net/Uri;

    const-class v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v52

    check-cast v52, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    const-class v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v53

    check-cast v53, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->offlineReadAdFormatExclusionReasonList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v54

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->readInfoCardList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v55

    const-class v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v56

    check-cast v56, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v56}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;Ljava/lang/String;ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Landroid/net/Uri;Landroid/net/Uri;ZZJIZZLcom/google/a/a/a/a/ne;Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;JZZLandroid/net/Uri;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;)V

    return-void

    :cond_0
    const/16 v38, 0x0

    goto/16 :goto_0

    :cond_1
    const/16 v39, 0x0

    goto/16 :goto_1

    :cond_2
    const/16 v43, 0x0

    goto/16 :goto_2

    :cond_3
    const/16 v44, 0x0

    goto/16 :goto_3

    :cond_4
    const/16 v49, 0x0

    goto :goto_4

    :cond_5
    const/16 v50, 0x0

    goto :goto_5
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;Ljava/lang/String;ILcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Landroid/net/Uri;Landroid/net/Uri;ZZJIZZLcom/google/a/a/a/a/ne;Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;JZZLandroid/net/Uri;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->impressionUris:Ljava/util/List;

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adVideoId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->originalVideoId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adBreakId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->title:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerName:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerUri:Landroid/net/Uri;

    iput-object p8, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->vastAdId:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->vastAdSystem:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->billingPartner:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    iput-object p11, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adFormat:Ljava/lang/String;

    iput p12, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->duration:I

    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->videoStreamingData:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    invoke-static/range {p14 .. p14}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playbackTracking:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    invoke-static/range {p15 .. p15}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playerConfig:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    invoke-static/range {p17 .. p17}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->startPingUris:Ljava/util/List;

    invoke-static/range {p18 .. p18}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->firstQuartilePingUris:Ljava/util/List;

    invoke-static/range {p19 .. p19}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->midpointPingUris:Ljava/util/List;

    invoke-static/range {p20 .. p20}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->thirdQuartilePingUris:Ljava/util/List;

    invoke-static/range {p21 .. p21}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->progressPings:Ljava/util/List;

    invoke-static/range {p22 .. p22}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->skipPingUris:Ljava/util/List;

    invoke-static/range {p23 .. p23}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->skipShownPingUris:Ljava/util/List;

    invoke-static/range {p24 .. p24}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->engagedViewPingUris:Ljava/util/List;

    invoke-static/range {p25 .. p25}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->completePingUris:Ljava/util/List;

    invoke-static/range {p26 .. p26}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->closePingUris:Ljava/util/List;

    invoke-static/range {p27 .. p27}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->pausePingUris:Ljava/util/List;

    invoke-static/range {p28 .. p28}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->resumePingUris:Ljava/util/List;

    invoke-static/range {p29 .. p29}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->mutePingUris:Ljava/util/List;

    invoke-static/range {p30 .. p30}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->fullscreenPingUris:Ljava/util/List;

    invoke-static/range {p31 .. p31}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->clickTrackingPingUris:Ljava/util/List;

    invoke-static/range {p32 .. p32}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->videoTitleClickedPingUris:Ljava/util/List;

    invoke-static/range {p33 .. p33}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->errorPingUris:Ljava/util/List;

    invoke-static/range {p34 .. p34}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->exclusionReasonPingUris:Ljava/util/List;

    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->videoAdTrackingTemplateUri:Landroid/net/Uri;

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adSenseBaseConversionUri:Landroid/net/Uri;

    move/from16 v0, p37

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldPingVssOnEngaged:Z

    move/from16 v0, p38

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowAdsFallback:Z

    move-wide/from16 v0, p39

    iput-wide v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->expirationTimeMillis:J

    move/from16 v0, p41

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->assetFrequencyCap:I

    move/from16 v0, p42

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isPublicVideo:Z

    move/from16 v0, p43

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->showCtaAnnotations:Z

    move-object/from16 v0, p44

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adAnnotations:Lcom/google/a/a/a/a/ne;

    move-object/from16 v0, p45

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adInfoCards:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    move-wide/from16 v0, p46

    iput-wide v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->requestTimeMillis:J

    move/from16 v0, p48

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->offlineShouldCountPlayback:Z

    move/from16 v0, p49

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowQueuedOfflinePings:Z

    move-object/from16 v0, p50

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adWrapperUri:Landroid/net/Uri;

    if-eqz p50, :cond_0

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isVastWrapper:Z

    move-object/from16 v0, p51

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->prefetchedAd:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-object/from16 v0, p52

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->parentWrapper:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->computedRequestTimes()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->requestTimes:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->computedAdSystems()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adSystems:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->computedAdIds()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adIds:Ljava/lang/String;

    invoke-static/range {p53 .. p53}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->offlineAdFormatExclusionReasons:Ljava/util/List;

    invoke-static/range {p54 .. p54}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->infoCards:Ljava/util/List;

    move-object/from16 v0, p55

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->survey:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playbackTracking:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playerConfig:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowQueuedOfflinePings()Z

    move-result v0

    return v0
.end method

.method public static isYouTubeHostedUri(Landroid/net/Uri;)Z
    .locals 2

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "http"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v1, "www.youtube"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static offlineReadAdFormatExclusionReasonList(Landroid/os/Parcel;)Ljava/util/List;
    .locals 4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-class v3, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    invoke-static {v3, v0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static offlineWriteAdFormatExclusionReasonList(Landroid/os/Parcel;Ljava/util/List;)V
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$OfflineAdFormatExclusionReason;->name()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    return-void
.end method

.method private static readInfoCardList(Landroid/os/Parcel;)Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static readProgressPingsList(Landroid/os/Parcel;)Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$ProgressPing;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static readUriList(Landroid/os/Parcel;)Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private shouldAllowQueuedOfflinePings()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowQueuedOfflinePings:Z

    return v0
.end method


# virtual methods
.method public buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/ba;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getImpressionUris()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdBreakId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->c(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->e(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->s(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->f(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdSystem()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->g(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getBillingPartner()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdFormat()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->h(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getDuration()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(I)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->videoStreamingData:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playbackTracking:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playerConfig:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClickthroughUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->t(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getStartPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getFirstQuartilePingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->c(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getMidpointPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getThirdQuartilePingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->e(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getProgressPings()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->f(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSkipPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->g(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSkipShownPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->h(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getEngagedViewPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->i(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getCompletePingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->j(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClosePingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->k(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPausePingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->l(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getResumePingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->m(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getMutePingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->n(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getFullscreenPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->o(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClickTrackingPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->p(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoTitleClickedPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->q(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getErrorPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->r(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExclusionReasonPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->s(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoAdTrackingTemplateUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->u(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdSenseBaseConversionUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->v(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldPingVssOnEngaged()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowAdsFallback()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExpirationTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAssetFrequencyCap()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(I)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isPublicVideo()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->c(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldShowCtaAnnotations()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->d(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adAnnotations:Lcom/google/a/a/a/a/ne;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/a/a/a/a/ne;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adInfoCards:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getRequestTimeMills()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->c(J)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isOfflineShouldCountPlayback()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->e(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowQueuedOfflinePings()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->f(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdWrapperUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->w(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPrefetchedAd()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->b(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getOfflineAdFormatExclusionReasons()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->t(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getInfoCards()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->u(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSurvey()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/ba;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;)Lcom/google/android/apps/youtube/datalib/legacy/model/ba;

    move-result-object v0

    return-object v0
.end method

.method public computedAdIds()Ljava/lang/String;
    .locals 2

    new-instance v1, Lcom/google/android/apps/youtube/common/e/l;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/common/e/l;-><init>()V

    :goto_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/common/e/l;->offer(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object p0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdId()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    const-string v0, ","

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public computedAdSystems()Ljava/lang/String;
    .locals 2

    new-instance v1, Lcom/google/android/apps/youtube/common/e/l;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/common/e/l;-><init>()V

    :goto_0
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdSystem()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/common/e/l;->offer(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object p0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdSystem()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    const-string v0, ","

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public computedRequestTimes()Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/common/e/l;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/common/e/l;-><init>()V

    :goto_0
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getRequestTimeMills()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/common/e/l;->offer(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object p0

    goto :goto_0

    :cond_0
    const-string v1, ","

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public createCustomAdSenseConversionUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdSenseBaseConversionUri()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdSenseBaseConversionUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "label"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdBreakId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdBreakId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerName:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerUri:Landroid/net/Uri;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerUri:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdSystem()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdSystem()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getBillingPartner()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getBillingPartner()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdFormat()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdFormat()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->videoStreamingData:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->videoStreamingData:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playbackTracking:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playbackTracking:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playerConfig:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playerConfig:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClickthroughUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClickthroughUri()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getDuration()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getDuration()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldPingVssOnEngaged()Z

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldPingVssOnEngaged()Z

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowAdsFallback()Z

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowAdsFallback()Z

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExpirationTimeMillis()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExpirationTimeMillis()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAssetFrequencyCap()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAssetFrequencyCap()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getImpressionUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getImpressionUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getStartPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getStartPingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getFirstQuartilePingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getFirstQuartilePingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getMidpointPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getMidpointPingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getThirdQuartilePingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getThirdQuartilePingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getProgressPings()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getProgressPings()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSkipPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSkipPingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSkipShownPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSkipShownPingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getEngagedViewPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getEngagedViewPingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getCompletePingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getCompletePingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClosePingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClosePingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPausePingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPausePingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getResumePingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getResumePingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getMutePingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getMutePingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getFullscreenPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getFullscreenPingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClickTrackingPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClickTrackingPingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoTitleClickedPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoTitleClickedPingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getErrorPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getErrorPingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExclusionReasonPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExclusionReasonPingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoAdTrackingTemplateUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoAdTrackingTemplateUri()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdSenseBaseConversionUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdSenseBaseConversionUri()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdWrapperUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdWrapperUri()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPrefetchedAd()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPrefetchedAd()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldShowCtaAnnotations()Z

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldShowCtaAnnotations()Z

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isOfflineShouldCountPlayback()Z

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isOfflineShouldCountPlayback()Z

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowQueuedOfflinePings()Z

    move-result v1

    invoke-direct {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowQueuedOfflinePings()Z

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isOfflineShouldCountPlayback()Z

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isOfflineShouldCountPlayback()Z

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getOfflineAdFormatExclusionReasons()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getOfflineAdFormatExclusionReasons()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getInfoCards()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getInfoCards()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSurvey()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSurvey()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public firstStreamUri()Landroid/net/Uri;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->videoStreamingData:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->videoStreamingData:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;->getAllFormatStreams()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getUri()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getAdBreakId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adBreakId:Ljava/lang/String;

    return-object v0
.end method

.method public getAdFormat()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adFormat:Ljava/lang/String;

    return-object v0
.end method

.method public getAdFormatSubType()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSurvey()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;->SURVEY:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isSkippable()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;->SKIPPABLE:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;->NONE:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$AdFormatSubType;

    goto :goto_0
.end method

.method public getAdIds()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adIds:Ljava/lang/String;

    return-object v0
.end method

.method public getAdOwnerName()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isPublicVideo:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAdOwnerUri()Landroid/net/Uri;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isPublicVideo:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerUri:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAdSenseBaseConversionUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adSenseBaseConversionUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getAdSystems()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adSystems:Ljava/lang/String;

    return-object v0
.end method

.method public getAdVideoId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adVideoId:Ljava/lang/String;

    return-object v0
.end method

.method public getAdWrapperUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adWrapperUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getAssetFrequencyCap()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->assetFrequencyCap:I

    return v0
.end method

.method public getBillingPartner()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->billingPartner:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    return-object v0
.end method

.method public getClickTrackingPingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->clickTrackingPingUris:Ljava/util/List;

    return-object v0
.end method

.method public getClickthroughUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->clickthroughUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getClosePingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->closePingUris:Ljava/util/List;

    return-object v0
.end method

.method public getCompletePingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->completePingUris:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getConverter()Lcom/google/android/apps/youtube/datalib/legacy/a/b;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getConverter()Lcom/google/android/apps/youtube/datalib/legacy/model/bb;

    move-result-object v0

    return-object v0
.end method

.method public getConverter()Lcom/google/android/apps/youtube/datalib/legacy/model/bb;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bb;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->duration:I

    return v0
.end method

.method public getEngagedViewPingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->engagedViewPingUris:Ljava/util/List;

    return-object v0
.end method

.method public getErrorPingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->errorPingUris:Ljava/util/List;

    return-object v0
.end method

.method public getExclusionReasonPingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->exclusionReasonPingUris:Ljava/util/List;

    return-object v0
.end method

.method public getExpirationTimeMillis()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->expirationTimeMillis:J

    return-wide v0
.end method

.method public getFirstQuartilePingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->firstQuartilePingUris:Ljava/util/List;

    return-object v0
.end method

.method public getFullscreenPingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->fullscreenPingUris:Ljava/util/List;

    return-object v0
.end method

.method public getImpressionUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->impressionUris:Ljava/util/List;

    return-object v0
.end method

.method public getInfoCards()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->infoCards:Ljava/util/List;

    return-object v0
.end method

.method public getMidpointPingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->midpointPingUris:Ljava/util/List;

    return-object v0
.end method

.method public getMutePingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->mutePingUris:Ljava/util/List;

    return-object v0
.end method

.method public getOfflineAdFormatExclusionReasons()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->offlineAdFormatExclusionReasons:Ljava/util/List;

    return-object v0
.end method

.method public getOriginalVideoId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->originalVideoId:Ljava/lang/String;

    return-object v0
.end method

.method public getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->parentWrapper:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-object v0
.end method

.method public getParentWrapperCount()I
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    goto :goto_0

    :cond_0
    return v1
.end method

.method public getPausePingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->pausePingUris:Ljava/util/List;

    return-object v0
.end method

.method public getPlaybackTracking()Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playbackTracking:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    return-object v0
.end method

.method public getPlayerConfig()Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playerConfig:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    return-object v0
.end method

.method public getPrefetchedAd()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->prefetchedAd:Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    return-object v0
.end method

.method public getProgressPings()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->progressPings:Ljava/util/List;

    return-object v0
.end method

.method public getRequestTimeMills()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->requestTimeMillis:J

    return-wide v0
.end method

.method public getRequestTimes()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->requestTimes:Ljava/lang/String;

    return-object v0
.end method

.method public getResumePingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->resumePingUris:Ljava/util/List;

    return-object v0
.end method

.method public getShouldAllowQueuedOfflinePings()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowQueuedOfflinePings()Z

    move-result v0

    return v0
.end method

.method public getSkipPingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->skipPingUris:Ljava/util/List;

    return-object v0
.end method

.method public getSkipShownPingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->skipShownPingUris:Ljava/util/List;

    return-object v0
.end method

.method public getStartPingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->startPingUris:Ljava/util/List;

    return-object v0
.end method

.method public getSurvey()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->survey:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    return-object v0
.end method

.method public getThirdQuartilePingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->thirdQuartilePingUris:Ljava/util/List;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getVastAdId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->vastAdId:Ljava/lang/String;

    return-object v0
.end method

.method public getVastAdSystem()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->vastAdSystem:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoAdTrackingTemplateUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->videoAdTrackingTemplateUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getVideoStreamingData()Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->videoStreamingData:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    return-object v0
.end method

.method public getVideoTitleClickedPingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->videoTitleClickedPingUris:Ljava/util/List;

    return-object v0
.end method

.method public hasExpired(Lcom/google/android/apps/youtube/common/e/b;)Z
    .locals 4

    invoke-interface {p1}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExpirationTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getImpressionUris()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isForecastingAd()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->videoStreamingData:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isVastWrapper()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSurvey()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOfflineShouldCountPlayback()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->offlineShouldCountPlayback:Z

    return v0
.end method

.method public isPublicVideo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isPublicVideo:Z

    return v0
.end method

.method public isSkippable()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSkipPingUris()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVastWrapper()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isVastWrapper:Z

    return v0
.end method

.method public shouldAllowAdsFallback()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowAdsFallback:Z

    return v0
.end method

.method public shouldPingVssOnEngaged()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldPingVssOnEngaged:Z

    return v0
.end method

.method public shouldPlayAd(Lcom/google/android/apps/youtube/common/e/b;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isForecastingAd()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->hasExpired(Lcom/google/android/apps/youtube/common/e/b;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldShowCtaAnnotations()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->showCtaAnnotations:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isVastWrapper()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "VastAd Wrapper: [wrapperUri="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdWrapperUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "VastAd: [vastAdId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", adVideoId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",videoTitle= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", vastAdSystem = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdSystem()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getImpressionUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdBreakId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adOwnerUri:Landroid/net/Uri;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVastAdSystem()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getBillingPartner()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd$BillingPartner;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdFormat()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getDuration()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->videoStreamingData:Lcom/google/android/apps/youtube/datalib/innertube/model/media/VideoStreamingData;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playbackTracking:Lcom/google/android/apps/youtube/datalib/innertube/model/PlaybackTracking;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->playerConfig:Lcom/google/android/apps/youtube/datalib/innertube/model/media/PlayerConfig;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClickthroughUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getStartPingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getFirstQuartilePingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getMidpointPingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getThirdQuartilePingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getProgressPings()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSkipPingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSkipShownPingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getEngagedViewPingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getCompletePingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClosePingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPausePingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getResumePingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getMutePingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getFullscreenPingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getClickTrackingPingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoTitleClickedPingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getErrorPingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExclusionReasonPingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getVideoAdTrackingTemplateUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdSenseBaseConversionUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldPingVssOnEngaged()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowAdsFallback()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getExpirationTimeMillis()J

    move-result-wide v3

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAssetFrequencyCap()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isPublicVideo()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldShowCtaAnnotations()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adAnnotations:Lcom/google/a/a/a/a/ne;

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/e/j;->a(Landroid/os/Parcel;Lcom/google/protobuf/nano/c;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->adInfoCards:Lcom/google/android/apps/youtube/datalib/innertube/model/InfoCardCollection;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getRequestTimeMills()J

    move-result-wide v3

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->isOfflineShouldCountPlayback()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->shouldAllowQueuedOfflinePings()Z

    move-result v0

    if-eqz v0, :cond_5

    :goto_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getAdWrapperUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getPrefetchedAd()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getParentWrapper()Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getOfflineAdFormatExclusionReasons()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->offlineWriteAdFormatExclusionReasonList(Landroid/os/Parcel;Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getInfoCards()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->getSurvey()Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v1, v2

    goto :goto_5
.end method
