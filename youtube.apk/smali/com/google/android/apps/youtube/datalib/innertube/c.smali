.class public final Lcom/google/android/apps/youtube/datalib/innertube/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/a/f;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/client/h;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/client/h;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/client/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/c;->a:Lcom/google/android/apps/youtube/core/client/h;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/a/a/a/a/ii;)V
    .locals 4

    iget-object v0, p1, Lcom/google/a/a/a/a/ii;->i:Lcom/google/a/a/a/a/o;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/a/a/a/a/o;

    invoke-direct {v0}, Lcom/google/a/a/a/a/o;-><init>()V

    iput-object v0, p1, Lcom/google/a/a/a/a/ii;->i:Lcom/google/a/a/a/a/o;

    :cond_0
    new-instance v0, Lcom/google/a/a/a/a/ix;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ix;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/c;->a:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/client/h;->a()Lcom/google/android/apps/youtube/core/utils/a;

    invoke-static {}, Lcom/google/android/apps/youtube/core/utils/a;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/ix;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/c;->a:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/client/h;->a()Lcom/google/android/apps/youtube/core/utils/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/utils/a;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/ix;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/a/a/a/a/ii;->i:Lcom/google/a/a/a/a/o;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/a/a/a/a/ix;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    iput-object v2, v1, Lcom/google/a/a/a/a/o;->b:[Lcom/google/a/a/a/a/ix;

    return-void
.end method
