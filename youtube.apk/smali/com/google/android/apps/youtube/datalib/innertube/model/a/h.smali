.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/a/h;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a([Lcom/google/a/a/a/a/gi;)Ljava/util/List;
    .locals 6

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, p0, v0

    iget-object v4, v3, Lcom/google/a/a/a/a/gi;->c:Lcom/google/a/a/a/a/go;

    if-eqz v4, :cond_0

    new-instance v4, Lcom/google/android/apps/youtube/datalib/innertube/model/a/i;

    iget-object v3, v3, Lcom/google/a/a/a/a/gi;->c:Lcom/google/a/a/a/a/go;

    invoke-direct {v4, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/i;-><init>(Lcom/google/a/a/a/a/go;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v4, v3, Lcom/google/a/a/a/a/gi;->d:Lcom/google/a/a/a/a/ge;

    if-eqz v4, :cond_1

    new-instance v4, Lcom/google/android/apps/youtube/datalib/innertube/model/a/f;

    iget-object v3, v3, Lcom/google/a/a/a/a/gi;->d:Lcom/google/a/a/a/a/ge;

    invoke-direct {v4, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/f;-><init>(Lcom/google/a/a/a/a/ge;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v4, v3, Lcom/google/a/a/a/a/gi;->b:Lcom/google/a/a/a/a/gl;

    if-eqz v4, :cond_2

    new-instance v4, Lcom/google/android/apps/youtube/datalib/innertube/model/a/g;

    iget-object v3, v3, Lcom/google/a/a/a/a/gi;->b:Lcom/google/a/a/a/a/gl;

    invoke-direct {v4, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/g;-><init>(Lcom/google/a/a/a/a/gl;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unsupported Guide renderer found: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    return-object v1
.end method
