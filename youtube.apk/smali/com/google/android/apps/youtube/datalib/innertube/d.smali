.class public final Lcom/google/android/apps/youtube/datalib/innertube/d;
.super Lcom/google/android/apps/youtube/datalib/innertube/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/i;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/r;Lcom/google/android/apps/youtube/datalib/innertube/p;Lcom/android/volley/l;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/youtube/datalib/innertube/a;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/r;Lcom/google/android/apps/youtube/datalib/innertube/p;Lcom/android/volley/l;)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "VL"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/g;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/g;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/d;->b:Lcom/google/android/apps/youtube/datalib/innertube/p;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/innertube/g;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;B)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/g;Lcom/google/android/apps/youtube/datalib/a/l;)V
    .locals 4

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/e;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/youtube/datalib/innertube/e;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/d;Lcom/google/android/apps/youtube/datalib/a/l;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/d;->c:Lcom/android/volley/l;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/d;->a:Lcom/google/android/apps/youtube/datalib/innertube/r;

    const-class v3, Lcom/google/a/a/a/a/ar;

    invoke-virtual {v2, p1, v3, v0}, Lcom/google/android/apps/youtube/datalib/innertube/r;->a(Lcom/google/android/apps/youtube/datalib/innertube/s;Ljava/lang/Class;Lcom/google/android/apps/youtube/datalib/a/l;)Lcom/google/android/apps/youtube/datalib/innertube/q;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/volley/l;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/a/l;)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/d;->a()Lcom/google/android/apps/youtube/datalib/innertube/g;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/datalib/innertube/g;->a:[B

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/g;->a([B)V

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/g;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/g;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/f;

    invoke-direct {v1, p0, p2}, Lcom/google/android/apps/youtube/datalib/innertube/f;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/d;Lcom/google/android/apps/youtube/datalib/a/l;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/d;->a(Lcom/google/android/apps/youtube/datalib/innertube/g;Lcom/google/android/apps/youtube/datalib/a/l;)V

    return-void
.end method
