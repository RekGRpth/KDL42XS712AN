.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/legacy/model/r;
.implements Ljava/io/Serializable;


# instance fields
.field private end:Ljava/util/Date;

.field private selfUri:Landroid/net/Uri;

.field private start:Ljava/util/Date;

.field private status:Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Status;

.field private video:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->start:Ljava/util/Date;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->end:Ljava/util/Date;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Status;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->status:Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Status;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->selfUri:Landroid/net/Uri;

    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->video:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->build()Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->start:Ljava/util/Date;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->end:Ljava/util/Date;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->status:Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Status;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->selfUri:Landroid/net/Uri;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/p;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->video:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final build()Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent;
    .locals 6

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->start:Ljava/util/Date;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->end:Ljava/util/Date;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->status:Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Status;

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->selfUri:Landroid/net/Uri;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->video:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent;-><init>(Ljava/util/Date;Ljava/util/Date;Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Status;Landroid/net/Uri;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)V

    return-object v0
.end method

.method public final bridge synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->build()Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent;

    move-result-object v0

    return-object v0
.end method

.method public final end(Ljava/util/Date;)Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->end:Ljava/util/Date;

    return-object p0
.end method

.method public final selfUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->selfUri:Landroid/net/Uri;

    return-object p0
.end method

.method public final start(Ljava/util/Date;)Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->start:Ljava/util/Date;

    return-object p0
.end method

.method public final status(Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Status;)Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->status:Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Status;

    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Live Event [start = \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->start:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", end=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->end:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', status: \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->status:Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', video: \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->video:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\']"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final video(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/LiveEvent$Builder;->video:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    return-object p0
.end method
