.class public final Lcom/google/android/apps/youtube/datalib/innertube/u;
.super Lcom/google/android/apps/youtube/datalib/innertube/b;
.source "SourceFile"


# instance fields
.field private c:Ljava/util/List;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/b;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/u;->c:Ljava/util/List;

    const/4 v0, 0x0

    new-array v0, v0, [B

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/datalib/innertube/u;->a([B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/u;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    return-void
.end method


# virtual methods
.method public final b([B)Lcom/google/android/apps/youtube/datalib/innertube/u;
    .locals 4

    new-instance v0, Lcom/google/a/a/a/a/ik;

    invoke-direct {v0}, Lcom/google/a/a/a/a/ik;-><init>()V

    new-instance v1, Lcom/google/a/a/a/a/va;

    invoke-direct {v1}, Lcom/google/a/a/a/a/va;-><init>()V

    iput-object v1, v0, Lcom/google/a/a/a/a/ik;->b:Lcom/google/a/a/a/a/va;

    iget-object v1, v0, Lcom/google/a/a/a/a/ik;->b:Lcom/google/a/a/a/a/va;

    const/4 v2, 0x1

    new-array v2, v2, [[B

    const/4 v3, 0x0

    aput-object p1, v2, v3

    iput-object v2, v1, Lcom/google/a/a/a/a/va;->b:[[B

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/u;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method protected final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/u;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    const-string v0, "log_interaction"

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/nano/c;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/u;->b()V

    new-instance v1, Lcom/google/a/a/a/a/ij;

    invoke-direct {v1}, Lcom/google/a/a/a/a/ij;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/u;->d()Lcom/google/a/a/a/a/ii;

    move-result-object v0

    iput-object v0, v1, Lcom/google/a/a/a/a/ij;->b:Lcom/google/a/a/a/a/ii;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/u;->c:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/u;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/a/a/a/a/ik;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/a/a/a/a/ik;

    iput-object v0, v1, Lcom/google/a/a/a/a/ij;->c:[Lcom/google/a/a/a/a/ik;

    return-object v1
.end method
