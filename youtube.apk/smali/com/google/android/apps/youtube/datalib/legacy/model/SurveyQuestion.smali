.class public Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final DEFAULT_DURATION_SECONDS:I = 0xf


# instance fields
.field private beaconUris:Ljava/util/List;

.field private final surveyQuestionProto:Lcom/google/android/apps/youtube/a/a/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/at;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/at;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/youtube/a/a/a/c;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/a/c;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->surveyQuestionProto:Lcom/google/android/apps/youtube/a/a/a/c;

    iget-object v0, p1, Lcom/google/android/apps/youtube/a/a/a/c;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/apps/youtube/a/a/a/c;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->fromName(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->UNSUPPORTED:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    if-eq v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iget-object v0, p1, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/a/a/a/c;Lcom/google/android/apps/youtube/datalib/legacy/model/at;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;-><init>(Lcom/google/android/apps/youtube/a/a/a/c;)V

    return-void
.end method


# virtual methods
.method public buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/au;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/au;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->surveyQuestionProto:Lcom/google/android/apps/youtube/a/a/a/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/au;-><init>(Lcom/google/android/apps/youtube/a/a/a/c;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getType()Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getType()Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getAnswerPresentationOrder()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getAnswerPresentationOrder()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getQuestion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getQuestion()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getAnswers()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getAnswers()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getBeaconUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getBeaconUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getApiContext()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getApiContext()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getDurationSeconds()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getDurationSeconds()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public getAnswerOrdering()Ljava/lang/String;
    .locals 2

    const-string v0, "."

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->surveyQuestionProto:Lcom/google/android/apps/youtube/a/a/a/c;

    iget-object v1, v1, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/e/c;->a([I)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAnswerPresentationOrder()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->surveyQuestionProto:Lcom/google/android/apps/youtube/a/a/a/c;

    iget-object v0, v0, Lcom/google/android/apps/youtube/a/a/a/c;->j:[I

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/c;->a([I)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getAnswers()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->surveyQuestionProto:Lcom/google/android/apps/youtube/a/a/a/c;

    iget-object v0, v0, Lcom/google/android/apps/youtube/a/a/a/c;->c:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getApiContext()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->surveyQuestionProto:Lcom/google/android/apps/youtube/a/a/a/c;

    iget-object v0, v0, Lcom/google/android/apps/youtube/a/a/a/c;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getBeaconUris()Ljava/util/List;
    .locals 5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->beaconUris:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->surveyQuestionProto:Lcom/google/android/apps/youtube/a/a/a/c;

    iget-object v1, v0, Lcom/google/android/apps/youtube/a/a/a/c;->f:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->beaconUris:Ljava/util/List;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->beaconUris:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDurationSeconds()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->surveyQuestionProto:Lcom/google/android/apps/youtube/a/a/a/c;

    iget v0, v0, Lcom/google/android/apps/youtube/a/a/a/c;->h:I

    return v0
.end method

.method public getId()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->surveyQuestionProto:Lcom/google/android/apps/youtube/a/a/a/c;

    iget v0, v0, Lcom/google/android/apps/youtube/a/a/a/c;->i:I

    return v0
.end method

.method public getQuestion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->surveyQuestionProto:Lcom/google/android/apps/youtube/a/a/a/c;

    iget-object v0, v0, Lcom/google/android/apps/youtube/a/a/a/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getSurveyQuestionProto()Lcom/google/android/apps/youtube/a/a/a/c;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/a/a/a/c;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/a/c;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->surveyQuestionProto:Lcom/google/android/apps/youtube/a/a/a/c;

    invoke-static {v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/a/c;

    return-object v0
.end method

.method public getType()Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->surveyQuestionProto:Lcom/google/android/apps/youtube/a/a/a/c;

    iget-object v0, v0, Lcom/google/android/apps/youtube/a/a/a/c;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->fromName(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    move-result-object v0

    return-object v0
.end method

.method public isMultiSelectQuestion()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getType()Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->MULTI_SELECT:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Question [type: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getType()Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "question:\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getQuestion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" answers: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->getAnswers()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion;->surveyQuestionProto:Lcom/google/android/apps/youtube/a/a/a/c;

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/e/j;->a(Landroid/os/Parcel;Lcom/google/protobuf/nano/c;)V

    return-void
.end method
