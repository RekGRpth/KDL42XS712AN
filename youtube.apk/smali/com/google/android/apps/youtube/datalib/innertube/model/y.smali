.class final Lcom/google/android/apps/youtube/datalib/innertube/model/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;
    .locals 4

    :try_start_0
    new-instance v0, Lcom/google/a/a/a/a/nw;

    invoke-direct {v0}, Lcom/google/a/a/a/a/nw;-><init>()V

    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/common/e/j;->b(Landroid/os/Parcel;Lcom/google/protobuf/nano/c;)Lcom/google/protobuf/nano/c;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/nw;

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;-><init>(Lcom/google/a/a/a/a/nw;J)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/y;->a(Landroid/os/Parcel;)Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/apps/youtube/datalib/innertube/model/PlayerResponse;

    return-object v0
.end method
