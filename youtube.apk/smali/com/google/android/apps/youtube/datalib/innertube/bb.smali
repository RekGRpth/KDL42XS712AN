.class public final Lcom/google/android/apps/youtube/datalib/innertube/bb;
.super Lcom/google/android/apps/youtube/datalib/innertube/b;
.source "SourceFile"


# instance fields
.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/b;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->d:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->e:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->f:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->g:Ljava/lang/String;

    new-array v0, v1, [B

    invoke-virtual {p0, v0}, Lcom/google/android/apps/youtube/datalib/innertube/bb;->a([B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/bb;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/youtube/datalib/innertube/bb;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->e:I

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/bb;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/bb;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/bb;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->f:Ljava/lang/String;

    return-object p0
.end method

.method protected final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/bb;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->g:Ljava/lang/String;

    return-object p0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    const-string v0, "next"

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/nano/c;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/bb;->b()V

    new-instance v0, Lcom/google/a/a/a/a/wf;

    invoke-direct {v0}, Lcom/google/a/a/a/a/wf;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/wf;->d:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->e:I

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->e:I

    iput v1, v0, Lcom/google/a/a/a/a/wf;->f:I

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->f:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/wf;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/wf;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/bb;->g:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/a/a/a/a/wf;->g:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/bb;->d()Lcom/google/a/a/a/a/ii;

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/wf;->b:Lcom/google/a/a/a/a/ii;

    return-object v0
.end method
