.class final Lcom/google/android/apps/youtube/datalib/offline/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/a/a/c;

.field final synthetic b:Lcom/google/android/apps/youtube/datalib/offline/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/datalib/offline/a;Lcom/google/android/apps/youtube/a/a/c;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/offline/e;->b:Lcom/google/android/apps/youtube/datalib/offline/a;

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/offline/e;->a:Lcom/google/android/apps/youtube/a/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/e;->b:Lcom/google/android/apps/youtube/datalib/offline/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/offline/a;->b(Lcom/google/android/apps/youtube/datalib/offline/a;)Lcom/google/android/apps/youtube/common/database/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/database/d;->a()V

    :try_start_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Requeue request with %d errors to %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/offline/e;->a:Lcom/google/android/apps/youtube/a/a/c;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/a/a/c;->m()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/offline/e;->a:Lcom/google/android/apps/youtube/a/a/c;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/a/a/c;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/e;->b:Lcom/google/android/apps/youtube/datalib/offline/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/offline/a;->b(Lcom/google/android/apps/youtube/datalib/offline/a;)Lcom/google/android/apps/youtube/common/database/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/e;->a:Lcom/google/android/apps/youtube/a/a/c;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/a/a/c;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/offline/e;->a:Lcom/google/android/apps/youtube/a/a/c;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/common/database/d;->a(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/e;->b:Lcom/google/android/apps/youtube/datalib/offline/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/offline/a;->b(Lcom/google/android/apps/youtube/datalib/offline/a;)Lcom/google/android/apps/youtube/common/database/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/database/d;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/e;->b:Lcom/google/android/apps/youtube/datalib/offline/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/offline/a;->b(Lcom/google/android/apps/youtube/datalib/offline/a;)Lcom/google/android/apps/youtube/common/database/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/database/d;->b()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/offline/e;->b:Lcom/google/android/apps/youtube/datalib/offline/a;

    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/offline/a;->b(Lcom/google/android/apps/youtube/datalib/offline/a;)Lcom/google/android/apps/youtube/common/database/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/database/d;->b()V

    throw v0
.end method
