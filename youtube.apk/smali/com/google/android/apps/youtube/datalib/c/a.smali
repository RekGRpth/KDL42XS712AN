.class public final Lcom/google/android/apps/youtube/datalib/c/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/c/f;


# instance fields
.field private final a:Lcom/android/volley/l;

.field private final b:Landroid/content/SharedPreferences;

.field private final c:Lcom/google/android/apps/youtube/datalib/config/a;

.field private d:Lcom/google/android/apps/youtube/datalib/c/b;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/volley/l;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/datalib/config/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/volley/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->a:Lcom/android/volley/l;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->b:Landroid/content/SharedPreferences;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/config/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->c:Lcom/google/android/apps/youtube/datalib/config/a;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->e:Ljava/lang/String;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->f:Ljava/lang/String;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "%s_%s"

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "apiary_device_id"

    aput-object v2, v1, v3

    aput-object p6, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->g:Ljava/lang/String;

    const-string v0, "%s_%s"

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "apiary_device_key"

    aput-object v2, v1, v3

    aput-object p6, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->h:Ljava/lang/String;

    return-void
.end method

.method private declared-synchronized b()Lcom/google/android/apps/youtube/datalib/c/b;
    .locals 9

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->d:Lcom/google/android/apps/youtube/datalib/c/b;

    if-eqz v0, :cond_0

    const-string v0, "Returned cached device auth."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->f(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->d:Lcom/google/android/apps/youtube/datalib/c/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->b:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/c/a;->g:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->b:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/c/a;->h:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    const/4 v3, 0x0

    invoke-static {v0, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    new-instance v0, Lcom/google/android/apps/youtube/datalib/c/b;

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/youtube/datalib/c/b;-><init>(Ljava/lang/String;[B)V

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->d:Lcom/google/android/apps/youtube/datalib/c/b;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->d:Lcom/google/android/apps/youtube/datalib/c/b;

    if-eqz v0, :cond_2

    const-string v0, "Returned device auth from shared preferences."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->d:Lcom/google/android/apps/youtube/datalib/c/b;

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1

    :cond_2
    new-instance v2, Lcom/google/android/apps/youtube/common/e/d;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/common/e/d;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->c:Lcom/google/android/apps/youtube/datalib/config/a;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/config/a;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/c/a;->c:Lcom/google/android/apps/youtube/datalib/config/a;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/datalib/config/a;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v3, "key"

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/c/a;->e:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v3, "rawDeviceId"

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/c/a;->f:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/a/k;->a()Lcom/google/android/apps/youtube/datalib/a/k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    :try_start_2
    new-instance v4, Lcom/google/android/apps/youtube/datalib/c/e;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/c/a;->c:Lcom/google/android/apps/youtube/datalib/config/a;

    invoke-interface {v5}, Lcom/google/android/apps/youtube/datalib/config/a;->g()[B

    move-result-object v5

    const/4 v6, 0x4

    invoke-direct {v4, v0, v3, v5, v6}, Lcom/google/android/apps/youtube/datalib/c/e;-><init>(Landroid/net/Uri;Lcom/google/android/apps/youtube/datalib/a/l;[BI)V
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->a:Lcom/android/volley/l;

    invoke-virtual {v0, v4}, Lcom/android/volley/l;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    const-wide/16 v5, 0xf

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v5, v6, v0}, Lcom/google/android/apps/youtube/datalib/a/k;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/c/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->d:Lcom/google/android/apps/youtube/datalib/c/b;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->d:Lcom/google/android/apps/youtube/datalib/c/b;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/c/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/datalib/c/a;->g:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/c/b;->b()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/youtube/datalib/c/a;->h:Ljava/lang/String;

    new-instance v7, Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/c/b;->a()[B

    move-result-object v0

    const/4 v8, 0x0

    invoke-static {v0, v8}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/lang/String;-><init>([B)V

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    const-string v0, "Successfully completed device registration."

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->d:Lcom/google/android/apps/youtube/datalib/c/b;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    goto/16 :goto_0

    :catch_2
    move-exception v0

    :try_start_4
    const-string v5, "Could not do device auth handshake: "

    invoke-static {v5, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/common/e/d;->a()Z

    move-result v0

    if-nez v0, :cond_3

    move-object v0, v1

    goto/16 :goto_0

    :catch_3
    move-exception v0

    const-string v5, "Could not do device auth handshake: "

    invoke-static {v5, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    instance-of v5, v5, Lcom/android/volley/VolleyError;

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lcom/android/volley/VolleyError;

    iget-object v5, v0, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/j;

    if-eqz v5, :cond_4

    iget-object v5, v0, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/j;

    iget-object v5, v5, Lcom/android/volley/j;->b:[B

    if-eqz v5, :cond_4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Server returned: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/String;

    iget-object v0, v0, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/j;

    iget-object v0, v0, Lcom/android/volley/j;->b:[B

    invoke-direct {v6, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->e(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v2}, Lcom/google/android/apps/youtube/common/e/d;->a()Z

    move-result v0

    if-nez v0, :cond_3

    move-object v0, v1

    goto/16 :goto_0

    :catch_4
    move-exception v0

    const-string v5, "Could not do device auth handshake: "

    invoke-static {v5, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/common/e/d;->a()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v0

    if-nez v0, :cond_3

    move-object v0, v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->d:Lcom/google/android/apps/youtube/datalib/c/b;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/c/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/c/a;->g:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/c/a;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public final a(Ljava/util/Map;Ljava/lang/String;[B)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/c/a;->b()Lcom/google/android/apps/youtube/datalib/c/b;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/datalib/c/b;->a(Ljava/util/Map;Ljava/lang/String;[B)V

    :cond_0
    return-void
.end method
