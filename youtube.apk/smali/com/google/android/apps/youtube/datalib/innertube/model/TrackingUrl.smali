.class public Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Comparable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final NOT_SET:I = -0x1

.field public static final NO_CLIENT_PARAMS:Ljava/util/Set;


# instance fields
.field private final baseUrl:Ljava/lang/String;

.field private final clientParams:Ljava/util/Set;

.field private final elapsedMediaTimeSec:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->NO_CLIENT_PARAMS:Ljava/util/Set;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/aq;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/aq;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/a/a/a/a/tb;Ljava/util/Set;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/google/a/a/a/a/tb;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->baseUrl:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->clientParams:Ljava/util/Set;

    iget v0, p1, Lcom/google/a/a/a/a/tb;->c:I

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/google/a/a/a/a/tb;->c:I

    :goto_0
    iput v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->elapsedMediaTimeSec:I

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/a/a/h;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/h;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/h;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->baseUrl:Ljava/lang/String;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->clientParams:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/h;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->clientParams:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->fromValue(I)Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    const-string v0, ""

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/h;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/h;->d()I

    move-result v0

    :goto_2
    iput v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->elapsedMediaTimeSec:I

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Set;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;-><init>(Ljava/lang/String;Ljava/util/Set;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/Set;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->baseUrl:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->clientParams:Ljava/util/Set;

    iput p3, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->elapsedMediaTimeSec:I

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;)I
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->elapsedMediaTimeSec:I

    iget v1, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->elapsedMediaTimeSec:I

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->elapsedMediaTimeSec:I

    iget v1, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->elapsedMediaTimeSec:I

    if-ge v0, v1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->baseUrl:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->baseUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->compareTo(Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;)I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    if-eqz v1, :cond_1

    check-cast p1, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;

    if-eq p0, p1, :cond_0

    invoke-virtual {p1, p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->compareTo(Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;)I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->hashCode()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->hashCode()I

    move-result v2

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public getBaseUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->baseUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getClientParams()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->clientParams:Ljava/util/Set;

    return-object v0
.end method

.method public getElapsedMediaTimeSec(I)I
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->elapsedMediaTimeSec:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    :goto_0
    return p1

    :cond_0
    iget p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->elapsedMediaTimeSec:I

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->baseUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->clientParams:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->elapsedMediaTimeSec:I

    add-int/2addr v0, v1

    return v0
.end method

.method public shouldPingNow(J)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->baseUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->elapsedMediaTimeSec:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->elapsedMediaTimeSec:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    cmp-long v0, v0, p1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "@"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->elapsedMediaTimeSec:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "s->"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->baseUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->clientParams:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toTrackingUrlProto()Lcom/google/android/apps/youtube/a/a/h;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/a/a/h;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/h;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->baseUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/h;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/h;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->elapsedMediaTimeSec:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/h;->b(I)Lcom/google/android/apps/youtube/a/a/h;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->clientParams:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;

    # getter for: Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->value:I
    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;->access$000(Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl$ClientParam;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/a/a/h;->a(I)Lcom/google/android/apps/youtube/a/a/h;

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/TrackingUrl;->toTrackingUrlProto()Lcom/google/android/apps/youtube/a/a/h;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/e/j;->a(Landroid/os/Parcel;Lcom/google/protobuf/micro/c;)V

    return-void
.end method
