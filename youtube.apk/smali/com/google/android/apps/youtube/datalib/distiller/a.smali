.class public final Lcom/google/android/apps/youtube/datalib/distiller/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/apps/youtube/core/identity/ak;

.field private final c:Lcom/google/android/apps/youtube/datalib/c/f;

.field private final d:Ljava/util/List;

.field private final e:Lcom/android/volley/l;

.field private final f:Lcom/google/android/apps/youtube/datalib/config/a;

.field private final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    :try_start_0
    const-string v0, "http://www.youtube.com/watch?v="

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/datalib/distiller/a;->a:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Device doesn\'t support UTF-8."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/datalib/c/f;Ljava/util/List;Lcom/android/volley/l;Lcom/google/android/apps/youtube/datalib/config/a;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/identity/ak;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->b:Lcom/google/android/apps/youtube/core/identity/ak;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/c/f;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->c:Lcom/google/android/apps/youtube/datalib/c/f;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->d:Ljava/util/List;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/volley/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->e:Lcom/android/volley/l;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/config/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->f:Lcom/google/android/apps/youtube/datalib/config/a;

    invoke-static {p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->g:Ljava/lang/String;

    return-void
.end method

.method public static a()Lcom/google/android/apps/youtube/datalib/distiller/c;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/distiller/c;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/distiller/c;-><init>()V

    return-object v0
.end method

.method public static b()Lcom/google/android/apps/youtube/datalib/distiller/d;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/distiller/d;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/distiller/d;-><init>()V

    return-object v0
.end method

.method public static c()Lcom/google/android/apps/youtube/datalib/distiller/f;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/distiller/f;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/distiller/f;-><init>()V

    return-object v0
.end method

.method public static d()Lcom/google/android/apps/youtube/datalib/distiller/e;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/distiller/e;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/distiller/e;-><init>()V

    return-object v0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/distiller/a;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/distiller/c;Lcom/google/android/apps/youtube/datalib/a/l;)V
    .locals 9

    new-instance v3, Lcom/google/android/apps/youtube/datalib/distiller/b;

    invoke-direct {v3, p0, p2, p1}, Lcom/google/android/apps/youtube/datalib/distiller/b;-><init>(Lcom/google/android/apps/youtube/datalib/distiller/a;Lcom/google/android/apps/youtube/datalib/a/l;Lcom/google/android/apps/youtube/datalib/distiller/c;)V

    new-instance v0, Lcom/google/android/apps/youtube/datalib/distiller/h;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->b:Lcom/google/android/apps/youtube/core/identity/ak;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->c:Lcom/google/android/apps/youtube/datalib/c/f;

    iget-object v6, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->d:Ljava/util/List;

    iget-object v7, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->f:Lcom/google/android/apps/youtube/datalib/config/a;

    iget-object v8, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->g:Ljava/lang/String;

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/datalib/distiller/h;-><init>(ILcom/google/android/apps/youtube/datalib/distiller/i;Lcom/google/android/apps/youtube/datalib/a/l;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/datalib/c/f;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/config/a;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->e:Lcom/android/volley/l;

    invoke-virtual {v1, v0}, Lcom/android/volley/l;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/distiller/d;Lcom/google/android/apps/youtube/datalib/a/l;)V
    .locals 9

    new-instance v0, Lcom/google/android/apps/youtube/datalib/distiller/h;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->b:Lcom/google/android/apps/youtube/core/identity/ak;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->c:Lcom/google/android/apps/youtube/datalib/c/f;

    iget-object v6, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->d:Ljava/util/List;

    iget-object v7, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->f:Lcom/google/android/apps/youtube/datalib/config/a;

    iget-object v8, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->g:Ljava/lang/String;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/datalib/distiller/h;-><init>(ILcom/google/android/apps/youtube/datalib/distiller/i;Lcom/google/android/apps/youtube/datalib/a/l;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/datalib/c/f;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/config/a;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->e:Lcom/android/volley/l;

    invoke-virtual {v1, v0}, Lcom/android/volley/l;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/distiller/e;Lcom/google/android/apps/youtube/datalib/a/l;)V
    .locals 9

    new-instance v0, Lcom/google/android/apps/youtube/datalib/distiller/h;

    const/4 v1, 0x3

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->b:Lcom/google/android/apps/youtube/core/identity/ak;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->c:Lcom/google/android/apps/youtube/datalib/c/f;

    iget-object v6, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->d:Ljava/util/List;

    iget-object v7, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->f:Lcom/google/android/apps/youtube/datalib/config/a;

    iget-object v8, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->g:Ljava/lang/String;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/datalib/distiller/h;-><init>(ILcom/google/android/apps/youtube/datalib/distiller/i;Lcom/google/android/apps/youtube/datalib/a/l;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/datalib/c/f;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/config/a;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->e:Lcom/android/volley/l;

    invoke-virtual {v1, v0}, Lcom/android/volley/l;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/distiller/f;Lcom/google/android/apps/youtube/datalib/a/l;)V
    .locals 9

    new-instance v0, Lcom/google/android/apps/youtube/datalib/distiller/h;

    const/4 v1, 0x1

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->b:Lcom/google/android/apps/youtube/core/identity/ak;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->c:Lcom/google/android/apps/youtube/datalib/c/f;

    iget-object v6, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->d:Ljava/util/List;

    iget-object v7, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->f:Lcom/google/android/apps/youtube/datalib/config/a;

    iget-object v8, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->g:Ljava/lang/String;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/youtube/datalib/distiller/h;-><init>(ILcom/google/android/apps/youtube/datalib/distiller/i;Lcom/google/android/apps/youtube/datalib/a/l;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/datalib/c/f;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/config/a;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/distiller/a;->e:Lcom/android/volley/l;

    invoke-virtual {v1, v0}, Lcom/android/volley/l;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method
