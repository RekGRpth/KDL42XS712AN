.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a([Lcom/google/a/a/a/a/gm;)Ljava/util/List;
    .locals 5

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, p0, v0

    iget-object v4, v3, Lcom/google/a/a/a/a/gm;->d:Lcom/google/a/a/a/a/fy;

    if-eqz v4, :cond_1

    new-instance v4, Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;

    iget-object v3, v3, Lcom/google/a/a/a/a/gm;->d:Lcom/google/a/a/a/a/fy;

    invoke-direct {v4, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/b;-><init>(Lcom/google/a/a/a/a/fy;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v4, v3, Lcom/google/a/a/a/a/gm;->b:Lcom/google/a/a/a/a/gc;

    if-eqz v4, :cond_0

    new-instance v4, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;

    iget-object v3, v3, Lcom/google/a/a/a/a/gm;->b:Lcom/google/a/a/a/a/gc;

    invoke-direct {v4, v3}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;-><init>(Lcom/google/a/a/a/a/gc;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-object v1
.end method
