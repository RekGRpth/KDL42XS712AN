.class public Lcom/google/android/apps/youtube/datalib/legacy/model/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:Landroid/net/Uri;

.field private c:Ljava/lang/String;

.field private d:Ljava/util/List;


# direct methods
.method public constructor <init>(ILandroid/net/Uri;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/i;->a:I

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/i;->b:Landroid/net/Uri;

    iput-object p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/i;->c:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/i;->d:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;
    .locals 5

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/i;->a:I

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/i;->b:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/i;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/i;->d:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardAction;-><init>(ILandroid/net/Uri;Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/i;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/i;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method
