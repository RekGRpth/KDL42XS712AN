.class public Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/apps/youtube/datalib/legacy/a/a;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final TYPE_APP:I = 0x2

.field public static final TYPE_NONE:I = 0x0

.field public static final TYPE_PRODUCT:I = 0x1

.field public static final TYPE_VOTE:I = 0x3


# instance fields
.field private final actions:Ljava/util/List;

.field private final events:Ljava/util/List;

.field private final infoCardApp:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

.field private final type:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/e;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/e;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(ILjava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->type:I

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->actions:Ljava/util/List;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->events:Ljava/util/List;

    iput-object p4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->infoCardApp:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    return-void
.end method

.method synthetic constructor <init>(ILjava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;Lcom/google/android/apps/youtube/datalib/legacy/model/e;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;-><init>(ILjava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getEvents()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getEvents()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getActions()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getActions()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getInfoCardApp()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getInfoCardApp()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getActions()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->actions:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getConverter()Lcom/google/android/apps/youtube/datalib/legacy/a/b;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getConverter()Lcom/google/android/apps/youtube/datalib/legacy/model/g;

    move-result-object v0

    return-object v0
.end method

.method public getConverter()Lcom/google/android/apps/youtube/datalib/legacy/model/g;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/g;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;)V

    return-object v0
.end method

.method public getEvents()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->events:Ljava/util/List;

    return-object v0
.end method

.method public getInfoCardApp()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->infoCardApp:Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->type:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getType()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getActions()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getEvents()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard;->getInfoCardApp()Lcom/google/android/apps/youtube/datalib/legacy/model/InfoCard$InfoCardApp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
