.class public final enum Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

.field public static final enum BULLETIN_POSTED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

.field public static final enum FRIEND_ADDED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

.field public static final enum USER_SUBSCRIPTION_ADDED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

.field public static final enum VIDEO_ADDED_TO_PLAYLIST:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

.field public static final enum VIDEO_COMMENTED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

.field public static final enum VIDEO_FAVORITED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

.field public static final enum VIDEO_LIKED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

.field public static final enum VIDEO_RECOMMENDED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

.field public static final enum VIDEO_SHARED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

.field public static final enum VIDEO_UPLOADED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;


# instance fields
.field public final stringId:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final targetsVideo:Z


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    const-string v1, "VIDEO_LIKED"

    sget v2, Lcom/google/android/youtube/p;->bJ:I

    invoke-direct {v0, v1, v5, v2, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_LIKED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    const-string v1, "VIDEO_SHARED"

    sget v2, Lcom/google/android/youtube/p;->bL:I

    invoke-direct {v0, v1, v4, v2, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_SHARED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    const-string v1, "VIDEO_FAVORITED"

    sget v2, Lcom/google/android/youtube/p;->bI:I

    invoke-direct {v0, v1, v6, v2, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_FAVORITED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    const-string v1, "VIDEO_COMMENTED"

    sget v2, Lcom/google/android/youtube/p;->bH:I

    invoke-direct {v0, v1, v7, v2, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_COMMENTED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    const-string v1, "VIDEO_UPLOADED"

    sget v2, Lcom/google/android/youtube/p;->bM:I

    invoke-direct {v0, v1, v8, v2, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_UPLOADED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    const-string v1, "VIDEO_RECOMMENDED"

    const/4 v2, 0x5

    sget v3, Lcom/google/android/youtube/p;->bK:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_RECOMMENDED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    const-string v1, "VIDEO_ADDED_TO_PLAYLIST"

    const/4 v2, 0x6

    sget v3, Lcom/google/android/youtube/p;->bG:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_ADDED_TO_PLAYLIST:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    const-string v1, "FRIEND_ADDED"

    const/4 v2, 0x7

    sget v3, Lcom/google/android/youtube/p;->bE:I

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->FRIEND_ADDED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    const-string v1, "USER_SUBSCRIPTION_ADDED"

    const/16 v2, 0x8

    sget v3, Lcom/google/android/youtube/p;->bF:I

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->USER_SUBSCRIPTION_ADDED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    const-string v1, "BULLETIN_POSTED"

    const/16 v2, 0x9

    sget v3, Lcom/google/android/youtube/p;->bD:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->BULLETIN_POSTED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_LIKED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_SHARED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_FAVORITED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_COMMENTED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_UPLOADED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_RECOMMENDED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->VIDEO_ADDED_TO_PLAYLIST:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->FRIEND_ADDED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->USER_SUBSCRIPTION_ADDED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->BULLETIN_POSTED:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->$VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->stringId:I

    iput-boolean p4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->targetsVideo:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->$VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    return-object v0
.end method
