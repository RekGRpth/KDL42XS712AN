.class public final Lcom/google/android/apps/youtube/datalib/innertube/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/datalib/c/f;

.field private final b:Lcom/google/android/apps/youtube/common/fromguava/e;

.field private final c:Ljava/util/List;

.field private final d:Ljava/util/List;

.field private final e:Lcom/google/android/apps/youtube/datalib/config/a;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/c/f;Lcom/google/android/apps/youtube/common/fromguava/e;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/config/a;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/c/f;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/r;->a:Lcom/google/android/apps/youtube/datalib/c/f;

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/innertube/r;->b:Lcom/google/android/apps/youtube/common/fromguava/e;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/r;->c:Ljava/util/List;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/r;->d:Ljava/util/List;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/config/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/r;->e:Lcom/google/android/apps/youtube/datalib/config/a;

    iput-object p6, p0, Lcom/google/android/apps/youtube/datalib/innertube/r;->f:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/youtube/datalib/innertube/r;->g:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/s;Ljava/lang/Class;Lcom/google/android/apps/youtube/datalib/a/l;)Lcom/google/android/apps/youtube/datalib/innertube/q;
    .locals 11

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/q;

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/r;->a:Lcom/google/android/apps/youtube/datalib/c/f;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/innertube/r;->c:Ljava/util/List;

    iget-object v6, p0, Lcom/google/android/apps/youtube/datalib/innertube/r;->d:Ljava/util/List;

    iget-object v7, p0, Lcom/google/android/apps/youtube/datalib/innertube/r;->e:Lcom/google/android/apps/youtube/datalib/config/a;

    iget-object v8, p0, Lcom/google/android/apps/youtube/datalib/innertube/r;->f:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/youtube/datalib/innertube/r;->g:Ljava/lang/String;

    const/4 v10, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/youtube/datalib/innertube/q;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/s;Ljava/lang/Class;Lcom/google/android/apps/youtube/datalib/a/l;Lcom/google/android/apps/youtube/datalib/c/f;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/config/a;Ljava/lang/String;Ljava/lang/String;B)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/r;->b:Lcom/google/android/apps/youtube/common/fromguava/e;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/r;->b:Lcom/google/android/apps/youtube/common/fromguava/e;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/fromguava/e;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/volley/q;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/q;->a(Lcom/android/volley/q;)V

    :cond_0
    return-object v0
.end method
