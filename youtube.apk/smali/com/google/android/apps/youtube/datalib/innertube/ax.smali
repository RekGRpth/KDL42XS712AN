.class public final Lcom/google/android/apps/youtube/datalib/innertube/ax;
.super Lcom/google/android/apps/youtube/datalib/innertube/b;
.source "SourceFile"


# instance fields
.field private final c:Ljava/util/Set;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/b;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ax;->c:Ljava/util/Set;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/p;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/ax;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/p;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/innertube/ax;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ax;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method protected final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ax;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    const-string v0, "subscription/unsubscribe"

    return-object v0
.end method

.method public final synthetic f()Lcom/google/protobuf/nano/c;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/ax;->b()V

    new-instance v1, Lcom/google/a/a/a/a/to;

    invoke-direct {v1}, Lcom/google/a/a/a/a/to;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/ax;->d()Lcom/google/a/a/a/a/ii;

    move-result-object v0

    iput-object v0, v1, Lcom/google/a/a/a/a/to;->b:Lcom/google/a/a/a/a/ii;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/ax;->c:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/ax;->c:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Lcom/google/a/a/a/a/to;->c:[Ljava/lang/String;

    return-object v1
.end method
