.class public final Lcom/google/android/apps/youtube/datalib/offline/i;
.super Lcom/google/android/apps/youtube/common/database/BasicKeyValueStore;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteOpenHelper;)V
    .locals 1

    const-string v0, "OfflineHttpRequestProto"

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/common/database/BasicKeyValueStore;-><init>(Landroid/database/sqlite/SQLiteOpenHelper;Ljava/lang/String;)V

    return-void
.end method

.method private static b([B)Lcom/google/android/apps/youtube/a/a/c;
    .locals 1

    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/youtube/a/a/c;->a([B)Lcom/google/android/apps/youtube/a/a/c;
    :try_end_0
    .catch Lcom/google/protobuf/micro/InvalidProtocolBufferMicroException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/apps/youtube/a/a/c;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/c;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic a([B)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/offline/i;->b([B)Lcom/google/android/apps/youtube/a/a/c;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)[B
    .locals 1

    check-cast p1, Lcom/google/android/apps/youtube/a/a/c;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/c;->s()[B

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic b(Ljava/lang/Object;)J
    .locals 2

    check-cast p1, Lcom/google/android/apps/youtube/a/a/c;

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/c;->j()Z

    move-result v0

    const-string v1, "Must have stored time set"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/a/a/c;->i()J

    move-result-wide v0

    return-wide v0
.end method
