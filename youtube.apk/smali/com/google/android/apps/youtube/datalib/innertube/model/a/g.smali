.class public Lcom/google/android/apps/youtube/datalib/innertube/model/a/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Ljava/util/List;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/g;->b:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/google/a/a/a/a/gl;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/g;->b:Ljava/util/List;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/gl;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/g;->a:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/a/a/a/a/gl;->b:[Lcom/google/a/a/a/a/gm;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/e;->a([Lcom/google/a/a/a/a/gm;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/g;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/g;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/g;->b:Ljava/util/List;

    return-object v0
.end method
