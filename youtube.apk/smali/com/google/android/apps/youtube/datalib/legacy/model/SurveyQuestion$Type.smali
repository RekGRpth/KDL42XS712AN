.class public final enum Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

.field public static final enum MULTI_SELECT:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

.field public static final enum SINGLE_ANSWERS:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

.field public static final enum UNSUPPORTED:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    const-string v1, "SINGLE_ANSWERS"

    const-string v2, "single-answer"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->SINGLE_ANSWERS:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    const-string v1, "MULTI_SELECT"

    const-string v2, "multi-select"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->MULTI_SELECT:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    const-string v1, "UNSUPPORTED"

    const-string v2, "unsupported"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->UNSUPPORTED:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->SINGLE_ANSWERS:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->MULTI_SELECT:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->UNSUPPORTED:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->$VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->name:Ljava/lang/String;

    return-void
.end method

.method public static fromName(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;
    .locals 5

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->values()[Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    iget-object v4, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->name:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->UNSUPPORTED:Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->$VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SurveyQuestion$Type;->name:Ljava/lang/String;

    return-object v0
.end method
