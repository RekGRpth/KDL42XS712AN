.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/ay;
.super Lcom/google/android/apps/youtube/datalib/legacy/a/b;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/a/b;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/a/b;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ay;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final a(Lorg/json/JSONObject;I)Lcom/google/android/apps/youtube/datalib/legacy/a/a;
    .locals 4

    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    new-instance v0, Lorg/json/JSONException;

    const-string v1, "Unsupported version"

    invoke-direct {v0, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    const-string v1, "urlMatchRegex"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "shouldAddVisitorId"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    const-string v3, "shouldAddUserAuth"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;-><init>(Ljava/lang/String;ZZ)V

    return-object v0
.end method

.method protected final a(Lorg/json/JSONObject;)V
    .locals 2

    const-string v0, "urlMatchRegex"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ay;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->getUrlMatchPattern()Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "shouldAddVisitorId"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ay;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->shouldAddVisitorId()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "shouldAddUserAuth"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/ay;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->shouldAddUserAuth()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    return-void
.end method
