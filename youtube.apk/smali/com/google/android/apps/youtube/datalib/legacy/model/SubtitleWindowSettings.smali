.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# static fields
.field public static final ANCHOR_BOTTOM:I = 0x20

.field public static final ANCHOR_CENTER_HORIZONTAL:I = 0x2

.field public static final ANCHOR_CENTER_VERTICAL:I = 0x10

.field public static final ANCHOR_LEFT:I = 0x1

.field public static final ANCHOR_POS_MAX:I = 0x64

.field public static final ANCHOR_POS_MIN:I = 0x0

.field public static final ANCHOR_RIGHT:I = 0x4

.field public static final ANCHOR_TOP:I = 0x8

.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final DEFAULT_ANCHOR_HORIZONTAL_POS:I = 0x32

.field public static final DEFAULT_ANCHOR_POINT:I = 0x22

.field public static final DEFAULT_ANCHOR_VERTICAL_POS:I = 0x5f

.field public static final DEFAULT_SUBTITLE_WINDOW_SETTINGS:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;


# instance fields
.field public final anchorHorizontalPos:I

.field public final anchorPoint:I

.field public final anchorVerticalPos:I

.field public final rollUp:Z

.field public final visible:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/af;

    invoke-direct {v0, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/af;-><init>(B)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;

    const/16 v1, 0x22

    const/16 v2, 0x32

    const/16 v3, 0x5f

    const/4 v4, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;-><init>(IIIZZ)V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->DEFAULT_SUBTITLE_WINDOW_SETTINGS:Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;

    return-void
.end method

.method public constructor <init>(IIIZZ)V
    .locals 6

    const/16 v5, 0x64

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-ltz p2, :cond_0

    if-gt p2, v5, :cond_0

    move v0, v1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "invalid anchorHorizontalPos: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    if-ltz p3, :cond_1

    if-gt p3, v5, :cond_1

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "invalid anchorVerticalPos: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    iput p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->anchorPoint:I

    iput p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->anchorHorizontalPos:I

    iput p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->anchorVerticalPos:I

    iput-boolean p4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->visible:Z

    iput-boolean p5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->rollUp:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    const/4 v5, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-ne v4, v0, :cond_1

    move v4, v0

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-ne v6, v0, :cond_0

    move v5, v0

    :cond_0
    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;-><init>(IIIZZ)V

    return-void

    :cond_1
    move v4, v5

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/apps/youtube/datalib/legacy/model/ae;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    const-string v0, "ap=%d, ah=%d, av=%d, vs=%b, sd=%b"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->anchorPoint:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->anchorHorizontalPos:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->anchorVerticalPos:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->visible:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-boolean v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->rollUp:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->anchorPoint:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->anchorHorizontalPos:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->anchorVerticalPos:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->visible:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/SubtitleWindowSettings;->rollUp:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
