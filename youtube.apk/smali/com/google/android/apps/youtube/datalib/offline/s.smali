.class public final Lcom/google/android/apps/youtube/datalib/offline/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/d/i;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/datalib/e/b;

.field private final b:Lcom/google/android/apps/youtube/datalib/offline/m;

.field private final c:Lcom/google/android/apps/youtube/common/d/j;

.field private final d:Lcom/google/android/apps/youtube/common/e/b;

.field private final e:Lcom/google/android/apps/youtube/datalib/config/c;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/datalib/offline/m;Lcom/google/android/apps/youtube/common/d/j;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/datalib/config/c;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/s;->a:Lcom/google/android/apps/youtube/datalib/e/b;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/offline/m;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/s;->b:Lcom/google/android/apps/youtube/datalib/offline/m;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/d/j;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/s;->c:Lcom/google/android/apps/youtube/common/d/j;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/s;->d:Lcom/google/android/apps/youtube/common/e/b;

    invoke-static {p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/config/c;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/offline/s;->e:Lcom/google/android/apps/youtube/datalib/config/c;

    return-void
.end method

.method public static a(JLcom/google/android/apps/youtube/datalib/config/c;)Lcom/google/android/apps/youtube/a/a/g;
    .locals 4

    new-instance v0, Lcom/google/android/apps/youtube/a/a/g;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/g;-><init>()V

    sget-object v1, Lcom/google/android/apps/youtube/datalib/offline/q;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/a/a/g;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/a/a/g;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    add-long/2addr v1, p0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/a/a/g;->a(J)Lcom/google/android/apps/youtube/a/a/g;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p2}, Lcom/google/android/apps/youtube/datalib/config/c;->d()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/a/a/g;->b(J)Lcom/google/android/apps/youtube/a/a/g;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/apps/youtube/a/a/g;)Lcom/google/android/apps/youtube/common/d/h;
    .locals 7

    new-instance v0, Lcom/google/android/apps/youtube/datalib/offline/q;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/offline/s;->a:Lcom/google/android/apps/youtube/datalib/e/b;

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/offline/s;->b:Lcom/google/android/apps/youtube/datalib/offline/m;

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/offline/s;->c:Lcom/google/android/apps/youtube/common/d/j;

    iget-object v5, p0, Lcom/google/android/apps/youtube/datalib/offline/s;->d:Lcom/google/android/apps/youtube/common/e/b;

    iget-object v6, p0, Lcom/google/android/apps/youtube/datalib/offline/s;->e:Lcom/google/android/apps/youtube/datalib/config/c;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/datalib/offline/q;-><init>(Lcom/google/android/apps/youtube/a/a/g;Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/datalib/offline/m;Lcom/google/android/apps/youtube/common/d/j;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/datalib/config/c;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/offline/q;->a:Ljava/lang/String;

    return-object v0
.end method
