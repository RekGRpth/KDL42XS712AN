.class public final Lcom/google/android/apps/youtube/datalib/a/h;
.super Lcom/android/volley/toolbox/a;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/android/apps/youtube/datalib/config/e;

.field private final e:Lcom/google/android/apps/youtube/common/e/b;


# direct methods
.method public constructor <init>(Lcom/android/volley/toolbox/f;Lcom/google/android/apps/youtube/datalib/config/e;Lcom/google/android/apps/youtube/common/e/b;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/volley/toolbox/a;-><init>(Lcom/android/volley/toolbox/f;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/config/e;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/a/h;->d:Lcom/google/android/apps/youtube/datalib/config/e;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/a/h;->e:Lcom/google/android/apps/youtube/common/e/b;

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/Request;)Lcom/android/volley/j;
    .locals 9

    instance-of v0, p1, Lcom/google/android/apps/youtube/datalib/a/r;

    if-eqz v0, :cond_3

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/youtube/datalib/a/r;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/a/h;->d:Lcom/google/android/apps/youtube/datalib/config/e;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/datalib/config/e;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/a/r;->s()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/a/h;->e:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v2

    invoke-super {p0, p1}, Lcom/android/volley/toolbox/a;->a(Lcom/android/volley/Request;)Lcom/android/volley/j;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/a/h;->e:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v4}, Lcom/google/android/apps/youtube/common/e/b;->b()J

    move-result-wide v4

    sub-long v2, v4, v2

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/a/h;->d:Lcom/google/android/apps/youtube/datalib/config/e;

    invoke-interface {v4}, Lcom/google/android/apps/youtube/datalib/config/e;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Response for %s took %d ms and had status code %d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/a/r;->c()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v7

    const/4 v2, 0x2

    iget v3, v1, Lcom/android/volley/j;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v2

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/a/h;->d:Lcom/google/android/apps/youtube/datalib/config/e;

    invoke-interface {v2}, Lcom/google/android/apps/youtube/datalib/config/e;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "Logging response for YouTube API call."

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/a/r;->b(Lcom/android/volley/j;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    :goto_1
    return-object v0

    :cond_3
    invoke-super {p0, p1}, Lcom/android/volley/toolbox/a;->a(Lcom/android/volley/Request;)Lcom/android/volley/j;

    move-result-object v0

    goto :goto_1
.end method
