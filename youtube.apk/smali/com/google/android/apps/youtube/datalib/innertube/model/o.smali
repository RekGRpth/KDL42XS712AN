.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/a/a/a/a/it;

.field private b:Ljava/util/List;

.field private c:Lcom/google/a/a/a/a/dp;


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/it;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/it;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->a:Lcom/google/a/a/a/a/it;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->b:Ljava/util/List;

    if-nez v0, :cond_d

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->a:Lcom/google/a/a/a/a/it;

    iget-object v1, v1, Lcom/google/a/a/a/a/it;->b:[Lcom/google/a/a/a/a/iv;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->b:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->a:Lcom/google/a/a/a/a/it;

    iget-object v2, v0, Lcom/google/a/a/a/a/it;->b:[Lcom/google/a/a/a/a/iv;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_d

    aget-object v0, v2, v1

    iget-object v4, v0, Lcom/google/a/a/a/a/iv;->m:Lcom/google/a/a/a/a/db;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/e;

    iget-object v0, v0, Lcom/google/a/a/a/a/iv;->m:Lcom/google/a/a/a/a/db;

    invoke-direct {v5, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/e;-><init>(Lcom/google/a/a/a/a/db;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v4, v0, Lcom/google/a/a/a/a/iv;->k:Lcom/google/a/a/a/a/de;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/f;

    iget-object v0, v0, Lcom/google/a/a/a/a/iv;->k:Lcom/google/a/a/a/a/de;

    invoke-direct {v5, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/f;-><init>(Lcom/google/a/a/a/a/de;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v4, v0, Lcom/google/a/a/a/a/iv;->i:Lcom/google/a/a/a/a/dj;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/h;

    iget-object v0, v0, Lcom/google/a/a/a/a/iv;->i:Lcom/google/a/a/a/a/dj;

    invoke-direct {v5, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/h;-><init>(Lcom/google/a/a/a/a/dj;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget-object v4, v0, Lcom/google/a/a/a/a/iv;->e:Lcom/google/a/a/a/a/ab;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/a;

    iget-object v0, v0, Lcom/google/a/a/a/a/iv;->e:Lcom/google/a/a/a/a/ab;

    invoke-direct {v5, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a;-><init>(Lcom/google/a/a/a/a/ab;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    iget-object v4, v0, Lcom/google/a/a/a/a/iv;->v:Lcom/google/a/a/a/a/kg;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/s;

    iget-object v0, v0, Lcom/google/a/a/a/a/iv;->v:Lcom/google/a/a/a/a/kg;

    invoke-direct {v5, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/s;-><init>(Lcom/google/a/a/a/a/kg;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    iget-object v4, v0, Lcom/google/a/a/a/a/iv;->y:Lcom/google/a/a/a/a/df;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/g;

    iget-object v0, v0, Lcom/google/a/a/a/a/iv;->y:Lcom/google/a/a/a/a/df;

    invoke-direct {v5, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/g;-><init>(Lcom/google/a/a/a/a/df;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_6
    iget-object v4, v0, Lcom/google/a/a/a/a/iv;->q:Lcom/google/a/a/a/a/uw;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/at;

    iget-object v0, v0, Lcom/google/a/a/a/a/iv;->q:Lcom/google/a/a/a/a/uw;

    invoke-direct {v5, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/at;-><init>(Lcom/google/a/a/a/a/uw;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_7
    iget-object v4, v0, Lcom/google/a/a/a/a/iv;->S:Lcom/google/a/a/a/a/pk;

    if-eqz v4, :cond_9

    iget-object v4, v0, Lcom/google/a/a/a/a/iv;->S:Lcom/google/a/a/a/a/pk;

    invoke-static {v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/k;->a(Lcom/google/a/a/a/a/pk;)Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/k;

    iget-object v0, v0, Lcom/google/a/a/a/a/iv;->S:Lcom/google/a/a/a/a/pk;

    invoke-direct {v5, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/k;-><init>(Lcom/google/a/a/a/a/pk;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_8
    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;

    iget-object v0, v0, Lcom/google/a/a/a/a/iv;->S:Lcom/google/a/a/a/a/pk;

    invoke-direct {v5, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/ah;-><init>(Lcom/google/a/a/a/a/pk;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_9
    iget-object v4, v0, Lcom/google/a/a/a/a/iv;->l:Lcom/google/a/a/a/a/ff;

    if-eqz v4, :cond_c

    iget-object v4, v0, Lcom/google/a/a/a/a/iv;->l:Lcom/google/a/a/a/a/ff;

    invoke-static {v4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v4, Lcom/google/a/a/a/a/ff;->j:Lcom/google/a/a/a/a/fg;

    if-eqz v0, :cond_b

    iget-object v0, v4, Lcom/google/a/a/a/a/ff;->j:Lcom/google/a/a/a/a/fg;

    iget-object v0, v0, Lcom/google/a/a/a/a/fg;->d:Lcom/google/a/a/a/a/op;

    if-eqz v0, :cond_a

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/aa;

    invoke-direct {v0, v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/aa;-><init>(Lcom/google/a/a/a/a/ff;)V

    :goto_2
    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->b:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_a
    iget-object v0, v4, Lcom/google/a/a/a/a/ff;->j:Lcom/google/a/a/a/a/fg;

    iget-object v0, v0, Lcom/google/a/a/a/a/fg;->b:Lcom/google/a/a/a/a/uy;

    if-eqz v0, :cond_b

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/as;

    invoke-direct {v0, v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/as;-><init>(Lcom/google/a/a/a/a/ff;)V

    goto :goto_2

    :cond_b
    const/4 v0, 0x0

    goto :goto_2

    :cond_c
    iget-object v4, v0, Lcom/google/a/a/a/a/iv;->L:Lcom/google/a/a/a/a/jl;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->b:Ljava/util/List;

    new-instance v5, Lcom/google/android/apps/youtube/datalib/innertube/model/r;

    iget-object v0, v0, Lcom/google/a/a/a/a/iv;->L:Lcom/google/a/a/a/a/jl;

    invoke-direct {v5, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/r;-><init>(Lcom/google/a/a/a/a/jl;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->b:Ljava/util/List;

    return-object v0
.end method

.method public final b()Lcom/google/a/a/a/a/dp;
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->c:Lcom/google/a/a/a/a/dp;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/a/a/a/a/dp;

    invoke-direct {v0}, Lcom/google/a/a/a/a/dp;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->c:Lcom/google/a/a/a/a/dp;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->a:Lcom/google/a/a/a/a/it;

    iget-object v1, v0, Lcom/google/a/a/a/a/it;->c:[Lcom/google/a/a/a/a/iu;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    iget-object v4, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->c:Lcom/google/a/a/a/a/dp;

    iget-object v3, v3, Lcom/google/a/a/a/a/iu;->b:Lcom/google/a/a/a/a/li;

    iput-object v3, v4, Lcom/google/a/a/a/a/dp;->c:Lcom/google/a/a/a/a/li;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/o;->c:Lcom/google/a/a/a/a/dp;

    return-object v0
.end method
