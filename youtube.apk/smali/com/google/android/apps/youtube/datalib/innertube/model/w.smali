.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/w;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/a/a/a/a/mw;

.field private final b:I

.field private final c:Z

.field private final d:Z


# direct methods
.method public constructor <init>(Lcom/google/a/a/a/a/mw;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/mw;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->a:Lcom/google/a/a/a/a/mw;

    iget v0, p1, Lcom/google/a/a/a/a/mw;->b:I

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b:I

    if-eqz p2, :cond_1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->d:Z

    iget-boolean v0, p1, Lcom/google/a/a/a/a/mw;->e:Z

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->c:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-boolean v0, p1, Lcom/google/a/a/a/a/mw;->f:Z

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->a:Lcom/google/a/a/a/a/mw;

    iget-object v0, v0, Lcom/google/a/a/a/a/mw;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/google/android/apps/youtube/datalib/innertube/model/w;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->a:Lcom/google/a/a/a/a/mw;

    if-nez v2, :cond_4

    iget-object v2, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->a:Lcom/google/a/a/a/a/mw;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b:I

    iget v3, p1, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->c:Z

    return v0
.end method

.method public final hashCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->b:I

    add-int/lit8 v0, v0, 0x1f

    return v0
.end method

.method public final i()Lcom/google/a/a/a/a/ai;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->a:Lcom/google/a/a/a/a/mw;

    iget-object v0, v0, Lcom/google/a/a/a/a/mw;->g:Lcom/google/a/a/a/a/ai;

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->d:Z

    return v0
.end method

.method public final k()Lcom/google/a/a/a/a/lv;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/w;->a:Lcom/google/a/a/a/a/mw;

    iget-object v0, v0, Lcom/google/a/a/a/a/mw;->h:Lcom/google/a/a/a/a/lv;

    goto :goto_0
.end method
