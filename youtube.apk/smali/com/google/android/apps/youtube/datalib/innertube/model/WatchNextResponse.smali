.class public Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final backgroundLoopAutoplaySet:Lcom/google/a/a/a/a/ag;

.field private final backgroundLoopShuffleAutoplaySet:Lcom/google/a/a/a/a/ag;

.field private final backgroundNormalAutoplaySet:Lcom/google/a/a/a/a/ag;

.field private final backgroundShuffleAutoplaySet:Lcom/google/a/a/a/a/ag;

.field private final clickTrackingParams:[B

.field private final currentNavigationEndpoint:Lcom/google/a/a/a/a/kz;

.field private final loopAutoplaySet:Lcom/google/a/a/a/a/ag;

.field private final loopShuffleAutoplaySet:Lcom/google/a/a/a/a/ag;

.field private final normalAutoplaySet:Lcom/google/a/a/a/a/ag;

.field private final playlistPanel:Lcom/google/android/apps/youtube/datalib/innertube/model/ac;

.field private final sectionList:Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

.field private final shuffleAutoplaySet:Lcom/google/a/a/a/a/ag;

.field private final videoId:Ljava/lang/String;

.field private final videoMetadataRenderer:Lcom/google/a/a/a/a/uv;

.field private final watchNextResponseProto:Lcom/google/a/a/a/a/wg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/az;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/az;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    new-instance v0, Lcom/google/a/a/a/a/wg;

    invoke-direct {v0}, Lcom/google/a/a/a/a/wg;-><init>()V

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/e/j;->b(Landroid/os/Parcel;Lcom/google/protobuf/nano/c;)Lcom/google/protobuf/nano/c;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/wg;

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;-><init>(Lcom/google/a/a/a/a/wg;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/apps/youtube/datalib/innertube/model/az;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/a/a/a/a/wg;)V
    .locals 20

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/a/a/a/a/wg;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->watchNextResponseProto:Lcom/google/a/a/a/a/wg;

    const/4 v1, 0x0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/a/a/a/a/wg;->j:Lcom/google/a/a/a/a/kz;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->currentNavigationEndpoint:Lcom/google/a/a/a/a/kz;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->currentNavigationEndpoint:Lcom/google/a/a/a/a/kz;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->currentNavigationEndpoint:Lcom/google/a/a/a/a/kz;

    iget-object v2, v2, Lcom/google/a/a/a/a/kz;->b:[B

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->clickTrackingParams:[B

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->currentNavigationEndpoint:Lcom/google/a/a/a/a/kz;

    iget-object v2, v2, Lcom/google/a/a/a/a/kz;->i:Lcom/google/a/a/a/a/wb;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->currentNavigationEndpoint:Lcom/google/a/a/a/a/kz;

    iget-object v1, v1, Lcom/google/a/a/a/a/kz;->i:Lcom/google/a/a/a/a/wb;

    iget-object v1, v1, Lcom/google/a/a/a/a/wb;->b:Ljava/lang/String;

    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->videoId:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/a/a/a/a/wg;->h:Lcom/google/a/a/a/a/wk;

    if-eqz v1, :cond_5

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/a/a/a/a/wg;->h:Lcom/google/a/a/a/a/wk;

    iget-object v1, v1, Lcom/google/a/a/a/a/wk;->b:Lcom/google/a/a/a/a/rp;

    move-object v4, v1

    :goto_1
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x0

    if-eqz v4, :cond_d

    iget-object v12, v4, Lcom/google/a/a/a/a/rp;->b:Lcom/google/a/a/a/a/rt;

    if-eqz v12, :cond_1

    iget-object v1, v4, Lcom/google/a/a/a/a/rp;->b:Lcom/google/a/a/a/a/rt;

    iget-object v1, v1, Lcom/google/a/a/a/a/rt;->b:Lcom/google/a/a/a/a/qq;

    :cond_1
    iget-object v12, v4, Lcom/google/a/a/a/a/rp;->c:Lcom/google/a/a/a/a/rs;

    if-eqz v12, :cond_2

    iget-object v12, v4, Lcom/google/a/a/a/a/rp;->c:Lcom/google/a/a/a/a/rs;

    iget-object v12, v12, Lcom/google/a/a/a/a/rs;->b:Lcom/google/a/a/a/a/ol;

    if-eqz v12, :cond_2

    iget-object v2, v4, Lcom/google/a/a/a/a/rp;->c:Lcom/google/a/a/a/a/rs;

    iget-object v12, v2, Lcom/google/a/a/a/a/rs;->b:Lcom/google/a/a/a/a/ol;

    new-instance v2, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->clickTrackingParams:[B

    invoke-direct {v2, v12, v13}, Lcom/google/android/apps/youtube/datalib/innertube/model/ac;-><init>(Lcom/google/a/a/a/a/ol;[B)V

    :cond_2
    iget-object v12, v4, Lcom/google/a/a/a/a/rp;->d:Lcom/google/a/a/a/a/rq;

    if-eqz v12, :cond_c

    iget-object v12, v4, Lcom/google/a/a/a/a/rp;->d:Lcom/google/a/a/a/a/rq;

    iget-object v12, v12, Lcom/google/a/a/a/a/rq;->b:Lcom/google/a/a/a/a/af;

    if-eqz v12, :cond_c

    iget-object v4, v4, Lcom/google/a/a/a/a/rp;->d:Lcom/google/a/a/a/a/rq;

    iget-object v13, v4, Lcom/google/a/a/a/a/rq;->b:Lcom/google/a/a/a/a/af;

    iget-object v14, v13, Lcom/google/a/a/a/a/af;->b:[Lcom/google/a/a/a/a/ag;

    array-length v15, v14

    const/4 v4, 0x0

    move v12, v4

    :goto_2
    if-ge v12, v15, :cond_6

    aget-object v4, v14, v12

    iget v0, v4, Lcom/google/a/a/a/a/ag;->b:I

    move/from16 v16, v0

    packed-switch v16, :pswitch_data_0

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    :goto_3
    add-int/lit8 v7, v12, 0x1

    move v12, v7

    move-object v7, v6

    move-object v6, v5

    move-object v5, v4

    goto :goto_2

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->currentNavigationEndpoint:Lcom/google/a/a/a/a/kz;

    iget-object v2, v2, Lcom/google/a/a/a/a/kz;->w:Lcom/google/a/a/a/a/me;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->currentNavigationEndpoint:Lcom/google/a/a/a/a/kz;

    iget-object v1, v1, Lcom/google/a/a/a/a/kz;->w:Lcom/google/a/a/a/a/me;

    iget-object v1, v1, Lcom/google/a/a/a/a/me;->b:Ljava/lang/String;

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/google/android/apps/youtube/datalib/innertube/b;->a:[B

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->clickTrackingParams:[B

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    move-object v4, v1

    goto :goto_1

    :pswitch_0
    move-object/from16 v19, v5

    move-object v5, v6

    move-object v6, v4

    move-object/from16 v4, v19

    goto :goto_3

    :pswitch_1
    move-object v6, v7

    move-object/from16 v19, v4

    move-object v4, v5

    move-object/from16 v5, v19

    goto :goto_3

    :pswitch_2
    move-object v5, v6

    move-object v6, v7

    goto :goto_3

    :pswitch_3
    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    goto :goto_3

    :cond_6
    iget-object v13, v13, Lcom/google/a/a/a/a/af;->e:[Lcom/google/a/a/a/a/ag;

    array-length v14, v13

    const/4 v4, 0x0

    move v12, v4

    move-object v4, v8

    :goto_4
    if-ge v12, v14, :cond_7

    aget-object v8, v13, v12

    iget v15, v8, Lcom/google/a/a/a/a/ag;->b:I

    packed-switch v15, :pswitch_data_1

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    :goto_5
    add-int/lit8 v11, v12, 0x1

    move v12, v11

    move-object v11, v10

    move-object v10, v9

    move-object v9, v8

    goto :goto_4

    :pswitch_4
    move-object/from16 v19, v9

    move-object v9, v10

    move-object v10, v8

    move-object/from16 v8, v19

    goto :goto_5

    :pswitch_5
    move-object v10, v11

    move-object/from16 v19, v8

    move-object v8, v9

    move-object/from16 v9, v19

    goto :goto_5

    :pswitch_6
    move-object v9, v10

    move-object v10, v11

    goto :goto_5

    :pswitch_7
    move-object v4, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v11

    goto :goto_5

    :cond_7
    move-object v8, v7

    move-object v12, v2

    move-object v7, v6

    move-object v2, v1

    move-object v6, v5

    move-object v5, v3

    :goto_6
    const/4 v1, 0x0

    if-eqz v2, :cond_b

    new-instance v3, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    invoke-direct {v3, v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/aj;-><init>(Lcom/google/a/a/a/a/qq;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->sectionList:Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    iget-object v13, v2, Lcom/google/a/a/a/a/qq;->b:[Lcom/google/a/a/a/a/qt;

    array-length v14, v13

    const/4 v2, 0x0

    :goto_7
    if-ge v2, v14, :cond_8

    aget-object v3, v13, v2

    iget-object v3, v3, Lcom/google/a/a/a/a/qt;->b:Lcom/google/a/a/a/a/it;

    if-eqz v3, :cond_a

    iget-object v15, v3, Lcom/google/a/a/a/a/it;->b:[Lcom/google/a/a/a/a/iv;

    array-length v0, v15

    move/from16 v16, v0

    const/4 v3, 0x0

    :goto_8
    move/from16 v0, v16

    if-ge v3, v0, :cond_a

    aget-object v17, v15, v3

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/a/a/a/a/iv;->r:Lcom/google/a/a/a/a/uv;

    move-object/from16 v18, v0

    if-eqz v18, :cond_9

    move-object/from16 v0, v17

    iget-object v1, v0, Lcom/google/a/a/a/a/iv;->r:Lcom/google/a/a/a/a/uv;

    :cond_8
    :goto_9
    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->videoMetadataRenderer:Lcom/google/a/a/a/a/uv;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->playlistPanel:Lcom/google/android/apps/youtube/datalib/innertube/model/ac;

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->normalAutoplaySet:Lcom/google/a/a/a/a/ag;

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->loopAutoplaySet:Lcom/google/a/a/a/a/ag;

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->shuffleAutoplaySet:Lcom/google/a/a/a/a/ag;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->loopShuffleAutoplaySet:Lcom/google/a/a/a/a/ag;

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->backgroundNormalAutoplaySet:Lcom/google/a/a/a/a/ag;

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->backgroundLoopAutoplaySet:Lcom/google/a/a/a/a/ag;

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->backgroundShuffleAutoplaySet:Lcom/google/a/a/a/a/ag;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->backgroundLoopShuffleAutoplaySet:Lcom/google/a/a/a/a/ag;

    return-void

    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_b
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->sectionList:Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    goto :goto_9

    :cond_c
    move-object v4, v8

    move-object v12, v2

    move-object v8, v7

    move-object v2, v1

    move-object v7, v6

    move-object v6, v5

    move-object v5, v3

    goto :goto_6

    :cond_d
    move-object v4, v8

    move-object v12, v2

    move-object v8, v7

    move-object v2, v1

    move-object v7, v6

    move-object v6, v5

    move-object v5, v3

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method public static final createWatchNextResponseForOfflinePlaylist(Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;Landroid/content/Context;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;I)Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->watchNextResponseProto:Lcom/google/a/a/a/a/wg;

    new-instance p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;

    invoke-static {p1, v0, p2, p3, p4}, Lcom/google/android/apps/youtube/core/player/fetcher/g;->a(Landroid/content/Context;Lcom/google/a/a/a/a/wg;Lcom/google/android/apps/youtube/datalib/legacy/model/Playlist;Ljava/util/List;I)Lcom/google/a/a/a/a/wg;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;-><init>(Lcom/google/a/a/a/a/wg;)V

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAutoplaySet(ZZZ)Lcom/google/a/a/a/a/ag;
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->isLoopSupported()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->isShuffleSupported()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    if-nez v2, :cond_3

    if-nez v0, :cond_3

    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->backgroundNormalAutoplaySet:Lcom/google/a/a/a/a/ag;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->backgroundNormalAutoplaySet:Lcom/google/a/a/a/a/ag;

    :goto_2
    return-object v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->normalAutoplaySet:Lcom/google/a/a/a/a/ag;

    goto :goto_2

    :cond_3
    if-nez v2, :cond_5

    if-eqz p3, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->backgroundShuffleAutoplaySet:Lcom/google/a/a/a/a/ag;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->backgroundShuffleAutoplaySet:Lcom/google/a/a/a/a/ag;

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->shuffleAutoplaySet:Lcom/google/a/a/a/a/ag;

    goto :goto_2

    :cond_5
    if-nez v0, :cond_7

    if-eqz p3, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->backgroundLoopAutoplaySet:Lcom/google/a/a/a/a/ag;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->backgroundLoopAutoplaySet:Lcom/google/a/a/a/a/ag;

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->loopAutoplaySet:Lcom/google/a/a/a/a/ag;

    goto :goto_2

    :cond_7
    if-eqz p3, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->backgroundLoopShuffleAutoplaySet:Lcom/google/a/a/a/a/ag;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->backgroundLoopShuffleAutoplaySet:Lcom/google/a/a/a/a/ag;

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->loopShuffleAutoplaySet:Lcom/google/a/a/a/a/ag;

    goto :goto_2
.end method

.method public getClickTrackingParams()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->clickTrackingParams:[B

    return-object v0
.end method

.method public getContinuationRenderers()Lcom/google/a/a/a/a/dq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->watchNextResponseProto:Lcom/google/a/a/a/a/wg;

    iget-object v0, v0, Lcom/google/a/a/a/a/wg;->i:Lcom/google/a/a/a/a/dq;

    return-object v0
.end method

.method public getCurrentWatchEndpoint()Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->currentNavigationEndpoint:Lcom/google/a/a/a/a/kz;

    return-object v0
.end method

.method public getPlaylistPanel()Lcom/google/android/apps/youtube/datalib/innertube/model/ac;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->playlistPanel:Lcom/google/android/apps/youtube/datalib/innertube/model/ac;

    return-object v0
.end method

.method public getSectionList()Lcom/google/android/apps/youtube/datalib/innertube/model/aj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->sectionList:Lcom/google/android/apps/youtube/datalib/innertube/model/aj;

    return-object v0
.end method

.method public getVideoId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->videoId:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoMetadataRenderer()Lcom/google/a/a/a/a/uv;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->videoMetadataRenderer:Lcom/google/a/a/a/a/uv;

    return-object v0
.end method

.method public isLoopSupported()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->loopAutoplaySet:Lcom/google/a/a/a/a/ag;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShuffleSupported()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->shuffleAutoplaySet:Lcom/google/a/a/a/a/ag;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/WatchNextResponse;->watchNextResponseProto:Lcom/google/a/a/a/a/wg;

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/e/j;->a(Landroid/os/Parcel;Lcom/google/protobuf/nano/c;)V

    return-void
.end method
