.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

.field private final b:Z

.field private final c:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;Z)V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;ZJ)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;ZJ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->a:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    iput-boolean p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->b:Z

    iput-wide p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->c:J

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->a:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)Lcom/google/android/apps/youtube/datalib/legacy/model/t;
    .locals 4

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->b:Z

    iget-wide v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->c:J

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/t;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;ZJ)V

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->a:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getVideoId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->a:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getItag()I

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->b:Z

    return v0
.end method

.method public final e()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->c:J

    return-wide v0
.end method

.method public final f()J
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->a:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getContentLength()J

    move-result-wide v0

    return-wide v0
.end method

.method public final g()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->c:J

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/t;->a:Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getContentLength()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
