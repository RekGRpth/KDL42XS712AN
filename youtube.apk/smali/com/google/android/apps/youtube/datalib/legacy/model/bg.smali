.class public Lcom/google/android/apps/youtube/datalib/legacy/model/bg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/legacy/model/r;


# instance fields
.field private a:Ljava/util/List;

.field private b:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;->a:Ljava/util/List;

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;->NO_TRACKING_AUTH_SETTINGS:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/legacy/model/bf;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/bf;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bf;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;)Lcom/google/android/apps/youtube/datalib/legacy/model/bg;
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;->b:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)Lcom/google/android/apps/youtube/datalib/legacy/model/bg;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/bg;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;->a:Ljava/util/List;

    return-object p0
.end method

.method public synthetic build()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bg;->a()Lcom/google/android/apps/youtube/datalib/legacy/model/bf;

    move-result-object v0

    return-object v0
.end method
