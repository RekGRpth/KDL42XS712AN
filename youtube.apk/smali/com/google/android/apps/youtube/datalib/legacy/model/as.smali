.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/as;
.super Lcom/google/android/apps/youtube/datalib/legacy/a/b;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/a/b;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/datalib/legacy/a/b;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/as;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method protected final a(Lorg/json/JSONObject;I)Lcom/google/android/apps/youtube/datalib/legacy/a/a;
    .locals 4

    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    new-instance v0, Lorg/json/JSONException;

    const-string v1, "Unsupported version"

    invoke-direct {v0, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    new-instance v0, Lcom/google/android/apps/youtube/a/a/a/b;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/a/a/a/b;-><init>()V

    const-string v2, "data_pb"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/a/a/a/b;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;-><init>(Lcom/google/android/apps/youtube/a/a/a/b;Lcom/google/android/apps/youtube/datalib/legacy/model/aq;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Lorg/json/JSONException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to parse proto: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected final a(Lorg/json/JSONObject;)V
    .locals 3

    const-string v0, "data_pb"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/as;->a:Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;

    # getter for: Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->surveyProto:Lcom/google/android/apps/youtube/a/a/a/b;
    invoke-static {v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;->access$100(Lcom/google/android/apps/youtube/datalib/legacy/model/Survey;)Lcom/google/android/apps/youtube/a/a/a/b;

    move-result-object v1

    invoke-static {v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;)[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method
