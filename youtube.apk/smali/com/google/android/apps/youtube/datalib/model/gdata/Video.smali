.class public final Lcom/google/android/apps/youtube/datalib/model/gdata/Video;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static DEFAULT_LANG_REGEX:Ljava/util/regex/Pattern;


# instance fields
.field public final accessControl:Ljava/util/Map;

.field public final adultContent:Z

.field public final captionTracksUri:Landroid/net/Uri;

.field public final categoryLabel:Ljava/lang/String;

.field public final categoryTerm:Ljava/lang/String;

.field public final defaultThumbnailUri:Landroid/net/Uri;

.field public final description:Ljava/lang/String;

.field public final dislikesCount:J

.field public final duration:I

.field public final editUri:Landroid/net/Uri;

.field public final hqThumbnailUri:Landroid/net/Uri;

.field public final id:Ljava/lang/String;

.field public final is3d:Z

.field public final isInOfflineStore:Z

.field public final isUpsell:Z

.field public final likesCount:J

.field public final liveEventUri:Landroid/net/Uri;

.field public final location:Ljava/lang/String;

.field public final monetize:Z

.field public final monetizeExceptionCountries:Ljava/util/Set;

.field public final mqThumbnailUri:Landroid/net/Uri;

.field public final offlineChannelAvatarThumbnailUrl:Ljava/lang/String;

.field public final owner:Ljava/lang/String;

.field public final ownerDisplayName:Ljava/lang/String;

.field public final ownerUri:Landroid/net/Uri;

.field public final paidContent:Z

.field public final privacy:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

.field public final publishedDate:Ljava/util/Date;

.field public final sdThumbnailUri:Landroid/net/Uri;

.field public final showSubtitlesAlways:Z

.field public final state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

.field public final streams:Ljava/util/Set;

.field public final tags:Ljava/lang/String;

.field public final title:Ljava/lang/String;

.field public final uploadedDate:Ljava/util/Date;

.field public final viewCount:J

.field public final watchUri:Landroid/net/Uri;

.field public final where:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "yt:cc_default_lang=([a-zA-Z]{2})"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->DEFAULT_LANG_REGEX:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/util/Set;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;IJJJLjava/lang/String;Landroid/net/Uri;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/youtube/datalib/model/gdata/Video$State;ZLjava/util/Set;ZZLandroid/net/Uri;ZLjava/lang/String;ZLjava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v2, "youTubeId can\'t be empty"

    invoke-static {p1, v2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->streams:Ljava/util/Set;

    iput-object p3, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->watchUri:Landroid/net/Uri;

    iput-object p4, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->defaultThumbnailUri:Landroid/net/Uri;

    iput-object p5, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->mqThumbnailUri:Landroid/net/Uri;

    iput-object p6, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->hqThumbnailUri:Landroid/net/Uri;

    iput-object p7, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->sdThumbnailUri:Landroid/net/Uri;

    iput-object p8, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->editUri:Landroid/net/Uri;

    iput-object p9, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->captionTracksUri:Landroid/net/Uri;

    iput-object p10, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->title:Ljava/lang/String;

    iput p11, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->duration:I

    iput-wide p12, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->viewCount:J

    move-wide/from16 v0, p14

    iput-wide v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->likesCount:J

    move-wide/from16 v0, p16

    iput-wide v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->dislikesCount:J

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->owner:Ljava/lang/String;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->ownerUri:Landroid/net/Uri;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->uploadedDate:Ljava/util/Date;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->publishedDate:Ljava/util/Date;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->categoryTerm:Ljava/lang/String;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->categoryLabel:Ljava/lang/String;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->tags:Ljava/lang/String;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->description:Ljava/lang/String;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->privacy:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->accessControl:Ljava/util/Map;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->location:Ljava/lang/String;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->where:Ljava/lang/String;

    move/from16 v0, p30

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->adultContent:Z

    invoke-static/range {p31 .. p31}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    move/from16 v0, p32

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->monetize:Z

    if-nez p33, :cond_0

    const/4 v2, 0x0

    :goto_0
    iput-object v2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->monetizeExceptionCountries:Ljava/util/Set;

    if-eqz p24, :cond_1

    const-string v2, "yt:cc=alwayson"

    move-object/from16 v0, p24

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->showSubtitlesAlways:Z

    move/from16 v0, p34

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->paidContent:Z

    move/from16 v0, p35

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isUpsell:Z

    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->liveEventUri:Landroid/net/Uri;

    move/from16 v0, p37

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->is3d:Z

    invoke-static/range {p38 .. p38}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_2
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->ownerDisplayName:Ljava/lang/String;

    move/from16 v0, p39

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isInOfflineStore:Z

    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->offlineChannelAvatarThumbnailUrl:Ljava/lang/String;

    return-void

    :cond_0
    invoke-static/range {p33 .. p33}, Lcom/google/android/apps/youtube/common/e/m;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    move-object/from16 p18, p38

    goto :goto_2
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/util/Set;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;IJJJLjava/lang/String;Landroid/net/Uri;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/youtube/datalib/model/gdata/Video$State;ZLjava/util/Set;ZZLandroid/net/Uri;ZLjava/lang/String;ZLjava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/c;)V
    .locals 0

    invoke-direct/range {p0 .. p40}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;-><init>(Ljava/lang/String;Ljava/util/Set;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;IJJJLjava/lang/String;Landroid/net/Uri;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/youtube/datalib/model/gdata/Video$State;ZLjava/util/Set;ZZLandroid/net/Uri;ZLjava/lang/String;ZLjava/lang/String;)V

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "builder required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->buildUpon()Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final buildUpon()Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->id(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    new-instance v1, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->streams:Ljava/util/Set;

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->streams(Ljava/util/Set;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->watchUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->watchUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->defaultThumbnailUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->defaultThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->mqThumbnailUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->mqThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->hqThumbnailUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->hqThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->sdThumbnailUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->sdThumbnailUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->editUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->editUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->captionTracksUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->captionTracksUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->title(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->duration:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->duration(I)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->viewCount:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->viewCount(J)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->likesCount:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->likesCount(J)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->dislikesCount:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->dislikesCount(J)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->owner:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->owner(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->ownerUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->ownerUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->uploadedDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->uploadedDate(Ljava/util/Date;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->publishedDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->publishedDate(Ljava/util/Date;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->categoryTerm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->categoryTerm(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->categoryLabel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->categoryLabel(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->tags:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->tags(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->description(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->privacy:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->privacy(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Privacy;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->accessControl:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->accessControl(Ljava/util/Map;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->location:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->location(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->where:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->where(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->adultContent:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->adultContent(Z)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->state(Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->monetize:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->monetize(Z)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->monetizeExceptionCountries:Ljava/util/Set;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->monetizeExceptionCountries(Ljava/util/Set;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->paidContent:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->paidContent(Z)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isUpsell:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->isUpsell(Z)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->liveEventUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->liveEventUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->is3d:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->is3d(Z)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->ownerDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->ownerDisplayName(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isInOfflineStore:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->isInOfflineStore(Z)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->offlineChannelAvatarThumbnailUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;->offlineChannelAvatarThumbnailUrl(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/model/gdata/Video$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final couldBeMusicVideo()Z
    .locals 2

    const-string v0, "Music"

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->categoryTerm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final getDefaultSubtitleLanguageCode()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->tags:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->DEFAULT_LANG_REGEX:Ljava/util/regex/Pattern;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->tags:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final isActionable()Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isInOfflineStore:Z

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/gdata/c;->a:[I

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->isLive()Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final isLive()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->liveEventUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->state:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    sget-object v1, Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;->PLAYABLE:Lcom/google/android/apps/youtube/datalib/model/gdata/Video$State;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isMonetized(Ljava/lang/String;)Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->monetizeExceptionCountries:Ljava/util/Set;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->monetize:Z

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->monetizeExceptionCountries:Ljava/util/Set;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->monetize:Z

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Video[id = \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', title=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/model/gdata/Video;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\']"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
