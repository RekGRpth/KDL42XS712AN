.class public Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/model/u;


# instance fields
.field protected final a:Lcom/google/a/a/a/a/kz;

.field protected b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

.field protected final c:Ljava/lang/String;

.field protected final d:Ljava/lang/String;

.field protected final e:I

.field protected f:Z

.field private final g:Lcom/google/android/apps/youtube/datalib/innertube/model/a/d;


# direct methods
.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->a:Lcom/google/a/a/a/a/kz;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->c:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->d:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->e:I

    iput-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/a/d;

    return-void
.end method

.method public constructor <init>(Lcom/google/a/a/a/a/gc;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;-><init>(Lcom/google/a/a/a/a/gc;Lcom/google/android/apps/youtube/datalib/innertube/model/a/d;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/a/a/a/a/gc;Lcom/google/android/apps/youtube/datalib/innertube/model/a/d;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/gc;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/a/a/a/a/gc;->b:Lcom/google/a/a/a/a/kz;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->a:Lcom/google/a/a/a/a/kz;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    iget-object v1, p1, Lcom/google/a/a/a/a/gc;->d:Lcom/google/a/a/a/a/sx;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;-><init>(Lcom/google/a/a/a/a/sx;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    iget-object v0, p1, Lcom/google/a/a/a/a/gc;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->c:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/a/a/a/a/gc;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->d:Ljava/lang/String;

    iget v0, p1, Lcom/google/a/a/a/a/gc;->e:I

    iput v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->e:I

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/a/d;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/a/a/a/a/kz;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->a:Lcom/google/a/a/a/a/kz;

    iget-object v1, v1, Lcom/google/a/a/a/a/kz;->i:Lcom/google/a/a/a/a/wb;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->a:Lcom/google/a/a/a/a/kz;

    iget-object v1, v1, Lcom/google/a/a/a/a/kz;->u:Lcom/google/a/a/a/a/wl;

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->a:Lcom/google/a/a/a/a/kz;

    invoke-static {v1, p1, v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/a;->a(Lcom/google/a/a/a/a/kz;Lcom/google/a/a/a/a/kz;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->f:Z

    return-void
.end method

.method public final d()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    return-object v0
.end method

.method public final e()Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->a:Lcom/google/a/a/a/a/kz;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->f:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/a/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/a/c;->g:Lcom/google/android/apps/youtube/datalib/innertube/model/a/d;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/a/d;->c()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
