.class public final enum Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

.field public static final enum ACTIVE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

.field public static final enum CANNOT_OFFLINE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

.field public static final enum COMPLETE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

.field public static final enum DELETED:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

.field public static final enum DISK_READ_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

.field public static final enum DISK_WRITE_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

.field public static final enum FAILED_UNKNOWN:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

.field public static final enum NETWORK_READ_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

.field public static final enum PAUSED:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

.field static final lookup:Landroid/util/SparseArray;


# instance fields
.field private final fatal:Z

.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    const-string v2, "DELETED"

    const/4 v3, -0x1

    invoke-direct {v1, v2, v0, v3, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->DELETED:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    const-string v2, "COMPLETE"

    invoke-direct {v1, v2, v5, v0, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->COMPLETE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    const-string v2, "ACTIVE"

    invoke-direct {v1, v2, v6, v5, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->ACTIVE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    const-string v2, "FAILED_UNKNOWN"

    invoke-direct {v1, v2, v7, v6, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->FAILED_UNKNOWN:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    const-string v2, "DISK_WRITE_ERROR"

    invoke-direct {v1, v2, v8, v7, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->DISK_WRITE_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    const-string v2, "DISK_READ_ERROR"

    const/4 v3, 0x5

    invoke-direct {v1, v2, v3, v8, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->DISK_READ_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    const-string v2, "NETWORK_READ_ERROR"

    const/4 v3, 0x6

    const/4 v4, 0x5

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->NETWORK_READ_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    const-string v2, "CANNOT_OFFLINE"

    const/4 v3, 0x7

    const/4 v4, 0x6

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->CANNOT_OFFLINE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    const-string v2, "PAUSED"

    const/16 v3, 0x8

    const/4 v4, 0x7

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;-><init>(Ljava/lang/String;IIZ)V

    sput-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->PAUSED:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    const/16 v1, 0x9

    new-array v1, v1, [Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->DELETED:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    aput-object v2, v1, v0

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->COMPLETE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    aput-object v2, v1, v5

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->ACTIVE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    aput-object v2, v1, v6

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->FAILED_UNKNOWN:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    aput-object v2, v1, v7

    sget-object v2, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->DISK_WRITE_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->DISK_READ_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->NETWORK_READ_ERROR:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->CANNOT_OFFLINE:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->PAUSED:Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    aput-object v3, v1, v2

    sput-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->$VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    sput-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->lookup:Landroid/util/SparseArray;

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->values()[Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    sget-object v4, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->lookup:Landroid/util/SparseArray;

    iget v5, v3, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->value:I

    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->value:I

    iput-boolean p4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->fatal:Z

    return-void
.end method

.method public static fromValue(I)Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->lookup:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->$VALUES:[Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    invoke-virtual {v0}, [Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;

    return-object v0
.end method


# virtual methods
.method public final isFatal()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->fatal:Z

    return v0
.end method

.method public final value()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/OfflineMediaStatus;->value:I

    return v0
.end method
