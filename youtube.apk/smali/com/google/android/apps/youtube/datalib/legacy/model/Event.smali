.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/Event;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

.field public final displayUsername:Ljava/lang/String;

.field public final groupId:Ljava/lang/String;

.field public final subject:Ljava/lang/String;

.field public final subjectUri:Landroid/net/Uri;

.field public final target:Ljava/lang/String;

.field public targetVideo:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

.field public final when:Ljava/util/Date;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;Ljava/lang/String;Lcom/google/android/apps/youtube/datalib/model/gdata/Video;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->subject:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->subjectUri:Landroid/net/Uri;

    iput-object p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    iput-object p4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->target:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->targetVideo:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    iput-object p7, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->when:Ljava/util/Date;

    if-eqz p3, :cond_0

    iget-boolean v0, p3, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->targetsVideo:Z

    if-nez v0, :cond_0

    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->displayUsername:Ljava/lang/String;

    :goto_0
    iput-object p8, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->groupId:Ljava/lang/String;

    return-void

    :cond_0
    iput-object p6, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->displayUsername:Ljava/lang/String;

    goto :goto_0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "builder required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->subject:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->subject(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->subjectUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->subjectUri(Landroid/net/Uri;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->action(Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->target:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->target(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->targetVideo:Lcom/google/android/apps/youtube/datalib/model/gdata/Video;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->targetVideo(Lcom/google/android/apps/youtube/datalib/model/gdata/Video;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->displayUsername:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->displayUsername(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->when:Ljava/util/Date;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->when(Ljava/util/Date;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->groupId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;->groupId(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final targetIsChannel()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->targetsVideo:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final targetIsVideo()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    iget-boolean v0, v0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->targetsVideo:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->subject:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->action:Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/Event$Action;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/Event;->target:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
