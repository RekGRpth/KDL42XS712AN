.class public abstract Lcom/google/android/apps/youtube/datalib/innertube/model/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/datalib/innertube/model/u;


# instance fields
.field protected final a:Lcom/google/a/a/a/a/ff;

.field private b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

.field private c:Lcom/google/a/a/a/a/kz;

.field private d:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lcom/google/a/a/a/a/ff;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/a/a/a/a/ff;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/i;->a:Lcom/google/a/a/a/a/ff;

    iget-object v0, p1, Lcom/google/a/a/a/a/ff;->j:Lcom/google/a/a/a/a/fg;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/model/ap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/i;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/i;->a:Lcom/google/a/a/a/a/ff;

    iget-object v1, v1, Lcom/google/a/a/a/a/ff;->b:Lcom/google/a/a/a/a/sx;

    invoke-direct {v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/ap;-><init>(Lcom/google/a/a/a/a/sx;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/i;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/i;->b:Lcom/google/android/apps/youtube/datalib/innertube/model/ap;

    return-object v0
.end method

.method public final b()Lcom/google/a/a/a/a/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/i;->c:Lcom/google/a/a/a/a/kz;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/i;->a:Lcom/google/a/a/a/a/ff;

    iget-object v0, v0, Lcom/google/a/a/a/a/ff;->c:Lcom/google/a/a/a/a/kz;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/i;->c:Lcom/google/a/a/a/a/kz;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/i;->c:Lcom/google/a/a/a/a/kz;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/i;->d:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/i;->a:Lcom/google/a/a/a/a/ff;

    iget-object v0, v0, Lcom/google/a/a/a/a/ff;->d:Lcom/google/a/a/a/a/fk;

    invoke-static {v0}, Lcom/google/android/apps/youtube/datalib/innertube/a/b;->a(Lcom/google/a/a/a/a/fk;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/i;->d:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/i;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public abstract d()Lcom/google/android/apps/youtube/datalib/innertube/model/u;
.end method

.method public final e()Lcom/google/a/a/a/a/kz;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/innertube/model/i;->d()Lcom/google/android/apps/youtube/datalib/innertube/model/u;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/datalib/innertube/model/u;->e()Lcom/google/a/a/a/a/kz;

    move-result-object v0

    return-object v0
.end method
