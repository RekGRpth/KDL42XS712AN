.class public final Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/google/android/apps/youtube/datalib/legacy/a/a;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final adBreakId:Ljava/lang/String;

.field private final ads:Ljava/util/List;

.field private final breakEndPingUris:Ljava/util/List;

.field private final breakStartPingUris:Ljava/util/List;

.field private final breakType:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

.field private final errorPingUris:Ljava/util/List;

.field private final isDisplayAdAllowed:Z

.field private final isForOffline:Z

.field private final isLinearAdAllowed:Z

.field private final isNonlinearAdAllowed:Z

.field private final offsetType:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

.field private final offsetValue:I

.field private final originalVideoId:Ljava/lang/String;

.field private final trackingDecoration:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/bh;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bh;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 14

    invoke-static {}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;->values()[Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v1, v0, v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v4, 0x1

    if-ne v0, v4, :cond_1

    const/4 v4, 0x1

    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v5, 0x1

    if-ne v0, v5, :cond_2

    const/4 v5, 0x1

    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->readAdBreakList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v8

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v9

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v10

    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->readUriList(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v11

    const-class v0, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v13, 0x1

    if-ne v0, v13, :cond_3

    const/4 v13, 0x1

    :goto_3
    move-object v0, p0

    invoke-direct/range {v0 .. v13}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;IZZZLjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;Z)V

    return-void

    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    :cond_2
    const/4 v5, 0x0

    goto :goto_2

    :cond_3
    const/4 v13, 0x0

    goto :goto_3
.end method

.method private constructor <init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;IZZZLjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->offsetType:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    invoke-static {p9}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->breakStartPingUris:Ljava/util/List;

    invoke-static {p10}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->breakEndPingUris:Ljava/util/List;

    invoke-static {p11}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->errorPingUris:Ljava/util/List;

    iput-object p12, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->trackingDecoration:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    iput-boolean p3, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isLinearAdAllowed:Z

    iput-boolean p4, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isNonlinearAdAllowed:Z

    iput-boolean p5, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isDisplayAdAllowed:Z

    const-string v0, "adBreakId must not be empty"

    invoke-static {p6, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->adBreakId:Ljava/lang/String;

    const-string v0, "originalVideoId must not be null"

    invoke-static {p7, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->originalVideoId:Ljava/lang/String;

    invoke-static {p8}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->ads:Ljava/util/List;

    iput-boolean p13, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isForOffline:Z

    if-ltz p2, :cond_3

    const/4 v0, 0x1

    :goto_0
    const-string v1, "offsetValue must be >= 0"

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(ZLjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;->PRE_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;->POST_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    if-ne p1, v0, :cond_4

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iput v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->offsetValue:I

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;->PRE_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    if-eq p1, v0, :cond_2

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;->TIME:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    if-ne p1, v0, :cond_1

    if-eqz p2, :cond_2

    :cond_1
    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;->PERCENTAGE:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    if-ne p1, v0, :cond_5

    const/4 v0, 0x1

    move v1, v0

    :goto_2
    if-nez p2, :cond_6

    const/4 v0, 0x1

    :goto_3
    and-int/2addr v0, v1

    if-eqz v0, :cond_7

    :cond_2
    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;->PRE_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->breakType:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    :goto_4
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    move v0, p2

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    move v1, v0

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    :cond_7
    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;->POST_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    if-eq p1, v0, :cond_8

    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;->PERCENTAGE:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    if-ne p1, v0, :cond_9

    const/4 v0, 0x1

    move v1, v0

    :goto_5
    const/16 v0, 0x64

    if-ne p2, v0, :cond_a

    const/4 v0, 0x1

    :goto_6
    and-int/2addr v0, v1

    if-eqz v0, :cond_b

    :cond_8
    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;->POST_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->breakType:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    goto :goto_4

    :cond_9
    const/4 v0, 0x0

    move v1, v0

    goto :goto_5

    :cond_a
    const/4 v0, 0x0

    goto :goto_6

    :cond_b
    sget-object v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;->MID_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->breakType:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    goto :goto_4
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;IZZZLjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;ZLcom/google/android/apps/youtube/datalib/legacy/model/bh;)V
    .locals 0

    invoke-direct/range {p0 .. p13}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;IZZZLjava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;Z)V

    return-void
.end method

.method public static firstPrerollAd(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;
    .locals 2

    invoke-static {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->firstPrerollAdBreak(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAds()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;

    goto :goto_0
.end method

.method public static firstPrerollAdBreak(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;
    .locals 5

    const/4 v1, 0x0

    if-nez p0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getBreakType()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;->PRE_ROLL:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    if-ne v3, v4, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAds()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAds()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private static readAdBreakList(Landroid/os/Parcel;)Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static readUriList(Landroid/os/Parcel;)Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p0, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final buildUpon()Lcom/google/android/apps/youtube/datalib/legacy/model/bj;
    .locals 2

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOffsetType()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOffsetValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a(I)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isLinearAdAllowed()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isNonlinearAdAllowed()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->b(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isDisplayAdAllowed()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->c(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAdBreakId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAds()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a(Ljava/util/List;)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isForOffline()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->d(Z)Lcom/google/android/apps/youtube/datalib/legacy/model/bj;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getBreakStartPingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->a(Lcom/google/android/apps/youtube/datalib/legacy/model/bj;Ljava/util/List;)Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getBreakEndPingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->b(Lcom/google/android/apps/youtube/datalib/legacy/model/bj;Ljava/util/List;)Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getErrorPingUris()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/datalib/legacy/model/bj;->c(Lcom/google/android/apps/youtube/datalib/legacy/model/bj;Ljava/util/List;)Ljava/util/List;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOffsetType()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOffsetType()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    move-result-object v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOffsetValue()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOffsetValue()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isLinearAdAllowed()Z

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isLinearAdAllowed()Z

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isNonlinearAdAllowed()Z

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isNonlinearAdAllowed()Z

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isDisplayAdAllowed()Z

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isDisplayAdAllowed()Z

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAdBreakId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAdBreakId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAds()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAds()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getBreakStartPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getBreakStartPingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getBreakEndPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getBreakEndPingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getErrorPingUris()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getErrorPingUris()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getTrackingDecoration()Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getTrackingDecoration()Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/fromguava/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isForOffline()Z

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isForOffline()Z

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public final getAdBreakId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->adBreakId:Ljava/lang/String;

    return-object v0
.end method

.method public final getAds()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->ads:Ljava/util/List;

    return-object v0
.end method

.method public final getBreakEndPingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->breakEndPingUris:Ljava/util/List;

    return-object v0
.end method

.method public final getBreakStartPingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->breakStartPingUris:Ljava/util/List;

    return-object v0
.end method

.method public final getBreakType()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->breakType:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$BreakType;

    return-object v0
.end method

.method public final bridge synthetic getConverter()Lcom/google/android/apps/youtube/datalib/legacy/a/b;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getConverter()Lcom/google/android/apps/youtube/datalib/legacy/model/bk;

    move-result-object v0

    return-object v0
.end method

.method public final getConverter()Lcom/google/android/apps/youtube/datalib/legacy/model/bk;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/bk;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;)V

    return-object v0
.end method

.method public final getErrorPingUris()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->errorPingUris:Ljava/util/List;

    return-object v0
.end method

.method public final getOffsetType()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->offsetType:Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    return-object v0
.end method

.method public final getOffsetValue()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->offsetValue:I

    return v0
.end method

.method public final getOriginalVideoId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->originalVideoId:Ljava/lang/String;

    return-object v0
.end method

.method public final getTrackingDecoration()Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->trackingDecoration:Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    return-object v0
.end method

.method public final isDisplayAdAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isDisplayAdAllowed:Z

    return v0
.end method

.method public final isForOffline()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isForOffline:Z

    return v0
.end method

.method public final isLinearAdAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isLinearAdAllowed:Z

    return v0
.end method

.method public final isNonlinearAdAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isNonlinearAdAllowed:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    const-string v1, "VastAdBreak: [id=%s, offsetType=%s, offset:%s, allow:[l:%s , nl:%s, da:%s] ads: %s]"

    const/4 v0, 0x7

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAdBreakId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOffsetType()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOffsetValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isLinearAdAllowed()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isNonlinearAdAllowed()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isDisplayAdAllowed()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAds()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAds()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "none"

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOffsetType()Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak$OffsetType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOffsetValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isLinearAdAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isNonlinearAdAllowed()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isDisplayAdAllowed()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAdBreakId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getOriginalVideoId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getAds()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getBreakStartPingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getBreakEndPingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getErrorPingUris()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->getTrackingDecoration()Lcom/google/android/apps/youtube/datalib/legacy/model/TrackingPingAuthenticationSettings;

    move-result-object v0

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/datalib/legacy/model/VmapAdBreak;->isForOffline()Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method
