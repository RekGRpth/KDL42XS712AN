.class public abstract Lcom/google/android/apps/youtube/datalib/innertube/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/apps/youtube/datalib/innertube/r;

.field protected final b:Lcom/google/android/apps/youtube/datalib/innertube/p;

.field protected final c:Lcom/android/volley/l;


# direct methods
.method protected constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/a;->a:Lcom/google/android/apps/youtube/datalib/innertube/r;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/a;->b:Lcom/google/android/apps/youtube/datalib/innertube/p;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/a;->c:Lcom/android/volley/l;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/r;Lcom/google/android/apps/youtube/datalib/innertube/p;Lcom/android/volley/l;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/r;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/a;->a:Lcom/google/android/apps/youtube/datalib/innertube/r;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/datalib/innertube/p;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/a;->b:Lcom/google/android/apps/youtube/datalib/innertube/p;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/volley/l;

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/a;->c:Lcom/android/volley/l;

    return-void
.end method
