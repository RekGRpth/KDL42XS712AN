.class public final Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/a/a/a/a/fj;

.field private b:Landroid/net/Uri$Builder;

.field private c:Ljava/lang/String;

.field private final d:J


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;-><init>(J)V

    return-void
.end method

.method private constructor <init>(J)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/a/a/a/a/fj;

    invoke-direct {v0}, Lcom/google/a/a/a/a/fj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;->a:Lcom/google/a/a/a/a/fj;

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;->b:Landroid/net/Uri$Builder;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;->c:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;->d:J

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->getFormatStreamProto()Lcom/google/a/a/a/a/fj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;->a:Lcom/google/a/a/a/a/fj;

    # getter for: Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->uri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->access$100(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;->b:Landroid/net/Uri$Builder;

    # getter for: Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->access$200(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;->c:Ljava/lang/String;

    # getter for: Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->videoDurationMillis:J
    invoke-static {p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;->access$300(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;->d:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;->a:Lcom/google/a/a/a/a/fj;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;->b:Landroid/net/Uri$Builder;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/a/a/a/a/fj;->c:Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;

    iget-object v1, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;->a:Lcom/google/a/a/a/a/fj;

    iget-object v2, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;->c:Ljava/lang/String;

    iget-wide v3, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;->d:J

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/FormatStream;-><init>(Lcom/google/a/a/a/a/fj;Ljava/lang/String;J)V

    return-object v0
.end method

.method public final a(J)Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/datalib/innertube/model/media/b;->a:Lcom/google/a/a/a/a/fj;

    iput-wide p1, v0, Lcom/google/a/a/a/a/fj;->m:J

    return-object p0
.end method
