.class public final Lcom/google/android/apps/youtube/common/a/a;
.super Lcom/google/android/apps/youtube/common/a/f;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 1

    invoke-direct {p0, p2}, Lcom/google/android/apps/youtube/common/a/f;-><init>(Lcom/google/android/apps/youtube/common/a/b;)V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/a/a;->a:Landroid/app/Activity;

    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/a;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/common/a/a;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/youtube/common/a/a;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/common/a/b;)V

    return-object v0
.end method


# virtual methods
.method protected final a(Ljava/lang/Runnable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/a/a;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method
