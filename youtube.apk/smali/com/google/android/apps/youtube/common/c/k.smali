.class public abstract Lcom/google/android/apps/youtube/common/c/k;
.super Lcom/google/android/apps/youtube/common/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/youtube/common/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/youtube/common/c/k;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "TimestampedEvent not yet posted"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/youtube/common/c/k;->a:J

    return-wide v0
.end method

.method protected final a(J)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/youtube/common/a;->a(J)V

    return-void
.end method
