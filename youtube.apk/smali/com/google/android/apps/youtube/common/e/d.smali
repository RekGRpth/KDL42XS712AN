.class public final Lcom/google/android/apps/youtube/common/e/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:J

.field private final b:J

.field private final c:J

.field private final d:Ljava/util/Random;

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>()V
    .locals 7

    const-wide/16 v1, 0x3e8

    const-wide/16 v3, 0x7530

    const-wide/16 v5, 0x5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/common/e/d;-><init>(JJJ)V

    return-void
.end method

.method private constructor <init>(JJJ)V
    .locals 5

    const-wide/16 v3, 0x5

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/e/d;->d:Ljava/util/Random;

    iput-wide v1, p0, Lcom/google/android/apps/youtube/common/e/d;->e:J

    iput-wide v1, p0, Lcom/google/android/apps/youtube/common/e/d;->f:J

    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lcom/google/android/apps/youtube/common/e/d;->a:J

    const-wide/16 v0, 0x7530

    iput-wide v0, p0, Lcom/google/android/apps/youtube/common/e/d;->b:J

    const-wide/16 v0, 0x1

    cmp-long v0, v3, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Z)V

    iput-wide v3, p0, Lcom/google/android/apps/youtube/common/e/d;->c:J

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 7

    const/4 v2, 0x0

    iget-wide v0, p0, Lcom/google/android/apps/youtube/common/e/d;->e:J

    iget-wide v3, p0, Lcom/google/android/apps/youtube/common/e/d;->c:J

    cmp-long v0, v0, v3

    if-ltz v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iget-object v3, p0, Lcom/google/android/apps/youtube/common/e/d;->d:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextDouble()D

    move-result-wide v3

    add-double/2addr v0, v3

    iget-wide v3, p0, Lcom/google/android/apps/youtube/common/e/d;->a:J

    long-to-double v3, v3

    mul-double/2addr v0, v3

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    iget-wide v5, p0, Lcom/google/android/apps/youtube/common/e/d;->e:J

    long-to-double v5, v5

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    mul-double/2addr v0, v3

    double-to-long v0, v0

    iget-wide v3, p0, Lcom/google/android/apps/youtube/common/e/d;->b:J

    cmp-long v3, v0, v3

    if-lez v3, :cond_1

    iget-wide v0, p0, Lcom/google/android/apps/youtube/common/e/d;->b:J

    :cond_1
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sleeping thread for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/youtube/common/L;->d(Ljava/lang/String;)V

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    iget-wide v3, p0, Lcom/google/android/apps/youtube/common/e/d;->e:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, p0, Lcom/google/android/apps/youtube/common/e/d;->e:J

    iput-wide v0, p0, Lcom/google/android/apps/youtube/common/e/d;->f:J
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Thread interrupted"

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v2

    goto :goto_0
.end method
