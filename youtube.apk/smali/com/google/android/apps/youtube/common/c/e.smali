.class public final Lcom/google/android/apps/youtube/common/c/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;

.field private final b:Ljava/lang/Class;

.field private final c:Lcom/google/android/apps/youtube/common/c/d;

.field private final d:I

.field private final e:I


# direct methods
.method constructor <init>(Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;Lcom/google/android/apps/youtube/common/c/d;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/c/e;->a:Ljava/lang/ref/WeakReference;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/c/e;->b:Ljava/lang/Class;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/d;

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/c/e;->c:Lcom/google/android/apps/youtube/common/c/d;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/youtube/common/c/e;->b:Ljava/lang/Class;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/youtube/common/c/e;->c:Lcom/google/android/apps/youtube/common/c/d;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/common/c/e;->d:I

    invoke-virtual {p3}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/youtube/common/c/e;->e:I

    return-void
.end method


# virtual methods
.method final a()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/c/e;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method final b()Ljava/lang/Class;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/c/e;->b:Ljava/lang/Class;

    return-object v0
.end method

.method final c()Lcom/google/android/apps/youtube/common/c/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/c/e;->c:Lcom/google/android/apps/youtube/common/c/d;

    return-object v0
.end method

.method public final d()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/common/c/e;->e:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/youtube/common/c/e;

    if-eqz v2, :cond_3

    check-cast p1, Lcom/google/android/apps/youtube/common/c/e;

    iget-object v2, p0, Lcom/google/android/apps/youtube/common/c/e;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/apps/youtube/common/c/e;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/common/c/e;->b:Ljava/lang/Class;

    iget-object v3, p1, Lcom/google/android/apps/youtube/common/c/e;->b:Ljava/lang/Class;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/youtube/common/c/e;->e:I

    iget v3, p1, Lcom/google/android/apps/youtube/common/c/e;->e:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/youtube/common/c/e;->c:Lcom/google/android/apps/youtube/common/c/d;

    iget-object v3, p1, Lcom/google/android/apps/youtube/common/c/e;->c:Lcom/google/android/apps/youtube/common/c/d;

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/common/c/e;->d:I

    return v0
.end method
