.class public final Lcom/google/android/apps/youtube/common/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;
.implements Ljava/util/concurrent/Future;


# instance fields
.field private final a:Landroid/os/ConditionVariable;

.field private volatile b:Z

.field private volatile c:Ljava/lang/Object;

.field private volatile d:Ljava/lang/Exception;


# direct methods
.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/a/c;->a:Landroid/os/ConditionVariable;

    return-void
.end method

.method public static a()Lcom/google/android/apps/youtube/common/a/c;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/common/a/c;

    invoke-direct {v0}, Lcom/google/android/apps/youtube/common/a/c;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/a/c;->c:Ljava/lang/Object;

    iput-object p2, p0, Lcom/google/android/apps/youtube/common/a/c;->d:Ljava/lang/Exception;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/common/a/c;->b:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/a/c;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    iput-object p2, p0, Lcom/google/android/apps/youtube/common/a/c;->c:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/a/c;->d:Ljava/lang/Exception;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/common/a/c;->b:Z

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/a/c;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    return-void
.end method

.method public final cancel(Z)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final get()Ljava/lang/Object;
    .locals 2

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/a/c;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/a/c;->d:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/concurrent/ExecutionException;

    iget-object v1, p0, Lcom/google/android/apps/youtube/common/a/c;->d:Ljava/lang/Exception;

    invoke-direct {v0, v1}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/common/a/c;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public final get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 3

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->b()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/a/c;->a:Landroid/os/ConditionVariable;

    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/os/ConditionVariable;->block(J)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/a/c;->d:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/concurrent/ExecutionException;

    iget-object v1, p0, Lcom/google/android/apps/youtube/common/a/c;->d:Ljava/lang/Exception;

    invoke-direct {v0, v1}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/common/a/c;->c:Ljava/lang/Object;

    return-object v0

    :cond_1
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v0
.end method

.method public final isCancelled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final isDone()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/common/a/c;->b:Z

    return v0
.end method
