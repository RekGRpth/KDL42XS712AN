.class final Lcom/google/android/apps/youtube/common/c/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/Object;

.field final synthetic b:Ljava/lang/Object;

.field final synthetic c:Lcom/google/android/apps/youtube/common/c/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/youtube/common/c/a;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/common/c/b;->c:Lcom/google/android/apps/youtube/common/c/a;

    iput-object p2, p0, Lcom/google/android/apps/youtube/common/c/b;->a:Ljava/lang/Object;

    iput-object p3, p0, Lcom/google/android/apps/youtube/common/c/b;->b:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/c/b;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/a;)Ljava/util/concurrent/locks/ReadWriteLock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/youtube/common/c/b;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/c/a;->b(Lcom/google/android/apps/youtube/common/c/a;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/common/c/b;->a:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/e/c;->a(Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/common/c/b;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/a;)Ljava/util/concurrent/locks/ReadWriteLock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/c/e;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/c/e;->d()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/common/c/b;->b:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    if-eq v2, v3, :cond_1

    sget-object v3, Lcom/google/android/apps/youtube/common/c/a;->a:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->hashCode()I

    move-result v3

    if-ne v2, v3, :cond_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/c/e;->a()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/common/c/e;->c()Lcom/google/android/apps/youtube/common/c/d;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/common/c/b;->a:Ljava/lang/Object;

    invoke-interface {v0, v2}, Lcom/google/android/apps/youtube/common/c/d;->a(Ljava/lang/Object;)V

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "exception "

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/youtube/common/c/b;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/a;)Ljava/util/concurrent/locks/ReadWriteLock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/common/c/b;->c:Lcom/google/android/apps/youtube/common/c/a;

    invoke-static {v1}, Lcom/google/android/apps/youtube/common/c/a;->a(Lcom/google/android/apps/youtube/common/c/a;)Ljava/util/concurrent/locks/ReadWriteLock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/youtube/common/c/b;->c:Lcom/google/android/apps/youtube/common/c/a;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/apps/youtube/common/c/e;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/common/c/a;->a([Lcom/google/android/apps/youtube/common/c/e;)V

    goto :goto_1

    :cond_3
    return-void
.end method
