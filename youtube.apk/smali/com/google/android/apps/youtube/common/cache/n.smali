.class public final Lcom/google/android/apps/youtube/common/cache/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/cache/a;


# instance fields
.field private final a:Ljava/util/Map;

.field private final b:Lcom/google/android/apps/youtube/common/cache/a;

.field private final c:Lcom/google/android/apps/youtube/common/e/b;

.field private final d:J


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/common/e/b;J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/cache/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/cache/n;->b:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/e/b;

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/cache/n;->c:Lcom/google/android/apps/youtube/common/e/b;

    iput-wide p3, p0, Lcom/google/android/apps/youtube/common/cache/n;->d:J

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/cache/n;->a:Ljava/util/Map;

    return-void
.end method

.method public static a(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/common/e/b;J)Lcom/google/android/apps/youtube/common/cache/n;
    .locals 3

    new-instance v0, Lcom/google/android/apps/youtube/common/cache/n;

    const-wide/32 v1, 0x36ee80

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/google/android/apps/youtube/common/cache/n;-><init>(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/common/e/b;J)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/cache/n;->b:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/common/cache/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/common/cache/n;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/cache/n;->c:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v5

    sub-long v3, v5, v3

    iget-wide v5, p0, Lcom/google/android/apps/youtube/common/cache/n;->d:J

    cmp-long v0, v5, v3

    if-lez v0, :cond_1

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-ltz v0, :cond_1

    move-object v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/youtube/common/cache/n;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/cache/n;->b:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/cache/a;->a()V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/common/fromguava/d;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/cache/n;->b:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/common/cache/a;->a(Lcom/google/android/apps/youtube/common/fromguava/d;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/cache/n;->b:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/common/cache/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/cache/n;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/youtube/common/cache/n;->c:Lcom/google/android/apps/youtube/common/e/b;

    invoke-interface {v1}, Lcom/google/android/apps/youtube/common/e/b;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/cache/n;->b:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/common/cache/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/common/cache/n;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
