.class public final Lcom/google/android/apps/youtube/common/cache/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/cache/a;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/common/cache/a;

.field private final b:Lcom/google/android/apps/youtube/common/cache/a;

.field private final c:Lcom/google/android/apps/youtube/common/cache/m;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/common/cache/a;Lcom/google/android/apps/youtube/common/cache/m;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/cache/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/cache/l;->a:Lcom/google/android/apps/youtube/common/cache/a;

    const-string v0, "singleElementsCache may not be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/cache/a;

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/cache/l;->b:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/cache/m;

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/cache/l;->c:Lcom/google/android/apps/youtube/common/cache/m;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/cache/l;->a:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/common/cache/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/cache/l;->a:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/cache/a;->a()V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/common/fromguava/d;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/cache/l;->a:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/common/cache/a;->a(Lcom/google/android/apps/youtube/common/fromguava/d;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/cache/l;->c:Lcom/google/android/apps/youtube/common/cache/m;

    iget-object v1, p0, Lcom/google/android/apps/youtube/common/cache/l;->b:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-interface {v0, p1, p2, v1}, Lcom/google/android/apps/youtube/common/cache/m;->a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/apps/youtube/common/cache/a;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/cache/l;->a:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/common/cache/a;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/cache/l;->a:Lcom/google/android/apps/youtube/common/cache/a;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/common/cache/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
