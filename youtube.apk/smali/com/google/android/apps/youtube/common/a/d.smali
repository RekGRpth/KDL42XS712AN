.class public final Lcom/google/android/apps/youtube/common/a/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/common/a/b;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/common/a/b;

.field private volatile b:Z


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/common/a/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/youtube/common/a/d;->a:Lcom/google/android/apps/youtube/common/a/b;

    return-void
.end method

.method public static a(Lcom/google/android/apps/youtube/common/a/b;)Lcom/google/android/apps/youtube/common/a/d;
    .locals 1

    new-instance v0, Lcom/google/android/apps/youtube/common/a/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/youtube/common/a/d;-><init>(Lcom/google/android/apps/youtube/common/a/b;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/youtube/common/a/d;->b:Z

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/common/a/d;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/a/d;->a:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/common/a/d;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/a/d;->a:Lcom/google/android/apps/youtube/common/a/b;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/common/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/youtube/common/a/d;->b:Z

    return v0
.end method
