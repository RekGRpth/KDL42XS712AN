.class public Lcom/google/android/apps/youtube/common/d/g;
.super Lcom/google/android/apps/youtube/common/d/a;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/apps/youtube/common/network/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/youtube/common/d/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/common/d/g;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/common/network/h;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/common/d/a;-><init>(Lcom/google/android/apps/youtube/common/c/a;)V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/common/network/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/common/d/g;->b:Lcom/google/android/apps/youtube/common/network/h;

    invoke-virtual {p1, p0}, Lcom/google/android/apps/youtube/common/c/a;->a(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/youtube/common/d/g;->a:Ljava/lang/String;

    return-object v0
.end method

.method protected final c()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/common/d/g;->b:Lcom/google/android/apps/youtube/common/network/h;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/common/network/h;->a()Z

    move-result v0

    return v0
.end method

.method public handleConnectivityChangedEvent(Lcom/google/android/apps/youtube/common/network/a;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/youtube/common/c/j;
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/apps/youtube/common/network/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/youtube/common/d/g;->a()V

    :cond_0
    return-void
.end method
