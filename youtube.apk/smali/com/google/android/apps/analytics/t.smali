.class final Lcom/google/android/apps/analytics/t;
.super Landroid/os/HandlerThread;


# instance fields
.field volatile a:Landroid/os/Handler;

.field private final b:Lcom/google/android/apps/analytics/z;

.field private final c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private f:J

.field private g:Lcom/google/android/apps/analytics/u;

.field private final h:Lcom/google/android/apps/analytics/h;

.field private final i:Lcom/google/android/apps/analytics/v;

.field private final j:Lcom/google/android/apps/analytics/s;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/analytics/h;Lcom/google/android/apps/analytics/z;Ljava/lang/String;Lcom/google/android/apps/analytics/s;)V
    .locals 2

    const-string v0, "DispatcherThread"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    const/16 v0, 0x1e

    iput v0, p0, Lcom/google/android/apps/analytics/t;->e:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/analytics/t;->g:Lcom/google/android/apps/analytics/u;

    iput-object p1, p0, Lcom/google/android/apps/analytics/t;->h:Lcom/google/android/apps/analytics/h;

    iput-object p3, p0, Lcom/google/android/apps/analytics/t;->c:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/analytics/t;->b:Lcom/google/android/apps/analytics/z;

    new-instance v0, Lcom/google/android/apps/analytics/v;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/analytics/v;-><init>(Lcom/google/android/apps/analytics/t;B)V

    iput-object v0, p0, Lcom/google/android/apps/analytics/t;->i:Lcom/google/android/apps/analytics/v;

    iget-object v0, p0, Lcom/google/android/apps/analytics/t;->b:Lcom/google/android/apps/analytics/z;

    iget-object v1, p0, Lcom/google/android/apps/analytics/t;->i:Lcom/google/android/apps/analytics/v;

    iput-object v1, v0, Lcom/google/android/apps/analytics/z;->b:Lcom/google/android/apps/analytics/aa;

    iput-object p4, p0, Lcom/google/android/apps/analytics/t;->j:Lcom/google/android/apps/analytics/s;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/analytics/h;Ljava/lang/String;Lcom/google/android/apps/analytics/s;)V
    .locals 2

    new-instance v0, Lcom/google/android/apps/analytics/z;

    invoke-static {p3}, Lcom/google/android/apps/analytics/s;->a(Lcom/google/android/apps/analytics/s;)Lorg/apache/http/HttpHost;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/analytics/z;-><init>(Lorg/apache/http/HttpHost;)V

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/apps/analytics/t;-><init>(Lcom/google/android/apps/analytics/h;Lcom/google/android/apps/analytics/z;Ljava/lang/String;Lcom/google/android/apps/analytics/s;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/analytics/h;Ljava/lang/String;Lcom/google/android/apps/analytics/s;B)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/analytics/t;-><init>(Lcom/google/android/apps/analytics/h;Ljava/lang/String;Lcom/google/android/apps/analytics/s;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/analytics/t;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/analytics/t;->d:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/apps/analytics/t;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/analytics/t;->e:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/apps/analytics/t;J)J
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/analytics/t;->f:J

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/analytics/t;->f:J

    return-wide v0
.end method

.method static synthetic a(Lcom/google/android/apps/analytics/t;Lcom/google/android/apps/analytics/u;)Lcom/google/android/apps/analytics/u;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/analytics/t;->g:Lcom/google/android/apps/analytics/u;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/analytics/t;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/apps/analytics/t;->d:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/apps/analytics/t;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/analytics/t;->f:J

    return-wide v0
.end method

.method static synthetic b(Lcom/google/android/apps/analytics/t;J)J
    .locals 2

    const-wide/16 v0, 0x2

    iput-wide v0, p0, Lcom/google/android/apps/analytics/t;->f:J

    return-wide v0
.end method

.method static synthetic c(Lcom/google/android/apps/analytics/t;)Lcom/google/android/apps/analytics/s;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/t;->j:Lcom/google/android/apps/analytics/s;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/analytics/t;)Lcom/google/android/apps/analytics/z;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/t;->b:Lcom/google/android/apps/analytics/z;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/analytics/t;)Lcom/google/android/apps/analytics/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/t;->h:Lcom/google/android/apps/analytics/h;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/analytics/t;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/analytics/t;->e:I

    return v0
.end method

.method static synthetic g(Lcom/google/android/apps/analytics/t;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/t;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/apps/analytics/t;)Lcom/google/android/apps/analytics/v;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/t;->i:Lcom/google/android/apps/analytics/v;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/apps/analytics/t;)Lcom/google/android/apps/analytics/u;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/t;->g:Lcom/google/android/apps/analytics/u;

    return-object v0
.end method


# virtual methods
.method public final a([Lcom/google/android/apps/analytics/n;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/analytics/t;->a:Landroid/os/Handler;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/analytics/t;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/analytics/u;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/analytics/u;-><init>(Lcom/google/android/apps/analytics/t;[Lcom/google/android/apps/analytics/n;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected final onLooperPrepared()V
    .locals 1

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/analytics/t;->a:Landroid/os/Handler;

    return-void
.end method
