.class public final Lcom/google/android/apps/analytics/ac;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:D

.field private final d:D

.field private final e:D


# direct methods
.method private constructor <init>(Lcom/google/android/apps/analytics/ad;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/analytics/ad;->a(Lcom/google/android/apps/analytics/ad;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/analytics/ac;->a:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/analytics/ad;->b(Lcom/google/android/apps/analytics/ad;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/analytics/ac;->c:D

    invoke-static {p1}, Lcom/google/android/apps/analytics/ad;->c(Lcom/google/android/apps/analytics/ad;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/analytics/ac;->b:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/analytics/ad;->d(Lcom/google/android/apps/analytics/ad;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/analytics/ac;->d:D

    invoke-static {p1}, Lcom/google/android/apps/analytics/ad;->e(Lcom/google/android/apps/analytics/ad;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/analytics/ac;->e:D

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/analytics/ad;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/analytics/ac;-><init>(Lcom/google/android/apps/analytics/ad;)V

    return-void
.end method


# virtual methods
.method final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/ac;->a:Ljava/lang/String;

    return-object v0
.end method

.method final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/ac;->b:Ljava/lang/String;

    return-object v0
.end method

.method final c()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/analytics/ac;->c:D

    return-wide v0
.end method

.method final d()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/analytics/ac;->d:D

    return-wide v0
.end method

.method final e()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/analytics/ac;->e:D

    return-wide v0
.end method
