.class public final Lcom/google/android/apps/analytics/q;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:D

.field private final f:J


# direct methods
.method private constructor <init>(Lcom/google/android/apps/analytics/r;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/analytics/r;->a(Lcom/google/android/apps/analytics/r;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/analytics/q;->a:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/analytics/r;->b(Lcom/google/android/apps/analytics/r;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/analytics/q;->b:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/analytics/r;->c(Lcom/google/android/apps/analytics/r;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/analytics/q;->e:D

    invoke-static {p1}, Lcom/google/android/apps/analytics/r;->d(Lcom/google/android/apps/analytics/r;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/analytics/q;->f:J

    invoke-static {p1}, Lcom/google/android/apps/analytics/r;->e(Lcom/google/android/apps/analytics/r;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/analytics/q;->c:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/analytics/r;->f(Lcom/google/android/apps/analytics/r;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/analytics/q;->d:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/analytics/r;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/analytics/q;-><init>(Lcom/google/android/apps/analytics/r;)V

    return-void
.end method


# virtual methods
.method final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/q;->a:Ljava/lang/String;

    return-object v0
.end method

.method final b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/q;->b:Ljava/lang/String;

    return-object v0
.end method

.method final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/q;->c:Ljava/lang/String;

    return-object v0
.end method

.method final d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/q;->d:Ljava/lang/String;

    return-object v0
.end method

.method final e()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/analytics/q;->e:D

    return-wide v0
.end method

.method final f()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/analytics/q;->f:J

    return-wide v0
.end method
