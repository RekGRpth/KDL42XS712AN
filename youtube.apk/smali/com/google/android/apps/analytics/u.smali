.class final Lcom/google/android/apps/analytics/u;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/analytics/t;

.field private final b:Ljava/util/LinkedList;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/analytics/t;[Lcom/google/android/apps/analytics/n;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/analytics/u;->b:Ljava/util/LinkedList;

    iget-object v0, p0, Lcom/google/android/apps/analytics/u;->b:Ljava/util/LinkedList;

    invoke-static {v0, p2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/analytics/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/u;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/analytics/n;

    return-object v0
.end method

.method public final run()V
    .locals 13

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-static {v0, p0}, Lcom/google/android/apps/analytics/t;->a(Lcom/google/android/apps/analytics/t;Lcom/google/android/apps/analytics/u;)Lcom/google/android/apps/analytics/u;

    move v7, v2

    :goto_0
    const/4 v0, 0x5

    if-ge v7, v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/analytics/u;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_6

    const-wide/16 v0, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-static {v3}, Lcom/google/android/apps/analytics/t;->a(Lcom/google/android/apps/analytics/t;)I

    move-result v3

    const/16 v4, 0x1f4

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-static {v3}, Lcom/google/android/apps/analytics/t;->a(Lcom/google/android/apps/analytics/t;)I

    move-result v3

    const/16 v4, 0x1f7

    if-ne v3, v4, :cond_5

    :cond_0
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    iget-object v3, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-static {v3}, Lcom/google/android/apps/analytics/t;->b(Lcom/google/android/apps/analytics/t;)J

    move-result-wide v3

    long-to-double v3, v3

    mul-double/2addr v0, v3

    double-to-long v0, v0

    iget-object v3, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-static {v3}, Lcom/google/android/apps/analytics/t;->b(Lcom/google/android/apps/analytics/t;)J

    move-result-wide v3

    const-wide/16 v5, 0x100

    cmp-long v3, v3, v5

    if-gez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    const-wide/16 v4, 0x2

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/analytics/t;->a(Lcom/google/android/apps/analytics/t;J)J

    :cond_1
    :goto_1
    const-wide/16 v3, 0x3e8

    mul-long/2addr v0, v3

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    iget-object v0, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-static {v0}, Lcom/google/android/apps/analytics/t;->c(Lcom/google/android/apps/analytics/t;)Lcom/google/android/apps/analytics/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/analytics/s;->a()Z

    move-result v8

    invoke-static {}, Lcom/google/android/apps/analytics/j;->a()Lcom/google/android/apps/analytics/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/analytics/j;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v8, :cond_2

    const-string v0, "GoogleAnalyticsTracker"

    const-string v1, "dispatching hits in dry run mode"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/analytics/u;->b:Ljava/util/LinkedList;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/android/apps/analytics/n;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/analytics/n;

    move v6, v2

    :goto_2
    array-length v1, v0

    if-ge v6, v1, :cond_e

    iget-object v1, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-static {v1}, Lcom/google/android/apps/analytics/t;->f(Lcom/google/android/apps/analytics/t;)I

    move-result v1

    if-ge v6, v1, :cond_e

    aget-object v1, v0, v6

    iget-object v1, v1, Lcom/google/android/apps/analytics/n;->a:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v1, v3, v4}, Lcom/google/android/apps/analytics/ae;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v5

    const/16 v1, 0x3f

    invoke-virtual {v5, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-gez v3, :cond_7

    move-object v1, v5

    :cond_3
    const-string v3, ""

    move-object v4, v3

    move-object v3, v1

    :goto_3
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v9, 0x7f4

    if-ge v1, v9, :cond_9

    new-instance v1, Lorg/apache/http/message/BasicHttpEntityEnclosingRequest;

    const-string v3, "GET"

    invoke-direct {v1, v3, v5}, Lorg/apache/http/message/BasicHttpEntityEnclosingRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v1

    :goto_4
    iget-object v1, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-static {v1}, Lcom/google/android/apps/analytics/t;->c(Lcom/google/android/apps/analytics/t;)Lcom/google/android/apps/analytics/s;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/analytics/s;->a(Lcom/google/android/apps/analytics/s;)Lorg/apache/http/HttpHost;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-static {v5}, Lcom/google/android/apps/analytics/t;->c(Lcom/google/android/apps/analytics/t;)Lcom/google/android/apps/analytics/s;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/analytics/s;->a(Lcom/google/android/apps/analytics/s;)Lorg/apache/http/HttpHost;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/http/HttpHost;->getPort()I

    move-result v5

    const/16 v9, 0x50

    if-eq v5, v9, :cond_4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ":"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-static {v5}, Lcom/google/android/apps/analytics/t;->c(Lcom/google/android/apps/analytics/t;)Lcom/google/android/apps/analytics/s;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/analytics/s;->a(Lcom/google/android/apps/analytics/s;)Lorg/apache/http/HttpHost;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/http/HttpHost;->getPort()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_4
    const-string v5, "Host"

    invoke-interface {v3, v5, v1}, Lorg/apache/http/HttpEntityEnclosingRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "User-Agent"

    iget-object v5, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-static {v5}, Lcom/google/android/apps/analytics/t;->g(Lcom/google/android/apps/analytics/t;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v1, v5}, Lorg/apache/http/HttpEntityEnclosingRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/apps/analytics/j;->a()Lcom/google/android/apps/analytics/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/analytics/j;->d()Z

    move-result v1

    if-eqz v1, :cond_b

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    invoke-interface {v3}, Lorg/apache/http/HttpEntityEnclosingRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v9

    array-length v10, v9

    move v1, v2

    :goto_5
    if-ge v1, v10, :cond_a

    aget-object v11, v9, v1

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    const-wide/16 v4, 0x2

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/analytics/t;->b(Lcom/google/android/apps/analytics/t;J)J
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/http/HttpException; {:try_start_0 .. :try_end_0} :catch_2

    goto/16 :goto_1

    :catch_0
    move-exception v0

    const-string v1, "GoogleAnalyticsTracker"

    const-string v2, "Couldn\'t sleep."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_6
    :goto_6
    iget-object v0, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-static {v0}, Lcom/google/android/apps/analytics/t;->d(Lcom/google/android/apps/analytics/t;)Lcom/google/android/apps/analytics/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/analytics/z;->b()V

    iget-object v0, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-static {v0}, Lcom/google/android/apps/analytics/t;->e(Lcom/google/android/apps/analytics/t;)Lcom/google/android/apps/analytics/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/analytics/h;->a()V

    iget-object v0, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/analytics/t;->a(Lcom/google/android/apps/analytics/t;Lcom/google/android/apps/analytics/u;)Lcom/google/android/apps/analytics/u;

    return-void

    :cond_7
    if-lez v3, :cond_8

    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {v5, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :goto_7
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    if-ge v3, v4, :cond_3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v5, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    move-object v3, v1

    goto/16 :goto_3

    :cond_8
    const-string v1, ""

    goto :goto_7

    :cond_9
    new-instance v1, Lorg/apache/http/message/BasicHttpEntityEnclosingRequest;

    const-string v5, "POST"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "/p"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v5, v3}, Lorg/apache/http/message/BasicHttpEntityEnclosingRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "Content-Length"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v3, v5}, Lorg/apache/http/HttpEntityEnclosingRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "Content-Type"

    const-string v5, "text/plain"

    invoke-interface {v1, v3, v5}, Lorg/apache/http/HttpEntityEnclosingRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lorg/apache/http/entity/StringEntity;

    invoke-direct {v3, v4}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    invoke-interface {v1, v3}, Lorg/apache/http/HttpEntityEnclosingRequest;->setEntity(Lorg/apache/http/HttpEntity;)V

    move-object v3, v1

    goto/16 :goto_4

    :cond_a
    invoke-interface {v3}, Lorg/apache/http/HttpEntityEnclosingRequest;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v9, "\n"

    invoke-virtual {v1, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v1, "GoogleAnalyticsTracker"

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v4, 0x2000

    if-le v1, v4, :cond_c

    const-string v1, "GoogleAnalyticsTracker"

    const-string v3, "Hit too long (> 8192 bytes)--not sent"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-static {v1}, Lcom/google/android/apps/analytics/t;->h(Lcom/google/android/apps/analytics/t;)Lcom/google/android/apps/analytics/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/analytics/v;->a()V

    :goto_8
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto/16 :goto_2

    :cond_c
    if-eqz v8, :cond_d

    iget-object v1, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-static {v1}, Lcom/google/android/apps/analytics/t;->h(Lcom/google/android/apps/analytics/t;)Lcom/google/android/apps/analytics/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/analytics/v;->a()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/apache/http/HttpException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_8

    :catch_1
    move-exception v0

    const-string v1, "GoogleAnalyticsTracker"

    const-string v2, "Problem with socket or streams."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_6

    :cond_d
    :try_start_2
    iget-object v1, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-static {v1}, Lcom/google/android/apps/analytics/t;->d(Lcom/google/android/apps/analytics/t;)Lcom/google/android/apps/analytics/z;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/apps/analytics/z;->a(Lorg/apache/http/HttpEntityEnclosingRequest;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/apache/http/HttpException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_8

    :catch_2
    move-exception v0

    const-string v1, "GoogleAnalyticsTracker"

    const-string v2, "Problem with http streams."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_6

    :cond_e
    if-nez v8, :cond_f

    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/analytics/u;->a:Lcom/google/android/apps/analytics/t;

    invoke-static {v0}, Lcom/google/android/apps/analytics/t;->d(Lcom/google/android/apps/analytics/t;)Lcom/google/android/apps/analytics/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/analytics/z;->a()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lorg/apache/http/HttpException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_f
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto/16 :goto_0
.end method
