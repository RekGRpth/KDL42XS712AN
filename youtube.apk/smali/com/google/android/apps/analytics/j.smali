.class public final Lcom/google/android/apps/analytics/j;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/google/android/apps/analytics/j;


# instance fields
.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:I

.field private g:Ljava/lang/String;

.field private h:Landroid/content/Context;

.field private i:Landroid/net/ConnectivityManager;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:I

.field private n:Lcom/google/android/apps/analytics/p;

.field private o:Lcom/google/android/apps/analytics/g;

.field private p:Z

.field private q:Z

.field private r:Lcom/google/android/apps/analytics/a;

.field private s:Lcom/google/android/apps/analytics/w;

.field private t:Lcom/google/android/apps/analytics/e;

.field private u:Ljava/util/Map;

.field private v:Ljava/util/Map;

.field private w:Landroid/os/Handler;

.field private x:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/analytics/j;

    invoke-direct {v0}, Lcom/google/android/apps/analytics/j;-><init>()V

    sput-object v0, Lcom/google/android/apps/analytics/j;->a:Lcom/google/android/apps/analytics/j;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/apps/analytics/j;->b:Z

    iput-boolean v0, p0, Lcom/google/android/apps/analytics/j;->c:Z

    iput-boolean v0, p0, Lcom/google/android/apps/analytics/j;->d:Z

    iput-boolean v0, p0, Lcom/google/android/apps/analytics/j;->e:Z

    const/16 v0, 0x64

    iput v0, p0, Lcom/google/android/apps/analytics/j;->f:I

    const-string v0, "GoogleAnalytics"

    iput-object v0, p0, Lcom/google/android/apps/analytics/j;->j:Ljava/lang/String;

    const-string v0, "1.5.1"

    iput-object v0, p0, Lcom/google/android/apps/analytics/j;->k:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/analytics/j;->l:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/analytics/j;->u:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/analytics/j;->v:Ljava/util/Map;

    new-instance v0, Lcom/google/android/apps/analytics/k;

    invoke-direct {v0, p0}, Lcom/google/android/apps/analytics/k;-><init>(Lcom/google/android/apps/analytics/j;)V

    iput-object v0, p0, Lcom/google/android/apps/analytics/j;->x:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/analytics/j;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/j;->w:Landroid/os/Handler;

    return-object v0
.end method

.method public static a()Lcom/google/android/apps/analytics/j;
    .locals 1

    sget-object v0, Lcom/google/android/apps/analytics/j;->a:Lcom/google/android/apps/analytics/j;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/analytics/j;)Lcom/google/android/apps/analytics/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/analytics/j;->n:Lcom/google/android/apps/analytics/p;

    return-object v0
.end method

.method private e()V
    .locals 4

    iget v0, p0, Lcom/google/android/apps/analytics/j;->m:I

    if-gez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/analytics/j;->w:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/analytics/j;->x:Ljava/lang/Runnable;

    iget v2, p0, Lcom/google/android/apps/analytics/j;->m:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/analytics/j;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "GoogleAnalyticsTracker"

    const-string v1, "Scheduled next dispatch"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/analytics/j;->w:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/analytics/j;->w:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/analytics/j;->x:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILandroid/content/Context;)V
    .locals 6

    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/analytics/j;->n:Lcom/google/android/apps/analytics/p;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/analytics/x;

    invoke-direct {v0, v2}, Lcom/google/android/apps/analytics/x;-><init>(Landroid/content/Context;)V

    iget-boolean v1, p0, Lcom/google/android/apps/analytics/j;->d:Z

    invoke-interface {v0, v1}, Lcom/google/android/apps/analytics/p;->a(Z)V

    iget v1, p0, Lcom/google/android/apps/analytics/j;->f:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/analytics/p;->a(I)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/analytics/j;->o:Lcom/google/android/apps/analytics/g;

    if-nez v1, :cond_2

    new-instance v1, Lcom/google/android/apps/analytics/s;

    iget-object v3, p0, Lcom/google/android/apps/analytics/j;->j:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/analytics/j;->k:Ljava/lang/String;

    invoke-direct {v1, v3, v4}, Lcom/google/android/apps/analytics/s;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/google/android/apps/analytics/j;->c:Z

    invoke-interface {v1, v3}, Lcom/google/android/apps/analytics/g;->a(Z)V

    :goto_1
    new-instance v3, Lcom/google/android/apps/analytics/l;

    invoke-direct {v3, p0}, Lcom/google/android/apps/analytics/l;-><init>(Lcom/google/android/apps/analytics/j;)V

    iput-object p1, p0, Lcom/google/android/apps/analytics/j;->g:Ljava/lang/String;

    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/analytics/j;->n:Lcom/google/android/apps/analytics/p;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/analytics/j;->o:Lcom/google/android/apps/analytics/g;

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/analytics/j;->h:Landroid/content/Context;

    new-instance v4, Lcom/google/android/apps/analytics/f;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/apps/analytics/f;-><init>(Landroid/content/pm/PackageManager;)V

    iput-object v4, p0, Lcom/google/android/apps/analytics/j;->s:Lcom/google/android/apps/analytics/w;

    iput-object v0, p0, Lcom/google/android/apps/analytics/j;->n:Lcom/google/android/apps/analytics/p;

    new-instance v0, Lcom/google/android/apps/analytics/a;

    invoke-direct {v0}, Lcom/google/android/apps/analytics/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/analytics/j;->r:Lcom/google/android/apps/analytics/a;

    iget-object v0, p0, Lcom/google/android/apps/analytics/j;->n:Lcom/google/android/apps/analytics/p;

    invoke-interface {v0}, Lcom/google/android/apps/analytics/p;->c()V

    iput-object v1, p0, Lcom/google/android/apps/analytics/j;->o:Lcom/google/android/apps/analytics/g;

    iget-object v0, p0, Lcom/google/android/apps/analytics/j;->o:Lcom/google/android/apps/analytics/g;

    invoke-interface {v0, v3}, Lcom/google/android/apps/analytics/g;->a(Lcom/google/android/apps/analytics/h;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/analytics/j;->q:Z

    iget-object v0, p0, Lcom/google/android/apps/analytics/j;->i:Landroid/net/ConnectivityManager;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/analytics/j;->h:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/apps/analytics/j;->i:Landroid/net/ConnectivityManager;

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/analytics/j;->w:Landroid/os/Handler;

    if-nez v0, :cond_6

    new-instance v0, Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/analytics/j;->w:Landroid/os/Handler;

    :goto_2
    iget v0, p0, Lcom/google/android/apps/analytics/j;->m:I

    iput p2, p0, Lcom/google/android/apps/analytics/j;->m:I

    if-gtz v0, :cond_7

    invoke-direct {p0}, Lcom/google/android/apps/analytics/j;->e()V

    :cond_5
    :goto_3
    return-void

    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/analytics/j;->f()V

    goto :goto_2

    :cond_7
    if-lez v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/apps/analytics/j;->f()V

    invoke-direct {p0}, Lcom/google/android/apps/analytics/j;->e()V

    goto :goto_3
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/analytics/j;->j:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/analytics/j;->k:Ljava/lang/String;

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 9

    iget-object v0, p0, Lcom/google/android/apps/analytics/j;->s:Lcom/google/android/apps/analytics/w;

    invoke-interface {v0}, Lcom/google/android/apps/analytics/w;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "category cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    if-nez p2, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "action cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/analytics/j;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/analytics/j;->l:Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/analytics/i;

    iget-object v3, p0, Lcom/google/android/apps/analytics/j;->h:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v7, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v3, p0, Lcom/google/android/apps/analytics/j;->h:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v8, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/analytics/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    iget-object v1, p0, Lcom/google/android/apps/analytics/j;->t:Lcom/google/android/apps/analytics/e;

    iput-object v1, v0, Lcom/google/android/apps/analytics/i;->j:Lcom/google/android/apps/analytics/e;

    iget-object v1, p0, Lcom/google/android/apps/analytics/j;->r:Lcom/google/android/apps/analytics/a;

    invoke-virtual {v1}, Lcom/google/android/apps/analytics/a;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/analytics/i;->b(I)V

    iget-boolean v1, p0, Lcom/google/android/apps/analytics/j;->e:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/analytics/i;->b(Z)V

    new-instance v1, Lcom/google/android/apps/analytics/e;

    invoke-direct {v1}, Lcom/google/android/apps/analytics/e;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/analytics/j;->t:Lcom/google/android/apps/analytics/e;

    iget-object v1, p0, Lcom/google/android/apps/analytics/j;->n:Lcom/google/android/apps/analytics/p;

    invoke-interface {v1, v0}, Lcom/google/android/apps/analytics/p;->a(Lcom/google/android/apps/analytics/i;)V

    iget-boolean v0, p0, Lcom/google/android/apps/analytics/j;->p:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/analytics/j;->p:Z

    invoke-direct {p0}, Lcom/google/android/apps/analytics/j;->e()V

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;Ljava/lang/String;I)Z
    .locals 2

    :try_start_0
    new-instance v0, Lcom/google/android/apps/analytics/d;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/apps/analytics/d;-><init>(ILjava/lang/String;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/google/android/apps/analytics/j;->t:Lcom/google/android/apps/analytics/e;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/analytics/e;

    invoke-direct {v1}, Lcom/google/android/apps/analytics/e;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/analytics/j;->t:Lcom/google/android/apps/analytics/e;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/analytics/j;->t:Lcom/google/android/apps/analytics/e;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/analytics/e;->a(Lcom/google/android/apps/analytics/d;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/google/android/apps/analytics/j;->b:Z

    if-eqz v2, :cond_0

    const-string v2, "GoogleAnalyticsTracker"

    const-string v3, "Called dispatch"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/apps/analytics/j;->q:Z

    if-eqz v2, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/analytics/j;->b:Z

    if-eqz v1, :cond_1

    const-string v1, "GoogleAnalyticsTracker"

    const-string v2, "...but dispatcher was busy"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/analytics/j;->e()V

    :cond_2
    :goto_0
    return v0

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/analytics/j;->i:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    iget-boolean v1, p0, Lcom/google/android/apps/analytics/j;->b:Z

    if-eqz v1, :cond_5

    const-string v1, "GoogleAnalyticsTracker"

    const-string v2, "...but there was no network connected"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/analytics/j;->e()V

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/analytics/j;->n:Lcom/google/android/apps/analytics/p;

    invoke-interface {v2}, Lcom/google/android/apps/analytics/p;->b()I

    move-result v2

    if-eqz v2, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/analytics/j;->n:Lcom/google/android/apps/analytics/p;

    invoke-interface {v0}, Lcom/google/android/apps/analytics/p;->a()[Lcom/google/android/apps/analytics/n;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/analytics/j;->o:Lcom/google/android/apps/analytics/g;

    invoke-interface {v2, v0}, Lcom/google/android/apps/analytics/g;->a([Lcom/google/android/apps/analytics/n;)V

    iput-boolean v1, p0, Lcom/google/android/apps/analytics/j;->q:Z

    invoke-direct {p0}, Lcom/google/android/apps/analytics/j;->e()V

    iget-boolean v2, p0, Lcom/google/android/apps/analytics/j;->b:Z

    if-eqz v2, :cond_7

    const-string v2, "GoogleAnalyticsTracker"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sending "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v0, v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " hits to dispatcher"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    move v0, v1

    goto :goto_0

    :cond_8
    iput-boolean v1, p0, Lcom/google/android/apps/analytics/j;->p:Z

    iget-boolean v1, p0, Lcom/google/android/apps/analytics/j;->b:Z

    if-eqz v1, :cond_2

    const-string v1, "GoogleAnalyticsTracker"

    const-string v2, "...but there was nothing to dispatch"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method final c()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/analytics/j;->q:Z

    return-void
.end method

.method public final d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/analytics/j;->b:Z

    return v0
.end method
