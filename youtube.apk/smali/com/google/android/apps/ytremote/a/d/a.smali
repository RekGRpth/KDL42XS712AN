.class public final Lcom/google/android/apps/ytremote/a/d/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lorg/apache/http/impl/client/DefaultHttpClient;

.field private static b:Lorg/apache/http/impl/client/DefaultHttpClient;

.field private static c:Lorg/apache/http/impl/client/DefaultHttpClient;

.field private static d:Ljava/lang/String;


# direct methods
.method public static declared-synchronized a()Lorg/apache/http/impl/client/DefaultHttpClient;
    .locals 3

    const-class v1, Lcom/google/android/apps/ytremote/a/d/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/ytremote/a/d/a;->a:Lorg/apache/http/impl/client/DefaultHttpClient;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/ytremote/a/d/a;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/ytremote/util/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v0, 0xbb8

    const/16 v2, 0x1388

    invoke-static {v0, v2}, Lcom/google/android/apps/ytremote/a/d/a;->a(II)Lorg/apache/http/impl/client/DefaultHttpClient;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/ytremote/a/d/a;->a:Lorg/apache/http/impl/client/DefaultHttpClient;

    :cond_0
    sget-object v0, Lcom/google/android/apps/ytremote/a/d/a;->a:Lorg/apache/http/impl/client/DefaultHttpClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(II)Lorg/apache/http/impl/client/DefaultHttpClient;
    .locals 7

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/ytremote/a/d/c;

    invoke-direct {v1}, Lcom/google/android/apps/ytremote/a/d/c;-><init>()V

    invoke-virtual {v0, v1}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->setHostnameVerifier(Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V

    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    invoke-virtual {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/conn/ClientConnectionManager;->getSchemeRegistry()Lorg/apache/http/conn/scheme/SchemeRegistry;

    move-result-object v3

    new-instance v4, Lorg/apache/http/conn/scheme/Scheme;

    const-string v5, "https"

    const/16 v6, 0x1bb

    invoke-direct {v4, v5, v0, v6}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v3, v4}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    invoke-interface {v2}, Lorg/apache/http/conn/ClientConnectionManager;->getSchemeRegistry()Lorg/apache/http/conn/scheme/SchemeRegistry;

    move-result-object v0

    new-instance v3, Lorg/apache/http/conn/scheme/Scheme;

    const-string v4, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v5

    const/16 v6, 0x50

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v0, v3}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    invoke-virtual {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    const-string v1, "http.conn-manager.max-total"

    const/16 v3, 0x1e

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    invoke-static {v0, p0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    invoke-static {v0, p1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    sget-object v1, Lcom/google/android/apps/ytremote/a/d/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    new-instance v3, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-interface {v2}, Lorg/apache/http/conn/ClientConnectionManager;->getSchemeRegistry()Lorg/apache/http/conn/scheme/SchemeRegistry;

    move-result-object v2

    invoke-direct {v3, v0, v2}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    invoke-direct {v1, v3, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/google/android/apps/ytremote/a/d/a;->d:Ljava/lang/String;

    return-void
.end method

.method public static declared-synchronized b()Lorg/apache/http/impl/client/DefaultHttpClient;
    .locals 3

    const-class v1, Lcom/google/android/apps/ytremote/a/d/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/ytremote/a/d/a;->b:Lorg/apache/http/impl/client/DefaultHttpClient;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/ytremote/a/d/a;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/ytremote/util/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v0, 0x3a98

    const/16 v2, 0x4e20

    invoke-static {v0, v2}, Lcom/google/android/apps/ytremote/a/d/a;->a(II)Lorg/apache/http/impl/client/DefaultHttpClient;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/ytremote/a/d/a;->b:Lorg/apache/http/impl/client/DefaultHttpClient;

    :cond_0
    sget-object v0, Lcom/google/android/apps/ytremote/a/d/a;->b:Lorg/apache/http/impl/client/DefaultHttpClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized c()Lorg/apache/http/impl/client/DefaultHttpClient;
    .locals 3

    const-class v1, Lcom/google/android/apps/ytremote/a/d/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/ytremote/a/d/a;->c:Lorg/apache/http/impl/client/DefaultHttpClient;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/ytremote/a/d/a;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/ytremote/util/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v0, 0x708

    const/16 v2, 0x708

    invoke-static {v0, v2}, Lcom/google/android/apps/ytremote/a/d/a;->a(II)Lorg/apache/http/impl/client/DefaultHttpClient;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/ytremote/a/d/a;->c:Lorg/apache/http/impl/client/DefaultHttpClient;

    :cond_0
    sget-object v0, Lcom/google/android/apps/ytremote/a/d/a;->c:Lorg/apache/http/impl/client/DefaultHttpClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
