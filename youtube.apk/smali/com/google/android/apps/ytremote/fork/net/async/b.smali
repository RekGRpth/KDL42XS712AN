.class public Lcom/google/android/apps/ytremote/fork/net/async/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# static fields
.field static final synthetic a:Z

.field private static final b:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final c:I

.field private final d:J

.field private final e:J

.field private final f:Lcom/google/android/apps/ytremote/fork/net/async/c;

.field private final g:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/ytremote/fork/net/async/b;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/ytremote/fork/net/async/b;->a:Z

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/google/android/apps/ytremote/fork/net/async/b;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(JJLcom/google/android/apps/ytremote/fork/net/async/c;Ljava/lang/Object;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/apps/ytremote/fork/net/async/b;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/b;->c:I

    sget-boolean v0, Lcom/google/android/apps/ytremote/fork/net/async/b;->a:Z

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v0, Lcom/google/android/apps/ytremote/fork/net/async/b;->a:Z

    if-nez v0, :cond_1

    cmp-long v0, p1, p3

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    sget-boolean v0, Lcom/google/android/apps/ytremote/fork/net/async/b;->a:Z

    if-nez v0, :cond_2

    if-nez p5, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    iput-wide p1, p0, Lcom/google/android/apps/ytremote/fork/net/async/b;->d:J

    iput-wide p3, p0, Lcom/google/android/apps/ytremote/fork/net/async/b;->e:J

    iput-object p5, p0, Lcom/google/android/apps/ytremote/fork/net/async/b;->f:Lcom/google/android/apps/ytremote/fork/net/async/c;

    iput-object p6, p0, Lcom/google/android/apps/ytremote/fork/net/async/b;->g:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/b;->d:J

    return-wide v0
.end method

.method public final b()Lcom/google/android/apps/ytremote/fork/net/async/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/b;->f:Lcom/google/android/apps/ytremote/fork/net/async/c;

    return-object v0
.end method

.method public final c()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/b;->g:Ljava/lang/Object;

    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 6

    const/4 v1, 0x1

    const/4 v0, -0x1

    check-cast p1, Lcom/google/android/apps/ytremote/fork/net/async/b;

    iget-wide v2, p0, Lcom/google/android/apps/ytremote/fork/net/async/b;->d:J

    iget-wide v4, p1, Lcom/google/android/apps/ytremote/fork/net/async/b;->d:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v2, p0, Lcom/google/android/apps/ytremote/fork/net/async/b;->d:J

    iget-wide v4, p1, Lcom/google/android/apps/ytremote/fork/net/async/b;->d:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/google/android/apps/ytremote/fork/net/async/b;->c:I

    iget v3, p1, Lcom/google/android/apps/ytremote/fork/net/async/b;->c:I

    if-lt v2, v3, :cond_0

    iget v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/b;->c:I

    iget v2, p1, Lcom/google/android/apps/ytremote/fork/net/async/b;->c:I

    if-le v0, v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    sget-boolean v0, Lcom/google/android/apps/ytremote/fork/net/async/b;->a:Z

    if-nez v0, :cond_4

    if-eq p0, p1, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Expiration-time("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/google/android/apps/ytremote/fork/net/async/b;->d:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/ytremote/fork/net/async/b;->d:J

    iget-wide v3, p0, Lcom/google/android/apps/ytremote/fork/net/async/b;->e:J

    sub-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " + Creation-time("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/ytremote/fork/net/async/b;->e:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "); time is msec since epoch, hash code = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
