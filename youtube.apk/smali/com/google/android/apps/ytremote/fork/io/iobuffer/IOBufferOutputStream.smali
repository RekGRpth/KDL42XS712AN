.class public final Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream;
.super Ljava/io/OutputStream;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;)V
    .locals 0

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream;->a:Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;

    return-void
.end method


# virtual methods
.method public final close()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream;->a:Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream;->a:Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;->b()V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream;->a:Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;->a(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream;->a:Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;

    goto :goto_0
.end method

.method public final flush()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream;->a:Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream$IOBufferOutputStreamClosedException;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream$IOBufferOutputStreamClosedException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream;->a:Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;->b()V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream;->a:Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;->a()V

    return-void
.end method

.method public final write(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream;->a:Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream$IOBufferOutputStreamClosedException;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream$IOBufferOutputStreamClosedException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream;->a:Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;->b(I)V

    return-void
.end method

.method public final write([B)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream;->a:Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream$IOBufferOutputStreamClosedException;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream$IOBufferOutputStreamClosedException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream;->a:Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;->b([B)V

    return-void
.end method

.method public final write([BII)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream;->a:Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream$IOBufferOutputStreamClosedException;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream$IOBufferOutputStreamClosedException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/io/iobuffer/IOBufferOutputStream;->a:Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/ytremote/fork/io/iobuffer/b;->b([BII)V

    return-void
.end method
