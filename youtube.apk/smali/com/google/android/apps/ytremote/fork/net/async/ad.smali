.class public final Lcom/google/android/apps/ytremote/fork/net/async/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/ytremote/fork/net/async/ah;


# instance fields
.field private final a:Lcom/google/android/apps/ytremote/fork/net/async/ag;

.field private b:J

.field private c:Lcom/google/android/apps/ytremote/fork/net/async/ae;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/ytremote/fork/net/async/d;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/ad;->b:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/ad;->c:Lcom/google/android/apps/ytremote/fork/net/async/ae;

    new-instance v0, Lcom/google/android/apps/ytremote/fork/net/async/ag;

    invoke-direct {v0, p1}, Lcom/google/android/apps/ytremote/fork/net/async/ag;-><init>(Lcom/google/android/apps/ytremote/fork/net/async/d;)V

    iput-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/ad;->a:Lcom/google/android/apps/ytremote/fork/net/async/ag;

    return-void
.end method


# virtual methods
.method public final a(JLcom/google/android/apps/ytremote/fork/net/async/ae;)V
    .locals 4

    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid period for PeriodicAlarm (must be positive): "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/ad;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "switchOn() is called when a PeriodicAlarm is already on"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_1
    iput-wide p1, p0, Lcom/google/android/apps/ytremote/fork/net/async/ad;->b:J

    iput-object p3, p0, Lcom/google/android/apps/ytremote/fork/net/async/ad;->c:Lcom/google/android/apps/ytremote/fork/net/async/ae;

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/ad;->a:Lcom/google/android/apps/ytremote/fork/net/async/ag;

    iget-wide v1, p0, Lcom/google/android/apps/ytremote/fork/net/async/ad;->b:J

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/apps/ytremote/fork/net/async/ag;->a(JLcom/google/android/apps/ytremote/fork/net/async/ah;)V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Lcom/google/android/apps/ytremote/fork/net/async/ag;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/ad;->a:Lcom/google/android/apps/ytremote/fork/net/async/ag;

    if-eq p1, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown ResettableAlarm expired:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/ad;->c:Lcom/google/android/apps/ytremote/fork/net/async/ae;

    invoke-interface {v0}, Lcom/google/android/apps/ytremote/fork/net/async/ae;->b()V

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/ad;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/ad;->a:Lcom/google/android/apps/ytremote/fork/net/async/ag;

    iget-wide v1, p0, Lcom/google/android/apps/ytremote/fork/net/async/ad;->b:J

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/apps/ytremote/fork/net/async/ag;->a(JLcom/google/android/apps/ytremote/fork/net/async/ah;)V

    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized toString()Ljava/lang/String;
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/apps/ytremote/fork/net/async/ad;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "PeriodicAlarm is on with "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/ytremote/fork/net/async/ad;->a:Lcom/google/android/apps/ytremote/fork/net/async/ag;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    const-string v0, "PeriodicAlarm is off"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
