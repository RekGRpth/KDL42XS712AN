.class public final Lcom/google/android/apps/ytremote/backend/a/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/ytremote/backend/logic/a;


# instance fields
.field private final a:Landroid/content/SharedPreferences;

.field private b:Ljava/util/List;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "preferences can not be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/ytremote/util/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->a:Landroid/content/SharedPreferences;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->c:Z

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "screenIds"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "screenNames"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private declared-synchronized b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->c:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/ytremote/backend/a/n;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;)Lcom/google/android/apps/ytremote/model/CloudScreen;
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/ytremote/backend/a/n;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->b:Ljava/util/List;

    invoke-static {v0, p3}, Lcom/google/android/apps/ytremote/backend/a/c;->a(Ljava/util/List;Lcom/google/android/apps/ytremote/model/ScreenId;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/a/n;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/a/n;->b:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/google/android/apps/ytremote/backend/a/n;->a(Ljava/util/List;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen;
    .locals 4

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/ytremote/backend/a/n;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->b:Ljava/util/List;

    invoke-static {v0, p3}, Lcom/google/android/apps/ytremote/backend/a/c;->a(Ljava/util/List;Lcom/google/android/apps/ytremote/model/ScreenId;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v1

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-virtual {v1, p4}, Lcom/google/android/apps/ytremote/model/CloudScreen;->withName(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/ytremote/backend/a/n;->b:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/a/n;->b:Ljava/util/List;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/a/n;->b:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/google/android/apps/ytremote/backend/a/n;->a(Ljava/util/List;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 8

    const/4 v7, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->b:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->a:Landroid/content/SharedPreferences;

    const-string v1, "screenIds"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->b:Ljava/util/List;

    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->c:Z

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->b:Ljava/util/List;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->a:Landroid/content/SharedPreferences;

    const-string v1, "screenIds"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->a:Landroid/content/SharedPreferences;

    const-string v1, "screenNames"

    const-string v3, ""

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_2
    array-length v1, v2

    if-ge v0, v1, :cond_4

    aget-object v1, v2, v0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    new-instance v5, Lcom/google/android/apps/ytremote/model/CloudScreen;

    new-instance v6, Lcom/google/android/apps/ytremote/model/ScreenId;

    invoke-direct {v6, v1}, Lcom/google/android/apps/ytremote/model/ScreenId;-><init>(Ljava/lang/String;)V

    array-length v1, v3

    if-ge v0, v1, :cond_3

    aget-object v1, v3, v0

    :goto_3
    invoke-direct {v5, v6, v1, v7, v7}, Lcom/google/android/apps/ytremote/model/CloudScreen;-><init>(Lcom/google/android/apps/ytremote/model/ScreenId;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ClientName;Lcom/google/android/apps/ytremote/model/LoungeToken;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    const-string v1, ""

    goto :goto_3

    :cond_4
    iput-object v4, p0, Lcom/google/android/apps/ytremote/backend/a/n;->b:Ljava/util/List;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/CloudScreen;)V
    .locals 3

    const/4 v2, 0x5

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/ytremote/backend/a/n;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/ytremote/backend/a/n;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/n;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/apps/ytremote/backend/a/n;->a(Ljava/util/List;)V

    return-void
.end method
