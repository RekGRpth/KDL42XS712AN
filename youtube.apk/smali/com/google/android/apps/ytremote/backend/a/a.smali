.class public final Lcom/google/android/apps/ytremote/backend/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/ytremote/backend/logic/a;


# instance fields
.field private final a:Lcom/google/android/apps/ytremote/backend/logic/a;

.field private volatile b:Lcom/google/android/apps/ytremote/backend/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/ytremote/backend/logic/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/ytremote/backend/a/a;->a:Lcom/google/android/apps/ytremote/backend/logic/a;

    new-instance v0, Lcom/google/android/apps/ytremote/backend/a/b;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/backend/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/a;->b:Lcom/google/android/apps/ytremote/backend/a/b;

    return-void
.end method

.method private declared-synchronized b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/a;->b:Lcom/google/android/apps/ytremote/backend/a/b;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/ytremote/backend/a/b;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/a;->b:Lcom/google/android/apps/ytremote/backend/a/b;

    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/a/a;->a:Lcom/google/android/apps/ytremote/backend/logic/a;

    invoke-interface {v1, p1, p2}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/ytremote/backend/a/b;->a(Ljava/lang/String;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;)Lcom/google/android/apps/ytremote/model/CloudScreen;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/ytremote/backend/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/a;->a:Lcom/google/android/apps/ytremote/backend/logic/a;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/a;->b:Lcom/google/android/apps/ytremote/backend/a/b;

    invoke-virtual {v0, p1, p3}, Lcom/google/android/apps/ytremote/backend/a/b;->a(Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/ytremote/backend/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/a;->a:Lcom/google/android/apps/ytremote/backend/logic/a;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/a;->b:Lcom/google/android/apps/ytremote/backend/a/b;

    invoke-virtual {v0, p1, p3, p4}, Lcom/google/android/apps/ytremote/backend/a/b;->a(Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/ytremote/backend/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/a;->b:Lcom/google/android/apps/ytremote/backend/a/b;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/ytremote/backend/a/b;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/a;->b:Lcom/google/android/apps/ytremote/backend/a/b;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/backend/a/b;->a()V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/CloudScreen;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/ytremote/backend/a/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/a;->a:Lcom/google/android/apps/ytremote/backend/logic/a;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/a;->b:Lcom/google/android/apps/ytremote/backend/a/b;

    invoke-virtual {v0, p1, p3}, Lcom/google/android/apps/ytremote/backend/a/b;->a(Ljava/lang/String;Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    return-void
.end method
