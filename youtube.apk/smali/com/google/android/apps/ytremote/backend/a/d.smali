.class public final Lcom/google/android/apps/ytremote/backend/a/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/ytremote/backend/logic/a;


# instance fields
.field private final a:Lcom/google/android/apps/ytremote/backend/logic/a;

.field private final b:Lcom/google/android/apps/ytremote/backend/logic/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/ytremote/backend/logic/a;Lcom/google/android/apps/ytremote/backend/logic/a;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/ytremote/backend/a/d;->a:Lcom/google/android/apps/ytremote/backend/logic/a;

    iput-object p2, p0, Lcom/google/android/apps/ytremote/backend/a/d;->b:Lcom/google/android/apps/ytremote/backend/logic/a;

    return-void
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "SIGNED_OUT_USER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;)Lcom/google/android/apps/ytremote/model/CloudScreen;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/d;->a:Lcom/google/android/apps/ytremote/backend/logic/a;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v1

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/android/apps/ytremote/backend/a/d;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/d;->b:Lcom/google/android/apps/ytremote/backend/logic/a;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen;
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/d;->a:Lcom/google/android/apps/ytremote/backend/logic/a;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v1

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/android/apps/ytremote/backend/a/d;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/d;->b:Lcom/google/android/apps/ytremote/backend/logic/a;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/d;->a:Lcom/google/android/apps/ytremote/backend/logic/a;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/android/apps/ytremote/backend/a/d;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/d;->b:Lcom/google/android/apps/ytremote/backend/logic/a;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    return-object v2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/CloudScreen;)V
    .locals 2

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/ytremote/backend/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/ytremote/backend/a/c;->a(Ljava/util/List;Lcom/google/android/apps/ytremote/model/ScreenId;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/ytremote/backend/a/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/d;->a:Lcom/google/android/apps/ytremote/backend/logic/a;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    invoke-static {p1}, Lcom/google/android/apps/ytremote/backend/a/d;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/d;->b:Lcom/google/android/apps/ytremote/backend/logic/a;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/ytremote/backend/logic/a;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    :cond_1
    return-void
.end method
