.class public Lcom/google/android/apps/ytremote/backend/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/support/v4/f/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/ytremote/backend/a/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/ytremote/backend/a/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/support/v4/f/f;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Landroid/support/v4/f/f;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/b;->b:Landroid/support/v4/f/f;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;)Lcom/google/android/apps/ytremote/model/CloudScreen;
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/ytremote/backend/a/b;->b:Landroid/support/v4/f/f;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/b;->b:Landroid/support/v4/f/f;

    invoke-virtual {v0, p1}, Landroid/support/v4/f/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/ytremote/backend/a/b;->a:Ljava/lang/String;

    const-string v1, "Cache is not initialized. Remove operation has been refused."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    monitor-exit v2

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, p2}, Lcom/google/android/apps/ytremote/backend/a/c;->a(Ljava/util/List;Lcom/google/android/apps/ytremote/model/ScreenId;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ScreenId;Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen;
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/ytremote/backend/a/b;->b:Landroid/support/v4/f/f;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/b;->b:Landroid/support/v4/f/f;

    invoke-virtual {v0, p1}, Landroid/support/v4/f/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/ytremote/backend/a/b;->a:Ljava/lang/String;

    const-string v3, "Cache is not initialized. Update operation has been refused."

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v2

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, p2}, Lcom/google/android/apps/ytremote/backend/a/c;->a(Ljava/util/List;Lcom/google/android/apps/ytremote/model/ScreenId;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    invoke-interface {v0, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v3, p3}, Lcom/google/android/apps/ytremote/model/CloudScreen;->withName(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen;

    move-result-object v1

    invoke-interface {v0, v4, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move-object v0, v1

    :goto_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/b;->b:Landroid/support/v4/f/f;

    invoke-virtual {v0, p1}, Landroid/support/v4/f/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/b;->b:Landroid/support/v4/f/f;

    invoke-virtual {v0}, Landroid/support/v4/f/f;->a()V

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/ytremote/model/CloudScreen;)V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/a/b;->b:Landroid/support/v4/f/f;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/b;->b:Landroid/support/v4/f/f;

    invoke-virtual {v0, p1}, Landroid/support/v4/f/f;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/ytremote/backend/a/b;->a:Ljava/lang/String;

    const-string v2, "Cache is not initialized. Add operation has been refused."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x0

    invoke-interface {v0, v2, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/ytremote/backend/a/b;->b:Landroid/support/v4/f/f;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/backend/a/b;->b:Landroid/support/v4/f/f;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/f/f;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
