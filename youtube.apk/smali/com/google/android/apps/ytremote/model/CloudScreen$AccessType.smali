.class public final enum Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

.field public static final enum PERMANENT:Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

.field public static final enum TEMPORARY:Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    const-string v1, "PERMANENT"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;->PERMANENT:Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    new-instance v0, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    const-string v1, "TEMPORARY"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;->TEMPORARY:Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    sget-object v1, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;->PERMANENT:Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;->TEMPORARY:Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;->$VALUES:[Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;
    .locals 1

    const-class v0, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;
    .locals 1

    sget-object v0, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;->$VALUES:[Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    invoke-virtual {v0}, [Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    return-object v0
.end method
