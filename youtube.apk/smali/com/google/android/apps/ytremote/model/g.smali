.class public final Lcom/google/android/apps/ytremote/model/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/apps/ytremote/model/AppStatus;

.field private b:Landroid/net/Uri;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lcom/google/android/apps/ytremote/model/SsdpId;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    # getter for: Lcom/google/android/apps/ytremote/model/YouTubeDevice;->deviceName:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->access$000(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/ytremote/model/g;->c:Ljava/lang/String;

    # getter for: Lcom/google/android/apps/ytremote/model/YouTubeDevice;->manufacturer:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->access$100(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/ytremote/model/g;->e:Ljava/lang/String;

    # getter for: Lcom/google/android/apps/ytremote/model/YouTubeDevice;->modelName:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->access$200(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/ytremote/model/g;->f:Ljava/lang/String;

    # getter for: Lcom/google/android/apps/ytremote/model/YouTubeDevice;->ssdpId:Lcom/google/android/apps/ytremote/model/SsdpId;
    invoke-static {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->access$300(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/ytremote/model/g;->g:Lcom/google/android/apps/ytremote/model/SsdpId;

    # getter for: Lcom/google/android/apps/ytremote/model/YouTubeDevice;->appUrl:Landroid/net/Uri;
    invoke-static {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->access$400(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/ytremote/model/g;->b:Landroid/net/Uri;

    # getter for: Lcom/google/android/apps/ytremote/model/YouTubeDevice;->hasDialSupport:Z
    invoke-static {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->access$500(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/ytremote/model/g;->d:Z

    # getter for: Lcom/google/android/apps/ytremote/model/YouTubeDevice;->appStatus:Lcom/google/android/apps/ytremote/model/AppStatus;
    invoke-static {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->access$600(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)Lcom/google/android/apps/ytremote/model/AppStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/ytremote/model/g;->a:Lcom/google/android/apps/ytremote/model/AppStatus;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/ytremote/model/g;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/model/g;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/ytremote/model/g;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/model/g;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/ytremote/model/g;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/model/g;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/ytremote/model/g;)Lcom/google/android/apps/ytremote/model/SsdpId;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/model/g;->g:Lcom/google/android/apps/ytremote/model/SsdpId;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/ytremote/model/g;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/model/g;->b:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/ytremote/model/g;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/ytremote/model/g;->d:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/apps/ytremote/model/g;)Lcom/google/android/apps/ytremote/model/AppStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/model/g;->a:Lcom/google/android/apps/ytremote/model/AppStatus;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/ytremote/model/YouTubeDevice;
    .locals 1

    new-instance v0, Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-direct {v0, p0}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;-><init>(Lcom/google/android/apps/ytremote/model/g;)V

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)Lcom/google/android/apps/ytremote/model/g;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/ytremote/model/g;->b:Landroid/net/Uri;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/ytremote/model/AppStatus;)Lcom/google/android/apps/ytremote/model/g;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/ytremote/model/g;->a:Lcom/google/android/apps/ytremote/model/AppStatus;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/ytremote/model/SsdpId;)Lcom/google/android/apps/ytremote/model/g;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/ytremote/model/g;->g:Lcom/google/android/apps/ytremote/model/SsdpId;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/g;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/ytremote/model/g;->c:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Z)Lcom/google/android/apps/ytremote/model/g;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/ytremote/model/g;->d:Z

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/g;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/ytremote/model/g;->e:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/g;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/ytremote/model/g;->f:Ljava/lang/String;

    return-object p0
.end method
