.class public final Lcom/google/android/apps/ytremote/model/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/net/Uri;

.field private b:Ljava/lang/String;

.field private c:Lcom/google/android/apps/ytremote/model/ScreenId;

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/ytremote/model/b;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/ytremote/model/b;->d:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/ytremote/model/b;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/model/b;->a:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/ytremote/model/b;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/model/b;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/ytremote/model/b;)Lcom/google/android/apps/ytremote/model/ScreenId;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/model/b;->c:Lcom/google/android/apps/ytremote/model/ScreenId;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/ytremote/model/b;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/ytremote/model/b;->e:Z

    return v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/ytremote/model/AppStatus;
    .locals 1

    new-instance v0, Lcom/google/android/apps/ytremote/model/AppStatus;

    invoke-direct {v0, p0}, Lcom/google/android/apps/ytremote/model/AppStatus;-><init>(Lcom/google/android/apps/ytremote/model/b;)V

    return-object v0
.end method

.method public final a(I)Lcom/google/android/apps/ytremote/model/b;
    .locals 0

    iput p1, p0, Lcom/google/android/apps/ytremote/model/b;->d:I

    return-object p0
.end method

.method public final a(Landroid/net/Uri;)Lcom/google/android/apps/ytremote/model/b;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/ytremote/model/b;->a:Landroid/net/Uri;

    return-object p0
.end method

.method public final a(Lcom/google/android/apps/ytremote/model/ScreenId;)Lcom/google/android/apps/ytremote/model/b;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/ytremote/model/b;->c:Lcom/google/android/apps/ytremote/model/ScreenId;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/b;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/ytremote/model/b;->b:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Z)Lcom/google/android/apps/ytremote/model/b;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/apps/ytremote/model/b;->e:Z

    return-object p0
.end method
