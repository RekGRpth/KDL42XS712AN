.class final Lcom/google/android/apps/ytremote/model/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 5

    invoke-static {}, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;->values()[Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    sget-object v1, Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;->PERMANENT:Lcom/google/android/apps/ytremote/model/CloudScreen$AccessType;

    if-ne v0, v1, :cond_0

    const-class v0, Lcom/google/android/apps/ytremote/model/ScreenId;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/ScreenId;

    const-class v1, Lcom/google/android/apps/ytremote/model/ClientName;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/ytremote/model/ClientName;

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/ytremote/model/LoungeToken;

    new-instance v3, Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-direct {v3, v0, v4, v1, v2}, Lcom/google/android/apps/ytremote/model/CloudScreen;-><init>(Lcom/google/android/apps/ytremote/model/ScreenId;Ljava/lang/String;Lcom/google/android/apps/ytremote/model/ClientName;Lcom/google/android/apps/ytremote/model/LoungeToken;)V

    move-object v0, v3

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/TemporaryAccessToken;

    new-instance v1, Lcom/google/android/apps/ytremote/model/CloudScreen;

    invoke-direct {v1, v0, v4}, Lcom/google/android/apps/ytremote/model/CloudScreen;-><init>(Lcom/google/android/apps/ytremote/model/TemporaryAccessToken;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/apps/ytremote/model/CloudScreen;

    return-object v0
.end method
