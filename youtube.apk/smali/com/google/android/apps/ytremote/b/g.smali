.class final Lcom/google/android/apps/ytremote/b/g;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/ytremote/backend/logic/b;


# direct methods
.method private constructor <init>(Landroid/os/Looper;Lcom/google/android/apps/ytremote/backend/logic/b;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p2, p0, Lcom/google/android/apps/ytremote/b/g;->a:Lcom/google/android/apps/ytremote/backend/logic/b;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Looper;Lcom/google/android/apps/ytremote/backend/logic/b;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/ytremote/b/g;-><init>(Landroid/os/Looper;Lcom/google/android/apps/ytremote/backend/logic/b;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/ytremote/b/h;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/b/h;->a()Lcom/google/android/apps/ytremote/b/h;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/ytremote/b/h;->a:Lcom/google/android/apps/ytremote/b/h;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/ytremote/b/h;->c:Lcom/google/android/apps/ytremote/logic/e;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/ytremote/b/g;->a(Lcom/google/android/apps/ytremote/logic/e;Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    invoke-static {p0, v1, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/ytremote/b/h;->b()J

    move-result-wide v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/apps/ytremote/b/g;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method private static a(Lcom/google/android/apps/ytremote/logic/e;Lcom/google/android/apps/ytremote/model/CloudScreen;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x3

    invoke-interface {p0, v0}, Lcom/google/android/apps/ytremote/logic/e;->a(I)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p0, p1}, Lcom/google/android/apps/ytremote/logic/e;->a(Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/ytremote/logic/e;)V
    .locals 1

    invoke-static {p1, p2}, Lcom/google/android/apps/ytremote/b/h;->a(Lcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/ytremote/logic/e;)Lcom/google/android/apps/ytremote/b/h;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/ytremote/b/g;->a(Lcom/google/android/apps/ytremote/b/h;)V

    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 6

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/ytremote/b/h;

    iget-object v1, v0, Lcom/google/android/apps/ytremote/b/h;->b:Lcom/google/android/apps/ytremote/model/PairingCode;

    iget-object v2, v0, Lcom/google/android/apps/ytremote/b/h;->c:Lcom/google/android/apps/ytremote/logic/e;

    iget-object v3, p0, Lcom/google/android/apps/ytremote/b/g;->a:Lcom/google/android/apps/ytremote/backend/logic/b;

    invoke-interface {v3, v1}, Lcom/google/android/apps/ytremote/backend/logic/b;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/ytremote/model/CloudScreen;

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/android/apps/ytremote/b/d;->b()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Found screen with id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/apps/ytremote/model/ScreenId;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/ytremote/model/ScreenId;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v2, v1}, Lcom/google/android/apps/ytremote/b/g;->a(Lcom/google/android/apps/ytremote/logic/e;Lcom/google/android/apps/ytremote/model/CloudScreen;)V

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/ytremote/b/g;->a(Lcom/google/android/apps/ytremote/b/h;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
