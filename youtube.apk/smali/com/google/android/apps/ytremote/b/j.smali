.class public Lcom/google/android/apps/ytremote/b/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/ytremote/logic/b;


# static fields
.field private static final a:Ljava/util/List;

.field private static final b:Ljava/lang/String;

.field private static c:Ljava/util/regex/Pattern;


# instance fields
.field private final d:Ljava/util/concurrent/ScheduledExecutorService;

.field private final e:Lcom/google/android/apps/ytremote/b/a;

.field private final f:Lcom/google/android/apps/ytremote/logic/a;

.field private final g:Ljava/net/InetAddress;

.field private final h:Ljava/util/Set;

.field private final i:Ljava/util/Map;

.field private final j:Ljava/util/Set;

.field private final k:Ljava/util/Map;

.field private final l:Ljava/util/List;

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "urn:dial-multiscreen-org:service:dial:1"

    aput-object v0, v2, v1

    if-eqz v2, :cond_0

    array-length v0, v2

    if-nez v0, :cond_2

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/ytremote/b/j;->a:Ljava/util/List;

    const-class v0, Lcom/google/android/apps/ytremote/b/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    const-string v0, "^(.+?): (.+)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/ytremote/b/j;->c:Ljava/util/regex/Pattern;

    return-void

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    array-length v3, v2

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    array-length v3, v2

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/apps/ytremote/b/j;->d()Ljava/net/InetAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->g:Ljava/net/InetAddress;

    new-instance v0, Lcom/google/android/apps/ytremote/b/c;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/b/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->f:Lcom/google/android/apps/ytremote/logic/a;

    new-instance v0, Lcom/google/android/apps/ytremote/b/i;

    invoke-direct {v0}, Lcom/google/android/apps/ytremote/b/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->e:Lcom/google/android/apps/ytremote/b/a;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->h:Ljava/util/Set;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->i:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->j:Ljava/util/Set;

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->k:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->l:Ljava/util/List;

    return-void
.end method

.method private a(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)Lcom/google/android/apps/ytremote/model/YouTubeDevice;
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->hasDialSupport()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/ytremote/b/j;->f:Lcom/google/android/apps/ytremote/logic/a;

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getAppUri()Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/ytremote/logic/a;->a(Landroid/net/Uri;)Lcom/google/android/apps/ytremote/model/AppStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/AppStatus;->getStatus()I

    move-result v2

    const/4 v3, -0x2

    if-eq v2, v3, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/AppStatus;->getStatus()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    :cond_0
    sget-object v2, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Dropping TV: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " status: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/model/AppStatus;->getStatus()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {p1, v1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->withAppStatus(Lcom/google/android/apps/ytremote/model/AppStatus;)Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v0

    goto :goto_0

    :cond_3
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/ytremote/b/j;->e:Lcom/google/android/apps/ytremote/b/a;

    invoke-virtual {p1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getModelName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/ytremote/b/a;->a(Ljava/lang/String;)Lcom/google/android/apps/ytremote/b/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/b/b;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/ytremote/b/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->withNewDeviceName(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/YouTubeDevice;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    const-string v3, "Couldn\'t retrieve device information"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static a(Lorg/apache/http/HttpResponse;)Lcom/google/android/apps/ytremote/model/YouTubeDevice;
    .locals 7

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "Application-URL"

    invoke-interface {p0, v0}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v0

    array-length v2, v0

    if-eq v2, v4, :cond_0

    sget-object v0, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    const-string v2, "Expected one Application-URL header. Found 0 or more"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    aget-object v0, v0, v3

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    :try_start_0
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v4, 0x200

    new-array v4, v4, [B

    :goto_1
    invoke-virtual {v0, v4}, Ljava/io/InputStream;->read([B)I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v6, v5}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v2, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    const-string v3, "Error parsing device description response: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljavax/xml/parsers/DocumentBuilderFactory;->setNamespaceAware(Z)V

    invoke-virtual {v3}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v3

    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-direct {v4, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v3, v4}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    const-string v3, "device"

    invoke-interface {v0, v3}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/ytremote/model/g;

    invoke-direct {v3}, Lcom/google/android/apps/ytremote/model/g;-><init>()V

    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    if-nez v4, :cond_2

    sget-object v0, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    const-string v2, "No devices found in device description XML."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    const-string v4, "friendlyName"

    const-string v5, "urn:schemas-upnp-org:device-1-0"

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/ytremote/b/j;->a(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/ytremote/model/g;->a(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/g;

    new-instance v4, Lcom/google/android/apps/ytremote/model/SsdpId;

    const-string v5, "UDN"

    const-string v6, "urn:schemas-upnp-org:device-1-0"

    invoke-static {v0, v5, v6}, Lcom/google/android/apps/ytremote/b/j;->a(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/apps/ytremote/model/SsdpId;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/ytremote/model/g;->a(Lcom/google/android/apps/ytremote/model/SsdpId;)Lcom/google/android/apps/ytremote/model/g;

    const-string v4, "manufacturer"

    const-string v5, "urn:schemas-upnp-org:device-1-0"

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/ytremote/b/j;->a(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/ytremote/model/g;->b(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/g;

    const-string v4, "modelName"

    const-string v5, "urn:schemas-upnp-org:device-1-0"

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/ytremote/b/j;->a(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/apps/ytremote/model/g;->c(Ljava/lang/String;)Lcom/google/android/apps/ytremote/model/g;

    if-eqz v2, :cond_3

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/google/android/apps/ytremote/model/g;->a(Z)Lcom/google/android/apps/ytremote/model/g;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "YouTube"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/apps/ytremote/model/g;->a(Landroid/net/Uri;)Lcom/google/android/apps/ytremote/model/g;

    :goto_2
    invoke-virtual {v3}, Lcom/google/android/apps/ytremote/model/g;->a()Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/google/android/apps/ytremote/model/g;->a(Z)Lcom/google/android/apps/ytremote/model/g;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    return-object v0
.end method

.method private static a(Lorg/w3c/dom/Element;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    invoke-interface {p0}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-interface {v1, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    instance-of v3, v2, Lorg/w3c/dom/Element;

    if-eqz v3, :cond_0

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getLocalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNamespaceURI()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/net/DatagramPacket;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/ytremote/b/j;->c(Ljava/lang/String;)Ljava/net/DatagramPacket;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/net/NetworkInterface;)Ljava/net/InetAddress;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v2

    if-nez v2, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    instance-of v3, v0, Ljava/net/Inet4Address;

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/ytremote/b/j;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->l:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/ytremote/b/j;Ljava/lang/String;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/android/apps/ytremote/a/d/a;->c()Lorg/apache/http/impl/client/DefaultHttpClient;

    move-result-object v0

    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    const-string v2, "Origin"

    const-string v3, "package:com.google.android.youtube"

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-interface {v0, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/ytremote/b/j;->a(Lorg/apache/http/HttpResponse;)Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/ytremote/b/j;->a(Ljava/lang/String;Lcom/google/android/apps/ytremote/model/YouTubeDevice;)V
    :try_end_0
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    const-string v1, "Timed out reading device details at %s failed"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    const-string v2, "Reading device details at %s failed: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/ytremote/b/j;Ljava/net/DatagramSocket;Ljava/util/List;)V
    .locals 11

    const/4 v3, 0x0

    const/16 v0, 0x7d0

    const/16 v1, 0x400

    new-array v4, v1, [B

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    new-instance v6, Ljava/net/DatagramPacket;

    array-length v1, v4

    invoke-direct {v6, v4, v1}, Ljava/net/DatagramPacket;-><init>([BI)V

    :try_start_0
    invoke-virtual {p1, v0}, Ljava/net/DatagramSocket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    const/4 v1, 0x1

    :try_start_1
    invoke-virtual {p1, v6}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long v7, v9, v7

    long-to-int v2, v7

    sub-int v2, v0, v2

    if-lez v2, :cond_0

    if-eqz v1, :cond_3

    new-instance v0, Ljava/lang/String;

    invoke-virtual {v6}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v1

    invoke-virtual {v6}, Ljava/net/DatagramPacket;->getLength()I

    move-result v6

    invoke-direct {v0, v1, v3, v6}, Ljava/lang/String;-><init>([BII)V

    invoke-static {v0}, Lcom/google/android/apps/ytremote/b/j;->b(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    const-string v1, "ST"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "LOCATION"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/ytremote/b/j;->k:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/ytremote/b/j;->k:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/ytremote/b/j;->a(Ljava/lang/String;Lcom/google/android/apps/ytremote/model/YouTubeDevice;)V

    move v0, v2

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    const-string v3, "Error setting socket timeout"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v2, v0

    :cond_0
    :goto_2
    const-wide/16 v0, 0x1c84

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-wide v3, v0

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    int-to-long v9, v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v9, v10, v1}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    const-wide/16 v0, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long v7, v9, v7

    sub-long v7, v3, v7

    invoke-static {v0, v1, v7, v8}, Ljava/lang/Math;->max(JJ)J
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_5

    move-result-wide v0

    move-wide v3, v0

    goto :goto_3

    :catch_1
    move-exception v1

    move v2, v0

    goto :goto_2

    :catch_2
    move-exception v1

    sget-object v2, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    const-string v9, "Error receiving m search response packet"

    invoke-static {v2, v9, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v1, v3

    goto/16 :goto_1

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/ytremote/b/j;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v6, Lcom/google/android/apps/ytremote/b/k;

    invoke-direct {v6, p0, v0}, Lcom/google/android/apps/ytremote/b/k;-><init>(Lcom/google/android/apps/ytremote/b/j;Ljava/lang/String;)V

    invoke-interface {v1, v6}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v2

    goto/16 :goto_0

    :catch_3
    move-exception v0

    sget-object v0, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    const-string v1, "Read device response task cancelled while waiting for reading device details task to complete"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v5}, Lcom/google/android/apps/ytremote/b/j;->a(Ljava/util/List;)V

    :cond_2
    return-void

    :catch_4
    move-exception v0

    sget-object v1, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    const-string v7, "Error waiting for reading device details task to complete"

    invoke-static {v1, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :catch_5
    move-exception v0

    sget-object v0, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    const-string v1, "Timed out whilst reading device details"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_3
    move v0, v2

    goto/16 :goto_0
.end method

.method private declared-synchronized a(Ljava/lang/String;Lcom/google/android/apps/ytremote/model/YouTubeDevice;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->h:Ljava/util/Set;

    invoke-virtual {p2}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->h:Ljava/util/Set;

    invoke-virtual {p2}, Lcom/google/android/apps/ytremote/model/YouTubeDevice;->getSsdpId()Lcom/google/android/apps/ytremote/model/SsdpId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, p2}, Lcom/google/android/apps/ytremote/b/j;->a(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->i:Ljava/util/Map;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/logic/c;

    invoke-interface {v0, v1}, Lcom/google/android/apps/ytremote/logic/c;->a(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->k:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method private static a(Ljava/util/List;)V
    .locals 3

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/ytremote/b/j;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/ytremote/b/j;->m:Z

    return v0
.end method

.method private static b(Ljava/net/NetworkInterface;)Ljava/net/MulticastSocket;
    .locals 6

    const/4 v3, 0x1

    :try_start_0
    new-instance v0, Ljava/net/MulticastSocket;

    invoke-direct {v0}, Ljava/net/MulticastSocket;-><init>()V

    invoke-virtual {v0, p0}, Ljava/net/MulticastSocket;->setNetworkInterface(Ljava/net/NetworkInterface;)V

    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Ljava/net/MulticastSocket;->setReceiveBufferSize(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/MulticastSocket;->setBroadcast(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    const-string v2, "Error creating socket on interface %s"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljava/net/NetworkInterface;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/google/android/apps/ytremote/b/j;->a:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/ytremote/b/j;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->k:Ljava/util/Map;

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/util/Map;
    .locals 5

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    new-instance v1, Ljava/util/Scanner;

    invoke-direct {v1, p0}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/util/Scanner;->hasNextLine()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/ytremote/b/j;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private static c(Ljava/lang/String;)Ljava/net/DatagramPacket;
    .locals 5

    :try_start_0
    const-string v0, "239.255.255.250"

    invoke-static {v0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "M-SEARCH * HTTP/1.1\r\nHOST: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":1900"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\r\nMAN: \"ssdp:discover\"\r\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "MX: 1"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\r\nST: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\r\n\r\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    new-instance v0, Ljava/net/DatagramPacket;

    array-length v3, v2

    const/16 v4, 0x76c

    invoke-direct {v0, v2, v3, v1, v4}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()Ljava/util/List;
    .locals 9

    const/4 v4, 0x1

    const/4 v3, 0x0

    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/NetworkInterface;

    :try_start_1
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x9

    if-lt v2, v6, :cond_3

    invoke-virtual {v0}, Ljava/net/NetworkInterface;->isLoopback()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Ljava/net/NetworkInterface;->isPointToPoint()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v2, v4

    :goto_1
    if-nez v2, :cond_0

    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/ytremote/b/j;->a(Ljava/net/NetworkInterface;)Ljava/net/InetAddress;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    const-string v2, "Error retrieving local interfaces"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_2
    move v2, v3

    goto :goto_1

    :cond_3
    :try_start_2
    invoke-static {v0}, Lcom/google/android/apps/ytremote/b/j;->a(Ljava/net/NetworkInterface;)Ljava/net/InetAddress;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/ytremote/b/j;->g:Ljava/net/InetAddress;

    invoke-virtual {v2, v6}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v2

    if-eqz v2, :cond_4

    move v2, v4

    goto :goto_1

    :cond_4
    move v2, v3

    goto :goto_1

    :catch_1
    move-exception v2

    sget-object v6, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    const-string v7, "Could not read interface type for %s"

    new-array v8, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/net/NetworkInterface;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v8, v3

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method static synthetic c(Lcom/google/android/apps/ytremote/b/j;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->i:Ljava/util/Map;

    return-object v0
.end method

.method private static d()Ljava/net/InetAddress;
    .locals 1

    :try_start_0
    const-string v0, "127.0.0.1"

    invoke-static {v0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/apps/ytremote/b/j;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->h:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/apps/ytremote/b/j;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->j:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/apps/ytremote/b/j;)V
    .locals 11

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/ytremote/b/j;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/ytremote/b/j;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lcom/google/android/apps/ytremote/b/j;->k:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/NetworkInterface;

    invoke-static {v0}, Lcom/google/android/apps/ytremote/b/j;->b(Ljava/net/NetworkInterface;)Ljava/net/MulticastSocket;

    move-result-object v4

    if-eqz v4, :cond_0

    sget-object v0, Lcom/google/android/apps/ytremote/b/j;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move v1, v2

    :goto_1
    const/4 v6, 0x3

    if-ge v1, v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/ytremote/b/j;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v7, Lcom/google/android/apps/ytremote/b/l;

    invoke-direct {v7, p0, v0, v4}, Lcom/google/android/apps/ytremote/b/l;-><init>(Lcom/google/android/apps/ytremote/b/j;Ljava/lang/String;Ljava/net/MulticastSocket;)V

    mul-int/lit16 v8, v1, 0x12c

    int-to-long v8, v8

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v6, v7, v8, v9, v10}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->l:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/ytremote/b/j;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v5, Lcom/google/android/apps/ytremote/b/m;

    invoke-direct {v5, p0, v4}, Lcom/google/android/apps/ytremote/b/m;-><init>(Lcom/google/android/apps/ytremote/b/j;Ljava/net/MulticastSocket;)V

    invoke-interface {v1, v5}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/google/android/apps/ytremote/b/n;

    invoke-direct {v1, p0}, Lcom/google/android/apps/ytremote/b/n;-><init>(Lcom/google/android/apps/ytremote/b/j;)V

    const-wide/16 v2, 0x2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    :cond_4
    :goto_2
    return-void

    :cond_5
    iput-boolean v2, p0, Lcom/google/android/apps/ytremote/b/j;->m:Z

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/ytremote/logic/c;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->d:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->isShutdown()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/ytremote/b/j;->b:Ljava/lang/String;

    const-string v1, "Can not call find after stopSearch. Bye!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->j:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Lcom/google/android/apps/ytremote/b/j;->m:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/ytremote/model/YouTubeDevice;

    invoke-interface {p1, v0}, Lcom/google/android/apps/ytremote/logic/c;->a(Lcom/google/android/apps/ytremote/model/YouTubeDevice;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/ytremote/b/j;->m:Z

    iget-object v0, p0, Lcom/google/android/apps/ytremote/b/j;->d:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/google/android/apps/ytremote/b/o;

    invoke-direct {v1, p0}, Lcom/google/android/apps/ytremote/b/o;-><init>(Lcom/google/android/apps/ytremote/b/j;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
