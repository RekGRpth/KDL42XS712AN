.class final Lcom/google/android/apps/ytremote/b/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/apps/ytremote/b/h;

.field private static final e:[J


# instance fields
.field final b:Lcom/google/android/apps/ytremote/model/PairingCode;

.field final c:Lcom/google/android/apps/ytremote/logic/e;

.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const/16 v0, 0xa

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/ytremote/b/h;->e:[J

    new-instance v0, Lcom/google/android/apps/ytremote/b/h;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/apps/ytremote/b/h;-><init>(ILcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/ytremote/logic/e;)V

    sput-object v0, Lcom/google/android/apps/ytremote/b/h;->a:Lcom/google/android/apps/ytremote/b/h;

    return-void

    nop

    :array_0
    .array-data 8
        0x1388
        0x7d0
        0x7d0
        0x7d0
        0x7d0
        0x7d0
        0x1388
        0x2710
        0x3a98
        0x4e20
    .end array-data
.end method

.method private constructor <init>(ILcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/ytremote/logic/e;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/ytremote/b/h;->d:I

    iput-object p2, p0, Lcom/google/android/apps/ytremote/b/h;->b:Lcom/google/android/apps/ytremote/model/PairingCode;

    iput-object p3, p0, Lcom/google/android/apps/ytremote/b/h;->c:Lcom/google/android/apps/ytremote/logic/e;

    return-void
.end method

.method public static a(Lcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/ytremote/logic/e;)Lcom/google/android/apps/ytremote/b/h;
    .locals 2

    new-instance v0, Lcom/google/android/apps/ytremote/b/h;

    const/4 v1, -0x1

    invoke-direct {v0, v1, p0, p1}, Lcom/google/android/apps/ytremote/b/h;-><init>(ILcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/ytremote/logic/e;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/ytremote/b/h;
    .locals 4

    iget v0, p0, Lcom/google/android/apps/ytremote/b/h;->d:I

    sget-object v1, Lcom/google/android/apps/ytremote/b/h;->e:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    new-instance v0, Lcom/google/android/apps/ytremote/b/h;

    iget v1, p0, Lcom/google/android/apps/ytremote/b/h;->d:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/ytremote/b/h;->b:Lcom/google/android/apps/ytremote/model/PairingCode;

    iget-object v3, p0, Lcom/google/android/apps/ytremote/b/h;->c:Lcom/google/android/apps/ytremote/logic/e;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/ytremote/b/h;-><init>(ILcom/google/android/apps/ytremote/model/PairingCode;Lcom/google/android/apps/ytremote/logic/e;)V

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/ytremote/b/h;->a:Lcom/google/android/apps/ytremote/b/h;

    goto :goto_0
.end method

.method public final b()J
    .locals 2

    sget-object v0, Lcom/google/android/apps/ytremote/b/h;->e:[J

    iget v1, p0, Lcom/google/android/apps/ytremote/b/h;->d:I

    aget-wide v0, v0, v1

    return-wide v0
.end method
