.class public final Lcom/google/android/gms/drive/internal/h;
.super Lcom/google/android/gms/internal/jt;


# static fields
.field public static final a:[Lcom/google/android/gms/drive/internal/h;


# instance fields
.field public b:I

.field public c:Ljava/lang/String;

.field public d:J

.field public e:J

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/drive/internal/h;

    sput-object v0, Lcom/google/android/gms/drive/internal/h;->a:[Lcom/google/android/gms/drive/internal/h;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, -0x1

    invoke-direct {p0}, Lcom/google/android/gms/internal/jt;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/h;->b:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/h;->c:Ljava/lang/String;

    iput-wide v1, p0, Lcom/google/android/gms/drive/internal/h;->d:J

    iput-wide v1, p0, Lcom/google/android/gms/drive/internal/h;->e:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/h;->g:I

    return-void
.end method

.method public static a([B)Lcom/google/android/gms/drive/internal/h;
    .locals 3

    new-instance v0, Lcom/google/android/gms/drive/internal/h;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/h;-><init>()V

    const/4 v1, 0x0

    array-length v2, p0

    invoke-static {v0, p0, v1, v2}, Lcom/google/android/gms/internal/jt;->b(Lcom/google/android/gms/internal/jt;[BII)Lcom/google/android/gms/internal/jt;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/h;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 4

    iget v0, p0, Lcom/google/android/gms/drive/internal/h;->b:I

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/android/gms/internal/ll;->a(I)I

    move-result v1

    if-ltz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/internal/ll;->c(I)I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/drive/internal/h;->c:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/gms/internal/ll;->a(I)I

    move-result v2

    invoke-static {v1}, Lcom/google/android/gms/internal/ll;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/h;->d:J

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/internal/ll;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/h;->e:J

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/internal/ll;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/drive/internal/h;->g:I

    return v0

    :cond_0
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/gms/internal/js;)Lcom/google/android/gms/internal/jt;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/js;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/jw;->a(Lcom/google/android/gms/internal/js;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/android/gms/internal/js;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/internal/h;->b:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/android/gms/internal/js;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/h;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/android/gms/internal/js;->d()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/internal/h;->d:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/android/gms/internal/js;->d()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/internal/h;->e:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lcom/google/android/gms/internal/ll;)V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/gms/drive/internal/h;->b:I

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, Lcom/google/android/gms/internal/ll;->a(II)V

    if-ltz v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/ll;->b(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/h;->c:Ljava/lang/String;

    invoke-virtual {p1, v3, v3}, Lcom/google/android/gms/internal/ll;->a(II)V

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p1, v1}, Lcom/google/android/gms/internal/ll;->b(I)V

    array-length v1, v0

    invoke-virtual {p1, v0, v2, v1}, Lcom/google/android/gms/internal/ll;->b([BII)V

    const/4 v0, 0x3

    iget-wide v1, p0, Lcom/google/android/gms/drive/internal/h;->d:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/gms/internal/ll;->a(IJ)V

    const/4 v0, 0x4

    iget-wide v1, p0, Lcom/google/android/gms/drive/internal/h;->e:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/gms/internal/ll;->a(IJ)V

    return-void

    :cond_0
    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/internal/ll;->a(J)V

    goto :goto_0
.end method
