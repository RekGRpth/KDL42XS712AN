.class public Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/aa;


# instance fields
.field public final appInfo:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

.field final jE:I

.field final jS:[Lcom/google/android/gms/appdatasearch/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/aa;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/aa;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->CREATOR:Lcom/google/android/gms/appdatasearch/aa;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;[Lcom/google/android/gms/appdatasearch/g;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->jE:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->appInfo:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->jS:[Lcom/google/android/gms/appdatasearch/g;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;Ljava/util/Map;)V
    .locals 2

    const/4 v0, 0x1

    invoke-static {p2}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->n(Ljava/util/Map;)[Lcom/google/android/gms/appdatasearch/g;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;-><init>(ILcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;[Lcom/google/android/gms/appdatasearch/g;)V

    return-void
.end method

.method private static n(Ljava/util/Map;)[Lcom/google/android/gms/appdatasearch/g;
    .locals 6

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v0

    new-array v3, v0, [Lcom/google/android/gms/appdatasearch/g;

    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    new-instance v5, Lcom/google/android/gms/appdatasearch/g;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/internal/gi;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/gi;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appdatasearch/Feature;

    invoke-direct {v5, v1, v0}, Lcom/google/android/gms/appdatasearch/g;-><init>(Ljava/lang/String;[Lcom/google/android/gms/appdatasearch/Feature;)V

    aput-object v5, v3, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v3

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->CREATOR:Lcom/google/android/gms/appdatasearch/aa;

    const/4 v0, 0x0

    return v0
.end method

.method public getCorpusFeatures(Ljava/lang/String;)[Lcom/google/android/gms/appdatasearch/Feature;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->jS:[Lcom/google/android/gms/appdatasearch/g;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->jS:[Lcom/google/android/gms/appdatasearch/g;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->jS:[Lcom/google/android/gms/appdatasearch/g;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/google/android/gms/appdatasearch/g;->corpusName:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->jS:[Lcom/google/android/gms/appdatasearch/g;

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/g;->features:[Lcom/google/android/gms/appdatasearch/Feature;

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getCorpusNames()Ljava/util/Set;
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->jS:[Lcom/google/android/gms/appdatasearch/g;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/util/HashSet;

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->jS:[Lcom/google/android/gms/appdatasearch/g;

    array-length v0, v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->jS:[Lcom/google/android/gms/appdatasearch/g;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->jS:[Lcom/google/android/gms/appdatasearch/g;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/android/gms/appdatasearch/g;->corpusName:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->CREATOR:Lcom/google/android/gms/appdatasearch/aa;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/aa;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;Landroid/os/Parcel;I)V

    return-void
.end method
