.class public Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/t;


# instance fields
.field public final corpus:Lcom/google/android/gms/appdatasearch/CorpusId;

.field final jE:I

.field public final weight:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/t;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/t;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/t;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/appdatasearch/CorpusId;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->jE:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->corpus:Lcom/google/android/gms/appdatasearch/CorpusId;

    iput p3, p0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->weight:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/appdatasearch/CorpusId;I)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;-><init>(ILcom/google/android/gms/appdatasearch/CorpusId;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 3

    const/4 v0, 0x1

    new-instance v1, Lcom/google/android/gms/appdatasearch/CorpusId;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1, p2}, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;-><init>(ILcom/google/android/gms/appdatasearch/CorpusId;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/CorpusId;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;-><init>(Lcom/google/android/gms/appdatasearch/CorpusId;I)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/t;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/t;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/t;->a(Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;Landroid/os/Parcel;I)V

    return-void
.end method
