.class public Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/af;


# instance fields
.field final jE:I

.field final mErrorMessage:Ljava/lang/String;

.field public final nextIterToken:[B

.field public final updates:[Lcom/google/android/gms/appdatasearch/PIMEUpdate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/af;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/af;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;->CREATOR:Lcom/google/android/gms/appdatasearch/af;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;[B[Lcom/google/android/gms/appdatasearch/PIMEUpdate;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;->jE:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;->mErrorMessage:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;->nextIterToken:[B

    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;->updates:[Lcom/google/android/gms/appdatasearch/PIMEUpdate;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;->CREATOR:Lcom/google/android/gms/appdatasearch/af;

    const/4 v0, 0x0

    return v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;->mErrorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public hasError()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;->mErrorMessage:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;->CREATOR:Lcom/google/android/gms/appdatasearch/af;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/af;->a(Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;Landroid/os/Parcel;I)V

    return-void
.end method
