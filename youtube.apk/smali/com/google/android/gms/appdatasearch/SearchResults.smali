.class public Lcom/google/android/gms/appdatasearch/SearchResults;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Ljava/lang/Iterable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/ap;


# instance fields
.field final jE:I

.field final kK:[I

.field final kL:[B

.field final kM:[Landroid/os/Bundle;

.field final kN:[Landroid/os/Bundle;

.field final kO:[Landroid/os/Bundle;

.field final kP:I

.field final kQ:[I

.field final kR:[Ljava/lang/String;

.field final mErrorMessage:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/ap;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/ap;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:Lcom/google/android/gms/appdatasearch/ap;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;[I[B[Landroid/os/Bundle;[Landroid/os/Bundle;[Landroid/os/Bundle;I[I[Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->jE:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mErrorMessage:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->kK:[I

    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->kL:[B

    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->kM:[Landroid/os/Bundle;

    iput-object p6, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->kN:[Landroid/os/Bundle;

    iput-object p7, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->kO:[Landroid/os/Bundle;

    iput p8, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->kP:I

    iput-object p9, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->kQ:[I

    iput-object p10, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->kR:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:Lcom/google/android/gms/appdatasearch/ap;

    const/4 v0, 0x0

    return v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mErrorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getNumResults()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->kP:I

    return v0
.end method

.method public hasError()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/SearchResults;->mErrorMessage:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Lcom/google/android/gms/appdatasearch/l;
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/l;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/l;-><init>(Lcom/google/android/gms/appdatasearch/SearchResults;)V

    return-object v0
.end method

.method public iterator(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/l;
    .locals 2

    new-instance v0, Lcom/google/android/gms/appdatasearch/m;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/appdatasearch/m;-><init>(Lcom/google/android/gms/appdatasearch/SearchResults;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public iterator(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/l;
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/m;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/appdatasearch/m;-><init>(Lcom/google/android/gms/appdatasearch/SearchResults;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/SearchResults;->iterator()Lcom/google/android/gms/appdatasearch/l;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/SearchResults;->CREATOR:Lcom/google/android/gms/appdatasearch/ap;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/ap;->a(Lcom/google/android/gms/appdatasearch/SearchResults;Landroid/os/Parcel;I)V

    return-void
.end method
