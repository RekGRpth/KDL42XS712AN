.class public Lcom/google/android/gms/appdatasearch/QuerySpecification;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/aj;


# instance fields
.field final jE:I

.field public final prefixMatch:Z

.field public final wantUris:Z

.field public final wantedSections:Ljava/util/List;

.field public final wantedTags:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/aj;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/aj;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/aj;

    return-void
.end method

.method constructor <init>(IZLjava/util/List;Ljava/util/List;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->jE:I

    iput-boolean p2, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->wantUris:Z

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->wantedTags:Ljava/util/List;

    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->wantedSections:Ljava/util/List;

    iput-boolean p5, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->prefixMatch:Z

    return-void
.end method

.method public constructor <init>(ZZLjava/util/List;Ljava/util/List;)V
    .locals 6

    const/4 v1, 0x2

    move-object v0, p0

    move v2, p2

    move-object v3, p4

    move-object v4, p3

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/appdatasearch/QuerySpecification;-><init>(IZLjava/util/List;Ljava/util/List;Z)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/aj;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/aj;

    invoke-static {p0, p1}, Lcom/google/android/gms/appdatasearch/aj;->a(Lcom/google/android/gms/appdatasearch/QuerySpecification;Landroid/os/Parcel;)V

    return-void
.end method
