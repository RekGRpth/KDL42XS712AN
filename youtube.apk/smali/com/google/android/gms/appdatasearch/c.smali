.class final Lcom/google/android/gms/appdatasearch/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 4

    check-cast p1, Lcom/google/android/gms/appdatasearch/b;

    check-cast p2, Lcom/google/android/gms/appdatasearch/b;

    iget-wide v0, p2, Lcom/google/android/gms/appdatasearch/b;->c:D

    iget-wide v2, p1, Lcom/google/android/gms/appdatasearch/b;->c:D

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->signum(D)D

    move-result-wide v0

    double-to-int v0, v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/b;->b:Ljava/lang/String;

    iget-object v1, p2, Lcom/google/android/gms/appdatasearch/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    :cond_0
    return v0
.end method
