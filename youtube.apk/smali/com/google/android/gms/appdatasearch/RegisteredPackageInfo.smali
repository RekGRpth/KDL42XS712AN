.class public Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/am;


# instance fields
.field public final blocked:Z

.field final jE:I

.field public final packageName:Ljava/lang/String;

.field public final reclaimableDiskBytes:J

.field public final usedDiskBytes:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/am;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/am;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/am;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;JZJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->jE:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->packageName:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->usedDiskBytes:J

    iput-boolean p5, p0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->blocked:Z

    iput-wide p6, p0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->reclaimableDiskBytes:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JZJ)V
    .locals 8

    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move-wide v3, p2

    move v5, p4

    move-wide v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;-><init>(ILjava/lang/String;JZJ)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/am;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/am;

    invoke-static {p0, p1}, Lcom/google/android/gms/appdatasearch/am;->a(Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;Landroid/os/Parcel;)V

    return-void
.end method
