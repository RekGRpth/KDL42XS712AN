.class public Lcom/google/android/gms/appdatasearch/PIMEUpdate;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/ae;


# instance fields
.field public final createdTimestamp:J

.field public final inputByUser:Z

.field final jE:I

.field final kf:[B

.field final kg:[B

.field final kh:Landroid/os/Bundle;

.field public final score:J

.field public final sourceClass:I

.field public final sourceCorpusHandle:Ljava/lang/String;

.field public final sourcePackageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/ae;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/ae;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:Lcom/google/android/gms/appdatasearch/ae;

    return-void
.end method

.method constructor <init>(I[B[BILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->jE:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->kf:[B

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->kg:[B

    iput p4, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->sourceClass:I

    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->sourcePackageName:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->sourceCorpusHandle:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->inputByUser:Z

    iput-object p8, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->kh:Landroid/os/Bundle;

    iput-wide p9, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->score:J

    iput-wide p11, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->createdTimestamp:J

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:Lcom/google/android/gms/appdatasearch/ae;

    const/4 v0, 0x0

    return v0
.end method

.method public getContents()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->kf:[B

    return-object v0
.end method

.method public getTokenIterator()Lcom/google/android/gms/appdatasearch/e;
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/e;-><init>(Lcom/google/android/gms/appdatasearch/PIMEUpdate;)V

    return-object v0
.end method

.method public getTopLanguageScores()[Lcom/google/android/gms/appdatasearch/b;
    .locals 10

    const/4 v0, 0x0

    const-wide/16 v5, 0x0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->kh:Landroid/os/Bundle;

    if-nez v1, :cond_0

    new-array v0, v0, [Lcom/google/android/gms/appdatasearch/b;

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->kh:Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Bundle;->size()I

    move-result v1

    new-array v7, v1, [Lcom/google/android/gms/appdatasearch/b;

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->kh:Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v0

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->kh:Landroid/os/Bundle;

    invoke-virtual {v2, v0, v5, v6}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v2

    cmpg-double v4, v2, v5

    if-gtz v4, :cond_2

    move-wide v3, v5

    :goto_2
    add-int/lit8 v2, v1, 0x1

    new-instance v9, Lcom/google/android/gms/appdatasearch/b;

    invoke-direct {v9, v0, v3, v4}, Lcom/google/android/gms/appdatasearch/b;-><init>(Ljava/lang/String;D)V

    aput-object v9, v7, v1

    move v1, v2

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/google/android/gms/appdatasearch/b;->a:Ljava/util/Comparator;

    invoke-static {v7, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    move-object v0, v7

    goto :goto_0

    :cond_2
    move-wide v3, v2

    goto :goto_2
.end method

.method public getVarIntLengths()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->kg:[B

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->CREATOR:Lcom/google/android/gms/appdatasearch/ae;

    invoke-static {p0, p1}, Lcom/google/android/gms/appdatasearch/ae;->a(Lcom/google/android/gms/appdatasearch/PIMEUpdate;Landroid/os/Parcel;)V

    return-void
.end method
