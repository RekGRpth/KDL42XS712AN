.class public final Lcom/google/android/gms/appdatasearch/e;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Iterable;
.implements Ljava/util/Iterator;


# instance fields
.field a:Lcom/google/android/gms/internal/jr;

.field final synthetic b:Lcom/google/android/gms/appdatasearch/PIMEUpdate;

.field private c:I

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/appdatasearch/PIMEUpdate;)V
    .locals 3

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/e;->b:Lcom/google/android/gms/appdatasearch/PIMEUpdate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/e;->b:Lcom/google/android/gms/appdatasearch/PIMEUpdate;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->kg:[B

    array-length v1, v0

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/internal/jr;->a([BII)Lcom/google/android/gms/internal/jr;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/e;->a:Lcom/google/android/gms/internal/jr;

    iput v2, p0, Lcom/google/android/gms/appdatasearch/e;->c:I

    iput v2, p0, Lcom/google/android/gms/appdatasearch/e;->d:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/appdatasearch/e;->e:Z

    return-void
.end method

.method private a()Lcom/google/android/gms/appdatasearch/d;
    .locals 6

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/e;->a:Lcom/google/android/gms/internal/jr;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/jr;->a()I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/appdatasearch/e;->d:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/e;->b:Lcom/google/android/gms/appdatasearch/PIMEUpdate;

    iget-object v2, v2, Lcom/google/android/gms/appdatasearch/PIMEUpdate;->kf:[B

    iget v3, p0, Lcom/google/android/gms/appdatasearch/e;->c:I

    iget v4, p0, Lcom/google/android/gms/appdatasearch/e;->d:I

    const-string v5, "UTF-8"

    invoke-direct {v1, v2, v3, v4, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    new-instance v2, Lcom/google/android/gms/appdatasearch/d;

    iget-boolean v3, p0, Lcom/google/android/gms/appdatasearch/e;->e:Z

    invoke-direct {v2, v1, v3, v0}, Lcom/google/android/gms/appdatasearch/d;-><init>(Ljava/lang/String;ZB)V

    iget v1, p0, Lcom/google/android/gms/appdatasearch/e;->c:I

    iget v3, p0, Lcom/google/android/gms/appdatasearch/e;->d:I

    add-int/2addr v1, v3

    iput v1, p0, Lcom/google/android/gms/appdatasearch/e;->c:I

    iget-boolean v1, p0, Lcom/google/android/gms/appdatasearch/e;->e:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/gms/appdatasearch/e;->e:Z

    return-object v2

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final hasNext()Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/e;->a:Lcom/google/android/gms/internal/jr;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/jr;->b()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    const-string v2, "AppDataSearchClient"

    invoke-static {v2, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 0

    return-object p0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/appdatasearch/e;->a()Lcom/google/android/gms/appdatasearch/d;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "remove not implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
