.class public final Lcom/google/android/gms/c/a;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/android/gms/internal/iz;

.field private b:Lcom/google/android/gms/internal/ju;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Lcom/google/android/gms/c/b;)V
    .locals 6

    const/4 v3, 0x0

    const/16 v2, 0xb

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/c/a;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/c/b;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/c/b;)V
    .locals 7

    const/4 v4, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/c/a;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/c/b;Z)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/c/b;Z)V
    .locals 7

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v2, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Lcom/google/android/gms/internal/ju;

    const/4 v6, 0x1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/ju;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/gms/c/a;->b:Lcom/google/android/gms/internal/ju;

    new-instance v0, Lcom/google/android/gms/internal/iz;

    new-instance v1, Lcom/google/android/gms/internal/ix;

    invoke-direct {v1, p5}, Lcom/google/android/gms/internal/ix;-><init>(Lcom/google/android/gms/c/b;)V

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/internal/iz;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/ix;)V

    iput-object v0, p0, Lcom/google/android/gms/c/a;->a:Lcom/google/android/gms/internal/iz;

    return-void

    :catch_0
    move-exception v0

    const-string v0, "PlayLogger"

    const-string v3, "This can\'t happen."

    invoke-static {v0, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/c/a;->a:Lcom/google/android/gms/internal/iz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/iz;->c()V

    return-void
.end method

.method public final varargs a(JLjava/lang/String;[B[Ljava/lang/String;)V
    .locals 8

    iget-object v6, p0, Lcom/google/android/gms/c/a;->a:Lcom/google/android/gms/internal/iz;

    iget-object v7, p0, Lcom/google/android/gms/c/a;->b:Lcom/google/android/gms/internal/ju;

    new-instance v0, Lcom/google/android/gms/internal/jq;

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/jq;-><init>(JLjava/lang/String;[B[Ljava/lang/String;)V

    invoke-virtual {v6, v7, v0}, Lcom/google/android/gms/internal/iz;->a(Lcom/google/android/gms/internal/ju;Lcom/google/android/gms/internal/jq;)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/c/a;->a:Lcom/google/android/gms/internal/iz;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/iz;->l()V

    return-void
.end method
