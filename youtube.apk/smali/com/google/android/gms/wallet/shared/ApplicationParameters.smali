.class public final Lcom/google/android/gms/wallet/shared/ApplicationParameters;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field Pp:I

.field Pu:Landroid/accounts/Account;

.field Pv:Z

.field final jE:I

.field mArgs:Landroid/os/Bundle;

.field mTheme:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/wallet/shared/c;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/shared/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->Pv:Z

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->jE:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->Pp:I

    iput v1, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->mTheme:I

    return-void
.end method

.method constructor <init>(IILandroid/accounts/Account;Landroid/os/Bundle;ZI)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->Pv:Z

    iput p1, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->jE:I

    iput p2, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->Pp:I

    iput-object p3, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->Pu:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->mArgs:Landroid/os/Bundle;

    iput-boolean p5, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->Pv:Z

    iput p6, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->mTheme:I

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/wallet/shared/a;
    .locals 3

    new-instance v0, Lcom/google/android/gms/wallet/shared/a;

    new-instance v1, Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    invoke-direct {v1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/shared/a;-><init>(Lcom/google/android/gms/wallet/shared/ApplicationParameters;B)V

    return-object v0
.end method

.method public static newBuilderFrom(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lcom/google/android/gms/wallet/shared/a;
    .locals 3

    invoke-static {}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->newBuilder()Lcom/google/android/gms/wallet/shared/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->getEnvironment()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/a;->a(I)Lcom/google/android/gms/wallet/shared/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->getBuyerAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/a;->a(Landroid/accounts/Account;)Lcom/google/android/gms/wallet/shared/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->getArgs()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/a;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->isAccountSelectionAllowed()Z

    move-result v1

    iget-object v2, v0, Lcom/google/android/gms/wallet/shared/a;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput-boolean v1, v2, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->Pv:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->getTheme()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/a;->b(I)Lcom/google/android/gms/wallet/shared/a;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getArgs()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->mArgs:Landroid/os/Bundle;

    return-object v0
.end method

.method public final getBuyerAccount()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->Pu:Landroid/accounts/Account;

    return-object v0
.end method

.method public final getEnvironment()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->Pp:I

    return v0
.end method

.method public final getTheme()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->mTheme:I

    return v0
.end method

.method public final isAccountSelectionAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->Pv:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/wallet/shared/c;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;Landroid/os/Parcel;I)V

    return-void
.end method
