.class public final Lcom/google/android/gms/wallet/shared/a;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/shared/a;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/wallet/shared/ApplicationParameters;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/shared/a;-><init>(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/wallet/shared/ApplicationParameters;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/a;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    return-object v0
.end method

.method public final a(I)Lcom/google/android/gms/wallet/shared/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/a;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput p1, v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->Pp:I

    return-object p0
.end method

.method public final a(Landroid/accounts/Account;)Lcom/google/android/gms/wallet/shared/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/a;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput-object p1, v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->Pu:Landroid/accounts/Account;

    return-object p0
.end method

.method public final a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/a;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput-object p1, v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->mArgs:Landroid/os/Bundle;

    return-object p0
.end method

.method public final b(I)Lcom/google/android/gms/wallet/shared/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/a;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput p1, v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->mTheme:I

    return-object p0
.end method
