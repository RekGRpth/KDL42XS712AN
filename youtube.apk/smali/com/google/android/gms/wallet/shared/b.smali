.class public final Lcom/google/android/gms/wallet/shared/b;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/wallet/shared/b;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/shared/b;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/b;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v0, v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->Px:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/b;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->Px:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/b;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lcom/google/android/gms/wallet/shared/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/b;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object p1, v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->Py:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/b;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object p1, v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->Px:Ljava/lang/String;

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/b;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object p1, v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->Pz:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/b;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object p1, v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->PA:Ljava/lang/String;

    return-object p0
.end method
