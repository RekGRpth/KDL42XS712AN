.class public final Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field PA:Ljava/lang/String;

.field PB:Ljava/lang/String;

.field Px:Ljava/lang/String;

.field Py:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

.field Pz:Ljava/lang/String;

.field final jE:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/wallet/shared/d;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/shared/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->jE:I

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Lcom/google/android/gms/wallet/shared/ApplicationParameters;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->jE:I

    iput-object p2, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->Px:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->Py:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput-object p4, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->Pz:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->PA:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->PB:Ljava/lang/String;

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/wallet/shared/b;
    .locals 3

    new-instance v0, Lcom/google/android/gms/wallet/shared/b;

    new-instance v1, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-direct {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/shared/b;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;B)V

    return-object v0
.end method

.method public static newBuilderFrom(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/b;
    .locals 3

    invoke-static {}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->newBuilder()Lcom/google/android/gms/wallet/shared/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->getApplicationParams()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/b;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lcom/google/android/gms/wallet/shared/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->getCallingAppIdentifier()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/wallet/shared/b;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v1, v2, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->PB:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->getCallingPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/b;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->getFlowName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/b;->c(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->getTransactionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/b;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/b;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getApplicationParams()Lcom/google/android/gms/wallet/shared/ApplicationParameters;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->Py:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    return-object v0
.end method

.method public final getCallingAppIdentifier()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->PB:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->Pz:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->PB:Ljava/lang/String;

    goto :goto_0
.end method

.method public final getCallingPackage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->Pz:Ljava/lang/String;

    return-object v0
.end method

.method public final getFlowName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->PA:Ljava/lang/String;

    return-object v0
.end method

.method public final getTransactionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->Px:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/wallet/shared/d;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/os/Parcel;I)V

    return-void
.end method
