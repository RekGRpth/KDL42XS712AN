.class public final Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;
.super Lcom/google/android/gms/wallet/ia/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const-string v0, "com.google.android.gms.wallet.ACTION_REVIEW_PURCHASE_OPTIONS"

    const-string v1, "flow_inapp"

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/wallet/ia/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final onIntentBuilt(Landroid/content/Intent;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/content/Intent;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v1, "com.google.android.libraries.inapp.EXTRA_JWT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "JWT is required"

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/gi;->b(ZLjava/lang/Object;)V

    return-object p1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setDisplayHints(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;->mArgs:Landroid/os/Bundle;

    const-string v1, "com.google.android.libraries.inapp.EXTRA_DISPLAY_HINTS"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    return-object p0
.end method

.method public final setFreeUsageAmount(I)Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;->mArgs:Landroid/os/Bundle;

    const-string v1, "com.google.android.gms.wallet.freeUsageAmount"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object p0
.end method

.method public final setJwt(Ljava/lang/String;)Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;->mIntent:Landroid/content/Intent;

    const-string v1, "com.google.android.libraries.inapp.EXTRA_JWT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object p0
.end method

.method public final setUiTemplate(I)Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;->mArgs:Landroid/os/Bundle;

    const-string v1, "com.google.android.libraries.inapp.EXTRA_UI_TEMPLATE"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object p0
.end method

.method public final setUsageUnit(I)Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PurchaseIntentBuilder;->mArgs:Landroid/os/Bundle;

    const-string v1, "com.google.android.libraries.inapp.EXTRA_USAGE_UNIT"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object p0
.end method
