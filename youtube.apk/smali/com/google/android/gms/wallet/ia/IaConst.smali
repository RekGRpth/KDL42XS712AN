.class public final Lcom/google/android/gms/wallet/ia/IaConst;
.super Ljava/lang/Object;


# static fields
.field public static final ERROR_CODE_CANNOT_AUTHENTICATE:Ljava/lang/String; = "com.google.android.libraries.inapp.ERROR_CODE_CANNOT_AUTHENTICATE"

.field public static final ERROR_CODE_DELIVERY_FAILED:Ljava/lang/String; = "com.google.android.libraries.inapp.ERROR_DELIVERY_FAILED"

.field public static final ERROR_CODE_GOOGLE_AUTHENTICATOR_UNAVAILABLE:Ljava/lang/String; = "com.google.android.libraries.inapp.ERROR_CODE_GOOGLE_AUTHENTICATOR_UNAVAILABLE"

.field public static final ERROR_CODE_INVALID_JWT:Ljava/lang/String; = "com.google.android.libraries.inapp.ERROR_CODE_INVALID_JWT"

.field public static final ERROR_CODE_NETWORK_ERROR:Ljava/lang/String; = "com.google.android.libraries.inapp.ERROR_CODE_NETWORK_ERROR"

.field public static final ERROR_CODE_PAYMENT_FAILED:Ljava/lang/String; = "com.google.android.libraries.inapp.ERROR_PAYMENT_FAILED"

.field public static final ERROR_CODE_SELF_PURCHASE:Ljava/lang/String; = "com.google.android.libraries.inapp.ERROR_CODE_SELF_PURCHASE"

.field public static final ERROR_CODE_UNKNOWN:Ljava/lang/String; = "com.google.android.libraries.inapp.ERROR_CODE_UNKNOWN"

.field public static final EXTRA_ERROR_CODE:Ljava/lang/String; = "com.google.android.libraries.inapp.EXTRA_ERROR_CODE"

.field public static final EXTRA_JWT:Ljava/lang/String; = "com.google.android.libraries.inapp.EXTRA_JWT"

.field public static final EXTRA_REQUEST:Ljava/lang/String; = "com.google.android.libraries.inapp.EXTRA_REQUEST"

.field public static final EXTRA_RESPONSE:Ljava/lang/String; = "com.google.android.libraries.inapp.EXTRA_RESPONSE"

.field public static final GREENBACK_AVAILABLE:I = 0x0

.field public static final GREENBACK_UPDATE_REQUIRED:I = 0x578

.field public static final PLAY_SERVICES_DISABLED:I = 0x3

.field public static final PLAY_SERVICES_INVALID:I = 0x9

.field public static final PLAY_SERVICES_MISSING:I = 0x1

.field public static final PLAY_SERVICES_UPDATE_REQUIRED:I = 0x2

.field public static final RESULT_ERROR:I = 0x1


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
