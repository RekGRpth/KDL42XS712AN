.class public final Lcom/google/android/gms/b/e;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/b;


# instance fields
.field private final a:Lcom/google/android/gms/internal/ii;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/ii;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/internal/ii;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;)V

    iput-object v0, p0, Lcom/google/android/gms/b/e;->a:Lcom/google/android/gms/internal/ii;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/b/e;->a:Lcom/google/android/gms/internal/ii;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ii;->d()V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/b/e;->a:Lcom/google/android/gms/internal/ii;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/ii;->a(Lcom/google/android/gms/common/c;)V

    return-void
.end method

.method public final a(Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/os/Bundle;)V
    .locals 4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-eqz p1, :cond_0

    const-string v1, "latency_micros"

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    if-eqz p2, :cond_1

    const-string v1, "latency_bps"

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_1
    if-eqz p3, :cond_2

    const-string v1, "latitude_e6"

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_2
    if-eqz p4, :cond_3

    const-string v1, "longitude_e6"

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_3
    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz p5, :cond_5

    invoke-virtual {p5}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/b/e;->a:Lcom/google/android/gms/internal/ii;

    invoke-virtual {v1, v0, p5}, Lcom/google/android/gms/internal/ii;->a(Landroid/os/Bundle;Landroid/os/Bundle;)V

    :cond_5
    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/b/e;->a:Lcom/google/android/gms/internal/ii;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ii;->g()V

    return-void
.end method

.method public final b(Lcom/google/android/gms/common/c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/b/e;->a:Lcom/google/android/gms/internal/ii;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/ii;->b(Lcom/google/android/gms/common/c;)V

    return-void
.end method
