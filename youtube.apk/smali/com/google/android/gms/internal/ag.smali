.class public final Lcom/google/android/gms/internal/ag;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/android/gms/internal/ce;

.field private final b:Lcom/google/android/gms/internal/at;

.field private final c:Landroid/content/Context;

.field private final d:Ljava/lang/Object;

.field private final e:Lcom/google/android/gms/internal/aj;

.field private f:Z

.field private g:Lcom/google/android/gms/internal/am;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/ce;Lcom/google/android/gms/internal/at;Lcom/google/android/gms/internal/aj;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/ag;->d:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/ag;->f:Z

    iput-object p1, p0, Lcom/google/android/gms/internal/ag;->c:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/internal/ag;->a:Lcom/google/android/gms/internal/ce;

    iput-object p3, p0, Lcom/google/android/gms/internal/ag;->b:Lcom/google/android/gms/internal/at;

    iput-object p4, p0, Lcom/google/android/gms/internal/ag;->e:Lcom/google/android/gms/internal/aj;

    return-void
.end method


# virtual methods
.method public final a(JJ)Lcom/google/android/gms/internal/ap;
    .locals 12

    const-string v0, "Starting mediation."

    invoke-static {v0}, Lcom/google/android/gms/internal/do;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/ag;->e:Lcom/google/android/gms/internal/aj;

    iget-object v0, v0, Lcom/google/android/gms/internal/aj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/internal/ai;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Trying mediation network: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v5, Lcom/google/android/gms/internal/ai;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/do;->b(Ljava/lang/String;)V

    iget-object v0, v5, Lcom/google/android/gms/internal/ai;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/gms/internal/ag;->d:Ljava/lang/Object;

    monitor-enter v11

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/internal/ag;->f:Z

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/gms/internal/ap;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ap;-><init>(I)V

    monitor-exit v11

    :goto_1
    return-object v0

    :cond_2
    new-instance v0, Lcom/google/android/gms/internal/am;

    iget-object v1, p0, Lcom/google/android/gms/internal/ag;->c:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/internal/ag;->b:Lcom/google/android/gms/internal/at;

    iget-object v4, p0, Lcom/google/android/gms/internal/ag;->e:Lcom/google/android/gms/internal/aj;

    iget-object v6, p0, Lcom/google/android/gms/internal/ag;->a:Lcom/google/android/gms/internal/ce;

    iget-object v6, v6, Lcom/google/android/gms/internal/ce;->hu:Lcom/google/android/gms/internal/aa;

    iget-object v7, p0, Lcom/google/android/gms/internal/ag;->a:Lcom/google/android/gms/internal/ce;

    iget-object v7, v7, Lcom/google/android/gms/internal/ce;->eq:Lcom/google/android/gms/internal/ac;

    iget-object v8, p0, Lcom/google/android/gms/internal/ag;->a:Lcom/google/android/gms/internal/ce;

    iget-object v8, v8, Lcom/google/android/gms/internal/ce;->en:Lcom/google/android/gms/internal/cz;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/internal/am;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/internal/at;Lcom/google/android/gms/internal/aj;Lcom/google/android/gms/internal/ai;Lcom/google/android/gms/internal/aa;Lcom/google/android/gms/internal/ac;Lcom/google/android/gms/internal/cz;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/ag;->g:Lcom/google/android/gms/internal/am;

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/internal/ag;->g:Lcom/google/android/gms/internal/am;

    const-wide/32 v1, 0xea60

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/google/android/gms/internal/am;->a(JJ)Lcom/google/android/gms/internal/ap;

    move-result-object v0

    iget v1, v0, Lcom/google/android/gms/internal/ap;->a:I

    if-nez v1, :cond_3

    const-string v1, "Adapter succeeded."

    invoke-static {v1}, Lcom/google/android/gms/internal/do;->a(Ljava/lang/String;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v11

    throw v0

    :cond_3
    iget-object v1, v0, Lcom/google/android/gms/internal/ap;->c:Lcom/google/android/gms/internal/av;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/gms/internal/dn;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/internal/ah;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/internal/ah;-><init>(Lcom/google/android/gms/internal/ag;Lcom/google/android/gms/internal/ap;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_4
    new-instance v0, Lcom/google/android/gms/internal/ap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/ap;-><init>(I)V

    goto :goto_1
.end method

.method public final a()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/ag;->d:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/internal/ag;->f:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/ag;->g:Lcom/google/android/gms/internal/am;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/ag;->g:Lcom/google/android/gms/internal/am;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/am;->a()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
