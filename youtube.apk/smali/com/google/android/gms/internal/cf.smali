.class public final Lcom/google/android/gms/internal/cf;
.super Ljava/lang/Object;


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/gms/internal/ce;Lcom/google/android/gms/internal/ch;)Lcom/google/android/gms/internal/de;
    .locals 1

    iget-object v0, p1, Lcom/google/android/gms/internal/ce;->en:Lcom/google/android/gms/internal/cz;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/cz;->iP:Z

    if-eqz v0, :cond_0

    const-string v0, "Fetching ad response from local ad request service."

    invoke-static {v0}, Lcom/google/android/gms/internal/do;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/internal/cj;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/internal/cj;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/ce;Lcom/google/android/gms/internal/ch;)V

    invoke-virtual {v0}, Lcom/google/android/gms/internal/cj;->e()V

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Fetching ad response from remote ad request service."

    invoke-static {v0}, Lcom/google/android/gms/internal/do;->a(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/gms/common/e;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Failed to connect to remote ad request service."

    invoke-static {v0}, Lcom/google/android/gms/internal/do;->d(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/internal/ck;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/internal/ck;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/ce;Lcom/google/android/gms/internal/ch;)V

    goto :goto_0
.end method
