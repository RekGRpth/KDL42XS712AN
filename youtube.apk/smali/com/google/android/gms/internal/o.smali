.class public final Lcom/google/android/gms/internal/o;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/y;


# instance fields
.field private final a:Lcom/google/android/gms/internal/p;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/p;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/o;->a:Lcom/google/android/gms/internal/p;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/internal/db;Ljava/util/Map;)V
    .locals 1

    const-string v0, "name"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "App event with no name parameter."

    invoke-static {v0}, Lcom/google/android/gms/internal/do;->d(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/o;->a:Lcom/google/android/gms/internal/p;

    const-string v0, "info"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
