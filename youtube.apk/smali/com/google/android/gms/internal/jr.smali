.class public final Lcom/google/android/gms/internal/jr;
.super Ljava/lang/Object;


# instance fields
.field private final a:[B

.field private b:I

.field private c:I

.field private d:I

.field private final e:Ljava/io/InputStream;

.field private f:I

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method private constructor <init>([BII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/gms/internal/jr;->g:I

    const/16 v0, 0x40

    iput v0, p0, Lcom/google/android/gms/internal/jr;->h:I

    const/high16 v0, 0x4000000

    iput v0, p0, Lcom/google/android/gms/internal/jr;->i:I

    iput-object p1, p0, Lcom/google/android/gms/internal/jr;->a:[B

    add-int v0, p2, p3

    iput v0, p0, Lcom/google/android/gms/internal/jr;->b:I

    iput p2, p0, Lcom/google/android/gms/internal/jr;->d:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/jr;->e:Ljava/io/InputStream;

    return-void
.end method

.method public static a([BII)Lcom/google/android/gms/internal/jr;
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/jr;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p2}, Lcom/google/android/gms/internal/jr;-><init>([BII)V

    return-object v0
.end method

.method private a(Z)Z
    .locals 4

    const/4 v1, -0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/gms/internal/jr;->d:I

    iget v3, p0, Lcom/google/android/gms/internal/jr;->b:I

    if-ge v0, v3, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "refillBuffer() called when buffer wasn\'t empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/gms/internal/jr;->f:I

    iget v3, p0, Lcom/google/android/gms/internal/jr;->b:I

    add-int/2addr v0, v3

    iget v3, p0, Lcom/google/android/gms/internal/jr;->g:I

    if-ne v0, v3, :cond_2

    if-eqz p1, :cond_1

    invoke-static {}, Lcom/google/android/gms/internal/lj;->gE()Lcom/google/android/gms/internal/lj;

    move-result-object v0

    throw v0

    :cond_1
    move v0, v2

    :goto_0
    return v0

    :cond_2
    iget v0, p0, Lcom/google/android/gms/internal/jr;->f:I

    iget v3, p0, Lcom/google/android/gms/internal/jr;->b:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/google/android/gms/internal/jr;->f:I

    iput v2, p0, Lcom/google/android/gms/internal/jr;->d:I

    iget-object v0, p0, Lcom/google/android/gms/internal/jr;->e:Ljava/io/InputStream;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    iput v0, p0, Lcom/google/android/gms/internal/jr;->b:I

    iget v0, p0, Lcom/google/android/gms/internal/jr;->b:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/gms/internal/jr;->b:I

    if-ge v0, v1, :cond_5

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InputStream#read(byte[]) returned invalid result: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/internal/jr;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nThe InputStream implementation is buggy."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/internal/jr;->e:Ljava/io/InputStream;

    iget-object v3, p0, Lcom/google/android/gms/internal/jr;->a:[B

    invoke-virtual {v0, v3}, Ljava/io/InputStream;->read([B)I

    move-result v0

    goto :goto_1

    :cond_5
    iget v0, p0, Lcom/google/android/gms/internal/jr;->b:I

    if-ne v0, v1, :cond_7

    iput v2, p0, Lcom/google/android/gms/internal/jr;->b:I

    if-eqz p1, :cond_6

    invoke-static {}, Lcom/google/android/gms/internal/lj;->gE()Lcom/google/android/gms/internal/lj;

    move-result-object v0

    throw v0

    :cond_6
    move v0, v2

    goto :goto_0

    :cond_7
    iget v0, p0, Lcom/google/android/gms/internal/jr;->b:I

    iget v1, p0, Lcom/google/android/gms/internal/jr;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/internal/jr;->b:I

    iget v0, p0, Lcom/google/android/gms/internal/jr;->f:I

    iget v1, p0, Lcom/google/android/gms/internal/jr;->b:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/jr;->g:I

    if-le v0, v1, :cond_9

    iget v1, p0, Lcom/google/android/gms/internal/jr;->g:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/internal/jr;->c:I

    iget v0, p0, Lcom/google/android/gms/internal/jr;->b:I

    iget v1, p0, Lcom/google/android/gms/internal/jr;->c:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/internal/jr;->b:I

    :goto_2
    iget v0, p0, Lcom/google/android/gms/internal/jr;->f:I

    iget v1, p0, Lcom/google/android/gms/internal/jr;->b:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/jr;->c:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/jr;->i:I

    if-gt v0, v1, :cond_8

    if-gez v0, :cond_a

    :cond_8
    invoke-static {}, Lcom/google/android/gms/internal/lj;->gG()Lcom/google/android/gms/internal/lj;

    move-result-object v0

    throw v0

    :cond_9
    iput v2, p0, Lcom/google/android/gms/internal/jr;->c:I

    goto :goto_2

    :cond_a
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private c()B
    .locals 3

    iget v0, p0, Lcom/google/android/gms/internal/jr;->d:I

    iget v1, p0, Lcom/google/android/gms/internal/jr;->b:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/jr;->a(Z)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/jr;->a:[B

    iget v1, p0, Lcom/google/android/gms/internal/jr;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/gms/internal/jr;->d:I

    aget-byte v0, v0, v1

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gms/internal/jr;->c()B

    move-result v0

    if-ltz v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    and-int/lit8 v0, v0, 0x7f

    invoke-direct {p0}, Lcom/google/android/gms/internal/jr;->c()B

    move-result v1

    if-ltz v1, :cond_2

    shl-int/lit8 v1, v1, 0x7

    or-int/2addr v0, v1

    goto :goto_0

    :cond_2
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0x7

    or-int/2addr v0, v1

    invoke-direct {p0}, Lcom/google/android/gms/internal/jr;->c()B

    move-result v1

    if-ltz v1, :cond_3

    shl-int/lit8 v1, v1, 0xe

    or-int/2addr v0, v1

    goto :goto_0

    :cond_3
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0xe

    or-int/2addr v0, v1

    invoke-direct {p0}, Lcom/google/android/gms/internal/jr;->c()B

    move-result v1

    if-ltz v1, :cond_4

    shl-int/lit8 v1, v1, 0x15

    or-int/2addr v0, v1

    goto :goto_0

    :cond_4
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0x15

    or-int/2addr v0, v1

    invoke-direct {p0}, Lcom/google/android/gms/internal/jr;->c()B

    move-result v1

    shl-int/lit8 v2, v1, 0x1c

    or-int/2addr v0, v2

    if-gez v1, :cond_0

    const/4 v1, 0x0

    :goto_1
    const/4 v2, 0x5

    if-ge v1, v2, :cond_5

    invoke-direct {p0}, Lcom/google/android/gms/internal/jr;->c()B

    move-result v2

    if-gez v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    invoke-static {}, Lcom/google/android/gms/internal/lj;->gF()Lcom/google/android/gms/internal/lj;

    move-result-object v0

    throw v0
.end method

.method public final b()Z
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/gms/internal/jr;->d:I

    iget v2, p0, Lcom/google/android/gms/internal/jr;->b:I

    if-ne v1, v2, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/jr;->a(Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
