.class final Lcom/google/android/gms/internal/ao;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/internal/al;

.field final synthetic b:Lcom/google/android/gms/internal/am;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/am;Lcom/google/android/gms/internal/al;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/ao;->b:Lcom/google/android/gms/internal/am;

    iput-object p2, p0, Lcom/google/android/gms/internal/ao;->a:Lcom/google/android/gms/internal/al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/internal/ao;->b:Lcom/google/android/gms/internal/am;

    invoke-static {v0}, Lcom/google/android/gms/internal/am;->a(Lcom/google/android/gms/internal/am;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ao;->b:Lcom/google/android/gms/internal/am;

    invoke-static {v0}, Lcom/google/android/gms/internal/am;->b(Lcom/google/android/gms/internal/am;)I

    move-result v0

    const/4 v2, -0x2

    if-eq v0, v2, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ao;->b:Lcom/google/android/gms/internal/am;

    iget-object v2, p0, Lcom/google/android/gms/internal/ao;->b:Lcom/google/android/gms/internal/am;

    invoke-static {v2}, Lcom/google/android/gms/internal/am;->c(Lcom/google/android/gms/internal/am;)Lcom/google/android/gms/internal/av;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/internal/am;->a(Lcom/google/android/gms/internal/am;Lcom/google/android/gms/internal/av;)Lcom/google/android/gms/internal/av;

    iget-object v0, p0, Lcom/google/android/gms/internal/ao;->b:Lcom/google/android/gms/internal/am;

    invoke-static {v0}, Lcom/google/android/gms/internal/am;->d(Lcom/google/android/gms/internal/am;)Lcom/google/android/gms/internal/av;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/ao;->b:Lcom/google/android/gms/internal/am;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/am;->a(I)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/internal/ao;->a:Lcom/google/android/gms/internal/al;

    iget-object v2, p0, Lcom/google/android/gms/internal/ao;->b:Lcom/google/android/gms/internal/am;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/al;->a(Lcom/google/android/gms/internal/aq;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/ao;->b:Lcom/google/android/gms/internal/am;

    iget-object v2, p0, Lcom/google/android/gms/internal/ao;->a:Lcom/google/android/gms/internal/al;

    invoke-static {v0, v2}, Lcom/google/android/gms/internal/am;->a(Lcom/google/android/gms/internal/am;Lcom/google/android/gms/internal/al;)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
