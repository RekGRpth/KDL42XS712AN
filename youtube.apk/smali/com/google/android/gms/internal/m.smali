.class public final Lcom/google/android/gms/internal/m;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lcom/google/android/gms/internal/as;

.field private b:Lcom/google/android/gms/ads/a;

.field private c:Lcom/google/android/gms/internal/g;

.field private d:[Lcom/google/android/gms/ads/c;

.field private e:Ljava/lang/String;

.field private f:Landroid/view/ViewGroup;

.field private g:Lcom/google/android/gms/ads/doubleclick/a;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/as;

    invoke-direct {v0}, Lcom/google/android/gms/internal/as;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/m;->a:Lcom/google/android/gms/internal/as;

    iput-object p1, p0, Lcom/google/android/gms/internal/m;->f:Landroid/view/ViewGroup;

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup;Landroid/util/AttributeSet;Z)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/as;

    invoke-direct {v0}, Lcom/google/android/gms/internal/as;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/m;->a:Lcom/google/android/gms/internal/as;

    iput-object p1, p0, Lcom/google/android/gms/internal/m;->f:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    :try_start_0
    new-instance v0, Lcom/google/android/gms/internal/af;

    invoke-direct {v0, v1, p2}, Lcom/google/android/gms/internal/af;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {v0, p3}, Lcom/google/android/gms/internal/af;->a(Z)[Lcom/google/android/gms/ads/c;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/internal/m;->d:[Lcom/google/android/gms/ads/c;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/af;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/m;->e:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/ac;

    iget-object v2, p0, Lcom/google/android/gms/internal/m;->d:[Lcom/google/android/gms/ads/c;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/ac;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/c;)V

    const-string v1, "Ads by Google"

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/internal/dn;->a(Landroid/view/ViewGroup;Lcom/google/android/gms/internal/ac;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v2, Lcom/google/android/gms/internal/ac;

    sget-object v3, Lcom/google/android/gms/ads/c;->a:Lcom/google/android/gms/ads/c;

    invoke-direct {v2, v1, v3}, Lcom/google/android/gms/internal/ac;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/c;)V

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v2, v1, v0}, Lcom/google/android/gms/internal/dn;->a(Landroid/view/ViewGroup;Lcom/google/android/gms/internal/ac;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/ads/c;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/m;->c:Lcom/google/android/gms/internal/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/m;->c:Lcom/google/android/gms/internal/g;

    invoke-interface {v0}, Lcom/google/android/gms/internal/g;->a()Lcom/google/android/gms/internal/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ac;->P()Lcom/google/android/gms/ads/c;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "Failed to get the current AdSize."

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/do;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/m;->d:[Lcom/google/android/gms/ads/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/m;->d:[Lcom/google/android/gms/ads/c;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/ads/a;)V
    .locals 2

    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/internal/m;->b:Lcom/google/android/gms/ads/a;

    iget-object v0, p0, Lcom/google/android/gms/internal/m;->c:Lcom/google/android/gms/internal/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/m;->c:Lcom/google/android/gms/internal/g;

    if-eqz p1, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/kb;

    invoke-direct {v0, p1}, Lcom/google/android/gms/internal/kb;-><init>(Lcom/google/android/gms/ads/a;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to set the AdListener."

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/do;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/ads/doubleclick/a;)V
    .locals 2

    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/internal/m;->g:Lcom/google/android/gms/ads/doubleclick/a;

    iget-object v0, p0, Lcom/google/android/gms/internal/m;->c:Lcom/google/android/gms/internal/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/m;->c:Lcom/google/android/gms/internal/g;

    if-eqz p1, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/c;

    invoke-direct {v0, p1}, Lcom/google/android/gms/internal/c;-><init>(Lcom/google/android/gms/ads/doubleclick/a;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to set the AppEventListener."

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/do;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/m;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The ad unit ID can only be set once on AdView."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/internal/m;->e:Ljava/lang/String;

    return-void
.end method

.method public final varargs a([Lcom/google/android/gms/ads/c;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/m;->d:[Lcom/google/android/gms/ads/c;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The ad size can only be set once on AdView."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/m;->b([Lcom/google/android/gms/ads/c;)V

    return-void
.end method

.method public final varargs b([Lcom/google/android/gms/ads/c;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/gms/internal/m;->d:[Lcom/google/android/gms/ads/c;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/m;->c:Lcom/google/android/gms/internal/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/m;->c:Lcom/google/android/gms/internal/g;

    new-instance v0, Lcom/google/android/gms/internal/ac;

    iget-object v1, p0, Lcom/google/android/gms/internal/m;->f:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/m;->d:[Lcom/google/android/gms/ads/c;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/ac;-><init>(Landroid/content/Context;[Lcom/google/android/gms/ads/c;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/internal/m;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to set the ad size."

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/do;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
