.class public final Lcom/google/android/gms/internal/aa;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/a;


# instance fields
.field public final eB:J

.field public final eC:I

.field public final eD:Ljava/util/List;

.field public final eE:Z

.field public final eF:Z

.field public final eG:Ljava/lang/String;

.field public final eH:Lcom/google/android/gms/internal/an;

.field public final eI:Landroid/location/Location;

.field public final extras:Landroid/os/Bundle;

.field public final tagForChildDirectedTreatment:I

.field public final versionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/a;

    invoke-direct {v0}, Lcom/google/android/gms/internal/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/aa;->CREATOR:Lcom/google/android/gms/internal/a;

    return-void
.end method

.method constructor <init>(IJLandroid/os/Bundle;ILjava/util/List;ZIZLjava/lang/String;Lcom/google/android/gms/internal/an;Landroid/location/Location;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/aa;->versionCode:I

    iput-wide p2, p0, Lcom/google/android/gms/internal/aa;->eB:J

    iput-object p4, p0, Lcom/google/android/gms/internal/aa;->extras:Landroid/os/Bundle;

    iput p5, p0, Lcom/google/android/gms/internal/aa;->eC:I

    iput-object p6, p0, Lcom/google/android/gms/internal/aa;->eD:Ljava/util/List;

    iput-boolean p7, p0, Lcom/google/android/gms/internal/aa;->eE:Z

    iput p8, p0, Lcom/google/android/gms/internal/aa;->tagForChildDirectedTreatment:I

    iput-boolean p9, p0, Lcom/google/android/gms/internal/aa;->eF:Z

    iput-object p10, p0, Lcom/google/android/gms/internal/aa;->eG:Ljava/lang/String;

    iput-object p11, p0, Lcom/google/android/gms/internal/aa;->eH:Lcom/google/android/gms/internal/an;

    iput-object p12, p0, Lcom/google/android/gms/internal/aa;->eI:Landroid/location/Location;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/k;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/internal/aa;->versionCode:I

    invoke-virtual {p2}, Lcom/google/android/gms/internal/k;->a()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lcom/google/android/gms/internal/aa;->eB:J

    invoke-virtual {p2}, Lcom/google/android/gms/internal/k;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/aa;->eC:I

    invoke-virtual {p2}, Lcom/google/android/gms/internal/k;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/android/gms/internal/aa;->eD:Ljava/util/List;

    invoke-virtual {p2, p1}, Lcom/google/android/gms/internal/k;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/aa;->eE:Z

    invoke-virtual {p2}, Lcom/google/android/gms/internal/k;->h()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/aa;->tagForChildDirectedTreatment:I

    invoke-virtual {p2}, Lcom/google/android/gms/internal/k;->d()Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/aa;->eI:Landroid/location/Location;

    const-class v0, Lcom/google/android/gms/ads/a/a/a;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/internal/k;->a(Ljava/lang/Class;)Lcom/google/android/gms/ads/a/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/a/a/a;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/ads/a/a/a;->a()Landroid/os/Bundle;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/google/android/gms/internal/aa;->extras:Landroid/os/Bundle;

    invoke-virtual {p2}, Lcom/google/android/gms/internal/k;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/aa;->eF:Z

    invoke-virtual {p2}, Lcom/google/android/gms/internal/k;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/aa;->eG:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/gms/internal/k;->g()Lcom/google/android/gms/ads/search/a;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Lcom/google/android/gms/internal/an;

    invoke-direct {v2, v0}, Lcom/google/android/gms/internal/an;-><init>(Lcom/google/android/gms/ads/search/a;)V

    :cond_0
    iput-object v2, p0, Lcom/google/android/gms/internal/aa;->eH:Lcom/google/android/gms/internal/an;

    return-void

    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0

    :cond_2
    move-object v0, v2

    goto :goto_1

    :cond_3
    move-object v0, v2

    goto :goto_2
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/a;->a(Lcom/google/android/gms/internal/aa;Landroid/os/Parcel;I)V

    return-void
.end method
