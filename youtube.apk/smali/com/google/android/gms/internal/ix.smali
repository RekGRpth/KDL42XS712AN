.class public final Lcom/google/android/gms/internal/ix;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/c;
.implements Lcom/google/android/gms/common/d;


# instance fields
.field private final a:Lcom/google/android/gms/c/b;

.field private b:Lcom/google/android/gms/internal/iz;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/c/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/ix;->a:Lcom/google/android/gms/c/b;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/ix;->b:Lcom/google/android/gms/internal/iz;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/ix;->c:Z

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/ix;->b:Lcom/google/android/gms/internal/iz;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/iz;->a(Z)V

    iget-boolean v0, p0, Lcom/google/android/gms/internal/ix;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/ix;->a:Lcom/google/android/gms/c/b;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/ix;->a:Lcom/google/android/gms/c/b;

    invoke-virtual {p1}, Lcom/google/android/gms/common/a;->c()Landroid/app/PendingIntent;

    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/ix;->c:Z

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/ix;->a:Lcom/google/android/gms/c/b;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/internal/iz;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/ix;->b:Lcom/google/android/gms/internal/iz;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/internal/ix;->c:Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/ix;->b:Lcom/google/android/gms/internal/iz;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/iz;->a(Z)V

    return-void
.end method

.method public final u_()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/ix;->b:Lcom/google/android/gms/internal/iz;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/iz;->a(Z)V

    iget-boolean v0, p0, Lcom/google/android/gms/internal/ix;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/ix;->a:Lcom/google/android/gms/c/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/ix;->a:Lcom/google/android/gms/c/b;

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/gms/internal/ix;->c:Z

    return-void
.end method
