.class public final Lcom/google/android/gms/internal/js;
.super Ljava/lang/Object;


# instance fields
.field private final a:[B

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method private constructor <init>([BII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/gms/internal/js;->f:I

    const/16 v0, 0x40

    iput v0, p0, Lcom/google/android/gms/internal/js;->g:I

    const/high16 v0, 0x4000000

    iput v0, p0, Lcom/google/android/gms/internal/js;->h:I

    iput-object p1, p0, Lcom/google/android/gms/internal/js;->a:[B

    iput p2, p0, Lcom/google/android/gms/internal/js;->b:I

    add-int v0, p2, p3

    iput v0, p0, Lcom/google/android/gms/internal/js;->c:I

    iput p2, p0, Lcom/google/android/gms/internal/js;->d:I

    return-void
.end method

.method public static a([BII)Lcom/google/android/gms/internal/js;
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/js;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/internal/js;-><init>([BII)V

    return-object v0
.end method

.method private c(I)V
    .locals 2

    if-gez p1, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/lm;->gR()Lcom/google/android/gms/internal/lm;

    move-result-object v0

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/android/gms/internal/js;->d:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/google/android/gms/internal/js;->f:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/gms/internal/js;->f:I

    iget v1, p0, Lcom/google/android/gms/internal/js;->d:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/js;->c(I)V

    invoke-static {}, Lcom/google/android/gms/internal/lm;->gQ()Lcom/google/android/gms/internal/lm;

    move-result-object v0

    throw v0

    :cond_1
    iget v0, p0, Lcom/google/android/gms/internal/js;->c:I

    iget v1, p0, Lcom/google/android/gms/internal/js;->d:I

    sub-int/2addr v0, v1

    if-gt p1, v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/internal/js;->d:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/gms/internal/js;->d:I

    return-void

    :cond_2
    invoke-static {}, Lcom/google/android/gms/internal/lm;->gQ()Lcom/google/android/gms/internal/lm;

    move-result-object v0

    throw v0
.end method

.method private e()I
    .locals 3

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v0

    if-ltz v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    and-int/lit8 v0, v0, 0x7f

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v1

    if-ltz v1, :cond_2

    shl-int/lit8 v1, v1, 0x7

    or-int/2addr v0, v1

    goto :goto_0

    :cond_2
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0x7

    or-int/2addr v0, v1

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v1

    if-ltz v1, :cond_3

    shl-int/lit8 v1, v1, 0xe

    or-int/2addr v0, v1

    goto :goto_0

    :cond_3
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0xe

    or-int/2addr v0, v1

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v1

    if-ltz v1, :cond_4

    shl-int/lit8 v1, v1, 0x15

    or-int/2addr v0, v1

    goto :goto_0

    :cond_4
    and-int/lit8 v1, v1, 0x7f

    shl-int/lit8 v1, v1, 0x15

    or-int/2addr v0, v1

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v1

    shl-int/lit8 v2, v1, 0x1c

    or-int/2addr v0, v2

    if-gez v1, :cond_0

    const/4 v1, 0x0

    :goto_1
    const/4 v2, 0x5

    if-ge v1, v2, :cond_5

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v2

    if-gez v2, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    invoke-static {}, Lcom/google/android/gms/internal/lm;->gS()Lcom/google/android/gms/internal/lm;

    move-result-object v0

    throw v0
.end method

.method private f()B
    .locals 3

    iget v0, p0, Lcom/google/android/gms/internal/js;->d:I

    iget v1, p0, Lcom/google/android/gms/internal/js;->c:I

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/lm;->gQ()Lcom/google/android/gms/internal/lm;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/js;->a:[B

    iget v1, p0, Lcom/google/android/gms/internal/js;->d:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/gms/internal/js;->d:I

    aget-byte v0, v0, v1

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/gms/internal/js;->d:I

    iget v2, p0, Lcom/google/android/gms/internal/js;->c:I

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    iput v0, p0, Lcom/google/android/gms/internal/js;->e:I

    :goto_1
    return v0

    :cond_0
    move v1, v0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->e()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/js;->e:I

    iget v0, p0, Lcom/google/android/gms/internal/js;->e:I

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/android/gms/internal/lm;->gT()Lcom/google/android/gms/internal/lm;

    move-result-object v0

    throw v0

    :cond_2
    iget v0, p0, Lcom/google/android/gms/internal/js;->e:I

    goto :goto_1
.end method

.method public final a(I)V
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/js;->e:I

    if-eq v0, p1, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/lm;->gU()Lcom/google/android/gms/internal/lm;

    move-result-object v0

    throw v0

    :cond_0
    return-void
.end method

.method public final b()I
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->e()I

    move-result v0

    return v0
.end method

.method public final b(I)Z
    .locals 14

    const/4 v0, 0x1

    const-wide/16 v12, 0xff

    invoke-static {p1}, Lcom/google/android/gms/internal/jw;->a(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-static {}, Lcom/google/android/gms/internal/lm;->gV()Lcom/google/android/gms/internal/lm;

    move-result-object v0

    throw v0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->e()I

    :goto_0
    return v0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v1

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v2

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v3

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v4

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v5

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v6

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v7

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v8

    int-to-long v9, v1

    and-long/2addr v9, v12

    int-to-long v1, v2

    and-long/2addr v1, v12

    const/16 v11, 0x8

    shl-long/2addr v1, v11

    or-long/2addr v1, v9

    int-to-long v9, v3

    and-long/2addr v9, v12

    const/16 v3, 0x10

    shl-long/2addr v9, v3

    or-long/2addr v1, v9

    int-to-long v3, v4

    and-long/2addr v3, v12

    const/16 v9, 0x18

    shl-long/2addr v3, v9

    or-long/2addr v1, v3

    int-to-long v3, v5

    and-long/2addr v3, v12

    const/16 v5, 0x20

    shl-long/2addr v3, v5

    or-long/2addr v1, v3

    int-to-long v3, v6

    and-long/2addr v3, v12

    const/16 v5, 0x28

    shl-long/2addr v3, v5

    or-long/2addr v1, v3

    int-to-long v3, v7

    and-long/2addr v3, v12

    const/16 v5, 0x30

    shl-long/2addr v3, v5

    or-long/2addr v1, v3

    int-to-long v3, v8

    and-long/2addr v3, v12

    const/16 v5, 0x38

    shl-long/2addr v3, v5

    or-long/2addr v1, v3

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->e()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/internal/js;->c(I)V

    goto :goto_0

    :cond_0
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/gms/internal/js;->a()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/internal/js;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/internal/jw;->b(I)I

    move-result v1

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/jw;->a(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/internal/js;->a(I)V

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_5
    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v1

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v2

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v3

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v4

    and-int/lit16 v1, v1, 0xff

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    and-int/lit16 v2, v3, 0xff

    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    and-int/lit16 v2, v4, 0xff

    shl-int/lit8 v2, v2, 0x18

    or-int/2addr v1, v2

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final c()Ljava/lang/String;
    .locals 6

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->e()I

    move-result v1

    iget v0, p0, Lcom/google/android/gms/internal/js;->c:I

    iget v2, p0, Lcom/google/android/gms/internal/js;->d:I

    sub-int/2addr v0, v2

    if-gt v1, v0, :cond_0

    if-lez v1, :cond_0

    new-instance v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/internal/js;->a:[B

    iget v3, p0, Lcom/google/android/gms/internal/js;->d:I

    const-string v4, "UTF-8"

    invoke-direct {v0, v2, v3, v1, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/internal/js;->d:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/gms/internal/js;->d:I

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    if-gez v1, :cond_1

    invoke-static {}, Lcom/google/android/gms/internal/lm;->gR()Lcom/google/android/gms/internal/lm;

    move-result-object v0

    throw v0

    :cond_1
    iget v2, p0, Lcom/google/android/gms/internal/js;->d:I

    add-int/2addr v2, v1

    iget v3, p0, Lcom/google/android/gms/internal/js;->f:I

    if-le v2, v3, :cond_2

    iget v0, p0, Lcom/google/android/gms/internal/js;->f:I

    iget v1, p0, Lcom/google/android/gms/internal/js;->d:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/js;->c(I)V

    invoke-static {}, Lcom/google/android/gms/internal/lm;->gQ()Lcom/google/android/gms/internal/lm;

    move-result-object v0

    throw v0

    :cond_2
    iget v2, p0, Lcom/google/android/gms/internal/js;->c:I

    iget v3, p0, Lcom/google/android/gms/internal/js;->d:I

    sub-int/2addr v2, v3

    if-gt v1, v2, :cond_3

    new-array v2, v1, [B

    iget-object v3, p0, Lcom/google/android/gms/internal/js;->a:[B

    iget v4, p0, Lcom/google/android/gms/internal/js;->d:I

    const/4 v5, 0x0

    invoke-static {v3, v4, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v3, p0, Lcom/google/android/gms/internal/js;->d:I

    add-int/2addr v1, v3

    iput v1, p0, Lcom/google/android/gms/internal/js;->d:I

    const-string v1, "UTF-8"

    invoke-direct {v0, v2, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/google/android/gms/internal/lm;->gQ()Lcom/google/android/gms/internal/lm;

    move-result-object v0

    throw v0
.end method

.method public final d()J
    .locals 6

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    :goto_0
    const/16 v3, 0x40

    if-ge v2, v3, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/internal/js;->f()B

    move-result v3

    and-int/lit8 v4, v3, 0x7f

    int-to-long v4, v4

    shl-long/2addr v4, v2

    or-long/2addr v0, v4

    and-int/lit16 v3, v3, 0x80

    if-nez v3, :cond_0

    const/4 v2, 0x1

    ushr-long v2, v0, v2

    const-wide/16 v4, 0x1

    and-long/2addr v0, v4

    neg-long v0, v0

    xor-long/2addr v0, v2

    return-wide v0

    :cond_0
    add-int/lit8 v2, v2, 0x7

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/android/gms/internal/lm;->gS()Lcom/google/android/gms/internal/lm;

    move-result-object v0

    throw v0
.end method
