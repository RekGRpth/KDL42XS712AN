.class public Lcom/google/android/gms/internal/ih;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final Gv:Z

.field private final Gw:Z

.field private final jE:I

.field private final lr:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/id;

    invoke-direct {v0}, Lcom/google/android/gms/internal/id;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ih;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;ZZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/ih;->jE:I

    iput-object p2, p0, Lcom/google/android/gms/internal/ih;->lr:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/gms/internal/ih;->Gv:Z

    iput-boolean p4, p0, Lcom/google/android/gms/internal/ih;->Gw:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/ih;->jE:I

    iput-object p1, p0, Lcom/google/android/gms/internal/ih;->lr:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/google/android/gms/internal/ih;->Gv:Z

    iput-boolean p3, p0, Lcom/google/android/gms/internal/ih;->Gw:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ih;->lr:Ljava/lang/String;

    return-object v0
.end method

.method getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/ih;->jE:I

    return v0
.end method

.method public isOptedInForAppUsageCollection()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/ih;->Gv:Z

    return v0
.end method

.method public isOptedInForCallAndSmsLogCollection()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/ih;->Gw:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/gms/internal/id;->a(Lcom/google/android/gms/internal/ih;Landroid/os/Parcel;)V

    return-void
.end method
