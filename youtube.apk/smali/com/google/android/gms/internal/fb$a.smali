.class public final Lcom/google/android/gms/internal/fb$a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/gp;


# instance fields
.field final un:Ljava/lang/String;

.field final uo:I

.field final versionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/gp;

    invoke-direct {v0}, Lcom/google/android/gms/internal/gp;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/fb$a;->CREATOR:Lcom/google/android/gms/internal/gp;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/fb$a;->versionCode:I

    iput-object p2, p0, Lcom/google/android/gms/internal/fb$a;->un:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/gms/internal/fb$a;->uo:I

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/fb$a;->versionCode:I

    iput-object p1, p0, Lcom/google/android/gms/internal/fb$a;->un:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/gms/internal/fb$a;->uo:I

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/fb$a;->CREATOR:Lcom/google/android/gms/internal/gp;

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/fb$a;->CREATOR:Lcom/google/android/gms/internal/gp;

    invoke-static {p0, p1}, Lcom/google/android/gms/internal/gp;->a(Lcom/google/android/gms/internal/fb$a;Landroid/os/Parcel;)V

    return-void
.end method
