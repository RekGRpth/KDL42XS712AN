.class public final Lcom/google/android/gms/internal/ce;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/cn;


# instance fields
.field public final adUnitId:Ljava/lang/String;

.field public final applicationInfo:Landroid/content/pm/ApplicationInfo;

.field public final en:Lcom/google/android/gms/internal/cz;

.field public final eq:Lcom/google/android/gms/internal/ac;

.field public final ht:Landroid/os/Bundle;

.field public final hu:Lcom/google/android/gms/internal/aa;

.field public final hv:Landroid/content/pm/PackageInfo;

.field public final hw:Ljava/lang/String;

.field public final hx:Ljava/lang/String;

.field public final hy:Ljava/lang/String;

.field public final versionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/cn;

    invoke-direct {v0}, Lcom/google/android/gms/internal/cn;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/ce;->CREATOR:Lcom/google/android/gms/internal/cn;

    return-void
.end method

.method constructor <init>(ILandroid/os/Bundle;Lcom/google/android/gms/internal/aa;Lcom/google/android/gms/internal/ac;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Landroid/content/pm/PackageInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/cz;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/ce;->versionCode:I

    iput-object p2, p0, Lcom/google/android/gms/internal/ce;->ht:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/google/android/gms/internal/ce;->hu:Lcom/google/android/gms/internal/aa;

    iput-object p4, p0, Lcom/google/android/gms/internal/ce;->eq:Lcom/google/android/gms/internal/ac;

    iput-object p5, p0, Lcom/google/android/gms/internal/ce;->adUnitId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/internal/ce;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iput-object p7, p0, Lcom/google/android/gms/internal/ce;->hv:Landroid/content/pm/PackageInfo;

    iput-object p8, p0, Lcom/google/android/gms/internal/ce;->hw:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/gms/internal/ce;->hx:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/gms/internal/ce;->hy:Ljava/lang/String;

    iput-object p11, p0, Lcom/google/android/gms/internal/ce;->en:Lcom/google/android/gms/internal/cz;

    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;Lcom/google/android/gms/internal/aa;Lcom/google/android/gms/internal/ac;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Landroid/content/pm/PackageInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/cz;)V
    .locals 12

    const/4 v1, 0x1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/internal/ce;-><init>(ILandroid/os/Bundle;Lcom/google/android/gms/internal/aa;Lcom/google/android/gms/internal/ac;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Landroid/content/pm/PackageInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/cz;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/cm;Ljava/lang/String;)V
    .locals 11

    iget-object v1, p1, Lcom/google/android/gms/internal/cm;->a:Landroid/os/Bundle;

    iget-object v2, p1, Lcom/google/android/gms/internal/cm;->b:Lcom/google/android/gms/internal/aa;

    iget-object v3, p1, Lcom/google/android/gms/internal/cm;->c:Lcom/google/android/gms/internal/ac;

    iget-object v4, p1, Lcom/google/android/gms/internal/cm;->d:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/gms/internal/cm;->e:Landroid/content/pm/ApplicationInfo;

    iget-object v6, p1, Lcom/google/android/gms/internal/cm;->f:Landroid/content/pm/PackageInfo;

    iget-object v8, p1, Lcom/google/android/gms/internal/cm;->g:Ljava/lang/String;

    iget-object v9, p1, Lcom/google/android/gms/internal/cm;->h:Ljava/lang/String;

    iget-object v10, p1, Lcom/google/android/gms/internal/cm;->i:Lcom/google/android/gms/internal/cz;

    move-object v0, p0

    move-object v7, p2

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/internal/ce;-><init>(Landroid/os/Bundle;Lcom/google/android/gms/internal/aa;Lcom/google/android/gms/internal/ac;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Landroid/content/pm/PackageInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/cz;)V

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/cn;->a(Lcom/google/android/gms/internal/ce;Landroid/os/Parcel;I)V

    return-void
.end method
