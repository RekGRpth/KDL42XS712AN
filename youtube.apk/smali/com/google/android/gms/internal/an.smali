.class public final Lcom/google/android/gms/internal/an;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/n;


# instance fields
.field public final backgroundColor:I

.field public final fd:I

.field public final fe:I

.field public final ff:I

.field public final fg:I

.field public final fh:I

.field public final fi:I

.field public final fj:I

.field public final fk:Ljava/lang/String;

.field public final fl:I

.field public final fm:Ljava/lang/String;

.field public final fn:I

.field public final fo:I

.field public final fp:Ljava/lang/String;

.field public final versionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/n;

    invoke-direct {v0}, Lcom/google/android/gms/internal/n;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/an;->CREATOR:Lcom/google/android/gms/internal/n;

    return-void
.end method

.method constructor <init>(IIIIIIIIILjava/lang/String;ILjava/lang/String;IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/an;->versionCode:I

    iput p2, p0, Lcom/google/android/gms/internal/an;->fd:I

    iput p3, p0, Lcom/google/android/gms/internal/an;->backgroundColor:I

    iput p4, p0, Lcom/google/android/gms/internal/an;->fe:I

    iput p5, p0, Lcom/google/android/gms/internal/an;->ff:I

    iput p6, p0, Lcom/google/android/gms/internal/an;->fg:I

    iput p7, p0, Lcom/google/android/gms/internal/an;->fh:I

    iput p8, p0, Lcom/google/android/gms/internal/an;->fi:I

    iput p9, p0, Lcom/google/android/gms/internal/an;->fj:I

    iput-object p10, p0, Lcom/google/android/gms/internal/an;->fk:Ljava/lang/String;

    iput p11, p0, Lcom/google/android/gms/internal/an;->fl:I

    iput-object p12, p0, Lcom/google/android/gms/internal/an;->fm:Ljava/lang/String;

    iput p13, p0, Lcom/google/android/gms/internal/an;->fn:I

    iput p14, p0, Lcom/google/android/gms/internal/an;->fo:I

    iput-object p15, p0, Lcom/google/android/gms/internal/an;->fp:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/ads/search/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/an;->versionCode:I

    invoke-virtual {p1}, Lcom/google/android/gms/ads/search/a;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/an;->fd:I

    invoke-virtual {p1}, Lcom/google/android/gms/ads/search/a;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/an;->backgroundColor:I

    invoke-virtual {p1}, Lcom/google/android/gms/ads/search/a;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/an;->fe:I

    invoke-virtual {p1}, Lcom/google/android/gms/ads/search/a;->d()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/an;->ff:I

    invoke-virtual {p1}, Lcom/google/android/gms/ads/search/a;->e()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/an;->fg:I

    invoke-virtual {p1}, Lcom/google/android/gms/ads/search/a;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/an;->fh:I

    invoke-virtual {p1}, Lcom/google/android/gms/ads/search/a;->g()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/an;->fi:I

    invoke-virtual {p1}, Lcom/google/android/gms/ads/search/a;->h()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/an;->fj:I

    invoke-virtual {p1}, Lcom/google/android/gms/ads/search/a;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/an;->fk:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/ads/search/a;->j()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/an;->fl:I

    invoke-virtual {p1}, Lcom/google/android/gms/ads/search/a;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/an;->fm:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/ads/search/a;->l()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/an;->fn:I

    invoke-virtual {p1}, Lcom/google/android/gms/ads/search/a;->m()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/an;->fo:I

    invoke-virtual {p1}, Lcom/google/android/gms/ads/search/a;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/an;->fp:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/gms/internal/n;->a(Lcom/google/android/gms/internal/an;Landroid/os/Parcel;)V

    return-void
.end method
