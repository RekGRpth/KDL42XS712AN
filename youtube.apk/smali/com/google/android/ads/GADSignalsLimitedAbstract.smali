.class public abstract Lcom/google/android/ads/GADSignalsLimitedAbstract;
.super Lcom/google/android/ads/r;
.source "SourceFile"


# static fields
.field static c:Z

.field private static d:Ljava/lang/reflect/Method;

.field private static e:Ljava/lang/reflect/Method;

.field private static f:Ljava/lang/reflect/Method;

.field private static g:Ljava/lang/reflect/Method;

.field private static h:Ljava/lang/reflect/Method;

.field private static i:Ljava/lang/reflect/Method;

.field private static j:Ljava/lang/String;

.field private static k:J

.field private static l:Lcom/google/android/ads/Obfuscator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/google/android/ads/GADSignalsLimitedAbstract;->k:J

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/ads/GADSignalsLimitedAbstract;->c:Z

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/ads/u;Lcom/google/android/ads/v;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/ads/r;-><init>(Landroid/content/Context;Lcom/google/android/ads/u;Lcom/google/android/ads/v;)V

    return-void
.end method

.method private static a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/ads/GADSignalsLimitedAbstract;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>()V

    throw v0

    :cond_0
    sget-object v0, Lcom/google/android/ads/GADSignalsLimitedAbstract;->j:Ljava/lang/String;

    return-object v0
.end method

.method private static a([BLjava/lang/String;)Ljava/lang/String;
    .locals 3

    :try_start_0
    new-instance v0, Ljava/lang/String;

    sget-object v1, Lcom/google/android/ads/GADSignalsLimitedAbstract;->l:Lcom/google/android/ads/Obfuscator;

    invoke-virtual {v1, p0, p1}, Lcom/google/android/ads/Obfuscator;->a([BLjava/lang/String;)[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/ads/Obfuscator$ObfuscatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected static declared-synchronized a(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/ads/u;)V
    .locals 12

    const-class v1, Lcom/google/android/ads/GADSignalsLimitedAbstract;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/google/android/ads/GADSignalsLimitedAbstract;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    new-instance v0, Lcom/google/android/ads/Obfuscator;

    const/4 v2, 0x0

    invoke-direct {v0, p2, v2}, Lcom/google/android/ads/Obfuscator;-><init>(Lcom/google/android/ads/u;Ljava/security/SecureRandom;)V

    sput-object v0, Lcom/google/android/ads/GADSignalsLimitedAbstract;->l:Lcom/google/android/ads/Obfuscator;

    sput-object p0, Lcom/google/android/ads/GADSignalsLimitedAbstract;->j:Ljava/lang/String;
    :try_end_1
    .catch Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    sget-object v0, Lcom/google/android/ads/GADSignalsLimitedAbstract;->l:Lcom/google/android/ads/Obfuscator;

    const-string v2, "HsNqdSi3oaahMJ1qB578bN3ZzyO5qtAaBeV+hOcy37A="

    invoke-virtual {v0, v2}, Lcom/google/android/ads/Obfuscator;->a(Ljava/lang/String;)[B

    move-result-object v2

    sget-object v0, Lcom/google/android/ads/GADSignalsLimitedAbstract;->l:Lcom/google/android/ads/Obfuscator;

    const-string v3, "BjrGzPIDJ8UlyQmp0JmsnYV2b+8E+fuYzo+u5+QrHFPUTZahvlPOO3k5YF2IcN6MMS5QcQnUblVH/C0KvBN7UQi1BJv/zc1Ko2z1MqaZfElSMMSP97gbpSOb1Bd41res+EjrBWrntNXwK7Q2tSc7ypkuzoE8yVCmZB4LfPwr2vhcaRaHgUf1yRM/6/FazMO5tY/ZCNGljsYC/x1O1m7T1+b09PxrPQ0DCVRpJK1rwFvDA2M6Q/sODNB53UtOLWj16CFSCb0mIvP1wabLQDkj051dVVrewfo8bP6LfCUn+agVi6hv5Nxf70GtbYBjLHu6DUQrToQ6kIC5NkrexyRUuI+FeTF3XhRJuozVNnDxOVfVgXmEkfNHa3XOL1PZ4WA8vnZfX5gmEAC2VixPZJdnmZhQSn+dn/cFwUXU00kw5LolqKYLqIZ731bydVmeikH0D11QvzOtY2bK7Eo/Mez0WPWMDyCHhuvoBDHPInGSc9BIKHNSk9DsgqH2lTURj9Tt9Y+RkNFMg5c3Gr0poQqreknOfdNe/nCvuTCMXVKfLaADY/cebX6dDYPFJUVTQBpR83xEbz5flE14AVN8xt3zZr/mqwZQ93epZTGafD648yI6gUpwB9VSpeXjvhA9rUclq4T4SbvdeDS+ketoNKERU4Ehg3med4RB9524sZUtd6NVaq3kImakvcEb+AKx1NDmeqJY4wEO2NeISopvd5XI0l2zLYMiAFfVrHr6DYfy8Tkh99LxmA8RHSWJyWpsb+o892yB3L0x4qv/PhunXiJ2nk6FFBeFHWNU5uKlE2bwje+VUXPYSTI/Nx5Q5oUlATw6ewvW/0fvtaeW84Xbx70n56eZZsUsjcHAtw1MlOjhoRQsErbJv392f35Zp2W2ByDtYSEzd2zqVjCnGsb2EYYSwlpMKdJXEbs0tlU/OM1kWRU1O/wJlPVHNDzLJ93C6PcN+j6BA0r1vQzprx0gwnr+/4NR/wOrSBg3IOy4ttmy6+b0NybBRXPqvK6F+OxZLo7arh3CHRLZaxBr76tfJl+ni/i1feManvlZvvXtpyKb5c7mgC5I29BOWnfJaHnEuOD09AxIaYNf3LTe0AVJwMdZa/hXXQFM6Xh6B6++TjgbdjwffdncZb4oOZrH6s2M9MG4Fj05oKd1xHtl+uXEAj162jaurRWgCxd33jmzCVXScI/XSYQ+YYMtLSlRMCKxP2tR1O8qfnnUQvdN3lwo9HxFAh46ZyOgJo1pp54lZWKxHmB4W1l1cOyfPRtvuYwLEv5N9LNzws8TiqgxungZEhNy/V78U8/BjIcPq87Btsq6BLQ4Fj/9Py2tE5FAZFNpCujNAoRd/jvBvBOyqhdPPY/7VG0PjWEvyU6wT1vjJfjrraJTyukK7q/gBfOvXfYNN7UkSOWtHp75oLwwOAP319rHxrx+UKywFWN3Ru71b1xQb08eP3vHwQGP+FMPrwbXq9R5J2mHfyTd/GE7ZsXV+lIsuLNTiThSQaK9OLD5Uq9My4F/6hrGYLnEbovNrYZGPikUwT+lhTxhO1rHxJmoW8/HM+2Y+3DNfQpwdxoATbXXx/SIIAewESf6EoZlNFwJ5ciC/TA5dexWztCH7D/eranLcQ60ZYS+BUwuMqxnUpUcFKMBQQ7g477jWySA1FUPI0G5aDE/vpWjsyoDsllzn3vRQz1V0wgwi3QvMJEfs8Eo0ZRUasBTCewWclNM+G8QDrc77Sc1arFGXXFL17CudcuLM6XYGfWVQe4xtqg6QSaFeqG3oGnKnR/VbZ8rUAunoABq+WIDDKzwqmPSIBHF6d1T4cjGiXEpQ1Hujf2gbN7KEWX2I4incvVQ0vE3cJ6ps5v7FZNaF/CIN/teQFkKwN4cpazL4hsJDcFXDIw9mXXIhZx3sIW9dTaQBTpoBZl3VoHrrd1SZZaSzh6fyLffRHTk0p1jX+SUw87koASzdMzpRGB4RuXsTMWSJc2tjucJGnmuIOQIFH/TpmhWT+Q3ab3HupLTIhH+JbcuZQYRYYnfp0BCc+NHu+nmYrbEh6naDFIA8pDGgm8bDRZQTsvdBgg5L1StZVJjzj3lhkSWJS6sHfBbTxUnyWw1154+mpiiurRjQF78GkUXSIGt38XV34Rb32HDSZYf0y39uDW0TBAPwXjUb36kMh9o1SegvMm/Na3lBn0kDSJLdwKruTD4QNHcMckNAa4nhsiEjav8pQWUvbyj9Wq9VEK/Ql3deFxsGeKNiGgeEB7+jlXFyfWHE4ARp2wgOLTZO/rNIBkjOxZ1eulTgj8FsKROSi53uXL7SqGLOMYBRuyRVPrYEmrXui3F5V64KMIHWd66PyRMm/jyXkjF136wARLhYRdxBeFDtIBRXQXG6Am91WGvjPPRQUvND1YzjITGnihf0XbZGveygzk9G5osDdGJfS+Sk14i2p3hD47yOES56NMR9KjVV80EGZOLZc0dzz70o/Y9rObDDCDeOLImGNDLqnzIXAtxGevzwlinOwhKC1voDIABVHuiu6tY+AahYq18DI7PxSF2Na1VEzNERCyOOZpf21EJ4brYto780ueq73q163AB2Uae4U5Dnrs5bcZvvFTnxwY9m9IrVK1J6W9OsTHAyYHxg1c0eAPC+2xoyzP6vrV2hkIOEAvDoTVQ7Y8gnL/MM8A7ZCk8pSgTl88YRWXxBVAfSZGaP2pVyFUfsQ7VOWQVhkceo8iwPIwbmO+If+ZHnJ9RtUGlU0VkAETavDz/lP/la/PZ0Lh0ne60pzxKdzZr5TJE4XWwlW0REAZeZ65uk3XIORP/vTw5V0nG5nWnqiEBtolOSASYUQOfUe3uzuiuXN5pkSvfsnA/bWnrHY1KWxiJYt224WY71sbestiQ9pRMMg8rI7T8EzIk3TUir6Ji/c+MDFYhpyQNzWre0aw+Vk/VqsblL+8z+Gu2rXJrU+R6VvABW25RCrAdMzq3B8V8v1tgUAV709xkXmYoPwd8OLTP8d0mXAlSs7UM7DeLcn5Rk7XNwvrVMtgLHwLt9U1CnTCzNTpXYXIRuiUqcc2t4HacI3f1L4/GvW3cD4M4wCr3bpY0p/14KeSrKf7hN+DGRJ+V/2XQidvqkKn3Fq5W2XCNyn/guyJFCuSgoX7jcTsLs7SHw72Mcp4K2hw9z/RgfkxcuVCPdQxiRq/dC3U9lr0cuez/cDeftesHM5dQXfJyx9aONGpMrAFgUxSrQnwelRNqlwOX++4t48pKNNdPQMZPrz18lI687bEYQu5Z486JuNn6bLI9kKUSdrhVE7hgAqaPSZeTCk9ExAPCmcm5nCwT37zEVIJXb7xXXyku2cAIH8iG38GwDLBHxwWcJ0aWdy/Rqb76qpHdfXHddu/YhbNRe4a6u4t22I8I2nP/3YKJwAHQysqi22ygqGQThtexg2UpUnMkx5ttsS3FYBh1H3krTVjM5XpiaBUABYAYkcYtgKbXCKi7hCEkHftBNiTjJGRODU64Bye0NfTEqkh99L2VNc6aWegjKcVY6A/p/8NDfv85pwEYsx4+pPZ7tQOmmcJW+6WMcqSJFY7hjgSaxgRSGfwRFKrqYZzBD7TiN6gjj9wzkX6LovI5pHabVvSKodvQz9ecdsVxK3dwfGlr9EEDcKYcWY+Ty4povuSuylc3PrE6E6N20Nkj+w/0b7QTjRgXau0BlnSb2pu3U0y1wxp2KMwrT5VZPH4Rf8RkLV7PsGPKaRnKJhRLhxvsy0HH8svWgEPcoxuF1FMiHO/soW94ae/4gm4l/EcvGuSKQoS3eYGe8GfN0j/A9G+S49ftjFc2CJ+gaXF38v4eisrhqhh9KnnVBL50RufRdEyLjf1RaKW50Mqw6evS8zAAWGwhOQ3NwbkEYQ0LxQiiAcjXyf5FX/VM/6DVYbI39exwL2HOMWnQ2FHL1rf7mFStfpN4XOk6THEifvxl5H36LWzdQMPC8xB5bbMCo7FE2EtwEJ6soGVvBnUlDn8RDY0E4i0vmfZuXzzya2bD4DVWBLi4QKcYDOhLpfwwRJ1gwRt4C760QZACkrzTaL619S206P+r6JhGo10CySmh+D7SyP4KLbLD8+kE4BBBcyuR3o2xKgMQs/uh7xYd2zKxMNYT56XOx0A1esvwLMOiogEvWXv/FCldzIQ5XtAd/uDWAdW7+7Ukl+ro1NxXXiXne1kgJ/v56Yjv/MbUyzEi06uiHuGR/7YYAfpHFZzafmuX1EVrvmSpYVfWG5qhcHFft2wuyn6BWURegXRDyA1E/lg0Ta30r4sq0aKjGPlXNnsIUI1SiGrm6wZuzlsfqkr+hWNTVWHwsem+xhwlukurmCQ/lir+9wOHcMozZs3PDb9ymTYsqLNu6n13Tjj7v4VWxaysTdiFQl2UegkoTPOVolO72wUSW7RoP1qPKCnR7DAbTEMeNuMHjMwv"

    invoke-virtual {v0, v2, v3}, Lcom/google/android/ads/Obfuscator;->a([BLjava/lang/String;)[B

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "dex"

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v4}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>()V

    throw v0
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lcom/google/android/ads/Obfuscator$ObfuscatorException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v2, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v2, v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catch Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_1
    move-exception v0

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    :try_start_4
    const-string v4, "ads"

    const-string v5, ".jar"

    invoke-static {v4, v5, v0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v4

    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v6, 0x0

    array-length v7, v3

    invoke-virtual {v5, v3, v6, v7}, Ljava/io/FileOutputStream;->write([BII)V

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    new-instance v3, Ldalvik/system/DexClassLoader;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-direct {v3, v5, v6, v7, v8}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    const-string v5, "M7+3J745Gn4xpUgFQW2Br/MkMqyj0sfwCpOmVPZsRHV7cmHGtcMDm237UJD76Z75"

    invoke-static {v2, v5}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    const-string v6, "uanf7tIdkZt1rkDmRp+YXaSmEFCdT+UYRsbo7RzIwPEVE8mDK4sw9RpvJvOfP0ux"

    invoke-static {v2, v6}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const-string v7, "DTHI6MG5Tw9Bm0XfM04+6aQ0W6kXU0TKHFlJbJw/5zYJ8v83LWD9YVS+XrZ9fQ7R"

    invoke-static {v2, v7}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    const-string v8, "kQyz3dB+6CkJ9E149f9jKLoLgH8RmNhjqWJyEMFqYZIbnKecLMqMV6DTy9y8A7v1"

    invoke-static {v2, v8}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    const-string v9, "jxeJzDN9sUJwdlEoqXDTeox07O1XHMJgsT8Dr6CQYFNqGtczcWf3bS5VxVokwrUF"

    invoke-static {v2, v9}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v9

    const-string v10, "nGaQVQDR5+6vh7e+VoNb/P+/hktmV8zgOLvt/DHMAxMXC/B9XMzcmWQl/hyyolSQ"

    invoke-static {v2, v10}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const-string v10, "FiJ/ai6UrZpMzOYv/4QLJAGUMYgHzKAgLUWY7LuoK3o="

    invoke-static {v2, v10}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Class;

    invoke-virtual {v5, v10, v11}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/android/ads/GADSignalsLimitedAbstract;->d:Ljava/lang/reflect/Method;

    const-string v5, "KgFWb0HLIDnIlTrgtCqulbOveusdOe1kID1hQDE3B0I="

    invoke-static {v2, v5}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Class;

    invoke-virtual {v6, v5, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/android/ads/GADSignalsLimitedAbstract;->e:Ljava/lang/reflect/Method;

    const-string v5, "G3ELUJagtx0EKR5P516SvL/SkGX3lFHDlvNeLyuU+SY="

    invoke-static {v2, v5}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v10, 0x0

    const-class v11, Landroid/content/Context;

    aput-object v11, v6, v10

    invoke-virtual {v7, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/android/ads/GADSignalsLimitedAbstract;->f:Ljava/lang/reflect/Method;

    const-string v5, "vZN0MCOiJ0mVyXODND2OiBPHqlm23wpyGHWPIubBqR8="

    invoke-static {v2, v5}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v10, Landroid/view/MotionEvent;

    aput-object v10, v6, v7

    const/4 v7, 0x1

    const-class v10, Landroid/util/DisplayMetrics;

    aput-object v10, v6, v7

    invoke-virtual {v8, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/android/ads/GADSignalsLimitedAbstract;->g:Ljava/lang/reflect/Method;

    const-string v5, "968zt1kP6qxceFwhc8hDAcvE+9VVdfW2ZB325MvLxRw="

    invoke-static {v2, v5}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/content/Context;

    aput-object v8, v6, v7

    invoke-virtual {v9, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    sput-object v5, Lcom/google/android/ads/GADSignalsLimitedAbstract;->h:Ljava/lang/reflect/Method;

    const-string v5, "VYAmaRrQjOR3u2b3fvxT0SkGGwXHdIOyrVYxukVAjco="

    invoke-static {v2, v5}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Landroid/content/Context;

    aput-object v7, v5, v6

    invoke-virtual {v3, v2, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lcom/google/android/ads/GADSignalsLimitedAbstract;->i:Ljava/lang/reflect/Method;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    new-instance v3, Ljava/io/File;

    const-string v4, ".jar"

    const-string v5, ".dex"

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Lcom/google/android/ads/Obfuscator$ObfuscatorException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    invoke-static {}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->b()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sput-wide v2, Lcom/google/android/ads/GADSignalsLimitedAbstract;->k:J

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/ads/GADSignalsLimitedAbstract;->c:Z

    goto/16 :goto_0

    :catch_2
    move-exception v0

    goto/16 :goto_0

    :catch_3
    move-exception v0

    new-instance v2, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v2, v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_5
    .catch Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_4
    move-exception v0

    :try_start_6
    new-instance v2, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v2, v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :catch_5
    move-exception v0

    new-instance v2, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v2, v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :catch_6
    move-exception v0

    new-instance v2, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v2, v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :catch_7
    move-exception v0

    new-instance v2, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v2, v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_6
    .catch Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method private static b()Ljava/lang/Long;
    .locals 3

    sget-object v0, Lcom/google/android/ads/GADSignalsLimitedAbstract;->d:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/android/ads/GADSignalsLimitedAbstract;->d:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static c()Ljava/lang/String;
    .locals 3

    sget-object v0, Lcom/google/android/ads/GADSignalsLimitedAbstract;->e:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/android/ads/GADSignalsLimitedAbstract;->e:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static c(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/google/android/ads/GADSignalsLimitedAbstract;->h:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/android/ads/GADSignalsLimitedAbstract;->h:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;

    invoke-direct {v1, v0}, Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    return-object v0
.end method


# virtual methods
.method protected final b(Landroid/content/Context;)V
    .locals 3

    const/4 v0, 0x1

    :try_start_0
    invoke-static {}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x2

    :try_start_1
    invoke-static {}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->a(ILjava/lang/String;)V
    :try_end_1
    .catch Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    const/16 v0, 0x19

    :try_start_2
    invoke-static {}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->a(IJ)V
    :try_end_2
    .catch Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    const/16 v0, 0x18

    :try_start_3
    invoke-static {p1}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/ads/GADSignalsLimitedAbstract;->a(ILjava/lang/String;)V
    :try_end_3
    .catch Lcom/google/android/ads/GADSignalsLimitedAbstract$SignalUnavailableException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :goto_3
    return-void

    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_1

    :catch_4
    move-exception v0

    goto :goto_0
.end method
