.class public Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;
.super Landroid/support/v7/media/MediaRouteProviderService;
.source "SourceFile"


# instance fields
.field private a:Landroid/support/v7/media/f;

.field private b:Lcom/google/android/apps/youtube/app/ax;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v7/media/MediaRouteProviderService;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/support/v7/media/f;
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->a:Landroid/support/v7/media/f;

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ax;->aP()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "exported_remote_id"

    invoke-static {v1, v2}, Lcom/google/android/apps/youtube/app/remote/bk;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    new-instance v1, Lcom/google/android/apps/youtube/app/remote/bk;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/ax;->aX()Lcom/google/android/apps/youtube/common/network/h;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/ax;->aP()Landroid/content/SharedPreferences;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->aV()Lcom/google/android/apps/youtube/core/identity/ak;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/app/ax;->aT()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v7}, Lcom/google/android/apps/youtube/app/ax;->aR()Lcom/google/android/apps/youtube/core/identity/b;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/app/ax;->aJ()Ljava/util/concurrent/Executor;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v9}, Lcom/google/android/apps/youtube/app/ax;->f()Lcom/google/android/apps/youtube/core/client/bc;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v10}, Lcom/google/android/apps/youtube/app/ax;->b()Lcom/google/android/apps/youtube/app/aw;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/apps/youtube/app/aw;->h()Z

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v11}, Lcom/google/android/apps/youtube/app/ax;->b()Lcom/google/android/apps/youtube/app/aw;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/apps/youtube/app/aw;->i()Z

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v12}, Lcom/google/android/apps/youtube/app/ax;->b()Lcom/google/android/apps/youtube/app/aw;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/apps/youtube/app/aw;->j()Z

    move-result v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/ax;->b()Lcom/google/android/apps/youtube/app/aw;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/apps/youtube/app/aw;->k()Z

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v14}, Lcom/google/android/apps/youtube/app/ax;->Y()Lcom/google/android/apps/youtube/app/ac;

    move-result-object v14

    const/4 v15, 0x0

    invoke-direct/range {v1 .. v16}, Lcom/google/android/apps/youtube/app/remote/bk;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/network/h;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/core/identity/ak;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/identity/b;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/core/client/bc;ZZZZLcom/google/android/apps/youtube/app/ac;ZLjava/lang/String;)V

    new-instance v2, Lcom/google/android/apps/youtube/app/remote/ad;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v4}, Lcom/google/android/apps/youtube/app/ax;->Q()Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->b:Lcom/google/android/apps/youtube/app/ax;

    invoke-virtual {v5}, Lcom/google/android/apps/youtube/app/ax;->aJ()Ljava/util/concurrent/Executor;

    move-result-object v5

    invoke-direct {v2, v3, v4, v1, v5}, Lcom/google/android/apps/youtube/app/remote/ad;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/app/remote/YouTubeTvScreensMonitor;Lcom/google/android/apps/youtube/app/remote/bk;Ljava/util/concurrent/Executor;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->a:Landroid/support/v7/media/f;

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->a:Landroid/support/v7/media/f;

    return-object v1
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/support/v7/media/MediaRouteProviderService;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->d()Lcom/google/android/apps/youtube/app/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/remote/YouTubeTvRouteProviderService;->b:Lcom/google/android/apps/youtube/app/ax;

    return-void
.end method
