.class public final Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;
.super Lcom/google/android/apps/youtube/api/jar/a/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/api/jar/ah;
.implements Lcom/google/android/apps/youtube/api/jar/i;


# instance fields
.field private A:Z

.field private B:Z

.field private j:Lcom/google/android/apps/youtube/api/b/a/ao;

.field private final k:Lcom/google/android/apps/youtube/api/jar/a/dy;

.field private final l:Lcom/google/android/apps/youtube/api/jar/a/ej;

.field private final m:Lcom/google/android/apps/youtube/api/jar/a/ev;

.field private final n:Lcom/google/android/apps/youtube/api/jar/a/dt;

.field private final o:Lcom/google/android/apps/youtube/api/jar/a/v;

.field private final p:Lcom/google/android/apps/youtube/api/jar/a/ez;

.field private final q:Lcom/google/android/apps/youtube/api/jar/a/ae;

.field private final r:Lcom/google/android/apps/youtube/api/jar/a/bb;

.field private final s:Lcom/google/android/apps/youtube/api/jar/a/do;

.field private final t:Lcom/google/android/apps/youtube/api/jar/a/ed;

.field private final u:Lcom/google/android/apps/youtube/api/jar/a/fh;

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:I

.field private z:I


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/api/b/a/al;Z)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a(Landroid/app/Activity;)Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/google/android/apps/youtube/api/b/a/al;Z)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/app/Activity;Lcom/google/android/apps/youtube/api/b/a/al;Z)V
    .locals 4

    new-instance v0, Lcom/google/android/apps/youtube/api/jar/a/az;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    invoke-direct {v0, p2, v1, v2, v3}, Lcom/google/android/apps/youtube/api/jar/a/az;-><init>(Landroid/content/Context;Landroid/content/res/Resources;Ljava/lang/ClassLoader;Landroid/content/res/Resources$Theme;)V

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a;

    invoke-direct {v1, p2}, Lcom/google/android/apps/youtube/api/jar/a;-><init>(Landroid/app/Activity;)V

    invoke-direct {p0, v0, v1, p3, p4}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/a;Lcom/google/android/apps/youtube/api/b/a/al;Z)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/a;Lcom/google/android/apps/youtube/api/b/a/al;Z)V
    .locals 17

    new-instance v3, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/youtube/api/jar/a/a;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/a;Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;)V

    const-string v3, "apiPlayerFactoryService cannot be null"

    move-object/from16 v0, p3

    invoke-static {v0, v3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p4, :cond_0

    new-instance v3, Lcom/google/android/apps/youtube/api/jar/DefaultApiPlayerSurface;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/youtube/api/jar/DefaultApiPlayerSurface;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/i;)V

    new-instance v4, Lcom/google/android/apps/youtube/api/jar/a/ej;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    move-object/from16 v0, p1

    invoke-direct {v4, v3, v0, v5}, Lcom/google/android/apps/youtube/api/jar/a/ej;-><init>(Lcom/google/android/apps/youtube/api/jar/a/et;Landroid/content/Context;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->l:Lcom/google/android/apps/youtube/api/jar/a/ej;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->m:Lcom/google/android/apps/youtube/api/jar/a/ev;

    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->b:Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/api/jar/h;->a()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;->setVideoView(Landroid/view/View;)V

    new-instance v4, Lcom/google/android/apps/youtube/api/jar/a/dy;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->b:Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v4, v5, v6}, Lcom/google/android/apps/youtube/api/jar/a/dy;-><init>(Lcom/google/android/apps/youtube/core/player/am;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->k:Lcom/google/android/apps/youtube/api/jar/a/dy;

    new-instance v4, Lcom/google/android/apps/youtube/api/jar/a/dt;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v4, v3, v5}, Lcom/google/android/apps/youtube/api/jar/a/dt;-><init>(Lcom/google/android/apps/youtube/api/jar/h;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->n:Lcom/google/android/apps/youtube/api/jar/a/dt;

    new-instance v3, Lcom/google/android/apps/youtube/api/jar/a/v;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->d:Lcom/google/android/apps/youtube/core/player/overlay/a;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/youtube/api/jar/a/v;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/a;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->o:Lcom/google/android/apps/youtube/api/jar/a/v;

    new-instance v3, Lcom/google/android/apps/youtube/api/jar/a/ez;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->e:Lcom/google/android/apps/youtube/core/player/overlay/DefaultSurveyOverlay;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/youtube/api/jar/a/ez;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/bm;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->p:Lcom/google/android/apps/youtube/api/jar/a/ez;

    new-instance v3, Lcom/google/android/apps/youtube/api/jar/a/ae;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->f:Lcom/google/android/apps/youtube/core/player/overlay/g;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/youtube/api/jar/a/ae;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/g;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->q:Lcom/google/android/apps/youtube/api/jar/a/ae;

    new-instance v3, Lcom/google/android/apps/youtube/api/jar/a/bb;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->c:Lcom/google/android/apps/youtube/api/jar/b;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/youtube/api/jar/a/bb;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/ControlsOverlay;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->r:Lcom/google/android/apps/youtube/api/jar/a/bb;

    new-instance v3, Lcom/google/android/apps/youtube/api/jar/a/do;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->g:Lcom/google/android/apps/youtube/core/player/overlay/ak;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/youtube/api/jar/a/do;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/ak;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->s:Lcom/google/android/apps/youtube/api/jar/a/do;

    new-instance v3, Lcom/google/android/apps/youtube/api/jar/a/ed;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->h:Lcom/google/android/apps/youtube/core/player/overlay/be;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/youtube/api/jar/a/ed;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/be;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->t:Lcom/google/android/apps/youtube/api/jar/a/ed;

    new-instance v3, Lcom/google/android/apps/youtube/api/jar/a/fh;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->i:Lcom/google/android/apps/youtube/core/player/overlay/br;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/youtube/api/jar/a/fh;-><init>(Lcom/google/android/apps/youtube/core/player/overlay/br;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->u:Lcom/google/android/apps/youtube/api/jar/a/fh;

    new-instance v4, Lcom/google/android/youtube/api/jar/client/a;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v3}, Lcom/google/android/youtube/api/jar/client/a;-><init>(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;B)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->k:Lcom/google/android/apps/youtube/api/jar/a/dy;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->l:Lcom/google/android/apps/youtube/api/jar/a/ej;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->m:Lcom/google/android/apps/youtube/api/jar/a/ev;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->n:Lcom/google/android/apps/youtube/api/jar/a/dt;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->o:Lcom/google/android/apps/youtube/api/jar/a/v;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->p:Lcom/google/android/apps/youtube/api/jar/a/ez;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->q:Lcom/google/android/apps/youtube/api/jar/a/ae;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->r:Lcom/google/android/apps/youtube/api/jar/a/bb;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->s:Lcom/google/android/apps/youtube/api/jar/a/do;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->t:Lcom/google/android/apps/youtube/api/jar/a/ed;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->u:Lcom/google/android/apps/youtube/api/jar/a/fh;

    move-object/from16 v3, p3

    move/from16 v16, p4

    invoke-interface/range {v3 .. v16}, Lcom/google/android/apps/youtube/api/b/a/al;->a(Lcom/google/android/apps/youtube/api/jar/a/ck;Lcom/google/android/apps/youtube/api/jar/a/cw;Lcom/google/android/apps/youtube/api/jar/a/dc;Lcom/google/android/apps/youtube/api/jar/a/df;Lcom/google/android/apps/youtube/api/jar/a/ct;Lcom/google/android/apps/youtube/api/jar/a/ce;Lcom/google/android/apps/youtube/api/jar/a/di;Lcom/google/android/apps/youtube/api/jar/a/ch;Lcom/google/android/apps/youtube/api/jar/a/cn;Lcom/google/android/apps/youtube/api/jar/a/cq;Lcom/google/android/apps/youtube/api/jar/a/cz;Lcom/google/android/apps/youtube/api/jar/a/dl;Z)Lcom/google/android/apps/youtube/api/b/a/ao;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    return-void

    :cond_0
    new-instance v3, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/youtube/api/jar/TextureApiPlayerSurface;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/api/jar/ah;)V

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->l:Lcom/google/android/apps/youtube/api/jar/a/ej;

    new-instance v4, Lcom/google/android/apps/youtube/api/jar/a/ev;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    move-object/from16 v0, p1

    invoke-direct {v4, v3, v0, v5}, Lcom/google/android/apps/youtube/api/jar/a/ev;-><init>(Lcom/google/android/apps/youtube/api/jar/a/ey;Landroid/content/Context;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->m:Lcom/google/android/apps/youtube/api/jar/a/ev;

    goto/16 :goto_0
.end method

.method public constructor <init>(Landroid/os/IBinder;Landroid/os/IBinder;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;-><init>(Landroid/os/IBinder;Landroid/os/IBinder;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Z)V
    .locals 3

    invoke-static {p1}, Lcom/google/android/youtube/player/internal/dynamic/b;->a(Landroid/os/IBinder;)Lcom/google/android/youtube/player/internal/dynamic/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/player/internal/dynamic/d;->a(Lcom/google/android/youtube/player/internal/dynamic/a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/youtube/player/internal/dynamic/b;->a(Landroid/os/IBinder;)Lcom/google/android/youtube/player/internal/dynamic/a;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/player/internal/dynamic/d;->a(Lcom/google/android/youtube/player/internal/dynamic/a;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-static {p3}, Lcom/google/android/apps/youtube/api/b/a/am;->a(Landroid/os/IBinder;)Lcom/google/android/apps/youtube/api/b/a/al;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2, p4}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;-><init>(Landroid/content/Context;Landroid/app/Activity;Lcom/google/android/apps/youtube/api/b/a/al;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/os/IBinder;Landroid/os/IBinder;Z)V
    .locals 2

    invoke-static {p1}, Lcom/google/android/youtube/player/internal/dynamic/b;->a(Landroid/os/IBinder;)Lcom/google/android/youtube/player/internal/dynamic/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/player/internal/dynamic/d;->a(Lcom/google/android/youtube/player/internal/dynamic/a;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {p2}, Lcom/google/android/apps/youtube/api/b/a/am;->a(Landroid/os/IBinder;)Lcom/google/android/apps/youtube/api/b/a/al;

    move-result-object v1

    invoke-direct {p0, v0, v1, p3}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;-><init>(Landroid/app/Activity;Lcom/google/android/apps/youtube/api/b/a/al;Z)V

    return-void
.end method

.method static synthetic A(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->P()V

    return-void
.end method

.method static synthetic B(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic C(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->Q()V

    return-void
.end method

.method static synthetic D(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    return p1
.end method

.method private static a(Landroid/app/Activity;)Landroid/app/Activity;
    .locals 6

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Landroid/app/Activity;

    if-ne v4, v5, :cond_0

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    :try_start_0
    invoke-virtual {v3, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Could not get the activity from the ActivityWrapper"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to extract the wrapped activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->T()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->x:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;I)I
    .locals 0

    iput p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->z:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->U()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->w:Z

    return p1
.end method

.method static synthetic c(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->V()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->g(I)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->v:Z

    return p1
.end method

.method static synthetic d(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->k(Z)V

    return-void
.end method

.method static synthetic e(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->W()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j(Z)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;Z)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->l(Z)V

    return-void
.end method

.method static synthetic g(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->X()V

    return-void
.end method

.method static synthetic h(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->O()V

    return-void
.end method

.method static synthetic i(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->Y()V

    return-void
.end method

.method static synthetic k(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->Z()V

    return-void
.end method

.method static synthetic l(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->R()V

    return-void
.end method

.method static synthetic m(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->aa()V

    return-void
.end method

.method static synthetic o(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->S()V

    return-void
.end method

.method static synthetic p(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic q(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->ab()V

    return-void
.end method

.method static synthetic r(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->S()V

    return-void
.end method

.method static synthetic s(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic t(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->ac()V

    return-void
.end method

.method static synthetic u(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic v(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic w(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic x(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic y(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->N()V

    return-void
.end method

.method static synthetic z(Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final A()V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/b/a/ao;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final B()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->v:Z

    return v0
.end method

.method public final C()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->w:Z

    return v0
.end method

.method public final D()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->x:Z

    return v0
.end method

.method public final E()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/b/a/ao;->f()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final F()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/b/a/ao;->g()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final G()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    return v0
.end method

.method public final H()I
    .locals 1

    iget v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->z:I

    return v0
.end method

.method public final I()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/b/a/ao;->i()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final J()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/b/a/ao;->j()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final K()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/b/a/ao;->h()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final L()Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/b/a/ao;->e()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final M()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->A:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/b/a/ao;->l()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->A:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->M()V

    :cond_0
    return-void
.end method

.method protected final a([B)Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/ao;->a([B)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->A:Z

    return-void
.end method

.method public final c()V
    .locals 2

    const-string v0, "Cannot attach a YouTubePlayerView backed by a TextureView to a Window that is not hardware accelerated"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/youtube/player/internal/b/a;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;->UNABLE_TO_USE_TEXTUREVIEW:Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->a(Lcom/google/android/youtube/player/YouTubePlayer$ErrorReason;)V

    return-void
.end method

.method public final c(Ljava/lang/String;I)V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    iput p2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/api/b/a/ao;->a(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(Ljava/lang/String;II)V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    iput p3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/b/a/ao;->a(Ljava/lang/String;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(Ljava/util/List;II)V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    iput p3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/b/a/ao;->a(Ljava/util/List;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(ILandroid/view/KeyEvent;)Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/api/b/a/ao;->a(ILandroid/view/KeyEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d(Ljava/lang/String;I)V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    iput p2, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/api/b/a/ao;->b(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d(Ljava/lang/String;II)V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    iput p3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/b/a/ao;->b(Ljava/lang/String;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final d(Ljava/util/List;II)V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    iput p3, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/api/b/a/ao;->b(Ljava/util/List;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected final d()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/youtube/api/jar/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(ILandroid/view/KeyEvent;)Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/api/b/a/ao;->b(ILandroid/view/KeyEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final e(I)V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    iput p1, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/ao;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final f(I)V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    iget v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->y:I

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/ao;->b(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final f(Z)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/ao;->b(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final g(Z)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/ao;->c(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final h(Z)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/ao;->d(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final i(Z)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/ao;->a(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->l:Lcom/google/android/apps/youtube/api/jar/a/ej;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->l:Lcom/google/android/apps/youtube/api/jar/a/ej;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/a/ej;->d()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->m:Lcom/google/android/apps/youtube/api/jar/a/ev;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->m:Lcom/google/android/apps/youtube/api/jar/a/ev;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/a/ev;->a()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->k:Lcom/google/android/apps/youtube/api/jar/a/dy;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/a/dy;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->o:Lcom/google/android/apps/youtube/api/jar/a/v;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/a/v;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->q:Lcom/google/android/apps/youtube/api/jar/a/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/a/ae;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->r:Lcom/google/android/apps/youtube/api/jar/a/bb;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/a/bb;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->s:Lcom/google/android/apps/youtube/api/jar/a/do;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/a/do;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final x()[B
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/b/a/ao;->k()[B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final y()V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/b/a/ao;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final z()V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->B:Z

    iget-object v0, p0, Lcom/google/android/youtube/api/jar/client/RemoteEmbeddedPlayer;->j:Lcom/google/android/apps/youtube/api/b/a/ao;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/b/a/ao;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
