.class public abstract Lcom/google/protobuf/nano/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected dm:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/protobuf/nano/c;->dm:I

    return-void
.end method

.method public static final a(Lcom/google/protobuf/nano/c;[B)Lcom/google/protobuf/nano/c;
    .locals 2

    const/4 v0, 0x0

    array-length v1, p1

    invoke-static {p0, p1, v0, v1}, Lcom/google/protobuf/nano/c;->b(Lcom/google/protobuf/nano/c;[BII)Lcom/google/protobuf/nano/c;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/protobuf/nano/c;[BII)V
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, v0, p3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->a([BII)Lcom/google/protobuf/nano/CodedOutputByteBufferNano;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    invoke-virtual {v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static final a(Lcom/google/protobuf/nano/c;Lcom/google/protobuf/nano/c;)Z
    .locals 4

    const/4 v0, 0x0

    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/protobuf/nano/c;->a()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/protobuf/nano/c;->a()I

    move-result v2

    if-ne v2, v1, :cond_0

    new-array v2, v1, [B

    new-array v3, v1, [B

    invoke-static {p0, v2, v0, v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[BII)V

    invoke-static {p1, v3, v0, v1}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[BII)V

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public static final a(Lcom/google/protobuf/nano/c;)[B
    .locals 3

    invoke-virtual {p0}, Lcom/google/protobuf/nano/c;->a()I

    move-result v0

    new-array v0, v0, [B

    const/4 v1, 0x0

    array-length v2, v0

    invoke-static {p0, v0, v1, v2}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/c;[BII)V

    return-object v0
.end method

.method private static b(Lcom/google/protobuf/nano/c;[BII)Lcom/google/protobuf/nano/c;
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, v0, p3}, Lcom/google/protobuf/nano/a;->a([BII)Lcom/google/protobuf/nano/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/protobuf/nano/c;->a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/protobuf/nano/a;->a(I)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    return-object p0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/protobuf/nano/c;->dm:I

    return v0
.end method

.method public abstract a(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/c;
.end method

.method public abstract a(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/google/protobuf/nano/c;->dm:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/protobuf/nano/c;->a()I

    :cond_0
    iget v0, p0, Lcom/google/protobuf/nano/c;->dm:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/google/protobuf/nano/d;->a(Lcom/google/protobuf/nano/c;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
