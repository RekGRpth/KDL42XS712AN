.class public final Lcom/google/ads/conversiontracking/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Z

.field private c:Lcom/google/ads/conversiontracking/GoogleConversionPing$ConversionType;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Lcom/google/ads/conversiontracking/e;

.field private h:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/ads/conversiontracking/g;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/ads/conversiontracking/g;->b:Z

    return v0
.end method

.method static synthetic b(Lcom/google/ads/conversiontracking/g;)Lcom/google/ads/conversiontracking/GoogleConversionPing$ConversionType;
    .locals 1

    iget-object v0, p0, Lcom/google/ads/conversiontracking/g;->c:Lcom/google/ads/conversiontracking/GoogleConversionPing$ConversionType;

    return-object v0
.end method

.method static synthetic c(Lcom/google/ads/conversiontracking/g;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/ads/conversiontracking/g;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/ads/conversiontracking/g;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/ads/conversiontracking/g;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/ads/conversiontracking/g;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/google/ads/conversiontracking/g;->h:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Lcom/google/ads/conversiontracking/g;)Lcom/google/ads/conversiontracking/e;
    .locals 1

    iget-object v0, p0, Lcom/google/ads/conversiontracking/g;->g:Lcom/google/ads/conversiontracking/e;

    return-object v0
.end method

.method static synthetic g(Lcom/google/ads/conversiontracking/g;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/ads/conversiontracking/g;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/google/ads/conversiontracking/g;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/ads/conversiontracking/g;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/ads/conversiontracking/g;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ads/conversiontracking/g;->b:Z

    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/ads/conversiontracking/g;
    .locals 0

    iput-object p1, p0, Lcom/google/ads/conversiontracking/g;->a:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Ljava/util/Map;)Lcom/google/ads/conversiontracking/g;
    .locals 0

    iput-object p1, p0, Lcom/google/ads/conversiontracking/g;->h:Ljava/util/Map;

    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/ads/conversiontracking/g;
    .locals 0

    iput-object p1, p0, Lcom/google/ads/conversiontracking/g;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/ads/conversiontracking/g;
    .locals 0

    iput-object p1, p0, Lcom/google/ads/conversiontracking/g;->e:Ljava/lang/String;

    return-object p0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/ads/conversiontracking/g;
    .locals 0

    iput-object p1, p0, Lcom/google/ads/conversiontracking/g;->f:Ljava/lang/String;

    return-object p0
.end method
