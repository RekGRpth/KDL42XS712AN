.class public final Lcom/google/ads/conversiontracking/GoogleConversionPing;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 5

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GoogleConversionPing"

    const-string v1, "Error sending remarketing ping with empty screen name"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    new-instance v0, Lcom/google/ads/conversiontracking/g;

    invoke-direct {v0}, Lcom/google/ads/conversiontracking/g;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/ads/conversiontracking/g;->a(Ljava/lang/String;)Lcom/google/ads/conversiontracking/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ads/conversiontracking/g;->a()Lcom/google/ads/conversiontracking/g;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/ads/conversiontracking/g;->b(Ljava/lang/String;)Lcom/google/ads/conversiontracking/g;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/ads/conversiontracking/g;->c(Ljava/lang/String;)Lcom/google/ads/conversiontracking/g;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/ads/conversiontracking/g;->d(Ljava/lang/String;)Lcom/google/ads/conversiontracking/g;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/ads/conversiontracking/g;->a(Ljava/util/Map;)Lcom/google/ads/conversiontracking/g;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/ads/conversiontracking/b;->b(Landroid/content/Context;Lcom/google/ads/conversiontracking/g;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/google/ads/conversiontracking/a;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v2, v0, v3, v4, p0}, Lcom/google/ads/conversiontracking/a;-><init>(Ljava/lang/String;ZLcom/google/ads/conversiontracking/g;Landroid/content/Context;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "GoogleConversionPing"

    const-string v2, "Error sending ping"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
