.class public final Lcom/android/volley/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/p;


# instance fields
.field private final a:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/volley/f;

    invoke-direct {v0, p0, p1}, Lcom/android/volley/f;-><init>(Lcom/android/volley/e;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/volley/e;->a:Ljava/util/concurrent/Executor;

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/Request;Lcom/android/volley/VolleyError;)V
    .locals 4

    const-string v0, "post-error"

    invoke-virtual {p1, v0}, Lcom/android/volley/Request;->a(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/android/volley/m;->a(Lcom/android/volley/VolleyError;)Lcom/android/volley/m;

    move-result-object v0

    iget-object v1, p0, Lcom/android/volley/e;->a:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/android/volley/g;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, v0, v3}, Lcom/android/volley/g;-><init>(Lcom/android/volley/e;Lcom/android/volley/Request;Lcom/android/volley/m;Ljava/lang/Runnable;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Lcom/android/volley/Request;Lcom/android/volley/m;)V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/android/volley/Request;->q()V

    const-string v1, "post-response"

    invoke-virtual {p1, v1}, Lcom/android/volley/Request;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/volley/e;->a:Ljava/util/concurrent/Executor;

    new-instance v2, Lcom/android/volley/g;

    invoke-direct {v2, p0, p1, p2, v0}, Lcom/android/volley/g;-><init>(Lcom/android/volley/e;Lcom/android/volley/Request;Lcom/android/volley/m;Ljava/lang/Runnable;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
