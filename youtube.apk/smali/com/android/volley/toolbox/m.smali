.class public final Lcom/android/volley/toolbox/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/android/volley/toolbox/g;

.field private b:Landroid/graphics/Bitmap;

.field private final c:Lcom/android/volley/toolbox/n;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/volley/toolbox/g;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/toolbox/n;)V
    .locals 0

    iput-object p1, p0, Lcom/android/volley/toolbox/m;->a:Lcom/android/volley/toolbox/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/volley/toolbox/m;->b:Landroid/graphics/Bitmap;

    iput-object p3, p0, Lcom/android/volley/toolbox/m;->e:Ljava/lang/String;

    iput-object p4, p0, Lcom/android/volley/toolbox/m;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/android/volley/toolbox/m;->c:Lcom/android/volley/toolbox/n;

    return-void
.end method

.method static synthetic a(Lcom/android/volley/toolbox/m;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    iput-object p1, p0, Lcom/android/volley/toolbox/m;->b:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic a(Lcom/android/volley/toolbox/m;)Lcom/android/volley/toolbox/n;
    .locals 1

    iget-object v0, p0, Lcom/android/volley/toolbox/m;->c:Lcom/android/volley/toolbox/n;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/android/volley/toolbox/m;->c:Lcom/android/volley/toolbox/n;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/volley/toolbox/m;->a:Lcom/android/volley/toolbox/g;

    invoke-static {v0}, Lcom/android/volley/toolbox/g;->a(Lcom/android/volley/toolbox/g;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/android/volley/toolbox/m;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/volley/toolbox/k;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p0}, Lcom/android/volley/toolbox/k;->b(Lcom/android/volley/toolbox/m;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/volley/toolbox/m;->a:Lcom/android/volley/toolbox/g;

    invoke-static {v0}, Lcom/android/volley/toolbox/g;->a(Lcom/android/volley/toolbox/g;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/android/volley/toolbox/m;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/volley/toolbox/m;->a:Lcom/android/volley/toolbox/g;

    invoke-static {v0}, Lcom/android/volley/toolbox/g;->b(Lcom/android/volley/toolbox/g;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/android/volley/toolbox/m;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/volley/toolbox/k;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/android/volley/toolbox/k;->b(Lcom/android/volley/toolbox/m;)Z

    invoke-static {v0}, Lcom/android/volley/toolbox/k;->a(Lcom/android/volley/toolbox/k;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/volley/toolbox/m;->a:Lcom/android/volley/toolbox/g;

    invoke-static {v0}, Lcom/android/volley/toolbox/g;->b(Lcom/android/volley/toolbox/g;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/android/volley/toolbox/m;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/android/volley/toolbox/m;->b:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/volley/toolbox/m;->e:Ljava/lang/String;

    return-object v0
.end method
