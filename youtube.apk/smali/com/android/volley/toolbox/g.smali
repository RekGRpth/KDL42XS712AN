.class public final Lcom/android/volley/toolbox/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/android/volley/l;

.field private b:I

.field private final c:Lcom/android/volley/toolbox/l;

.field private final d:Ljava/util/HashMap;

.field private final e:Ljava/util/HashMap;

.field private final f:Landroid/os/Handler;

.field private g:Ljava/lang/Runnable;


# direct methods
.method static synthetic a(Lcom/android/volley/toolbox/g;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/volley/toolbox/g;->g:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic a(Lcom/android/volley/toolbox/g;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/android/volley/toolbox/g;->d:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic a(Lcom/android/volley/toolbox/g;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/android/volley/toolbox/g;->c:Lcom/android/volley/toolbox/l;

    iget-object v0, p0, Lcom/android/volley/toolbox/g;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/volley/toolbox/k;

    if-eqz v0, :cond_0

    invoke-static {v0, p2}, Lcom/android/volley/toolbox/k;->a(Lcom/android/volley/toolbox/k;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    invoke-direct {p0, p1, v0}, Lcom/android/volley/toolbox/g;->a(Ljava/lang/String;Lcom/android/volley/toolbox/k;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/android/volley/toolbox/g;Ljava/lang/String;Lcom/android/volley/VolleyError;)V
    .locals 1

    iget-object v0, p0, Lcom/android/volley/toolbox/g;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/volley/toolbox/k;

    invoke-virtual {v0, p2}, Lcom/android/volley/toolbox/k;->a(Lcom/android/volley/VolleyError;)V

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, v0}, Lcom/android/volley/toolbox/g;->a(Ljava/lang/String;Lcom/android/volley/toolbox/k;)V

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/android/volley/toolbox/k;)V
    .locals 4

    iget-object v0, p0, Lcom/android/volley/toolbox/g;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/volley/toolbox/g;->g:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/volley/toolbox/j;

    invoke-direct {v0, p0}, Lcom/android/volley/toolbox/j;-><init>(Lcom/android/volley/toolbox/g;)V

    iput-object v0, p0, Lcom/android/volley/toolbox/g;->g:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/android/volley/toolbox/g;->f:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/volley/toolbox/g;->g:Ljava/lang/Runnable;

    iget v2, p0, Lcom/android/volley/toolbox/g;->b:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/android/volley/toolbox/g;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/android/volley/toolbox/g;->e:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/android/volley/toolbox/n;)Lcom/android/volley/toolbox/m;
    .locals 12

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ImageLoader must be invoked from the main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xc

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "#W"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "#H"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/android/volley/toolbox/g;->c:Lcom/android/volley/toolbox/l;

    invoke-interface {v0}, Lcom/android/volley/toolbox/l;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v0, Lcom/android/volley/toolbox/m;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/android/volley/toolbox/m;-><init>(Lcom/android/volley/toolbox/g;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/toolbox/n;)V

    const/4 v1, 0x1

    invoke-interface {p2, v0, v1}, Lcom/android/volley/toolbox/n;->a(Lcom/android/volley/toolbox/m;Z)V

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/android/volley/toolbox/m;

    const/4 v2, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/volley/toolbox/m;-><init>(Lcom/android/volley/toolbox/g;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/toolbox/n;)V

    const/4 v1, 0x1

    invoke-interface {p2, v0, v1}, Lcom/android/volley/toolbox/n;->a(Lcom/android/volley/toolbox/m;Z)V

    iget-object v1, p0, Lcom/android/volley/toolbox/g;->d:Ljava/util/HashMap;

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/volley/toolbox/k;

    if-eqz v1, :cond_2

    invoke-virtual {v1, v0}, Lcom/android/volley/toolbox/k;->a(Lcom/android/volley/toolbox/m;)V

    goto :goto_0

    :cond_2
    new-instance v5, Lcom/android/volley/toolbox/o;

    new-instance v7, Lcom/android/volley/toolbox/h;

    invoke-direct {v7, p0, v4}, Lcom/android/volley/toolbox/h;-><init>(Lcom/android/volley/toolbox/g;Ljava/lang/String;)V

    const/4 v8, 0x0

    const/4 v9, 0x0

    sget-object v10, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    new-instance v11, Lcom/android/volley/toolbox/i;

    invoke-direct {v11, p0, v4}, Lcom/android/volley/toolbox/i;-><init>(Lcom/android/volley/toolbox/g;Ljava/lang/String;)V

    move-object v6, p1

    invoke-direct/range {v5 .. v11}, Lcom/android/volley/toolbox/o;-><init>(Ljava/lang/String;Lcom/android/volley/o;IILandroid/graphics/Bitmap$Config;Lcom/android/volley/n;)V

    iget-object v1, p0, Lcom/android/volley/toolbox/g;->a:Lcom/android/volley/l;

    invoke-virtual {v1, v5}, Lcom/android/volley/l;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    iget-object v1, p0, Lcom/android/volley/toolbox/g;->d:Ljava/util/HashMap;

    new-instance v2, Lcom/android/volley/toolbox/k;

    invoke-direct {v2, p0, v5, v0}, Lcom/android/volley/toolbox/k;-><init>(Lcom/android/volley/toolbox/g;Lcom/android/volley/Request;Lcom/android/volley/toolbox/m;)V

    invoke-virtual {v1, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
