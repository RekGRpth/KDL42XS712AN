.class public final Lcom/android/volley/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/Object;

.field public final b:Lcom/android/volley/b;

.field public final c:Lcom/android/volley/VolleyError;

.field public d:Z


# direct methods
.method private constructor <init>(Lcom/android/volley/VolleyError;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/volley/m;->d:Z

    iput-object v1, p0, Lcom/android/volley/m;->a:Ljava/lang/Object;

    iput-object v1, p0, Lcom/android/volley/m;->b:Lcom/android/volley/b;

    iput-object p1, p0, Lcom/android/volley/m;->c:Lcom/android/volley/VolleyError;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;Lcom/android/volley/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/volley/m;->d:Z

    iput-object p1, p0, Lcom/android/volley/m;->a:Ljava/lang/Object;

    iput-object p2, p0, Lcom/android/volley/m;->b:Lcom/android/volley/b;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/volley/m;->c:Lcom/android/volley/VolleyError;

    return-void
.end method

.method public static a(Lcom/android/volley/VolleyError;)Lcom/android/volley/m;
    .locals 1

    new-instance v0, Lcom/android/volley/m;

    invoke-direct {v0, p0}, Lcom/android/volley/m;-><init>(Lcom/android/volley/VolleyError;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Lcom/android/volley/b;)Lcom/android/volley/m;
    .locals 1

    new-instance v0, Lcom/android/volley/m;

    invoke-direct {v0, p0, p1}, Lcom/android/volley/m;-><init>(Ljava/lang/Object;Lcom/android/volley/b;)V

    return-object v0
.end method
