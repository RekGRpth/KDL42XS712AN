.class Lcom/konka/lockscreen/DateWeather$4;
.super Ljava/lang/Thread;
.source "DateWeather.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/lockscreen/DateWeather;->checkNetwork()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/lockscreen/DateWeather;


# direct methods
.method constructor <init>(Lcom/konka/lockscreen/DateWeather;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/lockscreen/DateWeather$4;->this$0:Lcom/konka/lockscreen/DateWeather;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    invoke-super {p0}, Ljava/lang/Thread;->run()V

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather$4;->this$0:Lcom/konka/lockscreen/DateWeather;

    # invokes: Lcom/konka/lockscreen/DateWeather;->isNetworkAvailable()Z
    invoke-static {v0}, Lcom/konka/lockscreen/DateWeather;->access$800(Lcom/konka/lockscreen/DateWeather;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather$4;->this$0:Lcom/konka/lockscreen/DateWeather;

    # getter for: Lcom/konka/lockscreen/DateWeather;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/konka/lockscreen/DateWeather;->access$700(Lcom/konka/lockscreen/DateWeather;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather$4;->this$0:Lcom/konka/lockscreen/DateWeather;

    # getter for: Lcom/konka/lockscreen/DateWeather;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/konka/lockscreen/DateWeather;->access$700(Lcom/konka/lockscreen/DateWeather;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    const-string v0, "DateWeather"

    const-string v1, "network is available"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v0, p0, Lcom/konka/lockscreen/DateWeather$4;->this$0:Lcom/konka/lockscreen/DateWeather;

    const/4 v1, 0x0

    # setter for: Lcom/konka/lockscreen/DateWeather;->mNetThread:Ljava/lang/Thread;
    invoke-static {v0, v1}, Lcom/konka/lockscreen/DateWeather;->access$902(Lcom/konka/lockscreen/DateWeather;Ljava/lang/Thread;)Ljava/lang/Thread;

    return-void

    :cond_1
    const-string v0, "DateWeather"

    const-string v1, "network is unavailable"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
