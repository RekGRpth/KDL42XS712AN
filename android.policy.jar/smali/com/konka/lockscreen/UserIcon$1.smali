.class Lcom/konka/lockscreen/UserIcon$1;
.super Ljava/lang/Object;
.source "UserIcon.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/lockscreen/UserIcon;->addEvent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/lockscreen/UserIcon;


# direct methods
.method constructor <init>(Lcom/konka/lockscreen/UserIcon;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/lockscreen/UserIcon$1;->this$0:Lcom/konka/lockscreen/UserIcon;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    sget-object v0, Lcom/konka/lockscreen/UserIcon$3;->$SwitchMap$com$konka$lockscreen$UserMode:[I

    iget-object v1, p0, Lcom/konka/lockscreen/UserIcon$1;->this$0:Lcom/konka/lockscreen/UserIcon;

    # getter for: Lcom/konka/lockscreen/UserIcon;->mUserMode:Lcom/konka/lockscreen/UserMode;
    invoke-static {v1}, Lcom/konka/lockscreen/UserIcon;->access$000(Lcom/konka/lockscreen/UserIcon;)Lcom/konka/lockscreen/UserMode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/lockscreen/UserMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/lockscreen/UserIcon$1;->this$0:Lcom/konka/lockscreen/UserIcon;

    # getter for: Lcom/konka/lockscreen/UserIcon;->mUsers:Lcom/konka/lockscreen/Users;
    invoke-static {v0}, Lcom/konka/lockscreen/UserIcon;->access$100(Lcom/konka/lockscreen/UserIcon;)Lcom/konka/lockscreen/Users;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/lockscreen/Users;->prev()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/lockscreen/UserIcon$1;->this$0:Lcom/konka/lockscreen/UserIcon;

    # getter for: Lcom/konka/lockscreen/UserIcon;->mUsers:Lcom/konka/lockscreen/Users;
    invoke-static {v0}, Lcom/konka/lockscreen/UserIcon;->access$100(Lcom/konka/lockscreen/UserIcon;)Lcom/konka/lockscreen/Users;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/lockscreen/Users;->next()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/lockscreen/UserIcon$1;->this$0:Lcom/konka/lockscreen/UserIcon;

    # getter for: Lcom/konka/lockscreen/UserIcon;->mUsers:Lcom/konka/lockscreen/Users;
    invoke-static {v0}, Lcom/konka/lockscreen/UserIcon;->access$100(Lcom/konka/lockscreen/UserIcon;)Lcom/konka/lockscreen/Users;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/lockscreen/Users;->enter()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
