.class Lcom/konka/lockscreen/Messages$1;
.super Ljava/lang/Object;
.source "Messages.java"

# interfaces
.implements Lcom/konka/lockscreen/service/IMessageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/lockscreen/Messages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/lockscreen/Messages;


# direct methods
.method constructor <init>(Lcom/konka/lockscreen/Messages;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/lockscreen/Messages$1;->this$0:Lcom/konka/lockscreen/Messages;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public messages(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public updates(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/konka/lockscreen/Messages$1;->this$0:Lcom/konka/lockscreen/Messages;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/konka/lockscreen/Messages;->mUpdateInfo:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/konka/lockscreen/Messages;->access$002(Lcom/konka/lockscreen/Messages;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/lockscreen/Messages$1;->this$0:Lcom/konka/lockscreen/Messages;

    # getter for: Lcom/konka/lockscreen/Messages;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/konka/lockscreen/Messages;->access$100(Lcom/konka/lockscreen/Messages;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/konka/lockscreen/Messages$1;->this$0:Lcom/konka/lockscreen/Messages;

    # getter for: Lcom/konka/lockscreen/Messages;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/lockscreen/Messages;->access$200(Lcom/konka/lockscreen/Messages;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/lockscreen/service/MessageService;->destroyUpdatesService(Landroid/content/Context;)V

    return-void
.end method
